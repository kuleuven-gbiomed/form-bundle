###
##. Configuration
###

RUN_UID ?= $(shell cat /dev/urandom | env LC_CTYPE=C tr -dc 'a-z0-9' | fold -w 16 | head -n 1)

THIS_MAKEFILE ?= $(lastword $(MAKEFILE_LIST))
THIS_MAKE ?= $(MAKE) --file $(THIS_MAKEFILE)

QA_MAKEFILE = qa
QA_MAKE = $(MAKE) --file $(QA_MAKEFILE)

DOCKER ?= $(shell command -v docker 2> /dev/null)
DOCKER_SOCKET ?= /var/run/docker.sock
DOCKER_SERVICE_NAME ?= docker
DOCKER_CONFIG_FILE ?= /etc/docker/daemon.json
DOCKER_SWARM_DEPLOY_FLAGS ?= --compose-file swarm.yml --prune

# This checks if you are running in an OSX environment. If so, the swarm.osx.yml override gets loaded.
# You can check the config override with: docker-compose -f swarm.yml -f swarm.osx.yml config
UNAME := $(shell uname)

ifeq ($(findstring swarm.ci.yml,$(DOCKER_SWARM_DEPLOY_FLAGS)),)
    # only do this if we are not using swarm.ci.yml
    ifeq (Darwin, $(UNAME)$(findstring swarm.osx.yml,$(DOCKER_SWARM_DEPLOY_FLAGS)))
      DOCKER_SWARM_DEPLOY_FLAGS	+= --compose-file swarm.osx.yml
    endif

    ifeq (Linux, $(UNAME)$(findstring swarm.linux.yml,$(DOCKER_SWARM_DEPLOY_FLAGS)))
      DOCKER_SWARM_DEPLOY_FLAGS	+= --compose-file swarm.linux.yml
    endif
endif

CENTOS_DOCKER_REPOSITORY ?= https://download.docker.com/linux/centos/docker-ce.repo
UBUNTU_DOCKER_REPOSITORY ?= https://download.docker.com/linux/ubuntu
CENTOS_DOCKER_VERSION ?= docker-ce-18.03.1.ce-1.el7.centos
UBUNTU_DOCKER_VERSION ?= 18.03.0~ce-0~ubuntu

BASH_IMAGE ?= bash
BASH ?= $(DOCKER) run --rm --name bash-$(RUN_UID) $(BASH_IMAGE) bash

JQ_IMAGE ?= phpqa/jq
JQ ?= $(DOCKER) run --rm --name jq-$(RUN_UID) $(JQ_IMAGE) jq

PHP = $(shell command -v php 2> /dev/null)
PHP_RUN_COMMAND = $(PHP) -d memory_limit=1G

STYLE_RESET = \033[0m
STYLE_TITLE = \033[1;33m
STYLE_ERROR = \033[31m
STYLE_SUCCESS = \033[32m
STYLE_DIM = \033[2m

###
## About
###

.PHONY: help version
.DEFAULT_GOAL: help

HELP_MAKEFILE = $(THIS_MAKEFILE)
HELP_MAKE = $(THIS_MAKE)

# Print this documentation
help:

	@ \
		regexp=$$(                                                                                                     \
			$(HELP_MAKE) --print-data-base --no-builtin-rules --no-builtin-variables : 2>/dev/null                     \
			| awk '/^[a-zA-Z0-9_%-]+:/{ if (skipped) printf "|"; printf "^%s", $$1; skipped=1 }'                       \
		);                                                                                                             \
		awk -v pattern="$${regexp}" '                                                                                  \
			{ if (/^## /) { printf "\n%s\n",substr($$0,4); next } }                                                    \
			{ if ($$0 ~ pattern && doc) { gsub(/:.*/,"",$$1); printf "\033[36m%-40s\033[0m %s\n", $$1, doc; } }        \
			{ if (/^# /) { doc=substr($$0,3,match($$0"# TODO",/# TODO/)-3) } else { doc="No documentation" } }         \
			{ if (/^#\. /) { doc="" } }                                                                                \
			{ gsub(/#!/,"\xE2\x9D\x97 ",doc) }                                                                         \
		' $(HELP_MAKEFILE);
	@printf "\\n"

# Print the version
version:

	@date -r $(HELP_MAKEFILE) +"%d/%m/%Y %H:%M:%S"

###
## Docker
###

.PHONY: list-available-docker-ce install-docker-ce \
		require-docker print-docker-version \
		restart-docker-service print-docker-service-status follow-docker-service-logs \
		kill-docker-daemon

YUM := $(shell command -v yum 2> /dev/null)
APT-GET := $(shell command -v apt-get 2> /dev/null)
APT-CACHE := $(shell command -v apt-cache 2> /dev/null)

# List the available Docker CE packages
list-available-docker-ce:

	@printf "$(STYLE_TITLE)List available Docker CE versions$(STYLE_RESET)\\n"
ifdef YUM
	@yum list docker-ce --showduplicates | sort -r | grep "^docker-ce." | awk '{ print "docker-ce-" $$2 "\t" $$3 }'
endif
ifdef APT-CACHE
	@sudo apt-get update -qq
	@apt-cache madison docker-ce
endif

# Install Docker CE
install-docker-ce:

	@printf "$(STYLE_TITLE)Install dependencies$(STYLE_RESET)\\n"
ifdef YUM
	@sudo yum install -y yum-utils device-mapper-persistent-data lvm2
endif
ifdef APT-GET
	@sudo apt-get update -qq && sudo apt-get install --yes apt-transport-https ca-certificates curl software-properties-common
endif

	@printf "$(STYLE_TITLE)Add Docker's official GPG key$(STYLE_RESET)\\n"
ifdef APT-GET
	@curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
endif

	@printf "$(STYLE_TITLE)Add repository for Docker CE$(STYLE_RESET)\\n"
ifdef YUM
	@sudo yum-config-manager --add-repo ${CENTOS_DOCKER_REPOSITORY}
endif
ifdef APT-GET
	@sudo add-apt-repository "deb [arch=amd64] ${UBUNTU_DOCKER_REPOSITORY} $$(lsb_release -cs) stable edge"
endif

	@printf "$(STYLE_TITLE)Install Docker CE$(STYLE_RESET)\\n"
ifdef YUM
	@sudo yum install -y ${CENTOS_DOCKER_VERSION}
endif
ifdef APT-GET
	@sudo apt-get update -qq && sudo apt-get install --yes docker-ce=${UBUNTU_DOCKER_VERSION}
endif

	@printf "$(STYLE_TITLE)Create docker group$(STYLE_RESET)\\n"
	@getent group docker || sudo groupadd --force docker

	@printf "$(STYLE_TITLE)Add current user to docker group$(STYLE_RESET)\\n"
	@sudo usermod --append --groups docker $$USER
	@sudo groups $$USER

	@printf "$(STYLE_TITLE)Enable the Docker service on boot$(STYLE_RESET)\\n"
	@sudo systemctl enable $(DOCKER_SERVICE_NAME)
	@sudo systemctl is-enabled $(DOCKER_SERVICE_NAME)

	@printf "$(STYLE_TITLE)Start the Docker service$(STYLE_RESET)\\n"
	@sudo systemctl start $(DOCKER_SERVICE_NAME)
	@sudo systemctl is-active $(DOCKER_SERVICE_NAME)

#. Check if Docker is available, exit if it is not
require-docker:

	@if ! test -x "$(DOCKER)"; then \
		printf "$(STYLE_ERROR)Docker is not available. Please install Docker.$(STYLE_RESET)\\n"; \
		exit 1; \
	fi;

# Print the Docker version
print-docker-version:

	@$(DOCKER) --version 2>/dev/null || printf "Docker not installed\\n"

# Restart the Docker service
restart-docker-service:

	@if test "found" != "$$(sudo systemctl status $(DOCKER_SERVICE_NAME) | grep --only-matching "not-found" || printf "found")"; then \
		exit 1; \
	fi;
	@printf "$(STYLE_TITLE)Restart the Docker service$(STYLE_RESET)\\n"
	@sudo systemctl reset-failed $(DOCKER_SERVICE_NAME).service
	@sudo systemctl restart $(DOCKER_SERVICE_NAME)
	@$(THIS_MAKE) --quiet print-docker-service-status

# Print the Docker service status
print-docker-service-status:

	@sudo systemctl status --full --no-pager $(DOCKER_SERVICE_NAME)

# Follow the Docker service logs
follow-docker-service-logs:

	@sudo journalctl --follow --unit $(DOCKER_SERVICE_NAME)

# Kill the Docker daemon
kill-docker-daemon:

	@export DOCKER_DAEMON_EXECUTABLE=$$(command -v dockerd); \
	if test ! -z "$${DOCKER_DAEMON_EXECUTABLE}"; then \
		if pgrep --full --list-full "^$${DOCKER_DAEMON_EXECUTABLE}" > /dev/null; then \
			printf "$(STYLE_TITLE)Kill $${DOCKER_DAEMON_EXECUTABLE}$(STYLE_RESET)\\n"; \
			sudo pgrep --full --list-full "^$${DOCKER_DAEMON_EXECUTABLE}"; \
			sudo pkill --full "^$${DOCKER_DAEMON_EXECUTABLE}"; \
		fi; \
	fi

###
## Swarm
###

.PHONY: init-swarm assure-swarm-init leave-swarm print-swarm-status

# Initialize this Docker instance as a Swarm node
init-swarm: require-docker

	@printf "$(STYLE_TITLE)Initialize Swarm$(STYLE_RESET)\\n"
	@$(DOCKER) swarm init --force-new-cluster --advertise-addr 127.0.0.1

#. Assure this Docker instance is initialized as a Swarm node
assure-swarm-init: require-docker

	@if ! $(DOCKER) node ls > /dev/null 2>&1; then \
		$(THIS_MAKE) --quiet init-swarm; \
	fi

# Leave the Swarm
leave-swarm: require-docker

	@if $(DOCKER) node ls > /dev/null 2>&1; then \
		printf "$(STYLE_TITLE)Leave Swarm$(STYLE_RESET)\\n"; \
		$(DOCKER) swarm leave --force; \
	fi

# Print the Swarm status
print-swarm-status: require-docker

	@if $(DOCKER) node ls > /dev/null 2>&1; then \
		printf "Docker swarm node %s %s\\n" \
		$$($(DOCKER) node ls | grep -v "^ID" | grep "Leader" | awk '{print $$3}') \
		$$($(DOCKER) node ls | grep -v "^ID" | grep "Leader" | awk '{print tolower($$4)}'); \
	fi

###
## Secrets
###

.PHONY: create-secret-% assure-secret-% remove-secret-% replace-secret-% \
		create-file-secret-% assure-file-secret-% remove-file-secret-% replace-file-secret-%

# Create secret "%"
create-secret-%: require-docker assure-swarm-init

	$(eval $@_SECRET := $(patsubst create-secret-%,%,$@))
	@printf "$(STYLE_TITLE)Create secret \"$($@_SECRET)\" $(STYLE_RESET)\\n"
	@if $(DOCKER) secret ls | grep -q -w "$($@_SECRET)"; then \
		printf "Could not create secret, secret \"$($@_SECRET)\" already present\\n"; \
	elif test -n "$$(sed -n -E "s/^$($@_SECRET)[ ]*\=[ ]*\"?([^\"$$]+)\"?$$/\1/p" ".env" 2>/dev/null)"; then \
		sed -n -E "s/^$($@_SECRET) {0,}= {0,}\"{0,}([^\"$$]+)\"{0,}$$/\1/p" ".env" | $(DOCKER) secret create $($@_SECRET) - > /dev/null; \
		printf "Secret \"$($@_SECRET)\" added from .env file\\n"; \
	else \
		printf "$($@_SECRET): " && read -r -s $($@_SECRET) && printf "$${$($@_SECRET)}" | $(DOCKER) secret create $($@_SECRET) - > /dev/null; \
		printf "\\nSecret \"$($@_SECRET)\" added from input\\n"; \
	fi

#. Make sure that secret "%" is available
assure-secret-%: require-docker assure-swarm-init

	$(eval $@_SECRET := $(patsubst assure-secret-%,%,$@))
	@if ! $(DOCKER) secret ls | grep -q -w "$($@_SECRET)"; then \
		$(THIS_MAKE) --quiet create-secret-$($@_SECRET); \
	fi

# Remove secret "%"
remove-secret-%: require-docker assure-swarm-init

	$(eval $@_SECRET := $(patsubst remove-secret-%,%,$@))
	@if $(DOCKER) secret ls | grep -q -w "$($@_SECRET)"; then \
		$(DOCKER) secret rm $($@_SECRET) > /dev/null; \
		printf "Secret $($@_SECRET) removed\\n"; \
	fi

# Replace secret "%"
replace-secret-%: remove-secret-% create-secret-%

# Create file secret "%"
create-file-secret-%: require-docker assure-swarm-init

	$(eval $@_SECRET := $(patsubst create-file-secret-%,%,$@))
	@printf "$(STYLE_TITLE)Create secret \"$($@_SECRET)\" $(STYLE_RESET)\\n"
	@set -e; \
		if $(DOCKER) secret ls | grep -q -w "$($@_SECRET)"; then \
			printf "Could not create secret, secret \"$($@_SECRET)\" already present\\n"; \
		elif test -n "$$(sed -n -E "s/^$($@_SECRET)[ ]*\=[ ]*\"?([^\"$$]+)\"?$$/\1/p" ".env" 2>/dev/null)"; then \
			$(DOCKER) secret create "$($@_SECRET)" "$$(sed -n -E "s/^$($@_SECRET)[ ]*=[ ]*\"*([^\"$$]+)\"*$$/\1/p" ".env")" > /dev/null; \
			printf "Secret \"$($@_SECRET)\" added from .env file\\n"; \
		else \
			printf "$($@_SECRET) path: " && read -r $($@_SECRET) && $(DOCKER) secret create $($@_SECRET) "$${$($@_SECRET)}" > /dev/null; \
			printf "Secret \"$($@_SECRET)\" added from input\\n"; \
		fi

#. Make sure that file secret "%" is available
assure-file-secret-%: require-docker assure-swarm-init

	$(eval $@_SECRET := $(patsubst assure-file-secret-%,%,$@))
	@if ! $(DOCKER) secret ls | grep -q -w "$($@_SECRET)"; then \
		$(THIS_MAKE) --quiet create-file-secret-$($@_SECRET); \
	fi

# Remove file secret "%"
remove-file-secret-%: require-docker assure-swarm-init

	$(eval $@_SECRET := $(patsubst remove-file-secret-%,%,$@))
	@if $(DOCKER) secret ls | grep -q -w "$($@_SECRET)"; then \
		$(DOCKER) secret rm $($@_SECRET) > /dev/null; \
		printf "Secret $($@_SECRET) removed\\n"; \
	fi

# Replace file secret "%"
replace-file-secret-%: remove-file-secret-% create-file-secret-%

###
##. Stack
###

.PHONY: wait-for-stack-%-to-be-deployed wait-for-stack-%-to-be-removed wait-for-stack-%-volumes-to-be-removed \
		deploy-stack-% remove-stack-% clear-stack-% require-service-% wait-for-service-%

CLEAR_LAST_TWO_LINES=\\n\033[2K\033[1A\033[2K\033[1A\033[2K

#. Wait for stack to be deployed # TODO add SIGTERM trap in waiting scripts (to be able to cancel through vagrant ctrl+c)
wait-for-stack-%-to-be-deployed:

	$(eval $@_STACK_NAME := $(patsubst wait-for-stack-%-to-be-deployed,%,$@))
	@printf "$(STYLE_DIM)Waiting for services to be deployed$(STYLE_RESET)\\n"
	@until test -z "$$($(DOCKER) stack ps --filter desired-state=ready --filter desired-state=preparing --quiet $($@_STACK_NAME) 2>/dev/null)"; do \
		if test -t 1; then \
			printf "${CLEAR_LAST_TWO_LINES}$(STYLE_DIM)Waiting for services to be updated$(STYLE_RESET)\\n$(STYLE_DIM)%s$(STYLE_RESET)" \
				"$$($(DOCKER) inspect --format "{{.Spec.Name}}" $$($(DOCKER) inspect --format "{{.ServiceID}}" $$($(DOCKER) stack ps --filter desired-state=ready --filter desired-state=preparing --quiet $($@_STACK_NAME))) | sort | awk '{printf("%s, ",$$0)}' | sed 's/, *$$//')"; \
		fi; \
		sleep 1; \
	done;
	@until test -z "$$($(DOCKER) stack services $($@_STACK_NAME) | grep "\b0/[0-9]")"; do \
		if test -t 1; then \
			printf "${CLEAR_LAST_TWO_LINES}$(STYLE_DIM)Waiting for services to be deployed$(STYLE_RESET)\\n$(STYLE_DIM)%s$(STYLE_RESET)" \
				"$$($(DOCKER) stack services $($@_STACK_NAME) | grep "\b0/[0-9]" | awk '{print $$2}' | sort | awk '{printf("%s, ",$$0)}' | sed 's/, *$$//')"; \
		fi; \
		sleep 1; \
	done;
	@if test -t 1; then printf "${CLEAR_LAST_TWO_LINES}"; fi

#. Wait for stack to be removed # TODO add SIGTERM trap in waiting scripts (to be able to cancel through vagrant ctrl+c)
wait-for-stack-%-to-be-removed:

	$(eval $@_STACK_NAME := $(patsubst wait-for-stack-%-to-be-removed,%,$@))
	@printf "$(STYLE_DIM)Waiting for services to be removed$(STYLE_RESET)\\n"
	@until test -z "$$($(DOCKER) service ls --filter label=com.docker.stack.namespace=$($@_STACK_NAME) --quiet)"; do \
		if test -t 1; then \
			printf "${CLEAR_LAST_TWO_LINES}$(STYLE_DIM)Waiting for services to be removed$(STYLE_RESET)\\n$(STYLE_DIM)%s$(STYLE_RESET)" \
				"$$($(DOCKER) service ls --filter label=com.docker.stack.namespace=$($@_STACK_NAME) | grep -v "^ID" | awk '{print $$2}' | sort | awk '{printf("%s, ",$$0)}' | sed 's/, *$$//')"; \
		fi; \
		$(DOCKER) service rm $$($(DOCKER) service ls --filter label=com.docker.stack.namespace=$($@_STACK_NAME) --quiet) > /dev/null 2>&1; \
		sleep 1; \
	done;
	@if test -t 1; then printf "${CLEAR_LAST_TWO_LINES}"; fi
	@printf "$(STYLE_DIM)Waiting for containers to be removed$(STYLE_RESET)\\n"
	@until test -z "$$($(DOCKER) ps --filter label=com.docker.stack.namespace=$($@_STACK_NAME) --all --quiet)"; do \
		if test -t 1; then \
			printf "${CLEAR_LAST_TWO_LINES}$(STYLE_DIM)Waiting for containers to be removed$(STYLE_RESET)\\n$(STYLE_DIM)%s$(STYLE_RESET)" \
				"$$($(DOCKER) ps --filter label=com.docker.stack.namespace=$($@_STACK_NAME) | grep -v "^CONTAINER ID" | awk '{print $$1}' | sort | awk '{printf("%s, ",$$0)}' | sed 's/, *$$//')"; \
		fi; \
		$(DOCKER) rm $$($(DOCKER) ps --filter label=com.docker.stack.namespace=$($@_STACK_NAME) --all --quiet) > /dev/null 2>&1; \
		sleep 1; \
	done;
	@if test -t 1; then printf "${CLEAR_LAST_TWO_LINES}"; fi
	@printf "$(STYLE_DIM)Waiting for networks to be removed$(STYLE_RESET)\\n"
	@until test -z "$$($(DOCKER) network ls --filter label=com.docker.stack.namespace=$($@_STACK_NAME) --quiet)"; do \
		if test -t 1; then \
			printf "${CLEAR_LAST_TWO_LINES}$(STYLE_DIM)Waiting for networks to be removed$(STYLE_RESET)\\n$(STYLE_DIM)%s$(STYLE_RESET)" \
				"$$($(DOCKER) network ls --filter label=com.docker.stack.namespace=$($@_STACK_NAME) | grep -v "^NETWORK ID" | awk '{print $$2}' | sort | awk '{printf("%s, ",$$0)}' | sed 's/, *$$//')"; \
		fi; \
		$(DOCKER) network rm $$($(DOCKER) network ls --filter label=com.docker.stack.namespace=$($@_STACK_NAME) --quiet) > /dev/null 2>&1; \
		$(DOCKER) network prune -f > /dev/null 2>&1; \
		sleep 1; \
	done;
	@if test -t 1; then printf "${CLEAR_LAST_TWO_LINES}"; fi

#. Wait for stack volumes to be removed # TODO add SIGTERM trap in waiting scripts (to be able to cancel through vagrant ctrl+c)
wait-for-stack-%-volumes-to-be-removed:

	$(eval $@_STACK_NAME := $(patsubst wait-for-stack-%-volumes-to-be-removed,%,$@))
	@printf "$(STYLE_DIM)Waiting for volumes to be removed$(STYLE_RESET)\\n"
	@until test -z "$$($(DOCKER) volume ls --filter label=com.docker.stack.namespace=$($@_STACK_NAME) --quiet)"; do \
		if test -t 1; then \
			printf "${CLEAR_LAST_TWO_LINES}$(STYLE_DIM)Waiting for volumes to be removed$(STYLE_RESET)\\n$(STYLE_DIM)%s$(STYLE_RESET)" \
				"$$($(DOCKER) volume ls --filter label=com.docker.stack.namespace=$($@_STACK_NAME) | grep -v "^DRIVER" | awk '{print $$2}' | sort | awk '{printf("%s, ",$$0)}' | sed 's/, *$$//')"; \
		fi; \
		$(DOCKER) volume rm $$($(DOCKER) volume ls --filter label=com.docker.stack.namespace=$($@_STACK_NAME) --quiet) > /dev/null 2>&1; \
		sleep 1; \
	done;
	@if test -t 1; then printf "${CLEAR_LAST_TWO_LINES}"; fi

#. Deploy the stack "%"
deploy-stack-%: require-docker assure-swarm-init

	$(eval $@_STACK_NAME := $(patsubst deploy-stack-%,%,$@))
	@printf "$(STYLE_TITLE)Deploy $($@_STACK_NAME) stack$(STYLE_RESET)\\n"
	@printf "Flags: $(DOCKER_SWARM_DEPLOY_FLAGS)\\n"
	$(DOCKER) stack deploy $(DOCKER_SWARM_DEPLOY_FLAGS) $($@_STACK_NAME)
	$(THIS_MAKE) --quiet wait-for-stack-$($@_STACK_NAME)-to-be-deployed

#. Remove the stack "%"
remove-stack-%:

	$(eval $@_STACK_NAME := $(patsubst remove-stack-%,%,$@))
	@printf "$(STYLE_TITLE)Remove $($@_STACK_NAME) stack$(STYLE_RESET)\\n"
	@set -e; \
		retry() { \
			printf "$(STYLE_TITLE)Remove dangling containers, networks and images from $($@_STACK_NAME) stack$(STYLE_RESET)\\n"; \
			$(DOCKER) system prune --force --filter label=com.docker.stack.namespace=$($@_STACK_NAME) || true; \
			if test -n "$$(docker stack ls | grep "^$($@_STACK_NAME)\b")"; then \
				printf "$(STYLE_TITLE)Try removing $($@_STACK_NAME) stack again$(STYLE_RESET)\\n"; \
				$(DOCKER) stack rm $($@_STACK_NAME); \
				$(THIS_MAKE) --quiet wait-for-stack-$($@_STACK_NAME)-to-be-removed; \
			fi; \
		}; \
		trap 'test "$?" != "0" && retry' EXIT; \
		$(DOCKER) stack rm $($@_STACK_NAME); \
		$(THIS_MAKE) --quiet wait-for-stack-$($@_STACK_NAME)-to-be-removed

#. Remove the stack "%", including the volumes
clear-stack-%: remove-stack-%

	$(eval $@_STACK_NAME := $(patsubst clear-stack-%,%,$@))
	@if test -n "$$($(DOCKER) volume ls --filter label=com.docker.stack.namespace=$($@_STACK_NAME) --quiet)"; then \
		printf "$(STYLE_TITLE)Remove $($@_STACK_NAME) volumes$(STYLE_RESET)\\n"; \
		$(DOCKER) volume rm $$($(DOCKER) volume ls --filter label=com.docker.stack.namespace=$($@_STACK_NAME) --quiet); \
		$(THIS_MAKE) --quiet wait-for-stack-$($@_STACK_NAME)-volumes-to-be-removed; \
	fi;

#. Check if service "%" is available, exit if it is not
require-service-%:

	$(eval $@_SERVICE := $(patsubst require-service-%,%,$@))
	@if test -z "$$($(DOCKER) service ps --no-trunc --quiet $($@_SERVICE))"; then \
		printf "$(STYLE_ERROR)Service $($@_SERVICE) is not available. Please start the service.$(STYLE_RESET)\\n"; \
		exit 1; \
	fi

#. Wait for service "%"
wait-for-service-%: require-docker require-service-%

	$(eval $@_SERVICE := $(patsubst wait-for-service-%,%,$@))
	@until test -n "$$($(DOCKER) service ps --no-trunc --filter desired-state=running --quiet $($@_SERVICE))"; do \
		printf "Waiting for $($@_SERVICE)...\\n"; \
		sleep 1; \
	done;

###
## Configuration
###

.PHONY: assure-bash-image assure-jq-image \
		assure-valid-docker-config-file empty-docker-config-file print-docker-config-file \
		set-default-docker-ulimit-for-% check-docker-ulimit-for-% print-docker-ulimit-for-% \
		set-default-vm-value-for-% check-vm-value-for-% print-vm-value-for-%

as-label = the address space limit / size of virtual memory
fsize-label = the maximum filesize
memlock-label = the maximum locked-in-memory address space
nofile-label = the maximum number of open file descriptors
nproc-label = the maximum number of processes
max_map_count-label = the maximum number of memory map areas a process may have

as-ulimit-parameter = v
fsize-ulimit-parameter = f
memlock-ulimit-parameter = l
nofile-ulimit-parameter = n
nproc-ulimit-parameter = u

#. Assure that a bash image is present
assure-bash-image:

	@if test "" = "$$($(DOCKER) images --quiet $(BASH_IMAGE) 2> /dev/null)"; then \
		printf "$(STYLE_TITLE)Pull $(BASH_IMAGE) image$(STYLE_RESET)\\n"; \
		$(DOCKER) pull $(BASH_IMAGE); \
	fi

#. Assure that a jq image is present
assure-jq-image:

	@if test "" = "$$($(DOCKER) images --quiet $(JQ_IMAGE) 2> /dev/null)"; then \
		printf "$(STYLE_TITLE)Pull $(JQ_IMAGE) image$(STYLE_RESET)\\n"; \
		$(DOCKER) pull $(JQ_IMAGE); \
	fi

# Assure that a valid Docker daemon config file present
assure-valid-docker-config-file: assure-jq-image

	@if sudo test ! -f "$(DOCKER_CONFIG_FILE)"; then printf "{}\\n" | sudo tee "$(DOCKER_CONFIG_FILE)" > /dev/null; fi
	@if sudo test ! -s "$(DOCKER_CONFIG_FILE)"; then printf "{}\\n" | sudo tee "$(DOCKER_CONFIG_FILE)" > /dev/null; fi
	@$(JQ) -n --exit-status '$$config' --argjson config "$$(sudo cat "$(DOCKER_CONFIG_FILE)")" >/dev/null 2>&1 \
		|| (printf "Found incorrect json in $(DOCKER_CONFIG_FILE)\\n" && sudo cat "$(DOCKER_CONFIG_FILE)" && exit 1)

# Empty the Docker daemon config file                                           #! Use with caution
empty-docker-config-file:

	@printf "{}\\n" | sudo tee "$(DOCKER_CONFIG_FILE)" > /dev/null

# Print the Docker daemon config file
print-docker-config-file: assure-jq-image

	@$(JQ) -n -C '$$config' --argjson config "$$(sudo cat "$(DOCKER_CONFIG_FILE)")"

#. Set the ulimit "%" ("%" should be <NAME>-to-<VALUE>)          [as, fsize, memlock, nofile, nproc]
set-default-docker-ulimit-for-%: assure-jq-image assure-valid-docker-config-file

	$(eval $@_NAME := $(firstword $(subst -to-, ,$(patsubst set-default-docker-ulimit-for-%,%,$@))))
	$(eval $@_VALUE := $(lastword $(subst -to-, ,$(patsubst set-default-docker-ulimit-for-%,%,$@))))
	@if test "$($@_NAME)" = "$($@_VALUE)"; then \
		printf -- "$(STYLE_ERROR)No value given for $($@_NAME) $(STYLE_RESET)\\n"; \
		exit 1; \
	fi
	@printf "$(STYLE_TITLE)Assure that $($($@_NAME)-label) is set to $($@_VALUE) $(STYLE_RESET)\\n"
	@$(JQ) -M -n '$$config * $$limit' --argjson config "$$(sudo cat "$(DOCKER_CONFIG_FILE)")" --argjson limit '{"default-ulimits":{"$($@_NAME)":{"Name":"$($@_NAME)","Soft":$($@_VALUE),"Hard":$($@_VALUE)}}}' \
		| sudo tee /etc/docker/daemon-new.json > /dev/null && sudo cp /etc/docker/daemon-new.json "$(DOCKER_CONFIG_FILE)" && sudo rm -rf /etc/docker/daemon-new.json
	@$(JQ) -n -C '$$config."default-ulimits"."$($@_NAME)"' --argjson config "$$(sudo cat "$(DOCKER_CONFIG_FILE)")"

# Check the ulimit "%" ("%" should be <NAME>-at-least-<VALUE>)    [as, fsize, memlock, nofile, nproc]
check-docker-ulimit-for-%: assure-bash-image

	$(eval $@_NAME := $(firstword $(subst -at-least-, ,$(patsubst check-docker-ulimit-for-%,%,$@))))
	$(eval $@_VALUE := $(lastword $(subst -at-least-, ,$(patsubst check-docker-ulimit-for-%,%,$@))))
	@if test "$($@_NAME)" = "$($@_VALUE)"; then \
		printf -- "$(STYLE_ERROR)No value given for $($@_NAME) $(STYLE_RESET)\\n"; \
		exit 1; \
	fi
	@printf "$(STYLE_TITLE)Check that $($($@_NAME)-label) is set to at least $($@_VALUE)$(STYLE_RESET)\\n"
	@VALUE=$$($(BASH) -c 'printf "%s" $$(ulimit -S$($($@_NAME)-ulimit-parameter)) | sed -e "s/unlimited/-1/g"'); \
		if test "$${VALUE}" -eq "-1" -o "$${VALUE}" -ge "$($@_VALUE)"; then printf -- "Soft limit: $${VALUE}\\n"; else printf -- "$(STYLE_ERROR)Default soft limit for ulimit $($@_NAME) set to %s, should be %s.$(STYLE_RESET)\\n\\n" "$${VALUE}" "$($@_VALUE)"; exit 1; fi;
	@VALUE=$$($(BASH) -c 'printf "%s" $$(ulimit -H$($($@_NAME)-ulimit-parameter)) | sed -e "s/unlimited/-1/g"'); \
		if test "$${VALUE}" -eq "-1" -o "$${VALUE}" -ge "$($@_VALUE)"; then printf -- "Hard limit: $${VALUE}\\n"; else printf -- "$(STYLE_ERROR)Default hard limit for ulimit $($@_NAME) set to %s, should be %s.$(STYLE_RESET)\\n\\n" "$${VALUE}" "$($@_VALUE)"; exit 1; fi;

# Print the ulimit "%"                                          [as, fsize, memlock, nofile, nproc]
print-docker-ulimit-for-%: assure-bash-image

	$(eval $@_NAME := $(patsubst print-docker-ulimit-for-%,%,$@))
	@printf "default-ulimits.%s: %d:%d\\n" \
			"$($@_NAME)" \
			"$$($(BASH) -c 'printf "%d" "$$(ulimit -S$($($@_NAME)-ulimit-parameter) | sed -e "s/unlimited/-1/g")"')" \
			"$$($(BASH) -c 'printf "%d" "$$(ulimit -H$($($@_NAME)-ulimit-parameter) | sed -e "s/unlimited/-1/g")"')"

#. Set the vm value "%" ("%" should be <NAME>-to-<VALUE>)                           [max_map_count]
set-default-vm-value-for-%:

	$(eval $@_NAME := $(firstword $(subst -to-, ,$(patsubst set-default-vm-value-for-%,%,$@))))
	$(eval $@_VALUE := $(lastword $(subst -to-, ,$(patsubst set-default-vm-value-for-%,%,$@))))
	@if test "$($@_NAME)" = "$($@_VALUE)"; then \
		printf -- "$(STYLE_ERROR)No value given for $($@_NAME) $(STYLE_RESET)\\n"; \
		exit 1; \
	fi
	@printf "$(STYLE_TITLE)Assure that $($($@_NAME)-label) is set to $($@_VALUE) $(STYLE_RESET)\\n"
	@if ! sudo grep --fixed-strings --line-regexp --quiet "vm.$($@_NAME) = $($@_VALUE)" /etc/sysctl.conf; then \
		sudo sed --quiet --in-place --expression "/^vm.$($@_NAME) = /!p" /etc/sysctl.conf; \
		printf "vm.$($@_NAME) = $($@_VALUE)\\n" | sudo tee --append /etc/sysctl.conf > /dev/null; \
	fi
	@sudo sysctl -p | grep "vm.$($@_NAME)"

# Check the vm value for "%" ("%" should be <NAME>-at-least-<VALUE>)                [max_map_count]
check-vm-value-for-%: assure-bash-image

	$(eval $@_NAME := $(firstword $(subst -at-least-, ,$(patsubst check-vm-value-for-%,%,$@))))
	$(eval $@_VALUE := $(lastword $(subst -at-least-, ,$(patsubst check-vm-value-for-%,%,$@))))
	@if test "$($@_NAME)" = "$($@_VALUE)"; then \
		printf -- "$(STYLE_ERROR)No value given for $($@_NAME) $(STYLE_RESET)\\n"; \
		exit 1; \
	fi
	@printf "$(STYLE_TITLE)Check that $($($@_NAME)-label) is set to at least $($@_VALUE) $(STYLE_RESET)\\n"
	@VALUE=$$($(BASH) -c 'sysctl -n vm.$($@_NAME)'); \
		if test "$${VALUE}" -eq "-1" -o "$${VALUE}" -ge "$($@_VALUE)"; then printf -- "$${VALUE}\\n"; else printf -- "$(STYLE_ERROR)Default value for vm.$($@_NAME) set to %s, should be %s.$(STYLE_RESET)\\n\\n" "$${VALUE}" "$($@_VALUE)"; exit 1; fi;

# Print the vm value for "%"                                                        [max_map_count]
print-vm-value-for-%: assure-bash-image

	$(eval $@_NAME := $(patsubst print-vm-value-for-%,%,$@))
	@VALUE=$$($(BASH) -c 'sysctl -n vm.$($@_NAME)'); \
		printf "vm.$($@_NAME): %s\\n" "$${VALUE}"

###
## ELK requirements
###

.PHONY: prepare-for-elk check-elk-parameters print-elk-parameters

# Prepare the system for running an ELK stack
prepare-for-elk: \
	require-docker \
	set-default-docker-ulimit-for-nofile-to-65536 \
	set-default-docker-ulimit-for-memlock-to--1 \
	set-default-docker-ulimit-for-nproc-to-4096 \
	set-default-docker-ulimit-for-as-to--1 \
	set-default-docker-ulimit-for-fsize-to--1 \
	set-default-vm-value-for-max_map_count-to-262144 \
	restart-docker-service

# Check the parameters that influence the ELK stack
check-elk-parameters: assure-bash-image

	@set -e; \
		service_name=$@-$(RUN_UID); \
		cleanup() { \
			code=$$?; \
			docker kill $${service_name} > /dev/null || true; \
			exit $$code; \
		}; \
		trap 'cleanup' INT TERM EXIT; \
		$(DOCKER) run --rm --detach --name $${service_name} $(BASH_IMAGE) -c "tail -f /dev/null" > /dev/null; \
		export BASH="$(DOCKER) exec $${service_name} bash" && \
		$(THIS_MAKE) --quiet \
			check-docker-ulimit-for-nofile-at-least-65536 \
			check-docker-ulimit-for-memlock-at-least--1 \
			check-docker-ulimit-for-nproc-at-least-4096 \
			check-docker-ulimit-for-as-at-least--1 \
			check-docker-ulimit-for-fsize-at-least--1 \
			check-vm-value-for-max_map_count-at-least-262144

# Print the parameters that influence the ELK stack
print-elk-parameters: assure-bash-image

	@set -e; \
		service_name=$@-$(RUN_UID); \
		cleanup() { \
			code=$$?; \
			docker kill $${service_name} > /dev/null || true; \
			exit $$code; \
		}; \
		trap 'cleanup' INT TERM EXIT; \
		$(DOCKER) run --rm --detach --name $${service_name} $(BASH_IMAGE) -c "tail -f /dev/null" > /dev/null; \
		export BASH="$(DOCKER) exec $${service_name} bash" && \
		$(THIS_MAKE) --quiet \
			print-docker-ulimit-for-nofile \
			print-docker-ulimit-for-memlock \
			print-docker-ulimit-for-nproc \
			print-docker-ulimit-for-as \
			print-docker-ulimit-for-fsize \
			print-vm-value-for-max_map_count

###
## Composer & Symfony
###

.PHONY: require-php \
		install-composer-on-% install-composer install-project-dependencies-on-% install-project-dependencies \
		run-warm-up-scripts-on-% run-warm-up-scripts run-cool-down-scripts-on-% run-cool-down-scripts \
		reset-project-persistent-storage-on-% reset-project-persistent-storage

ifneq ($(DOCKER),)

# Install composer on service "%"
install-composer-on-%: require-docker wait-for-service-%

	$(eval $@_SERVICE := $(patsubst install-composer-on-%,%,$@))
	@$(DOCKER) exec $$($(DOCKER) ps --filter name=$$($(DOCKER) service ps $($@_SERVICE) --no-trunc | awk '{print $$2 "." $$1}' | grep "^$($@_SERVICE)") --quiet --no-trunc) sh -c "make --quiet install-composer"

# Install project dependencies on service "%"
install-project-dependencies-on-%: require-docker wait-for-service-%

	$(eval $@_SERVICE := $(patsubst install-project-dependencies-on-%,%,$@))
	@$(DOCKER) exec $$($(DOCKER) ps --filter name=$$($(DOCKER) service ps $($@_SERVICE) --no-trunc | awk '{print $$2 "." $$1}' | grep "^$($@_SERVICE)") --quiet --no-trunc) sh -c "make --quiet install-project-dependencies"

# Run warm-up scripts on service "%"
run-warm-up-scripts-on-%: require-docker wait-for-service-%

	$(eval $@_SERVICE := $(patsubst run-warm-up-scripts-on-%,%,$@))
	@$(DOCKER) exec $$($(DOCKER) ps --filter name=$$($(DOCKER) service ps $($@_SERVICE) --no-trunc | awk '{print $$2 "." $$1}' | grep "^$($@_SERVICE)") --quiet --no-trunc) sh -c "make --quiet run-warm-up-scripts"

# Run cool-down scripts on service "%"
run-cool-down-scripts-on-%: require-docker wait-for-service-%

	$(eval $@_SERVICE := $(patsubst run-cool-down-scripts-on-%,%,$@))
	@$(DOCKER) exec $$($(DOCKER) ps --filter name=$$($(DOCKER) service ps $($@_SERVICE) --no-trunc | awk '{print $$2 "." $$1}' | grep "^$($@_SERVICE)") --quiet --no-trunc) sh -c "make --quiet run-cool-down-scripts"

# Reset project persistence on service "%"                         #! resets all persistent storage
reset-project-persistent-storage-on-%: require-docker wait-for-service-%

	$(eval $@_SERVICE := $(patsubst reset-project-persistent-storage-on-%,%,$@))
	@$(DOCKER) exec $$($(DOCKER) ps --filter name=$$($(DOCKER) service ps $($@_SERVICE) --no-trunc | awk '{print $$2 "." $$1}' | grep "^$($@_SERVICE)") --quiet --no-trunc) sh -c "make --quiet reset-project-persistent-storage"

else

#. Check if PHP is available, exit if it is not
require-php:

	@if ! test -x "$(PHP)"; then \
		echo "$(STYLE_ERROR)PHP is not available. Please install PHP.$(STYLE_RESET)"; \
		exit 1; \
	fi;

# Install composer
install-composer: require-php

	@printf "$(STYLE_TITLE)Install composer$(STYLE_RESET)\\n"
	@if test ! -d bin; then \
		mkdir -p bin; \
	fi
	@$(PHP_RUN_COMMAND) -r "readfile('https://getcomposer.org/installer');" > /tmp/setup.php
	@$(PHP_RUN_COMMAND) -r "readfile('https://composer.github.io/installer.sig');" > /tmp/setup.sig
	@$(PHP) -r " \
		if (hash_file('SHA384', '/tmp/setup.php') !== trim(file_get_contents('/tmp/setup.sig'))) { \
			echo 'Installer corrupt'; unlink('setup.php'); \
		} \
		echo PHP_EOL; \
	"
	@$(PHP) /tmp/setup.php --no-ansi --install-dir=bin --filename=composer
	@$(PHP) -r "unlink('/tmp/setup.php');"
	@$(PHP) -r "unlink('/tmp/setup.sig');"
	@$(PHP_RUN_COMMAND) bin/composer --version

# Install project dependencies
install-project-dependencies: require-php

	@if test ! -d bin; then \
		mkdir -p bin; \
	fi
	if test ! -x bin/composer; then \
		$(THIS_MAKE) install-composer; \
	fi

	export COMPOSER_CACHE_DIR=.composer/
	# If you see errors about composer trying to install collection-utilities via git over ssh,
	# make sure that the personal access token has access to the gitlab API.
	$(PHP_RUN_COMMAND) bin/composer config -g gitlab-token.gitlab.com $(BUILD_USER_ACCESS_TOKEN)

	export NODEV="";
	if test "prod" = "$${APP_ENV}"; then \
		export NODEV="--no-dev"; \
	fi

	$(PHP_RUN_COMMAND) bin/composer dump-autoload --no-scripts $${NODEV} > /dev/null 2>&1 || (printf "$(STYLE_TITLE)Removing vendor directory before rebuilding$(STYLE_RESET)\\n" && rm -rf vendor/*)
	@printf "$(STYLE_TITLE)Install dependencies$(STYLE_RESET)\\n"
	$(PHP_RUN_COMMAND) bin/composer install $${NODEV} --no-ansi --no-interaction --no-progress --optimize-autoloader

	@printf "$(STYLE_TITLE)List installed direct dependencies$(STYLE_RESET)\\n"
	# composer show just shows information. It does not matter if it fails.
	# (putting a - before the command below, instructs make to continue in case of failure)
	-$(PHP_RUN_COMMAND) bin/composer show --latest --direct

# Install lowest project dependencies
install-lowest-project-dependencies: require-php

	@if test ! -d bin; then \
		mkdir -p bin; \
	fi
	if test ! -x bin/composer; then \
		$(THIS_MAKE) install-composer; \
	fi

	export COMPOSER_CACHE_DIR=.composer/
	# If you see errors about composer trying to install collection-utilities via git over ssh,
	# make sure that the personal access token has access to the gitlab API.
	$(PHP_RUN_COMMAND) bin/composer config -g gitlab-token.gitlab.com $(BUILD_USER_ACCESS_TOKEN)

	export NODEV="";
	if test "prod" = "$${APP_ENV}"; then \
		export NODEV="--no-dev"; \
	fi

	$(PHP_RUN_COMMAND) bin/composer dump-autoload --no-scripts $${NODEV} > /dev/null 2>&1 || (printf "$(STYLE_TITLE)Removing vendor directory before rebuilding$(STYLE_RESET)\\n" && rm -rf vendor/*)
	@printf "$(STYLE_TITLE)Install dependencies$(STYLE_RESET)\\n"
	$(PHP_RUN_COMMAND) bin/composer update --prefer-lowest $${NODEV} --no-ansi --no-interaction --no-progress --optimize-autoloader

	@printf "$(STYLE_TITLE)List installed direct dependencies$(STYLE_RESET)\\n"
	# composer show just shows information. It does not matter if it fails.
	# (putting a - before the command below, instructs make to continue in case of failure)
	-$(PHP_RUN_COMMAND) bin/composer show --latest --direct

# Run warm-up scripts
run-warm-up-scripts: require-php

# Removed the warm-up scripts, since this is no web app.

# Run cool-down scripts
run-cool-down-scripts: require-php

	@printf "$(STYLE_TITLE)Destroy project persistence$(STYLE_RESET)\\n"
	-@$(PHP_RUN_COMMAND) bin/console persistence:destroy

# Reset project persistence                                        #! resets all persistent storage
reset-project-persistent-storage: require-php

	@printf "$(STYLE_TITLE)Reset project persistence$(STYLE_RESET)\\n"
	@$(PHP_RUN_COMMAND) bin/console reset

	@printf "$(STYLE_TITLE)Create project admin$(STYLE_RESET)\\n"
	@$(PHP_RUN_COMMAND) bin/console admin:init

endif

###
## Application
###

#. Copy application files from the GitLab build container to the PHP container of the % stack
copy-application-files-from-gitlab-ci-to-stack-%: wait-for-service-%_php

	$(eval $@_STACK_NAME := $(patsubst copy-application-files-from-gitlab-ci-to-stack-%,%,$@))
	@printf "$(STYLE_TITLE)Copy application files from the GitLab build container to the PHP container of the $($@_STACK_NAME) stack$(STYLE_RESET)\\n"
	@$(DOCKER) cp $${CI_PROJECT_DIR}/. $$($(DOCKER) ps --filter name=$$($(DOCKER) service ps $($@_STACK_NAME)_php --no-trunc | awk '{print $$2 "." $$1}' | grep "^$($@_STACK_NAME)_php") --quiet --no-trunc):/var/www/html/
	@$(DOCKER) exec $$($(DOCKER) ps --filter name=$$($(DOCKER) service ps $($@_STACK_NAME)_php --no-trunc | awk '{print $$2 "." $$1}' | grep "^$($@_STACK_NAME)_php") --quiet --no-trunc) ls -1A

# Deploy the stack, install application dependencies and warm up
deploy-application-%:

	$(eval $@_STACK_NAME := $(patsubst deploy-application-%,%,$@))
	@printf "$(STYLE_TITLE)Report Docker version$(STYLE_RESET)\\n"
	@$(THIS_MAKE) --quiet print-docker-version
	@$(THIS_MAKE) --quiet check-elk-parameters
	@$(THIS_MAKE) --quiet deploy-stack-$($@_STACK_NAME)
	@if test -n "$${CI}"; then \
		$(THIS_MAKE) --quiet copy-application-files-from-gitlab-ci-to-stack-$($@_STACK_NAME); \
	fi
	@$(THIS_MAKE) --quiet install-project-dependencies-on-$($@_STACK_NAME)_php
	@$(THIS_MAKE) --quiet run-warm-up-scripts-on-$($@_STACK_NAME)_php

# Cool down the application and remove the stack
remove-application-%:

	$(eval $@_STACK_NAME := $(patsubst remove-application-%,%,$@))
	@if test -n "$$($(DOCKER) stack ls | grep "^$($@_STACK_NAME)\b")"; then \
		if test -n "$$($(DOCKER) service ps --filter desired-state=running --quiet $($@_STACK_NAME)_php 2> /dev/null)"; then \
			$(THIS_MAKE) --quiet run-cool-down-scripts-on-$($@_STACK_NAME)_php || printf "$(STYLE_ERROR)Could not run the cool down scripts first.$(STYLE_RESET)\\n"; \
		else \
			printf "$(STYLE_ERROR)Could not find the $($@_STACK_NAME)_php service to run the cool down scripts first.$(STYLE_RESET)\\n"; \
		fi; \
	fi
	@$(THIS_MAKE) --quiet remove-stack-$($@_STACK_NAME)

# Cool down the application and clear the stack
clear-application-%:

	$(eval $@_STACK_NAME := $(patsubst clear-application-%,%,$@))
	@if test -n "$$($(DOCKER) stack ls | grep "^$($@_STACK_NAME)\b")"; then \
		if test -n "$$($(DOCKER) service ps --filter desired-state=running --quiet $($@_STACK_NAME)_php 2> /dev/null)"; then \
			$(THIS_MAKE) --quiet run-cool-down-scripts-on-$($@_STACK_NAME)_php || printf "$(STYLE_ERROR)Could not run the cool down scripts first.$(STYLE_RESET)\\n"; \
		else \
			printf "$(STYLE_ERROR)Could not find the $($@_STACK_NAME)_php service to run the cool down scripts first.$(STYLE_RESET)\\n"; \
		fi; \
	fi
	@$(THIS_MAKE) --quiet clear-stack-$($@_STACK_NAME)

###
## Tests
###

.PHONY: run-unit-tests run-functional-tests run-acceptance-tests tests

# Run unit tests on stack "%"
run-unit-tests-on-%:

	$(eval $@_STACK_NAME := $(patsubst run-unit-tests-on-%,%,$@))
	@printf "\\n$(STYLE_TITLE)Codeception Unit Tests\\n======================\\n$(STYLE_RESET)\\n"
	@$(DOCKER) exec $$($(DOCKER) ps --filter name=$$($(DOCKER) service ps $($@_STACK_NAME)_php --no-trunc | awk '{print $$2 "." $$1}' | grep "^$($@_STACK_NAME)_php") --quiet --no-trunc) \
		bash -c "export APP_ENV=test && php vendor/bin/phpunit tests"

# Run the unit, functional and acceptance tests on stack "%"
run-tests-on-%:

	$(eval $@_STACK_NAME := $(patsubst run-tests-on-%,%,$@))
	@$(THIS_MAKE) --quiet run-unit-tests-on-$($@_STACK_NAME)

# Run the unit, functional and acceptance tests on an isolated stack
run-tests-isolated: require-docker

	$(eval $@_STACK_NAME := tests-$(RUN_UID))
	@set -e; \
		if test -z "$${DOCKER_SWARM_DEPLOY_FLAGS}"; then export DOCKER_SWARM_DEPLOY_FLAGS="$(DOCKER_SWARM_DEPLOY_FLAGS)"; fi; \
		cleanup() { \
			code=$$?; \
			printf "\\n\\n"; \
			$(THIS_MAKE) --quiet clear-application-$($@_STACK_NAME); \
			printf "$(STYLE_TITLE)Kill the fallback cleanup container$(STYLE_RESET)\\n"; \
			$(DOCKER) kill $($@_STACK_NAME)-cleanup; \
			exit $$code; \
		}; \
		trap 'cleanup' INT TERM EXIT; \
		printf "$(STYLE_TITLE)Start a fallback cleanup container$(STYLE_RESET)\\n"; \
		$(DOCKER) run --name $($@_STACK_NAME)-cleanup --rm --detach --volume $(DOCKER_SOCKET):$(DOCKER_SOCKET) docker:latest sh -c "sleep 1h; docker stack rm \"$($@_STACK_NAME)\""; \
		$(THIS_MAKE) --quiet deploy-application-$($@_STACK_NAME); \
		$(THIS_MAKE) --quiet run-tests-on-$($@_STACK_NAME)

clean-tests-isolated: require-docker

	$(eval $@_STACK_NAME := tests-$(RUN_UID))
	@set -e; \
		if test -z "$${DOCKER_SWARM_DEPLOY_FLAGS}"; then export DOCKER_SWARM_DEPLOY_FLAGS="$(DOCKER_SWARM_DEPLOY_FLAGS)"; fi; \
		$(THIS_MAKE) --quiet clear-application-$($@_STACK_NAME)


###
## Quality Assurance
###

.PHONY: analysis review fix

# Run a selective set of QA tools for a quick analysis (in parallel)
analysis:

	@$(QA_MAKE) --quiet $@

# Run a comprehensive set of QA tools for a complete review (in parellel)
review:

	@$(QA_MAKE) --quiet $@

# Run a small set of QA tools that can fix things out-of-the-box               #! might change code
fix:

	@$(QA_MAKE) --quiet $@

###
## Main
###

.PHONY: start stop restart clear reset

# Start your EQA development environment
start: \
	require-docker \
	assure-swarm-init

	@set -e; \
		if test -z "$${DOCKER_SWARM_DEPLOY_FLAGS}"; then export DOCKER_SWARM_DEPLOY_FLAGS="$(DOCKER_SWARM_DEPLOY_FLAGS)"; fi; \
		export DOCKER_SWARM_DEPLOY_FLAGS="$${DOCKER_SWARM_DEPLOY_FLAGS} --compose-file swarm.dev.yml"; \
		$(THIS_MAKE) --quiet deploy-application-eqa

# Stop your EQA development environment
stop: remove-application-eqa

# Restart your EQA development environment
restart: stop start

# Clear your EQA development environment
clear: clear-application-eqa

# Reset your EQA development environment
reset: clear start

ifneq ($(DOCKER),)

# Prepare for xdebug
host-for-linux:

	@$(DOCKER) exec $$($(DOCKER) ps --filter name=$$($(DOCKER) service ps eqa_php --no-trunc | awk '{print $$2 "." $$1}' | grep "^eqa_php") --quiet --no-trunc) sh -c "make --quiet $@"

else

#. Prepare for xdebug
host-for-linux:

	@apt-get -qq update 1> /dev/null
	@apt-get -qq install -y iproute2 1> /dev/null
	@printf "$$(ip route | awk '/default/ { print $$3 }')\\thost.docker.internal\\n" | tee -a /etc/hosts
	@apt-get -qq remove -y iproute2 1> /dev/null

endif

###
## Services
###

.PHONY: ctop portainer netdata ldapadmin

# Run Ctop - Real-time metrics for containers                                      https://ctop.sh/
ctop:

	@set -e; \
		if test -z "$$($(DOCKER) ps --quiet --filter="name=ctop")"; then \
			$(DOCKER) run --rm --interactive --tty --name ctop \
				--volume $(DOCKER_SOCKET):$(DOCKER_SOCKET):ro \
				quay.io/vektorlab/ctop:latest; \
		else \
			$(DOCKER) attach ctop; \
		fi

# Run portainer - Management UI for Docker                                    https://portainer.io/
portainer:

	@$(DOCKER) service create --name portainer \
		--publish "9000:9000" \
		--constraint 'node.role == manager' \
		--mount type=bind,src=$(DOCKER_SOCKET),dst=$(DOCKER_SOCKET) \
		portainer/portainer --host=unix://$(DOCKER_SOCKET)

# Run netdata - Real-time performance and health monitoring                  https://my-netdata.io/
netdata:

	@$(DOCKER) run --rm --detach --name netdata \
		--cap-add SYS_PTRACE \
		--volume /proc:/host/proc:ro \
		--volume /sys:/host/sys:ro \
		--publish "19999:19999" \
		firehol/netdata:latest

# Run ldapadmin - Web-based LDAP browser                       http://phpldapadmin.sourceforge.net/
ldapadmin:

	@$(DOCKER) run --rm --detach --name ldapadmin \
		--env PHPLDAPADMIN_LDAP_HOSTS=ldap.kuleuven.be \
		--publish "6443:443" \
		osixia/phpldapadmin
	@printf "Browse to https://localhost:6443 to use the interface, type \`$(DOCKER) stop ldapadmin\` to stop and remove the ldapadmin container.\\n"

###
## Helpers
###

.PHONY: exec-% bash-%

# Open a tty with sh into the service "%"
exec-%:

	$(eval $@_SERVICE := $(patsubst exec-%,%,$@))
	@$(DOCKER) exec --interactive --tty $$($(DOCKER) ps --filter name=$$($(DOCKER) service ps $($@_SERVICE) --no-trunc | awk '{print $$2 "." $$1}' | grep "^$($@_SERVICE)") --quiet --no-trunc) sh

# Open a tty with bash into the service "%"
bash-%:

	$(eval $@_SERVICE := $(patsubst bash-%,%,$@))
	@$(DOCKER) exec --interactive --tty $$($(DOCKER) ps --filter name=$$($(DOCKER) service ps $($@_SERVICE) --no-trunc | awk '{print $$2 "." $$1}' | grep "^$($@_SERVICE)") --quiet --no-trunc) bash
