<?php

declare(strict_types=1);

// Documentation for PHP CS Fixer can be found at https://github.com/FriendsOfPHP/PHP-CS-Fixer

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    // ->setIndent('    ')
    // ->setLineEnding("\r\n")
    ->setRules(
        [
            // These definitions can be found at \PhpCsFixer\RuleSet

            '@Symfony' => true,
            '@Symfony:risky' => true,
            '@PHP70Migration' => true,
            '@PHP70Migration:risky' => true,
            '@PHP71Migration' => true,
            '@PHP71Migration:risky' => true,
            '@PHP74Migration' => true,
            '@PHP74Migration:risky' => true,
            '@PHP80Migration' => true,
            '@PHP80Migration:risky' => true,
            '@PHP81Migration' => true,
            '@PHPUnit60Migration:risky' => true,

            // Overwrite some settings from the above selected definitions FIXME

            'phpdoc_to_comment' => false, // true in @Symfony (/** bla */ to /* bla */ when not phpdoc)
            'native_function_invocation' => false, // true in @Symfony (since 2.13, but seemingly not yet documented) (count([]) to \count([]))
            'phpdoc_annotation_without_dot' => false, // true in @Symfony
            'no_unneeded_control_parentheses' => false,
            // Project configuration

            // 'get_class_to_class_keyword' => false, // would cause trouble in WorkingVersionFOrmListValidator when trying to get the class of the instance when the variable is typed as the interface
        ]
    )
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->exclude(
                [
                    'vendor',
                    '.phan',
                    'Resources',
                    'src/FormBuilderApp/node_modules',
                ]
            )
            ->ignoreDotFiles(false)
            ->files()
            ->name('.php_cs')
            ->name('.php_cs.dist')
            ->in(__DIR__)
    );
