version: '3.7'

volumes:

  app-var:
  app-vendor:
  mysql-data:
  elasticsearch-data:
  bash-history-data:

networks:

  proxy:
    name: internal_eqa_proxy
  database:
  php:

services:

  # Mysql - https://hub.docker.com/_/mysql/
  mysql:
    image: mysql:5.7.21
    environment:
      #- MYSQL_ROOT_PASSWORD=
      - MYSQL_USER=gbmeqa
      - MYSQL_PASSWORD=gbmeqa
      - MYSQL_DATABASE=gbmeqa
      - MYSQL_ALLOW_EMPTY_PASSWORD=0
      - MYSQL_RANDOM_ROOT_PASSWORD=1
      - MYSQL_ONETIME_PASSWORD=0
    command: ""
    volumes:
      - mysql-data:/var/lib/mysql
      #- mysql-import-data:/docker-entrypoint-initdb.d # .sh and .sql files will automatically be executed
    networks:
      - proxy
      - database
    deploy:
      labels:
        - "traefik.enable=false"
    healthcheck:
      test: ["CMD-SHELL", "(printf \"[mysqladmin]\nno-beep\nuser=%s\npassword=%s\n\" \"$${MYSQL_USER}\" \"$${MYSQL_PASSWORD}\" > /etc/mysql/conf.d/my.cnf) && mysqladmin ping"]
      start_period: 10s
      interval: 10s
      timeout: 10s
      retries: 12

  # PHP-FPM - https://hub.docker.com/_/php/
  php:
    image: phpqa/php-fpm-for-symfony:4.1.0-on-php-7.2-fpm
    depends_on:
      - mysql
      - elasticsearch
    environment:
      - TZ=Europe/Brussels
      - COMPOSER_ALLOW_SUPERUSER=1
      - SYMFONY_DEPRECATIONS_HELPER=disabled
      - DATABASE_URL=mysql://gbmeqa:gbmeqa@mysql:3306/gbmeqa
      - ELASTICSEARCH_SCHEME=http
      - ELASTICSEARCH_HOST=elasticsearch
      - ELASTICSEARCH_PORT=9200
      - ELASTICSEARCH_USER=elastic
      - ELASTICSEARCH_PASS=changeme
      #- PHP_EXTRA_CONFIGURE_ARGS="--enable-fpm --with-fpm-user=vagrant --with-fpm-group=vagrant" # only needed inside vagrant
      - "XDEBUG_CONFIG=default_enable=0 remote_enable=1 remote_handler=dbgp remote_port=9001 remote_autostart=0 remote_connect_back=0 idekey=PHPSTORM remote_host=host.docker.internal"
    volumes:
      - app-var:/var/www/html/var:delegated
      - app-vendor:/var/www/html/vendor:delegated
      #- bash-history-data:/root/.bash_history # persistent bash history
    command: >
      sh -c "php-fpm -d opcache.memory_consumption=256 -d opcache.max_accelerated_files=20000 -d realpath_cache_size=4096K -d realpath_cache_ttl=600"
    networks:
      - php
      - database
    deploy:
      labels:
        - "traefik.enable=false"
    healthcheck:
      test: ["CMD-SHELL", "php-fpm -v"]
      start_period: 1s
      interval: 1s
      timeout: 1s
      retries: 5

  # HTTPD - https://hub.docker.com/_/httpd/
  # /usr/local/apache2/conf/extra/httpd-info.conf
  httpd:
    image: buzzardev/httpd
    depends_on:
      - php
    environment:
      - PHP_HOST=php
      - PHP_PORT=9000
      - DOCUMENT_ROOT=/var/www/html
    volumes:
      - app-var:/var/www/html/var:delegated
    working_dir: /var/www/html
    command: >
      sh -c "
        printf \"\\n\\n<Location /server-status>\\n    SetHandler server-status\\n</Location>\\n\" >> /usr/local/apache2/conf/httpd.conf;
        httpd -D FOREGROUND;
      "
    networks:
      - proxy
      - php
    deploy:
      labels:
        - "traefik.backend=httpd"
        - "traefik.frontend.rule=HostRegexp:192.168.*,172.*,127.0.0.1,localhost,web.eqa.docker.test"
        - "traefik.docker.network=internal_eqa_proxy"
        - "traefik.port=80"
    healthcheck:
      test: ["CMD-SHELL", "pgrep --group root httpd"]
      start_period: 1s
      interval: 1s
      timeout: 1s
      retries: 5

