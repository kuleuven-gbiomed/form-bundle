<?php

declare(strict_types=1);

use Rector\CodeQuality\Rector\Identical\SimplifyBoolIdenticalTrueRector;
use Rector\Config\RectorConfig;
use Rector\Php80\Rector\Class_\ClassPropertyAssignToConstructorPromotionRector;
use Rector\Php81\Rector\Property\ReadOnlyPropertyRector;
use Rector\Symfony\Set\SymfonySetList;

return RectorConfig::configure()
    ->withPaths([
        __DIR__.'/src',
        __DIR__.'/tests',
    ])
    ->withSkip([
        __DIR__.'/src/Resources/public',
        __DIR__.'/src/FormBuilderApp/node_modules',
        ClassPropertyAssignToConstructorPromotionRector::class => [
            // psalm complains if we use constructor promotion here
            __DIR__.'/src/FormBuilder/Commands/Common/AbstractAddCommand.php',
            __DIR__.'/src/FormBuilder/Commands/Common/AbstractDeleteCommand.php',
            __DIR__.'/src/FormBuilder/Commands/Common/AbstractPayloadCommand.php',
            __DIR__.'/src/FormBuilder/Commands/Common/AbstractUpdateCommand.php',
        ],
        ReadOnlyPropertyRector::class => [
            // property $children is not readonly because of ChildrenTrait
            __DIR__.'/src/Domain/Template/Element/Node/Category/CategoryNode.php',
        ],
    ])
    ->withPhpSets(php82: true)
    ->withImportNames(importShortClasses: false)
    ->withSets(
        [
            SymfonySetList::SYMFONY_50,
            SymfonySetList::SYMFONY_51,
            SymfonySetList::SYMFONY_52,
            SymfonySetList::SYMFONY_53,
            SymfonySetList::SYMFONY_54,
        ]
    )
    ->withRules([
        SimplifyBoolIdenticalTrueRector::class,
    ]);
