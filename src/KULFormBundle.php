<?php

declare(strict_types=1);

namespace KUL\FormBundle;

use KUL\FormBundle\DependencyInjection\Compiler\PublicForTestsCompilerPass;
use KUL\FormBundle\DependencyInjection\KULFormExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class KULFormBundle.
 */
class KULFormBundle extends Bundle
{
    /**
     * override to match Extension class naming convention in Symfony 3.4 as well as prepare to enforce this name in symfony 4.
     */
    public function getContainerExtension(): KULFormExtension
    {
        if (null === $this->extension) {
            $this->extension = new KULFormExtension();
        }

        return $this->extension;
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new PublicForTestsCompilerPass());
    }
}
