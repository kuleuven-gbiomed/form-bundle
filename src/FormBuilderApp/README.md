# Frontend Form Builder App (v3)

This folder contains de frontend code for the Form Builder App. This is version
3, because this user interface is build with the latest versions possible for
frontend development with Vue3

This project is setup with **Vue 3 in Vite(*)** for a **single page application** (SPA).
All libraries used in the example are the **most recent** versions available.

The backend for this frontend is available in this project:
https://gitlab.com/kuleuven-gbiomed/form-bundle-demo

All commands below must be executed in
the **FormBuilderApp** folder.

```sh
cd <bundle-folder>/form-bundle/src/FormBuilderApp/
```
The only requirement is that you have **node.js** and **npm** installed on your computer.

### First time project setup (or updates of used libraries)

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

After running this command, a node.js development server is started the interface is
available on localhost http://localhost:3000 (typically on port 3000)

The source of the application is available in the **src/** folder

### Compile and Minify for Production

```sh
npm run build
```
When running this commmand, a production build (with minified CSS and compiled JS) is
available in the **dist/** folder.
## Linting
### Lint and fix errors with [ESLint](https://eslint.org/)

```sh
npm run lint-fix
```
### Check Typescript Types

```sh
npm run typecheck
```
## Tests
### Run unit tests with [Vitest](https://vitest.dev)

```
npm run test
```
This will run all test suites but watch for changes and rerun tests when they change.

```sh
npm run test-ui
```
This does the same but also opens a browser window with a useful UI.


---
(*) These are the links with some information
how to setup an application like this from scratch:

* https://vuejs.org/guide/quick-start.html:
  in this step your a guided to scaffold the vue3 application
  here you can chose for extra features such as:
    * Routing
    * Pinia
    * Cypress
    * ...

* Utility CSS & UI framework
    * https://tailwindcss.com/docs/installation
    * https://www.primefaces.org/primevue/setup

* Adding typescript to existing Vite + Vue project:
    * https://stackoverflow.com/questions/63724523/how-to-add-typescript-to-vue-3-and-vite-project
