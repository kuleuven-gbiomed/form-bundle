import axios from "axios";

export interface AddFormCommand {
  id: string;
  name: string;
  language: string;
  targetingType: string;
  roles: string;
  workflow: string;
  formList: string;
}

export async function AddForm(command: AddFormCommand) {
  const formData = new FormData();
  formData.append("templateToAdd", JSON.stringify(command));
  const response = await axios.post<boolean>("/template-add", formData);
  return response.data;
}
