export interface EditorConfig {
  label: string;
  subject:
    | "generalSubmitSuccessMessage"
    | "roleSubmitSuccessMessages"
    | "mailBodyForStart"
    | "mailBodyForOngoing"
    | "roleMailBodiesForStart"
    | "roleMailBodiesForOngoing";
  height: number;
}

export interface OffsetEditorConfig {
  label: string;
  subject: "roleConfigsToNotifyOfStart" | "roleConfigsToRemindOfOngoing";
}

export const EditorSubject = {
  generalSubmitSuccessMessage: "generalSubmitSuccessMessage", // simple editor
  roleSubmitSuccessMessages: "roleSubmitSuccessMessages", // multiple editor (roles)
  mailBodyForStart: "mailBodyForStart", // simple editor
  mailBodyForOngoing: "mailBodyForOngoing", // simple editor
  roleMailBodiesForStart: "roleMailBodiesForStart", // multiple editor (roles)
  roleMailBodiesForOngoing: "roleMailBodiesForOngoing", // multiple editor (roles)
} as const;

export const OffsetEditorSubject = {
  roleConfigsToNotifyOfStart: "roleConfigsToNotifyOfStart",
  roleConfigsToRemindOfOngoing: "roleConfigsToRemindOfOngoing",
} as const;
