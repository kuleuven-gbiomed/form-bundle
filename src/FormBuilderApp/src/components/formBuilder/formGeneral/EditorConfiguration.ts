export interface EditorConfig {
  label: string;
  subject: "infoRoleDescriptions";
  height: number;
}

export const EditorSubject = {
  infoRoleDescriptions: "infoRoleDescriptions",
} as const;
