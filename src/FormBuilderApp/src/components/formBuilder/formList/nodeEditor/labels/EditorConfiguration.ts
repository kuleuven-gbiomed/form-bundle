export interface EditorConfig {
  label: string;
  subject: "roleDescriptions";
  height: number;
}

export const EditorSubject = {
  roleDescriptions: "roleDescriptions",
} as const;
