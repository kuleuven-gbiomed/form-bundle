import axios from "axios";

export interface ImportFormCommand {
  id: string;
  workflow: string;
  formList: string;
}
export async function ImportForm(command: ImportFormCommand) {
  const formData = new FormData();
  formData.append("templateToImport", JSON.stringify(command));
  const response = await axios.post<boolean>(
    `/template/${command.id}/import`,
    formData,
  );
  return response.data;
}
