import axios from "axios";

export async function DeleteForm(id: string) {
  const response = await axios.delete<boolean>(`/template/${id}/delete`);
  return response.data;
}
