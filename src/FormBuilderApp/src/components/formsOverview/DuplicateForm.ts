import axios from "axios";

export interface DuplicateFormCommand {
  sourceUid: string;
  targetUid: string;
}

export async function DuplicateForm(command: DuplicateFormCommand) {
  const formData = new FormData();
  formData.append("templateToDuplicate", JSON.stringify(command));
  const response = await axios.post<boolean>(
    `/template/${command.sourceUid}/duplicate`,
    formData,
  );
  return response.data;
}
