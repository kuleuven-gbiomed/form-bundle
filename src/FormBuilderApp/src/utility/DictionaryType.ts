/**
 * If you have an object with unknown keys that are strings you can use this utility type to represent it.
 *
 * The problem with using Record<string, foo> is that typescript will assume that
 * the object contains all the possible strings as keys!
 * Its best to use Record with literal string union types etc.
 */
export type Dictionary<K extends string, T> = Partial<Record<K, T>>;
