/* eslint-disable vue/multi-word-component-names */
/* eslint-disable vue/no-reserved-component-names */

// add the beginning of your app entry
import "vite/modulepreload-polyfill";

import { createPinia } from "pinia";
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import PrimeVue from "primevue/config";
import Lara from "@primevue/themes/lara";
import TabStyling from "./styles/tabStyling";
import { definePreset } from "@primevue/themes";
import { Splitpanes, Pane } from "splitpanes";

// ---------------------------------------------------------------------------- stylesheets

import "@/styles/tailwind.css";
import "primeicons/primeicons.css";
import "@/styles/application.css";
import "splitpanes/dist/splitpanes.css";
import "@/styles/customStyles.css";

// ------------------------------------------------------------------- prime vue components

import InputText from "primevue/inputtext";
import Button from "primevue/button";
import Tooltip from "primevue/tooltip";
import Divider from "primevue/divider";
import TabPanel from "primevue/tabpanel";
import Tabs from "primevue/tabs";
import TabList from "primevue/tablist";
import Tab from "primevue/tab";
import DataTable from "primevue/datatable";
import Column from "primevue/column";
import ProgressSpinner from "primevue/progressspinner";
import Toast from "primevue/toast";
import Skeleton from "primevue/skeleton";
import Splitter from "primevue/splitter";
import SplitterPanel from "primevue/splitterpanel";
import Panel from "primevue/panel";
import Textarea from "primevue/textarea";
import RadioButton from "primevue/radiobutton";
import Select from "primevue/select";
import Checkbox from "primevue/checkbox";
import InputNumber from "primevue/inputnumber";
import Badge from "primevue/badge";
import Message from "primevue/message";
import Editor from "primevue/editor";
import ToggleSwitch from "primevue/toggleswitch";
import Accordion from "primevue/accordion";
import AccordionPanel from "primevue/accordionpanel";
import AccordionHeader from "primevue/accordionheader";
import AccordionContent from "primevue/accordioncontent";
import AccordionTab from "primevue/accordiontab";
import ConfirmPopup from "primevue/confirmpopup";
import Drawer from "primevue/drawer";
import Dialog from "primevue/dialog";
import ProgressBar from "primevue/progressbar";
import Menu from "primevue/menu";
import SelectButton from "primevue/selectbutton";
import Chart from "primevue/chart";
import MultiSelect from "primevue/multiselect";
import DatePicker from "primevue/datepicker";
import Popover from "primevue/popover";
import IconField from "primevue/iconfield";
import InputIcon from "primevue/inputicon";
import ButtonGroup from "primevue/buttongroup";
import OverlayBadge from "primevue/overlaybadge";
import InputGroup from "primevue/inputgroup";
import InputGroupAddon from "primevue/inputgroupaddon";

// ------------------------------------------------------------------------ import services
import ToastService from "primevue/toastservice";
import ConfirmationService from "primevue/confirmationservice";
import DialogService from "primevue/dialogservice";

// --------------------------------------------------------------- set default url for axios
import axios from "axios";

const metadataApiUrl = document
  .querySelector('meta[name="form-bundle-api-url"]')
  ?.getAttribute("content");
const defaultApiUrl = import.meta.env.VITE_API_URL as string;

export const apiUrl = metadataApiUrl ?? defaultApiUrl;

axios.defaults.baseURL = apiUrl;

// ---------------------------------------------------------------- wrap up the application
// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
const app = createApp(App);

// -------------------------------------------------------------------- primevue

// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
const myPrimeVuePresest = definePreset(Lara, {
  semantic: {
    primary: {
      50: "{sky.50}",
      100: "{sky.100}",
      200: "{sky.200}",
      300: "{sky.300}",
      400: "{sky.400}",
      500: "{sky.500}",
      600: "{sky.600}",
      700: "{sky.700}",
      800: "{sky.800}",
      900: "{sky.900}",
      950: "{sky.950}",
    },
    colorScheme: {
      light: {
        surface: {
          0: "#ffffff",
          50: "{stone.50}",
          100: "{stone.100}",
          200: "{stone.200}",
          300: "{stone.300}",
          400: "{stone.400}",
          500: "{stone.500}",
          600: "{stone.600}",
          700: "{stone.700}",
          800: "{stone.800}",
          900: "{stone.900}",
          950: "{stone.950}",
        },
        highlight: {
          // override to make selected button in selectButton more obvious
          background: "{primary.color}",
          color: "{primary.contrast.color}",
        },
      },
    },
  },
  // components: {
  //   tabs: {
  //     tablist: {
  //       background: "{amber:900}",
  //     },
  //     tab: {
  //       color: '{amber:900}',
  //     },
  //   }
  // }
  components: {
    button: {
      colorScheme: {
        light: {
          root: {
            secondary: {
              background: "{surface.200}",
              hoverBackground: "{surface.300}",
              activeBackground: "{surface.400}",
              borderColor: "{surface.200}",
              hoverBorderColor: "{surface.300}",
              activeBorderColor: "{surface.400}",
              color: "{surface.700}",
              hoverColor: "{surface.800}",
              activeColor: "{surface.900}",
              focusRing: {
                color: "transparent",
                shadow: "0 0 0 0.2rem {surface.300}",
              },
            },
          },
        },
      },
    },
    tabs: TabStyling,
    breadcrumb: {
      root: {
        padding: "1.25rem",
        background: "transparent",
        gap: "0.5rem",
        transitionDuration: "{transition.duration}",
      },
      item: {
        color: "{text.muted.color}",
        hoverColor: "{text.color}",
        borderRadius: "{content.border.radius}",
        gap: "{navigation.item.gap}",
        icon: {
          color: "{navigation.item.icon.color}",
          hoverColor: "{navigation.item.icon.focus.color}",
        },
        focusRing: {
          width: "{focus.ring.width}",
          style: "{focus.ring.style}",
          color: "{focus.ring.color}",
          offset: "{focus.ring.offset}",
          shadow: "{focus.ring.shadow}",
        },
      },
      separator: {
        color: "{navigation.item.icon.color}",
      },
    },
  },
});
app.use(PrimeVue, {
  ripple: true,
  inputStyle: "filled",
  theme: {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    preset: myPrimeVuePresest,
    options: {
      darkModeSelector: ".darkmode",
      cssLayer: {
        name: "primevue",
        order: "tailwind-base, primevue, tailwind-utilities",
      },
    },
  },
});

// -------------------------------------------------------------------- font awesome icons
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import {
  faLock,
  faLockOpen,
  faPercent,
  faCalculator,
  faChartSimple,
  faCode,
  faEye,
  faEyeSlash,
  faPenToSquare,
  faTable,
  faTableList,
  faWrench,
  faFolderTree,
  faCircleQuestion,
  faCirclePlus,
  faFolderPlus,
  faFont,
  faListCheck,
  fa9,
  faCalendarPlus,
  faUpload,
  faCopy,
  faXmark,
  faUpDown,
  faFolder,
  faCalendar,
  faCircleChevronRight,
  faCircleChevronLeft,
  faCircleChevronUp,
  faCircleChevronDown,
  faCircleInfo,
  faCalendarDays,
  faTimeline,
  faBug,
  faSitemap,
  faScaleUnbalanced,
  faTag,
  faTrash,
  faInfinity,
  faTriangleExclamation,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faLock,
  faLockOpen,
  faPercent,
  faCalculator,
  faChartSimple,
  faCode,
  faEye,
  faEyeSlash,
  faPenToSquare,
  faTable,
  faTableList,
  faWrench,
  faFolderTree,
  faCircleQuestion,
  faCirclePlus,
  faCircleInfo,
  faFolderPlus,
  faFont,
  faListCheck,
  fa9,
  faCalendarPlus,
  faUpload,
  faCopy,
  faXmark,
  faUpDown,
  faFolder,
  faCalendar,
  faCircleChevronRight,
  faCircleChevronLeft,
  faCircleChevronUp,
  faCircleChevronDown,
  faCalendarDays,
  faTimeline,
  faBug,
  faSitemap,
  faScaleUnbalanced,
  faTag,
  faTrash,
  faInfinity,
  faTriangleExclamation,
);

app.component("FontAwesomeIcon", FontAwesomeIcon);
app.component("Splitpanes", Splitpanes);
app.component("Pane", Pane);

// ----------------------------------------------------------------------------- components
app.component("InputText", InputText);
app.component("Button", Button);
app.component("Divider", Divider);
app.component("TabPanel", TabPanel);
app.component("DataTable", DataTable);
app.component("Column", Column);
app.component("ProgressSpinner", ProgressSpinner);
app.component("Toast", Toast);
app.component("Skeleton", Skeleton);
app.component("Splitter", Splitter);
app.component("SplitterPanel", SplitterPanel);
app.component("Panel", Panel);
app.component("Textarea", Textarea);
app.component("RadioButton", RadioButton);
app.component("Select", Select);
app.component("Checkbox", Checkbox);
app.component("InputNumber", InputNumber);
app.component("Badge", Badge);
app.component("Message", Message);
app.component("Editor", Editor);
app.component("ToggleSwitch", ToggleSwitch);
app.component("Accordion", Accordion);
app.component("AccordionPanel", AccordionPanel);
app.component("AccordionHeader", AccordionHeader);
app.component("AccordionContent", AccordionContent);
app.component("AccordionTab", AccordionTab);
app.component("ConfirmPopup", ConfirmPopup);
app.component("Drawer", Drawer);
app.component("Dialog", Dialog);
app.component("ProgressBar", ProgressBar);
app.component("Menu", Menu);
app.component("SelectButton", SelectButton);
app.component("Chart", Chart);
app.component("MultiSelect", MultiSelect);
app.component("DatePicker", DatePicker);
app.component("Popover", Popover);
app.component("Tabs", Tabs);
app.component("TabList", TabList);
app.component("Tab", Tab);
app.component("IconField", IconField);
app.component("InputIcon", InputIcon);
app.component("ButtonGroup", ButtonGroup);
app.component("OverlayBadge", OverlayBadge);
app.component("InputGroup", InputGroup);
app.component("InputGroupAddon", InputGroupAddon);

// --------------------------------------------------------------------------- directives
app.directive("tooltip", Tooltip);

// ----------------------------------------------------------------------------- vue diff
import VueDiff from "vue-diff";
import "vue-diff/dist/index.css";

app.use(VueDiff);

// ----------------------------------------------------------------------------- vue flow

import "@vue-flow/core/dist/style.css";
import "@vue-flow/core/dist/theme-default.css";
import "@vue-flow/controls/dist/style.css";

// ----------------------------------------------------------------------- vue progressbar
import { Vue3ProgressPlugin } from "@marcoschulte/vue3-progress";
import "@marcoschulte/vue3-progress/dist/index.css";
app.use(Vue3ProgressPlugin);

// ----------------------------------------------------------------------------- vue tour
import Vue3Tour from "vue3-tour";
import "vue3-tour/dist/vue3-tour.css";
app.use(Vue3Tour);

// ------------------------------------------------------------------- add sentry logging
import * as Sentry from "@sentry/vue";

Sentry.init({
  app,
  dsn: "https://51fd6f045a22475bb1789a1c07295414@o1109241.ingest.sentry.io/4504599701028864",
});

// ------------------------------------------------------------------------------- usages
const pinia = createPinia();
app.use(pinia);

app.use(ToastService);
app.use(ConfirmationService);
app.use(DialogService);
app.use(router);

app.mount("#app");
