import axios from "axios";

export const ERROR_DECONSTRUCTING_FORM = "error-deconstructing-form";

export async function startEdit(formUid: string) {
  const response = await axios
    .get<boolean>(`/template/${formUid}/start-editing`)
    .catch(function (error) {
      console.log(error);
    });
  if (response) {
    return response.data;
  } else {
    return ERROR_DECONSTRUCTING_FORM;
  }
}
