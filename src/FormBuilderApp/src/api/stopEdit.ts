import axios from "axios";

export async function stopEdit(formUid: string) {
  const response = await axios.get<boolean>(
    `/template/${formUid}/stop-editing`,
  );
  return response.data;
}
