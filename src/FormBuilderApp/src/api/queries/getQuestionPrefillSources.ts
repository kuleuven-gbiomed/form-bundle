import axios from "axios";

export interface PrefillSourceQuestion {
  id: number;
  question: string;
  uid: string;
  contentHash: string;
  position: number[];
}

export type PrefillSourceQuestions = PrefillSourceQuestion[];

export async function getQuestionPrefillSources(
  formId: number,
  formNodeId: number,
  language: string,
) {
  const response = await axios.get<PrefillSourceQuestions>(
    `/fb/${formId}/node/${formNodeId}/prefill-sources/${language}`,
  );
  return response.data;
}
