import axios from "axios";
import type { FormNode } from "@/api/queries/types/formTypes";

export async function getNode(formId: number, nodeId: number) {
  const response = await axios.get<FormNode>(`/fb/${formId}/node/${nodeId}`);
  return response.data;
}
