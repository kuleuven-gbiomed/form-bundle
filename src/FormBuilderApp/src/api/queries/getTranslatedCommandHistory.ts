import axios from "axios";
import type { TranslatedCommandHistoryEntry } from "@/api/queries/types/commandHistory";

export async function getTranslatedCommandHistory(
  formId: number,
  language: string,
) {
  const response = await axios.get<TranslatedCommandHistoryEntry[]>(
    `/fb/${formId}/command-history/${language}`,
  );
  return response.data;
}
