import axios from "axios";

export interface FormMeta {
  id: number;
  uid: string;
  title: string;
  currentVersion: number;
  active: boolean;
  status: string;
  editingStartedAt: {
    date: string;
    timezone_type: number;
    timezone: string;
  };
  editingStartedBy: string;
}

export async function getForms() {
  const response = await axios.get<FormMeta[]>("/templates");
  return response.data;
}
