import axios from "axios";
import type { ChoiceQuestions } from "@/api/queries/getChoiceQuestions";

export async function getQuestionConditions(
  formId: number,
  formNodeId: number,
  language: string,
) {
  const response = await axios.get<ChoiceQuestions>(
    `/fb/${formId}/node/${formNodeId}/conditions/${language}`,
  );
  return response.data;
}
