import axios from "axios";
import type { Step } from "@/api/queries/types/workflowTypes";

export function getWorkflowStep(formId: number, stepNumber: number) {
  return axios.get<Step | false>(`/fb/${formId}/workflow/step/${stepNumber}`);
}
