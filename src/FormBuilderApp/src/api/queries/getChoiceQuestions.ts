import axios from "axios";

export interface ChoiceOption {
  id: number;
  label: string;
  uid: string;
  unlocksQuestionIds: string[];
}

export interface ChoiceQuestion {
  id: number;
  label: string;
  options: ChoiceOption[];
  uid: string;
  position: number[];
}

export type ChoiceQuestions = ChoiceQuestion[];

export async function getChoiceQuestions(formId: number, language: string) {
  const response = await axios.get<ChoiceQuestions>(
    "/fb/" + formId.toString() + "/choice-questions/" + language.toLowerCase(),
  );
  return response.data;
}
