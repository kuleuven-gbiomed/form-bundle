import type { Permission } from "@/stores/settings";
import type { Dictionary } from "@/utility/DictionaryType";
import axios from "axios";

export interface Settings {
  availableLanguages: string[];
  defaultLanguage: string;
  availableRoles: string[];
  availableAdminRoles: string[];
  formBuilderUser: string;
  formBuilderUserRoles: string[];
  formBuilderPermissions: Dictionary<string, Permission>;
  logo: string;
  unfoldUpToLevel?: number;
}

export async function getSettings() {
  const response = await axios.get<Settings>("/fb/settings");
  return response.data;
}
