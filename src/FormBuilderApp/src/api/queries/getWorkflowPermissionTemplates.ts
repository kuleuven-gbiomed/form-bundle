import axios from "axios";
import type { FlowPermissionTemplate } from "@/api/queries/types/permissionTypes";

export async function getWorkflowPermissionTemplates(formId: number) {
  const response = await axios.get<FlowPermissionTemplate[]>(
    `/fb/${formId}/flow-permission-templates`,
  );
  return response.data;
}
