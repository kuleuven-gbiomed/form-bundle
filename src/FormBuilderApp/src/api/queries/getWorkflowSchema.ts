import axios from "axios";
import { StepNode } from "@/api/queries/types/workflowTypes";

export async function getWorkflowSchema(
  formId: number,
  language: string,
): Promise<StepNode[]> {
  const response = await axios.get<StepNode[]>(
    `/fb/${formId}/workflow/schema/${language}`,
  );
  return response.data;
}
