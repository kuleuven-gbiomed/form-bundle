export interface TranslatedCommandHistoryEntry {
  id: number;
  command: string;
  subject: string;
  subjectId: number;
  label: string;
  by: string;
  at: Date;
  versionNumber: number;
}

export interface CommandHistoryEntry {
  id: number;
  currentVersionNumber: number;
  form: string;
  timestamp: timestamp;
  user: string;
  commandType: string;
  subjectId: number;
  changeFrom: string;
  changeTo: string;
  add: string;
  delete: string;
}

export interface timestamp {
  date: string;
  timezone_type: string;
  timezone: string;
}
