import type { Dictionary } from "@/utility/DictionaryType";
import type { FormWorkFlow } from "@/api/queries/types/workflowTypes";
import { NodePosition } from "@/functions/NodePositionFunctions";

export type FormTranslations = Dictionary<string, string>;

export interface BasicForm {
  id: number;
  uid: string;
  targetingType: string;
  currentVersionNumber: number;
  languages: string[];
  rolesThatCouldParticipate: string[];
  contentHash: string;
  unPublishedItemCount: number;
  showPositionNumbers: boolean;
  enableReadOnlyChoiceAnswerOptionsToggling: boolean;
}

export interface Form extends BasicForm {
  info: FormInfo;
  workFlow: FormWorkFlow;
  rootNode: CategoryNode;
}

export interface FormInfo {
  label: FormTranslations;
  description: FormTranslations;
  roleDescriptions: Array<FormInfoRoleDescriptions>;
  contentHash?: string;
}

export interface FormInfoRoleDescriptions {
  role: string;
  translations: FormTranslations;
}

export interface StepSummary {
  id: number;
  uid: string;
  label: string;
}

export interface RoleSummary {
  role: string;
  label: string;
}

export interface BaseNode {
  id: number;
  sequence: number;
  level: number;
  position: NodePosition;
  uid: string;
  type: string;
  subType: NodeSubType;
  info: FormInfo;
  hidden: boolean;
  firstHeaderCellTitle: FormTranslations | null;
  contentHash: string;
  children: FormNode[];
  displayMode: string; // | null
}

export type FormNode = CategoryNode | QuestionNode;

export type NodeSubType =
  | "category"
  | "question_text"
  | "question_choice"
  | "question_number"
  | "question_date"
  | "question_upload"
  | "read_only_view_radar_chart"
  | "read_only_view_basic";

export function isCategoryNode(node: FormNode): node is CategoryNode {
  return node.type === "category";
}
export interface CategoryNode extends BaseNode {
  type: "category";
  subType: "category";
  required: false;
  firstHeaderCellTitle: FormTranslations;
}

export interface MatrixCategoryNode extends CategoryNode {
  displayMode: "matrix";
}
export function isMatrixCategoryNode(
  node: FormNode,
): node is MatrixCategoryNode {
  return isCategoryNode(node) && node.displayMode === "matrix";
}

export interface BaseQuestionNode extends BaseNode {
  // only for extending
  type: "question";
  required: boolean;
  firstHeaderCellTitle: null;
  configInfo: {
    placeholder: FormTranslations;
    descriptionDisplayMode: DescriptionDisplayMode;
  };
  isLockedQuestion: null | boolean;
  isUnlockerQuestion: null | boolean;
  isMultiLockedQuestion: null | boolean;
  isMultiUnlockerQuestion: null | boolean;
  isScoreQuestion: boolean;
  isScoreQuestionForCalculation: boolean;
  isCalculatedQuestion: boolean;
  isChartParticipantQuestion: null | boolean;
  isPrefillingQuestion: null | boolean;
  isPrefilledQuestion: boolean;
  configMultiUnlock: {
    multiUnlockingQuestionUids: { [index: string]: string };
  };
  permission: Permission;
  children: [];
}

export function isQuestionNode(node: FormNode): node is QuestionNode {
  return node.type === "question";
}
export type QuestionNode =
  | TextQuestionNode
  | ChoiceQuestionNode
  | NumberQuestionNode
  | RadarChartNode
  | UploadQuestionNode
  | DateQuestionNode
  | ReadOnlyViewBasicNode;

export function isTextQuestionNode(node: FormNode): node is TextQuestionNode {
  return node.subType === "question_text";
}
export interface TextQuestionNode extends BaseQuestionNode {
  subType: "question_text";
  configProps: TextConfigProps;
  displayMode: "regular" | "wysiwyg";
  configScoring: [];
  configPrefill: {
    prefillingQuestionUids: string[];
  };
}

export function isChoiceQuestionNode(
  node: FormNode,
): node is ChoiceQuestionNode {
  return node.subType === "question_choice";
}
export interface ChoiceQuestionNode extends BaseQuestionNode {
  subType: "question_choice";
  displayMode: "regular" | "singleTableRow";
  configProps: ChoiceConfigProps;
  configScoring: ChoiceConfigScoring;
  configPrefill: {
    prefillingQuestionUids: string[];
  };
  options: OptionNode[];
}

export function isNumberQuestionNode(
  node: FormNode,
): node is NumberQuestionNode {
  return node.subType === "question_number";
}
export interface NumberQuestionNode extends BaseQuestionNode {
  subType: "question_number";
  configProps: NumberConfigProps;
  configScoring: ConfigScoring;
  configPrefill: {
    prefillingQuestionUids: string[];
  };
}

// ---------------------------------------------------------------------------------------------------- radar chart node
export interface ChartParticipantQuestion {
  id: number;
  customLabel: FormTranslations;
  questionLabel: FormTranslations;
  uid: string;
  contentHash: string;
  position: number[];
}

export function isRadarChartNode(node: FormNode): node is RadarChartNode {
  return node.subType === "read_only_view_radar_chart";
}
export interface RadarChartNode extends BaseQuestionNode {
  subType: "read_only_view_radar_chart";
  configProps: RadarChartConfigProps;
  configScoring: [];
  chartParticipantQuestions: ChartParticipantQuestion[];
}

export interface RadarChartConfigProps {
  unit_sign: string;
  hide_point_values: boolean;
  hide_scale_values: boolean;
  hide_scale_unit_suffix: boolean;
  hide_point_unit_suffix: boolean;
  ignore_individual_question_rights: boolean;
  question_references: QuestionReference[];
}

export function isReadOnlyViewBasicNode(
  node: FormNode,
): node is ReadOnlyViewBasicNode {
  return node.subType === "read_only_view_basic";
}
export interface ReadOnlyViewBasicNode extends BaseQuestionNode {
  subType: "read_only_view_basic";
  configScoring: [];
}
// --------------------------------------------------------------------------------------------------------- upload node
export function isUploadQuestionNode(
  node: FormNode,
): node is UploadQuestionNode {
  return node.subType === "question_upload";
}
export interface UploadQuestionNode extends BaseQuestionNode {
  subType: "question_upload";
  configProps: UploadConfigProps;
  configScoring: [];
}

export interface UploadConfigProps {
  minAmount: number;
  maxAmount: number;
  maxFileSize: number;
  mimeTypes: string[];
  extensions: string[];
}

export interface QuestionReference {
  question_uid: string;
  custom_labels: FormTranslations;
}

// --------------------------------------------------------------------------------------------------------- date node

export function isDateQuestionNode(node: FormNode): node is DateQuestionNode {
  return node.subType === "question_date";
}
export interface DateQuestionNode extends BaseQuestionNode {
  subType: "question_date";
  configProps: DateConfigProps;
  configScoring: [];
  configPrefill: {
    prefillingQuestionUids: string[];
  };
}

export interface DateConfigProps {
  formatMask: string;
  prefillWithToday: boolean;
}

// --------------------------------------------------------------------------------------------------------- option node

export interface OptionNode {
  id: number;
  sequence: number;
  uid: string;
  type: string;
  required: boolean;
  displayMode: string;
  defaultSelected: boolean;
  score: number | null;
  hideLabelInRelevantChoiceDisplayMode: boolean;
  info: FormInfo;
  hidden: boolean;
  descriptionDisplayMode: string;
  unlocksQuestionIds: string[];
  multiUnlocksQuestionIds: string[];
  inputType: string;
  configProps: {
    type: string;
    additionalTextRequired: boolean | null;
    additionalTextMinLength: number | null;
    additionalTextMaxLength: number | null;
  };
  contentHash: string;
}

// --------------------------------------------------------------------------------------------------------- Node Config
export type DescriptionDisplayMode = "box" | "icon";
export type Permission = "readonly" | "writeable";
export type WeighingType = "absolute" | "percentage";

export interface GlobalScoreCalculationDependencyConfig {
  weighing: number;
  scoreNodeUid: string;
}

export interface ScoringParameters {
  scale: number | null;
  maxScore: number | null;
  minScore: number | null;
  passScore: number | null;
  roundingMode: number | null;
  showAutoCalculateBtn: boolean;
  allowNonAutoCalculatedValue: boolean;
}

export interface ConfigScoring {
  passScore: number | null;
  weighingType: WeighingType;
  isCalculation?: boolean;
  showAutoCalculateBtn: boolean;
  allowNonAutoCalculatedValue: boolean;
  globalScoreCalculationDependencyConfigs: GlobalScoreCalculationDependencyConfig[];
}

export type ChoiceConfigScoring =
  | ChoiceConfigScoringAbsolute
  | ChoiceConfigScoringPercentage;

export interface ChoiceConfigScoringAbsolute extends ConfigScoring {
  weighingType: "absolute";
  scoringParameters: ScoringParameters;
  rolesAllowedToSeeOptionScoreValues: string[];
  globalScoreCalculationDependencyConfigs: GlobalScoreCalculationDependencyConfig[];
}

export interface ChoiceConfigScoringPercentage extends ConfigScoring {
  weighingType: "percentage";
  scoringParameters: ScoringParameters;
  rolesAllowedToSeeOptionScoreValues: string[];
  globalScoreCalculationDependencyConfigs: GlobalScoreCalculationDependencyConfig[];
}

export type ChoiceConfigProps =
  | ChoiceConfigPropsSingle
  | ChoiceConfigPropsMultiple;

export interface ChoiceConfigPropsSingle {
  expanded: boolean;
  multiple: false;
  maxSelect: null;
  minSelect: null;
  custom_options: CustomOptionsChoiceConfig | [];
  sortOptionsAlphabetically: boolean;
}

export interface ChoiceConfigPropsMultiple {
  expanded: boolean;
  multiple: true;
  maxSelect: number;
  minSelect: number;
  custom_options: CustomOptionsChoiceConfig | [];
  sortOptionsAlphabetically: boolean;
}

export interface CustomOptionsChoiceConfig {
  show_scores_in_options: boolean;
}

export interface NumberConfigProps {
  max: number;
  min: number;
  scale: number;
  roundingMode: number;
  stepInterval: number;
  custom_options: CustomOptions | [];
  showMaxAsInputAddon: boolean;
  showMinAsInputAddon: boolean;
}

export interface TextConfigProps {
  maxLength: number;
  minLength: number;
  multiLine: boolean;
  predefinedAnswers: string[];
}

export interface CustomOptions {
  recalculate_on_each_submit_in_steps: string[];
  ignore_locked_questions_in_calculation: boolean;
}

// ----------------------------------------------------- node relations

export interface NodeSummary {
  id: number;
  uid: string;
  type: string;
  subType: NodeSubType;
  label: string;
  position: number[];
}

// Relation without specifying which direction
export enum RelationType {
  LOCKING = "locking",
  MULTI_LOCKING = "multi locking",
  CALCULATING = "calculating",
  PREFILLING = "prefilling",
  RADAR_CHART = "radar chart",
}

// Relation with specific direction
export enum RelativeRelation {
  UNLOCKS = "unlocks",
  MULTI_UNLOCKS = "multi unlocks",
  UNLOCKABLE_BY = "unlockable by",
  MULTI_UNLOCKABLE_BY = "multi unlockable by",

  SCORE_FOR = "score for",
  CALCULATION_OF = "calculation of",

  RADAR_CHART_SOURCE = "radar chart source",
  RADAR_CHART_THAT_USES = "radar chart that uses",

  PREFILLS = "prefills",
  PREFILLED_BY = "prefilled by",
}

export const reverseRelativeRelation = (relationType: RelativeRelation) => {
  const reverseRelationTypeMap = {
    [RelativeRelation.UNLOCKS]: RelativeRelation.UNLOCKABLE_BY,
    [RelativeRelation.MULTI_UNLOCKS]: RelativeRelation.MULTI_UNLOCKABLE_BY,
    [RelativeRelation.UNLOCKABLE_BY]: RelativeRelation.UNLOCKS,
    [RelativeRelation.MULTI_UNLOCKABLE_BY]: RelativeRelation.MULTI_UNLOCKS,
    [RelativeRelation.SCORE_FOR]: RelativeRelation.CALCULATION_OF,
    [RelativeRelation.CALCULATION_OF]: RelativeRelation.SCORE_FOR,
    [RelativeRelation.RADAR_CHART_SOURCE]:
      RelativeRelation.RADAR_CHART_THAT_USES,
    [RelativeRelation.RADAR_CHART_THAT_USES]:
      RelativeRelation.RADAR_CHART_SOURCE,
    [RelativeRelation.PREFILLS]: RelativeRelation.PREFILLED_BY,
    [RelativeRelation.PREFILLED_BY]: RelativeRelation.PREFILLS,
  };
  return reverseRelationTypeMap[relationType];
};

export const relationTypeForRelativeRelation = (
  relationType: RelativeRelation,
) => {
  const relationTypeMap = {
    [RelativeRelation.UNLOCKS]: RelationType.LOCKING,
    [RelativeRelation.MULTI_UNLOCKS]: RelationType.MULTI_LOCKING,
    [RelativeRelation.UNLOCKABLE_BY]: RelationType.LOCKING,
    [RelativeRelation.MULTI_UNLOCKABLE_BY]: RelationType.MULTI_LOCKING,
    [RelativeRelation.SCORE_FOR]: RelationType.CALCULATING,
    [RelativeRelation.CALCULATION_OF]: RelationType.CALCULATING,
    [RelativeRelation.RADAR_CHART_SOURCE]: RelationType.RADAR_CHART,
    [RelativeRelation.RADAR_CHART_THAT_USES]: RelationType.RADAR_CHART,
    [RelativeRelation.PREFILLS]: RelationType.PREFILLING,
    [RelativeRelation.PREFILLED_BY]: RelationType.PREFILLING,
  };
  return relationTypeMap[relationType];
};

export type RelativePosition = "above" | "below";

export interface NodeRelation {
  sourceNode: NodeSummary;
  relationType: RelativeRelation;
  targetNode: NodeSummary;
  relativePosition: RelativePosition;
}
