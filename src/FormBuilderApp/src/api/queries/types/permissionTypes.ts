import type { CategoryNode, QuestionNode } from "@/api/queries/types/formTypes";
import type { Step } from "@/api/queries/types/workflowTypes";
import type { Dictionary } from "@/utility/DictionaryType";
export interface WorkflowPermissions {
  roles: string[];
  steps: Step[];
  rootNode?: PermissionCategoryNode;
}

export interface PermissionCategoryNode
  extends Pick<
    CategoryNode,
    "id" | "position" | "info" | "type" | "contentHash"
  > {
  children: PermissionNode[];
}

export interface PermissionQuestionNode
  extends Pick<
    QuestionNode,
    "id" | "position" | "info" | "type" | "contentHash"
  > {
  flowPermission: FlowPermissionRecord;
}

export type PermissionNode = PermissionCategoryNode | PermissionQuestionNode;

export interface FlowPermissionDetail {
  templateId: number;
  flowPermission: FlowPermissionRecord;
}

export interface FlowPermissionTemplate {
  id: number;
  name: string;
  contentHash: string;
  flowPermission: FlowPermissionRecord;
}

export type FlowPermissionRecord = Dictionary<string, RolePermissionRecord>;

export type RolePermissionRecord = Dictionary<string, RolePermission>;

export interface RolePermission {
  read: boolean;
  write: boolean;
}

export interface SetValueForCellEvent {
  step: string;
  role: string;
  type: "read" | "write" | "reset";
  value: boolean;
}

export interface SetValueForRolesEvent {
  role: string;
  type: "read" | "write" | "reset";
  value: boolean;
}

export interface SetValueForStepsEvent {
  step: string;
  type: "read" | "write" | "reset";
  value: boolean;
}
