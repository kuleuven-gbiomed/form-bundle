import type { FormInfo } from "@/api/queries/types/formTypes";
import { Moment } from "moment/moment";
import { FormTranslations } from "@/api/queries/types/formTypes";

export interface FormWorkFlow {
  id: number;
  stepsType: StepsType;
  contentHash: string;
  steps: Step[];
}

export interface BaseStep {
  id: number;
  uid: string;
  info: FormInfo;
  type?: StepType;
  sequence: number;
  contentHash?: string;
  allowsStartIfPreviousStepIsFulfilled: boolean;
  period: CalculatedPeriod | FixedPeriod;
  rolesWithOneSubmit: string[];
  hideReadOnlyNodesOnLanding: boolean;
  submitSuccessUserInteraction: SubmitSuccessUserInteraction;
}

export interface SubmitSuccessUserInteraction {
  notificationConfig: NotificationConfig;
  roleSubmitSuccessMessages: Array<RoleTranslation>;
  generalSubmitSuccessMessage: FormTranslations;
  redirectAwayOnSubmitSuccess: boolean;
}

export interface RoleTranslation {
  role: string;
  translations: FormTranslations;
}

export interface NotificationConfig {
  mailBodyForStart: FormTranslations;
  mailBodyForOngoing: FormTranslations;
  roleConfigsToNotifyOfStart: Array<RoleConfigsToOffset>;
  roleConfigsToRemindOfOngoing: Array<RoleConfigsToOffset>;
  roleMailBodiesForStart: Array<RoleTranslation>;
  roleMailBodiesForOngoing: Array<RoleTranslation>;
}

export interface RoleConfigsToOffset {
  role: string;
  offset: number;
}

export type Step = RelativeStep | FixedStep;

export type RelativeStep = BaseStep & {
  type: "stepWithCalculatedDateStart";
  period: CalculatedPeriod;
};

export type FixedStep = BaseStep & {
  type: "stepWithFixedDateStart";
  period: FixedPeriod;
};

export function isFixedStep(step: Step): step is FixedStep {
  return step.type === "stepWithFixedDateStart";
}
export function isRelativeStep(step: Step): step is RelativeStep {
  return step.type === "stepWithCalculatedDateStart";
}

export interface CalculatedPeriod {
  amountOfDaysToCalculateEndDate: number;
  amountOfDaysToCalculateStartDate: number;
}

export interface FixedPeriod {
  fixedStartDate: Date;
  fixedEndDate: Date;
}

export type StepsType =
  | "stepsWithFixedDateStart"
  | "stepsWithCalculatedDateStartOrFixedDateStart";
export type StepType = "stepWithCalculatedDateStart" | "stepWithFixedDateStart";

//-- schema rendering info structure -----------------------------------------------
export interface StepNode {
  id: string;
  label: string;
  data: StepNodeData;
  position: StepNodePosition;
  type: "fixed" | "relative" | "add";
  computedPosition: StepNodePosition;
}

export interface StepNodeData {
  id: number;
  sequence: number;
  uuid: string;
  type: "output" | "default" | "input";
  description: string;
  fromLabel: string;
  toLabel: string;
  from: string;
  to: string;
  step: number;
}

export interface StepNodePosition {
  x: number;
  y: number;
}

export interface StepEdge {
  id: string;
  source: string;
  target: string;
  type: string;
  markerEnd: string;
  animated: boolean;
  style: StepEdgeStyle;
}

export interface StepEdgeStyle {
  strokeWidth: number;
}

export interface ValidationStep {
  start: Moment;
  end: Moment;
  startValid: boolean;
  endValid: boolean;
}
