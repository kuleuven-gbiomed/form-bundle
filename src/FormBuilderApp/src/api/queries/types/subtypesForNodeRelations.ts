// subtypes for nodes that have relations to other nodes
import {
  ChoiceQuestionNode,
  NumberQuestionNode,
  FormNode,
  TextQuestionNode,
  DateQuestionNode,
} from "@/api/queries/types/formTypes";

export type CalculationQuestion = NumberQuestionNode | ChoiceQuestionNode;
export function isCalculationQuestion(
  node: FormNode,
): node is CalculationQuestion {
  return (
    (node.subType === "question_number" ||
      node.subType === "question_choice") &&
    node.configScoring.globalScoreCalculationDependencyConfigs.length > 0
  );
}

export type MaybeScoreQuestion = NumberQuestionNode | ScoreChoiceQuestion;
export function isMaybeScoreQuestion(
  node: FormNode,
): node is MaybeScoreQuestion {
  return (
    node.subType === "question_number" ||
    (node.subType === "question_choice" && isScoringChoiceQuestionNode(node))
  );
}

export type ScoreChoiceQuestion = ChoiceQuestionNode & {
  configScoring: {
    scoringParameters: {
      minScore: number;
      maxScore: number;
    };
  };
};
export function isScoringChoiceQuestionNode(
  node: ChoiceQuestionNode,
): node is ScoreChoiceQuestion {
  if (node.configProps.multiple) {
    return false;
  } else {
    if (
      node.configProps.custom_options &&
      typeof node.configProps.custom_options === "object" &&
      "show_scores_in_options" in node.configProps.custom_options
    ) {
      return node.configProps.custom_options.show_scores_in_options;
    }

    return (
      node.configScoring.scoringParameters.minScore !== null &&
      node.configScoring.scoringParameters.maxScore !== null &&
      !node.configProps.multiple
    );
  }
}

// either prefilling or prefilled
export type MaybePrefillQuestion =
  | TextQuestionNode
  | ChoiceQuestionNode
  | NumberQuestionNode
  | DateQuestionNode;

export function isMaybePrefillQuestion(
  node: FormNode,
): node is MaybePrefillQuestion {
  const allowedSubTypes = [
    "question_text",
    "question_choice",
    "question_number",
    "question_date",
  ];
  return allowedSubTypes.includes(node.subType);
}

export type PrefilledQuestion = MaybePrefillQuestion & {
  configPrefill: {
    prefillingQuestionUids: string[];
  };
};

export function isPrefilledQuestion(node: FormNode): node is PrefilledQuestion {
  if (!isMaybePrefillQuestion(node)) return false;
  return node.configPrefill.prefillingQuestionUids.length > 0;
}

export type MaybeRadarChartSource = NumberQuestionNode | ChoiceQuestionNode;
export function isMaybeRadarChartSource(
  node: FormNode,
): node is MaybeRadarChartSource {
  return (
    node.subType === "question_number" || node.subType === "question_choice"
  );
}
