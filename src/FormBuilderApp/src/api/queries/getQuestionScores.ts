import axios from "axios";

export interface ScoreQuestion {
  id: number;
  weight: number;
  question: string;
  uid: string;
  contentHash: string;
  position: number[];
}

export type ScoreQuestions = ScoreQuestion[];

export async function getQuestionScores(
  formId: number,
  formNodeId: number,
  language: string,
) {
  const response = await axios.get<ScoreQuestions>(
    `/fb/${formId}/node/${formNodeId}/scores/${language}`,
  );
  return response.data;
}
