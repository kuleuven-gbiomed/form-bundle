import axios from "axios";
import type { WorkflowPermissions } from "@/api/queries/types/permissionTypes";

export async function getOverviewWorkflowPermissions(formId: number) {
  const response = await axios.get<WorkflowPermissions>(
    `/fb/${formId}/flow-permissions`,
  );
  return response.data;
}
