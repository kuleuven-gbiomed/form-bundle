import axios from "axios";
import type { FlowPermissionDetail } from "@/api/queries/types/permissionTypes";

export async function getNodePermissions(formId: number, nodeId: number) {
  const response = await axios.get<FlowPermissionDetail>(
    `/fb/${formId}/node/${nodeId}/flow-permission`,
  );
  return response.data;
}
