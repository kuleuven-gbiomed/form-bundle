import axios from "axios";
import type { CommandHistoryEntry } from "@/api/queries/types/commandHistory";

export async function getCommandHistory(
  commandType: string,
  subjectId: number,
) {
  const response = await axios.get<CommandHistoryEntry[]>(
    `/fb/command-history/${commandType}/${subjectId}`,
  );
  return response.data;
}
