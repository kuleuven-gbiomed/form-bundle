import axios from "axios";
import type { BasicForm } from "@/api/queries/types/formTypes";

export const ERROR_LOADING_FORM = "error-loading-form";

export async function getFormByUid(uid: string) {
  const response = await axios
    .get<BasicForm>(`/fb/uid/${uid}`)
    .catch(function (error) {
      console.error(error);
    });
  if (response) {
    if (response.data?.id) {
      return response.data;
    } else {
      return ERROR_LOADING_FORM;
    }
  } else {
    return ERROR_LOADING_FORM;
  }
}
