import axios from "axios";
export interface Question {
  id: number;
  uid: string;
  label: string;
  type: "question";
  subtype: string;
  position: number[];
}

export async function getQuestionsByCategory(
  formId: number,
  language: string,
  categoryId: number,
) {
  const response = await axios.get<Question[]>(
    `/fb/${formId}/questions/${language}`,
    {
      params: { parent: categoryId },
    },
  );
  return response.data;
}

export async function getLockedQuestions(
  formId: number,
  language: string,
  unlocksQuestionIds: string[],
) {
  const response = await axios.get<Question[]>(
    `/fb/${formId}/questions/${language}`,
    {
      params: { uids: unlocksQuestionIds.join() },
    },
  );
  return response.data;
}
