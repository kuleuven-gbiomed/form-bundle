import axios from "axios";
import type { TranslatedCommandHistoryEntry } from "@/api/queries/types/commandHistory";

export async function getTranslatedCommandHistorySinceLastPublish(
  formId: number,
  language: string,
) {
  const response = await axios.get<TranslatedCommandHistoryEntry[]>(
    `/fb/${formId}/command-history/${language}/since-last-publish`,
  );
  return response.data;
}
