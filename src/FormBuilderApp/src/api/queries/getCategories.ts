import axios from "axios";

export interface Category {
  id: number;
  label: string;
  uid: string;
}

export async function getCategories(formId: number, language: string) {
  const response = await axios.get<Category[]>(
    `/fb/${formId}/categories/${language}`,
  );
  return response.data;
}
