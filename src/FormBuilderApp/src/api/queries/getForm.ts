import axios from "axios";
import type { Form } from "@/api/queries/types/formTypes";

export interface FormListFilter {
  role: string;
  stepUid: string;
}

export async function getForm(id: number, filter?: FormListFilter) {
  const url =
    filter == undefined
      ? `/fb/${id}`
      : `/fb/${id}/step/${filter.stepUid}/role/${filter.role}`;
  const response = await axios.get<Form>(url);
  return response.data;
}
