import axios from "axios";
import type { NodeRelation } from "@/api/queries/types/formTypes";

export async function getNodeRelations(
  formId: number,
  nodeId: number,
  language: string,
) {
  const response = await axios.get<NodeRelation[]>(
    `/fb/${formId}/node/${nodeId}/relations/${language}`,
  );
  return response.data;
}
