import axios from "axios";
import { ChartParticipantQuestion } from "./types/formTypes";

export async function getQuestionRadarChartParticipants(
  formId: number,
  formNodeId: number,
) {
  const response = await axios.get<ChartParticipantQuestion[]>(
    `/fb/${formId}/node/${formNodeId}/radar-chart`,
  );
  return response.data;
}
