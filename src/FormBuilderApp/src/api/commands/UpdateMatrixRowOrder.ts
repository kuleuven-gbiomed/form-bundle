import { Ref, computed, ref } from "vue";
import { MatrixCategoryNode } from "./../queries/types/formTypes";
import { UpdateCommand } from "./common/_commandTypes";
import { executeApiCommand } from "./common/executeApiCommand";
import { useLanguageStore } from "@/stores/language";

export type MatrixRowOrder = number[];
interface MatrixRowOrderPayload {
  rowOrder: MatrixRowOrder;
}
export interface UpdateMatrixRowOrderCommand
  extends UpdateCommand<MatrixRowOrderPayload> {
  type: "updateMatrixRowOrder";
  subject: "node";
}

export interface MatrixRowInfo {
  id: number;
  label: string;
}

export function useUpdateMatrixRowOrderCommand(
  matrixNode: Ref<MatrixCategoryNode>,
) {
  const languageStore = useLanguageStore();

  const currentRowOrder = computed<MatrixRowOrder>(() =>
    matrixNode.value.children.map((child) => child.id),
  );

  const matrixRows = ref<MatrixRowInfo[]>(
    matrixNode.value.children.map((row) => ({
      id: row.id,
      label: row.info.label[languageStore.activeLanguage.toLowerCase()] ?? "",
    })),
  );

  const newRowOrder = computed<MatrixRowOrder>(() =>
    matrixRows.value.map((row) => row.id),
  );

  const command = computed<UpdateMatrixRowOrderCommand>(() => ({
    type: "updateMatrixRowOrder",
    id: matrixNode.value.id,
    contentHash: matrixNode.value.contentHash,
    subject: "node",
    changeFrom: {
      rowOrder: currentRowOrder.value,
    },
    changeTo: {
      rowOrder: newRowOrder.value,
    },
  }));

  const changed = computed<boolean>(
    () =>
      JSON.stringify(currentRowOrder.value) !==
      JSON.stringify(newRowOrder.value),
  );

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    changed,
    items: matrixRows,
    command,
    executeCommand,
  };
}
