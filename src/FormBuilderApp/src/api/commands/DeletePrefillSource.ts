import type { DeleteCommand } from "@/api/commands/common/_commandTypes";
import { useDeleteCommand } from "./common/useDeleteCommand";
import { Ref, computed } from "vue";
import { useLanguageStore } from "@/stores/language";

export interface PrefillSourceToDelete {
  sourceId: number;
}

export interface DeletePrefillSourceCommand
  extends DeleteCommand<PrefillSourceToDelete> {
  language: string;
  type: "deletePrefillSource";
  subject: "node";
}

const languageStore = useLanguageStore();
export function useDeletePrefillSourceCommand(
  prefillSourceToDelete: Ref<PrefillSourceToDelete>,
  questionId: number,
  questionContentHash: Ref<string>,
) {
  return useDeleteCommand<DeletePrefillSourceCommand, PrefillSourceToDelete>(
    "deletePrefillSource",
    questionId,
    "node",
    questionContentHash,
    prefillSourceToDelete,
    computed(() => languageStore.activeLanguage),
  );
}
