import type { PayloadCommand, UpdateCommand } from "./common/_commandTypes";
import type {
  FlowPermissionRecord,
  PermissionNode,
  FlowPermissionTemplate,
} from "@/api/queries/types/permissionTypes";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/core";
import { useUpdateCommand } from "./common/useUpdateCommand";
import {
  CategoryNode,
  FormNode,
  QuestionNode,
} from "@/api/queries/types/formTypes";
import { usePayloadCommand } from "./common/usePayloadCommand";
import { useFormStore, findNodeById } from "@/stores/form";
import { getInfoForQuestions } from "@/functions/getInfoForQuestions";

interface PermissionDetail {
  flowPermission: FlowPermissionRecord;
  templateId: number;
}

export interface UpdatePermissionsCommand
  extends UpdateCommand<PermissionDetail> {
  id: number;
  type: "updatePermissions";
}

export function useUpdatePermissionCommand(
  node: Ref<FormNode | PermissionNode>,
  permissionDetail: Ref<PermissionDetail>,
) {
  const { newPayload, command, executeCommand } = useUpdateCommand<
    UpdatePermissionsCommand,
    PermissionDetail
  >(
    "updatePermissions",
    node.value.id,
    "node",
    computed(() => node.value.contentHash),
    permissionDetail,
  );

  const { flowPermission, templateId } = toRefs(newPayload);

  return {
    flowPermission,
    templateId,
    command,

    executeCommand,
  };
}

type NodeContentHash = Pick<QuestionNode, "id" | "contentHash">;
interface CategoryPermissionPayload {
  flowPermission: FlowPermissionRecord;
  templateId: number;
  nodeContentHashes: NodeContentHash[];
}

export interface ApplyPermissionToCategoryCommand
  extends PayloadCommand<CategoryPermissionPayload> {
  type: "applyPermissionsToCategory";
  subject: "node";
}
const formStore = useFormStore();
export function useApplyPermissionToCategory(
  permissionTemplate: Ref<FlowPermissionTemplate>,
  category: Ref<CategoryNode>,
) {
  const fullCategory = computed(() =>
    findNodeById(category.value.id, formStore.form.rootNode),
  );
  const payload = computed(() => ({
    flowPermission: permissionTemplate.value.flowPermission,
    templateId: permissionTemplate.value.id,
    nodeContentHashes: fullCategory.value
      ? getInfoForQuestions(
          fullCategory.value,
          (node): NodeContentHash => ({
            id: node.id,
            contentHash: node.contentHash,
          }),
        )
      : [],
  }));

  const { command, executeCommand } = usePayloadCommand<
    ApplyPermissionToCategoryCommand,
    CategoryPermissionPayload
  >(
    "applyPermissionsToCategory",
    category.value.id,
    "node",
    computed(() => category.value.contentHash),
    payload,
  );
  return {
    permissionTemplate,
    command,
    executeCommand,
  };
}
