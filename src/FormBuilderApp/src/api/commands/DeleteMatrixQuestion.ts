import { MatrixCategoryNode } from "@/api/queries/types/formTypes";
import type { Command } from "@/api/commands/common/_commandTypes.ts";
import { Ref, computed } from "vue";
import { executeApiCommand } from "./common/executeApiCommand";

export interface DeleteMatrixQuestionCommand extends Command {
  type: "deleteMatrixQuestion";
  subject: "node";
  colIndex: number;
}

export function useDeleteMatrixQuestionCommand(
  matrix: Ref<MatrixCategoryNode>,
  colIndex: Ref<number>,
) {
  const command = computed(
    (): DeleteMatrixQuestionCommand => ({
      id: matrix.value.id,
      type: "deleteMatrixQuestion",
      contentHash: matrix.value.contentHash ?? "",
      subject: "node",
      colIndex: colIndex.value,
    }),
  );

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    command,
    executeCommand,
  };
}
