import { RadarChartNode } from "@/api/queries/types/formTypes";
import type { UpdateCommand } from "./common/_commandTypes";
import { useLanguageStore } from "@/stores/language";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/shared";
import { useUpdateCommand } from "./common/useUpdateCommand";

export interface RadarChartProperties {
  hidePointValues: boolean;
  hideScaleValues: boolean;
  hidePointUnitSuffix: boolean;
  hideScaleUnitSuffix: boolean;
  unitSign: string;
}

export interface UpdateRadarChartPropertiesCommand
  extends UpdateCommand<RadarChartProperties> {
  subject: "node";
  type: "updateRadarChartProperties";
  language: string;
}

const languageStore = useLanguageStore();

export function useUpdateRadarChartPropertiesCommand(
  question: Ref<RadarChartNode>,
) {
  const currentState = computed<RadarChartProperties>(() => ({
    hidePointValues: question.value.configProps.hide_point_values,
    hideScaleValues: question.value.configProps.hide_scale_values,
    hideScaleUnitSuffix: question.value.configProps.hide_scale_unit_suffix,
    hidePointUnitSuffix: question.value.configProps.hide_point_unit_suffix,
    unitSign: question.value.configProps.unit_sign,
  }));

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateRadarChartPropertiesCommand,
    RadarChartProperties
  >(
    "updateRadarChartProperties",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  const {
    hidePointValues,
    hideScaleValues,
    hidePointUnitSuffix,
    hideScaleUnitSuffix,
    unitSign,
  } = toRefs(newPayload);

  return {
    newPayload,

    hidePointValues,
    hideScaleValues,
    hideScaleUnitSuffix,
    hidePointUnitSuffix,
    unitSign,

    command,
    changed,
    executeCommand,
  };
}
