import { QuestionNode } from "@/api/queries/types/formTypes";
import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { Ref, computed, ref, toRaw, watch } from "vue";
import { toRefs } from "@vueuse/core";
import { useLanguageStore } from "@/stores/language";
import { getNodeByPosition, useFormStore } from "@/stores/form";
import { executeApiCommand } from "./common/executeApiCommand";

interface MatrixQuestionInfo {
  question: string;
}
export interface UpdateMatrixQuestionCommand
  extends UpdateCommand<MatrixQuestionInfo> {
  type: "updateMatrixQuestion";
  language: string;
  subject: "node";
  colIndex: number;
}

const languageStore = useLanguageStore();
const formStore = useFormStore();

export function useUpdateMatrixQuestionCommand(
  questionNode: Ref<QuestionNode>,
) {
  const payload = computed<MatrixQuestionInfo>(() => ({
    question:
      questionNode.value.info.label[
        languageStore.activeLanguage.toLowerCase()
      ] ?? "",
  }));

  const newPayload: Ref<MatrixQuestionInfo> = ref(
    // structuredClone();
    JSON.parse(JSON.stringify(toRaw(payload.value))),
  ) as Ref<MatrixQuestionInfo>;

  watch(payload, () => {
    try {
      newPayload.value = structuredClone(toRaw(payload.value));
    } catch (error) {
      console.error(error);
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      newPayload.value = JSON.parse(JSON.stringify(payload.value));
    }
  });

  const matrixQuestion = getNodeByPosition(
    questionNode.value.position.slice(0, -2),
    formStore.form.rootNode,
  );

  const command = computed<UpdateMatrixQuestionCommand>(() => ({
    type: "updateMatrixQuestion",
    id: matrixQuestion.id,
    contentHash: matrixQuestion.contentHash,
    colIndex:
      questionNode.value.position[questionNode.value.position.length - 1],
    subject: "node",
    changeFrom: payload.value,
    changeTo: newPayload.value,
    language: languageStore.activeLanguage,
  }));

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  const { question } = toRefs(newPayload);

  return {
    question,
    command,
    executeCommand,
  };
}
