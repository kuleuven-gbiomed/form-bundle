import { Ref, computed, ref, watch } from "vue";
import { MatrixCategoryNode } from "./../queries/types/formTypes";
import { UpdateCommand } from "./common/_commandTypes";
import { executeApiCommand } from "./common/executeApiCommand";
import { useLanguageStore } from "@/stores/language";

export type MatrixColOrder = MatrixColInfo[];
interface MatrixColOrderPayload {
  colOrder: MatrixColInfo[];
}
export interface UpdateMatrixColOrderCommand
  extends UpdateCommand<MatrixColOrderPayload> {
  type: "updateMatrixColOrder";
  subject: "node";
}

export interface MatrixColInfo {
  currentColIndex: number;
  label: string;
}

export function useUpdateMatrixColOrderCommand(
  matrixNode: Ref<MatrixCategoryNode>,
) {
  const languageStore = useLanguageStore();

  const colOrder = () =>
    matrixNode.value.children[0].children.map((col, index) => ({
      currentColIndex: index,
      label: col.info.label[languageStore.activeLanguage.toLowerCase()] ?? "",
    }));

  const currentColOrder = computed<MatrixColOrder>(() => colOrder());

  const newColOrder = ref<MatrixColOrder>(colOrder());

  watch(currentColOrder, () => {
    newColOrder.value = colOrder();
  });

  const command = computed<UpdateMatrixColOrderCommand>(() => ({
    type: "updateMatrixColOrder",
    id: matrixNode.value.id,
    contentHash: matrixNode.value.contentHash,
    subject: "node",
    changeFrom: {
      colOrder: currentColOrder.value,
    },
    changeTo: {
      colOrder: newColOrder.value,
    },
  }));

  const changed = computed<boolean>(
    () =>
      JSON.stringify(currentColOrder.value) !==
      JSON.stringify(newColOrder.value),
  );

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    changed,
    items: newColOrder,
    command,
    executeCommand,
  };
}
