import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { RadarChartNode } from "@/api/queries/types/formTypes";
import { Ref, computed, ref, toRaw, watch } from "vue";
import { useLanguageStore } from "@/stores/language";
import { executeApiCommand } from "./common/executeApiCommand";

export interface ChartRelationOrder {
  order: string[];
}

export interface OrderChartRelationCommand
  extends UpdateCommand<ChartRelationOrder> {
  type: "orderChartRelation";
  language: string;
  subject: "node";
}

const languageStore = useLanguageStore();

function getOrder(node: RadarChartNode) {
  return node.chartParticipantQuestions.map((q) => q.uid);
}

export function useOrderChartRelationCommand(
  radarChartNode: Ref<RadarChartNode>,
) {
  const newRadarChartNode: Ref<RadarChartNode> = ref(
    structuredClone(toRaw(radarChartNode.value)),
  );
  const currentState = computed(
    (): ChartRelationOrder => ({
      order: getOrder(radarChartNode.value),
    }),
  );
  const newState = computed(
    (): ChartRelationOrder => ({
      order: getOrder(newRadarChartNode.value),
    }),
  );
  const command = computed(
    (): OrderChartRelationCommand => ({
      id: radarChartNode.value.id,
      type: "orderChartRelation",
      contentHash: radarChartNode.value.contentHash,
      subject: "node",
      language: languageStore.activeLanguage,
      changeFrom: currentState.value,
      changeTo: newState.value,
    }),
  );

  watch(radarChartNode, () => {
    // you have to explicitly watch here
    // https://vuejs.org/guide/reusability/composables.html#input-arguments
    newRadarChartNode.value = structuredClone(toRaw(radarChartNode.value));
  });

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    newRadarChartNode,
    command,

    executeCommand,
  };
}
