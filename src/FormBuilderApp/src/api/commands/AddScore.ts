import { findNodeById } from "@/stores/form";
import { useLanguageStore } from "@/stores/language.ts";
import type { AddCommand } from "./common/_commandTypes.ts";
import { Ref, computed, ref } from "vue";
import { executeApiCommand } from "./common/executeApiCommand";
import { useFormStore } from "@/stores/form";

export interface ScoreRelation {
  scoreQuestionId: number;
  scoreCalculationId: number;
  weight: number;
}

export interface AddScoreCommand extends AddCommand<ScoreRelation> {
  type: "addScore";
  language: string;
  subject: "node";
}

const languageStore = useLanguageStore();
export function useAddScoreCommand(
  calculationQuestionId: Ref<number>,
  scoreQuestionId: Ref<number>,
) {
  const formStore = useFormStore();
  const calculationQuestionContentHash = computed(() => {
    const q = findNodeById(
      calculationQuestionId.value,
      formStore.form.rootNode,
    );
    return q?.contentHash ?? "";
  });

  const weight = ref(1);

  const command = computed(
    (): AddScoreCommand => ({
      id: calculationQuestionId.value,
      type: "addScore",
      contentHash: calculationQuestionContentHash.value,
      subject: "node",
      language: languageStore.activeLanguage,
      add: {
        scoreQuestionId: scoreQuestionId.value,
        scoreCalculationId: calculationQuestionId.value,
        weight: weight.value,
      },
    }),
  );

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    weight,
    command,
    executeCommand,
  };
}
