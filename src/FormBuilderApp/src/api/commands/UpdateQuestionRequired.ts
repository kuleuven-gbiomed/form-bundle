import { QuestionNode } from "../queries/types/formTypes";
import type { UpdateCommand } from "./common/_commandTypes";
import { executeApiCommand } from "./common/executeApiCommand";
import { Ref, computed } from "vue";
// remote command: KUL\FormBundle\FormBuilder\Commands\

export interface UpdateQuestionRequiredCommand
  extends UpdateCommand<{ required: boolean }> {
  id: number;
  subject: "node";
  type: "updateQuestionRequired";
}

export function useUpdateQuestionRequiredCommand(node: Ref<QuestionNode>) {
  const command = computed<UpdateQuestionRequiredCommand>(() => ({
    type: "updateQuestionRequired",
    id: node.value.id,
    contentHash: node.value.contentHash,
    subject: "node",
    changeFrom: { required: node.value.required },
    changeTo: { required: !node.value.required },
  }));

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    command,
    executeCommand,
  };
}
