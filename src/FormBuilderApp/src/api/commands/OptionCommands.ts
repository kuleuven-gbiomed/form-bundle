import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { AddCommand, DeleteCommand } from "@/api/commands/common/_commandTypes";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand";
import { computed, ref, Ref } from "vue";
import { toRefs } from "@vueuse/core";
import { useLanguageStore } from "@/stores/language";
import { OptionNode, QuestionNode } from "@/api/queries/types/formTypes";
import { useAddCommand } from "./common/useAddCommand";

export interface Option {
  option: string;
  score: number | null;
  defaultSelected: boolean;
  inputType: string;
  additionalTextRequired: boolean | null;
  additionalTextMinLength: number | null;
  additionalTextMaxLength: number | null;
}

type OptionToAdd = Pick<Option, "option" | "score">;

export interface AddOptionCommand extends AddCommand<OptionToAdd> {
  type: "addOption";
  contentHash: string; // contentHash van de parent (node)
  language: string;
  subject: "node";
  id: number; // id van de parent (node)
}

export interface UpdateOptionCommand extends UpdateCommand<Option> {
  subject: "option";
  type: "updateOption";
  language: string;
}

export interface OptionToDelete {
  optionId: number;
  formId: number;
}

export interface DeleteOptionCommand extends DeleteCommand<OptionToDelete> {
  type: "deleteOption";
  subject: "option";
  language: string;
}

const languageStore = useLanguageStore();

export function useAddOptionCommand(question: Ref<QuestionNode>) {
  const option = ref("");
  const score = ref<number | null>(null);

  const { command, executeCommand } = useAddCommand<
    AddOptionCommand,
    OptionToAdd
  >(
    "addOption",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    computed(() => ({
      option: option.value,
      score: score.value, // === null ? null : parseFloat(score.value),
    })),
    computed(() => languageStore.activeLanguage),
  );

  return {
    option,
    score,

    command,
    executeCommand,
  };
}

export function useUpdateOptionCommand(optionNode: Ref<OptionNode>) {
  const currentState = computed<Option>(() => ({
    option: decodeHtmlEntities(
      optionNode.value.info.label[languageStore.activeLanguage.toLowerCase()] ??
        "",
    ),
    score: optionNode.value.score,
    defaultSelected: optionNode.value.defaultSelected,
    inputType: optionNode.value.inputType,
    additionalTextRequired:
      optionNode.value.configProps.additionalTextRequired ?? false,
    additionalTextMinLength:
      optionNode.value.configProps.additionalTextMinLength ?? 0,
    additionalTextMaxLength:
      optionNode.value.configProps.additionalTextMaxLength ?? 100,
  }));

  function decodeHtmlEntities(text: string): string {
    const textArea = document.createElement("textarea");
    textArea.innerHTML = text;
    return textArea.value;
  }

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateOptionCommand,
    Option
  >(
    "updateOption",
    optionNode.value.id,
    "option",
    computed(() => optionNode.value.contentHash),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  const {
    option,
    score,
    defaultSelected,
    inputType,
    additionalTextRequired,
    additionalTextMinLength,
    additionalTextMaxLength,
  } = toRefs(newPayload);

  return {
    newPayload,

    option,
    score,
    defaultSelected,
    inputType,
    additionalTextRequired,
    additionalTextMinLength,
    additionalTextMaxLength,

    command,
    changed,
    executeCommand,
  };
}
