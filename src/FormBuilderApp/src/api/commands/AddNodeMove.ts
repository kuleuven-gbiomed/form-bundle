import { useDragAndDropStore } from "./../../stores/dragAndDropStore";
import type { AddCommand } from "@/api/commands/common/_commandTypes.ts";
import type { SourceNodeInfo } from "@/api/commands/AddNodeCopy.ts";
import { useAddAndCopyStore } from "@/stores/addAndCopy.ts";
import { useLanguageStore } from "@/stores/language.ts";
import { computed } from "vue";
import { executeApiCommand } from "./common/executeApiCommand";

export interface AddNodeMoveCommand extends AddCommand<SourceNodeInfo> {
  type: "addNodeMove";
  contentHash: string; // contentHash target node
  language: string;
  subject: "node";
}
const addAndCopyStore = useAddAndCopyStore();
const dragAndDropStore = useDragAndDropStore();
const languageStore = useLanguageStore();

export function useAddNodeMoveCommand() {
  const sourceNodeState = computed<SourceNodeInfo>(() => ({
    targetDirection: addAndCopyStore.insertDirection,
    sourceNodeContentHash: addAndCopyStore.moveSelectedNode?.contentHash ?? "",
    sourceNodeId: addAndCopyStore.moveSelectedNode?.id ?? 0,
    permissionTemplateId: 0,
    label: "",
  }));

  const command = computed(
    (): AddNodeMoveCommand => ({
      type: "addNodeMove",
      id: dragAndDropStore.droppedSelectedNode?.id ?? 0,
      language: languageStore.activeLanguage,
      contentHash: dragAndDropStore.droppedSelectedNode?.contentHash ?? "",
      subject: "node",
      add: sourceNodeState.value,
    }),
  );
  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    command,
    executeCommand,
  };
}
