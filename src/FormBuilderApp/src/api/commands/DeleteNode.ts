import { computed } from "vue";
import { useFormStore } from "@/stores/form.ts";
import type { DeleteCommand } from "./common/_commandTypes.ts";
import { executeApiCommand } from "./common/executeApiCommand";

export interface NodeToDelete {
  nodeId: number;
  formId: number;
}

export interface DeleteNodeCommand extends DeleteCommand<NodeToDelete> {
  type: "deleteNode";
  subject: "node";
}

const formStore = useFormStore();

export function useDeleteNodeCommand() {
  const command = computed<DeleteNodeCommand>(() => ({
    type: "deleteNode",
    id: formStore.menuSelectedNode?.id ?? 0,
    contentHash: formStore.menuSelectedNode?.contentHash ?? "",
    subject: "node",
    delete: {
      nodeId: formStore.menuSelectedNode?.id ?? 0,
      formId: formStore.form.id,
    },
  }));

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    command,
    executeCommand,
  };
}
