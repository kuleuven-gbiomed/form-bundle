import { Ref, computed } from "vue";
import type { UpdateCommand } from "./common/_commandTypes";
import type { FormNode } from "@/api/queries/types/formTypes";
import { executeApiCommand } from "./common/executeApiCommand";

export interface Hidden {
  hidden: boolean;
}
export interface UpdateDisplayCategoryCommand extends UpdateCommand<Hidden> {
  id: number;
  subject: "node";
  type: "updateDisplayCategory";
}
export interface UpdateDisplayQuestionCommand extends UpdateCommand<Hidden> {
  id: number;
  subject: "node";
  type: "updateDisplayQuestion";
}
type UpdateDisplayCommand =
  | UpdateDisplayQuestionCommand
  | UpdateDisplayCategoryCommand;

export function useUpdateDisplayCommand(node: Ref<FormNode>) {
  const command = computed<UpdateDisplayCommand>(() => ({
    type:
      node.value.type === "category"
        ? "updateDisplayCategory"
        : "updateDisplayQuestion",
    id: node.value.id,
    contentHash: node.value.contentHash,
    subject: "node",
    changeFrom: { hidden: node.value.hidden },
    changeTo: { hidden: !node.value.hidden },
  }));

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    command,
    executeCommand,
  };
}
