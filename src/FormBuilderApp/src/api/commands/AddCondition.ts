import { QuestionNode } from "@/api/queries/types/formTypes.ts";
import type { AddCommand } from "./common/_commandTypes.ts";
import { Ref, computed } from "vue";
import { executeApiCommand } from "./common/executeApiCommand";

export interface ConditionToAdd {
  lockedId: number;
  unlockerId: number;
  optionId: number;
}

export interface AddConditionCommand extends AddCommand<ConditionToAdd> {
  type: "addCondition";
  subject: "node";
}

export function useAddConditionCommand(
  question: Ref<QuestionNode>,
  targetType: "locked" | "unlocker", // we can apply this command from the locked or unlocker side
  targetId: Ref<number>,
  optionId: Ref<number>,
) {
  const conditionPayload = computed<ConditionToAdd>(() => ({
    lockedId: targetType === "locked" ? targetId.value : question.value.id,
    unlockerId: targetType === "unlocker" ? targetId.value : question.value.id,
    optionId: optionId.value,
  }));

  const command = computed<AddConditionCommand>(() => ({
    type: "addCondition",
    id: question.value.id,
    subject: "node",
    contentHash: question.value.contentHash,
    add: conditionPayload.value,
  }));

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return { command, executeCommand };
}
