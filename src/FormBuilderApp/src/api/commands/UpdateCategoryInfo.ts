import { CategoryNode } from "../queries/types/formTypes";
import type { UpdateCommand } from "./common/_commandTypes";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/core";
import { useUpdateCommand } from "./common/useUpdateCommand";
import { useLanguageStore } from "@/stores/language";

interface CategoryInfo {
  title: string;
  description: string;
  displayMode: string;
  firstHeaderCellTitle: string;
}

export interface UpdateCategoryInfoCommand extends UpdateCommand<CategoryInfo> {
  type: "updateCategoryInfo";
  language: string;
  subject: "node";
}
const languageStore = useLanguageStore();

export function useUpdateCategoryInfoCommand(category: Ref<CategoryNode>) {
  const currentState = computed(() => ({
    title:
      category.value.info.label[languageStore.activeLanguage.toLowerCase()] ??
      "",
    description:
      category.value.info.description[
        languageStore.activeLanguage.toLowerCase()
      ] ?? "",
    displayMode: category.value.displayMode,
    firstHeaderCellTitle:
      category.value.firstHeaderCellTitle[
        languageStore.activeLanguage.toLowerCase()
      ] ?? "",
  }));

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateCategoryInfoCommand,
    CategoryInfo
  >(
    "updateCategoryInfo",
    category.value.id,
    "node",
    computed(() => category.value.contentHash),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  const { title, description, displayMode, firstHeaderCellTitle } =
    toRefs(newPayload);

  return {
    newPayload,

    title,
    description,
    displayMode,
    firstHeaderCellTitle,

    command,
    changed,
    executeCommand,
  };
}
