import { QuestionNode } from "@/api/queries/types/formTypes";
import type { DescriptionDisplayMode } from "@/api/queries/types/formTypes";
import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/core";
import { useLanguageStore } from "@/stores/language";

interface QuestionInfo {
  question: string;
  description: string;
  descriptionDisplayMode: DescriptionDisplayMode;
  placeholder: string;
}
export interface UpdateQuestionInfoCommand extends UpdateCommand<QuestionInfo> {
  type: "updateQuestionInfo";
  language: string;
  subject: "node";
}

const languageStore = useLanguageStore();

export function useUpdateQuestionInfoCommand(questionNode: Ref<QuestionNode>) {
  const currentState = computed<QuestionInfo>(() => ({
    question:
      questionNode.value.info.label[
        languageStore.activeLanguage.toLowerCase()
      ] ?? "",
    description:
      questionNode.value.info.description[
        languageStore.activeLanguage.toLowerCase()
      ] ?? "",
    descriptionDisplayMode:
      questionNode.value.configInfo.descriptionDisplayMode,
    placeholder:
      questionNode.value.configInfo.placeholder[
        languageStore.activeLanguage.toLowerCase()
      ] ?? "",
  }));

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateQuestionInfoCommand,
    QuestionInfo
  >(
    "updateQuestionInfo",
    questionNode.value.id,
    "node",
    computed(() => questionNode.value.contentHash),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  const { question, description, descriptionDisplayMode, placeholder } =
    toRefs(newPayload);

  return {
    newPayload,

    question,
    description,
    descriptionDisplayMode,
    placeholder,

    command,
    changed,
    executeCommand,
  };
}
