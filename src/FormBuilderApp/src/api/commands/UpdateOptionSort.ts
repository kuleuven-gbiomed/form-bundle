import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/core";
import { ChoiceQuestionNode } from "@/api/queries/types/formTypes";
import { useLanguageStore } from "@/stores/language";

export interface SortOption {
  id: number;
  label: string;
  contentHash: string;
  hasTextField: boolean;
}
export interface OptionSortData {
  options: SortOption[];
}
export interface UpdateOptionSortCommand extends UpdateCommand<OptionSortData> {
  id: number;
  type: "updateOptionSort";
  subject: "node";
}

const languageStore = useLanguageStore();
export function useUpdateUpdateOptionSortCommand(
  question: Ref<ChoiceQuestionNode>,
) {
  const sortOptions = computed(() =>
    question.value.options.map(
      (option): SortOption => ({
        id: option.id,
        label:
          option.info.label[languageStore.activeLanguage.toLowerCase()] ??
          "[Option Label]",
        contentHash: option.contentHash,
        hasTextField: option.inputType === "optionWithAdditionalText",
      }),
    ),
  );
  const currentState = computed(
    (): OptionSortData => ({ options: sortOptions.value }),
  );

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateOptionSortCommand,
    OptionSortData
  >(
    "updateOptionSort",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  const { options } = toRefs(newPayload);

  return {
    newPayload,
    options,
    command,
    changed,
    executeCommand,
  };
}
