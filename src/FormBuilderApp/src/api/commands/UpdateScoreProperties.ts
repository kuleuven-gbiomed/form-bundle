import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/core";
import { useLanguageStore } from "@/stores/language";
import { ChoiceQuestionNode } from "@/api/queries/types/formTypes";

export interface ScorePropertiesPayload {
  roundingMode: number | null;
  scale: number | null;
  minScore: number | null;
  maxScore: number | null;
}

export interface UpdateScoreProperties
  extends UpdateCommand<ScorePropertiesPayload> {
  type: "updateScoreProperties";
  language: string;
  subject: "node";
}

const languageStore = useLanguageStore();

export function useUpdateScoreProperties(question: Ref<ChoiceQuestionNode>) {
  const currentState = computed<ScorePropertiesPayload>(() => ({
    roundingMode:
      question.value.configScoring.scoringParameters.roundingMode ?? 1,
    scale: question.value.configScoring.scoringParameters.scale ?? 0,
    minScore: question.value.configScoring.scoringParameters.minScore ?? 0,
    maxScore: question.value.configScoring.scoringParameters.maxScore ?? 0,
  }));

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateScoreProperties,
    ScorePropertiesPayload
  >(
    "updateScoreProperties",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  const { roundingMode, scale, minScore, maxScore } = toRefs(newPayload);

  return {
    newPayload,

    roundingMode,
    scale,
    minScore,
    maxScore,

    command,
    changed,
    executeCommand,
  };
}
