import { useLanguageStore } from "@/stores/language.ts";
import {
  Form,
  FormInfoRoleDescriptions,
  FormTranslations,
  RoleSummary,
} from "@/api/queries/types/formTypes.ts";
import { UpdateCommand } from "@/api/commands/common/_commandTypes.ts";
import { computed, Ref } from "vue";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand.ts";
import { renderRoleLabel } from "@/functions/roles.ts";
import { EditorConfig } from "@/components/formBuilder/formGeneral/EditorConfiguration.ts";

const languageStore = useLanguageStore();

export interface RoleDescriptions {
  subject: string;
  descriptions: Array<RoleDescription>;
}

export interface RoleDescription {
  roles: Array<RoleSummary>;
  translations: Array<RoleDescriptionTranslation>;
}

export interface RoleDescriptionTranslation {
  language: string;
  description: string;
}

export interface UpdateRoleDescriptions
  extends UpdateCommand<RoleDescriptions> {
  id: number;
  type: "updateInfoRoleDescriptions";
  contentHash: string;
  subject: "form";
  language: string;
}

export function useInfoUpdateRoleDescriptions(
  form: Ref<Form>,
  config: Ref<EditorConfig>,
) {
  const currentState = computed((): RoleDescriptions => {
    return {
      subject: config.value.subject,
      descriptions: convertFormBundleStructureToFormBuildAppStructure(form),
    };
  });

  const { newPayload, command, changed, executeCommand } = useUpdateCommand<
    UpdateRoleDescriptions,
    RoleDescriptions
  >(
    "updateInfoRoleDescriptions",
    form.value.id,
    "form",
    computed(() => form.value.contentHash ?? ""),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  return {
    payload: newPayload,
    command,
    changed,
    executeCommand,
  };
}

function convertFormBundleStructureToFormBuildAppStructure(
  form: Ref<Form>,
): Array<RoleDescription> {
  const _result: Array<RoleDescription> = [];
  const _descriptions: Array<FormInfoRoleDescriptions> =
    form.value.info.roleDescriptions;

  // -- step 1: make the descriptions unique (combining the language keys) as a Json encoded string
  const _translationStringDescriptions: Array<string> = [];
  for (const _description of _descriptions) {
    const _translationString = JSON.stringify(_description.translations);
    _translationStringDescriptions.push(_translationString);
  }
  const _differentDescriptions = [...new Set(_translationStringDescriptions)];

  // -- step 2: loop over the unique messages and set roles and messages (combined)
  for (const _descriptionString of _differentDescriptions) {
    const _descriptionObject: FormTranslations = JSON.parse(
      _descriptionString,
    ) as FormTranslations;
    const _resultTranslations: Array<RoleDescriptionTranslation> = [];

    for (const languageKey in _descriptionObject) {
      const _resultTranslation: RoleDescriptionTranslation = {
        language: languageKey,
        description: _descriptionObject[languageKey] ?? "",
      };
      _resultTranslations.push(_resultTranslation);
    }

    const _roleDescription: RoleDescription = {
      roles: [],
      translations: _resultTranslations,
    };

    for (const _description of _descriptions) {
      const _translationString = JSON.stringify(_description.translations);
      if (_translationString === _descriptionString) {
        const _role: RoleSummary = {
          role: _description.role,
          label: renderRoleLabel(_description.role),
        };
        _roleDescription.roles.push(_role);
      }
    }

    _result.push(_roleDescription);
  }

  return _result;
}
