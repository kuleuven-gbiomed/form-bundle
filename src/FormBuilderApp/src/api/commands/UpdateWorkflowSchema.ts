import type { UpdateCommand } from "./common/_commandTypes.ts";
import { useFormStore } from "@/stores/form";
import { useLanguageStore } from "@/stores/language";
import { computed } from "vue";
import { useWorkflowStore } from "@/stores/workflow";
import { executeApiCommand } from "./common/executeApiCommand";

export interface SchemaConfiguration {
  config: StepNodeCoordinate[];
}

export interface StepNodeCoordinate {
  id: string;
  x: number;
  y: number;
}

export interface UpdateWorkflowSchema
  extends UpdateCommand<SchemaConfiguration> {
  id: number;
  type: "updateWorkflowSchema";
  contentHash: string;
  subject: "workflow";
}

export function useUpdateWorkflowSchemaCommand() {
  const formStore = useFormStore();
  const languageStore = useLanguageStore();
  const workflowStore = useWorkflowStore();

  const command = computed<UpdateWorkflowSchema>(() => ({
    type: "updateWorkflowSchema",
    id: formStore.form.workFlow.id,
    contentHash: formStore.form.workFlow.contentHash,
    subject: "workflow",
    changeFrom: {
      config: workflowStore.currentWorkflowSchemaCoordinates,
    },
    changeTo: { config: workflowStore.newWorkflowSchemaCoordinates },
    language: languageStore.activeLanguage,
  }));

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    command,
    executeCommand,
  };
}
