import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand";
import { Ref, computed, readonly, ref, toRaw, watch } from "vue";
import { toRefs } from "@vueuse/core";
import { useLanguageStore } from "@/stores/language";
import { UploadQuestionNode } from "@/api/queries/types/formTypes";

export interface UploadProperties {
  minAmount: number;
  maxAmount: number;
  maxFileSize: number;
  mimeTypes: string[];
  extensions: string[];
}

export interface UpdateUploadPropertiesCommand
  extends UpdateCommand<UploadProperties> {
  subject: "node";
  type: "updateUploadProperties";
  language: string;
}

export interface FileType {
  code: string;
  mimeType: string;
}

const allowedFileTypes: Readonly<FileType[]> = readonly([
  { code: "txt", mimeType: "text/plain" },
  { code: "zip", mimeType: "application/zip" },
  { code: "pdf", mimeType: "application/pdf" },
  { code: "doc", mimeType: "application/msword" },
  {
    code: "docx",
    mimeType:
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  },
  { code: "xls", mimeType: "application/vnd.ms-excel" },
  {
    code: "xlsx",
    mimeType:
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  },
  { code: "ppt", mimeType: "application/vnd.ms-powerpoint" },
  {
    code: "pptx",
    mimeType:
      "application/vnd.openxmlformats-officedocument.presentationml.presentation",
  },
  { code: "jpg", mimeType: "image/jpeg" },
  { code: "jpeg", mimeType: "image/jpeg" },
  { code: "png", mimeType: "image/png" },
  { code: "png", mimeType: "image/gif" },
  { code: "mp3", mimeType: "audio/mpeg" },
  {
    code: "odp",
    mimeType: "application/vnd.oasis.opendocument.presentation",
  },
  {
    code: "ods",
    mimeType: "application/vnd.oasis.opendocument.spreadsheet",
  },
  {
    code: "odt",
    mimeType: "application/vnd.oasis.opendocument.text",
  },
]);

const languageStore = useLanguageStore();

export function useUpdateUploadPropertiesCommand(
  question: Ref<UploadQuestionNode>,
) {
  const currentState = computed<UploadProperties>(() => ({
    minAmount: question.value.configProps.minAmount,
    maxAmount: question.value.configProps.maxAmount,
    maxFileSize: question.value.configProps.maxFileSize,
    mimeTypes: toRaw(question.value.configProps.mimeTypes),
    extensions: toRaw(question.value.configProps.extensions),
  }));

  const fileTypeForExtension = (extension: string) =>
    allowedFileTypes.find(
      (allowedFileType) => allowedFileType.code === extension,
    ) ?? { code: "", mimeType: "" };
  const fileTypes = ref<FileType[]>(
    currentState.value.extensions.map(fileTypeForExtension),
  );

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateUploadPropertiesCommand,
    UploadProperties
  >(
    "updateUploadProperties",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  const { minAmount, maxAmount, maxFileSize, mimeTypes, extensions } =
    toRefs(newPayload);

  watch(fileTypes, () => {
    mimeTypes.value = fileTypes.value.map((type) => type.mimeType);
    extensions.value = fileTypes.value.map((type) => type.code);
  });

  return {
    allowedFileTypes,

    newPayload,

    minAmount,
    maxAmount,
    maxFileSize,
    fileTypes,

    command,
    changed,
    executeCommand,
  };
}
