import { Ref, computed } from "vue";
import type { AddCommand } from "@/api/commands/common/_commandTypes.ts";
import { useAddAndCopyStore } from "@/stores/addAndCopy.ts";
import { useLanguageStore } from "@/stores/language.ts";
import { FlowPermissionTemplate } from "@/api/queries/types/permissionTypes.ts";
import { executeApiCommand } from "./common/executeApiCommand";
import { useDragAndDropStore } from "@/stores/dragAndDropStore";
import { NodeSubType } from "../queries/types/formTypes";
import { AddNodeInfo } from "./AddNode";

export type AllowedQuestionTypes = Extract<
  NodeSubType,
  | "question_text"
  | "question_number"
  | "question_choice"
  | "question_date"
  | "question_upload"
>;

export interface AddMatrixInfo extends AddNodeInfo {
  firstRowLabel: string;
  firstQuestionLabel: string;
  firstQuestionType: AllowedQuestionTypes;
}

export interface AddMatrixCommand extends AddCommand<AddMatrixInfo> {
  type: "addMatrix";
  contentHash: string; // contentHash target node
  language: string;
  id: number; // id van de target node
  subject: "node";
}

const addAndCopyStore = useAddAndCopyStore();
const dragAndDropStore = useDragAndDropStore();
const languageStore = useLanguageStore();

export function useAddMatrixCommand(
  label: Ref<string>,
  selectedTemplate: Ref<FlowPermissionTemplate | undefined>,
  firstRowLabel: Ref<string>,
  firstQuestionLabel: Ref<string>,
  firstQuestionType: Ref<AllowedQuestionTypes>,
) {
  const addMatrixState = computed<AddMatrixInfo>(() => ({
    targetDirection: addAndCopyStore.insertDirection,
    label: label.value,
    permissionTemplateId: selectedTemplate.value?.id ?? 0,
    nodeType: addAndCopyStore.actionType.replace("add_", ""),
    firstRowLabel: firstRowLabel.value,
    firstQuestionLabel: firstQuestionLabel.value,
    firstQuestionType: firstQuestionType.value,
  }));

  const command = computed(
    (): AddMatrixCommand => ({
      type: "addMatrix",
      id: dragAndDropStore.droppedSelectedNode?.id ?? 0,
      language: languageStore.activeLanguage,
      contentHash: dragAndDropStore.droppedSelectedNode?.contentHash ?? "",
      subject: "node",
      add: addMatrixState.value,
    }),
  );

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    label,
    firstRowLabel,
    firstQuestionLabel,
    firstQuestionType,
    selectedTemplate,
    addNodeState: addMatrixState,
    command,
    executeCommand,
  };
}
