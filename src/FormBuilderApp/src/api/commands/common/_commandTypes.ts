export interface Command {
  type: string;
  contentHash: string;
  subject:
    | "node"
    | "option"
    | "form"
    | "formInfo"
    | "workflow"
    | "step"
    | "permissionTemplate";
  id: number;
  force?: boolean;
}

export interface PayloadCommand<T> extends Command {
  payload: T;
}
export interface AddCommand<T> extends Command {
  add: T;
}
export interface DeleteCommand<T> extends Command {
  delete: T;
}
export interface UpdateCommand<T> extends Command {
  changeFrom: T;
  changeTo: T;
}
