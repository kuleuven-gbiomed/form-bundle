import type { Command } from "@/api/commands/common/_commandTypes.ts";
import axios from "axios";
import { useFormStore } from "@/stores/form";
import { useSettingsStore } from "@/stores/settings";

export interface ResponseData {
  result: boolean;
  title: string;
  message: string;
}

// workaround for: https://github.com/primefaces/primevue/issues/3209
import ToastEventBus from "primevue/toasteventbus";
const addToast = (message: object) => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
  ToastEventBus.emit("add", message);
};

//todo rename to executeApiCommand
export async function executeApiCommand(command: Command) {
  const formStore = useFormStore();
  const settingsStore = useSettingsStore();
  command.force = settingsStore.setForceOnCommands;

  const formData = new FormData();
  formData.append("command", JSON.stringify(command));

  const response = await axios.post<ResponseData>(
    "/fb/execute-command",
    formData,
  );

  if (response.data.result) {
    addToast({
      severity: "success",
      summary: response.data.title,
      detail: response.data.message,
      life: 3000,
    });
    await formStore.hardRefreshForm();
    return response.data;
  } else {
    addToast({
      severity: "error",
      summary: response.data.title, //todo
      detail: response.data.message,
      life: 10000,
    });
    return false;
  }
}
