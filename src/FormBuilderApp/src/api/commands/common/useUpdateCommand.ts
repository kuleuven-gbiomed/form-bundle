import type { UpdateCommand } from "../common/_commandTypes.ts";
import { executeApiCommand } from "./executeApiCommand";
import { Ref, ref, toRaw, watch, computed } from "vue";

export function useUpdateCommand<T extends UpdateCommand<P>, P>(
  type: T["type"],
  id: T["id"],
  subject: T["subject"],
  contentHash: Ref<T["contentHash"]>,
  payload: Ref<P>,
  language?: Ref<string>,
) {
  const newPayload: Ref<P> = ref(
    // structuredClone();
    JSON.parse(JSON.stringify(toRaw(payload.value))),
  ) as Ref<P>;

  const _id: Ref<number> = ref(id);
  const _type: Ref<string> = ref(type);

  function resetIdAnType(id: number, type: string) {
    _id.value = id;
    _type.value = type;
  }

  const command = computed(
    () =>
      ({
        type: _type.value,
        id: _id.value,
        contentHash: contentHash.value,
        subject: subject,
        changeFrom: payload.value,
        changeTo: newPayload.value,
        ...(language && { language: language.value }),
      }) as T,
    // I'm not totally sure why but it doesn't work without a type assertion
    // so we have to manually make sure that all the updateCommands fit in this type
  );

  watch(payload, () => {
    // you have to explicitly watch here
    // https://vuejs.org/guide/reusability/composables.html#input-arguments
    try {
      newPayload.value = structuredClone(toRaw(payload.value));
    } catch (error) {
      console.error(error);
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      newPayload.value = JSON.parse(JSON.stringify(payload.value));
    }
  });

  const changed = computed<boolean>(
    () =>
      JSON.stringify(command.value.changeFrom) !==
      JSON.stringify(command.value.changeTo),
  );

  const reset = () => {
    newPayload.value = structuredClone(toRaw(payload.value));
  };

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    newPayload,
    command,
    changed,
    reset,
    executeCommand,
    resetIdAnType,
  };
}
