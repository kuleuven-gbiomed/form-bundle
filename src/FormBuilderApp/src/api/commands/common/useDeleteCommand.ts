import type { DeleteCommand } from "../common/_commandTypes.ts";
import { executeApiCommand } from "./executeApiCommand";
import { Ref, computed } from "vue";

export function useDeleteCommand<T extends DeleteCommand<P>, P>(
  type: T["type"],
  id: T["id"],
  subject: T["subject"],
  contentHash: Ref<T["contentHash"]>,
  payload: Ref<P>,
  language?: Ref<string>,
) {
  const command = computed(
    () =>
      ({
        type: type,
        id: id,
        contentHash: contentHash.value,
        subject: subject,
        delete: payload.value,
        ...(language && { language: language.value }),
      }) as T,
    // I'm not totally sure why but it doesn't work without a type assertion
    // so we have to manually make sure that all the updateCommands fit in this type
  );

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    command,
    executeCommand,
  };
}
