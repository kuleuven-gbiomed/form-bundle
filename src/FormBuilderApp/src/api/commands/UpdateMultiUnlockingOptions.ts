import { QuestionNode } from "@/api/queries/types/formTypes.ts";
import type { UpdateCommand } from "./common/_commandTypes.ts";
import { Ref, computed, ref } from "vue";
import { executeApiCommand } from "./common/executeApiCommand";

export type UpdateMultiUnlockingOptionsPayload = { optionIds: number[] };

export interface UpdateMultiUnlockingOptionsCommand
  extends UpdateCommand<UpdateMultiUnlockingOptionsPayload> {
  type: "updateMultiUnlockingOptions";
  subject: "node";
}

// we will always apply this command from the locked side
export function useUpdateMultiUnlockingOptionsCommand(
  unlockingQuestion: QuestionNode,
  lockedQuestionId: number,
  unlockingOptions: Ref<number[]>,
) {
  const payload = ref<UpdateMultiUnlockingOptionsPayload>({
    optionIds: unlockingOptions.value,
  });

  const command = computed<UpdateMultiUnlockingOptionsCommand>(() => ({
    type: "updateMultiUnlockingOptions",
    id: unlockingQuestion.id,
    subject: "node",
    contentHash: unlockingQuestion.contentHash,
    lockedQuestionId: lockedQuestionId,
    changeFrom: { optionIds: unlockingOptions.value },
    changeTo: payload.value,
  }));

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  const changed = computed<boolean>(
    () =>
      payload.value.optionIds.length !== unlockingOptions.value.length ||
      payload.value.optionIds.some(
        (optionId) => !unlockingOptions.value.includes(optionId),
      ),
  );

  return { payload, changed, command, executeCommand };
}
