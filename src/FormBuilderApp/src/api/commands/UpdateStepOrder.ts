import { useLanguageStore } from "@/stores/language.ts";
import { Ref, computed, ref, toRaw } from "vue";
import { useFormStore } from "@/stores/form.ts";
import { UpdateCommand } from "@/api/commands/common/_commandTypes.ts";
import { Step } from "@/api/queries/types/workflowTypes.ts";
import { executeApiCommand } from "./common/executeApiCommand.ts";

interface StepOrderPayload {
  order: number[];
}
export interface UpdateStepOrder extends UpdateCommand<StepOrderPayload> {
  subject: "form";
  type: "updateStepOrder";
}

const formStore = useFormStore();
const languageStore = useLanguageStore();

function mapSteps(steps: Step[]): number[] {
  return steps.map((step) => step.id);
}
function payloadFromSteps(steps: Step[]): StepOrderPayload {
  return { order: mapSteps(steps) };
}

export function useUpdateStepOrderCommand() {
  const steps: Ref<Step[]> = ref(
    structuredClone(toRaw(formStore.form.workFlow.steps)),
  );

  const command = computed<UpdateStepOrder>(() => ({
    type: "updateStepOrder",
    id: formStore.form.id,
    contentHash: formStore.form.contentHash,
    subject: "form",
    changeFrom: payloadFromSteps(formStore.form.workFlow.steps),
    changeTo: payloadFromSteps(steps.value),
    language: languageStore.activeLanguage,
  }));

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    steps,

    command,
    executeCommand,
  };
}
