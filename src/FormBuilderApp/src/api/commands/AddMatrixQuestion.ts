import { AddNodeInfo } from "./AddNode";
import type { AddCommand } from "@/api/commands/common/_commandTypes";
import { Ref, computed } from "vue";
import { useLanguageStore } from "@/stores/language";
import { executeApiCommand } from "./common/executeApiCommand";
import { useAddAndCopyStore } from "@/stores/addAndCopy";
import { useDragAndDropStore } from "@/stores/dragAndDropStore";
import { FlowPermissionTemplate } from "../queries/types/permissionTypes";

export interface AddMatrixQuestionCommand extends AddCommand<AddNodeInfo> {
  type: "addMatrixQuestion";
  language: string;
  subject: "node";
  colIndex: number;
}

const addAndCopyStore = useAddAndCopyStore();
const dragAndDropStore = useDragAndDropStore();
const languageStore = useLanguageStore();

export function useAddMatrixQuestionCommand(
  label: Ref<string>,
  selectedTemplate: Ref<FlowPermissionTemplate | undefined>,
) {
  const addNodeState = computed<AddNodeInfo>(() => ({
    targetDirection: addAndCopyStore.insertDirection,
    label: label.value,
    permissionTemplateId: selectedTemplate.value?.id ?? 0,
    nodeType: addAndCopyStore.actionType.replace("add_", ""),
  }));

  const command = computed<AddMatrixQuestionCommand>(() => ({
    type: "addMatrixQuestion",
    id: dragAndDropStore.droppedSelectedMatrixQuestion?.matrix.id ?? 0,
    contentHash:
      dragAndDropStore.droppedSelectedMatrixQuestion?.matrix.contentHash ?? "",
    colIndex: dragAndDropStore.droppedSelectedMatrixQuestion?.colIndex ?? 0,
    subject: "node",
    language: languageStore.activeLanguage,
    add: addNodeState.value,
  }));

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    label,
    selectedTemplate,
    addNodeState,

    command,
    executeCommand,
  };
}
