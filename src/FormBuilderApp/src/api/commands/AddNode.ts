import { computed, Ref } from "vue";
import type { AddCommand } from "@/api/commands/common/_commandTypes.ts";
import { useAddAndCopyStore } from "@/stores/addAndCopy.ts";
import { useLanguageStore } from "@/stores/language.ts";
import { FlowPermissionTemplate } from "@/api/queries/types/permissionTypes.ts";
import { executeApiCommand } from "./common/executeApiCommand";
import { useDragAndDropStore } from "@/stores/dragAndDropStore";

export interface AddNodeInfo {
  nodeType: string;
  label: string;
  permissionTemplateId: number;
  targetDirection: "in" | "above" | "below";
}

export interface AddNodeCommand extends AddCommand<AddNodeInfo> {
  type: "addNode";
  contentHash: string; // contentHash target node
  language: string;
  id: number; // id van de target node
  subject: "node";
}

const addAndCopyStore = useAddAndCopyStore();
const dragAndDropStore = useDragAndDropStore();
const languageStore = useLanguageStore();

export function useAddNodeCommand(
  label: Ref<string>,
  selectedTemplate: Ref<FlowPermissionTemplate | undefined>,
) {
  const addNodeState = computed<AddNodeInfo>(() => ({
    targetDirection: addAndCopyStore.insertDirection,
    label: label.value,
    permissionTemplateId: selectedTemplate.value?.id ?? 0,
    nodeType: addAndCopyStore.actionType.replace("add_", ""),
  }));

  const command = computed(
    (): AddNodeCommand => ({
      type: "addNode",
      id: dragAndDropStore.droppedSelectedNode?.id ?? 0,
      language: languageStore.activeLanguage,
      contentHash: dragAndDropStore.droppedSelectedNode?.contentHash ?? "",
      subject: "node",
      add: addNodeState.value,
    }),
  );

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    label,
    selectedTemplate,
    addNodeState,
    command,
    executeCommand,
  };
}
