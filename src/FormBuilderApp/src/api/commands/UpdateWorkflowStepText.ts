import { UpdateCommand } from "@/api/commands/common/_commandTypes.ts";
import { computed, Ref } from "vue";
import { Step } from "@/api/queries/types/workflowTypes.ts";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand.ts";
import { useLanguageStore } from "@/stores/language.ts";
import {
  EditorConfig,
  EditorSubject,
} from "@/components/formBuilder/formWorkflow/detail/common/EditorConfiguration.ts";

const languageStore = useLanguageStore();

export interface WorkflowStepText {
  subject: string;
  description: string;
}

export interface UpdateWorkflowStepText
  extends UpdateCommand<WorkflowStepText> {
  id: number;
  type: "updateWorkflowStepText";
  contentHash: string;
  subject: "step";
  language: string;
}

export function useUpdateWorkflowStepText(
  step: Ref<Step>,
  config: Ref<EditorConfig>,
) {
  const currentState = computed((): WorkflowStepText => {
    let _description = "";
    switch (config.value.subject) {
      case EditorSubject.generalSubmitSuccessMessage:
        _description =
          step.value.submitSuccessUserInteraction.generalSubmitSuccessMessage[
            languageStore.activeLanguage.toLowerCase()
          ] ?? "";
        break;
      case EditorSubject.mailBodyForStart:
        _description =
          step.value.submitSuccessUserInteraction.notificationConfig
            .mailBodyForStart[languageStore.activeLanguage.toLowerCase()] ?? "";
        break;
      case EditorSubject.mailBodyForOngoing:
        _description =
          step.value.submitSuccessUserInteraction.notificationConfig
            .mailBodyForOngoing[languageStore.activeLanguage.toLowerCase()] ??
          "";
        break;
    }
    return {
      subject: config.value.subject,
      description: _description,
    };
  });

  const { newPayload, command, changed, executeCommand } = useUpdateCommand<
    UpdateWorkflowStepText,
    WorkflowStepText
  >(
    "updateWorkflowStepText",
    step.value.id,
    "step",
    computed(() => step.value.contentHash ?? ""),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  return {
    payload: newPayload,
    command,
    changed,
    executeCommand,
  };
}
