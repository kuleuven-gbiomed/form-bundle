import { useFormStore } from "@/stores/form.ts";
import type { Command } from "@/api/commands/common/_commandTypes.ts";
import { executeApiCommand } from "./common/executeApiCommand";
import { Ref, computed } from "vue";

export interface PublishFormCommand extends Command {
  type: "publishForm";
  subject: "form";
  id: number; // id van het formulier
  useNewVersionNumber: boolean;
}

export function usePublishFormCommand(useNewVersionNumber: Ref<boolean>) {
  const formStore = useFormStore();
  const command = computed<PublishFormCommand>(() => ({
    type: "publishForm",
    subject: "form",
    contentHash: formStore.form.contentHash,
    id: formStore.form.id,
    useNewVersionNumber: useNewVersionNumber.value,
  }));

  function executeCommand() {
    return executeApiCommand(command.value);
  }

  return { command, executeCommand };
}
