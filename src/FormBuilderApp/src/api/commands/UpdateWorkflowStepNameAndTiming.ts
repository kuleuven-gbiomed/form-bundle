import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand.ts";
import { Ref, computed } from "vue";
import type { UpdateCommand } from "./common/_commandTypes.ts";
import { Step, isFixedStep } from "@/api/queries/types/workflowTypes.ts";
import { useLanguageStore } from "@/stores/language.ts";

export interface StepNameAndTimingFixed {
  label: string;
  fixedStartDate: Date;
  fixedEndDate: Date;
  allowsStartIfPreviousStepIsFulfilled: boolean;
}

export interface StepNameAndTimingRelative {
  label: string;
  amountOfDaysToCalculateStartDate: string;
  amountOfDaysToCalculateEndDate: string;
  allowsStartIfPreviousStepIsFulfilled: boolean;
}

export function isFixedPayload(
  payload: StepNameAndTimingFixed | StepNameAndTimingRelative,
): payload is StepNameAndTimingFixed {
  return "fixedStartDate" in payload;
}

export function isRelativePayload(
  payload: StepNameAndTimingFixed | StepNameAndTimingRelative,
): payload is StepNameAndTimingRelative {
  return "amountOfDaysToCalculateStartDate" in payload;
}

export interface UpdateWorkflowStepNameAndTimingFixed
  extends UpdateCommand<StepNameAndTimingFixed> {
  id: number;
  type: "updateWorkflowStepNameAndTimingFixed";
  contentHash: string;
  subject: "step";
  language: string;
}

export interface UpdateWorkflowStepNameAndTimingRelative
  extends UpdateCommand<StepNameAndTimingRelative> {
  id: number;
  type: "updateWorkflowStepNameAndTimingRelative";
  contentHash: string;
  subject: "step";
  language: string;
}

export function useUpdateWorkflowStepNameAndTiming(step: Ref<Step>) {
  const languageStore = useLanguageStore();
  const currentState = computed(
    (): StepNameAndTimingRelative | StepNameAndTimingFixed => {
      if (isFixedStep(step.value)) {
        return {
          allowsStartIfPreviousStepIsFulfilled:
            step.value.allowsStartIfPreviousStepIsFulfilled,
          fixedEndDate: new Date(
            "fixedEndDate" in step.value.period
              ? step.value.period.fixedEndDate
              : "",
          ),
          fixedStartDate: new Date(
            "fixedStartDate" in step.value.period
              ? step.value.period.fixedStartDate
              : "",
          ),
          label:
            step.value.info.label[languageStore.activeLanguage.toLowerCase()] ??
            "",
        };
      } else {
        return {
          allowsStartIfPreviousStepIsFulfilled:
            step.value.allowsStartIfPreviousStepIsFulfilled,
          amountOfDaysToCalculateEndDate:
            "amountOfDaysToCalculateEndDate" in step.value.period
              ? step.value.period.amountOfDaysToCalculateEndDate.toString()
              : "0",
          amountOfDaysToCalculateStartDate:
            "amountOfDaysToCalculateStartDate" in step.value.period
              ? step.value.period.amountOfDaysToCalculateStartDate.toString()
              : "0",
          label:
            step.value.info.label[languageStore.activeLanguage.toLowerCase()] ??
            "",
        };
      }
    },
  );
  const { newPayload, command, changed, executeCommand, resetIdAnType } =
    useUpdateCommand<
      | UpdateWorkflowStepNameAndTimingFixed
      | UpdateWorkflowStepNameAndTimingRelative,
      StepNameAndTimingRelative | StepNameAndTimingFixed
    >(
      isFixedStep(step.value)
        ? "updateWorkflowStepNameAndTimingFixed"
        : "updateWorkflowStepNameAndTimingRelative",
      step.value.id,
      "step",
      computed(() => step.value.contentHash ?? ""),
      currentState,
      computed(() => languageStore.activeLanguage),
    );

  function resetCommand(step: Step) {
    const _id = step.id;
    const _type = isFixedStep(step)
      ? "updateWorkflowStepNameAndTimingFixed"
      : "updateWorkflowStepNameAndTimingRelative";
    resetIdAnType(_id, _type);
  }

  return {
    payload: newPayload,
    command,
    changed,
    executeCommand,
    resetCommand,
  };
}
