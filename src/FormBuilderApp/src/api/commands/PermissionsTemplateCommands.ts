import { computed, Ref, ref, toRaw } from "vue";
import type { UpdateCommand } from "./common/_commandTypes";
import { AddCommand, DeleteCommand } from "./common/_commandTypes";
import type { FlowPermissionTemplate } from "@/api/queries/types/permissionTypes";
import { FlowPermissionRecord } from "@/api/queries/types/permissionTypes";
import { useUpdateCommand } from "./common/useUpdateCommand";
import { toRefs } from "@vueuse/shared";
import { useFormStore } from "@/stores/form";
import { useAddCommand } from "@/api/commands/common/useAddCommand";
import { useDeleteCommand } from "./common/useDeleteCommand";

export type FlowPermissionTemplatePayload = Pick<
  FlowPermissionTemplate,
  "name" | "flowPermission"
>;

export interface AddPermissionsTemplateCommand
  extends AddCommand<FlowPermissionTemplatePayload> {
  type: "addPermissionsTemplate";
  subject: "form";
}
export interface UpdatePermissionsTemplateCommand
  extends UpdateCommand<FlowPermissionTemplatePayload> {
  type: "updatePermissionsTemplate";
  subject: "permissionTemplate";
}

interface Id {
  id: number;
}
export interface DeletePermissionsTemplateCommand extends DeleteCommand<Id> {
  subject: "form";
  type: "deletePermissionsTemplate";
}

const formStore = useFormStore();

function getEmptyFlowPermission(): FlowPermissionRecord {
  const emptyRolePermission = Object.fromEntries(
    formStore.form.rolesThatCouldParticipate.map((role) => [
      role,
      {
        read: false,
        write: false,
      },
    ]),
  );
  return Object.fromEntries(
    formStore.form.workFlow.steps.map((step) => [
      step.uid,
      structuredClone(emptyRolePermission),
    ]),
  );
}

export function useAddPermissionTemplateCommand() {
  const permissionTemplate = ref({
    name: "",
    flowPermission: getEmptyFlowPermission(),
  });

  const { command, executeCommand } = useAddCommand<
    AddPermissionsTemplateCommand,
    FlowPermissionTemplatePayload
  >(
    "addPermissionsTemplate",
    formStore.form.id,
    "form",
    computed(() => formStore.form.contentHash),
    permissionTemplate,
  );

  return { permissionTemplate, command, executeCommand };
}

export function useDeletePermissionTemplateCommand(templateId: Ref<number>) {
  const permissionTemplate = ref({
    name: "",
    flowPermission: getEmptyFlowPermission(),
  });

  const { command, executeCommand } = useDeleteCommand<
    DeletePermissionsTemplateCommand,
    Id
  >(
    "deletePermissionsTemplate",
    formStore.form.id,
    "form",
    computed(() => formStore.form.contentHash),
    computed(() => ({
      id: templateId.value,
    })),
  );

  return { permissionTemplate, command, executeCommand };
}

export function useUpdatePermissionTemplateCommand(
  template: Ref<FlowPermissionTemplate>,
) {
  const payload = computed(() => ({
    name: toRaw(template.value.name),
    flowPermission: toRaw(template.value.flowPermission),
  }));
  console.log(payload);
  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdatePermissionsTemplateCommand,
    FlowPermissionTemplatePayload
  >(
    "updatePermissionsTemplate",
    template.value.id,
    "permissionTemplate",
    computed(() => template.value.contentHash),
    payload,
  );

  const { flowPermission, name } = toRefs(newPayload);

  return {
    newPermissionTemplate: newPayload,

    flowPermission,
    name,
    command,
    changed,

    executeCommand,
  };
}
