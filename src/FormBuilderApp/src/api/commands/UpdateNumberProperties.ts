import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/core";
import { NumberQuestionNode } from "@/api/queries/types/formTypes";
import { useLanguageStore } from "@/stores/language";

export interface NumberProperties {
  min: number;
  max: number;
  scale: number;
  stepInterval: number;
  showMaxAsInputAddon: boolean;
  showMinAsInputAddon: boolean;
}

export interface UpdateNumberPropertiesCommand
  extends UpdateCommand<NumberProperties> {
  type: "updateNumberProperties";
  language: string;
  subject: "node";
}

export function useUpdateCalculationProperties(
  question: Ref<NumberQuestionNode>,
) {
  const languageStore = useLanguageStore();

  const currentState = computed(
    (): NumberProperties => ({
      min: question.value.configProps.min,
      max: question.value.configProps.max,
      scale: question.value.configProps.scale,
      stepInterval: question.value.configProps.stepInterval,
      showMaxAsInputAddon: question.value.configProps.showMaxAsInputAddon,
      showMinAsInputAddon: question.value.configProps.showMinAsInputAddon,
    }),
  );

  const { newPayload, changed, command, executeCommand } = useUpdateCommand<
    UpdateNumberPropertiesCommand,
    NumberProperties
  >(
    "updateNumberProperties",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  const {
    min,
    max,
    scale,
    stepInterval,
    showMaxAsInputAddon,
    showMinAsInputAddon,
  } = toRefs(newPayload);

  return {
    min,
    max,
    scale,
    stepInterval,
    showMaxAsInputAddon,
    showMinAsInputAddon,

    newPayload,

    changed,
    command,

    executeCommand,
  };
}
