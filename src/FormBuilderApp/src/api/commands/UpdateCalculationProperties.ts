import {
  ChoiceQuestionNode,
  NumberQuestionNode,
} from "../queries/types/formTypes";
import type { UpdateCommand } from "./common/_commandTypes";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/core";
import { useUpdateCommand } from "./common/useUpdateCommand";

export interface CalculationPayload {
  isCalculation: boolean;
  weighingType: "absolute" | "percentage";
  passScore: number | null;
  showAutoCalculateBtn: boolean;
  allowNonAutoCalculatedValue: boolean;
  roundingMode: number | null;
  scale: number | null;
  minScore: number | null;
  maxScore: number | null;
}
export interface UpdateCalculationProperties
  extends UpdateCommand<CalculationPayload> {
  subject: "node";
  language: string;
  type: "updateCalculationProperties";
}

export function useUpdateCalculationProperties(
  question: Ref<NumberQuestionNode | ChoiceQuestionNode>,
) {
  const questionIsCalculation = computed(() => {
    let _result =
      question.value.configScoring.globalScoreCalculationDependencyConfigs
        .length > 0;
    if (question.value.configScoring.isCalculation)
      _result = question.value.configScoring.isCalculation;
    return _result;
  });

  const currentState = computed<CalculationPayload>(() => {
    if (question.value.subType === "question_choice") {
      return {
        isCalculation: questionIsCalculation.value,
        weighingType: question.value.configScoring.weighingType,
        passScore: question.value.configScoring.scoringParameters.passScore,
        showAutoCalculateBtn:
          question.value.configScoring.scoringParameters.showAutoCalculateBtn,
        allowNonAutoCalculatedValue:
          question.value.configScoring.scoringParameters
            .allowNonAutoCalculatedValue,
        roundingMode:
          question.value.configScoring.scoringParameters.roundingMode,
        scale: question.value.configScoring.scoringParameters.scale,
        minScore: question.value.configScoring.scoringParameters.minScore,
        maxScore: question.value.configScoring.scoringParameters.maxScore,
      };
    } else {
      return {
        isCalculation: questionIsCalculation.value,
        weighingType: question.value.configScoring.weighingType,
        passScore: question.value.configScoring.passScore,
        showAutoCalculateBtn: question.value.configScoring.showAutoCalculateBtn,
        allowNonAutoCalculatedValue:
          question.value.configScoring.allowNonAutoCalculatedValue,
        roundingMode: question.value.configProps.roundingMode,
        scale: question.value.configProps.scale,
        minScore: null,
        maxScore: null,
      };
    }
  });

  const {
    newPayload: newCalculationState,
    command,
    executeCommand,
  } = useUpdateCommand<UpdateCalculationProperties, CalculationPayload>(
    "updateCalculationProperties",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    currentState,
  );

  const {
    isCalculation,
    weighingType,
    passScore,
    showAutoCalculateBtn,
    allowNonAutoCalculatedValue,
    roundingMode,
    scale,
    minScore,
    maxScore,
  } = toRefs(newCalculationState);

  return {
    isCalculation,
    weighingType,
    passScore,
    showAutoCalculateBtn,
    allowNonAutoCalculatedValue,
    roundingMode,
    scale,
    minScore,
    maxScore,

    newCalculationState,

    command,

    executeCommand,
  };
}
