import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { DeleteCommand } from "@/api/commands/common/_commandTypes";
import { computed, Ref } from "vue";
import { toRefs } from "@vueuse/core";
import { useUpdateCommand } from "./common/useUpdateCommand";
import { useLanguageStore } from "@/stores/language";
import {
  ChartParticipantQuestion,
  QuestionNode,
} from "@/api/queries/types/formTypes";
import { useDeleteCommand } from "@/api/commands/common/useDeleteCommand";
import type { AddCommand } from "./common/_commandTypes";
import { useAddCommand } from "./common/useAddCommand";

export interface ChartRelationCommands {
  participantId: number;
  chartId: number;
  customLabel: string;
}

export interface AddChartRelationCommand
  extends AddCommand<ChartRelationCommands> {
  type: "addChartRelation";
  language: string;
  subject: "node";
}

export interface DeleteChartRelationCommand
  extends DeleteCommand<ChartRelationCommands> {
  type: "deleteChartRelation";
  language: string;
  subject: "node";
}

export interface UpdateChartRelationCommand
  extends UpdateCommand<ChartRelationCommands> {
  type: "updateChartRelation";
  language: string;
  subject: "node";
}

const languageStore = useLanguageStore();
function chartRelation(
  chartId: number,
  chartParticipantQuestion: Pick<
    ChartParticipantQuestion,
    "id" | "customLabel"
  >,
): ChartRelationCommands {
  return {
    participantId: chartParticipantQuestion.id,
    chartId: chartId,
    customLabel:
      chartParticipantQuestion.customLabel[languageStore.activeLanguage] ?? "",
  };
}

export function useUpdateChartRelationCommand(
  question: Ref<QuestionNode>,
  chartParticipantQuestion: Ref<ChartParticipantQuestion>,
) {
  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateChartRelationCommand,
    ChartRelationCommands
  >(
    "updateChartRelation",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    computed(() =>
      chartRelation(question.value.id, chartParticipantQuestion.value),
    ),
    computed(() => languageStore.activeLanguage),
  );

  const { customLabel } = toRefs(newPayload);

  return {
    newPayload,

    customLabel,

    command,
    changed,
    executeCommand,
  };
}

export function useDeleteChartRelationCommand(
  question: Ref<QuestionNode>,
  chartParticipantQuestion: Ref<ChartParticipantQuestion>,
) {
  return useDeleteCommand<DeleteChartRelationCommand, ChartRelationCommands>(
    "deleteChartRelation",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    computed(() =>
      chartRelation(question.value.id, chartParticipantQuestion.value),
    ),
    computed(() => useLanguageStore().activeLanguage),
  );
}

export function useAddChartRelationCommand(
  chartId: Ref<number>,
  chartParticipantQuestion: Ref<QuestionNode>,
  customLabel: Ref<string>,
) {
  return useAddCommand<AddChartRelationCommand, ChartRelationCommands>(
    "addChartRelation",
    chartParticipantQuestion.value.id,
    "node",
    computed(() => chartParticipantQuestion.value.contentHash),
    computed(() => ({
      participantId: chartParticipantQuestion.value.id,
      customLabel: customLabel.value,
      chartId: chartId.value,
    })),
    computed(() => useLanguageStore().activeLanguage),
  );
}
