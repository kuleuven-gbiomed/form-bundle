import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/core";
import { useLanguageStore } from "@/stores/language";
import { TextQuestionNode } from "@/api/queries/types/formTypes";

interface TextProperties {
  multiLine: boolean;
  minLength: number;
  maxLength: number;
  displayMode: "regular" | "wysiwyg";
}

export interface UpdateTextPropertiesCommand
  extends UpdateCommand<TextProperties> {
  subject: "node";
  type: "updateTextProperties";
  language: string;
}

const languageStore = useLanguageStore();

export function useUpdateTextPropertiesCommand(
  question: Ref<TextQuestionNode>,
) {
  const currentState = computed<TextProperties>(() => ({
    multiLine: question.value.configProps.multiLine,
    minLength: question.value.configProps.minLength,
    maxLength: question.value.configProps.maxLength,
    displayMode: question.value.displayMode,
  }));

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateTextPropertiesCommand,
    TextProperties
  >(
    "updateTextProperties",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  const { multiLine, minLength, maxLength, displayMode } = toRefs(newPayload);

  return {
    currentState,
    newPayload,

    multiLine,
    minLength,
    maxLength,
    displayMode,

    command,
    changed,
    executeCommand,
  };
}
