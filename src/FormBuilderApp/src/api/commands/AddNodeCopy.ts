import type { AddCommand } from "@/api/commands/common/_commandTypes.ts";
import { FlowPermissionTemplate } from "@/api/queries/types/permissionTypes.ts";
import { useAddAndCopyStore } from "@/stores/addAndCopy.ts";
import { useLanguageStore } from "@/stores/language.ts";
import { ref, computed } from "vue";
import { executeApiCommand } from "./common/executeApiCommand";
import { useDragAndDropStore } from "@/stores/dragAndDropStore";

export interface SourceNodeInfo {
  sourceNodeId: number;
  targetDirection: "in" | "above" | "below";
  sourceNodeContentHash: string;
  permissionTemplateId: number;
  label: string;
}

export interface AddNodeCopyCommand extends AddCommand<SourceNodeInfo> {
  type: "addNodeCopy" | "addCategoryCopy";
  contentHash: string; // contentHash target node
  language: string;
  id: number; // id van de target node
  subject: "node";
}

export function useAddNodeCopyCommand() {
  const dragAndDropStore = useDragAndDropStore();
  const addAndCopyStore = useAddAndCopyStore();
  const languageStore = useLanguageStore();
  const selectedTemplate = ref<FlowPermissionTemplate>();
  const label = ref<string>("");

  const sourceNodeState = computed<SourceNodeInfo>(() => ({
    targetDirection: addAndCopyStore.insertDirection,
    sourceNodeContentHash: addAndCopyStore.copySelectedNode?.contentHash ?? "",
    sourceNodeId: addAndCopyStore.copySelectedNode?.id ?? 0,
    permissionTemplateId: selectedTemplate.value?.id ?? 0,
    label: label.value,
  }));

  const command = computed(
    (): AddNodeCopyCommand => ({
      type:
        addAndCopyStore.copySelectedNode?.type === "question"
          ? "addNodeCopy"
          : "addCategoryCopy",
      id: dragAndDropStore.droppedSelectedNode?.id ?? 0,
      language: languageStore.activeLanguage,
      contentHash: dragAndDropStore.droppedSelectedNode?.contentHash ?? "",
      subject: "node",
      add: sourceNodeState.value,
    }),
  );

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    label,
    selectedTemplate,
    sourceNodeState,
    command,
    executeCommand,
  };
}
