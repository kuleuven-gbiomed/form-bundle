import { Ref, computed } from "vue";
import type { DeleteCommand } from "./common/_commandTypes.ts";
import { QuestionNode } from "@/api/queries/types/formTypes.ts";
import { executeApiCommand } from "./common/executeApiCommand";

export interface ConditionToDelete {
  optionId: number;
  lockedId: number;
  unlockerId: number;
}
export interface DeleteMultiConditionCommand
  extends DeleteCommand<ConditionToDelete> {
  type: "deleteMultiCondition";
  subject: "node";
}

export function useDeleteMultiConditionCommand(
  question: Ref<QuestionNode>,
  targetType: "locked" | "unlocker", // we can apply this command from the locked or unlocker side
  targetId: Ref<number>,
  optionId: Ref<number>,
) {
  const conditionPayload = computed<ConditionToDelete>(() => ({
    lockedId: targetType === "locked" ? targetId.value : question.value.id,
    unlockerId: targetType === "unlocker" ? targetId.value : question.value.id,
    optionId: optionId.value,
  }));

  const command = computed<DeleteMultiConditionCommand>(() => ({
    type: "deleteMultiCondition",
    id: question.value.id,
    subject: "node",
    contentHash: question.value.contentHash,
    delete: conditionPayload.value,
  }));

  async function executeCommand() {
    console.log("ben aant executen");
    return await executeApiCommand(command.value);
  }

  return { command, executeCommand };
}
