import type { DeleteCommand } from "@/api/commands/common/_commandTypes.ts";
import { Step } from "@/api/queries/types/workflowTypes.ts";
import { useFormStore } from "@/stores/form.ts";
import { Ref, computed } from "vue";
import { executeApiCommand } from "./common/executeApiCommand";

export interface StepToDelete {
  stepId: number;
  formId: number;
}

export interface DeleteWorkflowStepCommand extends DeleteCommand<StepToDelete> {
  type: "deleteWorkFlowStep";
  id: number;
}

export function useDeleteWorkflowStepCommand(step: Ref<Step>) {
  const formStore = useFormStore();

  const command = computed(
    (): DeleteWorkflowStepCommand => ({
      id: step.value.id,
      type: "deleteWorkFlowStep",
      contentHash: step.value.contentHash ?? "",
      subject: "step",
      delete: { stepId: step.value.id, formId: formStore.form.id },
    }),
  );

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    command,
    executeCommand,
  };
}
