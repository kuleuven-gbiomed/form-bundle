import { UpdateCommand } from "@/api/commands/common/_commandTypes.ts";

export interface StepMessage {
  generalSubmitSuccessMessage: string;
}

export interface UpdateWorkflowMessages extends UpdateCommand<StepMessage> {
  id: number;
  type: "updateWorkflowMessages";
  contentHash: string;
  subject: "step";
  language: string;
}

// export function useUpdateWorkflowMessages(step: Ref<Step>) {}
