import type { Command } from "@/api/commands/common/_commandTypes.ts";
import { useFormStore } from "@/stores/form.ts";
import { executeApiCommand } from "./common/executeApiCommand";

export interface ResetToWorkingVersionCommand extends Command {
  type: "resetToWorkingVersion";
  subject: "form";
}

const formStore = useFormStore();
export async function executeResetToWorkingVersion() {
  const resetCommand: ResetToWorkingVersionCommand = {
    type: "resetToWorkingVersion",
    subject: "form",
    contentHash: formStore.form.contentHash,
    id: formStore.form.id,
  };
  await executeApiCommand(resetCommand);
}
