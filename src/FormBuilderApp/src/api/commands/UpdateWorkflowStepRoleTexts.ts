import { UpdateCommand } from "@/api/commands/common/_commandTypes.ts";
import { computed, Ref } from "vue";
import { RoleTranslation, Step } from "@/api/queries/types/workflowTypes.ts";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand.ts";
import { useLanguageStore } from "@/stores/language.ts";
import type {
  FormTranslations,
  RoleSummary,
} from "@/api/queries/types/formTypes.ts";
import {
  EditorConfig,
  EditorSubject,
} from "@/components/formBuilder/formWorkflow/detail/common/EditorConfiguration.ts";
import { renderRoleLabel } from "@/functions/roles.ts";

const languageStore = useLanguageStore();

export interface WorkflowStepRoleTexts {
  subject: string;
  messages: Array<RoleMessage>;
}

export interface RoleMessage {
  roles: Array<RoleSummary>;
  translations: Array<RoleMessageTranslation>;
}

export interface RoleMessageTranslation {
  language: string;
  message: string;
}

export interface UpdateWorkflowStepRoleTexts
  extends UpdateCommand<WorkflowStepRoleTexts> {
  id: number;
  type: "updateWorkflowStepRoleTexts";
  contentHash: string;
  subject: "step";
  language: string;
}

export function useUpdateWorkflowStepRoleTexts(
  step: Ref<Step>,
  config: Ref<EditorConfig>,
) {
  const currentState = computed((): WorkflowStepRoleTexts => {
    const _messages: Array<RoleMessage> =
      convertFormBundleStructureToFormBuildAppStructure(
        step,
        config.value.subject,
      );
    return {
      subject: config.value.subject,
      messages: _messages,
    };
  });

  const { newPayload, command, changed, executeCommand } = useUpdateCommand<
    UpdateWorkflowStepRoleTexts,
    WorkflowStepRoleTexts
  >(
    "updateWorkflowStepRoleTexts",
    step.value.id,
    "step",
    computed(() => step.value.contentHash ?? ""),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  return {
    payload: newPayload,
    command,
    changed,
    executeCommand,
  };
}

function convertFormBundleStructureToFormBuildAppStructure(
  step: Ref<Step>,
  type: string,
): Array<RoleMessage> {
  const _result: Array<RoleMessage> = [];
  let _messages: Array<RoleTranslation> = [];
  switch (type) {
    case EditorSubject.roleSubmitSuccessMessages:
      _messages =
        step.value.submitSuccessUserInteraction.roleSubmitSuccessMessages;
      break;
    case EditorSubject.roleMailBodiesForStart:
      if (
        step.value.submitSuccessUserInteraction.notificationConfig
          .roleMailBodiesForStart
      ) {
        _messages =
          step.value.submitSuccessUserInteraction.notificationConfig
            .roleMailBodiesForStart;
      }
      break;
    case EditorSubject.roleMailBodiesForOngoing:
      if (
        step.value.submitSuccessUserInteraction.notificationConfig
          .roleMailBodiesForOngoing
      ) {
        _messages =
          step.value.submitSuccessUserInteraction.notificationConfig
            .roleMailBodiesForOngoing;
      }
      break;
  }

  // -- step 1: make the messages unique (combining the language keys)
  const _translationStringMessages: Array<string> = [];
  for (const _message of _messages) {
    const _translationString = JSON.stringify(_message.translations);
    _translationStringMessages.push(_translationString);
  }
  const _differentMessages = [...new Set(_translationStringMessages)];

  // -- step 2: loop over the unique messages and set roles and messages (combined)
  for (const _messageString of _differentMessages) {
    const _messageObject: FormTranslations = JSON.parse(
      _messageString,
    ) as FormTranslations;
    const _resultTranslations: Array<RoleMessageTranslation> = [];
    for (const languageKey in _messageObject) {
      const _resultTranslation: RoleMessageTranslation = {
        language: languageKey,
        message: _messageObject[languageKey] ?? "",
      };
      _resultTranslations.push(_resultTranslation);
    }
    const _roleMessage: RoleMessage = {
      roles: [],
      translations: _resultTranslations,
    };
    for (const _message of _messages) {
      const _translationString = JSON.stringify(_message.translations);
      if (_translationString === _messageString) {
        const _role: RoleSummary = {
          role: _message.role,
          label: renderRoleLabel(_message.role),
        };
        _roleMessage.roles.push(_role);
      }
    }
    _result.push(_roleMessage);
  }

  return _result;
}
