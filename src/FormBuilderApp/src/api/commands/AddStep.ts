import { StepNodePosition } from "../queries/types/workflowTypes.ts";
import type { AddCommand } from "@/api/commands/common/_commandTypes.ts";
import { StepNodeData } from "@/api/queries/types/workflowTypes.ts";
import { useFormStore } from "@/stores/form.ts";
import { useLanguageStore } from "@/stores/language.ts";
import { Ref, computed, ref } from "vue";
import { executeApiCommand } from "./common/executeApiCommand";
export interface AddWorkflowStepInfo {
  label: string;
  step: number;
  type: "fixed" | "relative";
  x: number;
  y: number;
}

export interface AddWorkflowStepCommand
  extends AddCommand<AddWorkflowStepInfo> {
  type: "addWorkflowStep";
  contentHash: string;
  language: string;
  id: number;
  subject: "form";
}

export function useAddWorkflowStepCommand(
  data: Ref<StepNodeData>,
  position: Ref<StepNodePosition>,
) {
  const formStore = useFormStore();
  const languageStore = useLanguageStore();

  const label = ref<string>("");
  const type = ref<"fixed" | "relative">("fixed");

  const addStepState = computed<AddWorkflowStepInfo>(() => ({
    label: label.value,
    step: data.value.step,
    type: type.value,
    x: position.value.x,
    y: position.value.y,
  }));

  const command = computed(
    (): AddWorkflowStepCommand => ({
      type: "addWorkflowStep",
      id: formStore.form.id,
      language: languageStore.activeLanguage,
      contentHash: formStore.form.contentHash,
      subject: "form",
      add: addStepState.value,
    }),
  );

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    label,
    type,
    addStepState,
    command,
    executeCommand,
  };
}
