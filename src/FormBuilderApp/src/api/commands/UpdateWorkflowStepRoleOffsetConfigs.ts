import { UpdateCommand } from "@/api/commands/common/_commandTypes.ts";
import { computed, Ref } from "vue";
import {
  RoleConfigsToOffset,
  Step,
} from "@/api/queries/types/workflowTypes.ts";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand.ts";
import type { RoleSummary } from "@/api/queries/types/formTypes.ts";
import {
  OffsetEditorConfig,
  OffsetEditorSubject,
} from "@/components/formBuilder/formWorkflow/detail/common/EditorConfiguration.ts";
import { renderRoleLabel } from "@/functions/roles.ts";

export interface WorkflowStepRoleOffsetConfigs {
  subject: string;
  configs: Array<OffsetConfig>;
}

export interface OffsetConfig {
  roles: Array<RoleSummary>;
  offset: number;
}

export interface UpdateWorkflowStepRoleOffsetConfigs
  extends UpdateCommand<WorkflowStepRoleOffsetConfigs> {
  id: number;
  type: "updateWorkflowStepRoleOffsetConfigs";
  contentHash: string;
  subject: "step";
}

export function useUpdateWorkflowStepRoleOffsetConfigs(
  step: Ref<Step>,
  config: Ref<OffsetEditorConfig>,
) {
  const currentState = computed((): WorkflowStepRoleOffsetConfigs => {
    return {
      subject: config.value.subject,
      configs: convertFormBundleStructureToFormBuildAppStructure(step, config),
    };
  });

  const { newPayload, command, changed, executeCommand } = useUpdateCommand<
    UpdateWorkflowStepRoleOffsetConfigs,
    WorkflowStepRoleOffsetConfigs
  >(
    "updateWorkflowStepRoleOffsetConfigs",
    step.value.id,
    "step",
    computed(() => step.value.contentHash ?? ""),
    currentState,
  );

  return {
    payload: newPayload,
    command,
    changed,
    executeCommand,
  };
}

function convertFormBundleStructureToFormBuildAppStructure(
  step: Ref<Step>,
  config: Ref<OffsetEditorConfig>,
): Array<OffsetConfig> {
  const _result: Array<OffsetConfig> = [];
  let _configs: Array<RoleConfigsToOffset> = [];

  switch (config.value.subject) {
    case OffsetEditorSubject.roleConfigsToNotifyOfStart:
      _configs =
        step.value.submitSuccessUserInteraction.notificationConfig
          .roleConfigsToNotifyOfStart;
      break;
    case OffsetEditorSubject.roleConfigsToRemindOfOngoing:
      _configs =
        step.value.submitSuccessUserInteraction.notificationConfig
          .roleConfigsToRemindOfOngoing;
      break;
  }

  // -- step 1: make the messages unique (combining the config values for the roles)
  const _configValues: Array<number> = [];
  for (const _config of _configs) {
    _configValues.push(_config.offset);
  }
  const _differentOffsetValues: Array<number> = [...new Set(_configValues)];

  // -- step 2: loop over the unique configs and set roles and offset value (combined)
  for (const _offsetValue of _differentOffsetValues) {
    const _roleConfig: OffsetConfig = { roles: [], offset: _offsetValue };
    for (const _config of _configs) {
      if (_config.offset == _offsetValue) {
        const _role: RoleSummary = {
          role: _config.role,
          label: renderRoleLabel(_config.role),
        };
        _roleConfig.roles.push(_role);
      }
    }
    _result.push(_roleConfig);
  }

  return _result;
}
