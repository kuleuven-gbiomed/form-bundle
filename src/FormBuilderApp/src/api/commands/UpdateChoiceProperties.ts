import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { useUpdateCommand } from "./common/useUpdateCommand";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/core";
import { ChoiceQuestionNode } from "@/api/queries/types/formTypes";
import { useLanguageStore } from "@/stores/language";

export interface ChoiceProperties {
  multiple: boolean;
  expanded: boolean;
  minSelect: number | null;
  maxSelect: number | null;
  sortOptionsAlphabetically: boolean;
  displayMode: "regular" | "singleTableRow";
  custom_options: CustomOptionsChoiceConfig | [];
}

export interface CustomOptionsChoiceConfig {
  show_scores_in_options: boolean;
}

export interface UpdateChoicePropertiesCommand
  extends UpdateCommand<ChoiceProperties> {
  subject: "node";
  type: "updateChoiceProperties";
  language: string;
}

const languageStore = useLanguageStore();

export function useUpdateChoicePropertiesCommand(
  question: Ref<ChoiceQuestionNode>,
) {
  const currentState = computed(
    (): ChoiceProperties => ({
      multiple: question.value.configProps.multiple,
      expanded: question.value.configProps.expanded,
      minSelect: question.value.configProps.minSelect,
      maxSelect: question.value.configProps.maxSelect,
      sortOptionsAlphabetically:
        question.value.configProps.sortOptionsAlphabetically,
      displayMode: question.value.displayMode,
      custom_options: question.value.configProps.custom_options,
    }),
  );

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateChoicePropertiesCommand,
    ChoiceProperties
  >(
    "updateChoiceProperties",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  const {
    multiple,
    expanded,
    minSelect,
    maxSelect,
    sortOptionsAlphabetically,
    displayMode,
    custom_options,
  } = toRefs(newPayload);

  return {
    newPayload,

    multiple,
    expanded,
    minSelect,
    maxSelect,
    sortOptionsAlphabetically,
    displayMode,
    custom_options,

    command,
    changed,
    executeCommand,
  };
}
