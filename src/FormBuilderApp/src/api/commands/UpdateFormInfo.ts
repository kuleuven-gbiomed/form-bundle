import { useFormStore } from "@/stores/form";
import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand";
import { computed, Ref } from "vue";
import { toRefs } from "@vueuse/core";

interface FormInfo {
  title: string;
  description: string;
}
export interface UpdateFormInfoCommand extends UpdateCommand<FormInfo> {
  id: number;
  type: "updateFormInfo";
  subject: "formInfo";
  language: string;
}

const formStore = useFormStore();

export function useUpdateFormInfoCommand(language: Ref<string>) {
  const currentState = computed(() => ({
    title: formStore.form.info.label[language.value] ?? "",
    description: formStore.form.info.description[language.value] ?? "",
  }));

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateFormInfoCommand,
    FormInfo
  >(
    "updateFormInfo",
    formStore.form.id,
    "formInfo",
    computed(() => formStore.form.info.contentHash ?? ""),
    currentState,
    language,
  );

  const { title, description } = toRefs(newPayload);

  return {
    newPayload,

    title,
    description,

    command,
    changed,
    executeCommand,
  };
}
