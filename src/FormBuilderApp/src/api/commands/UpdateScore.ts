import { ScoreQuestionWithWeighing } from "@/stores/node";
import { QuestionNode } from "../queries/types/formTypes";
import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import type { ScoreRelation } from "@/api/commands/AddScore";
import { useLanguageStore } from "@/stores/language";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/core";

export interface UpdateScoreCommand extends UpdateCommand<ScoreRelation> {
  type: "updateScore";
  language: string;
  subject: "node";
}

const languageStore = useLanguageStore();

export function useUpdateScoreCommand(
  question: Ref<QuestionNode>,
  scoreQuestion: Ref<ScoreQuestionWithWeighing>,
) {
  const currentState = computed<ScoreRelation>(() => ({
    scoreQuestionId: scoreQuestion.value.id,
    scoreCalculationId: question.value.id,
    weight: scoreQuestion.value.weighing,
  }));

  const { newPayload, command, executeCommand, changed, reset } =
    useUpdateCommand<UpdateScoreCommand, ScoreRelation>(
      "updateScore",
      question.value.id,
      "node",
      computed(() => question.value.contentHash),
      currentState,
      computed(() => languageStore.activeLanguage),
    );

  const { scoreQuestionId, scoreCalculationId, weight } = toRefs(newPayload);

  return {
    newPayload,

    scoreQuestionId,
    scoreCalculationId,
    weight,

    command,
    changed,
    reset,
    executeCommand,
  };
}
