import type { UpdateCommand } from "./common/_commandTypes";
import { computed } from "vue";
import { useFormStore } from "@/stores/form";
import { useUpdateCommand } from "./common/useUpdateCommand";
import { toRefs } from "@vueuse/core";

export interface DisplayOptions {
  showPositionNumbers: boolean;
  enableReadOnlyChoiceAnswerOptionsToggling: boolean;
}

export interface UpdateDisplayOptionsCommand
  extends UpdateCommand<DisplayOptions> {
  id: number;
  subject: "form";
  type: "updateDisplayOptions";
}

const formStore = useFormStore();

export function useUpdateDisplayOptionsCommand() {
  const currentState = computed<DisplayOptions>(() => ({
    showPositionNumbers: formStore.form.showPositionNumbers,
    enableReadOnlyChoiceAnswerOptionsToggling:
      formStore.form.enableReadOnlyChoiceAnswerOptionsToggling,
  }));

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateDisplayOptionsCommand,
    DisplayOptions
  >(
    "updateDisplayOptions",
    formStore.form.id,
    "form",
    computed(() => formStore.form.contentHash ?? ""),
    currentState,
  );

  const { showPositionNumbers, enableReadOnlyChoiceAnswerOptionsToggling } =
    toRefs(newPayload);

  return {
    showPositionNumbers,
    enableReadOnlyChoiceAnswerOptionsToggling,

    changed,
    command,
    executeCommand,
  };
}
