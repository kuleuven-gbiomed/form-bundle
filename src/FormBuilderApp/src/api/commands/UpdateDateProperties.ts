import { DateQuestionNode } from "@/api/queries/types/formTypes";
import type { UpdateCommand } from "@/api/commands/common/_commandTypes";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand";
import { Ref, computed } from "vue";
import { toRefs } from "@vueuse/core";
import { useLanguageStore } from "@/stores/language";

export interface DateProperties {
  formatMask: string;
  prefillWithToday: boolean;
  minDate: string;
  maxDate: string;
}

export interface UpdateDatePropertiesCommand
  extends UpdateCommand<DateProperties> {
  subject: "node";
  type: "updateDateProperties";
  language: string;
}

export function useUpdateDatePropertiesCommand(
  question: Ref<DateQuestionNode>,
) {
  const currentState = computed<DateProperties>(() => ({
    formatMask: question.value.configProps.formatMask,
    prefillWithToday: question.value.configProps.prefillWithToday,
    minDate: "",
    maxDate: "",
  }));

  const languageStore = useLanguageStore();

  const { newPayload, command, executeCommand, changed } = useUpdateCommand<
    UpdateDatePropertiesCommand,
    DateProperties
  >(
    "updateDateProperties",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    currentState,
    computed(() => languageStore.activeLanguage),
  );

  const { formatMask, prefillWithToday, minDate, maxDate } = toRefs(newPayload);

  return {
    newPayload,

    formatMask,
    prefillWithToday,
    minDate,
    maxDate,

    command,
    changed,
    executeCommand,
  };
}
