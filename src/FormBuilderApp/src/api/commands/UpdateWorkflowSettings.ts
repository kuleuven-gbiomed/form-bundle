import { computed, Ref } from "vue";
import { UpdateCommand } from "@/api/commands/common/_commandTypes.ts";
import { useUpdateCommand } from "@/api/commands/common/useUpdateCommand.ts";
import type { Step } from "@/api/queries/types/workflowTypes.ts";

export interface StepSettings {
  rolesWithOneSubmit: string[];
  hideReadOnlyNodesOnLanding: boolean;
  redirectAwayOnSubmitSuccess: boolean;
}

export interface UpdateWorkflowSettings extends UpdateCommand<StepSettings> {
  id: number;
  type: "updateWorkflowSettings";
  contentHash: string;
  subject: "step";
}

export function useUpdateWorkflowSettings(step: Ref<Step>) {
  const currentState = computed((): StepSettings => {
    return {
      rolesWithOneSubmit: step.value.rolesWithOneSubmit,
      hideReadOnlyNodesOnLanding: step.value.hideReadOnlyNodesOnLanding,
      redirectAwayOnSubmitSuccess:
        step.value.submitSuccessUserInteraction.redirectAwayOnSubmitSuccess,
    };
  });

  const { newPayload, command, changed, executeCommand } = useUpdateCommand<
    UpdateWorkflowSettings,
    StepSettings
  >(
    "updateWorkflowSettings",
    step.value.id,
    "step",
    computed(() => step.value.contentHash ?? ""),
    currentState,
  );

  return {
    payload: newPayload,
    command,
    changed,
    executeCommand,
  };
}
