import { Ref, computed, ref } from "vue";
import type { AddCommand } from "./common/_commandTypes.ts";
import { QuestionNode } from "@/api/queries/types/formTypes.ts";
import { executeApiCommand } from "./common/executeApiCommand";

export interface PrefillSourceQuestion {
  prefillSourceQuestionId: number;
  prefilledQuestionId: number;
}

export interface AddPrefillSourceCommand
  extends AddCommand<PrefillSourceQuestion> {
  type: "addPrefillSource";
  subject: "node";
}

export function useAddPrefilledSourceCommand(question: Ref<QuestionNode>) {
  const prefilledQuestionId = ref<number>(0);
  const prefillSourceQuestion = computed<PrefillSourceQuestion>(() => ({
    prefillSourceQuestionId: question.value.id,
    prefilledQuestionId: prefilledQuestionId.value,
  }));
  const command = computed(
    (): AddPrefillSourceCommand => ({
      id: question.value.id,
      type: "addPrefillSource",
      contentHash: question.value.contentHash,
      subject: "node",
      add: prefillSourceQuestion.value,
    }),
  );

  function executeCommand() {
    return executeApiCommand(command.value);
  }

  return { prefilledQuestionId, command, executeCommand };
}
