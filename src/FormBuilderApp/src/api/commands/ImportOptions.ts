import { Ref, computed, ref } from "vue";
import type { AddCommand } from "@/api/commands/common/_commandTypes.ts";
import { QuestionNode } from "@/api/queries/types/formTypes.ts";
import { useLanguageStore } from "@/stores/language.ts";
import { executeApiCommand } from "./common/executeApiCommand";

export interface ImportOption {
  importString: string;
  isAppend: boolean;
}

export interface ImportOptionsCommand extends AddCommand<ImportOption> {
  type: "importOptions";
  contentHash: string;
  language: string;
  id: number;
  subject: "node";
}

export function useImportOptionsCommand(question: Ref<QuestionNode>) {
  const languageStore = useLanguageStore();
  const importString = ref("");
  const isAppend = ref(true);

  const command = computed<ImportOptionsCommand>(() => ({
    type: "importOptions",
    id: question.value.id,
    contentHash: question.value.contentHash,
    subject: "node",
    language: languageStore.activeLanguage,
    add: {
      importString: importString.value,
      isAppend: isAppend.value,
    },
  }));

  async function executeCommand() {
    return await executeApiCommand(command.value);
  }

  return {
    importString,
    isAppend,
    command,
    executeCommand,
  };
}
