import { MaybeScoreQuestion } from "./../queries/types/subtypesForNodeRelations";
import type { DeleteCommand } from "@/api/commands/common/_commandTypes";
import type { ScoreRelation } from "@/api/commands/AddScore";
import { QuestionNode } from "@/api/queries/types/formTypes";
import { Ref, computed } from "vue";
import { useLanguageStore } from "@/stores/language";
import { useDeleteCommand } from "@/api/commands/common/useDeleteCommand";

export interface DeleteScoreCommand extends DeleteCommand<ScoreRelation> {
  type: "deleteScore";
  language: string;
  subject: "node";
}

export function useDeleteScoreCommand(
  question: Ref<QuestionNode>,
  scoreQuestion: Ref<MaybeScoreQuestion>,
) {
  return useDeleteCommand<DeleteScoreCommand, ScoreRelation>(
    "deleteScore",
    question.value.id,
    "node",
    computed(() => question.value.contentHash),
    computed<ScoreRelation>(() => ({
      scoreQuestionId: scoreQuestion.value.id,
      scoreCalculationId: question.value.id,
      weight: 0, //FIXME: weight is not needed for delete command
    })),
    computed(() => useLanguageStore().activeLanguage),
  );
}
