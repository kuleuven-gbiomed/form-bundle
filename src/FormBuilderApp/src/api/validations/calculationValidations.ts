export function roundingModeValidatorFn(value: number) {
  if (!(value === 1 || value === 2)) {
    return false;
  }
  return true;
}

export function scaleValidatorFn(value: number) {
  if (!(value === 0 || value === 1 || value === 2 || value === 3)) {
    return false;
  }
  return true;
}
