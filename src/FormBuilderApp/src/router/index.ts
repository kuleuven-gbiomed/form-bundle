import { Permission, useSettingsStore } from "@/stores/settings";
import { createRouter, createWebHashHistory } from "vue-router";
import { RoutePermissionName } from "./routePermissions";
import routes from "./routes/base";

declare module "vue-router" {
  interface RouteMeta {
    permission?: RoutePermissionName;
  }
}

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;

/**
 * Global Before Navigation Guards
 *
 * The user has access to a route if they have read access for the route or no permission exists.
 */
router.beforeEach(async (to) => {
  const settingsStore = useSettingsStore();
  if (!settingsStore.isLoaded) {
    await settingsStore.loadSettings();
  }

  if (to.meta.permission) {
    return settingsStore.routesWithPermissions.includes(to.meta.permission) &&
      settingsStore.routePermissions[to.meta.permission] === Permission.None
      ? { name: RoutePermissionName.Home }
      : true;
  }
});
