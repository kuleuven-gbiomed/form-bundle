/**
 * Permissions with the same name as the route will then be automatically applied
 */
export type RoutePermissionName =
  | "home"
  | "formsOverview"
  | "formListByUid"
  | "formList"
  | "formRaw"
  | "formGeneral"
  | "formWorkflow"
  | "formPermissions"
  | "formAdd";
export const RoutePermissionName = {
  Home: "home",
  FormsOverview: "formsOverview",
  FormListByUid: "formListByUid",
  FormList: "formList",
  FormRaw: "formRaw",
  FormGeneral: "formGeneral",
  FormWorkflow: "formWorkflow",
  FormPermissions: "formPermissions",
  FormAdd: "formAdd",
} as const;
