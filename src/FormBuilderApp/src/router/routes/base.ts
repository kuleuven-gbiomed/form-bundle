import { RoutePermissionName } from "../routePermissions";
import FormBuilderRoutes from "./formBuilder";

export default [
  {
    path: "/",
    name: RoutePermissionName.Home,
    meta: { permission: RoutePermissionName.Home },
    component: () => import("@/views/HomeView.vue"),
  },
  {
    path: "/forms",
    name: RoutePermissionName.FormsOverview,
    meta: { permission: RoutePermissionName.FormsOverview },
    component: () => import("@/views/formsOverview/FormsOverviewView.vue"),
  },
  {
    path: "/form/add",
    name: RoutePermissionName.FormAdd,
    meta: { permission: RoutePermissionName.FormAdd },
    component: () => import("@/views/formAdd/formAddView.vue"),
  },
  // loads form by uid and redirects to form-list (and checks if the form is allready deconstructed)
  {
    path: "/form/:uid",
    name: RoutePermissionName.FormListByUid,

    component: () => import("@/views/formList/FormListByUidView.vue"),
    props: true,
  },
  {
    path: "/form/:id(\\d+)",
    component: () => import("@/views/FormBuilderView.vue"),
    props: true,
    redirect: { name: RoutePermissionName.FormList },
    children: FormBuilderRoutes,
  },
];
