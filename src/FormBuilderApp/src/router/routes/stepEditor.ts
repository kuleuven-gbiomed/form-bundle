import { RoutePermissionName } from "../routePermissions";

export const enum StepDetailRouteName {
  General = "stepDetailGeneral",
  RolesAndSettings = "stepDetailRolesAndSettings",
  TextsAndMessages = "stepDetailTextsAndMessages",
  Email = "stepDetailEmail",
  Sms = "stepDetailSms",
  Webhook = "stepDetailWebhook",
  Json = "stepDetailJson",
}

export default [
  {
    path: "step",
    redirect: { name: RoutePermissionName.FormWorkflow },
  },
  {
    path: "step/:stepNumber(\\d+)",
    name: "stepDetail",
    component: () =>
      import("@/components/formBuilder/formWorkflow/detail/StepDetail.vue"),
    redirect: { name: StepDetailRouteName.General },
    children: [
      {
        path: "general",
        name: StepDetailRouteName.General,
        component: () => import("@/views/formWorkflow/NameTimingView.vue"),
      },
      {
        path: "roles-settings",
        name: StepDetailRouteName.RolesAndSettings,
        component: () =>
          import("@/views/formWorkflow/RolesAndSettingsView.vue"),
      },
      {
        path: "texts-messages",
        name: StepDetailRouteName.TextsAndMessages,
        component: () =>
          import("@/views/formWorkflow/TextsAndMessagesView.vue"),
      },
      {
        path: "email",
        name: StepDetailRouteName.Email,
        component: () => import("@/views/formWorkflow/EmailMessagesView.vue"),
      },
      {
        path: "json",
        name: StepDetailRouteName.Json,
        component: () => import("@/views/formWorkflow/RawDetailView.vue"),
      },
    ],
  },
];
