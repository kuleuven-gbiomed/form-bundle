import { RoutePermissionName } from "../routePermissions";
import NodeEditorRoutes from "./nodeEditor";
import StepEditorRoutes from "./stepEditor";

export default [
  // {
  //   path: "form-list",
  //   name: RoutePermissionName.FormList,
  //   meta: { permission: RoutePermissionName.FormList },
  //   component: () => import("@/components/formBuilder/formList/FormList.vue"),
  // },
  {
    path: "form-list/:questionId(\\d+)?",
    name: RoutePermissionName.FormList,
    meta: { permission: RoutePermissionName.FormList },
    component: () => import("@/components/formBuilder/formList/FormList.vue"),
    children: NodeEditorRoutes,
  },
  {
    path: "raw",
    name: RoutePermissionName.FormRaw,
    meta: { permission: RoutePermissionName.FormRaw },
    component: () => import("@/views/formRaw/FormRawView.vue"),
  },
  {
    path: "general",
    name: RoutePermissionName.FormGeneral,
    meta: { permission: RoutePermissionName.FormGeneral },
    component: () =>
      import("@/components/formBuilder/formGeneral/FormSettings.vue"),
  },
  {
    path: "workflow",
    name: RoutePermissionName.FormWorkflow,
    meta: { permission: RoutePermissionName.FormWorkflow },
    component: () =>
      import("@/components/formBuilder/formWorkflow/FormWorkflow.vue"),
    children: StepEditorRoutes,
  },
  {
    path: "permissions",
    name: RoutePermissionName.FormPermissions,
    meta: { permission: RoutePermissionName.FormPermissions },
    component: () => import("@/views/formPermissions/FormPermissionsView.vue"),
  },
];
