export const enum NodeEditorRouteName {
  Labels = "nodeEditorLabels",
  Properties = "nodeEditorProperties",
  Calculation = "nodeEditorCalculation",
  Options = "nodeEditorOptions",
  Logic = "nodeEditorLogic",
  Prefill = "nodeEditorPrefill",
  Permissions = "nodeEditorPermissions",
  Json = "nodeEditorJson",
}

export default [
  {
    path: "labels",
    name: NodeEditorRouteName.Labels,
    component: () => import("@/views/formList/formListEditor/LabelsView.vue"),
  },
  {
    path: "properties",
    name: NodeEditorRouteName.Properties,
    component: () =>
      import("@/views/formList/formListEditor/PropertiesView.vue"),
  },
  {
    path: "calculation",
    name: NodeEditorRouteName.Calculation,
    component: () =>
      import("@/views/formList/formListEditor/CalculationView.vue"),
  },
  {
    path: "options",
    name: NodeEditorRouteName.Options,
    component: () => import("@/views/formList/formListEditor/OptionsView.vue"),
  },
  {
    path: "logic",
    name: NodeEditorRouteName.Logic,
    component: () => import("@/views/formList/formListEditor/LogicView.vue"),
  },
  {
    path: "prefill",
    name: NodeEditorRouteName.Prefill,
    component: () => import("@/views/formList/formListEditor/PrefillView.vue"),
  },
  {
    path: "permissions",
    name: NodeEditorRouteName.Permissions,
    component: () =>
      import("@/views/formList/formListEditor/PermissionsView.vue"),
  },
  {
    path: "json",
    name: NodeEditorRouteName.Json,
    component: () =>
      import(
        "@/components/formBuilder/formList/nodeEditor/NodePropertiesRaw.vue"
      ),
  },
];
