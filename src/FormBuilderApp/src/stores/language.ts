import { defineStore, acceptHMRUpdate } from "pinia";
import { ref } from "vue";

export const useLanguageStore = defineStore("language", () => {
  const languages = ref<string[]>([]);
  const activeLanguage = ref<string>("en");

  function addLanguage(language: string) {
    const lang = language.toLowerCase();
    if (languages.value.find((el) => el === lang) === undefined) {
      languages.value.push(lang);
    }
  }

  function removeLanguage(language: string) {
    const lang = language.toLowerCase();
    languages.value = languages.value.filter((l) => l !== lang);
  }

  return {
    languages,
    activeLanguage,
    addLanguage,
    removeLanguage,
  };
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useLanguageStore, import.meta.hot));
}
