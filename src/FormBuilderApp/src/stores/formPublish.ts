import { defineStore } from "pinia";
import axios from "axios";

interface ValidationMessage {
  type: string;
  message: string;
}

interface PublishState {
  isLoading: boolean;
  isValidated: boolean;
  isValid: boolean;
  validationMessages: ValidationMessage[];
}

export const useFormPublishStore = defineStore({
  id: "formPublish",
  state: (): PublishState => ({
    isLoading: false,
    isValidated: false,
    isValid: false,
    validationMessages: [],
  }),
  actions: {
    validateForm(id: number): void {
      this.isLoading = true;
      void axios
        .get<ValidationMessage[]>(`/fb/${id}/validate`)
        .then((response) => {
          this.isLoading = false;
          this.isValidated = true;
          this.validationMessages = response.data;
          if (this.validationMessages.length === 0) {
            this.isValid = true;
          } else {
            this.isValid = false;
          }
        });
    },
    resetValidation(): void {
      this.isLoading = false;
      this.isValidated = false;
      this.isValid = false;
    },
  },
  getters: {},
});
