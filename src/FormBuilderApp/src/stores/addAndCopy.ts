import { useDragAndDropStore } from "./dragAndDropStore";
import { defineStore, acceptHMRUpdate } from "pinia";
import type {
  FormNode,
  CategoryNode,
  NodeRelation,
  QuestionNode,
} from "@/api/queries/types/formTypes";
import { useFormStore } from "./form";
import { getNodeRelations } from "@/api/queries/getNodeRelations";
import { useLanguageStore } from "./language";
import { RelativeRelation } from "@/api/queries/types/formTypes";
import {
  below,
  isChildPosition,
  nextPosition,
} from "@/functions/NodePositionFunctions";
import { ref, computed } from "vue";

export type ActionType =
  | "add_category"
  | "add_matrix"
  | "add_question_text"
  | "add_question_number"
  | "add_question_choice"
  | "add_question_date"
  | "add_question_upload"
  | "add_read_only_view_radar_chart"
  | "add_read_only_view_basic"
  | "move_node"
  | "copy_node";
export const ActionType = {
  add: {
    matrix: "add_matrix",
    category: "add_category",
    questionText: "add_question_text",
    questionNumber: "add_question_number",
    questionChoice: "add_question_choice",
    questionDate: "add_question_date",
    questionUpload: "add_question_upload",
    readOnlyViewRadarChart: "add_read_only_view_radar_chart",
    readOnlyViewBasic: "add_read_only_view_basic",
  },
  move: "move_node",
  copy: "copy_node",
} as const;

export type InsertDirection = "in" | "above" | "below";

export const useAddAndCopyStore = defineStore("addAndCopy", () => {
  const actionType = ref<ActionType>(ActionType.add.category);
  const insertDirection = ref<InsertDirection>("in");
  const copySelectedNode = ref<FormNode | undefined>(undefined);
  const moveSelectedNode = ref<FormNode | undefined>(undefined);
  const nodeRelations = ref<NodeRelation[]>([]);

  const formStore = useFormStore();
  const languageStore = useLanguageStore();
  const dragAndDropStore = useDragAndDropStore();

  function resetStore() {
    dragAndDropStore.resetStore();
    copySelectedNode.value = undefined;
    moveSelectedNode.value = undefined;
    nodeRelations.value = [];
    insertDirection.value = "in";
    actionType.value = ActionType.add.category;
  }

  async function selectNodeForCopying(node: QuestionNode | CategoryNode) {
    copySelectedNode.value = node;
    if (node.type === "category") {
      nodeRelations.value = await getNodeRelations(
        formStore.form.id,
        copySelectedNode.value.id,
        languageStore.activeLanguage,
      );
    } else {
      nodeRelations.value = [];
    }
  }
  async function selectNodeForMoving(node: QuestionNode | CategoryNode) {
    moveSelectedNode.value = node;
    nodeRelations.value = await getNodeRelations(
      formStore.form.id,
      moveSelectedNode.value.id,
      languageStore.activeLanguage,
    );
  }
  function clearNodeForCopying() {
    copySelectedNode.value = undefined;
    dragAndDropStore.droppedSelectedNode = undefined;
  }
  function clearNodeForMoving() {
    moveSelectedNode.value = undefined;
    dragAndDropStore.droppedSelectedNode = undefined;
  }

  const showAddDialog = computed(
    (): boolean =>
      actionType.value !== ActionType.move &&
      actionType.value !== ActionType.copy &&
      dragAndDropStore.droppedSelectedType !== undefined,
  );
  const showCopyDialog = computed(
    (): boolean =>
      actionType.value === ActionType.copy &&
      dragAndDropStore.droppedSelectedNode !== undefined,
  );
  const showMoveDialog = computed(
    (): boolean =>
      actionType.value === ActionType.move &&
      dragAndDropStore.droppedSelectedNode !== undefined,
  );
  const targetPosition = computed(() => {
    return dragAndDropStore.droppedSelectedNode
      ? insertDirection.value === "above"
        ? dragAndDropStore.droppedSelectedNode.position
        : insertDirection.value === "below"
          ? nextPosition(dragAndDropStore.droppedSelectedNode.position)
          : [...dragAndDropStore.droppedSelectedNode.position, 0] // case "in"
      : [];
  });
  const relationsAbove = computed(() =>
    nodeRelations.value.filter(
      (relation: NodeRelation) => relation.relativePosition === "above",
    ),
  );
  const relationsBelow = computed(() =>
    nodeRelations.value.filter(
      (relation: NodeRelation) => relation.relativePosition === "below",
    ),
  );

  const problematicRelations = computed(
    (): { above: NodeRelation[]; below: NodeRelation[] } => {
      if (dragAndDropStore.droppedSelectedNode === undefined) {
        return { above: [], below: [] };
      }

      const problematicAbove = nodeRelations.value.filter(
        (relation) =>
          moveSelectedNode.value &&
          !isChildPosition(
            moveSelectedNode.value.position,
            relation.targetNode.position,
          ) &&
          relationIsBetterBelow(relation.relationType) &&
          !below(targetPosition.value, relation.targetNode.position),
      );
      const problematicBelow = nodeRelations.value.filter(
        (relation) =>
          moveSelectedNode.value &&
          !isChildPosition(
            moveSelectedNode.value.position,
            relation.targetNode.position,
          ) &&
          relationIsBetterAbove(relation.relationType) &&
          below(targetPosition.value, relation.targetNode.position),
      );
      return {
        above: problematicAbove,
        below: problematicBelow,
      };
    },
  );

  const relationsOutsideCopiedCategory = computed((): NodeRelation[] => {
    if (copySelectedNode.value?.type === "question") {
      return [];
    }
    return nodeRelations.value.filter(
      (relation) =>
        !isChildPosition(
          copySelectedNode.value?.position ?? [],
          relation.targetNode.position,
        ),
    );
  });

  return {
    // state
    actionType,
    insertDirection: insertDirection,
    copySelectedNode,
    moveSelectedNode,
    nodeRelations,

    // actions
    resetStore,
    selectNodeForCopying,
    selectNodeForMoving,
    clearNodeForCopying,
    clearNodeForMoving,

    // getters
    showAddDialog,
    showCopyDialog,
    showMoveDialog,
    targetPosition,
    relationsAbove,
    relationsBelow,
    problematicRelations,
    relationsOutsideCopiedCategory,
  };
});

function relationIsBetterBelow(relType: RelativeRelation): boolean {
  return (
    relType === RelativeRelation.CALCULATION_OF ||
    relType === RelativeRelation.RADAR_CHART_THAT_USES ||
    relType === RelativeRelation.PREFILLED_BY ||
    relType === RelativeRelation.UNLOCKABLE_BY
  );
}
function relationIsBetterAbove(relType: RelativeRelation): boolean {
  return (
    relType === RelativeRelation.SCORE_FOR ||
    relType === RelativeRelation.RADAR_CHART_SOURCE ||
    relType === RelativeRelation.PREFILLS ||
    relType === RelativeRelation.UNLOCKS
  );
}

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useAddAndCopyStore, import.meta.hot));
}
