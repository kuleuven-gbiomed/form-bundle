import { RoutePermissionName } from "@/router/routePermissions";
import { defineStore } from "pinia";
import { getSettings, type Settings } from "@/api/queries/getSettings";

export type Permission = "READ" | "WRITE" | "NONE";
export const Permission = {
  Read: "READ",
  Write: "WRITE",
  None: "NONE",
} as const;

export interface SettingsState extends Settings {
  isLoading: boolean;
  isLoaded: boolean;
  unfoldUpToLevel: number;
  setForceOnCommands: boolean;
  settingsDialogVisible: boolean;
}

export const useSettingsStore = defineStore({
  id: "settings",
  state: (): SettingsState => ({
    isLoading: false,
    isLoaded: false,
    availableLanguages: [],
    defaultLanguage: "en",
    availableRoles: [],
    availableAdminRoles: [],
    formBuilderUser: "",
    formBuilderUserRoles: [],
    formBuilderPermissions: {},
    logo: "",
    unfoldUpToLevel: 1, // collapse categories in formListView below set level. set to 0 or 1 to improve responsiveness.
    setForceOnCommands: false,
    settingsDialogVisible: false,
  }),
  actions: {
    async loadSettings() {
      if (!this.isLoaded) {
        this.isLoading = true;
        const settings = await getSettings();
        this.$patch(settings);
        this.isLoading = false;
        this.isLoaded = true;
      }
    },
    showSettingsDialog() {
      if (
        this.formBuilderPermissions[
          "allowForcePublishOnFormWithSubmissions"
        ] === Permission.Write
      ) {
        this.settingsDialogVisible = true;
      }
    },
  },
  getters: {
    getPermissionByName: (state) => {
      return (permissionName: string) =>
        state.formBuilderPermissions[permissionName] === undefined
          ? Permission.Write
          : state.formBuilderPermissions[permissionName];
    },
    routesWithPermissions: (state) =>
      Object.values(RoutePermissionName).filter(
        (routeName) => routeName in state.formBuilderPermissions,
      ),
    routePermissions(): Record<RoutePermissionName, Permission> {
      return this.routesWithPermissions.reduce<
        Record<RoutePermissionName, Permission>
      >(
        (accumulator, route) => ({
          ...accumulator,
          [route]: this.formBuilderPermissions[route],
        }),
        {} as Record<RoutePermissionName, Permission>,
      );
    },
  },
});
