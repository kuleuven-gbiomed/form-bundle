import { defineStore, acceptHMRUpdate } from "pinia";
import { computed, readonly, ref } from "vue";
import type {
  WorkflowPermissions,
  FlowPermissionRecord,
  FlowPermissionTemplate,
  PermissionNode,
} from "@/api/queries/types/permissionTypes";
import { useFormStore } from "@/stores/form";
import { getOverviewWorkflowPermissions } from "@/api/queries/getOverviewWorkflowPermissions";
import { getNodePermissions } from "@/api/queries/getNodePermissions";
import { getWorkflowPermissionTemplates } from "@/api/queries/getWorkflowPermissionTemplates";

export interface PermissionDetail {
  flowPermission: FlowPermissionRecord;
  templateId: number;
}

export const usePermissionStore = defineStore("permissions", () => {
  const formStore = useFormStore();

  // permissions for entire form
  const isLoading = ref<boolean>(false);
  const permissions = ref<WorkflowPermissions>({ steps: [], roles: [] });
  async function loadPermissions(formId: number) {
    isLoading.value = true;
    permissions.value = await getOverviewWorkflowPermissions(formId);
    isLoading.value = false;
  }

  // permissions for single node
  const emptyPermissionDetail: PermissionDetail = {
    flowPermission: {},
    templateId: 0,
  } as const;
  const isLoadingDetail = ref<boolean>(false);
  const permissionDetail = ref<PermissionDetail>(emptyPermissionDetail);
  const originalPermissionDetail = ref<PermissionDetail>(emptyPermissionDetail);

  async function loadNodePermissions(formNodeId: number) {
    isLoadingDetail.value = true;

    const result = await getNodePermissions(formStore.form.id, formNodeId);
    permissionDetail.value = result;
    originalPermissionDetail.value = readonly(structuredClone(result));

    isLoadingDetail.value = false;
  }

  // these are for the permissiondetail in the permissions overview
  const node = ref<PermissionNode>();
  const nodeId = computed(() => node.value?.id ?? 0);
  const nodeContentHash = computed(() => node.value?.contentHash ?? "");

  const nodeQuestion = ref<string>("");
  const showPermissionDetail = ref<boolean>(false);
  async function selectNodePermissionDetail(
    formNode: PermissionNode,
    question: string,
  ) {
    showPermissionDetail.value = true;
    node.value = formNode;
    nodeQuestion.value = question;

    await loadNodePermissions(formNode.id);
  }

  // permission templates
  const permissionTemplates = ref<FlowPermissionTemplate[]>([]);
  async function loadPermissionTemplates(formId: number) {
    permissionTemplates.value = await getWorkflowPermissionTemplates(formId);
  }
  const createNewTemplate = ref<boolean>(false);

  // view toggles
  const showPermissionsTemplates = ref<boolean>(false);
  const showPermissionTemplateManager = ref<boolean>(false);

  return {
    // state
    isLoading,
    isLoadingDetail,
    permissions,
    permissionDetail,
    originalPermissionDetail,
    showPermissionDetail,
    node,
    nodeId,
    nodeContentHash,
    nodeQuestion,
    showPermissionsTemplates,
    showPermissionTemplateManager,
    permissionTemplates,
    createNewTemplate,

    //actions
    loadPermissions,
    loadNodePermissions,
    selectNodePermissionDetail,
    loadPermissionTemplates,
  };
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(usePermissionStore, import.meta.hot));
}
