import { FormNode, MatrixCategoryNode } from "@/api/queries/types/formTypes";
import { acceptHMRUpdate, defineStore } from "pinia";
import { computed, ref } from "vue";

export const enum DragType {
  AddCopyMove = "addCopyMove",
  Relation = "relation",
}

export interface MatrixQuestion {
  matrix: MatrixCategoryNode;
  colIndex: number;
}

export const useDragAndDropStore = defineStore("dragAndDrop", () => {
  const draggedNodeId = ref<number>();
  const droppedSelectedNode = ref<FormNode>();
  const droppedSelectedMatrixQuestion = ref<MatrixQuestion>();
  const hoveredNodeId = ref<number>();
  const hoveredMatrixQuestion = ref<MatrixQuestion>();
  const isDragging = ref(false);
  const dragType = ref<DragType>();

  function resetStore() {
    isDragging.value = false;
    draggedNodeId.value = undefined;
    droppedSelectedNode.value = undefined;
    droppedSelectedMatrixQuestion.value = undefined;
    hoveredNodeId.value = undefined;
    hoveredMatrixQuestion.value = undefined;
  }

  // General Dragging

  function startDrag(type: DragType, nodeId?: number) {
    isDragging.value = true;
    hoveredNodeId.value = undefined;
    hoveredMatrixQuestion.value = undefined;
    dragType.value = type;
    draggedNodeId.value = nodeId;
  }

  function endDrag() {
    isDragging.value = false;
    hoveredNodeId.value = undefined;
    hoveredMatrixQuestion.value = undefined;
    dragType.value = undefined;
  }

  function hoverNode(nodeId: number | undefined) {
    if (isDragging.value) hoveredNodeId.value = nodeId;
  }

  function hoverMatrixQuestion(matrixQuestion: MatrixQuestion) {
    if (isDragging.value) {
      hoveredMatrixQuestion.value = matrixQuestion;
    }
  }

  // Dragging of nodes

  function endNodeDrag() {
    draggedNodeId.value = undefined;
    endDrag();
  }

  function startAddCopyMoveDrag(nodeId?: number) {
    if (nodeId === undefined) startDrag(DragType.AddCopyMove);
    else startDrag(DragType.AddCopyMove, nodeId);
  }

  function startRelationDrag(nodeId: number) {
    startDrag(DragType.Relation, nodeId);
  }

  const droppedSelectedType = computed<
    "question" | "category" | "matrix" | undefined
  >(
    () =>
      droppedSelectedNode.value?.type ??
      (droppedSelectedMatrixQuestion.value ? "matrix" : undefined),
  );

  return {
    resetStore,
    isDragging,
    dragType,
    hoveredNodeId,
    hoverNode,
    hoveredMatrixQuestion,
    hoverMatrixQuestion,
    startDrag,
    endDrag,

    // dragging of nodes
    draggedNodeId,
    droppedSelectedNode,
    droppedSelectedType,
    droppedSelectedMatrixQuestion,
    endNodeDrag,

    startAddCopyMoveDrag,
    startRelationDrag,
  };
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useDragAndDropStore, import.meta.hot));
}
