import { defineStore, acceptHMRUpdate } from "pinia";
import { getWorkflowStep } from "@/api/queries/getWorkflowStep";
import type { Step, StepNode } from "@/api/queries/types/workflowTypes";
import { useFormStore } from "./form";
import router from "@/router";
import { computed, ref } from "vue";
import { getWorkflowSchema } from "@/api/queries/getWorkflowSchema";
import { useLanguageStore } from "./language";
import { StepNodeCoordinate } from "@/api/commands/UpdateWorkflowSchema";
import { StepDetailRouteName } from "@/router/routes/stepEditor";
import { RoutePermissionName } from "@/router/routePermissions";

export interface SimulationPeriod {
  start?: Date;
  end?: Date;
}

export const useWorkflowStore = defineStore("workflow", () => {
  const isLoading = ref(false);
  const isStepLoading = ref(false);
  const isStepReloading = ref(false);
  const step = ref<Step>();

  const simulationPeriod = ref<SimulationPeriod>({
    start: undefined,
    end: undefined,
  });

  const showValidation = ref(false);

  const formStore = useFormStore();
  const languageStore = useLanguageStore();

  async function setCurrentStep(stepNumber: number) {
    await router.push({
      name: StepDetailRouteName.General,
      params: { stepNumber: stepNumber },
    });
    showValidation.value = false;
  }

  async function refresh() {
    await formStore.hardRefreshForm();
    await loadWorkflowSchema();
    await reloadStep();
  }

  const workflowSchema = ref<StepNode[]>([]);
  const workflowSchemaOriginal = ref<StepNode[]>([]);
  async function loadWorkflowSchema() {
    isLoading.value = true;
    const response = await getWorkflowSchema(
      formStore.form.id,
      languageStore.activeLanguage,
    );
    workflowSchema.value = response;
    workflowSchemaOriginal.value = structuredClone(response);
    isLoading.value = false;
  }

  const newWorkflowSchemaCoordinates = computed<StepNodeCoordinate[]>(() =>
    workflowSchema.value
      .filter((node) => "computedPosition" in node)
      .map((node) =>
        node.type === "add"
          ? {
              id: "add",
              x: node.computedPosition.x,
              y: node.computedPosition.y,
            }
          : {
              id: node.data.id.toString(),
              x: node.computedPosition.x,
              y: node.computedPosition.y,
            },
      ),
  );
  const currentWorkflowSchemaCoordinates = computed<StepNodeCoordinate[]>(() =>
    workflowSchemaOriginal.value
      .filter((node) => "position" in node)
      .map((node) =>
        node.type === "add"
          ? {
              id: "add",
              x: node.position.x,
              y: node.position.y,
            }
          : {
              id: node.data.id.toString(),
              x: node.position.x,
              y: node.position.y,
            },
      ),
  );

  const stepCount = computed(
    () =>
      workflowSchema.value.filter(
        (element) => element.type === "fixed" || element.type === "relative",
      ).length,
  );

  async function loadStep(stepNumber: number) {
    isStepLoading.value = true;
    await getWorkflowStep(formStore.form.id, stepNumber).then(
      async (response) => {
        if (response.data) {
          step.value = response.data;
        } else {
          await resetStep();
        }
      },
    );
    isStepLoading.value = false;
  }

  async function reloadStep() {
    const currentStep = step.value;
    if (currentStep && formStore.form.id !== 0) {
      isStepReloading.value = true;
      await loadStep(currentStep.sequence + 1);
      isStepReloading.value = false;
    }
  }

  async function resetStep() {
    step.value = undefined;
    await router.push({ name: RoutePermissionName.FormWorkflow });
  }

  function resetSimulationPeriod(): void {
    simulationPeriod.value = {
      start: undefined,
      end: undefined,
    };
  }

  const isSimulationPeriod = computed(
    () =>
      simulationPeriod.value.start instanceof Date &&
      simulationPeriod.value.end instanceof Date &&
      simulationPeriod.value.start <= simulationPeriod.value.end,
  );

  function showValidationScreen(): void {
    showValidation.value = true;
  }
  function hideValidationScreen(): void {
    showValidation.value = false;
  }

  if (import.meta.hot) {
    import.meta.hot.accept(acceptHMRUpdate(useWorkflowStore, import.meta.hot));
  }

  return {
    isLoading,
    isStepLoading,
    isStepReloading,
    workflowSchemaOriginal,
    workflowSchema,
    currentWorkflowSchemaCoordinates,
    newWorkflowSchemaCoordinates,
    stepCount,
    step,
    setCurrentStep,
    isSimulationPeriod,
    simulationPeriod,
    showValidation,
    loadWorkflowSchema,
    refresh,
    loadStep,
    reloadStep,
    resetStep,
    resetSimulationPeriod,
    showValidationScreen,
    hideValidationScreen,
  };
});
