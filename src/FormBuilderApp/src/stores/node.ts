import {
  MaybeScoreQuestion,
  isMaybeScoreQuestion,
} from "./../api/queries/types/subtypesForNodeRelations";
import { defineStore, acceptHMRUpdate } from "pinia";
import {
  findNodeByUid,
  useFormStore,
  findNodeById,
  getQuestionConditionsForNodeUid,
  getNodeByPosition,
} from "./form";
import { useLanguageStore } from "./language";
import type {
  ChoiceQuestionNode,
  FormNode,
  QuestionNode,
} from "@/api/queries/types/formTypes";
import { computed } from "vue";
import { ChoiceOption } from "@/api/queries/getChoiceQuestions";
import { ChoiceQuestion } from "@/api/queries/getChoiceQuestions";
import {
  isChoiceQuestionNode,
  isNumberQuestionNode,
  isTextQuestionNode,
} from "@/api/queries/types/formTypes";
import { isDefined } from "@vueuse/core";

export type ScoreQuestionWithWeighing = MaybeScoreQuestion & {
  weighing: number;
};

export type UnlockingMode = "multi" | "single";
export const UNLOCKING_MODES = {
  MULTI: "multi" as UnlockingMode,
  SINGLE: "single" as UnlockingMode,
};

const languageStore = useLanguageStore();
const formStore = useFormStore();

export const useNodeStore = defineStore("node", () => {
  const node = computed(() =>
    findNodeById(formStore.selectedNodeId, formStore.form.rootNode),
  );

  const scoreQuestions = computed<ScoreQuestionWithWeighing[]>(() => {
    if (
      node.value?.type === "question" &&
      "globalScoreCalculationDependencyConfigs" in node.value.configScoring
    ) {
      return node.value.configScoring.globalScoreCalculationDependencyConfigs.flatMap(
        (item) => {
          const question = findNodeByUid(
            item.scoreNodeUid,
            formStore.form.rootNode,
          );
          return question && isMaybeScoreQuestion(question)
            ? [
                {
                  ...question,
                  weighing: item.weighing,
                },
              ]
            : [];
        },
      );
    }
    return [];
  });

  const prefillSources = computed<QuestionNodeSummary[]>(() => {
    if (
      !(
        node.value &&
        (isTextQuestionNode(node.value) || isChoiceQuestionNode(node.value))
      )
    )
      return [];

    return node.value.configPrefill.prefillingQuestionUids.flatMap((uid) => {
      const question = findNodeByUid(uid, formStore.form.rootNode);
      return question?.type === "question"
        ? [getQuestionNodeSummary(question)]
        : [];
    });
  });

  const unlockingMode = computed<UnlockingMode>(() => {
    return multiUnlockQuestions.value.length > 0
      ? UNLOCKING_MODES.MULTI
      : UNLOCKING_MODES.SINGLE;
  });

  const multiUnlockQuestions = computed<ChoiceQuestion[]>(() => {
    if (node.value?.type !== "question") {
      return [];
    }

    const conditions = Object.entries(
      node.value.configMultiUnlock.multiUnlockingQuestionUids,
    ).map(([choiceId, optionIds]): ChoiceQuestion => {
      const question = findNodeByUid(
        choiceId,
        formStore.form.rootNode,
      ) as ChoiceQuestionNode;

      return {
        id: question.id,
        label: question.info.label[languageStore.activeLanguage] ?? "",
        options: question.options.flatMap((option): ChoiceOption[] => {
          if (optionIds.includes(option.uid)) {
            return [
              {
                id: option.id,
                label: option.info.label[languageStore.activeLanguage] ?? "",
                uid: option.uid,
                unlocksQuestionIds: option.unlocksQuestionIds,
              },
            ];
          } else {
            return [];
          }
        }),
        uid: question.uid,
        position: question.position,
      };
    });

    return conditions;
  });

  const questionConditions = computed<ChoiceQuestion[]>(() => {
    if (node.value?.type !== "question") {
      return [];
    }

    const conditions = getQuestionConditionsForNodeUid(
      node.value.uid,
      formStore.form.rootNode,
    ).map(
      (question): ChoiceQuestion => ({
        id: question.id,
        label: question.info.label[languageStore.activeLanguage] ?? "",
        options: question.options.flatMap((option): ChoiceOption[] => {
          if (
            option.unlocksQuestionIds.find((uid) => uid === node.value?.uid) !==
            undefined
          ) {
            return [
              {
                id: option.id,
                label: option.info.label[languageStore.activeLanguage] ?? "",
                uid: option.uid,
                unlocksQuestionIds: option.unlocksQuestionIds,
              },
            ];
          } else {
            return [];
          }
        }),
        uid: question.uid,
        position: question.position,
      }),
    );

    return conditions;
  });

  const selectedNodeIsMatrixQuestion = computed(
    () => isDefined(node) && nodeIsMatrixQuestion(node.value),
  );

  const selectedNodeIsMatrixRow = computed(
    () => isDefined(node) && nodeIsMatrixRow(node.value),
  );

  const nodeParents = computed(() =>
    isDefined(node) ? getNodeParents(node.value) : [],
  );

  return {
    node,
    nodeParents,
    selectedNodeIsMatrixQuestion,
    selectedNodeIsMatrixRow,
    questionConditions,
    scoreQuestions,
    prefillSources,
    multiUnlockQuestions,
    unlockingMode,
  };
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useNodeStore, import.meta.hot));
}

export interface QuestionNodeSummary {
  id: number;
  question: string;
  uid: string;
  contentHash: string;
  position: number[];
}

function getQuestionNodeSummary(question: QuestionNode): QuestionNodeSummary {
  return {
    id: question.id,
    question: question.info.label[languageStore.activeLanguage] ?? "",
    uid: question.uid,
    contentHash: question.contentHash,
    position: question.position,
  };
}

export function getMaxScoreForQuestion(question: MaybeScoreQuestion) {
  return isNumberQuestionNode(question)
    ? question.configProps.max
    : question.configScoring.scoringParameters.maxScore;
}

export function getMinScoreForQuestion(question: MaybeScoreQuestion) {
  return isNumberQuestionNode(question)
    ? question.configProps.min
    : question.configScoring.scoringParameters.minScore;
}

export function getMinMaxScoreForQuestion(question: MaybeScoreQuestion) {
  return {
    min: getMinScoreForQuestion(question),
    max: getMaxScoreForQuestion(question),
  };
}

export const nodeIsMatrixQuestion = (node: FormNode): boolean =>
  node.type === "question" &&
  getNodeByPosition(node.position.slice(0, -2), formStore.form.rootNode)
    .displayMode === "matrix";

export const nodeIsMatrixRow = (node: FormNode): boolean =>
  node.type === "category" &&
  getNodeByPosition(node.position.slice(0, -1), formStore.form.rootNode)
    .displayMode === "matrix";

export const getNodeParents = (node: FormNode): FormNode[] =>
  node.position
    .map((_, index, array) =>
      getNodeByPosition(array.slice(0, index + 1), formStore.form.rootNode),
    )
    .slice(1);
