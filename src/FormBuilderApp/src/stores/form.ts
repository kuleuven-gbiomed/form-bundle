import {
  isMatrixCategoryNode,
  MatrixCategoryNode,
  QuestionNode,
} from "./../api/queries/types/formTypes";
import {
  isPrefilledQuestion,
  PrefilledQuestion,
} from "./../api/queries/types/subtypesForNodeRelations";
import { acceptHMRUpdate, defineStore } from "pinia";
import { useFormPublishStore } from "./formPublish";
import {
  ChoiceQuestionNode,
  Form,
  FormNode,
  isRadarChartNode,
  RadarChartNode,
  RoleSummary,
  StepSummary,
} from "@/api/queries/types/formTypes";
import { useLanguageStore } from "@/stores/language";
import { getForm } from "@/api/queries/getForm";
import { renderRoleLabel } from "@/functions/roles";
import { computed, ref, watch } from "vue";
import router from "@/router";
import {
  getRelatedNodes,
  NodeRelations,
  RelationLookUpInfo,
} from "@/functions/nodeRelations";
import {
  CalculationQuestion,
  isCalculationQuestion,
} from "@/api/queries/types/subtypesForNodeRelations";
import { NodePosition } from "@/functions/NodePositionFunctions";
import { RoutePermissionName } from "@/router/routePermissions";

const emptyForm: Form = {
  id: 0,
  uid: "",
  targetingType: "",
  currentVersionNumber: 1,
  languages: ["en"],
  rolesThatCouldParticipate: [],
  contentHash: "",
  unPublishedItemCount: 0,
  showPositionNumbers: false,
  enableReadOnlyChoiceAnswerOptionsToggling: false,
  info: {
    label: {
      en: "Empty",
    },
    description: {},
    roleDescriptions: [],
  },
  workFlow: {
    id: 0,
    stepsType: "stepsWithFixedDateStart",
    contentHash: "",
    steps: [],
  },
  rootNode: {
    id: 0,
    sequence: 0,
    level: 0,
    position: [0],
    uid: "form",
    type: "category",
    subType: "category",
    required: false,
    info: { label: {}, description: {}, roleDescriptions: [] },
    hidden: false,
    displayMode: "default",
    firstHeaderCellTitle: {},
    contentHash: "",
    children: [],
  },
};

export interface FormFilter {
  role: RoleSummary;
  step: StepSummary;
  applied: boolean;
}

export const useFormStore = defineStore("form", () => {
  const isFormLoading = ref<boolean>(true);
  const isFormReloading = ref<boolean>(false);
  const form = ref<Form>(emptyForm);
  const filter = ref<FormFilter>({
    role: { role: "", label: "" },
    step: { id: 0, uid: "", label: "" },
    applied: false,
  });
  const menuSelectedNode = ref<FormNode>();
  const isDialogNodeDelete = ref<boolean>(false);

  const selectUnlockingOptionDialog = ref({
    visible: false,
    unlockingQuestion: undefined as ChoiceQuestionNode | undefined,
    lockedId: 0,
  });
  const selectMultiUnlockingOptionDialog = ref({
    visible: false,
    unlockingQuestion: undefined as ChoiceQuestionNode | undefined,
    lockedId: 0,
  });
  const setScoreWeightDialog = ref({
    visible: false,
    calculationQuestionId: 0,
    calculationQuestionLabel: "",
    calculationQuestionPosition: [] as NodePosition,
    scoreQuestionId: 0,
    scoreQuestionLabel: "",
    scoreQuestionPosition: [] as NodePosition,
  });
  const setChartRelationDialog = ref({
    visible: false,
    chartParticipantQuestion: undefined as QuestionNode | undefined,
    chartParticipantQuestionLabel: "",
    chartQuestionId: 0,
    chartQuestionLabel: "",
    chartQuestionPosition: [] as NodePosition,
  });

  const languageStore = useLanguageStore();
  const publishStore = useFormPublishStore();

  async function loadForm(id: number) {
    if (form.value.id !== id) {
      isFormLoading.value = true;

      // reset the state
      publishStore.resetValidation();
      clearFilter();
      relationsToLookUpForHighlight.value = [];

      // load the form and init the languages
      form.value = await getForm(id);
      languageStore.languages = form.value.languages;
      languageStore.activeLanguage = form.value.languages[0];
      isFormLoading.value = false;

      publishStore.validateForm(form.value.id);
    }
  }

  async function hardRefreshForm() {
    isFormReloading.value = true;

    publishStore.resetValidation();

    // load the form with the filter still applied
    let _filter = undefined;
    if (filter.value.role.role && filter.value.step.uid) {
      _filter = {
        role: filter.value.role.role,
        stepUid: filter.value.step.uid,
      };
    }
    form.value = await getForm(form.value.id, _filter);
    languageStore.languages = form.value.languages;
    isFormReloading.value = false;

    publishStore.validateForm(form.value.id);
  }

  async function selectNode(id: number) {
    await router.push({
      params: { questionId: id },
      query: router.currentRoute.value.query,
    });
  }

  async function deselectNode() {
    await router.push({
      name: RoutePermissionName.FormList,
      query: router.currentRoute.value.query,
    });
  }

  async function showMatrixEditor(node: MatrixCategoryNode) {
    await router.push({
      query: {
        matrix: node.id,
      },
    });
  }

  async function hideMatrixEditor() {
    await router.push({
      query: { matrix: undefined },
    });
  }

  const selectedMatrixNodeId = computed(() => {
    const questionIdString = router.currentRoute.value.query.matrix;
    if (typeof questionIdString === "string" && questionIdString !== "") {
      return parseInt(questionIdString);
    }
    return 0;
  });

  const selectedMatrixNode = computed(() => {
    const node = findNodeById(selectedMatrixNodeId.value, form.value.rootNode);
    if (node && isMatrixCategoryNode(node)) {
      return node;
    }
    return undefined;
  });

  // ------------------------------------------------------ relationHiglights
  const relationsToLookUpForHighlight = ref<RelationLookUpInfo[]>([]);

  function setRelationToHighlight(relation: RelationLookUpInfo) {
    relationsToLookUpForHighlight.value.push(relation);
  }

  function unsetRelationToHighlight(relation: RelationLookUpInfo) {
    relationsToLookUpForHighlight.value =
      relationsToLookUpForHighlight.value.filter(
        (r) => r.type !== relation.type,
      );
  }

  function clearRelationsToHighlight() {
    relationsToLookUpForHighlight.value = [];
  }

  // NodeRelations is a map of RelationType to a list of node ids
  const relationsToHighlightInFormList = computed<NodeRelations>(() => {
    let relations: NodeRelations = {};
    relationsToLookUpForHighlight.value.forEach((relation) => {
      relations = {
        ...relations,
        ...getRelatedNodes(relation, form.value.rootNode),
      };
    });

    return relations;
  });

  function deleteNode(node: FormNode) {
    menuSelectedNode.value = node;
    isDialogNodeDelete.value = true;
  }

  // ------------------------------------------------------------------------------ filter actions
  function clearFilter() {
    filter.value = {
      role: { role: "", label: "" },
      step: { id: 0, uid: "", label: "" },
      applied: false,
    };
  }

  const selectedNodeId = computed(() => {
    const questionIdString = router.currentRoute.value.params.questionId;
    if (typeof questionIdString === "string" && questionIdString !== "") {
      return parseInt(questionIdString);
    }
    return 0;
  });

  watch(selectedNodeId, () => {
    clearRelationsToHighlight();
  });

  // select highest node id
  async function selectedHighestNodeId() {
    await selectNode(findHighestId(form.value.rootNode.children, 0));
  }

  const rootNodeChildren = computed((): FormNode[] => {
    if (filter.value.applied) {
      return filterEmptyCategories(form.value.rootNode.children);
    }
    return form.value.rootNode.children;
  });
  const stepSummaries = computed((): StepSummary[] =>
    form.value.workFlow.steps.map((step, index) => ({
      id: step.id,
      uid: step.uid,
      label:
        step.info.label[languageStore.activeLanguage.toLowerCase()] ??
        (index + 1).toString(),
    })),
  );
  const roleSummaries = computed((): RoleSummary[] =>
    form.value.rolesThatCouldParticipate.map((role) => ({
      role: role,
      label: renderRoleLabel(role),
    })),
  );

  // ------------------------------------------------------ recursively unfold node
  const recursivelyUnfoldNodePosition = ref<NodePosition>();

  function recursivelyUnfoldNode(node: FormNode) {
    recursivelyUnfoldNodePosition.value = undefined;
    recursivelyUnfoldNodePosition.value = node.position;
  }

  return {
    //state
    isFormLoading,
    isFormReloading,
    recursivelyUnfoldNode,
    recursivelyUnfoldNodePosition,
    form,
    filter,
    menuSelectedNode,
    isDialogNodeDelete,
    selectedNodeId,
    selectedMatrixNodeId,
    selectedMatrixNode,
    selectUnlockingOptionDialog,
    selectMultiUnlockingOptionDialog,
    setScoreWeightDialog,
    setChartRelationDialog,

    //actions
    loadForm,
    hardRefreshForm,
    selectNode,
    deselectNode,
    deleteNode,
    clearFilter,
    selectedHighestNodeId,
    showMatrixEditor,
    hideMatrixEditor,

    //getters
    rootNodeChildren,
    stepSummaries,
    roleSummaries,

    // relations
    relationsToLookUpForHighlight,
    setRelationToHighlight,
    unsetRelationToHighlight,
    clearRelationsToHighlight,
    relationsToHighlightInFormList,
  };
});

function findHighestId(nodes: FormNode[], highestId: number): number {
  for (const node of nodes) {
    if (node.type === "question") {
      if (node.id > highestId) highestId = node.id;
    } else {
      if (node.id > highestId) highestId = node.id;
      highestId = findHighestId(node.children, highestId);
    }
  }
  return highestId;
}

const filterEmptyCategories = (t: FormNode[]) =>
  t.flatMap((item): [FormNode] | [] => {
    if (item.type === "question") return [item];
    else {
      const r = filterEmptyCategories(item.children);
      if (r.length) {
        item.children = r;
        return [item];
      }
      return [];
    }
  });

export function findNodeById(id: number, node: FormNode): FormNode | undefined {
  if (node.id === id) {
    return node;
  }
  if (node.type === "category") {
    for (const child of node.children) {
      const foundNode = findNodeById(id, child);
      if (foundNode) {
        return foundNode;
      }
    }
  }
  return undefined;
}

export function findNodeByUid(
  uid: string,
  node: FormNode,
): FormNode | undefined {
  if (node.uid === uid) {
    return node;
  }
  if (node.type === "category") {
    for (const child of node.children) {
      const foundNode = findNodeByUid(uid, child);
      if (foundNode) {
        return foundNode;
      }
    }
  }
  return undefined;
}

// this is probably the most performant way to get a node from the formStore
export function getNodeByPosition(
  position: number[],
  node: FormNode,
): FormNode {
  if (position.length < 2) {
    return node;
  }
  const child = node.children[position[1]];
  return getNodeByPosition(position.slice(1), child);
}

function traverseNodeTree<T extends FormNode>(
  rootNode: FormNode,
  conditionFn: (node: FormNode) => node is T,
): T[] {
  let nodes: T[] = [];
  if (rootNode.type === "category") {
    for (const child of rootNode.children) {
      nodes = nodes.concat(traverseNodeTree(child, conditionFn));
    }
    return nodes;
  }
  if (conditionFn(rootNode)) {
    return [rootNode];
  }
  return [];
}

export function getQuestionConditionsForNodeUid(
  uid: string,
  rootNode: FormNode,
): ChoiceQuestionNode[] {
  const conditionFn = (node: FormNode): node is ChoiceQuestionNode => {
    try {
      return (
        node &&
        node.subType === "question_choice" &&
        node.options.some(
          (option) =>
            option.unlocksQuestionIds &&
            option.unlocksQuestionIds.includes(uid),
        )
      );
    } catch (error) {
      console.error("Error in conditionFn:", error);
      return false;
    }
  };

  return traverseNodeTree(rootNode, conditionFn);
}

export function getMultiQuestionConditionsForNodeUid(
  uid: string,
  rootNode: FormNode,
): ChoiceQuestionNode[] {
  const conditionFn = (node: FormNode): node is ChoiceQuestionNode => {
    try {
      return (
        node &&
        node.subType === "question_choice" &&
        node.options.some(
          (option) =>
            option.multiUnlocksQuestionIds &&
            option.multiUnlocksQuestionIds.includes(uid),
        )
      );
    } catch (error) {
      console.error("Error in conditionFn:", error);
      return false;
    }
  };

  return traverseNodeTree(rootNode, conditionFn);
}

export function getCalculationsForScoreUid(
  uid: string,
  rootNode: FormNode,
): CalculationQuestion[] {
  const conditionFn = (node: FormNode): node is CalculationQuestion =>
    isCalculationQuestion(node) &&
    node.configScoring.globalScoreCalculationDependencyConfigs.some(
      (config) => config.scoreNodeUid === uid,
    );
  return traverseNodeTree(rootNode, conditionFn);
}

export function getNodesPrefilledByNodeUid(
  uid: string,
  rootNode: FormNode,
): FormNode[] {
  const conditionFn = (node: FormNode): node is PrefilledQuestion =>
    isPrefilledQuestion(node) &&
    node.configPrefill.prefillingQuestionUids.includes(uid);
  return traverseNodeTree(rootNode, conditionFn);
}

export function getRadarChartsForNodeUid(
  uid: string,
  rootNode: FormNode,
): RadarChartNode[] {
  const conditionFn = (node: FormNode): node is RadarChartNode =>
    isRadarChartNode(node) &&
    node.chartParticipantQuestions.some((question) => question.uid === uid);
  return traverseNodeTree(rootNode, conditionFn);
}

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useFormStore, import.meta.hot));
}
