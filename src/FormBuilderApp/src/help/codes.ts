export enum helpCodes {
  LABEL_QUESTION_LABEL = "label_question_label",
  LABEL_HELP_POSITION = "label_help_position",

  LABEL_TEST_POSITION = "label_test_position",

  LABEL_WORKFLOW_POPUP = "label_workflow_popup",

  LABEL_INLEIDING_POPUP = "label_inleiding_popup",

  LABEL_OPBOUW_FORM_POPUP = "label_opbouw_form_popup",

  LABEL_PERMISSIONS_POPUP = "label_permissions_popup",

  LABEL_ALGEMEEN_POPUP = "label_algemeen_popup",
}
