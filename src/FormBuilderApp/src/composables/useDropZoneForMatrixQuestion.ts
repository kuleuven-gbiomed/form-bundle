import { MatrixQuestion } from "./../stores/dragAndDropStore";
import { useDragAndDropStore } from "@/stores/dragAndDropStore";

import { MaybeRefOrGetter, useDropZone } from "@vueuse/core";
import { watch, Ref } from "vue";

export function useDropZoneForMatrixQuestion(
  dropZoneRef: MaybeRefOrGetter<HTMLElement | null | undefined>,
  matrixQuestion: Ref<MatrixQuestion>,
  onDrop: (event: DragEvent) => void,
) {
  const dragAndDropStore = useDragAndDropStore();

  const { isOverDropZone } = useDropZone(dropZoneRef, {
    onEnter: onDragEnter,
    onDrop: (_files, event) => onDrop(event),
  });

  watch(
    () => isOverDropZone.value,
    (value) => {
      if (!value) {
        dragAndDropStore.hoveredMatrixQuestion = undefined;
      }
    },
  );

  function onDragEnter() {
    dragAndDropStore.hoverMatrixQuestion(matrixQuestion.value);
    if (dragAndDropStore.isDragging) {
      dragAndDropStore.hoveredMatrixQuestion = matrixQuestion.value;
    }
  }

  return isOverDropZone;
}
