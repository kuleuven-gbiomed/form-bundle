import { FormNode } from "./../api/queries/types/formTypes";
import { useDragAndDropStore } from "@/stores/dragAndDropStore";
import { useFormStore } from "@/stores/form";
import { MaybeRefOrGetter, useDropZone } from "@vueuse/core";
import { watch, Ref } from "vue";

export function useDropZoneForNode(
  dropZoneRef: MaybeRefOrGetter<HTMLElement | null | undefined>,
  node: Ref<FormNode>,
  onDrop: (event: DragEvent) => void,
) {
  const dragAndDropStore = useDragAndDropStore();
  const formStore = useFormStore();

  const { isOverDropZone } = useDropZone(dropZoneRef, {
    onEnter: onDragEnter,
    onDrop: (_files, event) => onDrop(event),
  });

  watch(
    () => isOverDropZone.value,
    (value) => {
      if (!value) {
        dragAndDropStore.hoveredNodeId = undefined;
      }
    },
  );

  function onDragEnter() {
    if (dragAndDropStore.isDragging) {
      dragAndDropStore.hoveredNodeId = node.value.id;
    } else {
      if (node.value.id != formStore.selectedNodeId) {
        dragAndDropStore.hoveredNodeId = node.value.id;
      } else {
        dragAndDropStore.hoveredNodeId = undefined;
      }
    }
  }

  return isOverDropZone;
}
