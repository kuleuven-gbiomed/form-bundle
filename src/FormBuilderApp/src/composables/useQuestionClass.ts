import { Ref, computed } from "vue";
import {
  QuestionNode,
  reverseRelativeRelation,
} from "@/api/queries/types/formTypes";
import { useFormStore } from "@/stores/form";
import { useAddAndCopyStore } from "@/stores/addAndCopy";
import { RelativeRelation } from "@/api/queries/types/formTypes";
import { relativeRelationTailwindBgClass } from "@/styles/relationStyling";
import { useDragAndDropStore } from "@/stores/dragAndDropStore";

export interface QuestionStyling {
  hovered: string;
  status: {
    default: string;
    selected: string;
    dragging: string;
  };
}

export function useQuestionClass(
  question: Ref<QuestionNode>,
  styling: QuestionStyling,
) {
  const addAndCopyStore = useAddAndCopyStore();
  const dragAndDropStore = useDragAndDropStore();
  const formStore = useFormStore();

  const relatedNodeType = computed(
    () =>
      addAndCopyStore.nodeRelations.find(
        (relation) => relation.targetNode.uid === question.value.uid,
      )?.relationType,
  );
  const draggingNodeType = computed(
    () =>
      addAndCopyStore.nodeRelations.find(
        (relation) => relation.sourceNode.uid === question.value.uid,
      )?.relationType,
  );

  const statusClass = computed(() => {
    const selected = question.value.id === formStore.selectedNodeId;
    const dragging =
      dragAndDropStore.isDragging &&
      dragAndDropStore.draggedNodeId === question.value.id;

    if (dragging) return styling.status.dragging;
    if (selected) return styling.status.selected;
    return styling.status.default;
  });

  const draggingRelationClass = computed(() => {
    return relatedNodeType.value
      ? relativeRelationTailwindBgClass(
          reverseRelativeRelation(relatedNodeType.value),
        )
      : draggingNodeType.value
        ? relativeRelationTailwindBgClass(draggingNodeType.value)
        : "";
  });

  function highlightedRelationTypeForNodeUid(nodeUid: string) {
    const relationTypesToHighlight = Object.keys(
      formStore.relationsToHighlightInFormList,
    ) as RelativeRelation[];
    return relationTypesToHighlight.filter((RelationType) =>
      formStore.relationsToHighlightInFormList[RelationType]?.includes(nodeUid),
    );
  }

  const highlightRelationClass = computed(() => {
    return highlightedRelationTypeForNodeUid(question.value.uid)
      .map((relationType) => relativeRelationTailwindBgClass(relationType))
      .join(" ");
  });

  const questionClass = computed(() => {
    const hidden = question.value.hidden;
    const hovered =
      dragAndDropStore.hoveredNodeId === question.value.id &&
      dragAndDropStore.draggedNodeId !== question.value.id;

    const hiddenStyling = "text-gray-400 striped";
    return [
      statusClass.value,
      hidden ? hiddenStyling : "",
      hovered ? styling.hovered : "",
      dragAndDropStore.isDragging ? draggingRelationClass.value : undefined,
      highlightRelationClass.value,
    ].join(" ");
  });

  return questionClass;
}
