import { MatrixQuestion } from "./../stores/dragAndDropStore";
import { Ref, computed } from "vue";
import { ActionType, useAddAndCopyStore } from "@/stores/addAndCopy";
import { DragType, useDragAndDropStore } from "@/stores/dragAndDropStore";

export function useAddMatrixQuestionDropAction(
  matrixQuestion: Ref<MatrixQuestion>,
  allowedActionTypes: ActionType[] | "all" = "all",
) {
  const addAndCopyStore = useAddAndCopyStore();
  const dragAndDropStore = useDragAndDropStore();

  const actionTypeAllowed = computed(
    () =>
      allowedActionTypes === "all" ||
      // addAndCopyStore.actionType in array of allowed types
      (Array.isArray(allowedActionTypes) &&
        allowedActionTypes.includes(addAndCopyStore.actionType)),
  );

  const actionAllowed = computed(() => {
    if (dragAndDropStore.dragType !== DragType.AddCopyMove) return false;
    if (!actionTypeAllowed.value) return false;
    if (
      dragAndDropStore.hoveredMatrixQuestion?.colIndex !==
      matrixQuestion.value.colIndex
    )
      return false;
    return true;
  });

  function onDrop() {
    if (!actionAllowed.value) return;
    dragAndDropStore.droppedSelectedMatrixQuestion = matrixQuestion.value;
    addAndCopyStore.insertDirection = "below";
  }

  return {
    actionAllowed,
    onDrop,
  };
}
