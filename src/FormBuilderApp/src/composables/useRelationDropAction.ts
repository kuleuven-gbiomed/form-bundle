import { useAddConditionCommand } from "@/api/commands/AddCondition";
import { useAddPrefilledSourceCommand } from "@/api/commands/AddPrefillSource";
import { QuestionNode } from "@/api/queries/types/formTypes";
import { parsePosition } from "@/functions/NodePositionFunctions";
import { useDragAndDropStore } from "@/stores/dragAndDropStore";
import { useFormStore, findNodeById } from "@/stores/form";
import { useLanguageStore } from "@/stores/language";
import { useDialog } from "primevue/usedialog";
import { ref, computed, defineAsyncComponent, Component, Ref } from "vue";

export function useRelationDropAction(node: Ref<QuestionNode>) {
  const dragAndDropStore = useDragAndDropStore();
  const languageStore = useLanguageStore();
  const formStore = useFormStore();

  const dialog = useDialog();

  const label = computed(
    (): string =>
      node.value.info.label[languageStore.activeLanguage.toLowerCase()] ??
      "[label]",
  );

  function onDrop(event: DragEvent) {
    if (event.dataTransfer) {
      const _type = event.dataTransfer.getData("dropAction");
      switch (_type) {
        case "unlockOption":
          if (node.value.id != formStore.selectedNodeId) {
            unlockerOptionId.value = parseInt(
              event.dataTransfer.getData("optionId"),
            );
            unlockerId.value = parseInt(
              event.dataTransfer.getData("unlockerId"),
            );
            void executeAddLogicCommandViaDroppedOption();
            dragAndDropStore.hoveredNodeId = undefined;
          }
          break;
        case "lockQuestion":
          if (node.value.id != formStore.selectedNodeId) {
            if (node.value.subType !== "question_choice") {
              dialog.open(ErrorMessages, {
                data: {
                  messages: [
                    "Dit is geen keuze vraag. Gelieve een keuzevraag aan te duiden.",
                  ],
                },
                props: {
                  header: "Oeps!",
                  modal: true,
                },
              });

              dragAndDropStore.hoveredNodeId = undefined;
            } else {
              const lockedId = parseInt(event.dataTransfer.getData("lockedId"));
              formStore.selectUnlockingOptionDialog.lockedId = lockedId;
              formStore.selectUnlockingOptionDialog.unlockingQuestion =
                node.value;
              formStore.selectUnlockingOptionDialog.visible = true;
            }
          }
          break;
        case "multiLockQuestion":
          if (node.value.id != formStore.selectedNodeId) {
            if (node.value.subType !== "question_choice") {
              dialog.open(ErrorMessages, {
                data: {
                  messages: [
                    "Dit is geen keuze vraag. Gelieve een keuzevraag aan te duiden.",
                  ],
                },
                props: {
                  header: "Oeps!",
                  modal: true,
                },
              });

              dragAndDropStore.hoveredNodeId = undefined;
            } else {
              const multiLockedId = parseInt(
                event.dataTransfer.getData("multiLockedId"),
              );
              formStore.selectMultiUnlockingOptionDialog.lockedId =
                multiLockedId;
              formStore.selectMultiUnlockingOptionDialog.unlockingQuestion =
                node.value;
              formStore.selectMultiUnlockingOptionDialog.visible = true;
            }
          }
          break;
        case "assignToCalculation":
          if (node.value.isCalculatedQuestion) {
            formStore.setScoreWeightDialog.scoreQuestionId = parseInt(
              event.dataTransfer.getData("scoreQuestionId"),
            );
            formStore.setScoreWeightDialog.scoreQuestionLabel =
              event.dataTransfer.getData("scoreQuestionLabel");
            formStore.setScoreWeightDialog.scoreQuestionPosition =
              parsePosition(
                event.dataTransfer.getData("scoreQuestionPosition"),
              );
            formStore.setScoreWeightDialog.calculationQuestionId =
              node.value.id;
            formStore.setScoreWeightDialog.calculationQuestionLabel =
              label.value;
            formStore.setScoreWeightDialog.calculationQuestionPosition =
              node.value.position;
            formStore.setScoreWeightDialog.visible = true;
          } else {
            dialog.open(ErrorMessageCalculationQuestion, {
              props: {
                header: "Oeps!",
                modal: true,
              },
            });
            // showErrorMessageCalculationQuestion.value = true;
            dragAndDropStore.hoveredNodeId = undefined;
          }
          break;
        case "assignScore":
          if (
            node.value.subType == "question_number" ||
            (node.value.subType == "question_choice" &&
              !node.value.configProps.multiple)
          ) {
            formStore.setScoreWeightDialog.scoreQuestionId = node.value.id;
            formStore.setScoreWeightDialog.scoreQuestionLabel = label.value;
            formStore.setScoreWeightDialog.scoreQuestionPosition =
              node.value.position;
            formStore.setScoreWeightDialog.calculationQuestionId = parseInt(
              event.dataTransfer.getData("calculationQuestionId"),
            );
            formStore.setScoreWeightDialog.calculationQuestionLabel =
              event.dataTransfer.getData("calculationQuestionLabel");
            formStore.setScoreWeightDialog.calculationQuestionPosition =
              parsePosition(
                event.dataTransfer.getData("calculationQuestionPosition"),
              );
            formStore.setScoreWeightDialog.visible = true;
          } else {
            dialog.open(ErrorMessages, {
              data: {
                HTMLBody:
                  '<div style="width: 500px"><p>Dit is geen vraag die een vraag met score mogelijkheid.<br />Gelieve een <strong>keuze vraag of nummer vraag</strong> aan te duiden.</p></div>',
              },
              props: {
                modal: true,
                header: "Oeps!",
              },
            });
            dragAndDropStore.hoveredNodeId = undefined;
          }
          break;
        case "assignChartParticipant":
          if (
            node.value.subType === "question_number" ||
            (node.value.subType === "question_choice" &&
              !node.value.configProps.multiple)
          ) {
            formStore.setChartRelationDialog.chartParticipantQuestion =
              node.value;
            formStore.setChartRelationDialog.chartParticipantQuestionLabel =
              label.value;
            formStore.setChartRelationDialog.chartQuestionId = parseInt(
              event.dataTransfer.getData("chartQuestionId"),
            );
            formStore.setChartRelationDialog.chartQuestionLabel =
              event.dataTransfer.getData("chartQuestionLabel");
            formStore.setChartRelationDialog.visible = true;
          } else {
            dialog.open(ErrorMessages, {
              data: {
                HTMLBody:
                  '<div style="width: 500px"><p>Dit is geen vraag die een vraag met score mogelijkheid.<br />Gelieve een <strong>keuze vraag of nummer vraag</strong> aan te duiden.</p></div>',
              },
              props: {
                modal: true,
                header: "Oeps!",
              },
            });
            dragAndDropStore.hoveredNodeId = undefined;
          }
          break;
        case "assignPrefillSource": {
          prefilledQuestionId.value = parseInt(
            event.dataTransfer.getData("prefilledQuestionId"),
          );
          const prefilledQuestion = findNodeById(
            prefilledQuestionId.value,
            formStore.form.rootNode,
          );

          if (
            prefilledQuestion?.type !== "question" ||
            prefilledQuestion.id === node.value.id
          )
            break;
          const prefillCheckResult = prefillCheck(
            prefilledQuestion,
            node.value,
          );

          if (prefillCheckResult.allowed) {
            void addPrefilledSourceQuestionCommand();
            dragAndDropStore.hoveredNodeId = undefined;
          } else {
            dialog.open(ErrorMessages, {
              data: { messages: prefillCheckResult.messages },
              props: {
                modal: true,
                header: "Deze vraag kan niet gebruikt worden als prefill bron.",
              },
            });
            dragAndDropStore.hoveredNodeId = undefined;
          }
          break;
        }
      }
    }
  }

  const unlockerOptionId = ref<number>(0);
  const unlockerId = ref<number>(0);
  const { executeCommand: executeAddLogicCommandViaDroppedOption } =
    useAddConditionCommand(node, "unlocker", unlockerId, unlockerOptionId);

  function prefillCheck(
    sourceNode: QuestionNode,
    targetNode: QuestionNode,
  ): { allowed: boolean; messages: string[] } {
    const messages = [];
    if (sourceNode.id === targetNode.id) {
      messages.push("Bron- en doelvraag mogen niet dezelfde vraag zijn");
    }
    if (sourceNode.subType !== targetNode.subType) {
      messages.push("Bron- en doelvraag moeten hetzelfde type zijn");
    }
    if (
      sourceNode.subType === "question_choice" &&
      targetNode.subType === "question_choice" // typescript weet niet dat dit al gecontroleerd is
    ) {
      if (sourceNode.configProps.multiple) {
        messages.push("Bronvraag mag geen meerdere keuzes toestaan");
      }
      if (targetNode.configProps.multiple) {
        messages.push("Doelvraag mag geen meerdere keuzes toestaan");
      }
      if (sourceNode.options.length !== targetNode.options.length) {
        messages.push(
          "Bron- en doelvraag moeten hetzelfde aantal opties hebben",
        );
      }
      for (const [index, option] of sourceNode.options.entries()) {
        for (const [key, value] of Object.entries(option.info.label)) {
          if (value !== targetNode.options[index].info.label[key]) {
            messages.push(
              `De opties van de bron- en doelvraag moeten dezelfde labels hebben: (${key}): '${
                value ?? ""
              }' is niet hetzelfde als '${
                targetNode.options[index].info.label[key] ?? ""
              }'`,
            );
          }
        }
        if (option.score !== targetNode.options[index].score) {
          messages.push(
            `De opties van de bron- en doelvraag moeten dezelfde score hebben: ${
              option.score ?? ""
            } is niet hetzelfde als ${targetNode.options[index].score ?? ""}`,
          );
        }
      }
    }
    return { allowed: messages.length === 0, messages };
  }

  const {
    prefilledQuestionId,
    executeCommand: executePrefilledSourceQuestionCommand,
  } = useAddPrefilledSourceCommand(node);

  async function addPrefilledSourceQuestionCommand() {
    await executePrefilledSourceQuestionCommand();
  }

  const ErrorMessages = defineAsyncComponent<Component>(
    () =>
      import(
        "@/components/formBuilder/formList/formListEditor/dialog/ErrorMessages.vue"
      ),
  );
  const ErrorMessageCalculationQuestion = defineAsyncComponent<Component>(
    () =>
      import(
        "@/components/formBuilder/formList/formListEditor/dialog/ErrorMessageCalculationQuestion.vue"
      ),
  );

  return { onDrop };
}
