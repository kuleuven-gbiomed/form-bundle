import type { FormNode } from "@/api/queries/types/formTypes";
import { Ref, computed } from "vue";
import { ActionType, useAddAndCopyStore } from "@/stores/addAndCopy";
import { isChildPosition } from "@/functions/NodePositionFunctions";
import { DragType, useDragAndDropStore } from "@/stores/dragAndDropStore";

export function useAddCopyMoveDropAction(
  node: Ref<FormNode>,
  allowedActionTypes: ActionType[] | "all" = "all",
) {
  const addAndCopyStore = useAddAndCopyStore();
  const dragAndDropStore = useDragAndDropStore();

  const actionTypeAllowed = computed(
    () =>
      allowedActionTypes === "all" ||
      // addAndCopyStore.actionType in array of allowed types
      (Array.isArray(allowedActionTypes) &&
        allowedActionTypes.includes(addAndCopyStore.actionType)),
  );

  const moveAllowed = computed(
    () =>
      addAndCopyStore.moveSelectedNode !== undefined &&
      dragAndDropStore.hoveredNodeId !== addAndCopyStore.moveSelectedNode.id &&
      !isChildPosition(
        addAndCopyStore.moveSelectedNode.position,
        node.value.position,
      ),
  );

  const actionAllowed = computed(() => {
    if (dragAndDropStore.dragType !== DragType.AddCopyMove) return false;
    if (node.value.id !== dragAndDropStore.hoveredNodeId) return false;
    if (!actionTypeAllowed.value) return false;
    if (addAndCopyStore.actionType === ActionType.move)
      return moveAllowed.value;
    return true;
  });

  function onDrop() {
    if (!actionAllowed.value) return;
    dragAndDropStore.droppedSelectedNode = node.value;
    if (node.value.type == "question") {
      addAndCopyStore.insertDirection = "below";
    } else {
      addAndCopyStore.insertDirection = "in";
    }
  }

  return {
    actionAllowed,
    onDrop,
  };
}
