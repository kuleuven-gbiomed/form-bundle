import {
  RelationType,
  relationTypeForRelativeRelation,
  RelativeRelation,
} from "@/api/queries/types/formTypes";

/** @see tailwind.config.js for class safelist (dymamic classes are tree-shaken by default) */
export const relationStyling = {
  [RelationType.LOCKING]: "pink",
  [RelationType.MULTI_LOCKING]: "red",
  [RelationType.CALCULATING]: "green",
  [RelationType.RADAR_CHART]: "violet",
  [RelationType.PREFILLING]: "yellow",
};

export const relationTailwindBgClass = (relation: RelationType) => {
  const color = relationStyling[relation];
  return `bg-${color}-200`;
};

export const relativeRelationTailwindBgClass = (
  relativeRelation: RelativeRelation,
) => {
  const primary = [
    RelativeRelation.UNLOCKS,
    RelativeRelation.MULTI_UNLOCKS,
    RelativeRelation.CALCULATION_OF,
    RelativeRelation.RADAR_CHART_THAT_USES,
    RelativeRelation.PREFILLED_BY,
  ];

  const color =
    relationStyling[relationTypeForRelativeRelation(relativeRelation)];

  const light = "100";
  const regular = "200";
  const intensity = primary.includes(relativeRelation) ? regular : light;

  return `bg-${color}-${intensity}`;
};
