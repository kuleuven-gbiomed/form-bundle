// filter HTML tags from string
export function stripHtmlTags(str: string) {
  return str.replace(/<[^>]*>/g, "");
}
