export function renderRoleLabel(role: string) {
  return role
    .toLowerCase()
    .replace("role_", "")
    .replace(/_/g, " ")
    .split(" ")
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(" ");
}
