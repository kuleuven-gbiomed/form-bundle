export type NodePosition = number[];

export const isChildPosition = (
  parentPosition: NodePosition,
  childPosition: NodePosition,
): boolean =>
  childPosition.length > parentPosition.length &&
  parentPosition.every((num, i) => num === childPosition[i]);

export function nextPosition(position: NodePosition) {
  const positionCopy = [...position];
  ++positionCopy[position.length - 1];
  return positionCopy;
}

export function parsePosition(nodePositionString: string) {
  const result: number[] = [];
  const arrayPositions = nodePositionString.split(",");
  for (const arrayPosition of arrayPositions) {
    const position = parseInt(arrayPosition);
    result.push(position);
  }
  return result;
}

export function below(
  positionA: NodePosition,
  positionB: NodePosition,
): boolean {
  for (const [index, element] of positionA.entries()) {
    if (typeof positionB[index] === "undefined") {
      return true;
    }
    if (element !== positionB[index]) {
      return element > positionB[index];
    }
  }
  return false;
}

export function renderPositionString(position: NodePosition): string {
  return position
    .slice(1, position.length)
    .map((item) => item + 1)
    .join(".");
}
