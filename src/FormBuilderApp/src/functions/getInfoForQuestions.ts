import type { FormNode } from "@/api/queries/types/formTypes";

export function getInfoForQuestions<T>(
  node: FormNode,
  callback: (node: FormNode) => T | undefined,
): T[] {
  const result: T[] = [];

  if (node.type === "question") {
    const callbackResult = callback(node);
    if (callbackResult !== undefined) {
      result.push(callbackResult);
    }
  }

  for (const child of node.children) {
    result.push(...getInfoForQuestions(child, callback));
  }

  return result;
}
