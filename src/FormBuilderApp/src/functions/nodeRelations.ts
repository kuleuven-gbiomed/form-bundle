import { PrefilledQuestion } from "./../api/queries/types/subtypesForNodeRelations";
import {
  MaybeScoreQuestion,
  MaybeRadarChartSource,
  MaybePrefillQuestion,
  isMaybePrefillQuestion,
  isMaybeRadarChartSource,
  isPrefilledQuestion,
  CalculationQuestion,
  isCalculationQuestion,
  isMaybeScoreQuestion,
} from "@/api/queries/types/subtypesForNodeRelations";
import {
  ChoiceQuestionNode,
  FormNode,
  QuestionNode,
  RadarChartNode,
  isChoiceQuestionNode,
  isRadarChartNode,
  RelativeRelation,
} from "@/api/queries/types/formTypes";
import {
  getCalculationsForScoreUid,
  getMultiQuestionConditionsForNodeUid,
  getNodeByPosition,
  getNodesPrefilledByNodeUid,
  getQuestionConditionsForNodeUid,
  getRadarChartsForNodeUid,
} from "@/stores/form";

export type RelationLookUpInfo =
  | {
      type: RelativeRelation.UNLOCKS;
      node: ChoiceQuestionNode;
      optionIndex: number;
    }
  | {
      type: RelativeRelation.MULTI_UNLOCKS;
      node: ChoiceQuestionNode;
      optionIndex: number;
    }
  | {
      type: RelativeRelation.UNLOCKABLE_BY;
      node: QuestionNode;
      optionId?: number;
    }
  | {
      type: RelativeRelation.MULTI_UNLOCKABLE_BY;
      node: QuestionNode;
      optionId?: number;
    }
  | {
      type:
        | RelativeRelation.PREFILLS
        | RelativeRelation.PREFILLED_BY
        | RelativeRelation.RADAR_CHART_SOURCE
        | RelativeRelation.RADAR_CHART_THAT_USES
        | RelativeRelation.CALCULATION_OF
        | RelativeRelation.SCORE_FOR;
      node: QuestionNode;
    };

export type NodeRelations = Partial<Record<RelativeRelation, string[]>>;
export function getRelatedNodes(
  info: RelationLookUpInfo,
  rootNode: FormNode,
): NodeRelations {
  // get node reactively (the one in info is not reactive!).
  const node = getNodeByPosition(info.node.position, rootNode);
  if (node.type !== "question") {
    console.error("node is not a question", "node:", node);
    return {};
  }

  switch (info.type) {
    case RelativeRelation.UNLOCKS:
      if (!isChoiceQuestionNode(node)) return {};
      return getNodesUnlockedBy(node, info.optionIndex);

    case RelativeRelation.MULTI_UNLOCKS:
      if (!isChoiceQuestionNode(node)) return {};
      return getNodesMultiUnlockedBy(node, info.optionIndex);

    case RelativeRelation.UNLOCKABLE_BY:
      return getNodesUnlocking(node, info.optionId, rootNode);

    case RelativeRelation.MULTI_UNLOCKABLE_BY:
      return getNodesMultiUnlocking(node, info.optionId, rootNode);

    case RelativeRelation.CALCULATION_OF:
      if (!isCalculationQuestion(node)) return {};
      return getScoresForCalculation(node);

    case RelativeRelation.SCORE_FOR:
      if (!isMaybeScoreQuestion(node)) return {};
      return getCalculationsForScore(node, rootNode);

    case RelativeRelation.PREFILLED_BY:
      if (!isPrefilledQuestion(node)) return {};
      return getNodesPrefilling(node);

    case RelativeRelation.PREFILLS:
      if (!isMaybePrefillQuestion(node)) return {};
      return getNodesPrefilledBy(node, rootNode);

    case RelativeRelation.RADAR_CHART_SOURCE:
      if (!isMaybeRadarChartSource(node)) return {};
      return getRadarChartThatUses(node, rootNode);

    case RelativeRelation.RADAR_CHART_THAT_USES:
      if (!isRadarChartNode(node)) return {};
      return getRadarChartSources(node);
  }
}

function getNodesUnlockedBy(
  node: ChoiceQuestionNode,
  optionIndex: number,
): NodeRelations {
  return {
    [RelativeRelation.UNLOCKS]: [node.uid],
    [RelativeRelation.UNLOCKABLE_BY]:
      node.options[optionIndex].unlocksQuestionIds,
  };
}

function getNodesMultiUnlockedBy(
  node: ChoiceQuestionNode,
  optionIndex: number,
): NodeRelations {
  return {
    [RelativeRelation.MULTI_UNLOCKS]: [node.uid],
    [RelativeRelation.MULTI_UNLOCKABLE_BY]:
      node.options[optionIndex].multiUnlocksQuestionIds,
  };
}

function getNodesUnlocking(
  node: QuestionNode,
  optionId: number | undefined,
  rootNode: FormNode,
): NodeRelations {
  return {
    [RelativeRelation.UNLOCKABLE_BY]: [node.uid],
    [RelativeRelation.UNLOCKS]: optionId
      ? getQuestionConditionsForNodeUid(node.uid, rootNode)
          .filter((question) =>
            question.options.some((option) => option.id === optionId),
          )
          .map((question) => question.uid)
      : getQuestionConditionsForNodeUid(node.uid, rootNode).map(
          (question) => question.uid,
        ),
  };
}

function getNodesMultiUnlocking(
  node: QuestionNode,
  optionId: number | undefined,
  rootNode: FormNode,
): NodeRelations {
  return {
    [RelativeRelation.MULTI_UNLOCKABLE_BY]: [node.uid],
    [RelativeRelation.MULTI_UNLOCKS]: optionId
      ? getMultiQuestionConditionsForNodeUid(node.uid, rootNode)
          .filter((question) =>
            question.options.some((option) => option.id === optionId),
          )
          .map((question) => question.uid)
      : getMultiQuestionConditionsForNodeUid(node.uid, rootNode).map(
          (question) => question.uid,
        ),
  };
}

function getScoresForCalculation(node: CalculationQuestion): NodeRelations {
  return {
    [RelativeRelation.CALCULATION_OF]: [node.uid],
    [RelativeRelation.SCORE_FOR]:
      node.configScoring.globalScoreCalculationDependencyConfigs.map(
        (item) => item.scoreNodeUid,
      ),
  };
}

function getCalculationsForScore(
  node: MaybeScoreQuestion,
  rootNode: FormNode,
): NodeRelations {
  return {
    [RelativeRelation.SCORE_FOR]: [node.uid],
    [RelativeRelation.CALCULATION_OF]: getCalculationsForScoreUid(
      node.uid,
      rootNode,
    ).map((item) => item.uid),
  };
}

function getNodesPrefilling(node: PrefilledQuestion): NodeRelations {
  return {
    [RelativeRelation.PREFILLED_BY]: [node.uid],
    [RelativeRelation.PREFILLS]: node.configPrefill.prefillingQuestionUids,
  };
}

function getNodesPrefilledBy(
  node: MaybePrefillQuestion,
  rootNode: FormNode,
): NodeRelations {
  return {
    [RelativeRelation.PREFILLS]: [node.uid],
    [RelativeRelation.PREFILLED_BY]: getNodesPrefilledByNodeUid(
      node.uid,
      rootNode,
    ).map((item) => item.uid),
  };
}

function getRadarChartThatUses(
  node: MaybeRadarChartSource,
  rootNode: FormNode,
): NodeRelations {
  return {
    [RelativeRelation.RADAR_CHART_THAT_USES]: [node.uid],
    [RelativeRelation.RADAR_CHART_SOURCE]: getRadarChartsForNodeUid(
      node.uid,
      rootNode,
    ).map((item) => item.uid),
  };
}

function getRadarChartSources(node: RadarChartNode): NodeRelations {
  return {
    [RelativeRelation.RADAR_CHART_THAT_USES]: [node.uid],
    [RelativeRelation.RADAR_CHART_SOURCE]: node.chartParticipantQuestions.map(
      (q) => q.uid,
    ),
  };
}
