# Form Builder App Glossary
## General

* **Form** and **Template** mean roughly the same thing within the context of the *Form Builder App*.
  Note: This **Form** is for use within the *Form Builder* and must first be constructed to a
  *WorkingVersion* in order to be used within the *Form Bundle*.
* **Node:** question or category.
  * For further subdivision see: `api/queries/types/formTypes.ts`

## Components
### FormsOverview
Overview of available forms to customize.

### FormRaw
View where the full JSON of a Form is displayed.

### FormBuilder
Interface where a Form can be built and customized. (This is not a component or view)

**FormGeneral:** View where the general settings can be adjusted: title, languages, roles, ...

**FormWorkflow:** View where the workflow of a form can be configured.

**FormList:** View where questions and categories i.e. **Nodes** can be added and edited.

* **FormListEditor:** Interface on the left side where the *Nodes*
  within the context of that hierarchy can be edited.
  This interface works mainly with `src/stores/form.ts`

* **NodeProperties:** Interface on the right where the details of a Node
  can be customized. This can then be done in the components:
  * **CategoryProperties**
  * **QuestionProperties**
    * Subdivided into: **ChoiceProperties**, **NumberProperties** and **TextProperties**.

  This interface works mainly with `src/stores/node.ts`

**FormPermissions:** View with an overview of the workflow permissions on
all *Question Nodes* in all steps.

## API

**Queries:** Requests involving data retrieval.

**Commands:** Requests where an adjustment is made, but only returning
whether the command was successful. These are handled in via the *CommandBus/CommandHandler* in the FormBuilder backend.

**StartEdit** and **StopEdit** are listed separately because they function like commands, but not through the *CommandBus*.

### Types (api)
#### Queries
Typescript definitions of the JSON to be received is either included 
in the *query* files:
`src/api/queries`

Or the types for the entire *Form* and *Nodes*:
`api/queries/types/formTypes.ts`

#### Commands
Typescript definitions of the JSON to be sent as command are in the folder below.

`src/api/commands/types`
