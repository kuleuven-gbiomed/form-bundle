import tailwindcssPrimeui from "tailwindcss-primeui";

export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx,css}",
    "./node_modules/primevue/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Inter", "Roboto", "Helvetica", "sans-serif"],
      },
    },
  },
  plugins: [tailwindcssPrimeui],
  // Safelist dynamic color classes for node relationships in /src/styles/relationStyling.ts
  safelist: [
    {
      pattern: /bg-(pink|red|green|violet|yellow)-(100|200|300)/,
    },
  ],
};
