//@ts-check
import eslint from "@eslint/js";
import tseslint from "typescript-eslint";
import pluginVue from "eslint-plugin-vue";
import eslintConfigPrettier from "eslint-config-prettier";
import vueParser from "vue-eslint-parser";

export default tseslint.config(
  eslint.configs.recommended,
  ...tseslint.configs.recommendedTypeChecked,
  ...pluginVue.configs["flat/recommended"],
  {
    rules: {
      "vue/component-name-in-template-casing": "error",
      "vue/no-v-html": "off",
      "vue/multi-word-component-names": [
        "warn",
        {
          ignores: [
            "Button",
            "Divider",
            "Column",
            "Toast",
            "Skeleton",
            "Splitter",
            "Panel",
            "Textarea",
            "Select",
            "Checkbox",
            "Badge",
            "Message",
            "Editor",
            "Accordion",
            "Drawer",
            "Dialog",
            "Menu",
            "draggable",
            "DatePicker",
            "Chart",
            "Card",
          ],
        },
      ],
    },
    plugins: {
      "typescript-eslint": tseslint.plugin,
    },
    languageOptions: {
      parser: vueParser,
      globals: {
        "vue/setup-compiler-macros": true,
        node: true,
      },
      parserOptions: {
        parser: tseslint.parser,
        project: "./tsconfig.app.json",
        extraFileExtensions: [".vue"],
        sourceType: "module",
      },
    },
  },
  eslintConfigPrettier,
  {
    files: ["**/*.js"],
    extends: [tseslint.configs.disableTypeChecked],
  },
);
