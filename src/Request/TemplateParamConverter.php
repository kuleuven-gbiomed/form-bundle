<?php

declare(strict_types=1);

namespace KUL\FormBundle\Request;

use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class TemplateParamConverter implements ParamConverterInterface
{
    public function __construct(private readonly TemplateRepository $templates)
    {
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        $templateId = $request->attributes->get('templateId');

        if (!is_string($templateId) || '' === $templateId) {
            throw new \InvalidArgumentException('Route attribute is missing');
        }

        // get template or fail hard if it doesn't exist
        $template = $this->templates->getOneById($templateId);
        $request->attributes->set('template', $template);

        // both nodeId and nodeUid refer to same request attribute 'node'. usually, nodeUid is used in routes. nodeId is
        // not (anymore) or rarely used as route parameter (afaik only in old, now obsolete, removed admin side routes).
        $nodeIdentifier = $request->attributes->get('nodeUid');
        if (!is_string($nodeIdentifier) || '' === $nodeIdentifier) {
            $nodeIdentifier = $request->attributes->get('nodeId');
        }

        $stepId = $request->attributes->get('stepId');

        // no request attributes to set, then let's not bother retrieving (current) version from Template and quickly return.
        if (!is_string($nodeIdentifier) && !is_string($stepId)) {
            return true;
        }

        // if some element is to be retrieved & set in request attributes, get it from current version, never from working version.
        // only admin side (form-builder) works on the working version and that does not use or pass by this converter.
        $currentVersion = $template->getCurrentVersion();

        // is step to be retrieved, get it or fail hard if given id of step does not refer to existing step in this template's current workflow
        if (is_string($stepId) && '' !== $stepId) {
            $request->attributes->set('step', $currentVersion->getWorkFlow()->getOneStepByUid($stepId));
        }

        // is node to be retrieved, get it or fail hard if given id of node does not refer to existing node in this template's current formList
        if (is_string($nodeIdentifier) && '' !== $nodeIdentifier) {
            $request->attributes->set('node', $currentVersion->getFormList()->getNodeByUid($nodeIdentifier));
        }

        return true;
    }

    public function supports(ParamConverter $configuration)
    {
        return Template::class === $configuration->getClass();
    }
}
