<?php

declare(strict_types=1);

namespace KUL\FormBundle\ConsoleCommand;

use KUL\FormBundle\Client\Revert\ResetService;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * remove a filled-in form with all its history logs (submit & temp saved submissions) and uploaded files.
 *
 * syntax:
 *
 * bin/console kul_form:delete:stored_template_target <id-of-stored-template-target>
 * the id of the stored-template-target can be found in DB table: kuleuven_form_stored_template_target
 *
 * e.g. bin/console kul_form:delete:stored_template_target '2d8cf039-4cb4-4db0-9bde-8d47a93da72d'
 */
class DeleteStoredTemplateTargetCommand extends Command
{
    public function __construct(
        private readonly StoredTemplateTargetRepository $storedTemplateTargetRepository,
        private readonly ResetService $resetService,
    ) {
        parent::__construct('kul_form:delete:stored_template_target');
    }

    protected function configure(): void
    {
        $this->setHelp('this command removes (resets) a filled-in form (i.e. a StoredTemplateTarget) with all its history logs (submit & temp saved submissions) and uploaded files')
            ->addArgument('storedTemplateTargetId', InputArgument::REQUIRED, 'id of the storedTemplateTarget (filled-in form) that must be removed.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $storedTemplateTargetId = $input->getArgument('storedTemplateTargetId');
        $storedTemplateTarget = $this->storedTemplateTargetRepository->getOneOrNullByStoredTemplateTargetId($storedTemplateTargetId);

        if (null === $storedTemplateTarget) {
            $output->writeln('<fg=red> no storedTemplateTarget found for id "'.$storedTemplateTargetId.'"!</>');

            return Command::SUCCESS;
        }

        try {
            $this->resetService->resetStoredTemplateTargetForm($storedTemplateTarget);
        } catch (\Exception $exception) {
            $output->writeln('<fg=red> could not delete storedTemplateTarget with id "'.$storedTemplateTargetId.'": error thrown: '.$exception->getMessage().'!</>');

            return Command::SUCCESS;
        }

        $output->writeln('<fg=green> storedTemplateTarget with id "'.$storedTemplateTargetId.'" is successfully deleted</>');

        return Command::SUCCESS;
    }
}
