<?php

declare(strict_types=1);

namespace KUL\FormBundle\ConsoleCommand;

use KUL\FormBundle\Client\Revert\ResetService;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BulkRemoveAllSubmittedFormsForTemplateCommand extends Command
{
    public function __construct(
        private readonly TemplateRepository $templateRepository,
        private readonly StoredTemplateTargetRepository $storedTemplateTargetRepository,
        private readonly ResetService $resetService,
    ) {
        parent::__construct('kul_form:remove:all_submitted_forms_for_template');
    }

    protected function configure(): void
    {
        $this
            // the command help shown when running the command with the "--help" option
            ->setHelp('This is a destructive operation that removes all submitted forms for a given Template. to execute the deletion, add --execute to the command. This then removes - without possibility for retrieval - all the filled-in form records with all submitted and temp-saved answers (StoredTemplateTargets), the uploaded files (for upload questions), submission-history, etc. The Template self is left unchanged.')
            ->addArgument('templateId', InputArgument::REQUIRED, 'id of the template for which the submitted forms must be removed.')
            ->addOption('execute', null, InputOption::VALUE_NONE, 'add this option to effectively execute the deletion of all submitted forms. without this option, only the amount of submitted forms that would be deleted will be shown.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string $templateId */
        $templateId = $input->getArgument('templateId');
        /** @var bool $executeDeletion */
        $executeDeletion = $input->getOption('execute');

        $template = $this->templateRepository->getOneById($templateId);
        // technically, a template without a published (current) version can never have been submitted, and thus could
        // never have been submitted. but just in case someone tampered with DB data... let's not fail on getting
        // the label from current version
        $templateLabel = 'no template title found';

        if ($template->hasCurrentVersion()) {
            $templateLabel = $template->getCurrentVersion()->getLabel('en');
        } elseif ($template->getWorkingVersion()->hasInfo()) {
            $templateLabel = $template->getWorkingVersion()->getInfo()->getLabel('en');
        }

        $output->writeln('<fg=green> checking for submitted forms for template <'.$templateLabel.'>...</>');

        $storedTemplateTargets = $this->storedTemplateTargetRepository->allForTemplate($template);
        $storedTemplateTargetsCount = count($storedTemplateTargets);

        if (0 === $storedTemplateTargetsCount) {
            $output->writeln('<fg=green> nothing to delete: no submitted forms found for this template </>');

            return Command::SUCCESS;
        }

        $storedTemplateTargetsCountString = (string) $storedTemplateTargetsCount;
        $output->writeln('<fg=green> there are '.$storedTemplateTargetsCountString.' submitted forms found for this template</>');

        if (!$executeDeletion) {
            $output->writeln('<fg=green> to remove all these submitted forms, run the command again with --execute </>');

            return Command::SUCCESS;
        }

        try {
            $this->resetService->resetAllStoredTemplateTargetFormsForTemplate($template);
            $output->writeln('<fg=green> successfully removed '.$storedTemplateTargetsCountString.' submitted forms.</>');
        } catch (\Exception $exception) {
            $output->writeln('<fg=red> could not remove submitted forms, the following exception occurred:</>');
            $output->writeln('<fg=red> EXCEPTION MESSAGE: '.$exception->getMessage().'</>');
            $output->writeln('<fg=red> EXCEPTION TRACE:'.$exception->getTraceAsString().'</>');
        }

        return Command::SUCCESS;
    }
}
