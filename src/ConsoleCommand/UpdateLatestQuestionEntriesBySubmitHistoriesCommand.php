<?php

declare(strict_types=1);

namespace KUL\FormBundle\ConsoleCommand;

use KUL\FormBundle\Client\History\InputNodeSubmitEntry;
use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Entity\StoredTemplateTarget;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * properties for storing last changes on questions were added after this bundle was already used in implementing apps:.
 *
 * @see StoredTemplateTarget::$serializedLatestSavedQuestionEntries
 * @see StoredTemplateTarget::$serializedLatestTemporarySavedQuestionEntries
 * this command rebuilds the values for these properties for each @see StoredTemplateTarget by using the
 * submitHistory for the saved and temporary saved actions.
 * this was build intentionally for usage in  the implementing apps that were not yet aware of those properties.
 * this command can be run at any time without unwanted consequences, including on implementing apps started using
 * this bundle when those properties were already introduced. it won't change anything to the values in those
 * properties.
 */
class UpdateLatestQuestionEntriesBySubmitHistoriesCommand extends Command
{
    private array $updateInfo = [];

    public function __construct(
        private readonly StoredTemplateTargetRepository $storedTemplateTargetRepository,
    ) {
        parent::__construct('kul_form:update:latest_question_entries_by_submit_histories');
    }

    protected function configure(): void
    {
        $this->setHelp('this command rebuilds the values for the serializedLatestSavedQuestionEntries and serializedLatestTemporarySavedQuestionEntries for each StoredTemplateTarget by using the submitHistory for the saved and temporary saved actions');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $storedTemplateTargets = $this->storedTemplateTargetRepository->findAll();

        /** @var StoredTemplateTarget $storedTemplateTarget */
        foreach ($storedTemplateTargets as $storedTemplateTarget) {
            $this->processEntries($storedTemplateTarget, true);
            $this->processEntries($storedTemplateTarget, false);
        }

        $output->writeln('<fg=green> total stored-template-targets processed: '.(string) count($this->updateInfo).'</>');
        /**
         * @var string $storedTemplateTargetId
         * @var array  $updates
         */
        foreach ($this->updateInfo as $storedTemplateTargetId => $updates) {
            $hasForSaves = true === $updates['savesUpdated'];
            $hasForTempSaves = true === $updates['tempSavesUpdated'];
            if (false === $hasForSaves && false === $hasForTempSaves) {
                $output->writeln('<fg=green> stored-template-target "'.$storedTemplateTargetId.'": no entry updates were needed</>');
                continue;
            }
            $output->writeln('<fg=green> stored-template-target "'.$storedTemplateTargetId
                .'": latest save actions updated: '.($hasForSaves ? 'yes' : 'not needed').' | latest temp save actions updated: '.($hasForTempSaves ? 'yes' : 'not needed').'</>');
        }

        return Command::SUCCESS;
    }

    private function processEntries(StoredTemplateTarget $storedTemplateTarget, bool $tempSave): void
    {
        if ($tempSave) {
            $history = $storedTemplateTarget->getTemporarySubmitHistoryForCurrentFormList();
            $entries = $storedTemplateTarget->getLatestTemporarySavedQuestionEntriesForCurrentFormList();
            $key = 'tempSavesUpdated';
        } else {
            $history = $storedTemplateTarget->getSubmitHistoryForCurrentFormList();
            $entries = $storedTemplateTarget->getLatestSavedQuestionEntriesForCurrentFormList();
            $key = 'savesUpdated';
        }

        // no history
        if ($history->isEmpty()) {
            $this->updateInfo[$storedTemplateTarget->getId()][$key] = false;

            return;
        }

        $updatedEntries = $entries->getLatestPerQuestionUpdatedByEntries($history);
        if ($this->entriesDiffer($entries, $updatedEntries)) {
            $this->executeDbUpdate(
                $storedTemplateTarget,
                $tempSave ? 'serializedLatestTemporarySavedQuestionEntries' : 'serializedLatestSavedQuestionEntries',
                json_encode($updatedEntries, \JSON_THROW_ON_ERROR)
            );
            $this->updateInfo[$storedTemplateTarget->getId()][$key] = true;

            return;
        }

        $this->updateInfo[$storedTemplateTarget->getId()][$key] = false;
    }

    private function executeDbUpdate(StoredTemplateTarget $storedTemplateTarget, string $field, string $json): void
    {
        $this->storedTemplateTargetRepository
            ->createQueryBuilder('stt')
            ->update()
            ->set('stt.'.$field, ':json')
            ->where('stt.id = :sttId')
            ->setParameter('sttId', $storedTemplateTarget->getId())
            ->setParameter('json', $json)
            ->getQuery()
            ->execute();
    }

    private function entriesDiffer(SubmitHistory $oldEntries, SubmitHistory $newEntries): bool
    {
        if ($oldEntries->count() !== $newEntries->count()) {
            return true;
        }

        foreach ($newEntries as $newEntry) {
            $oldEntry = $oldEntries->forNodeUid($newEntry->getNodeUid())->first();
            if (!$oldEntry instanceof InputNodeSubmitEntry) {
                return true;
            }

            $oldEntryData = $oldEntry->jsonSerialize();
            foreach ($newEntry->jsonSerialize() as $key => $value) {
                if ($oldEntryData[$key] !== $value) {
                    return true;
                }
            }
        }

        return false;
    }
}
