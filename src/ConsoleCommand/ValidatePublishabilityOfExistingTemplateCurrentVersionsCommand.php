<?php

declare(strict_types=1);

namespace KUL\FormBundle\ConsoleCommand;

use KUL\FormBundle\Domain\Template\Version\Validator\InvalidPublishMessageCollection;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ValidatePublishabilityOfExistingTemplateCurrentVersionsCommand extends Command
{
    public function __construct(
        private readonly TemplateRepository $templateRepository,
    ) {
        parent::__construct('kul_form:validate:publishability_existing_template_current_versions');
    }

    protected function configure(): void
    {
        $this
            // the command help shown when running the command with the "--help" option
            ->setHelp('This command helps to catch any changes on publish validation inside this form-bundle versus already published templates in the DB of your implementing app. This command fetches all templates from your DB and for each checks if the current published version (if present) would still be valid for publish and if not, displays the publish validation errors.')
            ->addOption('show-invalid', null, InputOption::VALUE_NONE, 'outputs only results of the invalid templates with their errors.')
            ->addOption('show-all', null, InputOption::VALUE_NONE, 'outputs results of all templates, with errors for the invalid ones.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var bool $showInvalid */
        $showInvalid = $input->getOption('show-invalid');
        /** @var bool $showAll */
        $showAll = $input->getOption('show-all');

        $totalTemplates = $totalValid = $totalInvalid = $totalWithoutCurrentVersion = 0;

        $templates = $this->templateRepository->findAll();

        $outputTextLines = [];

        foreach ($templates as $template) {
            ++$totalTemplates;

            $currentVersionErrors = new InvalidPublishMessageCollection();
            $workingVersion = $template->getWorkingVersion();
            $label = $workingVersion->hasInfo() ? $workingVersion->getInfo()->getLabel('en') : 'unknown';
            $hasCurrentVersion = false;

            if ($template->hasCurrentVersion()) {
                $currentVersion = $template->getCurrentVersion();
                $hasCurrentVersion = true;
                $label = $currentVersion->getLabel('en');
                // use current version as working version to check publishability of the template
                $template->updateWorkingVersion($currentVersion->toWorkingVersion());
                $currentVersionErrors = $template->getInvalidPublishMessages();
            }

            if (false === $hasCurrentVersion) {
                ++$totalWithoutCurrentVersion;

                if ($showAll) {
                    $outputTextLines = array_merge(
                        $outputTextLines,
                        $this->getTemplateText($label, $template->getId(), '<fg=green>no current version published</>')
                    );
                }

                continue;
            }

            if (0 === $currentVersionErrors->count()) {
                ++$totalValid;

                if ($showAll) {
                    $outputTextLines = array_merge(
                        $outputTextLines,
                        $this->getTemplateText($label, $template->getId(), '<fg=green>current version is valid</>')
                    );
                }

                continue;
            }

            ++$totalInvalid;

            if (false === $showAll && false === $showInvalid) {
                continue;
            }

            $outputTextLines = array_merge(
                $outputTextLines,
                $this->getTemplateText(
                    $label,
                    $template->getId(),
                    "<fg=red>invalid current version. found {$currentVersionErrors->count()} issue(s) blocking (re)publish:</>",
                    array_map(
                        fn (string $error) => '<fg=red>-></> '.$error,
                        $currentVersionErrors->getMessages()
                    )
                )
            );
        }

        $output->writeln(
            array_merge(
                $outputTextLines,
                $this->getEndText($showAll, $showInvalid, $totalTemplates, $totalInvalid, $totalValid, $totalWithoutCurrentVersion)
            )
        );

        return Command::SUCCESS;
    }

    /**
     * @param string[] $bodyTextLines
     */
    private function getTemplateText(string $label, string $id, string $message, array $bodyTextLines = []): array
    {
        $text = [
            '============================================================',
            'label: '.$label,
            'id: '.$id,
            $message,
        ];

        foreach ($bodyTextLines as $line) {
            $text[] = $line;
        }

        $text[] = '============================================================';
        $text[] = '';

        return $text;
    }

    private function getEndText(
        bool $showAll,
        bool $showInvalid,
        int $totalTemplates,
        int $totalInvalid,
        int $totalValid,
        int $totalWithoutCurrentVersion,
    ): array {
        $text = [
            0 === $totalInvalid ? '<fg=green>all templates valid</>' : '<fg=red>Invalid templates found!</>',
            'total templates: '.(string) $totalTemplates.', invalid: '.(string) $totalInvalid.', valid: '.(string) $totalValid.', without current version: '.(string) $totalWithoutCurrentVersion,
        ];

        if (0 === $totalInvalid) {
            $text[] = $showAll ?
                'run \'kul_form:validate:publishability_existing_template_current_versions\' to view summary.' :
                'run \'kul_form:validate:publishability_existing_template_current_versions --show-all\' to view result of all templates.';

            return $text;
        }

        if ($showAll) {
            $text[] = 'run \'kul_form:validate:publishability_existing_template_current_versions\' to view summary.';
            $text[] = 'run \'kul_form:validate:publishability_existing_template_current_versions --show-invalid\' to view result of invalid templates.';

            return $text;
        }

        if ($showInvalid) {
            $text[] = 'run \'kul_form:validate:publishability_existing_template_current_versions\' to view summary.';
            $text[] = 'run \'kul_form:validate:publishability_existing_template_current_versions --show-all\' to view result of all templates.';

            return $text;
        }

        $text[] = 'run \'kul_form:validate:publishability_existing_template_current_versions --show-invalid\' to view result of invalid templates.';
        $text[] = 'run \'kul_form:validate:publishability_existing_template_current_versions --show-all\' to view result of all templates.';

        return $text;
    }
}
