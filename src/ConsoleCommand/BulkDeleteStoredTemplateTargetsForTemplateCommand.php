<?php

declare(strict_types=1);

namespace KUL\FormBundle\ConsoleCommand;

use KUL\FormBundle\Client\Revert\ResetService;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * remove all filled-in forms for a given Template with all history logs (submit & temp saved submissions)
 * and uploaded files for each of the template's filled-in forms.
 *
 * syntax:
 *
 * bin/console kul_form:delete:bulk_stored_template_targets_for_template <id-of-template>
 * the id of the template can be found in DB table: kuleuven_form_template
 *
 * e.g. 1 bin/console kul_form:delete:bulk_stored_template_targets_for_template '2d8cf039-4cb4-4db0-9bde-8d47a93da72d'
 * e.g. 2 bin/console kul_form:delete:bulk_stored_template_targets_for_template '03-eqa-form-datasheet-cf-2023-general'
 */
class BulkDeleteStoredTemplateTargetsForTemplateCommand extends Command
{
    public function __construct(
        private readonly StoredTemplateTargetRepository $storedTemplateTargetRepository,
        private readonly TemplateRepository $templateRepository,
        private readonly ResetService $resetService,
    ) {
        parent::__construct('kul_form:delete:bulk_stored_template_targets_for_template');
    }

    protected function configure(): void
    {
        $this->setHelp('this command removes (resets) all filled-in forms (i.e. a StoredTemplateTargets) for a given template with all history logs (submit & temp saved submissions) and uploaded files of each filled-in form')
            ->addArgument('templateId', InputArgument::REQUIRED, 'id of the template for which each filled-in form (storedTemplateTarget) must be removed.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $templateId = $input->getArgument('templateId');

        try {
            $template = $this->templateRepository->getOneById($templateId);
        } catch (\Exception $exception) {
            if ($exception instanceof \BadMethodCallException) {
                $output->writeln('<fg=red> no template found for id "'.$templateId.'"!</>');
            } else {
                $output->writeln('<fg=red> failed to fetch the template for id "'.$templateId.'"!</>');
            }

            return Command::SUCCESS;
        }

        $storedTemplateTargets = $this->storedTemplateTargetRepository->allForTemplate($template);
        $countFound = \count($storedTemplateTargets);
        if (0 === $countFound) {
            $output->writeln('<fg=green> nothing to remove: no filled-in forms (storedTemplateTargets) found for template with id "'.$templateId.'"</>');

            return Command::SUCCESS;
        } else {
            $output->writeln('<fg=green>now removing '.(string) $countFound.' filled-in forms (storedTemplateTargets) for template with id "'.$templateId.'" ...</>');
        }

        $countRemoved = 0;

        try {
            foreach ($storedTemplateTargets as $storedTemplateTarget) {
                $this->resetService->resetStoredTemplateTargetForm($storedTemplateTarget);
                ++$countRemoved;
            }
        } catch (\Exception $exception) {
            $output->writeln('<fg=red> an error occurred: could not delete storedTemplateTargets: error thrown: '.$exception->getMessage().'!</>');

            return Command::SUCCESS;
        }

        $output->writeln('<fg=green> successfully removed '.(string) $countRemoved.' filled-in forms (storedTemplateTargets) for template with id "'.$templateId.'"</>');

        return Command::SUCCESS;
    }
}
