<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\GlobalScore\Rendering;

/**
 * @implements \IteratorAggregate<array-key, GlobalScoreQuestionResultView>
 */
readonly class GlobalScoreQuestionResultViews implements \IteratorAggregate, \JsonSerializable
{
    /**
     * @var GlobalScoreQuestionResultView[]
     */
    private array $globalScoreQuestionResultViews;

    public function __construct(GlobalScoreQuestionResultView ...$globalScoreQuestionResultViews)
    {
        $this->globalScoreQuestionResultViews = array_values($globalScoreQuestionResultViews);
    }

    public static function empty(): self
    {
        return new self();
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->globalScoreQuestionResultViews);
    }

    public function isEmpty(): bool
    {
        return 0 === count($this->globalScoreQuestionResultViews);
    }

    public function with(GlobalScoreQuestionResultView ...$globalScoreQuestionResultViews): self
    {
        return new self(...$this->globalScoreQuestionResultViews, ...$globalScoreQuestionResultViews);
    }

    public function toArray(): array
    {
        return $this->globalScoreQuestionResultViews;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
