<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\GlobalScore\Rendering;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;

class GlobalScoreQuestionResultView implements \JsonSerializable
{
    public const QUESTION_UID = 'questionUid';
    public const QUESTION_LABEL = 'questionLabel';
    public const QUESTION_TYPE = 'questionType';
    public const QUESTION_NESTED_LABEL = 'questionNestedLabel';
    public const QUESTION_MIN_SCORE = 'questionMinScore';
    public const QUESTION_MAX_SCORE = 'questionMaxScore';
    public const QUESTION_PASS_SCORE = 'questionPassScore';
    public const RESULT_HAS_BEEN_SUBMITTED = 'resultHasBeenSubmitted';
    public const SUBMITTED_RESULT_SCORE = 'submittedResultScore';
    public const SUBMITTED_RESULT_CHOICE_OPTION_LABEL = 'submittedResultChoiceOptionLabel';

    /**
     * note:
     * if the global score question is not required (optional), it could have been submitted as blank, either manually
     * or via auto-calculation. in that case the $submittedResultScore will be null. but since NULL could also mean
     * a result was not yet submitted for a required global score question, we need to differentiate.
     * hence we mark if it was submitted ($resultHasBeenSubmitted).
     * alternatively, we could add a bool argument about the required config of the question (e.g. $globalScoreQuestionIsRequired)
     * and then deduct that if $resultScore is NULL and $globalScoreQuestionIsRequired is FALSE, it was submitted blank,
     * but since this just a view object, it seems better to be explicit, hence the $resultHasBeenSubmitted argument.
     *
     * $submittedResultScore & $submittedResultChoiceOptionLabel:
     * if the global score question is a number question, then the submitted result score (if any) will be the
     * number that the (last submitting) user has manually inputted or that was automatically added via auto-calculation.
     * if the global score question is a choice question, then the submitted result score (if any) will the
     * scoring value (@see OptionInputNode::getScoringValue()) of the answer option that the (last submitting) user has
     * manually selected as answer or that was automatically added via auto-calculation. for UX convenience, we also pass
     * the label of that manually selected or auto-calculated answer option along with the submittedResultScore (mainly
     * because a choice global score  question can have more than one answer option with the same scoring value).
     */
    private function __construct(
        private readonly string $questionUid,
        private readonly string $questionType,
        private readonly string $questionLabel,
        private readonly string $questionNestedLabel,
        private readonly float $questionMinScore,
        private readonly float $questionMaxScore,
        private readonly float $questionPassScore,
        private readonly bool $resultHasBeenSubmitted,
        private readonly ?float $submittedResultScore,
        private readonly ?string $submittedResultChoiceOptionLabel,
    ) {
        $this->guard();
    }

    public static function createForRequiredNumberGlobalScoreQuestionWithSubmittedResult(
        NumberInputNode $globalScoreQuestion,
        string $locale,
        float $submittedResultScore,
    ): self {
        self::guardRequiredGlobalScoreQuestion($globalScoreQuestion, $locale);

        return self::createForGlobalScoreQuestion($globalScoreQuestion, $locale, true, $submittedResultScore, null);
    }

    public static function createForRequiredChoiceGlobalScoreQuestionWithSubmittedResult(
        ChoiceInputNode $globalScoreQuestion,
        string $locale,
        float $submittedResultScore,
        string $submittedResultChoiceOptionLabel,
    ): self {
        self::guardRequiredGlobalScoreQuestion($globalScoreQuestion, $locale);

        return self::createForGlobalScoreQuestion($globalScoreQuestion, $locale, true, $submittedResultScore, $submittedResultChoiceOptionLabel);
    }

    public static function createForOptionalNumberGlobalScoreQuestionWithSubmittedResult(
        NumberInputNode $globalScoreQuestion,
        string $locale,
        ?float $submittedResultScore,
    ): self {
        self::guardOptionalGlobalScoreQuestion($globalScoreQuestion, $locale);

        return self::createForGlobalScoreQuestion($globalScoreQuestion, $locale, true, $submittedResultScore, null);
    }

    public static function createForOptionalChoiceGlobalScoreQuestionWithSubmittedResult(
        ChoiceInputNode $globalScoreQuestion,
        string $locale,
        ?float $submittedResultScore,
        ?string $submittedResultChoiceOptionLabel,
    ): self {
        self::guardOptionalGlobalScoreQuestion($globalScoreQuestion, $locale);

        return self::createForGlobalScoreQuestion($globalScoreQuestion, $locale, true, $submittedResultScore, $submittedResultChoiceOptionLabel);
    }

    public static function createForGlobalScoreQuestionWithoutSubmittedResult(
        ScorableInputNode $globalScoreQuestion,
        string $locale,
    ): self {
        return self::createForGlobalScoreQuestion($globalScoreQuestion, $locale, false, null, null);
    }

    private static function createForGlobalScoreQuestion(
        ScorableInputNode $globalScoreQuestion,
        string $locale,
        bool $resultHasBeenSubmitted,
        ?float $submittedResultScore,
        ?string $submittedResultChoiceOptionLabel,
    ): self {
        $scoringParams = $globalScoreQuestion->getInputScoringParameters();

        return new self(
            $globalScoreQuestion->getUid(),
            $globalScoreQuestion->getInput()::getType(),
            $globalScoreQuestion->getLabel($locale),
            $globalScoreQuestion->getNestedLabel($locale),
            $scoringParams->getMinScore(),
            $scoringParams->getMaxScore(),
            $scoringParams->getPassScore(),
            $resultHasBeenSubmitted,
            $submittedResultScore,
            $submittedResultChoiceOptionLabel
        );
    }

    private static function guardRequiredGlobalScoreQuestion(
        ScorableInputNode $globalScoreQuestion,
        string $locale,
    ): void {
        if (!$globalScoreQuestion->isRequired()) {
            self::throwInvalidArgumentExceptionForGlobalScoreQuestion($globalScoreQuestion, 'is not configured as required', $locale);
        }
    }

    private static function guardOptionalGlobalScoreQuestion(
        ScorableInputNode $globalScoreQuestion,
        string $locale,
    ): void {
        if ($globalScoreQuestion->isRequired()) {
            self::throwInvalidArgumentExceptionForGlobalScoreQuestion($globalScoreQuestion, 'is not configured as optional', $locale);
        }
    }

    private static function throwInvalidArgumentExceptionForGlobalScoreQuestion(
        ScorableInputNode $globalScoreQuestion,
        string $messageSuffix,
        string $locale,
    ): void {
        $uid = $globalScoreQuestion->getUid();
        $label = $globalScoreQuestion->getNestedLabel($locale);

        throw new \InvalidArgumentException("question [$uid | $label] ".$messageSuffix);
    }

    private function guard(): void
    {
        if ($this->questionPassScore > $this->questionMaxScore) {
            throw new \InvalidArgumentException('questionPassScore can not be greater than questionMaxScore');
        }

        if ($this->questionPassScore < $this->questionMinScore) {
            throw new \InvalidArgumentException('questionPassScore can not be less than questionMinScore');
        }

        if (null === $this->submittedResultScore) {
            return;
        }

        if ($this->submittedResultScore < $this->questionMinScore) {
            throw new \InvalidArgumentException('submittedResultScore can not be less than questionMinScore');
        }

        if ($this->submittedResultScore > $this->questionMaxScore) {
            throw new \InvalidArgumentException('submittedResultScore can not be greater than questionMaxScore');
        }
    }

    public function jsonSerialize(): array
    {
        return [
            self::QUESTION_UID => $this->questionUid,
            self::QUESTION_TYPE => $this->questionType,
            self::QUESTION_LABEL => $this->questionLabel,
            self::QUESTION_NESTED_LABEL => $this->questionNestedLabel,
            self::QUESTION_MIN_SCORE => $this->questionMinScore,
            self::QUESTION_MAX_SCORE => $this->questionMaxScore,
            self::QUESTION_PASS_SCORE => $this->questionPassScore,
            self::RESULT_HAS_BEEN_SUBMITTED => $this->resultHasBeenSubmitted,
            self::SUBMITTED_RESULT_SCORE => $this->submittedResultScore,
            self::SUBMITTED_RESULT_CHOICE_OPTION_LABEL => $this->submittedResultChoiceOptionLabel,
        ];
    }
}
