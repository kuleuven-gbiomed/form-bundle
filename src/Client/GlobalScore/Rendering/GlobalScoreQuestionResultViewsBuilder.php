<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\GlobalScore\Rendering;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;

class GlobalScoreQuestionResultViewsBuilder
{
    public static function buildAllowedGlobalScoreResultViewsForClientAccess(
        ClientCombinedAccessObject $accessObject,
        string $locale,
    ): GlobalScoreQuestionResultViews {
        // no access means you don't get to see anything, so most definitely no global score question results
        if (!$accessObject->grantsAccess()) {
            return GlobalScoreQuestionResultViews::empty();
        }

        $allReachableGlobalScoreQuestions = $accessObject->getCurrentVersionFormList()->getFlattenedParentalInputNodes()->getOperatingAsGlobalScoreNodes();
        // no global score questions in this template, or none that are not hidden (directly or indirectly inside a hidden category)
        if ($allReachableGlobalScoreQuestions->isEmpty()) {
            return GlobalScoreQuestionResultViews::empty();
        }

        // only global score questions that grant at least read access in the current open step qualify to be shown a result for.
        // if the accessObject (build based on user-role and current datetime) grants (at least read) access, it means there
        // is an open step. otherwise the user-role would not have any access.
        $reachableReadableGlobalScoreQuestionsInOpenStep = $allReachableGlobalScoreQuestions->getReadableInStep($accessObject->getOpenFlowStep());
        if ($reachableReadableGlobalScoreQuestionsInOpenStep->isEmpty()) {
            return GlobalScoreQuestionResultViews::empty();
        }

        // user's accessing role must have at least read access (directly via read-only access or indirectly via the write access) on the global score question
        $reachableReadableGlobalScoreQuestionsInOpenStepForAccessingRole = $reachableReadableGlobalScoreQuestionsInOpenStep->getReadableForRole($accessObject->getAccessingRole());
        if ($reachableReadableGlobalScoreQuestionsInOpenStepForAccessingRole->isEmpty()) {
            return GlobalScoreQuestionResultViews::empty();
        }

        $allSubmittedQuestionValues = [];
        // if at least once submitted or temp saved yet (i.e. a record exists in DB), there could be global score question results.
        if ($accessObject->hasStoredTemplateTarget()) {
            // get all the submitted (not the temp saved!) answers
            // note: we don't return with empty set here if there are no submitted answers (getSubmittedFormValues) yet (thus including
            // none for global score questions). because we want to return views for not-yet-submitted global score questions also!
            $allSubmittedQuestionValues = $accessObject->getStoredTemplateTarget()->getSubmittedFormValues();
        }

        /** @var array<array-key, GlobalScoreQuestionResultView> $resultViews */
        $resultViews = [];

        // add a result view for all global score questions to which (user)role has current step access, regardless if
        // there is already a result (answer) submitted at this point.
        // if the global score question to which user has current step access, was not yet submitted (no result yet), the
        // (front-end of) the implementing app can opt to show that information if needed/wanted or omit it anyway
        // (@see GlobalScoreQuestionResultView::$resultHasBeenSubmitted )
        /** @var ScorableInputNode $globalScoreQuestion */
        foreach ($reachableReadableGlobalScoreQuestionsInOpenStepForAccessingRole as $globalScoreQuestion) {
            $uid = $globalScoreQuestion->getUid();
            // a question has been submitted if its uid is in the (keys of the) single dimension array submittedValues
            // we add the global score questions without a submitted result as well, but marked as not-submitted
            if (!array_key_exists($globalScoreQuestion->getUid(), $allSubmittedQuestionValues)) {
                $resultViews[] = GlobalScoreQuestionResultView::createForGlobalScoreQuestionWithoutSubmittedResult($globalScoreQuestion, $locale);
                continue;
            }

            // an answered number global score question is stored in submitted form values as 'question-uid' => float
            if ($globalScoreQuestion instanceof NumberInputNode) {
                $resultViews[] = self::buildResultViewForSubmittedGlobalScoreNumberQuestion($globalScoreQuestion, $allSubmittedQuestionValues, $locale);
                continue;
            }

            // an answered choice global score question is stored in submitted form values as 'question-uid' => TRUE
            // and the answer-option selected as answer is stored separately in the same submitted form values
            // in the form of 'answered-choice-answer-option-uid' => TRUE|NULL|string
            if ($globalScoreQuestion instanceof ChoiceInputNode) {
                $resultViews[] = self::buildResultViewForSubmittedGlobalScoreChoiceQuestion($globalScoreQuestion, $allSubmittedQuestionValues, $locale);
                continue;
            }

            // in case we add a third type for global score questions in the future (apart from number & choice).
            throw new \DomainException("unhandled type for global choice question [$uid]: only number questions and choice questions can be handled");
        }

        return new GlobalScoreQuestionResultViews(...$resultViews);
    }

    private static function buildResultViewForSubmittedGlobalScoreNumberQuestion(
        NumberInputNode $globalScoreNumberQuestion,
        array $allSubmittedQuestionValues,
        string $locale,
    ): GlobalScoreQuestionResultView {
        $uid = $globalScoreNumberQuestion->getUid();

        if (!$globalScoreNumberQuestion->operatesAsGlobalScoreNode()) {
            throw new \InvalidArgumentException("Number question with [$uid] is not a global score question");
        }

        if (!array_key_exists($uid, $allSubmittedQuestionValues)) {
            throw new \InvalidArgumentException("Number global score question with [$uid] was never submitted");
        }

        if ($globalScoreNumberQuestion->isRequired()) {
            return GlobalScoreQuestionResultView::createForRequiredNumberGlobalScoreQuestionWithSubmittedResult(
                $globalScoreNumberQuestion,
                $locale,
                $allSubmittedQuestionValues[$uid]
            );
        }

        return GlobalScoreQuestionResultView::createForOptionalNumberGlobalScoreQuestionWithSubmittedResult(
            $globalScoreNumberQuestion,
            $locale,
            $allSubmittedQuestionValues[$uid]
        );
    }

    private static function buildResultViewForSubmittedGlobalScoreChoiceQuestion(
        ChoiceInputNode $globalScoreChoiceQuestion,
        array $allSubmittedQuestionValues,
        string $locale,
    ): GlobalScoreQuestionResultView {
        $uid = $globalScoreChoiceQuestion->getUid();

        if (!$globalScoreChoiceQuestion->operatesAsGlobalScoreNode()) {
            throw new \InvalidArgumentException("Choice question with [$uid] is not a global score question");
        }

        if (!array_key_exists($uid, $allSubmittedQuestionValues)) {
            throw new \InvalidArgumentException("Choice global score question with [$uid] was never submitted");
        }

        // a choice global score question never allows for multiple answers (asserted upon publish of a Template).
        // but in case that ever changes (although it should not), let's assert it here.
        if ($globalScoreChoiceQuestion->getInput()->isMultiple()) {
            throw new \InvalidArgumentException("Choice global score question with [$uid] is configured to allow multiple answers".' This is not allowed and asserted upon publish of the Template to which it belongs Maybe someone tampered directly in the Database or it was altered in the domain logic.');
        }

        // look for the submitted choice-answer option. if the global choice question is not required, it might have been
        // submitted blank. if it is required, there must be a choice-answer option chosen as answer and submitted as such.
        // the answer-option selected & submitted as answer is stored separately in the submitted form values in the
        // form of 'answered-choice-answer-option-uid' => TRUE|NULL|string (null or string if that answer-option is configured
        // to allow additional (required (string) or optional (string|null)) text input
        $submittedAnswerOption = null;
        /** @var OptionInputNode $availableOption */
        foreach ($globalScoreChoiceQuestion->getAvailableOptions() as $availableOption) {
            if (array_key_exists($availableOption->getUid(), $allSubmittedQuestionValues)) {
                $submittedAnswerOption = $availableOption;
                // a global score question never allows multiple answers -> break.
                break;
            }
        }

        $submittedScoreValue = $submittedAnswerOption instanceof OptionInputNode ? $submittedAnswerOption->getScoringValue() : null;
        $submittedAnswerOptionLabel = $submittedAnswerOption instanceof OptionInputNode ? $submittedAnswerOption->getLabel($locale) : null;

        // if the globalScoreChoiceQuestion is optional, a blank (null) value is allowed
        if (!$globalScoreChoiceQuestion->isRequired()) {
            return GlobalScoreQuestionResultView::createForOptionalChoiceGlobalScoreQuestionWithSubmittedResult(
                $globalScoreChoiceQuestion,
                $locale,
                $submittedScoreValue,
                $submittedAnswerOptionLabel
            );
        }

        // if the globalScoreChoiceQuestion is required, and it is submitted (asserted above), then there must be
        // chosen answer-option. if not, then someone tampered with DB data.
        // technically, the globalScoreChoiceQuestion could have been optional in a previous version of the Template,
        // and then altered to required in the current version. if it was validly submitted blank (null) while it was
        // still optional, then technically there could still be a null value now. But that would mean that a new version
        // was invalidly published: a change from optional to required on any question is a breaking version change!
        // publishing of that change into a new version should & would never be allowed in the form-builder without
        // making sure that:
        // either all already filled-in forms and their already submitted answers are still valid against that change (in
        // this case, all optional questions were answered not blank)
        // or either- if not valid anymore - all invalidated filled-in forms were rolled back to a step in which all
        // answers were still valid (i.e. converted to temporary answers, which allow circumventing required questions)
        if (!is_float($submittedScoreValue) || !is_string($submittedAnswerOptionLabel)) {
            throw new \InvalidArgumentException("invalid submitted global choice score question [$uid]: ".'the global choice score question has been submitted and it is configured as required. But no submitted choice answer option was found as chosen answer!');
        }

        return GlobalScoreQuestionResultView::createForRequiredChoiceGlobalScoreQuestionWithSubmittedResult(
            $globalScoreChoiceQuestion,
            $locale,
            $submittedScoreValue,
            $submittedAnswerOptionLabel
        );
    }
}
