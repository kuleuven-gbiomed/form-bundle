<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\Access\ClientTemplateTargetFormAccess;
use KUL\FormBundle\Client\Manage\FormManager;
use KUL\FormBundle\Client\Response\DoAccessDeniedResponse;
use KUL\FormBundle\Client\Revert\AllowToRevertChecker;
use KUL\FormBundle\Client\Route\ClientRoutesBuilder;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use KUL\FormBundle\Entity\SubmissionRepository;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\HttpFoundation\Request;

class TemplateTargetRevertService extends AbstractTemplateTargetService
{
    public function __construct(
        StoredTemplateTargetRepository $storedTemplateTargetRepository,
        SubmissionRepository $submissionRepository,
        ClientRoutesBuilder $clientRoutesBuilder,
        FormManager $formManager,
        string $defaultLocale,
        array $availableAdminRoles,
        int $autoSaveInterval,
        int $idleCheckInterval,
        TemplateRepository $templateRepository,
        private readonly AllowToRevertChecker $allowToRevertChecker,
    ) {
        parent::__construct(
            $storedTemplateTargetRepository,
            $submissionRepository,
            $clientRoutesBuilder,
            $formManager,
            $defaultLocale,
            $availableAdminRoles,
            $autoSaveInterval,
            $idleCheckInterval,
            $templateRepository
        );
    }

    public function findClientAccessForRollbackStep(
        Request $request,
        Template $template,
        TemplateTargetableInterface $target,
        ClientWithTargetAccessInterface $clientTargetAccess,
        StepInterface $rollbackStep,
    ): ?ClientCombinedAccessObject {
        $clientAccess = $this->findClientAccess($request, $template, $target, $clientTargetAccess);

        if (!$clientAccess instanceof ClientCombinedAccessObject) {
            return null;
        }

        $allowedRollbackSteps = $this->allowToRevertChecker->getAllowedRollbackStepsForClientAccess($clientAccess);

        if ($allowedRollbackSteps->hasOneStepByUid($rollbackStep->getUid())) {
            return $clientAccess;
        }

        return null;
    }

    public function findClientAccessForReset(
        Request $request,
        Template $template,
        TemplateTargetableInterface $target,
        ClientWithTargetAccessInterface $clientTargetAccess,
    ): ?ClientCombinedAccessObject {
        $clientAccess = $this->findClientAccess($request, $template, $target, $clientTargetAccess);

        if (!$clientAccess instanceof ClientCombinedAccessObject) {
            return null;
        }

        if ($this->allowToRevertChecker->allowsResetForClientAccess($clientAccess)) {
            return $clientAccess;
        }

        return null;
    }

    public function getReloadUrl(Request $request, ClientCombinedAccessObject $clientAccess): string
    {
        return $this->getStoringViewUrlForAccessingRole(
            $request,
            $this->clientRoutesBuilder->getClientRouteDataSetForTemplate($clientAccess->getTemplate()),
            $clientAccess
        );
    }

    private function findClientAccess(
        Request $request,
        Template $template,
        TemplateTargetableInterface $target,
        ClientWithTargetAccessInterface $clientTargetAccess,
    ): ?ClientCombinedAccessObject {
        $this->assertReferenceDate($template, $target->getReferenceDate());

        $doAccessDeniedResponse = $this->getOneOrNullPreliminaryDoAccessDeniedResponse($template, $clientTargetAccess);

        if ($doAccessDeniedResponse instanceof DoAccessDeniedResponse) {
            return null;
        }

        $formAccess = $this->getNullOrFormAccessForSingleClientAccessingRoleInRequest(
            $request,
            $template,
            $clientTargetAccess,
            $target->getReferenceDate(),
            $target->getReferenceDate()
        );

        if (!$formAccess instanceof ClientTemplateTargetFormAccess) {
            return null;
        }

        return $formAccess->getPrimaryAccess();
    }
}
