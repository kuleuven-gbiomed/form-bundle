<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\History;

use KUL\FormBundle\Domain\Factory\DateTimeImmutableFactory;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionWithAdditionalTextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\CheckBoxInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\DateInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use Webmozart\Assert\Assert;

class InputNodeSubmitEntry implements \JsonSerializable
{
    final public const DATETIME_FORMAT = 'Y-m-d H:i:s';
    final public const KEY_NODE = 'nodeUid';
    final public const KEY_SUBMIT_DATE = 'submitDate';
    final public const KEY_VALUE = 'value';
    final public const KEY_USER_ID = 'userId';
    final public const KEY_PERSON = 'person';
    final public const KEY_STEP_UID = 'stepUid';
    final public const KEY_ROLE = 'role';
    final public const KEY_AUTO_ADDED = 'autoAdded';
    final public const KEY_AUTO_SAVED = 'autoSaved';
    final public const KEY_ORIGINAL_SUBMIT_DATE_BEFORE_ROLLBACK = 'originalSubmitDateBeforeRollback';

    private readonly \DateTimeImmutable $date;
    private readonly string $userId;
    private readonly string $person;
    private readonly string $stepUid;
    private readonly string $role;

    private function __construct(
        private readonly ParentalInputNode $node,
        \DateTimeImmutable $submitDate,
        private readonly mixed $value,
        string $userId,
        string $person,
        string $stepUid,
        string $role,
        private readonly bool $autoAdded = false,
        private readonly bool $autoSaved = false,
        private readonly ?\DateTimeImmutable $originalSubmitDateBeforeRollback = null,
    ) {
        Assert::notEmpty($userId);
        Assert::notEmpty($person);
        Assert::notEmpty($stepUid);
        Assert::notEmpty($role);
        $this->date = DateTimeImmutableFactory::createFromDate($submitDate);
        $this->userId = $userId;
        $this->person = $person;
        $this->stepUid = $stepUid;
        $this->role = $role;

        $this->assertValue();
    }

    /**
     * TODO check this more thoroughly.
     *
     * @throws \Exception
     */
    protected function assertValue(): void
    {
        $node = $this->getNode();
        $value = $this->getRawValue();
        $baseError = self::class.": invalid value for node with uid '".$node->getUid()."'. ";
        switch (true) {
            case $node instanceof TextInputNode:
                if (!(null === $value || is_string($value))) {
                    throw new \Exception($baseError.'value must be null or a string if the input node is an instance of '.TextInputNode::class);
                }
                break;
            case $node instanceof NumberInputNode:
                if (!(null === $value || is_numeric($value))) {
                    throw new \Exception($baseError.'value must be null or numeric if the input node is an instance of '.NumberInputNode::class);
                }
                break;
            case $node instanceof CheckBoxInputNode:
                if (!(null === $value || is_bool($value))) {
                    throw new \Exception($baseError.'value must be null or a boolean if the input node is an instance of '.CheckBoxInputNode::class);
                }
                break;
            case $node instanceof ChoiceInputNode:
                if (!is_array($value)) {
                    throw new \Exception($baseError.'value must be an array if the input is an instance of '.ChoiceInputNode::class);
                }
                break;
            case $node instanceof UploadInputNode:
                if (!(null === $value || is_string($value))) {
                    throw new \Exception($baseError.'value must be null or a string if the input node is an instance of '.UploadInputNode::class);
                }
                break;
            case $node instanceof DateInputNode:
                if (!(null === $value || is_string($value))) {
                    throw new \Exception($baseError.'value must be null or a string if the input node is an instance of '.DateInputNode::class);
                }
                break;
            default:
                throw new \Exception(self::class.': can not process value check for input node of type '.$node::class);
        }
    }

    public static function fromNodeAndDateAndValue(
        ParentalInputNode $node,
        \DateTimeImmutable $submitDate,
        mixed $value,
        string $userId,
        string $person,
        string $stepUid,
        string $role,
        bool $autoAdded = false,
        bool $autoSaved = false,
        ?\DateTimeImmutable $originalSubmitDateBeforeRollback = null,
    ): self {
        return new self($node, $submitDate, $value, $userId, $person, $stepUid, $role, $autoAdded, $autoSaved, $originalSubmitDateBeforeRollback);
    }

    public static function fromNodeAndEntryData(
        ParentalInputNode $node,
        array $entryData,
    ): self {
        $submitDate = \DateTimeImmutable::createFromFormat(self::DATETIME_FORMAT, $entryData[self::KEY_SUBMIT_DATE]);
        if (false === $submitDate) {
            throw new \InvalidArgumentException('can not build a DateTimeImmutable for submitDate from entryData based on key '.self::KEY_SUBMIT_DATE);
        }

        $originalSubmitDateBeforeRollback = null;
        $originalSubmitDateBeforeRollbackData = $entryData[self::KEY_ORIGINAL_SUBMIT_DATE_BEFORE_ROLLBACK] ?? null;
        if (is_string($originalSubmitDateBeforeRollbackData) && '' !== $originalSubmitDateBeforeRollbackData) {
            $originalSubmitDateBeforeRollback = \DateTimeImmutable::createFromFormat(self::DATETIME_FORMAT, $originalSubmitDateBeforeRollbackData);
            if (false === $originalSubmitDateBeforeRollback) {
                throw new \InvalidArgumentException('can not build a DateTimeImmutable for originalSubmitDateBeforeRollback from entryData based on key '.self::KEY_ORIGINAL_SUBMIT_DATE_BEFORE_ROLLBACK);
            }
        }

        return self::fromNodeAndDateAndValue(
            $node,
            $submitDate,
            $entryData[self::KEY_VALUE],
            $entryData[self::KEY_USER_ID],
            $entryData[self::KEY_PERSON],
            $entryData[self::KEY_STEP_UID],
            $entryData[self::KEY_ROLE],
            $entryData[self::KEY_AUTO_ADDED],
            $entryData[self::KEY_AUTO_SAVED],
            $originalSubmitDateBeforeRollback,
        );
    }

    public function getNode(): ParentalInputNode
    {
        return $this->node;
    }

    public function getNodeUid(): string
    {
        return $this->getNode()->getUid();
    }

    public function getSubmitDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function getSubmitDateString(?string $locale = null): string
    {
        return HistoryDateFormatter::format($this->getSubmitDate(), $locale);
    }

    public function isConvertedByRollback(): bool
    {
        return $this->originalSubmitDateBeforeRollback instanceof \DateTimeImmutable;
    }

    public function getOriginalSubmitDateBeforeRollback(): \DateTimeImmutable
    {
        if (!$this->originalSubmitDateBeforeRollback instanceof \DateTimeImmutable) {
            throw new \Exception('no originalSubmitDateBeforeRollback found!');
        }

        return $this->originalSubmitDateBeforeRollback;
    }

    public function getOriginalSubmitDateBeforeRollbackString(?string $locale = null): string
    {
        return HistoryDateFormatter::format($this->getOriginalSubmitDateBeforeRollback(), $locale);
    }

    /**
     * for TextInput & TextareaInput: value will be string
     * for NumberInput: value will be number (float) or a string
     * for CheckBoxInput: value will be boolean
     * for ChoiceInput: value is array of submitted OptionInputNodeUids (regardless of option multiple being true or false, array is used).
     */
    public function getRawValue(): mixed
    {
        return $this->value;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getPerson(): string
    {
        return $this->person;
    }

    public function getStepUid(): string
    {
        return $this->stepUid;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function isAutoAdded(): bool
    {
        return $this->autoAdded;
    }

    public function isAutoSaved(): bool
    {
        return $this->autoSaved;
    }

    /**
     * TODO DV needs some more testing
     * compare raw value of InputNode of given InputNodeSubmitEntry against raw value of InputNode of this source entry.
     *
     * @throws \Exception
     */
    public function equalsNodeValue(self $entry): bool
    {
        $sourceNode = $this->getNode();
        $givenNode = $entry->getNode();

        // check if entries use same node
        if (!($sourceNode->getUid() === $givenNode->getUid())) {
            return false;
        }

        // if same uids, then they will be of same type, but can't harm to double check
        if (!$givenNode instanceof $sourceNode) {
            return false;
        }

        $sourceRawValue = $this->getRawValue();
        $givenRawValue = $entry->getRawValue();

        // compare raw values (strict since all node values are stored in the same way)
        switch (true) {
            case $sourceNode instanceof TextInputNode:
            case $sourceNode instanceof NumberInputNode:
            case $sourceNode instanceof CheckBoxInputNode:
            case $sourceNode instanceof UploadInputNode:
            case $sourceNode instanceof DateInputNode:
                return $sourceRawValue === $givenRawValue;
            case $sourceNode instanceof ChoiceInputNode:
                /*
                 * the answer of a choice question is always a set of @see OptionInputNode items
                 * that raw value is then always an array with those option uids as array keys. the array value for
                 * each option uid is then - in far most cases - the boolean TRUE (to indicate that that option was
                 * chosen as answer). if the optionInputNode uses @see OptionInput. but it can also be a string
                 * or NULL if the inputOptionNode uses @see OptionWithAdditionalTextInput, requiring or allowing users
                 * to 'add' an additional string alongside choosing the option as answer.
                 *
                 * @see OptionInputNode::usesAdditionalTextInput()
                 */
                if (!is_array($sourceRawValue) || !is_array($givenRawValue)) {
                    throw new \InvalidArgumentException('both sourceRawValue and givenRawValue must be arrays');
                }

                if (!(count($sourceRawValue) === count($givenRawValue))) {
                    return false;
                }

                // the raw value for a choice node is always an array with option uids as keys with bool/null/string as values
                foreach ($givenRawValue as $givenChoiceOptionUid => $givenChoiceOptionValue) {
                    // if given choice option is not an answer given for the source choice
                    if (!array_key_exists($givenChoiceOptionUid, $sourceRawValue)) {
                        return false;
                    }
                    // if the value for the given option is not the same as the value for the source option (99% of the
                    // cases the value is bool TRUE, but null/string is possible if user was allowed to give additional 'other' value)
                    if (!($sourceRawValue[$givenChoiceOptionUid] === $givenChoiceOptionValue)) {
                        return false;
                    }
                }

                // same amount of choice option as answers found in both source & given choice answers set of the choice
                // question, and value (bool, null or string) of each of those choice option answers is same in both sets
                return true;
            default:
                throw new \Exception(self::class.': can not process value equality check for input node of type '.$sourceNode::class);
        }
    }

    public function getValue(): mixed
    {
        return $this->getRawValue();
    }

    public function jsonSerialize(): array
    {
        return [
            self::KEY_NODE => $this->getNodeUid(),
            self::KEY_SUBMIT_DATE => $this->getSubmitDate()->format(self::DATETIME_FORMAT),
            self::KEY_VALUE => $this->getRawValue(),
            self::KEY_USER_ID => $this->getUserId(),
            self::KEY_PERSON => $this->getPerson(),
            self::KEY_STEP_UID => $this->getStepUid(),
            self::KEY_ROLE => $this->getRole(),
            self::KEY_AUTO_ADDED => $this->isAutoAdded(),
            self::KEY_AUTO_SAVED => $this->isAutoSaved(),
            self::KEY_ORIGINAL_SUBMIT_DATE_BEFORE_ROLLBACK => $this->isConvertedByRollback() ? $this->getOriginalSubmitDateBeforeRollback()->format(self::DATETIME_FORMAT) : null,
        ];
    }
}
