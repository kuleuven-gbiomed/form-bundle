<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\History;

class HistoryDateFormatter
{
    public static function format(\DateTimeImmutable $datetime, ?string $locale = null): string
    {
        $dateFormatter = \IntlDateFormatter::create($locale, \IntlDateFormatter::MEDIUM, \IntlDateFormatter::MEDIUM, \date_default_timezone_get(), \IntlDateFormatter::GREGORIAN);

        /**
         * @psalm-suppress PossiblyNullReference
         *
         * @var false|string $formattedDate not bool|string as phpstan seems to assume
         */
        $formattedDate = $dateFormatter->format($datetime);
        if (false === $formattedDate) {
            throw new \Exception('failed to format date');
        }

        return $formattedDate;
    }
}
