<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\History;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;

/**
 * @extends ArrayCollection<array-key,InputNodeSubmitEntry>
 */
final class SubmitHistory extends ArrayCollection implements \JsonSerializable
{
    public static function fromFormListAndEntriesData(
        FormList $formList,
        array $entriesData,
    ): self {
        $entries = [];

        foreach ($entriesData as $entryData) {
            $node = $formList->getNodeByUid($entryData[InputNodeSubmitEntry::KEY_NODE]);
            if (!$node instanceof ParentalInputNode) {
                throw new MissingNodeException('no question (parental node) found in formList for uid '.InputNodeSubmitEntry::KEY_NODE);
            }

            $entries[] = InputNodeSubmitEntry::fromNodeAndEntryData($node, $entryData);
        }

        return new self($entries);
    }

    public function getLatestPerQuestionUpdatedByEntries(self $entries): self
    {
        $thisLatestPerQuestion = $this->getLatestPerQuestion();
        // start from existing set.
        $result = $thisLatestPerQuestion->toArrayIndexedByUids();

        // check given set for new or more recent entry for each question (node)
        foreach ($entries->getLatestPerQuestion()->toArray() as $newEntry) {
            $nodeUid = $newEntry->getNodeUid();
            // there will either one or none for this node in existing set of latest entries.
            $existing = $thisLatestPerQuestion->forNodeUid($nodeUid)->first();

            // new entry for node that did not exist in existing entries
            if (!$existing instanceof InputNodeSubmitEntry) {
                $result[$nodeUid] = $newEntry;
                continue;
            }

            // make sure it's always the most recent entry that is kept.
            // theoretically, an entry for the same date-time will not be found twice or more. but it can not harm to
            // check & avoid duplicate entries in the result. note: one use case is older forms, that did not yet track
            // the latest entries separately from full entry history and required update of those latest entries.
            $result[$nodeUid] = $newEntry->getSubmitDate() >= $existing->getSubmitDate() ? $newEntry : $existing;
        }

        return new self(array_values($result));
    }

    public function getLatestPerQuestion(): self
    {
        $result = [];
        foreach ($this->getUniqueNodeUids() as $nodeUid) {
            $latestNodeEntry = $this->forNodeUid($nodeUid)->latest();
            if ($latestNodeEntry instanceof InputNodeSubmitEntry) {
                $result[] = $latestNodeEntry;
            }
        }

        return new self($result);
    }

    /**
     * Sorts InputNodeSubmitEntries by the submit date.
     */
    public function getSortedBySubmitDate(): self
    {
        /** @var \ArrayIterator<array-key,InputNodeSubmitEntry> $iterator */
        $iterator = $this->getIterator();
        $iterator->uasort(
            fn (InputNodeSubmitEntry $a, InputNodeSubmitEntry $b) => ($a->getSubmitDate() < $b->getSubmitDate()) ? -1 : 1
        );

        return new self(iterator_to_array($iterator));
    }

    /**
     * get InputNodeSubmitEntries not auto added.
     */
    public function getNotAutoAdded(): self
    {
        return $this->filter(fn (InputNodeSubmitEntry $entry) => !$entry->isAutoAdded());
    }

    /**
     * Gets the oldest InputNodeSubmitEntry (with the oldest submit date).
     */
    public function oldest(): ?InputNodeSubmitEntry
    {
        $entry = $this->getSortedBySubmitDate()->first();

        return $entry instanceof InputNodeSubmitEntry ? $entry : null;
    }

    /**
     * Gets the latest InputNodeSubmitEntry (with the most recent submit date).
     */
    public function latest(): ?InputNodeSubmitEntry
    {
        $entry = $this->getSortedBySubmitDate()->last();

        return $entry instanceof InputNodeSubmitEntry ? $entry : null;
    }

    /**
     * Gets the InputNodeSubmitEntries for given nodeUid.
     */
    public function forNodeUid(string $nodeUid): self
    {
        return $this->filter(fn (InputNodeSubmitEntry $entry) => $entry->getNodeUid() === $nodeUid);
    }

    public function isNotEmpty(): bool
    {
        return !$this->isEmpty();
    }

    public function getMergedWith(self $collection): self
    {
        return new self(array_merge($this->toArray(), $collection->toArray()));
    }

    /**
     * Returns collection with unique elements.
     */
    public function getUnique(): self
    {
        $elements = $this->toArray();

        $uniqueElements = [];
        foreach ($elements as $key => $element) {
            // check whether element is a duplicate
            if (!in_array($element, $uniqueElements, true)) {
                $uniqueElements[$key] = $element; // preserve the first encountered key (follows array_unique behaviour)
            }
        }

        return new self($uniqueElements);
    }

    /**
     * @return array<array-key, string>
     */
    public function getUniqueNodeUids(): array
    {
        $nodeUids = [];
        foreach ($this->toArray() as $entry) {
            if (in_array($entry->getNodeUid(), $nodeUids, true)) {
                continue;
            }

            $nodeUids[] = $entry->getNodeUid();
        }

        return $nodeUids;
    }

    /**
     * @return array<string, InputNodeSubmitEntry>
     */
    public function toArrayIndexedByUids(): array
    {
        $result = [];

        foreach ($this->toArray() as $entry) {
            $result[$entry->getNodeUid()] = $entry;
        }

        return $result;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
