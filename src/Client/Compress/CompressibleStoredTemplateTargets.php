<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Compress;

/**
 * @implements \IteratorAggregate<array-key, CompressibleStoredTemplateTarget>
 */
class CompressibleStoredTemplateTargets implements \IteratorAggregate
{
    /**
     * @var CompressibleStoredTemplateTarget[]
     */
    private readonly array $compressibleStoredTemplateTargets;

    public function __construct(CompressibleStoredTemplateTarget ...$compressibleStoredTemplateTargets)
    {
        $this->guardUniqueCompressLabels(...$compressibleStoredTemplateTargets);

        $this->compressibleStoredTemplateTargets = array_values($compressibleStoredTemplateTargets);
    }

    public static function empty(): self
    {
        return new self();
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->compressibleStoredTemplateTargets);
    }

    public function isEmpty(): bool
    {
        return 0 === count($this->compressibleStoredTemplateTargets);
    }

    private function guardUniqueCompressLabels(CompressibleStoredTemplateTarget ...$compressibleStoredTemplateTargets): void
    {
        $labels = array_map(fn (CompressibleStoredTemplateTarget $compressibleStoredTemplateTarget) => $compressibleStoredTemplateTarget->getCompressLabel(), $compressibleStoredTemplateTargets);

        if (count($labels) === count(array_unique($labels))) {
            return;
        }

        throw new \DomainException('all compress labels in a collection of compressibleStoredTemplateTargets must be unique!');
    }

    public function with(CompressibleStoredTemplateTarget ...$compressibleStoredTemplateTargets): self
    {
        return new self(...$this->compressibleStoredTemplateTargets, ...$compressibleStoredTemplateTargets);
    }

    public function mergedWith(self $compressibleStoredTemplateTargets): self
    {
        return $this->with(...$compressibleStoredTemplateTargets->toArray());
    }

    public function toArray(): array
    {
        return $this->compressibleStoredTemplateTargets;
    }
}
