<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Compress;

use Symfony\Component\HttpFoundation\StreamedResponse;

final class StreamedZipResponse extends StreamedResponse
{
    public function __construct(
        CompressibleStoredTemplateTargets $compressibleStoredTemplateTargets,
        string $submittedFilesDirectory,
        string $locale,
        string $zipFileName,
        int $limitQuestionLabelLength,
    ) {
        parent::__construct(
            function () use ($compressibleStoredTemplateTargets, $submittedFilesDirectory, $locale, $limitQuestionLabelLength): void {
                SubmittedFilesZipStream::create($compressibleStoredTemplateTargets, $submittedFilesDirectory, $locale, $limitQuestionLabelLength, true);
            },
            200,
            [
                'Content-Type' => 'application/zip',
                'Content-Disposition' => 'attachment;filename="'.$zipFileName.'.zip"',
                'Cache-Control' => 'max-age=0',
            ]
        );
    }
}
