<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Compress;

use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Entity\SavedFile;

class CompressibleSavedFile
{
    private function __construct(private readonly string $fileName, private readonly string $questionLabel, private readonly string $storagePath)
    {
    }

    public static function createFromSavedFile(
        SavedFile $savedFile,
        UploadInputNode $node,
        string $submittedFilesDirectory,
        string $locale,
        ?int $limitQuestionLabelLength,
    ): self {
        if ($savedFile->getNodeUid() === $node->getUid()) {
            return new self(
                $savedFile->getCompressSafeFileName(),
                $node->getCompressSafeLabel($locale, $limitQuestionLabelLength),
                $savedFile->getUploadedFile()->getFilePath($submittedFilesDirectory),
            );
        }

        throw new \InvalidArgumentException('saved file [id='.$savedFile->getUid().'] is not linked to the given upload question [node-uid='.$node->getUid().'] but is instead linked to another upload question [node-uid='.$savedFile->getNodeUid().']');
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function getQuestionLabel(): string
    {
        return $this->questionLabel;
    }

    public function getStoragePath(): string
    {
        return $this->storagePath;
    }
}
