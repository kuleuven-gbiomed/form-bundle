<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Compress;

use ZipStream\ZipStream;

/**
 * this service can be used to stream a prebuild, default ordered, OS safe zip file containing all submitted files used
 * as answers for upload questions. the submitted files are by default ordered by question and by @see StoredTemplateTarget.
 * the filenames and the questions (labels) are sanitized and made unique so that the streamed zip file can be read and
 * extracted on all OS.
 *
 * NOTE Why use ZipStream: avoids consuming too much memory: "streams dynamic zip files from PHP without writing to the
 * disk at all on the server". basically, it allows sending zipped files on the fly to the browser, instead of loading
 * the entire content in PHP. Contrary to e.g. PHPZip which also sends on the fly, but writes temporary files on disk,
 * consuming as much disk space as the biggest file to add in the zip. Or contrary to ZipArchive, which is even 'worse'
 * in that perspective as it stores the entire zip on server without some sort of cleanup 'post-removal' afterwards.
 */
class SubmittedFilesZipStreamer
{
    public function __construct(private readonly string $submittedFilesDirectory)
    {
    }

    public function getDefaultStreamedZipResponse(
        CompressibleStoredTemplateTargets $compressibleStoredTemplateTargets,
        string $locale,
        string $zipFileName,
    ): StreamedZipResponse {
        return new StreamedZipResponse(
            $compressibleStoredTemplateTargets,
            $this->submittedFilesDirectory,
            $locale,
            $zipFileName,
            31
        );
    }
}
