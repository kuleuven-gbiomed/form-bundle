<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Compress;

use ZipStream\ZipStream;

final class SubmittedFilesZipStream
{
    public static function create(
        CompressibleStoredTemplateTargets $compressibleStoredTemplateTargets,
        string $submittedFilesDirectory,
        string $locale,
        int $limitQuestionLabelLength = 31,
        bool $finishStream = true,
    ): ZipStream {
        $zipStream = new ZipStream();

        /** @var CompressibleStoredTemplateTarget $compressibleTarget */
        foreach ($compressibleStoredTemplateTargets->getIterator() as $compressibleTarget) {
            $compressibleFiles = $compressibleTarget->getTemplateTarget()
                ->getCompressibleSubmittedFilesForAllCurrentVersionUploadQuestions(
                    $submittedFilesDirectory,
                    $locale,
                    $limitQuestionLabelLength
                );

            if ($compressibleFiles->isEmpty()) {
                continue;
            }

            /** @var CompressibleSavedFile $compressibleFile */
            foreach ($compressibleFiles as $compressibleFile) {
                $zipStream->addFileFromPath(
                    $compressibleTarget->getCompressLabel().'/'.$compressibleFile->getQuestionLabel().'/'.$compressibleFile->getFileName(),
                    $compressibleFile->getStoragePath(),
                );
            }
        }

        if ($finishStream) {
            $zipStream->finish();
        }

        return $zipStream;
    }
}
