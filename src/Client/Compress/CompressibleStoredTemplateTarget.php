<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Compress;

use KUL\FormBundle\Entity\StoredTemplateTarget;

class CompressibleStoredTemplateTarget
{
    private readonly string $compressLabel;

    private function __construct(private readonly StoredTemplateTarget $templateTarget, string $compressLabel)
    {
        if ('' === $compressLabel) {
            throw new \InvalidArgumentException('compress label must be a non-empty string');
        }
        $this->compressLabel = $compressLabel;
    }

    public static function createFromStoredTemplateTargetWithCompressLabel(StoredTemplateTarget $storedTemplateTarget, string $compressLabel): self
    {
        return new self($storedTemplateTarget, $compressLabel);
    }

    public function getTemplateTarget(): StoredTemplateTarget
    {
        return $this->templateTarget;
    }

    public function getCompressLabel(): string
    {
        return $this->compressLabel;
    }
}
