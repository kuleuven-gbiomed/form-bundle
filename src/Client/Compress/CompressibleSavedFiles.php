<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Compress;

use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Entity\SavedFile;

/** @implements \IteratorAggregate<array-key,CompressibleSavedFile> */
class CompressibleSavedFiles implements \IteratorAggregate
{
    /**
     * @var CompressibleSavedFile[]
     */
    private readonly array $compressibleSavedFiles;

    public function __construct(CompressibleSavedFile ...$compressibleSavedFiles)
    {
        $this->compressibleSavedFiles = array_values($compressibleSavedFiles);
    }

    public static function empty(): self
    {
        return new self();
    }

    public static function createFromSavedFiles(
        SavedFileCollection $savedFiles,
        UploadInputNode $node,
        string $submittedFilesDirectory,
        string $locale,
        ?int $limitQuestionLabelLength,
    ): self {
        return new self(...array_map(
            fn (SavedFile $savedFile) => CompressibleSavedFile::createFromSavedFile(
                $savedFile,
                $node,
                $submittedFilesDirectory,
                $locale,
                $limitQuestionLabelLength
            ),
            $savedFiles->toArray()
        ));
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->compressibleSavedFiles);
    }

    public function isEmpty(): bool
    {
        return 0 === count($this->compressibleSavedFiles);
    }

    public function with(CompressibleSavedFile ...$compressibleSavedFiles): self
    {
        return new self(...$this->compressibleSavedFiles, ...$compressibleSavedFiles);
    }

    public function mergedWith(self $compressibleSavedFiles): self
    {
        return $this->with(...$compressibleSavedFiles->toArray());
    }

    public function toArray(): array
    {
        return $this->compressibleSavedFiles;
    }
}
