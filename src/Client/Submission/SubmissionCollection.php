<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Submission;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Domain\Contract\FormUserInterface;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Entity\Submission;

/**
 * @extends ArrayCollection<array-key,Submission>
 */
final class SubmissionCollection extends ArrayCollection
{
    public function getForVersionNumber(int $versionNumber): self
    {
        return $this->filter(fn (Submission $object) => $object->getVersionNumber() === $versionNumber);
    }

    public function getForUser(FormUserInterface $user): self
    {
        return $this->filter(fn (Submission $object) => $object->getSubmitterId() === $user->getIdentifier());
    }

    /**
     * check if an auto-saved submission exists for the combination user-role-step-version in the given access object
     * NOTE: this method is for usage in auto-save. it expects to find either 1 or 0 submissions, since auto-saved
     *       submissions are overwritten for the mentioned combination upon each auto-save.
     *
     * NOTE: this method is for usage in auto-save. it expects to find 0 or 1 submissions, since auto-saved submissions
     * are overwritten for the mentioned combination upon each auto-save to preserve DB storage capacity
     * in 99% of the cases there will be one (or none). but in edge case of some delayed DB transaction causing more
     * than one being inserted in DB due to the time auto-saving (default every 3 minutes) (worst encountered case
     * was 3), we make sure we get most recent one:
     * for normal submitting and temp saving, there is a submission added for each submit/temp-save action on
     * any step-role-user-version combo. so multiple for each step-role-user-version will exist. because that is
     * the whole point of it: we want to track all submission logs. but auto temporary saving, which is triggered
     * every 3 minutes (or less/more if set as such in config.yml) would really fill up DB space.therefore, every
     * auto-save action for exact same step-role-user-version overwrites the previous submission for that
     * step-role-user-version.
     * so there should be either none or one submission by auto save for each step-role-user-version.
     * but it has happened in edge cases that - due to unrelated heavy DB usage e.g. a huge import directly in DB -
     * a submission by auto save is added to the a MYSQL 'queue' that causes the insert in DB to be executed
     * several minutes later (yes, several minutes, hence the edge case!). in meanwhile the auto save keeps on
     * being triggered which might cause another submission by auto save to be added to that queue. resulting in
     * more than one for auto save being in DB! but that is not important at all: there should be none or one,
     * but if more than one is found due to some DB delay, we just take the most recent one, since that's the one
     * that should have overwritten previous one
     *
     * @throws \BadMethodCallException if more than one is found in collection
     * @throws MissingVersionException
     */
    public function hasOneAutoSavedForAccessObject(ClientCombinedAccessObject $accessObject): bool
    {
        return $this->getAutoSavedSortedByDescendingSubmitDateForAccessObject($accessObject)->first() instanceof Submission;
    }

    /**
     * get one or fail auto-saved submission for the combination user-role-step-version in the given access object.
     *
     * NOTE: this method is for usage in auto-save. it expects to find 1 submission, since auto-saved submissions
     * are overwritten for the mentioned combination upon each auto-save to preserve DB storage capacity
     * in 99% of the cases there will be one (or none). but in edge case of some delayed DB transaction causing more
     * than one being inserted in DB due to the time auto-saving (default every 3 minutes) (worst encountered case
     * was 3), we make sure we get most recent one, since the only reason we would like to find only one (or none) is
     * to do with preserving DB capacity, not with any domain logic . the other edge case is user having form open in
     * two browsers/computers/tabs each triggering auto-saves more or less in same (couple of seconds max) time
     * interval, etc). then two (more is even more edge case) might get stored and only one overwritten. having more
     * than one is no problem for domain because we only store one and overwrite it to avoid too much data in DB, but
     * here, we choose not to assert finding  one and failing on it, but instead silently just take the most recent one.
     * UX experience and form flow keeps working just the same.
     *
     * @throws \BadMethodCallException if none or more than one is found in collection
     * @throws MissingVersionException
     */
    public function getOneAutoSavedForAccessObject(ClientCombinedAccessObject $accessObject): Submission
    {
        $result = $this->getAutoSavedSortedByDescendingSubmitDateForAccessObject($accessObject);

        $first = $result->first();
        // technically, we should check if amount returned is exactly 1 (one). but there could be two in db due to
        // edge case issues, which cannot harm, so we take the most recent one: see docblock on this method
        if ($first instanceof Submission) {
            return $first;
        }

        throw new \BadMethodCallException('expected to find at least one auto saved submission for access object, got none');
    }

    /**
     * auto-saved submission for the combination user-role-step-version in the given access object.
     *
     * @throws \BadMethodCallException if none or more than one is found in collection
     * @throws MissingVersionException
     */
    public function getAutoSavedSortedByDescendingSubmitDateForAccessObject(ClientCombinedAccessObject $accessObject): self
    {
        return $this->getForUserStepRoleVersion(
            $accessObject->getUser(),
            $accessObject->getOpenFlowStep(),
            $accessObject->getAccessingRole(),
            $accessObject->getTemplate()->getCurrentVersionNumber()
        )->getAutoSaved()->getSortedByDescendingSubmitDate();
    }

    public function getForUserStepRoleVersion(
        FormUserInterface $user,
        StepInterface $step,
        Role $role,
        int $versionNumber,
    ): self {
        return $this->filter(fn (Submission $object) => $object->getSubmitterId() === $user->getIdentifier()
            && $object->getStepUid() === $step->getUid()
            && $object->getRole() === $role->getName()
            && $object->getVersionNumber() === $versionNumber);
    }

    /**
     * Sorts SubmissionCollection by the descending submit date (most recent submission first).
     */
    public function getSortedByDescendingSubmitDate(): self
    {
        /** @var \ArrayIterator<array-key,Submission> $iterator */
        $iterator = $this->getIterator();
        $iterator->uasort(
            fn (Submission $a, Submission $b) => ($a->getSubmitDate() > $b->getSubmitDate()) ? -1 : 1
        );

        return new self(iterator_to_array($iterator));
    }

    /**
     * Sorts SubmissionCollection by the ascending submit date (most oldest submission first).
     */
    public function getSortedByAscendingSubmitDate(): self
    {
        /** @var \ArrayIterator<array-key,Submission> $iterator */
        $iterator = $this->getIterator();
        $iterator->uasort(
            fn (Submission $a, Submission $b) => ($a->getSubmitDate() < $b->getSubmitDate()) ? -1 : 1
        );

        return new self(iterator_to_array($iterator));
    }

    /**
     * filter on FormWorkFlowStep.
     */
    public function getForStep(StepInterface $step): self
    {
        return $this->filter(fn (Submission $object) => $object->getStepUid() === $step->getUid());
    }

    /**
     * filter on role.
     */
    public function getForRole(Role $role): self
    {
        return $this->getForRoleName($role->getName());
    }

    /**
     * filter on role.
     */
    public function getForRoleName(string $role): self
    {
        return $this->filter(fn (Submission $object) => $object->getRole() === $role);
    }

    /**
     * filter on auto saved.
     */
    public function getAutoSaved(): self
    {
        return $this->filter(fn (Submission $object) => $object->isAutoSaved());
    }

    /**
     * filter on not auto saved.
     */
    public function getNotAutoSaved(): self
    {
        return $this->filter(fn (Submission $object) => !$object->isAutoSaved());
    }

    /**
     * @throws \Exception
     */
    public function getSubmitHistoryForFormList(FormList $formList): SubmitHistory
    {
        $submitHistory = new SubmitHistory();

        foreach ($this as $submission) {
            $submitHistory = $submitHistory->getMergedWith($submission->getSubmitHistoryForFormList($formList));
        }

        return $submitHistory->getUnique();
    }

    public function getMergedWith(self $collection): self
    {
        return new self(array_merge($this->toArray(), $collection->toArray()));
    }
}
