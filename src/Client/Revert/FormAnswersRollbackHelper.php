<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Revert;

use KUL\FormBundle\Client\History\InputNodeSubmitEntry;
use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\GlobalScoring\CalculationScoreNodeConfig;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepCollectionInterface;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;

/**
 * --------------------- IMPORTANT, PLEASE READ ------------------------------------------------------------------------
 * all comments & method logic in this class has been copied from SCONE with minimal changes to keep originally intended logic working.
 *
 * this is a rollback helper service designed for client side (end-user) usage (where usually the end-user will be admin (have admin role))
 * hence the namespacing under KUL\FormBundle\Client!
 * it might not be sufficient for admin-side auto-rollback, don't go using this service blindly in admin-side,
 * especially the global score question rollback part!
 * ---------------------------------------------------------------------------------------------------------------------
 *
 * helps to convert already submitted answers to temporary save answers
 * when setting a @see StoredTemplateTargetInterface back to
 * a given step in its associated @see Template::getCurrentVersionNumber().
 *
 * the converting from submitted answers to temporary saved answers is complicated in some cases and some domain
 * decision are made. namely, in case of submitted answers to globals score questions:
 * their answers could be an auto-calculated answer. it could have been overwritten manually later by a user with write
 * access. some or all of the dependency questions for a global score question could re-open for editing after rollback,
 * making the already stored auto-calculated answer no longer in sync if the answers of dependencies are changed by users
 * during submit after the rollback. hence it's better to simply erase temp (and submitted) answers for global scores, if
 * they have to be rolled back. also, if there has been a new version published in which the global score and its
 * calculation params and/or its questions dependencies  were updated (new score values,weighing,...) or when new ones were added!
 * in that case, erasing is  always needed.
 *
 * @see FormAnswersRollbackHelper::shouldEraseGlobalScoreAnswer()
 *
 * submitted answers, given for questions that are no longer in the current form-version are never converted to
 * temp answers. while any temp answer that might have been given to questions that are no longer in current version
 * are left to stay dormant in the updated temporary formValues, which is normal behaviour to allow to restore to a
 * previous version and have a previously temp answer ready.
 */
final class FormAnswersRollbackHelper
{
    public const KEY_ROLLED_BACK_SUBMITTED_ANSWERS = 'key_updated_submitted_form_values';
    public const KEY_ROLLED_BACK_TEMPORARY_SAVED_ANSWERS = 'key_updated_temporary_saved_form_values';

    private readonly FormList $currentVersionFormList;
    private FormList $lastUsedVersionFormList;
    private readonly WorkFlow $currentVersionWorkFlow;
    private bool $versionsAreEqual;

    private function __construct(
        TemplateInterface $template,
        int $lastUsedVersionNumber,
        private readonly array $originalSubmittedFormValues,
        private readonly array $originalTemporarySavedFormValues,
        private readonly SubmitHistory $submitHistory,
    ) {
        $currentVersion = $template->getCurrentVersion();
        $this->currentVersionFormList = $currentVersion->getFormList();
        $this->currentVersionWorkFlow = $currentVersion->getWorkFlow();
        // for performance: if last used version is current version, do not de-serialize again and mark as equal versions
        if ($lastUsedVersionNumber === $template->getCurrentVersionNumber()) {
            $this->lastUsedVersionFormList = $this->currentVersionFormList;
            $this->versionsAreEqual = true;
        } else {
            $previousVersion = $template->getPreviouslyPublishedVersions()->getOneForVersionNumber($lastUsedVersionNumber);
            $this->lastUsedVersionFormList = $previousVersion->getFormList();
            $this->versionsAreEqual = false;
        }
    }

    public static function createFromStoredTemplateTarget(StoredTemplateTargetInterface $storedTemplateTarget): self
    {
        return new self(
            $storedTemplateTarget->getTemplate(),
            $storedTemplateTarget->getLastUsedTemplateVersionNumber(),
            $storedTemplateTarget->getSubmittedFormValues(),
            $storedTemplateTarget->getTemporarySavedFormValues(),
            $storedTemplateTarget->getSubmitHistoryForCurrentFormList()
        );
    }

    /**
     * @return array{key_updated_temporary_saved_form_values: mixed[], key_updated_submitted_form_values: mixed[]}
     */
    public function getAnswersRolledBackToStep(StepInterface $stepToRollbackTo): array
    {
        // start from a copy of the original submitted & temporary saved answers
        $rolledBackTempValues = $this->originalTemporarySavedFormValues;
        $rolledBackSubmittedValues = $this->originalSubmittedFormValues;

        // get questions that have been submitted (= answered = question uid in submitted values. includes uid of optional questions
        // submitted blank). this will only get the questions that exist in current version. any questions that were submitted
        // in a previous version but no longer exist in current version, will not be found, thus not rolled back (answers not
        // erased or converted to temp answers). note this is intentional: answers of those no longer existing question are
        // kept - orphaned - because of the possibility to restore to previous version(s) & for submit/tempSave history purposes.
        $submittedQuestionsInCurrentVersion = $this->currentVersionFormList
            ->getFlattenedParentalInputNodes()
            ->getByNodeUids(array_keys($this->originalSubmittedFormValues));

        // loop over all submitted (current versioned) questions and if needed, convert the answer into temporary answer.
        foreach ($submittedQuestionsInCurrentVersion as $question) {
            if ($question instanceof ScorableInputNode && $question->operatesAsGlobalScoreNode()) {
                // if needed, erase global score submitted & temp answers, without conversion to a temporary saved answer,
                // so that a clean re-calculation is possible without the confusion of previous answers being converted!
                // don't touch the submitted and/or temp answer of global score question that does not need erasing
                // (the latter can only occur if the rollback is not to the first step)
                if ($this->shouldEraseGlobalScoreAnswer($question, $stepToRollbackTo)) {
                    $rolledBackTempValues = $this->getUpdatedFormValuesWithErasedNodeAnswer($rolledBackTempValues, $question);
                    $rolledBackSubmittedValues = $this->getUpdatedFormValuesWithErasedNodeAnswer($rolledBackSubmittedValues, $question);
                }
            } elseif ($this->shouldConvertQuestionAnswer($question, $stepToRollbackTo)) {
                // first use submitted answer (if any exist) as (new/updated) temp answer, then delete submitted answer
                $rolledBackTempValues = $this->getUpdatedTemporaryFormValuesWithConvertedNodeAnswer($rolledBackTempValues, $question);
                $rolledBackSubmittedValues = $this->getUpdatedFormValuesWithErasedNodeAnswer($rolledBackSubmittedValues, $question);
            }
        }

        return [
            self::KEY_ROLLED_BACK_TEMPORARY_SAVED_ANSWERS => $rolledBackTempValues,
            self::KEY_ROLLED_BACK_SUBMITTED_ANSWERS => $rolledBackSubmittedValues,
        ];
    }

    /**
     * for rollback purposes we don't care if a label or other non-calculating property has changed between versions.
     * this could be important from human perspective, but not for the viewpoint if the version change has affected
     * or will affect calculation. hence, checking global score and its dependencies on relevant calculation params only.
     */
    private function detectsBreakingGlobalScoreVersionChanges(OperableForScoring $globalScoreNode): bool
    {
        // check if used as a global score node, don't want exception thrown later on.
        if (!$globalScoreNode->operatesAsGlobalScoreNode()) {
            return false;
        }

        // if last action was done in the current version, then there no changes were made to the global score question (duh)
        // (unless such important changes were made directly in current version, but then the 'old' version is lost anyway)
        if ($this->versionsAreEqual) {
            return false;
        }

        // get the version of the global score node from previous (last used) formList version. it's rather unlikely/uncommon that
        // the global score will not have existed in that previous version, but not impossible. if it didn't exist, that's
        // a version change as well so that we can erase its answer and allow a re-calculate. It would be way too complicated
        // to check & deal with it as non version-change (e.g. its dependency questions might have existed as non dependencies, etc.)
        if (!$this->lastUsedVersionFormList->hasNodeByUid($globalScoreNode->getUid())) {
            return true;
        }

        $globalScoreNodeInLastUsedVersion = $this->lastUsedVersionFormList->getNodeByUid($globalScoreNode->getUid());
        // if global score node existed in previous version, it should have been at least a scorable Node, since type
        // changing of a question (in admin side) is not only not even allowed, it would make things unworkable. and if it
        // is a global score now, chances that it wasn't one in previous version are highly unlikely. still, need to make sure
        // it was a global score now and before. if it wasn't, it's definitely a reason to consider it changed and thus erase its answer.
        if (!$globalScoreNodeInLastUsedVersion instanceof ScorableInputNode
            || !$globalScoreNodeInLastUsedVersion->operatesAsGlobalScoreNode()) {
            return true;
        }

        // check important changes on global score itself
        if (!$this->matchingCalculatingAbilityBetweenNodeVersions($globalScoreNode, $globalScoreNodeInLastUsedVersion)) {
            return true;
        }

        // get the dependency questions for the global score node from current version
        $currentVersionValidDependencyQuestionConfigs = $globalScoreNode->getValidGlobalScoreCalculationScoreNodeConfigs();
        // get the dependency questions for the global score node from previous version
        $lastUsedVersionValidDependencyQuestionConfigs = $globalScoreNodeInLastUsedVersion->getValidGlobalScoreCalculationScoreNodeConfigs();

        // if dependency questions were added or removed, always erase
        if ($currentVersionValidDependencyQuestionConfigs->count() !== $lastUsedVersionValidDependencyQuestionConfigs->count()) {
            return true;
        }

        /** @var CalculationScoreNodeConfig $currentDependencyQuestionConfig */
        foreach ($currentVersionValidDependencyQuestionConfigs as $currentDependencyQuestionConfig) {
            // always erase if at least one dependency question did not exist in old version (e.g. one deleted + new one added)
            if (!$lastUsedVersionValidDependencyQuestionConfigs->hasForScoreNode($currentDependencyQuestionConfig->getScoreNode())) {
                return true;
            }

            $lastUsedQuestionConfig = $lastUsedVersionValidDependencyQuestionConfigs->getForScoreNode($currentDependencyQuestionConfig->getScoreNode());

            // always erase if weighing of dependency question was changed
            if ($currentDependencyQuestionConfig->getConfig()->getWeighing() !== $lastUsedQuestionConfig->getConfig()->getWeighing()) {
                return true;
            }

            // always erase if parameters required for calculation were changed
            $matchingCalculatingAbility = $this->matchingCalculatingAbilityBetweenNodeVersions(
                $currentDependencyQuestionConfig->getScoreNode(),
                $lastUsedQuestionConfig->getScoreNode()
            );

            if (false === $matchingCalculatingAbility) {
                return true;
            }
        }

        return false;
    }

    private function matchingCalculatingAbilityBetweenNodeVersions(
        OperableForScoring $currentVersionNode,
        OperableForScoring $previousVersionNode,
    ): bool {
        if ($currentVersionNode->getUid() !== $previousVersionNode->getUid()) {
            throw new \Exception('nodes do not match on uid');
        }
        $isGlobalScoreNode = $currentVersionNode->operatesAsGlobalScoreNode();

        $scoringParamsVersionsMatch = $currentVersionNode->getInputScoringParameters()->matchesCalculatingAbilityWith(
            $previousVersionNode->getInputScoringParameters(),
            $isGlobalScoreNode,
            $isGlobalScoreNode,
            false
        );

        if (false === $scoringParamsVersionsMatch) {
            return false;
        }

        if (!$currentVersionNode instanceof ChoiceInputNode) {
            return true;
        }

        // current version is choiceNode, then so will previous version be. if not, something went horribly wrong on
        // admin side, despite publish validation: type of existing question was changed, uid of question was re-assigned
        // to other question, etc. all things that should never happen (db tampering..).
        if (!$previousVersionNode instanceof ChoiceInputNode) {
            throw new \Exception('the current version of node '.$currentVersionNode->getUid().' is of choice type, but previous version is not.');
        }

        $currentVersionScoringOptions = $currentVersionNode->getAvailableOptions()->getHavingScoringValue();
        $previousVersionScoringOptions = $previousVersionNode->getAvailableOptions()->getHavingScoringValue();

        /** @var OptionInputNode $option */
        foreach ($currentVersionScoringOptions as $option) {
            // an option was removed or its scoring value was removed
            if (!$previousVersionScoringOptions->hasOneByNodeUid($option->getUid())) {
                return false;
            }

            /** @var OptionInputNode $previousOption */
            $previousOption = $previousVersionScoringOptions->getOneByNodeUid($option->getUid());
            // an option's scoring value was changed
            if ($option->getScoringValue() !== $previousOption->getScoringValue()) {
                return false;
            }
        }

        return true;
    }

    private function getUpdatedFormValuesWithErasedNodeAnswer(array $formValues, InputNode $node): array
    {
        // start from the given answers
        $updatedFormValues = $formValues;

        // remove the question key and its answer value
        unset($updatedFormValues[$node->getUid()]);

        if (!$node instanceof ChoiceInputNode) {
            return $updatedFormValues;
        }

        // in case of a choice question, the chosen (answered) choice-option(s) (key + value) must be removed separately
        foreach ($node->getChildren() as $child) {
            unset($updatedFormValues[$child->getUid()]);
        }

        return $updatedFormValues;
    }

    private function getUpdatedTemporaryFormValuesWithConvertedNodeAnswer(
        array $temporaryFormValues,
        InputNode $node,
    ): array {
        // start from the given temporary saved answers
        $updatedTempValues = $temporaryFormValues;

        // answer should exist in submitted answers if called from within this class. still, let's check and not break
        // note that an answer could validly be NULL (answered blank), hence not relying on isset on array[uid] here
        if (!array_key_exists($node->getUid(), $this->originalSubmittedFormValues)) {
            return $updatedTempValues;
        }

        // first remove the question key and its answer value from the existing temporary form values;
        $updatedTempValues = $this->getUpdatedFormValuesWithErasedNodeAnswer($updatedTempValues, $node);

        // add the submitted question key & answer to the temporary ones
        $updatedTempValues[$node->getUid()] = $this->originalSubmittedFormValues[$node->getUid()];

        if (!$node instanceof ChoiceInputNode) {
            return $updatedTempValues;
        }

        // in case of a choice question, the chosen choice-option(s) must be added separately (it's how it's build)
        foreach ($node->getChildren() as $child) {
            if (array_key_exists($child->getUid(), $this->originalSubmittedFormValues)) {
                $updatedTempValues[$child->getUid()] = $this->originalSubmittedFormValues[$child->getUid()];
            }
        }

        return $updatedTempValues;
    }

    /**
     * @param StepCollectionInterface<array-key, StepInterface> $steps
     */
    private static function considersQuestionAnswerableInsteps(InputNode $node, StepCollectionInterface $steps): bool
    {
        foreach ($steps as $step) {
            if ($node->getFlowPermission()->isStepWriteAccessGrantedForAtLeastOneRole($step->getUid())) {
                return true;
            }
        }

        return false;
    }

    /**
     * check if global score answer should be rolled back (erased) when rolling back to certain step.
     *
     * the rollback makes sure that an already executed calculation for a global score question can/should be re-auto-calculated
     * if that rollback (will) affect(s) the submitting possibilities of the dependency questions for that global score question.
     * if it does not affect the submitting possibilities of the dependency questions but a change of form-template versions is
     * detected which affects the calculation (ability) of the global score node or its dependency questions, it is also rolled back.
     *
     * note that a global score typically is auto-calculated only once, but it can be configured to keep re-calculating
     * on every submit, which does not affect the way a global score is handled here in rollback:
     *
     * @see ScorableInputNode::isRecalculatingOnEachSubmitInStep
     *
     * rollback global scores is a bit tricky:
     * a global score gets a (server-sided) auto-calculated answer (only once or on every submit) at the end of a valid
     * submit, on the condition that all its dependency questions are all submitted (answered) at that moment (either
     * submitted in that particular submit or in previous submits by same or other users).
     * therefore, conditions to roll back an answer for it does not depend on the global score question itself, but on
     * the dependency questions on which it depends for its (auto) calculation. we also never convert a global score
     * question's (auto-)submitted answer to a temporary answer, we always erase that answer (prefilling global score
     * input fields with temp (previously final) answers is too confusing for users: it's easily overlooked at as being
     * a temp answer and mistakenly assumed as final auto-calculated answer).
     * so we only roll back (i.e. erase) a global score (auto-)submitted answer in two cases:
     * - case 1: if at least one of its dependency questions can NOT be answered (NOT writable) in any preceding steps. in other
     *   words, if one or more of its dependency questions is only answerable in the step-to-roll-back-to or one of the
     *   steps  following the step-to-roll-back-to. this makes sure the auto-calculation will/could be re-triggered with
     *   a new calculation based on changes (re-submits) in the answers of those dependencies that will only be answerable
     *   in the step that was rolled back to or a following step. (see example below marked: (*) )
     * - case 2: if there are version changes on the global score or its dependencies, the answer is always erased, because
     *   the (auto) calculated result in the answer is no longer conform with the dependencies or global score params.
     *   this condition can result in an admin having to either intervene manually for the answer or having to rollback
     *   even further: when the rollback is to a step not far back enough in the workflow, it is possible that the global
     *   score auto-calculation will not be triggered anymore. it is completely impossible to deal with that, but we still
     *   have to erase the answer to avoid invalid answers! however, we have to assume that the admin rolling back,
     *   chooses correct step-to-roll-back-to if (same or other) admin made the drastic move to make changes to the
     *   version and its global scores! in any case, admin can always use the perpetual write access (that is usually
     *   granted to admins on a question from the moment a non-admin gets write access to that question) to manually
     *   trigger a re-calculation or fill in manually if no other non-admin user-role has write access left to do so.
     *
     * (*) START example for case 1: ---------------------------------------------------------------------------------
     *   note: a global score node is auto-calculated as soon as (during a submit action) all  its dependency questions
     *         have answers, regardless of the step or permissions on the global score node.
     *   note: the phrase 'at least one ... can NOT be answered ... in any preceding steps' is somewhat confusing,
     *         because what actually counts is the reverse: what IS answerable after the rollback. but by checking the
     *         NOT answerable questions in preceding steps, we know about what could affect global score in the step-to-roll-back-to
     *         or one of the steps following that.
     *   E.G.: suppose a form with 3 steps and two dependency questions A and B for a third, global score question C.
     *   suppose A is answerable in step 1, and readonly in steps 2& 3. suppose B is answerable in step 2, with no access in step 1
     *   and readonly in step 3. suppose C grants no access in steps 1 and 2 and read-only in step 3.
     *   the form is then submitted in step 1, effectively storing an answer for A. during that submit action, B is not
     *   answered yet, so C has not all its dependencies answered thus is not auto-calculated and remains unanswered.
     *   the form is then submitted in step 2, effectively storing an answer for B. A was already answered, so during that
     *   submit, the system detects that C now has all its dependencies answered and it did not yet had an asnwer, thus C
     *   is auto-calculated and its answer stored.
     *   suppose the form then goes to step 3 (by the same submit or by deadline and a temp save or later submit or whatever...).
     *   if we now roll back to step 2, then A is answerable in a preceding step (= step 1), but B is not. that means B can be edited
     *   and re-answered/re-submitted by a user after the rollback in step 2. if we would not erase C during the rollback, then any
     *   change to B by a submitting user in step 2 will NOT trigger a re-auto-calculation again (C had already an answer so is
     *   left untouched) and thus answer for C could be NOT the result of a calculation of A & B. so we erase it during rollback to step 2.
     *
     *   suppose we write the form differently and both A and B only can now be answered in step 2, with both having no access in step 1
     *   and readonly in step 3.then the same principle counts: none are answerable in a preceding step (step 1) when rolling back to step 2
     *   (when form was already in step 3  with C having an auto-calculated answer). but both A and B can be edited (re-answered/re-submitted)
     *   by users in step 2 after rollback to step 2, thus affecting auto-calculated result in C. so we erase anwser of C to allow a re-calculation.
     *
     *   suppose we write the form again differently, this time both A and B are ONLY answerable in step 1, and readonly
     *   in step 2 & 3. the auto-calculation of C will happen when a user submits in step 1 instead of step 2 now, but that
     *   is not so important. if the form goes to step 3 and we rol back to step 2, then the rollback will detect that
     *   none of the dependency questions are NOT answerable in a preceding step (step 1) (both A & B are answerable in step 1).
     *   this means that after rollback to step 2, we cal leave answer of C as it is, because in step 2 (and 3) no user can edit
     *   A nor B, thus not affect the auto-calculated answer of C.
     * -----------------------------------------------------------------------------------------------------------------
     */
    private function shouldEraseGlobalScoreAnswer(
        ScorableInputNode $globalScoreNode,
        StepInterface $forRollbackToStep,
    ): bool {
        // check if it's a global score node, if not, nothing to erase
        if (!$globalScoreNode->operatesAsGlobalScoreNode()) {
            return false;
        }

        // if rollback to first step, no need to go check further on dependencies on having write access in non-preceding
        // steps, because there are no preceding steps. erase the answer to allow for a clean recalculation of answer
        if ($forRollbackToStep->getUid() === $this->currentVersionWorkFlow->getSteps()->getStartingStep()->getUid()) {
            return true;
        }

        // in case of version changes which affect the global score, its dependencies and/or calculation, always erase,
        // even if that means it could result in no automatic server sided re-triggering of calculation, to allow for
        // a clean recalculation of answer based on version changes. it's way too complex (read: impossible) to deal with
        // version changes on (dependencies of) global score nodes, so it's better to erase them and let the re-calculation
        // be triggered again, unless the version changes do not affect the global score, its dependencies and/or calculation.
        if ($this->detectsBreakingGlobalScoreVersionChanges($globalScoreNode)) {
            return true;
        }

        // no significant version changes detected, so get current version dependency questions for the global score
        $dependencyQuestions = $globalScoreNode->getValidGlobalScoreCalculationScoreNodeConfigs()->getScoreNodes();
        $precedingSteps = $this->currentVersionWorkFlow->getSteps()->getSortedStepsThatPrecedeStep($forRollbackToStep);

        // check the dependencies. only erase if one or more dependency can not be answered in preceding steps, which
        // means those dependencies need to be re-answered after rollback and that could change global score answer.
        /** @var InputNode $dependencyQuestion */
        foreach ($dependencyQuestions as $dependencyQuestion) {
            if (!self::considersQuestionAnswerableInsteps($dependencyQuestion, $precedingSteps)) {
                return true;
            }
        }

        return false;
    }

    /**
     * the submitted answers (and logs) that are converted are only those of questions that were NOT answered in one
     * of the steps preceding the given step-to-roll-back-to. i.e. if there are no roles that had write access in any
     * of the preceding steps. because the rollback is about given users a chance to re-answer a question that was
     * answered in the step-to-roll-back-to or one of the following steps. if a question was answered in a preceding
     * step and there are roles that have write access again to that question in the given step-to-roll-back-to or one
     * of the following steps, then those roles will still be able to use that write access to edit that non-converted
     * answer in the 'normal' flow & rights, even after rollback. hence only question that no one could answer before
     * the step-to-roll-back-to, are converted and presented for re-answering. all other questions keep their answer
     * and users with write access can edit them if allowed and wanted in the rollback step or later step(s).
     * in reality, only admin roles get perpetual write access to all questions starting from the step were any other
     * role had write access for the first time, and admins keep having that write access in all following steps. while
     * non-admin roles commonly - but not exclusively - only have write access to a particular set of questions in one
     * step and get only read access to that set in all steps following that one step.
     *
     * note: applying conversion is about the question being NOT answerable in a preceding step, but whether that
     * question was actually answered or not, is not important. because that's not the rollbacks job to solve, but
     * purely end-user responsibility! e.g. such question could be unanswered if end-users waited too long and step
     * closed. the rollback to that same step will not change answer (there is none!) but if admins then extend the
     * deadline of that step - does not matter if they do that before or after the rollback - then the end-users can
     * answer that question anyway within the step's new (or existing) deadline.
     *
     * note: converting a submitted answer to a temporary one, makes it pop up as most recent temporary log/answer
     * in input fields with notification about it being temp answers. temp answers are only visible to users with write
     * access, because only final submitted answers re-used in read & export views!
     *
     * note: this method assumes no one messed with the submission logs (as in manually in DB: deleted them, changed
     * dates, changed type of action, changed logged answers, etc.) because this could lead to unwanted conversions!
     */
    private function shouldConvertQuestionAnswer(InputNode $question, StepInterface $forRollbackToStep): bool
    {
        $precedingSteps = $this->currentVersionWorkFlow->getSteps()->getSortedStepsThatPrecedeStep($forRollbackToStep);

        // in case of rollback to first step, always convert
        if ($precedingSteps->isEmpty()) {
            return true;
        }

        // do not convert if question is/was answerable in at least one step coming before step-to-roll-back-to
        if (self::considersQuestionAnswerableInsteps($question, $precedingSteps)) {
            return false;
        }

        // use submit logs to determine if conversion is needed. in theory, there is no need to filter history entries on
        // being for actions done in steps preceding step-to-roll-back-to: we already checked & handled if question was
        // answerable in preceding steps. hence, any actions could only have been done in step-to-roll-back-to or
        // a following step. but can not hurt to double-check
        $submitEntries = $this->submitHistory->forNodeUid($question->getUid())->filter(
            fn (InputNodeSubmitEntry $entry) =>
                // skip if entry is from action before step to rollback to
                !$precedingSteps->hasOneStepByUid($entry->getStepUid())
        );

        $latestSubmitEntry = $submitEntries->latest();

        // there is only something to convert if there was ever something submitted for that question
        return $latestSubmitEntry instanceof InputNodeSubmitEntry;
    }
}
