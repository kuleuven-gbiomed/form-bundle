<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Revert;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;

/**
 * --------------------- IMPORTANT, PLEASE READ ------------------------------------------------------------------------
 * all comments & method logic in this class has been copied from SCONE with minimal changes to keep originally intended logic working.
 *
 * this is a rollback service designed for client side (end-user) usage (where usually the end-user will be admin (have admin role))
 * hence the namespacing under KUL\FormBundle\Client!
 * it might not be sufficient for admin-side auto-rollback, don't go using this service blindly in admin-side!
 * please also @see FormAnswersRollbackHelper
 * ---------------------------------------------------------------------------------------------------------------------
 */
final readonly class RollbackService
{
    public function __construct(
        private StoredTemplateTargetRepository $storedTemplateTargetRepository,
    ) {
    }

    public function rollBackStoredTemplateTargetFormToStep(
        StoredTemplateTargetInterface $storedTemplateTarget,
        StepInterface $stepToRollBackTo,
    ): void {
        $storedTemplateTarget->rollBackToStep($stepToRollBackTo);
        $this->storedTemplateTargetRepository->save($storedTemplateTarget);
    }
}
