<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Revert;

use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Entity\SavedFileRepository;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use KUL\FormBundle\Entity\SubmissionRepository;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\UploadedFileRepository;

/**
 * --------------------- IMPORTANT, PLEASE READ ------------------------------------------------------------------------
 * all comments & method logic in this class has been copied from SCONE with minimal changes to keep originally intended logic working.
 *
 * this is a reset service designed for client side (end-user) usage (where usually the end-user will be admin (have admin role))
 * hence the namespacing under KUL\FormBundle\Client!
 * since it basically deletes everything for a filled-in form, it is sufficient for admin-side auto-reset, but it was not originally designed for admin-side!
 * ---------------------------------------------------------------------------------------------------------------------
 */
class ResetService
{
    public function __construct(
        private readonly StoredTemplateTargetRepository $storedTemplateTargetRepository,
        private readonly SavedFileRepository $savedFileRepository,
        private readonly SubmissionRepository $submissionRepository,
        private readonly UploadedFileRepository $uploadedFileRepository,
    ) {
    }

    /**
     * logic.
     *
     * delete submitted (filled-in) template-target form with everything from submission logs to saved uploaded files as a way to give users the chance to start
     * over (reset) with the process of filling in questions from scratch: all answers, submission logs, uploaded files, etc. for the template-target form
     * combo are deleted from DB and storage. no record or history of the once filled-in form whatsoever is kept.
     *
     * NOTE: I don't like Doctrine's cascade delete. you need to maintain (new) relations in .orm files instead of in
     * code and cascade deleting is not always desired, but it's nonetheless always executed! I prefer to do it myself:
     * 1. collecting related entities
     * 2. deleting AND flushing (!) this template-target entity (which de facto decouples & deletes many-to-many relations).
     * 3. using their own repo's, deleting the now orphaned entities.
     * if anything goes wrong on deletion/flushing of parent entity or related entity, SQL constraints will kick in.
     *
     * if needed, this could be executed inside a transaction. but this method is not where the transaction is to be
     * done! that's the responsibility of the caller (to avoid partially executed, partially rolled back SQL due to
     * nested transactions)
     */
    public function resetStoredTemplateTargetForm(StoredTemplateTargetInterface $storedTemplateTarget): void
    {
        // first collect the related submissions & savedFiles entities before removing template-target form entity (associated via many-to-many)...
        $submissionsToRemove = array_merge($storedTemplateTarget->getSubmissions()->toArray(), $storedTemplateTarget->getTemporarySubmissions()->toArray());
        $savedFilesToRemove = $storedTemplateTarget->getSavedFiles()->toArray();

        // then delete & flush the template-target form so that Doctrine decouples & deletes the many-to-many associations for Submission & SavedFile)
        $this->storedTemplateTargetRepository->delete($storedTemplateTarget);

        // now that all associated (many-to-many) Submission & SavedFile entities are de-coupled, delete & flush all of them
        foreach ($submissionsToRemove as $submission) {
            $this->submissionRepository->delete($submission);
        }

        foreach ($savedFilesToRemove as $savedFile) {
            $uploadedFile = $savedFile->getUploadedFile();
            // remove file link to this (filled-in) template-target form
            $this->savedFileRepository->delete($savedFile);
            // remove file data for the  file linked to this (filled-in) template-target form
            // also removes file itself from storage.
            $this->uploadedFileRepository->delete($uploadedFile);
        }
    }

    /**
     * delete all submitted (filled-in) template-target forms for given Template.
     */
    public function resetAllStoredTemplateTargetFormsForTemplate(Template $template): void
    {
        $storedTemplateTargets = $this->storedTemplateTargetRepository->allForTemplate($template);

        foreach ($storedTemplateTargets as $storedTemplateTarget) {
            $this->resetStoredTemplateTargetForm($storedTemplateTarget);
        }
    }
}
