<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Revert\Exception;

class AccessForResetDeniedException extends \Exception
{
}
