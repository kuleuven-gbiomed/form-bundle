<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Revert;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepCollectionInterface;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * the following comment is copied from SCONE and minimally adjusted.
 *
 * based on the implementing app's config settings for 'kul_form.revert.settings.roles_allowed_to_revert' (allowRevertToAccessDeniedStepsForRole)
 * we can limit revert actions based on the (admin) role having access to the step to which he/she wants to rollback
 * or the first step in case of a reset.
 * this is a soft limitation: there is no issue in allowing admins to reset or roll back to any of the available steps.
 * but it proved better not to show possibility to roll back to a step in which the admin could never have access,
 * even if the admin does the rollback not for himself, but for all other users that may or may not have access
 * to that step. it would be confusing for them to sees the possibility to rollback and then get an 'access denied' message
 * after the rollback to that particular step. although this will likely never occur (and hasn't at time of writing):
 * because when making templates (admin side), we always give admins access to all writable questions in each step as
 * soon as other roles get write access. in short: admins have access to all steps.
 * but in the future, it is not unlikely that 'lower' admins don't get the same access to steps as higher level
 * admins, while still getting the right to rollback.
 * note: this has nothing to do with the step being 'open' for admin or any other: the step could be passed
 * deadline. but that information is added to the action description so admins know about this.
 */
class AllowToRevertChecker
{
    private const RESET = 'reset';
    private const ROLLBACK = 'rollback';
    private const INCLUDE_REVERT_TO_ACCESS_DENIED_STEPS = 'includeRevertToAccessDeniedSteps';
    private const INCLUDE_REVERT_TO_DEADLINED_STEPS = 'includeRevertToDeadlinedSteps';

    public function __construct(
        private readonly array $rolesAllowedToRevert,
    ) {
    }

    public function allowsResetForClientAccess(ClientCombinedAccessObject $accessObject): bool
    {
        $accessingRole = $accessObject->getAccessingRole();

        // duh.
        if (!$this->allowResetForRole($accessingRole)) {
            return false;
        }

        // duh. kinda silly to reset if there is nothing to reset (nothing submitted or temp saved yet: i.e. no record in DB).
        if (!$accessObject->hasStoredTemplateTarget()) {
            return false;
        }

        // a reset means everything will be erased and form is to restart from scratch in first step.
        $startingStep = $accessObject->getCurrentVersionWorkflow()->getSteps()->getStartingStep();

        // don't allow reset if first step is passed its deadline (if that's not allowed for this role)
        if (!$this->allowRevertToDeadlinedStepsForRole($accessingRole)
            && $this->getDeadlinedStepsForClientAccess($accessObject)->hasOneStepByUid($startingStep->getUid())) {
            return false;
        }

        // don't allow reset if role has no access to first step (if that's not allowed for this role)
        if (!$this->allowRevertToAccessDeniedStepsForRole($accessingRole)
            && !$accessObject->getTemplate()->allowsRoleToParticipateInCurrentVersionStep($accessingRole, $startingStep)) {
            return false;
        }

        return true;
    }

    /** @return StepCollectionInterface<array-key, StepInterface> */
    public function getAllowedRollbackStepsForClientAccess(ClientCombinedAccessObject $accessObject): StepCollectionInterface
    {
        $steps = $accessObject->getCurrentVersionWorkflow()->getSteps();
        $accessingRole = $accessObject->getAccessingRole();

        // duh.
        if (!$this->allowRollbackForRole($accessingRole)) {
            return $steps::createEmpty();
        }

        // duh. can not roll back to any steps if there is nothing to rollback (nothing submitted or temp saved yet: i.e. no record in DB).
        if (!$accessObject->hasStoredTemplateTarget()) {
            return $steps::createEmpty();
        }

        // filter steps down to relevant steps preceding the last used step in the filled-in form
        $availableSteps = $accessObject->getStoredTemplateTarget()->getStepsAvailableForRollback();

        // could be a record exists but with only temp saved answers, so no submitted answer to roll back (convert to
        // temp saved answers), then there are no steps to roll back to.
        if ($availableSteps->isEmpty()) {
            return $steps::createEmpty();
        }

        // don't allow rollback to steps passed their deadline (if that's not allowed for this role)
        if (!$this->allowRevertToDeadlinedStepsForRole($accessingRole)) {
            $deadlinedSteps = $this->getDeadlinedStepsForClientAccess($accessObject);
            foreach ($availableSteps->getIterator() as $availableStep) {
                if ($deadlinedSteps->hasOneStepByUid($availableStep->getUid())) {
                    $availableSteps = $availableSteps->withRemovedStep($availableStep);
                }
            }
        }

        // don't allow rollback steps in which role has not at least minimal access (if that's not allowed for this role)
        if (!$this->allowRevertToAccessDeniedStepsForRole($accessingRole)) {
            $template = $accessObject->getTemplate();
            foreach ($availableSteps->getIterator() as $availableStep) {
                if (!$template->allowsRoleToParticipateInCurrentVersionStep($accessingRole, $availableStep)) {
                    $availableSteps = $availableSteps->withRemovedStep($availableStep);
                }
            }
        }

        return $availableSteps->getSortedByAscendingFlow();
    }

    /** @return StepCollectionInterface<array-key, StepInterface> */
    public function getDeadlinedStepsForClientAccess(ClientCombinedAccessObject $accessObject): StepCollectionInterface
    {
        $steps = $accessObject->getCurrentVersionWorkflow()->getSteps();

        return match (true) {
            $steps instanceof StepWithFixedDateStartCollection => $steps->findDeadlinedStepsForDate($accessObject->getCheckDate()),
            $steps instanceof StepWithCalculatedDateStartCollection => $steps->findDeadlinedStepsForDates($accessObject->getCheckDate(), $accessObject->getCalculatedStepReferenceDate()),
            $steps instanceof MixedCalculatedAndFixedStepCollection => $steps->findDeadlinedStepsForDates($accessObject->getCheckDate(), $accessObject->getCalculatedStepReferenceDate()),
            default => throw new StepException('unknown stepCollection type'),
        };
    }

    private function allowRollbackForRole(Role $role): bool
    {
        if (!array_key_exists($role->getName(), $this->rolesAllowedToRevert)) {
            return false;
        }

        return $this->rolesAllowedToRevert[$role->getName()][self::ROLLBACK];
    }

    private function allowResetForRole(Role $role): bool
    {
        if (!array_key_exists($role->getName(), $this->rolesAllowedToRevert)) {
            return false;
        }

        return $this->rolesAllowedToRevert[$role->getName()][self::RESET];
    }

    private function allowRevertToDeadlinedStepsForRole(Role $role): bool
    {
        if (!array_key_exists($role->getName(), $this->rolesAllowedToRevert)) {
            return false;
        }

        return $this->rolesAllowedToRevert[$role->getName()][self::INCLUDE_REVERT_TO_DEADLINED_STEPS];
    }

    private function allowRevertToAccessDeniedStepsForRole(Role $role): bool
    {
        if (!array_key_exists($role->getName(), $this->rolesAllowedToRevert)) {
            return false;
        }

        return $this->rolesAllowedToRevert[$role->getName()][self::INCLUDE_REVERT_TO_ACCESS_DENIED_STEPS];
    }
}
