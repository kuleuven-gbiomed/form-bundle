<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Revert\Rendering;

/**
 * @implements \IteratorAggregate<array-key, RevertAction>
 */
class RevertActions implements \IteratorAggregate, \JsonSerializable
{
    /**
     * @var RevertAction[]
     */
    private readonly array $revertActions;

    public function __construct(RevertAction ...$revertAction)
    {
        $this->revertActions = array_values($revertAction);
    }

    public static function empty(): self
    {
        return new self();
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->revertActions);
    }

    public function isEmpty(): bool
    {
        return 0 === count($this->revertActions);
    }

    public function with(RevertAction ...$revertAction): self
    {
        return new self(...$this->revertActions, ...$revertAction);
    }

    public function toArray(): array
    {
        return $this->revertActions;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
