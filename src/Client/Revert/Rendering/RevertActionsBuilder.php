<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Revert\Rendering;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\Revert\AllowToRevertChecker;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepCollectionInterface;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RevertActionsBuilder
{
    public function __construct(
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly TranslatorInterface $translator,
        private readonly AllowToRevertChecker $allowToRevertChecker,
    ) {
    }

    /**
     * build the revert actions allowed for this (user-)role and based on the revert config settings for that role
     * in the implementing app's 'kul_form.yaml'.those actions can then be rendered and if triggered (clicking the
     * link/button), will execute the revert action in this bundle's back-end, resulting in the server-side reload
     * (using @see RedirectResponse) of the form page upon success or failure.
     */
    public function buildAllowedRevertActionsWithFormPageReloadResponseForClientAccess(
        ClientCombinedAccessObject $accessObject,
        string $locale,
    ): RevertActions {
        return $this->buildAllowedRevertActionsForClientAccess($accessObject, $locale, false);
    }

    /**
     * build the revert actions allowed for this (user-)role and based on the revert config settings for that role
     * in the implementing app's 'kul_form.yaml'. those actions can then be rendered and if triggered (front-end AJAX
     * api call), will execute the revert action in this bundle's back-end, resulting in JSON response containing data
     * about the success or failure of the executed revert.
     */
    public function buildAllowedApiRevertActionsWithJsonResponseForClientAccess(
        ClientCombinedAccessObject $accessObject,
        string $locale,
    ): RevertActions {
        return $this->buildAllowedRevertActionsForClientAccess($accessObject, $locale, true);
    }

    /**
     * if a revert action could be allowed and thus build and added here, it means the back-end will also allow it to
     * be executed, since it uses the same checks to assert it is allowed. but it is possible some config has changed or
     * the user's role access was revoked, etc., between rendering the action and triggering it. in that case, an access
     * denied response will be triggered by the back-end.
     *
     * each allowed revert action contains the paths (urls), a label for the link/button and a display type (e.g. 'danger').
     * the label of each action contains the label of the step which the revert is targeting, whether it
     * is a rollback or a reset and whether that targeting step is passed deadline. the display type ('danger') can be used
     * to highlight that the revert action is for a revert to a step that is already past its deadline.
     *
     * the $rollbackPathName & the $resetPathName are paths that are part of this bundle, each containing specific
     * parameters.
     */
    private function buildAllowedRevertActionsForClientAccess(
        ClientCombinedAccessObject $accessObject,
        string $locale,
        bool $useApiPaths,
    ): RevertActions {
        // can not revert anything if there is nothing submitted or temp saved yet (i.e. no record in DB)
        if (!$accessObject->hasStoredTemplateTarget()) {
            return RevertActions::empty();
        }

        $rollbackSteps = $this->allowToRevertChecker->getAllowedRollbackStepsForClientAccess($accessObject);
        $allowReset = $this->allowToRevertChecker->allowsResetForClientAccess($accessObject);

        // nothing to rollback nor reset and/or not allowed to for accessing user-role at current moment
        if ($rollbackSteps->isEmpty() && false === $allowReset) {
            return RevertActions::empty();
        }

        $storedTemplateTarget = $accessObject->getStoredTemplateTarget();
        $roleSlug = $accessObject->getSlugifiedRole();
        $deadlinedSteps = $this->allowToRevertChecker->getDeadlinedStepsForClientAccess($accessObject);
        $actions = [];

        if ($allowReset) {
            $workflowStartingStep = $accessObject->getCurrentVersionWorkflow()->getSteps()->getStartingStep();
            $actions[] = $this->buildResetActionForRole(
                $storedTemplateTarget,
                $deadlinedSteps->hasOneStepByUid($workflowStartingStep->getUid()),
                $roleSlug,
                $useApiPaths
            );
        }

        if (!$rollbackSteps->isEmpty()) {
            $actions = array_merge($actions, $this->buildRollbackActionsForRole(
                $storedTemplateTarget,
                $rollbackSteps,
                $deadlinedSteps,
                $roleSlug,
                $locale,
                $useApiPaths
            )->toArray());
        }

        return new RevertActions(...$actions);
    }

    /**
     * no need to check if a reset is useful or not: reset is possible as soon as a placement-form record exists in DB.
     * even if placement-form is still in first step and there are no final, non-temp saved answers yet, it can still
     * be useful for admins to erase temporary answers,files and submission history anyway to give users a complete
     * fresh restart. basically, as long as there is a StoredTemplateTarget, it can be reset (removed from DB).
     */
    private function buildResetActionForRole(
        StoredTemplateTargetInterface $storedTemplateTarget,
        bool $firstStepIsDeadlined,
        string $roleSlug,
        bool $useApiPaths,
    ): RevertAction {
        $label = $this->translator->trans('revert.reset.action.label', [], 'kulFormRevert');

        $description = '<ul>';
        $description .= '<li>'.$this->translator->trans('revert.reset.descriptions.main', [], 'kulFormRevert').'</li>';

        if ($firstStepIsDeadlined) {
            $description .= '<li>'.$this->translator->trans('revert.reset.descriptions.deadlineAddon', [], 'kulFormRevert').'</li>';
            $description .= '<li>'.$this->translator->trans('revert.general.deadlineAdjustment', [], 'kulFormRevert').'</li>';
        }

        $description .= '</ul>';
        $description .= '<p><strong>'.$this->translator->trans('revert.reset.descriptions.warning', [], 'kulFormRevert').'</strong></p>';

        $messageType = RevertAction::TYPE_DEFAULT;

        // reset basically equals hard reset to first step. if first step is not open anymore, warn admin beforehand but
        // still allow to reset, because admins can always answer questions in later steps (or deadline can be extended).
        if ($firstStepIsDeadlined) {
            $messageType = 'danger';
            $label .= ' ('.$this->translator->trans('revert.actions.deadlineForResetStepWarning', [], 'kulFormRevert').')';
        }

        return new RevertAction(
            $label,
            $useApiPaths ? $this->generateApiResetUrl($storedTemplateTarget, $roleSlug) : $this->generateResetUrl($storedTemplateTarget, $roleSlug),
            $messageType,
            // add a unique value for client-side purposes
            ['value' => $storedTemplateTarget->getId().'_reset', 'description' => $description]
        );
    }

    /**
     * @param StepCollectionInterface<array-key, StepInterface> $rollbackSteps
     * @param StepCollectionInterface<array-key, StepInterface> $deadlinedSteps
     */
    private function buildRollbackActionsForRole(
        StoredTemplateTargetInterface $storedTemplateTarget,
        StepCollectionInterface $rollbackSteps,
        StepCollectionInterface $deadlinedSteps,
        string $roleSlug,
        string $locale,
        bool $useApiPaths,
    ): RevertActions {
        $actions = [];
        $allSteps = $storedTemplateTarget->getTemplate()->getCurrentVersion()->getWorkFlow()->getSteps();
        $lastUsedStep = $storedTemplateTarget->getLastUsedStep();

        foreach ($rollbackSteps->toArray() as $rollbackStep) {
            $stepUid = $rollbackStep->getUid();
            $stepIsDeadlined = $deadlinedSteps->hasOneStepByUid($stepUid);
            $stepLabel = '"'.$allSteps->getStepLabelPrefixedWithPositionNumber($rollbackStep, $locale).'"';

            // if rollback is to last used step and that step is still open (i.e. it's the current step) use custom label
            if (false === $stepIsDeadlined && $lastUsedStep->getUid() === $stepUid) {
                $label = $this->translator->trans('revert.rollback.action.restartLabel', ['%step%' => $stepLabel], 'kulFormRevert');
            } else {
                $label = $this->translator->trans('revert.rollback.action.label', ['%step%' => $stepLabel], 'kulFormRevert');
            }

            $description = '<ul>';
            $description .= '<li>'.$this->translator->trans('revert.rollback.descriptions.main', [], 'kulFormRevert').'</li>';
            $description .= '<li>'.$this->translator->trans('revert.rollback.descriptions.logsAddon', [], 'kulFormRevert').'</li>';
            $description .= '<li>'.$this->translator->trans('revert.rollback.descriptions.calculationsAddon', [], 'kulFormRevert').'</li>';
            $description .= '<li>'.$this->translator->trans('revert.rollback.descriptions.consequenceAddon', [], 'kulFormRevert').'</li>';
            if ($stepIsDeadlined) {
                $description .= '<li>'.$this->translator->trans('revert.rollback.descriptions.deadlineAddon', [], 'kulFormRevert').'</li>';
                $description .= '<li>'.$this->translator->trans('revert.general.deadlineAdjustment', [], 'kulFormRevert').'</li>';
            }

            $description .= '</ul>';
            $description .= '<p><strong>'.$this->translator->trans('revert.rollback.descriptions.warning', [], 'kulFormRevert').'</strong></p>';

            $messageType = RevertAction::TYPE_DEFAULT;

            // if step to revert to, is not open anymore, warn admin beforehand but still allow to rollback, because admins
            // can always answer questions in later steps (or deadline can be extended).
            if ($stepIsDeadlined) {
                $messageType = 'danger';
                $label .= ' ('.$this->translator->trans('revert.actions.deadlineForRollbackStepWarning', [], 'kulFormRevert').')';
            }

            $actions[] = new RevertAction(
                $label,
                $useApiPaths ? $this->generateApiRollbackUrl($storedTemplateTarget, $roleSlug, $stepUid) : $this->generateRollbackUrl($storedTemplateTarget, $roleSlug, $stepUid),
                $messageType,
                // add a unique value for client-side purposes
                ['value' => $storedTemplateTarget->getId().'_rollback_'.$stepUid, 'description' => $description]
            );
        }

        return new RevertActions(...$actions);
    }

    private function generateResetUrl(StoredTemplateTargetInterface $storedTemplateTarget, string $roleSlug): string
    {
        return $this->urlGenerator->generate(
            'kul_form.client.revert.reset',
            [
                'templateId' => $storedTemplateTarget->getTemplate()->getId(),
                'targetCode' => $storedTemplateTarget->getTargetCode(),
                'targetId' => $storedTemplateTarget->getTargetId(),
                'roleSlug' => $roleSlug,
            ]
        );
    }

    private function generateRollbackUrl(StoredTemplateTargetInterface $storedTemplateTarget, string $roleSlug, string $stepUid): string
    {
        return $this->urlGenerator->generate(
            'kul_form.client.revert.roll_back',
            [
                'templateId' => $storedTemplateTarget->getTemplate()->getId(),
                'targetCode' => $storedTemplateTarget->getTargetCode(),
                'targetId' => $storedTemplateTarget->getTargetId(),
                'stepId' => $stepUid,
                'roleSlug' => $roleSlug,
            ]
        );
    }

    private function generateApiResetUrl(StoredTemplateTargetInterface $storedTemplateTarget, string $roleSlug): string
    {
        return $this->urlGenerator->generate(
            'kul_form.client.revert.api_reset',
            [
                'templateId' => $storedTemplateTarget->getTemplate()->getId(),
                'targetCode' => $storedTemplateTarget->getTargetCode(),
                'targetId' => $storedTemplateTarget->getTargetId(),
                'roleSlug' => $roleSlug,
            ]
        );
    }

    private function generateApiRollbackUrl(StoredTemplateTargetInterface $storedTemplateTarget, string $roleSlug, string $stepUid): string
    {
        return $this->urlGenerator->generate(
            'kul_form.client.revert.api_roll_back',
            [
                'templateId' => $storedTemplateTarget->getTemplate()->getId(),
                'targetCode' => $storedTemplateTarget->getTargetCode(),
                'targetId' => $storedTemplateTarget->getTargetId(),
                'stepId' => $stepUid,
                'roleSlug' => $roleSlug,
            ]
        );
    }
}
