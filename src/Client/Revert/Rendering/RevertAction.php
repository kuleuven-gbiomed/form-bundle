<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Revert\Rendering;

class RevertAction implements \JsonSerializable
{
    final public const TYPE_DEFAULT = 'default';

    public function __construct(
        private readonly string $label,
        private readonly string $path,
        private readonly string $type,
        private readonly array $options,
    ) {
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @return array{label: string, path: string, type: string, options: mixed[]}
     */
    public function jsonSerialize(): array
    {
        return [
            'label' => $this->getLabel(),
            'path' => $this->getPath(),
            'type' => $this->getType(),
            'options' => $this->getOptions(),
        ];
    }
}
