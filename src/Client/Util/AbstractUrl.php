<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util;

use Webmozart\Assert\Assert;

abstract class AbstractUrl
{
    /** @var string */
    final public const NODE_UID_PLACEHOLDER = '__NODE_UID__';

    private readonly string $prototypeUrl;

    /** @throws \InvalidArgumentException */
    abstract protected function assertPlaceholders(): void;

    /**
     * FileUrls constructor.
     */
    public function __construct(string $prototypeUrl)
    {
        Assert::stringNotEmpty($prototypeUrl);

        $this->prototypeUrl = $prototypeUrl;

        $this->assertPlaceholders();
    }

    public function getPrototypeUrl(): string
    {
        return $this->prototypeUrl;
    }
}
