<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util;

class ActionButton implements \JsonSerializable
{
    final public const TYPE_DEFAULT = 'default';

    public function __construct(
        private readonly string $label,
        private readonly string $path,
        private readonly string $type = self::TYPE_DEFAULT,
        private readonly array $options = [],
    ) {
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * Specify data which should be serialized to JSON.
     *
     * @see  http://php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return array{label: string, path: string, type: string, options: mixed[]} data which can be serialized by <b>json_encode</b>,
     *                                                                            which is a value of any type other than a resource
     *
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return ['label' => $this->getLabel(), 'path' => $this->getPath(), 'type' => $this->getType(), 'options' => $this->getOptions()];
    }
}
