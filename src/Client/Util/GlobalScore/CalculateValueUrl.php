<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\GlobalScore;

use KUL\FormBundle\Client\Util\AbstractUrl;
use Webmozart\Assert\Assert;

class CalculateValueUrl extends AbstractUrl
{
    protected function assertPlaceholders(): void
    {
        Assert::contains($this->getPrototypeUrl(), self::NODE_UID_PLACEHOLDER);
    }
}
