<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\File;

use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Entity\UploadedFile;
use Symfony\Component\HttpFoundation\File\UploadedFile as TempFile;

/**
 * Class NodeTempFileDataRow.
 *
 * very specific helper to build serialized data for files that come in via an AJAX upload request and that have
 * to be uploaded in an upload @see UploadInputNode::$input. it is used for and allows to check and return those files in
 * the upload request that have invalid properties (invalid size or mimeType or both) based upon configuration as set
 * by the given UploadInputNode's @see UploadInput
 */
class NodeTempFileDataRow implements \JsonSerializable
{
    private readonly bool $invalidSize;
    private bool $invalidMimeType = false;
    private bool $invalidExtension = false;

    public function __construct(private readonly TempFile $tempFile, UploadInputNode $uploadNode)
    {
        $input = $uploadNode->getInput();

        $tempFileSize = $this->tempFile->getSize();
        $this->invalidSize = $tempFileSize > $input->getMaxFileSize() || $tempFileSize < $input->getMinFileSize();
        if (!$input->allowsAllMimeTypes()) {
            $this->invalidMimeType = !in_array($this->tempFile->getMimeType(), $input->getMimeTypes(), true);
        }
        if (!$input->allowsAllExtensions()) {
            $this->invalidExtension = !in_array(
                mb_strtolower($this->tempFile->getClientOriginalExtension()),
                $input->getExtensions(),
                true
            );
        }
    }

    public function isInvalid(): bool
    {
        return $this->invalidSize || $this->invalidMimeType || $this->hasInvalidExtension();
    }

    public function hasInvalidSize(): bool
    {
        return $this->invalidSize;
    }

    public function hasInvalidMimeType(): bool
    {
        return $this->invalidMimeType;
    }

    public function hasInvalidExtension(): bool
    {
        return $this->invalidExtension;
    }

    /**
     * @return array{name: string, size: string, mimeType: string|null, extension: string, hasInvalidSize: bool, hasInvalidMimeType: bool, hasInvalidExtension: bool}
     */
    public function toArray(): array
    {
        return [
            'name' => $this->tempFile->getClientOriginalName(),
            'size' => UploadedFile::convertToReadableSize($this->tempFile->getSize()),
            'mimeType' => $this->tempFile->getMimeType(),
            'extension' => $this->tempFile->getClientOriginalExtension(),
            'hasInvalidSize' => $this->invalidSize,
            'hasInvalidMimeType' => $this->invalidMimeType,
            'hasInvalidExtension' => $this->hasInvalidExtension(),
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
