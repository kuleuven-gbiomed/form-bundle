<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\File;

use KUL\FormBundle\Entity\SavedFile;

/**
 * Class SavedFileDataRow.
 *
 * specific helper to build serialized data from a @see SavedFile
 * and a @see DownloadFileUrl for returning json data after successful AJAX file upload request
 *
 * @see     SavedFileDataRowCollection
 */
class SavedFileDataRow implements \JsonSerializable
{
    /**
     * SavedFileDataRow constructor.
     */
    public function __construct(private readonly SavedFile $savedFile, private readonly DownloadFileUrl $downloadFileUrl)
    {
    }

    /**
     * @return array{nodeUid: string, fileUid: string, name: string, size: string, downloadUrl: string}
     */
    public function toArray(): array
    {
        return [
            'nodeUid' => $this->savedFile->getNodeUid(),
            'fileUid' => $this->savedFile->getUid(),
            'name' => $this->savedFile->getOriginalName(),
            'size' => $this->savedFile->getReadableSize(),
            'downloadUrl' => $this->downloadFileUrl->getUrlBySavedFile($this->savedFile),
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
