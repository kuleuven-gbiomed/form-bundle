<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\File;

use KUL\FormBundle\Entity\SavedFile;

/**
 * Creates urls for actions on {@see SavedFile}s (download, upload, delete), based on url templates.
 *
 * This helper object builds urls for a {@see SavedFile} (upload, download, delete), using uniform & asserted prototyped urls.
 * It enforces that the urls contains placeholders used by the twig templates in form-bundle.
 *
 * Although form-bundle is generic, from node & form building to twig rendering, the urls to upload / download
 * files depend on the project using form-bundle. The consuming project should e.g. take care of the
 * authorization for downloading file.
 */
final readonly class FileUrls
{
    private function __construct(private DownloadFileUrl $downloadFileUrl, private UploadFileUrl $uploadFileUrl)
    {
    }

    public static function fromPrototypeUrls(
        string $prototypeDownloadUrl,
        string $prototypeUploadUrl,
    ): self {
        return new self(
            new DownloadFileUrl($prototypeDownloadUrl),
            new UploadFileUrl($prototypeUploadUrl)
        );
    }

    public function getDownloadFileUrl(): DownloadFileUrl
    {
        return $this->downloadFileUrl;
    }

    public function getUploadFileUrl(): UploadFileUrl
    {
        return $this->uploadFileUrl;
    }
}
