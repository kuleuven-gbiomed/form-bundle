<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\File;

use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Entity\SavedFile;
use Webmozart\Assert\Assert;

/**
 * Class UploadFileUrl.
 *
 * helper object to build url for uploading a @see SavedFile by uniform & asserted prototyped url.
 * asserts that the placeholders in the url are in the format that the twigs in this bundle expect.
 * the @see InputNode::$uid is a required parameter for upload url
 */
class UploadFileUrl extends AbstractFileUrl
{
    protected function assertPlaceholders(): void
    {
        Assert::contains($this->getPrototypeUrl(), self::NODE_UID_PLACEHOLDER);
    }

    public function getUrlByNodeUid(string $nodeUid): string
    {
        Assert::notEmpty($nodeUid);

        return str_replace(self::NODE_UID_PLACEHOLDER, $nodeUid, $this->getPrototypeUrl());
    }
}
