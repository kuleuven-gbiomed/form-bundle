<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\File;

use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Entity\SavedFile;
use Webmozart\Assert\Assert;

/**
 * Class DownloadFileUrl.
 *
 * helper object to build url for downloading a @see SavedFile by uniform & asserted prototyped url.
 * asserts that the placeholders in the url are in the format that the twigs in this bundle expect.
 * the @see UploadInputNode::$uid
 * and @see SavedFile::$uid are required parameters for dowload url
 */
class DownloadFileUrl extends AbstractFileUrl
{
    protected function assertPlaceholders(): void
    {
        Assert::contains($this->getPrototypeUrl(), self::NODE_UID_PLACEHOLDER);
        Assert::contains($this->getPrototypeUrl(), self::SAVED_FILE_UID_PLACEHOLDER);
    }

    public function getUrlBySavedFile(SavedFile $submittedFile): string
    {
        $searchReplaceArray = [
            self::NODE_UID_PLACEHOLDER => $submittedFile->getNodeUid(),
            self::SAVED_FILE_UID_PLACEHOLDER => $submittedFile->getUid(),
        ];

        return str_replace(array_keys($searchReplaceArray), array_values($searchReplaceArray), $this->getPrototypeUrl());
    }
}
