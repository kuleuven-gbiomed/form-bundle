<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\File;

use KUL\FormBundle\Client\Util\AbstractUrl;

/**
 * Class AbstractFileUrl.
 */
abstract class AbstractFileUrl extends AbstractUrl
{
    /** @var string */
    final public const SAVED_FILE_UID_PLACEHOLDER = '__FILE_UID__';
}
