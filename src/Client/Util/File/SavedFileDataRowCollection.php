<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\File;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class SavedFileDataRowCollection.
 *
 * @see     SavedFileDataRow
 *
 * @method SavedFileDataRow[] toArray()
 *
 * @extends ArrayCollection<array-key,SavedFileDataRow>
 */
class SavedFileDataRowCollection extends ArrayCollection implements \JsonSerializable
{
    public function jsonSerialize(): array
    {
        return array_map(fn (SavedFileDataRow $savedFileData) => $savedFileData->jsonSerialize(), $this->toArray());
    }
}
