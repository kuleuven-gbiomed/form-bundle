<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\File;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Entity\Upload\TempFileCollection;

/**
 * @method NodeTempFileDataRow[] toArray()
 *
 * @extends ArrayCollection<array-key,NodeTempFileDataRow>
 */
final class NodeTempFileDataRowCollection extends ArrayCollection implements \JsonSerializable
{
    public static function fromTempFilesAndUploadNode(TempFileCollection $tempFiles, UploadInputNode $uploadNode): self
    {
        $nodeTempFiles = [];
        foreach ($tempFiles as $tempFile) {
            $nodeTempFiles[] = new NodeTempFileDataRow($tempFile, $uploadNode);
        }

        return new self($nodeTempFiles);
    }

    public function hasInvalid(): bool
    {
        /** @var NodeTempFileDataRow $nodeFileData */
        foreach ($this->getIterator() as $nodeFileData) {
            if ($nodeFileData->isInvalid()) {
                return true;
            }
        }

        return false;
    }

    public function getInvalid(): self
    {
        return $this->filter(fn (NodeTempFileDataRow $nodeFileData) => $nodeFileData->isInvalid());
    }

    public function hasWithInvalidSize(): bool
    {
        /** @var NodeTempFileDataRow $nodeFileData */
        foreach ($this->getIterator() as $nodeFileData) {
            if ($nodeFileData->hasInvalidSize()) {
                return true;
            }
        }

        return false;
    }

    public function hasWithInvalidMimeType(): bool
    {
        /** @var NodeTempFileDataRow $nodeFileData */
        foreach ($this->getIterator() as $nodeFileData) {
            if ($nodeFileData->hasInvalidMimeType()) {
                return true;
            }
        }

        return false;
    }

    public function hasWithInvalidExtension(): bool
    {
        /** @var NodeTempFileDataRow $nodeFileData */
        foreach ($this->getIterator() as $nodeFileData) {
            if ($nodeFileData->hasInvalidExtension()) {
                return true;
            }
        }

        return false;
    }

    public function jsonSerialize(): array
    {
        return array_map(fn (NodeTempFileDataRow $nodeFileData) => $nodeFileData->jsonSerialize(), $this->toArray());
    }
}
