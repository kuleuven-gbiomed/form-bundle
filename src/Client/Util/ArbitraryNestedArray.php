<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util;

use Doctrine\Common\Annotations\Annotation\IgnoreAnnotation;

/**
 * @IgnoreAnnotation("psalm")
 * Class ArbitraryNestedArray
 *
 * Allows to work with arbitrary nested arrays
 */
class ArbitraryNestedArray
{
    /**
     * @var array the data structure
     */
    protected array $array = [];

    /**
     * Get value in an arbitrary nested array.
     *
     * @param array $keys         Keys allowing to access value arbitrary deeply
     * @param mixed $defaultValue Value returned if no value was set before
     */
    public function get(array $keys, mixed $defaultValue = null): mixed
    {
        // return default value if no keys were given
        if (0 === count($keys)) {
            return $this->array;
        }

        /**
         * PHP does not have a syntax to dynamically access arbitrary nested arrays
         * use a simple loop to do this programmatically.
         *
         * @psalm-suppress UnsupportedPropertyReferenceUsage
         */
        $value = &$this->array;
        foreach ($keys as $key) {
            // check if combination contains any value
            if (isset($value[$key]) || array_key_exists($key, $value)) {
                // use a reference as a simple trick to not create new arrays over and over
                $value = &$value[$key];
            } else {
                return $defaultValue; // array does not contain one of the keys, return default value
            }
        }

        return $value;
    }

    /**
     * Set value in an arbitrary nested array.
     *
     * @param array $keys  Keys allowing to access value arbitrary deeply
     * @param mixed $value Value to be set
     *
     * @psalm-suppress UnsupportedReferenceUsage
     *
     * @throws \Exception if no keys were given
     */
    public function set(array $keys, mixed $value): void
    {
        $reference = &$this->getReference($keys);
        $reference = $value;
    }

    /**
     * Returns reference to a value in an arbitrary nested array.
     *
     * @param array $keys         Keys allowing to access value arbitrary deeply
     * @param mixed $defaultValue Value set at the reference if no value was found
     *
     * @throws \Exception if no keys were given
     */
    public function &getReference(array $keys, mixed $defaultValue = null): mixed
    {
        // return reference to the main array if no keys were given
        if (0 === count($keys)) {
            throw new \Exception('It is not allowed to obtain reference to the main array! Provide at least one key.');
        }

        /**
         * PHP does not have a syntax to dynamically access or set arbitrary nested arrays
         * therefore we are doing this with usage of references.
         *
         * @psalm-suppress UnsupportedPropertyReferenceUsage
         */
        $array = &$this->array;

        $i = 0;
        $countKeys = count($keys);

        // go trough all nested arrays (without the deepest reference, this will be the value)
        while ($i < $countKeys - 1) {
            // check if nested array exists, create one if not
            if (!isset($array[$keys[$i]])) {
                $array[$keys[$i]] = [];
            }

            /**
             * get a reference to the next nested array.
             *
             * @psalm-suppress UnsupportedReferenceUsage
             */
            $array = &$array[$keys[$i]];

            ++$i; // next key
        }

        $lastKey = $keys[$i];

        // check if value was found, set the default value if not
        if (!array_key_exists($lastKey, $array)) {
            $array[$lastKey] = $defaultValue;
        }

        // return reference
        return $array[$lastKey];
    }
}
