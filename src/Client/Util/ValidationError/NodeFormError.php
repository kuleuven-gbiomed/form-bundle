<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\ValidationError;

use KUL\FormBundle\Domain\Template\Element\Node\AbstractNode;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Webmozart\Assert\Assert;

/**
 * Class NodeFormError.
 *
 * custom helper object to get a serializable version of the validation error for the node in the given @see FormError
 * helps to return validation errors as Json in an XmlHttpRequest.
 *
 * IMPORTANT: this object assumes (and will indirectly asserts) usage of a @see AbstractNode
 *            for the @see FormInterface in the given FormError!
 *
 * @see     NodeFormErrorCollection
 */
class NodeFormError implements \JsonSerializable
{
    private readonly FormError $error;
    private readonly AbstractNode $node;
    private readonly string $locale;

    /**
     * NodeFormError constructor.
     */
    public function __construct(FormError $error, string $locale)
    {
        $origin = $error->getOrigin();

        if (!$origin instanceof FormInterface) {
            throw new \Exception('no origin Form find for error');
        }

        $node = $origin->getConfig()->getOption('node');

        Assert::stringNotEmpty($locale);
        Assert::isInstanceOf($node, AbstractNode::class);

        $this->error = $error;
        $this->node = $node;
        $this->locale = $locale;
    }

    /**
     * convert the @see FormError to an array of validation info related to the associated node
     * including the nested label (chain of parent category labels top-down to node label) for the node
     * (avoids confusion between validation errors on nodes with same (non-nested) labels).
     *
     * @return array{uid: string, label: string, message: string}
     */
    public function toArray(): array
    {
        return [
            'uid' => $this->node->getUid(),
            'label' => $this->node->getNestedLabel($this->locale),
            'message' => $this->error->getMessage(),
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
