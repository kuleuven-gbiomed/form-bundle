<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\ValidationError;

use KUL\FormBundle\Domain\Template\Element\Node\AbstractNode;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Webmozart\Assert\Assert;

/**
 * Class ValidationErrors.
 *
 * custom helper object to get a serializable flattened List of all validation errors
 * for each nested child Forms in given @see FormInterface
 *
 * IMPORTANT: this object is for usage with forms using @see AbstractNode for each child field only!
 *
 * @see     NodeFormError
 * @see     NodeFormErrorCollection
 */
class ValidationErrors implements \JsonSerializable
{
    private readonly NodeFormErrorCollection $nodeFormErrors;

    /**
     * ValidationErrors constructor.
     *
     * @param string $locale
     */
    public function __construct(FormInterface $form, $locale)
    {
        Assert::stringNotEmpty($locale);

        $this->nodeFormErrors = new NodeFormErrorCollection();
        /** @var FormError $error */
        foreach ($form->getErrors(true, true) as $error) {
            $this->nodeFormErrors->add(new NodeFormError($error, $locale));
        }
    }

    public function jsonSerialize(): array
    {
        return $this->nodeFormErrors->toArray();
    }
}
