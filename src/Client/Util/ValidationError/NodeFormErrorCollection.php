<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Util\ValidationError;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class NodeFormErrorCollection.
 *
 * @see     NodeFormError
 *
 * @method NodeFormError[] toArray()
 *
 * @extends ArrayCollection<array-key, NodeFormError>
 */
class NodeFormErrorCollection extends ArrayCollection implements \JsonSerializable
{
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
