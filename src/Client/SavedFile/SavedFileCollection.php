<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\SavedFile;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Entity\SavedFile;
use KUL\FormBundle\Entity\Upload\UploadedFileCollection;
use KUL\FormBundle\Entity\UploadedFile;

/**
 * @extends ArrayCollection<array-key,SavedFile>
 */
final class SavedFileCollection extends ArrayCollection implements \JsonSerializable
{
    /**
     * create collection from a set of @see UploadedFile files for a given @see InputNode.
     */
    public static function fromUploadedFilesAndInputNode(
        UploadedFileCollection $files,
        UploadInputNode $node,
    ): self {
        $savedFiles = [];

        foreach ($files as $file) {
            $savedFiles[] = SavedFile::fromUploadedFileAndInputNode($file, $node);
        }

        return new self($savedFiles);
    }

    public function getForInputNode(UploadInputNode $node): self
    {
        return $this
            ->filter(fn (SavedFile $savedFile) => $savedFile->getNodeUid() === $node->getUid())
            ->getUnique();
    }

    /**
     * check if one exists for given SavedFile identifier (uid).
     */
    public function hasOneByUid(string $uid): bool
    {
        if ('' === $uid) {
            return false;
        } // quick performance return in case of invalid string uid (no assert, method is supposed to return boolean, regardless of uid)

        return 1 === $this
                ->filter(fn (SavedFile $savedFile) => $savedFile->getUid() === $uid)
                ->count();
    }

    public function getOneByUid(string $uid): SavedFile
    {
        $result = $this->filter(fn (SavedFile $savedFile) => $savedFile->getUid() === $uid);
        $first = $result->first();
        if ($first instanceof SavedFile && 1 === $result->count()) {
            return $first;
        }

        throw new \BadMethodCallException("expected to find one SavedFile. found {$result->count()}.");
    }

    /**
     * get array with the uids of each element in the collection.
     *
     * @return string[]
     *
     * @psalm-return list<string>
     */
    public function getUids(): array
    {
        $uids = [];

        foreach ($this as $savedFile) {
            $uids[] = $savedFile->getUid();
        }

        return $uids;
    }

    /**
     * get all files with at valid size (between default min & max as set by @see UploadInput).
     *
     * especially important for filtering out files with 0 bytes caused by broken upload:
     * A file with 0 bytes size is never allowed to be submitted. but in form rendering this helps to (silently)
     * filter out invalid sized files caused in the edge case of broken uploads with partial files left behind
     */
    public function getWithValidFileSize(): self
    {
        /** @var SavedFileCollection $savedFileCollection */
        $savedFileCollection = $this->filter(function (SavedFile $savedFile) {
            $size = $savedFile->getUploadedFile()->getSize();

            return $size >= UploadInput::MIN_FILE_SIZE && $size <= UploadInput::MAX_FILE_SIZE;
        });

        return $savedFileCollection->getUnique();
    }

    /**
     * filter elements on given array of uids.
     *
     * @param string[] $uids
     */
    public function getWhereInUids(array $uids): self
    {
        /** @var SavedFileCollection $savedFileCollection */
        $savedFileCollection = $this->filter(fn (SavedFile $savedFile) => in_array($savedFile->getUid(), $uids, true));

        return $savedFileCollection->getUnique();
    }

    public function getSortedByAscendingUploadedDate(): self
    {
        /** @var \ArrayIterator<array-key,SavedFile> $iterator */
        $iterator = $this->getIterator();
        $iterator->uasort(
            fn (SavedFile $a, SavedFile $b) => ($a->getSavedAt() < $b->getSavedAt()) ? -1 : 1
        );

        return new self(iterator_to_array($iterator));
    }

    /**
     * Returns common nodeUid of the all elements in the collection. Throws exception if there is none.
     *
     * @throws \BadMethodCallException when there is no common nodeUid
     */
    public function getCommonNodeUid(): string
    {
        $first = $this->first();

        if (!$first instanceof SavedFile) {
            throw new \BadMethodCallException('The collection does not have common nodeUid!');
        }

        $nodeUid = $first->getNodeUid();

        foreach ($this as $element) {
            if (!($element->getNodeUid() === $nodeUid)) {
                throw new \BadMethodCallException('The collection does not have common nodeUid!');
            }
        }

        return $nodeUid;
    }

    /**
     * Checks whether all elements in the collection are aware of the same nodeUid.
     */
    public function hasCommonNodeUid(): bool
    {
        try {
            $this->getCommonNodeUid();

            return true;
        } catch (\BadMethodCallException) {
            return false;
        }
    }

    /**
     * Specify data which should be serialized to JSON.
     */
    public function jsonSerialize(): array
    {
        return array_map(fn (SavedFile $file) => $file->jsonSerialize(), $this->toArray());
    }

    public function getMergedWith(self $collection): self
    {
        return new self(array_merge($this->toArray(), $collection->toArray()));
    }

    public function getUnique(): self
    {
        $elements = $this->toArray();

        $uniqueElements = [];
        foreach ($elements as $key => $element) {
            // check whether element is a duplicate
            if (!in_array($element, $uniqueElements, true)) {
                $uniqueElements[$key] = $element; // preserve the first encountered key (follows array_unique behaviour)
            }
        }

        return new self($uniqueElements);
    }
}
