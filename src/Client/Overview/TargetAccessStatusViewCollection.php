<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Overview;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @extends ArrayCollection<array-key,TargetAccessStatusView>
 */
final class TargetAccessStatusViewCollection extends ArrayCollection
{
    public function getWhereAccessGrantedStatus(): self
    {
        return $this->filter(fn (TargetAccessStatusView $statusView) => $statusView->hasAccessGrantedStatus());
    }

    public function getUniqueByUrlAndHavingAccessGrantedStatus(): self
    {
        $uniquePerUrl = [];

        /** @var TargetAccessStatusView $accessStatusView */
        foreach ($this->getWhereAccessGrantedStatus() as $accessStatusView) {
            $uniquePerUrl[$accessStatusView->getViewForStoringUrl()] = $accessStatusView;
        }

        return new self(array_values($uniquePerUrl));
    }
}
