<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Overview;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\GlobalScore\Rendering\GlobalScoreQuestionResultViews;
use KUL\FormBundle\Client\Revert\Rendering\RevertActions;
use KUL\FormBundle\Client\Util\ActionButton;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Entity\Submission;

final readonly class TargetAccessStatusView implements \JsonSerializable
{
    public const KEY_ACCESS_TYPE = 'accessType';
    public const KEY_URL = 'url';
    public const KEY_ACCESSING_ROLE = 'accessingRole';
    public const KEY_ACCESSING_ROLE_SLUG = 'accessingRoleSlug';
    public const KEY_WORKFLOW_STATUS = 'workflowStatus';
    public const KEY_WORKFLOW_STATUS_DEADLINE_DATE = 'workflowStatusDeadlineDate';
    public const KEY_WORKFLOW_STATUS_DEADLINE_TIME = 'workflowStatusDeadlineTime';
    public const KEY_BUTTON = 'button';
    public const KEY_LAST_SUBMITTED_BY_FULL_NAME = 'lastSubmittedByFullName';
    public const KEY_LAST_SUBMITTED_BY_USER_ID = 'lastSubmittedByUserId';
    public const KEY_LAST_SUBMITTED_ON = 'lastSubmittedOn';
    public const KEY_REVERT_ACTIONS = 'revertActions';
    public const KEY_GLOBAL_SCORE_QUESTION_RESULT_VIEWS = 'globalScoreQuestionResultViews';

    private int $accessType;

    private function __construct(
        int $accessType,
        private ?string $currentOpenWorkflowStepLabel = null,
        private ?string $currentOpenWorkflowStepFormattedDeadlineDate = null,
        private ?string $currentOpenWorkflowStepFormattedDeadlineTime = null,
        private ?string $viewForStoringUrl = null,
        private ?Role $accessingRole = null,
        private ?string $accessingRoleSlug = null,
        private ?string $lastSubmittedByFullName = null,
        private ?string $lastSubmittedByUserId = null,
        private ?string $lastSubmittedOn = null,
        private ?RevertActions $revertActions = null,
        private ?GlobalScoreQuestionResultViews $globalScoreQuestionResultViews = null,
    ) {
        if (!in_array($accessType, ClientCombinedAccessObject::getAccessTypeDefinitions(), true)) {
            throw new \InvalidArgumentException('the accessType must be one of the defined accessTypes in '.ClientCombinedAccessObject::class);
        }

        $this->accessType = $accessType;
    }

    public static function createForNoAccessGranted(): self
    {
        return new self(ClientCombinedAccessObject::ACCESS_TYPE_NONE);
    }

    public static function createForAccessGrantedByClientAccessObject(
        ClientCombinedAccessObject $accessObject,
        string $viewForStoringUrl,
        bool $addLastSubmittedInfo,
        ?RevertActions $revertActions,
        ?GlobalScoreQuestionResultViews $globalScoreQuestionResultViews,
        ?string $locale,
    ): self {
        if (!$accessObject->grantsAccess()) {
            throw new \InvalidArgumentException('access object does not grant access!');
        }

        if ('' === $viewForStoringUrl) {
            throw new \InvalidArgumentException('the viewForStoringUrl must be a non empty string!');
        }

        $latestSubmission = $addLastSubmittedInfo ? $accessObject->getTargetSubmissions()->getSortedByDescendingSubmitDate()->first() : null;

        return new self(
            $accessObject->getAccessType(),
            $accessObject->getOpenStepNumberedLabel($locale),
            $accessObject->getOpenStepEndDate()->format('d-m-Y'),
            $accessObject->getOpenStepEndDate()->format('H-i'),
            $viewForStoringUrl,
            $accessObject->getAccessingRole(),
            $accessObject->getSlugifiedRole(),
            $latestSubmission instanceof Submission ? $latestSubmission->getSubmitterFullName() : null,
            $latestSubmission instanceof Submission ? $latestSubmission->getSubmitterId() : null,
            $latestSubmission instanceof Submission ? $latestSubmission->getSubmitDateString($locale) : null,
            $revertActions,
            $globalScoreQuestionResultViews,
        );
    }

    public function getAccessType(): int
    {
        return $this->accessType;
    }

    public function getAccessingRole(): Role
    {
        if (!$this->accessingRole instanceof Role) {
            throw new \BadMethodCallException('no accessingRole found');
        }

        return $this->accessingRole;
    }

    public function getAccessingRoleSlug(): string
    {
        if (!is_string($this->accessingRoleSlug) || '' === $this->accessingRoleSlug) {
            throw new \BadMethodCallException('no accessingRoleSlug found');
        }

        return $this->accessingRoleSlug;
    }

    public function hasAccessGrantedStatus(): bool
    {
        return !(ClientCombinedAccessObject::ACCESS_TYPE_NONE === $this->getAccessType());
    }

    public function getCurrentOpenWorkflowStepLabel(): string
    {
        if (!is_string($this->currentOpenWorkflowStepLabel) || '' === $this->currentOpenWorkflowStepLabel) {
            throw new \BadMethodCallException('no currentOpenWorkflowStepLabel found');
        }

        return $this->currentOpenWorkflowStepLabel;
    }

    public function getCurrentOpenWorkflowStepFormattedDeadlineDate(): string
    {
        if (!is_string($this->currentOpenWorkflowStepFormattedDeadlineDate) || '' === $this->currentOpenWorkflowStepFormattedDeadlineDate) {
            throw new \BadMethodCallException('no currentOpenWorkflowStepFormattedDeadlineDate found');
        }

        return $this->currentOpenWorkflowStepFormattedDeadlineDate;
    }

    public function getCurrentOpenWorkflowStepFormattedDeadlineTime(): string
    {
        if (!is_string($this->currentOpenWorkflowStepFormattedDeadlineTime) || '' === $this->currentOpenWorkflowStepFormattedDeadlineTime) {
            throw new \BadMethodCallException('no currentOpenWorkflowStepFormattedDeadlineTime found');
        }

        return $this->currentOpenWorkflowStepFormattedDeadlineTime;
    }

    public function getViewForStoringUrl(): string
    {
        if (!is_string($this->viewForStoringUrl) || '' === $this->viewForStoringUrl) {
            throw new \BadMethodCallException('no viewForStoringUrl found');
        }

        return $this->viewForStoringUrl;
    }

    public function findRevertActions(): ?RevertActions
    {
        return $this->revertActions;
    }

    public function findGlobalScoreQuestionResultViews(): ?GlobalScoreQuestionResultViews
    {
        return $this->globalScoreQuestionResultViews;
    }

    public function findLastSubmittedByFullName(): ?string
    {
        return $this->lastSubmittedByFullName;
    }

    public function findLastSubmittedByUserId(): ?string
    {
        return $this->lastSubmittedByUserId;
    }

    public function findLastSubmittedOn(): ?string
    {
        return $this->lastSubmittedOn;
    }

    public function jsonSerialize(): array
    {
        $data = [self::KEY_ACCESS_TYPE => $this->getAccessType()];

        // no access means user can not see any status info, revert options, last submitted info, etc. this means that
        // the implementing app has to check the returned $data['accessType'] first before trying to check for any other of the data.
        // no access = not at least read access on at least one question in current step for user-role. if user has no
        // access, then there is no accessing role anyway to build most of the data with.
        if (!$this->hasAccessGrantedStatus()) {
            return $data;
        }

        // always available if access granted
        $data[self::KEY_URL] = $this->getViewForStoringUrl();
        $data[self::KEY_ACCESSING_ROLE] = $this->getAccessingRole()->getName();
        $data[self::KEY_ACCESSING_ROLE_SLUG] = $this->getAccessingRoleSlug();
        $data[self::KEY_WORKFLOW_STATUS] = $this->getCurrentOpenWorkflowStepLabel();
        $data[self::KEY_WORKFLOW_STATUS_DEADLINE_DATE] = $this->getCurrentOpenWorkflowStepFormattedDeadlineDate();
        $data[self::KEY_WORKFLOW_STATUS_DEADLINE_TIME] = $this->getCurrentOpenWorkflowStepFormattedDeadlineTime();
        $data[self::KEY_BUTTON] = $this->findAccessButton($this->getViewForStoringUrl());

        // optionally available if access granted: only if at least once submitted (at least one question answered, not temp saved!)
        $data[self::KEY_LAST_SUBMITTED_BY_FULL_NAME] = $this->findLastSubmittedByFullName();
        $data[self::KEY_LAST_SUBMITTED_BY_USER_ID] = $this->findLastSubmittedByUserId();
        $data[self::KEY_LAST_SUBMITTED_ON] = $this->findLastSubmittedOn();

        // optionally available if access granted: if provided
        $data[self::KEY_REVERT_ACTIONS] = $this->findRevertActions();
        $data[self::KEY_GLOBAL_SCORE_QUESTION_RESULT_VIEWS] = $this->findGlobalScoreQuestionResultViews();

        return $data;
    }

    /**
     * get ActionButton for primary access, containing the link to the manage page
     * and with label, css class and icon set according to access type.
     */
    public function findAccessButton(string $path): ?ActionButton
    {
        if ('' === $path) {
            throw new \InvalidArgumentException('the path must be a non empty string!');
        }

        return match ($this->getAccessType()) {
            ClientCombinedAccessObject::ACCESS_TYPE_WRITE_FIRST_SUBMIT => new ActionButton(
                'writeFirstSubmit',
                $path,
                'primary',
                ['class' => 'form-detail-link', 'icon' => 'ico-plus']
            ),
            ClientCombinedAccessObject::ACCESS_TYPE_WRITE_EDIT => new ActionButton(
                'writeEdit',
                $path,
                'default',
                ['class' => 'form-detail-link', 'icon' => 'ico-pencil']
            ),
            ClientCombinedAccessObject::ACCESS_TYPE_READ_ONLY => new ActionButton(
                'stepReadOnly',
                $path,
                'default',
                ['class' => 'form-detail-link', 'icon' => 'ico-file4']
            ),
            default => null,
        };
    }
}
