<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\AnswerValue;

/**
 * Class Value.
 *
 * helper to collect a value with information about its identifier and whether it was added automatically or not
 * used for storing/serializing values for @see InputNode
 *
 * @see ValueCollection
 */
class Value implements \JsonSerializable
{
    private readonly string $uid;

    /**
     * @param string $uid       identifier (i.e. the node uid)
     * @param mixed  $value     could be null, array (choice input), string (text/textarea/file input) or numeric (number input)
     * @param bool   $autoAdded true if the value was added automatically (without submitting user's knowledge)
     */
    private function __construct(
        string $uid,
        private readonly mixed $value,
        private readonly bool $autoAdded,
    ) {
        if ('' === $uid) {
            throw new \InvalidArgumentException('uid must be a non-empty string');
        }

        $this->uid = $uid;
    }

    public static function createFromArray(array $array): self
    {
        return new self($array['uid'], $array['value'], $array['autoAdded']);
    }

    public static function createFromUidAndValue(
        string $uid,
        mixed $value,
        bool $autoAdded = false,
    ): self {
        return new self($uid, $value, $autoAdded);
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function isAutoAdded(): bool
    {
        return $this->autoAdded;
    }

    /**
     * @return array{uid: string, value: mixed, autoAdded: bool}
     */
    public function toArray(): array
    {
        return [
            'uid' => $this->getUid(),
            'value' => $this->getValue(),
            'autoAdded' => $this->isAutoAdded(),
        ];
    }

    /**
     * @return array{uid: string, value: mixed, autoAdded: bool}
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
