<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\AnswerValue;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ValueCollection.
 *
 * @see Value
 *
 * @extends ArrayCollection<array-key,Value>
 */
class ValueCollection extends ArrayCollection implements \JsonSerializable
{
    /**
     * returns array with @see Value::getUid() from each element in the collection.
     *
     * @return string[]
     */
    public function getUids(): array
    {
        $uids = [];

        foreach ($this as $value) {
            $uids[] = $value->getUid();
        }

        return $uids;
    }

    /**
     * Map to array indexed by ids.
     *
     * @return Value[]
     */
    public function toArrayIndexedByUids(): array
    {
        return array_combine(
            $this->getUids(),
            $this->toArray()
        );
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
