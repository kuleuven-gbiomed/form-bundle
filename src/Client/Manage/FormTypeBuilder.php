<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage;

use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\Manage\FormType\Contract\TypeInterface;
use KUL\FormBundle\Client\Manage\FormType\NestedType;
use KUL\FormBundle\Client\Manage\FormType\Node\AbstractPreSetInputNodeObject;
use KUL\FormBundle\Client\Manage\FormType\Node\PreSetCheckboxNodeObject;
use KUL\FormBundle\Client\Manage\FormType\Node\PreSetChoiceNodeObject;
use KUL\FormBundle\Client\Manage\FormType\Node\PreSetDateNodeObject;
use KUL\FormBundle\Client\Manage\FormType\Node\PreSetInputNodeObjectBuilder;
use KUL\FormBundle\Client\Manage\FormType\Node\PreSetNumberNodeObject;
use KUL\FormBundle\Client\Manage\FormType\Node\PreSetTextNodeObject;
use KUL\FormBundle\Client\Manage\FormType\Node\PreSetUploadNodeObject;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Utility\Role;
use Symfony\Component\Form\FormBuilderInterface;

class FormTypeBuilder
{
    /** @var string */
    final public const FALLBACK_DEFAULT_PLACEHOLDER = 'choose...';

    public static function addNodeFormType(
        FormBuilderInterface $builder,
        NodeInterface $node,
        array $values,
        SavedFileCollection $savedFiles,
        StepInterface $step,
        Role $role,
        array $tempValues = [],
        ?SubmitHistory $history = null,
        ?SubmitHistory $tempHistory = null,
    ): FormBuilderInterface {
        $history = $history instanceof SubmitHistory ? $history : new SubmitHistory();
        $tempHistory = $tempHistory instanceof SubmitHistory ? $tempHistory : new SubmitHistory();

        if ($node->isHidden()) {
            return $builder;
        } // don't build hidden fields & categories (set as hidden on node itself)

        if (!$node->isStepRoleWriteAccessGranted($step, $role)) {
            return $builder;
        } // don't build if flowPermission is used (= step & role not empty) & no write access granted (skips readOnlyView nodes)

        // case category
        if ($node instanceof CategoryNode) {
            return self::addCategoryNodeFormType(
                $builder,
                $node,
                $values,
                $step,
                $role,
                $tempValues,
                $history,
                $tempHistory,
                $savedFiles
            );
        }

        // if not category, then it's an input node. check explicitly for QA tools
        if ($node instanceof InputNode) {
            // case question
            return self::addInputNodeFormType(
                $builder,
                $node,
                $values,
                $tempValues,
                $savedFiles,
                $history,
                $tempHistory
            );
        }

        // not category & not input node should not occur...
        throw new \InvalidArgumentException('unknown type of node: '.$node::class);
    }

    private static function addCategoryNodeFormType(
        FormBuilderInterface $builder,
        CategoryNode $node,
        array $values,
        StepInterface $step,
        Role $role,
        array $tempValues,
        SubmitHistory $history,
        SubmitHistory $tempHistory,
        SavedFileCollection $savedFiles,
    ): FormBuilderInterface {
        // don't render categories without active children (i.e. category is not set as hidden but ALL its children are)
        if (!$node->hasReachableParentalInputNodeDescendants()) {
            return $builder;
        }

        $fieldOptions = array_merge(self::buildNodeFieldOptions($node, $builder), [
            NestedType::KEY_NODES => $node->getChildren(),
            NestedType::KEY_VALUES => $values,
            NestedType::KEY_STEP => $step,
            NestedType::KEY_ROLE => $role,
            NestedType::KEY_TEMP_VALUES => $tempValues,
            NestedType::KEY_HISTORY => $history,
            NestedType::KEY_TEMP_HISTORY => $tempHistory,
            NestedType::KEY_SAVED_FILES => $savedFiles,
        ]);
        $builder->add($node->getUid(), NestedType::class, $fieldOptions);

        return $builder;
    }

    private static function addInputNodeFormType(
        FormBuilderInterface $builder,
        InputNode $node,
        array $values,
        array $tempValues,
        SavedFileCollection $savedFiles,
        SubmitHistory $history,
        SubmitHistory $tempHistory,
    ): FormBuilderInterface {
        $preSetObject = PreSetInputNodeObjectBuilder::createFromNodeAndValues(
            $node,
            $values,
            $tempValues,
            $history,
            $tempHistory,
            $savedFiles
        );

        if ($preSetObject instanceof PreSetTextNodeObject) {
            return self::addTextInputFormType($builder, $preSetObject);
        }

        if ($preSetObject instanceof PreSetNumberNodeObject) {
            return self::addNumberInputFormType($builder, $preSetObject);
        }

        if ($preSetObject instanceof PreSetCheckboxNodeObject) {
            return self::addCheckboxInputFormType($builder, $preSetObject);
        }

        if ($preSetObject instanceof PreSetChoiceNodeObject) {
            return self::addChoiceInputFormType($builder, $preSetObject);
        }

        if ($preSetObject instanceof PreSetUploadNodeObject) {
            return self::addUploadInputFormType($builder, $preSetObject);
        }

        if ($preSetObject instanceof PreSetDateNodeObject) {
            return self::addDateInputFormType($builder, $preSetObject);
        }

        // case of new preSet objects not handled (should not occur, PreSetInputNodeObjectBuilder handles exceptions)
        throw new \BadMethodCallException("cannot handle node [{$node->getUid()}] : unhandled instance of ".AbstractPreSetInputNodeObject::class);
    }

    private static function getLocale(FormBuilderInterface $builder): string
    {
        $locale = $builder->getOption(TypeInterface::KEY_LOCALE);

        if (!is_string($locale) || '' === $locale) {
            throw new \BadFunctionCallException('locale is missing or empty');
        }

        return $locale;
    }

    private static function buildNodeFieldOptions(NodeInterface $node, FormBuilderInterface $builder): array
    {
        return [TypeInterface::KEY_NODE => $node, TypeInterface::KEY_LOCALE => self::getLocale($builder)];
    }

    private static function buildInputNodeFieldOptions(InputNode $node, FormBuilderInterface $builder): array
    {
        return array_merge(
            self::buildNodeFieldOptions($node, $builder),
            $node->getInput()->getFieldOptions(self::getLocale($builder))
        );
    }

    private static function addCheckboxInputFormType(FormBuilderInterface $builder, PreSetCheckboxNodeObject $preSetObject): FormBuilderInterface
    {
        return self::addGenericInputFormType($builder, $preSetObject);
    }

    private static function addNumberInputFormType(FormBuilderInterface $builder, PreSetNumberNodeObject $preSetObject): FormBuilderInterface
    {
        return self::addGenericInputFormType($builder, $preSetObject);
    }

    private static function addUploadInputFormType(FormBuilderInterface $builder, PreSetUploadNodeObject $preSetObject): FormBuilderInterface
    {
        return self::addGenericInputFormType($builder, $preSetObject);
    }

    private static function addTextInputFormType(FormBuilderInterface $builder, PreSetTextNodeObject $preSetObject): FormBuilderInterface
    {
        return self::addGenericInputFormType($builder, $preSetObject);
    }

    private static function addDateInputFormType(FormBuilderInterface $builder, PreSetDateNodeObject $preSetObject): FormBuilderInterface
    {
        return self::addGenericInputFormType($builder, $preSetObject);
    }

    private static function addChoiceInputFormType(FormBuilderInterface $builder, PreSetChoiceNodeObject $preSetObject): FormBuilderInterface
    {
        // a dropdown without options is pointless (just in case. because admin-manage side (should) enforce this)
        if (!$preSetObject->hasAvailableOptions()) {
            return $builder;
        }

        $fieldOptions = self::buildInputNodeFieldOptions($preSetObject->getNode(), $builder);
        $locale = self::getLocale($builder);

        $choices = [];
        $choiceNode = $preSetObject->getNode();
        $choiceInput = $choiceNode->getInput();
        // add placeholder choice-option for non-expanded choice questions (= if NOT to be rendered as radios or
        // checkboxes!), if it is needed.  this will be used as option in select(ize). contains empty string value
        // conform to ChoiceType. if this empty placeholder choice-option is submitted (and choice is set as required),
        // it will not pass a NotBlank() constraint. if choice is NOT required, it will be considered as valid empty submit.
        // adding this placeholder it as the first choice-option, before any other choice options, for UX reasons.
        if ($choiceInput->usePlaceholder()) {
            $choices[] = null;
        }

        // add the other options as objects (will handle value and key via custom choiceType methods)
        $newChoices = $choiceNode->getAvailableOptions()->toArray();
        $fieldOptions['choices'] = array_merge($choices, $newChoices);

        // there could be duplicate labels. this is intentional. to preserve those, build custom labels
        $fieldOptions['choice_label'] = function (?InputNode $optionInputNode = null) use ($locale, $choiceInput): string {
            // add the translated label of the option node
            if ($optionInputNode instanceof InputNode) {
                return $optionInputNode->getLabel($locale);
            }

            // if no option node, then it's the empty placeholder option. and if empty option added, it means
            // the choice will have a placeholder -> render custom label (usually 'choose...')
            if ($choiceInput->getLocalizedPlaceholder()->hasForLocaleOrFallback($locale)) {
                return $choiceInput->getLocalizedPlaceholder()->getForLocaleOrFallback($locale);
            }

            // if for some (edge case) reason, no placeholder is available, use some sensible default to avoid breaking UX
            return self::FALLBACK_DEFAULT_PLACEHOLDER;
        };

        // there could be duplicate labels. this is intentional. to avoid Symfony skipping duplicates, use the unique
        // option node uids to set values explicitly, otherwise Symfony will use labels as values and merge duplicates
        // in one. (common use of choice in development is having unique 'labels' (e.g. 'yes', 'no', 'maybe'), but in
        // this bundle the use of duplicate labels is not only allowed, it is a not so rare use case!)
        $fieldOptions['choice_value'] = function (?InputNode $optionInputNode = null): string {
            // add the unique uid  of the option node as identifiable option
            if ($optionInputNode instanceof InputNode) {
                return $optionInputNode->getUid();
            }

            // if no option node, then it's the empty placeholder option. add the default empty string value
            // that ChoiceType recognizes as empty submit. depending on choice's required state, that will pass
            // constraints or return with the default notBlank message
            return '';
        };

        // pre set data with the previously or default option node(s) (if any), (either the temp saved or submitted one(s))
        // could be either array of option objects or one option object, depending on 'multiple' state of the choice
        // (Symfony uses array for multiple, one object/value for non-multiple)
        $fieldOptions['data'] = $preSetObject->getPreSetDataValue();

        if ($preSetObject->usePreSetDataTemporaryHistoryEntry()) {
            $fieldOptions['temporaryHistoryEntry'] = $preSetObject->getPreSetDataTemporaryHistoryEntry();
        }

        $builder->add($preSetObject->getNode()->getUid(), $preSetObject->getFormTypeAlias(), $fieldOptions);

        return $builder;
    }

    private static function addGenericInputFormType(FormBuilderInterface $builder, AbstractPreSetInputNodeObject $preSetObject): FormBuilderInterface
    {
        $fieldOptions = self::buildInputNodeFieldOptions($preSetObject->getNode(), $builder);
        $fieldOptions['data'] = $preSetObject->getPreSetDataValue();
        if ($preSetObject->usePreSetDataTemporaryHistoryEntry()) {
            $fieldOptions['temporaryHistoryEntry'] = $preSetObject->getPreSetDataTemporaryHistoryEntry();
        }

        $builder->add($preSetObject->getNode()->getUid(), $preSetObject->getFormTypeAlias(), $fieldOptions);

        return $builder;
    }
}
