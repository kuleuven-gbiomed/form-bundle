<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\Manage\FormType\BaseType;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Domain\Utility\Role;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Webmozart\Assert\Assert;

class FormManager
{
    /**
     * FormManager constructor.
     */
    public function __construct(private readonly FormHandler $formHandler, private readonly FormFactoryInterface $formFactory, private readonly string $defaultLocale)
    {
    }

    /**
     * @return FormInterface
     *
     * @throws MissingVersionException
     */
    public function createFormByAccessObject(ClientCombinedAccessObject $accessObject, string $postUrl, ?string $locale)
    {
        return $this->createForm($accessObject, $postUrl, $locale, []);
    }

    /**
     * create a form that will allows to disable the constraint validation on all node fields in the form that are
     * NOT linked to the given global score node. keeping only the full constraint validation on the fields needed to
     * calculate the global score. this form allows to autocalculate the global score by posting a full (default) form
     * via ajax POST with full validation on the required fields while still keeping basic anti-hack validation on other fields.
     *
     * NOTE resulting form will use 'validation_groups' to disable (FALSE) or enable (['Default']) full validation
     *      basic validation = does field exists, does option exist in choice type, no string values in number fields, etc.
     *
     * @return FormInterface
     *
     * @throws MissingVersionException
     * @throws MissingNodeException
     */
    public function createFormByAccessObjectForGlobalScore(
        ClientCombinedAccessObject $accessObject,
        string $postUrl,
        ScorableInputNode $globalScoreNode,
        ?string $locale,
    ) {
        Assert::true($globalScoreNode->operatesAsGlobalScoreNode());
        Assert::true($accessObject->getCurrentVersionFormList()->hasNode($globalScoreNode));

        return $this->createForm(
            $accessObject,
            $postUrl,
            $locale,
            [BaseType::KEY_ONLY_VALIDATE_FOR_GLOBAL_SCORE_NODE => $globalScoreNode]
        );
    }

    /**
     * @return array{action: string, step: StepInterface, role: Role, useErrorAnchors: true}
     */
    private function getDefaultOptions(ClientCombinedAccessObject $accessObject, string $postUrl)
    {
        return [
            'action' => $postUrl,
            BaseType::KEY_STEP => $accessObject->getOpenFlowStep(),
            BaseType::KEY_ROLE => $accessObject->getAccessingRole(),
            BaseType::KEY_USE_ERROR_ANCHORS => true,
        ];
    }

    /**
     * @param array $options options to merge in default options, with higher priority
     *
     * @return FormInterface
     *
     * @throws MissingVersionException
     */
    private function createForm(
        ClientCombinedAccessObject $accessObject,
        string $postUrl,
        ?string $locale,
        array $options = [],
    ) {
        Assert::true($accessObject->isWriteAccessAuthorized());
        Assert::notEmpty($postUrl);

        $options['method'] = 'POST';
        $options[BaseType::KEY_FORM_LIST] = $accessObject->getCurrentVersionFormList();
        $options[BaseType::KEY_LOCALE] = null !== $locale && '' !== $locale ? $locale : $this->defaultLocale;

        // determine if we need to bother retrieving passing along submitted values, temp saved values, history and other
        // stuff that only could be present if template-target was at least once submitted or (auto) temporary saved before
        $options[BaseType::KEY_VALUES] = [];
        $options[BaseType::KEY_TEMP_VALUES] = [];
        $options[BaseType::KEY_HISTORY] = new SubmitHistory();
        $options[BaseType::KEY_TEMP_HISTORY] = new SubmitHistory();
        $options[BaseType::KEY_SAVED_FILES] = new SavedFileCollection();

        if ($accessObject->hasStoredTemplateTarget()) {
            $options[BaseType::KEY_VALUES] = $accessObject->getStoredTemplateTarget()->getSubmittedFormValues();
            $options[BaseType::KEY_TEMP_VALUES] = $accessObject->getStoredTemplateTarget()->getTemporarySavedFormValues();
            $options[BaseType::KEY_HISTORY] = $accessObject->getStoredTemplateTarget()->getLatestSavedQuestionEntriesForCurrentFormList();
            $options[BaseType::KEY_TEMP_HISTORY] = $accessObject->getStoredTemplateTarget()->getLatestTemporarySavedQuestionEntriesForCurrentFormList();
            $options[BaseType::KEY_SAVED_FILES] = $accessObject->getStoredTemplateTarget()->getSavedFiles();
        }

        return $this->formFactory->create(
            BaseType::class,
            null,
            array_merge($this->getDefaultOptions($accessObject, $postUrl), $options)
        );
    }

    /**
     * @throws \Exception
     */
    public function handleRequest(FormInterface $form, Request $request): void
    {
        $this->formHandler->handleRequest($form, $request);
    }

    /**
     * @throws MissingVersionException
     * @throws \Exception
     */
    public function getFlattenedFormValues(FormInterface $form): array
    {
        return $this->formHandler->getFlattenedFormData($form);
    }

    /**
     * @throws MissingVersionException
     * @throws \Exception
     */
    public function getFlattenedFormValuesMergedWithUnWritableAndUnReachableStoredValues(
        FormInterface $form,
        StoredTemplateTargetInterface $storedTemplateTarget,
    ): array {
        $newFormDataValues = $this->formHandler->getFlattenedFormData($form);
        $isTemporarySaveRequested = $this->formHandler->isTemporarySaveRequested($form);

        $formList = $storedTemplateTarget->getTemplate()->getCurrentVersion()->getFormList();

        $oldStoredFormValues = $isTemporarySaveRequested ? $storedTemplateTarget->getTemporarySavedFormValues() : $storedTemplateTarget->getSubmittedFormValues();

        $newFormDataValues = FormValuesMerger::mergeReachableNewFormValuesWithUnreachableOldFormValues(
            $formList,
            $newFormDataValues,
            $oldStoredFormValues
        );

        /** @var StepInterface $step */
        $step = $form->getConfig()->getOption(BaseType::KEY_STEP);
        /** @var Role $role */
        $role = $form->getConfig()->getOption(BaseType::KEY_ROLE);

        return FormValuesMerger::mergeNewFormValuesWithReachableUnWritableOldFormValues(
            $formList,
            $newFormDataValues,
            $oldStoredFormValues,
            $step,
            $role
        );
    }

    public function isTemporarySaveRequested(FormInterface $form): bool
    {
        return $this->formHandler->isTemporarySaveRequested($form);
    }

    /**
     * check if the given form was validly submitted with the intention to temporary save it's data.
     */
    public function isFormValidForTemporarySave(FormInterface $form): bool
    {
        return $form->isValid() && $form->isSubmitted() && $this->isTemporarySaveRequested($form);
    }
}
