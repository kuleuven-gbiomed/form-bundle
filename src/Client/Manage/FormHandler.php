<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage;

use Exception;
use KUL\FormBundle\Client\Manage\FormType\BaseType;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\CheckBoxInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\DateInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;
use Symfony\Component\Form\ClickableInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class FormHandler
{
    /**
     * FormHandler constructor.
     */
    public function __construct(private readonly WysiwygSanitizer $sanitizer)
    {
    }

    /**
     * maps & replaces the submitted flat array of key-value pairs in the POST data (with keys = uids of @see InputNode )
     * for given form back to the nested structure in that form (as set by form's option @see FormList ).
     * for the mapping it uses the uids of the ancestors in the tree structure of each inputNode and appends the
     * inputNodeUid, using array_merge and the ordering as set by @see InputNode::getAncestors() to keep correct order!
     *
     * IMPORTANT: - this is used for @see BaseType inside this Bundle!
     *
     * @throws \Exception
     */
    public function handleRequest(FormInterface $form, Request $request): void
    {
        $request = RequestFormDataConverter::convertPostRequestBaseFormDataToNestedFormData($form, $request);

        $form->handleRequest($request);
    }

    /**
     * flatten the submitted form data values in to a single dimensional array, keyed by each submitted nodeUid,
     * based on the @see FormList as supplied in the option 'formList' in the given form
     * WARNING: throws Exception if the form is not submitted or not valid
     *          to assert calling this method before the valid/submitted phase.
     *
     * @throws \Exception
     */
    public function getFlattenedFormData(FormInterface $form): array
    {
        self::assertValidSubmittedForm($form);

        /** @var array $formData */
        $formData = $form->getData();
        /** @var FormList $formList */
        $formList = $form->getConfig()->getOption('formList');

        return $this->flattenFormDataByFormList($formList, $formData);
    }

    /**
     * check if temporary save is requested by checking if the temporary save btn is available & clicked
     * throws exception if the field is present but not instance of @see ClickableInterface,
     * see corresponding button field in the @see BaseType.
     *
     * @throws \Exception
     */
    public function isTemporarySaveRequested(FormInterface $form): bool
    {
        if (!$form->has(BaseType::getTemporarySaveSubmitTypeFieldName())) {
            return false;
        }

        $tempSaveSubmitField = $form->get(BaseType::getTemporarySaveSubmitTypeFieldName());
        if (!$tempSaveSubmitField instanceof ClickableInterface) {
            throw new \Exception('the save draft field (button type) must implement the '.ClickableInterface::class);
        }

        return $tempSaveSubmitField->isClicked();
    }

    private function flattenFormDataByFormList(FormList $formList, array $formData): array
    {
        $result = [];

        foreach ($formData as $nodeUid => $formValue) {
            // 0 fetch the node
            if (!$formList->hasNodeByUid($nodeUid)) {
                continue;
            }

            $node = $formList->getNodeByUid($nodeUid);

            // 0 if node is CategoryNode, value will be array where keys are uids of (category and input) nodes
            // and values will be mixed, depending of type of node -> go recursive
            if ($node instanceof CategoryNode) {
                // DEVELOPER WARNING: just in case of changes on Nested (category) FormType building: catch $value changed to collection
                // of Nodes. need an array here, so just to be sure, if it's changed to a collection, cast to array
                if ($formValue instanceof NodeCollection) {
                    $formValue = $formValue->toArray();
                }

                if (is_array($formValue)) { // should normally be array, but just checking to be sure and keeping QA happy
                    $result = array_merge($result, $this->flattenFormDataByFormList($formList, $formValue));
                }
                continue;
            }

            // 2 if node is choiceInput Node, value will be array of optionNodes or instance of optionNode, depending on
            // how choiceInput Node is configured with regards to it's 'multiple' config
            if ($node instanceof ChoiceInputNode) { // if choiceInput Node
                // 1 store the uid of choiceInput Node to allow check/search/dependency/... on presence in submitted data
                // this means the Node uid will always be available in submitted values, regardless of its value.
                // this makes sure we can check if the choice question was submitted/tempsaved, regardless of any (if any) choice option answers given
                // (in case the $value linked to this key-uid is NULL, it means the uid will still be stored, but no child-optionInput will be stored)
                $result[$nodeUid] = true; // mark choice question as (part of) submitted (formData)

                // 2 store each value in the choiceInput Node that user has chosen separately to allow check/search/dependency/... in submitted data
                // (the value(s) will be either a single OptionInput node (if choice node is not multiple)
                // or an array (NOT a Collection!) of the child OptionInput node(s) in the parent choiceInput Node,
                // as based on the way the custom formType for a choice question is build)

                // DEVELOPER NOTICE: just in case of changes on Choice FormType building, checking for OptionInputNodeCollection as well
                if (is_array($formValue) || $formValue instanceof OptionInputNodeCollection) {
                    // 2.1. if the choiceInput Node is multiple-true, the value will be an array (a.k.a. select (expanded-false) or set of checkboxes (expanded-true)
                    // containing the child-option input nodes if the choiceInput required at least one choice to be made.
                    // if the choice input is not required (or temp saved which also does not require an answer) ,
                    // the $value will be an empty array and only the nodeUid will be stored (see above) to indicate the choice was submitted, but with no answers
                    foreach ($formValue as $key => $optionInputNode) {
                        if ($optionInputNode instanceof OptionInputNode) {
                            if ($optionInputNode->usesAdditionalTextInput()) {
                                $otherValueKey = $optionInputNode->getUid().ChoiceInputNode::OTHER_VALUE_FIELD_SUFFIX;
                                $result[$optionInputNode->getUid()] = $formData[$otherValueKey];
                            } else {
                                $result[$optionInputNode->getUid()] = true;
                            }
                        }
                    }
                } elseif ($formValue instanceof OptionInputNode) {
                    // 2.1 if the choiceInput Node is multiple-false (a single choice select or radio buttons),
                    // and the choiceInput is required (notBlank()) so that the value can not be null,
                    // the value will contain the single chosen OptionInput node (child(ren) of choiceInput)
                    // store its uid in the same way as if multiple was true, so that if in later version the multiple-parameter changes to true,
                    // the choice input will have no issue treating and implementing this value/uid in the multiple-array
                    if ($formValue->usesAdditionalTextInput()) {
                        // if the question uses radiobuttons or checkboxes, the way it works is different
                        // we have to get the value of the added text input field and add it to the correct node
                        $otherValueKey = $formValue->getUid().ChoiceInputNode::OTHER_VALUE_FIELD_SUFFIX;
                        $result[$formValue->getUid()] = $this->sanitizer->sanitizeTextValue($formData[$otherValueKey]);
                    } else {
                        // if node is a normal OptionInputNode, the value is simply 'true"
                        $result[$formValue->getUid()] = true;
                    }
                }

                continue;
            }

            // 3 if no category or choice input node, value will not be array and contains submitted data for
            // field string|null|int|bool..., depending on node type (at time of writing!)

            // 3.1 if node is text input node, value will be (empty) string or NULL
            if ($node instanceof TextInputNode) {
                // if text node is 'multiline' (textarea) (and set to use wysiwyg), the value must definitely be
                // sanitized for invalid HTML tags (like malicious script tags) that are not allowed (as configured
                // by FormBundle config parameters 'wysiwygAllowedTags ' and'allowedFrameTargets'
                // for 'normal' text or texarea without wysiwyg, this technically does not need sanitizing. however,
                // a user could still insert malicious script tags in a simple text input or textarea without wysiwyg.
                // so it's better so sanitize all text input to avoid having an text answer with e.g. script tags,
                // to be raw displayed and executing some script! no need for sanitizing if empty (string) or null
                // $result[$nodeUid] =  empty($value) || !$node->getInput()->isMultiLine() ? $value : $this->sanitizer->sanitizeTextValue($value);
                /** @var string|null $textValue keep QA tools happy */
                $textValue = $formValue;
                $result[$nodeUid] = (null === $formValue || '' === $formValue) ? $formValue : $this->sanitizer->sanitizeTextValue($textValue);
                continue;
            }

            // 3.2 if node is checkbox input node, value will be bool
            if ($node instanceof CheckBoxInputNode) {
                $result[$nodeUid] = $formValue;
                continue;
            }

            // 3.3 if node is number input node, value will be numeric or NULL
            if ($node instanceof NumberInputNode) {
                $result[$nodeUid] = $formValue;
                continue;
            }

            // 3.4 if node is upload input node, value will be string(!) or NULL
            if ($node instanceof UploadInputNode) {
                $result[$nodeUid] = $formValue;
                continue;
            }

            // 3.5 ignore the option inputs. those are handled in choice input node. there should never be any in the
            // flattened submitted array of values coming from submitted form, but there is no reason to throw an
            // exception if there are any, just ignore them and continue
            if ($node instanceof OptionInputNode) {
                continue;
            }

            // 3.6 if node is date input node, value will be string(!) or NULL
            if ($node instanceof DateInputNode) {
                $result[$nodeUid] = (null === $formValue || '' === $formValue) ? $formValue : IsoDateConverter::convertToIsoString($formValue, $node->getInput()->getFormatMask());
                continue;
            }

            // in case of a new type of input node added
            throw new \RuntimeException('DEVELOPER WARNING: a node of type '.$node::class.' is not (yet) handled! did you add a new type of (Input)Node?');
        }

        return $result;
    }

    /**
     * @throws \Exception
     */
    protected function assertValidSubmittedForm(FormInterface $form): void
    {
        if (!$form->isValid()) {
            throw new \Exception(self::class.": can not handle FormType with name: '".$form->getName()."'. form must be valid");
        }

        if (!$form->isSubmitted()) {
            throw new \Exception(self::class.": can not handle FormType with name: '".$form->getName()."'. form must be submitted");
        }

        $formList = $form->getConfig()->getOption('formList');
        if (!$formList instanceof FormList) {
            throw new \Exception(self::class.": can not handle FormType name: '".$form->getName()."'. the form does not contain an option 'formList' or option 'formList' is not an instance of FormList");
        }
    }
}
