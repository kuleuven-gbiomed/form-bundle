<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Utility\Role;

class FormValuesMerger
{
    public static function mergeReachableNewFormValuesWithUnreachableOldFormValues(
        FormList $formList,
        array $newFormValues,
        array $oldFormValues,
    ): array {
        if (0 === count($oldFormValues) || $formList->isEmpty()) {
            return $newFormValues;
        }

        $unreachableInputNodeUids = $formList->getFlattenedUnReachableInputNodes()->getUids();
        foreach ($oldFormValues as $key => $value) {
            // using array_key_exists instead of isset/empty: answer value could be validly NULL, empty string or number 0
            if (in_array($key, $unreachableInputNodeUids, true) && !array_key_exists($key, $newFormValues)) {
                $newFormValues[$key] = $value;
            }
        }

        return $newFormValues;
    }

    public static function mergeNewFormValuesWithReachableUnWritableOldFormValues(
        FormList $formList,
        array $newFormValues,
        array $oldFormValues,
        StepInterface $step,
        Role $role,
    ): array {
        if (0 === count($oldFormValues) || $formList->isEmpty()) {
            return $newFormValues;
        }

        $unWritableInputNodeUids = self::getValidReachableUnWritableInputNodeUids($formList, $step, $role);
        foreach ($oldFormValues as $key => $value) {
            if (in_array($key, $unWritableInputNodeUids, true) && !array_key_exists($key, $newFormValues)) {
                $newFormValues[$key] = $value;
            }
        }

        return $newFormValues;
    }

    public static function mergeNewFormValuesWithReachableWritableOldFormValues(
        FormList $formList,
        array $newFormValues,
        array $oldFormValues,
        StepInterface $step,
        Role $role,
    ): array {
        if (0 === count($oldFormValues) || $formList->isEmpty()) {
            return $newFormValues;
        }

        foreach ($formList->getFlattenedParentalInputNodes()->getWritableForRoleInStep($role, $step)->toArray() as $question) {
            // question with role-step write access is newly submitted (auto-saved) (has new values(s)) -> ignore possible old values(s) -> skip
            if (array_key_exists($question->getUid(), $newFormValues)) {
                continue;
            }

            // question with role-step write access is NOT newly submitted (auto-saved) but also never submitted before -> skip
            if (!array_key_exists($question->getUid(), $oldFormValues)) {
                continue;
            }

            // question with role-step write access is NOT newly submitted (auto-saved) but was submitted before -> merge old value(s) in new values
            $newFormValues[$question->getUid()] = $oldFormValues[$question->getUid()];

            // if not a choice question, then question-answer is a single key-value pair in formValues
            if (!$question instanceof ChoiceInputNode) {
                continue;
            }

            // a choice question is added as question-uid => TRUE in formValues to indicate it's submitted,and each chosen choice
            // answer option is added as choice-answer-option-uid => TRUE (or null/string if 'other option') to formValues.
            // get and merge each old chosen choice answer option (could be 0 (answered blank) to multiple) in to new values
            foreach ($question->getAvailableOptions()->toArray() as $optionInputNode) {
                if (array_key_exists($optionInputNode->getUid(), $oldFormValues)) {
                    $newFormValues[$optionInputNode->getUid()] = $oldFormValues[$optionInputNode->getUid()];
                }
            }
        }

        return $newFormValues;
    }

    private static function getValidReachableUnWritableInputNodeUids(
        FormList $formList,
        StepInterface $step,
        Role $role,
    ): array {
        $nodeUids = [];
        $parentalInputNodes = $formList->getFlattenedParentalInputNodes();
        /** @var InputNode $inputNode */
        foreach ($parentalInputNodes as $inputNode) {
            // inputNode with access granted on step/role can be skipped as they are not unWritable
            if ($inputNode->isStepRoleWriteAccessGranted($step, $role)) {
                continue;
            }

            $nodeUids[] = $inputNode->getUid();
            if (!$inputNode instanceof ChoiceInputNode) {
                continue;
            }

            /** @var OptionInputNode $optionInputNode */
            foreach ($inputNode->getActiveChildren() as $optionInputNode) {
                $nodeUids[] = $optionInputNode->getUid();
            }
        }

        return $nodeUids;
    }
}
