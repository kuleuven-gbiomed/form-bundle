<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;

class LockedQuestions
{
    private readonly OptionInputNodeCollection $unlockers;
    private readonly OptionInputNodeCollection $partialUnlockers;

    public function __construct(CategoryNode $rootNode)
    {
        $optionNodes = $rootNode
            ->getFlattenedWithAllDescendants()
            ->getInputNodes()
            ->getOptionInputNodes();
        $this->unlockers = $optionNodes
            ->filter(fn (OptionInputNode $node) => 0 !== count($node->getUnlocksQuestionsIds()));
        $this->partialUnlockers = $optionNodes
            ->filter(fn (OptionInputNode $node) => 0 !== count($node->getMultiUnlocksQuestionIds()));
    }

    public function getUnlockers(): OptionInputNodeCollection
    {
        return $this->unlockers;
    }

    public function getPartialUnlockers(): OptionInputNodeCollection
    {
        return $this->partialUnlockers;
    }

    public function getAllUnlockers(): OptionInputNodeCollection
    {
        return $this->unlockers->mergedWith($this->partialUnlockers);
    }

    public function getLockableQuestionIds(): array
    {
        $idsFound = [];

        /** @var OptionInputNode $optionInputNode */
        foreach ($this->unlockers as $optionInputNode) {
            foreach ($optionInputNode->getUnlocksQuestionsIds() as $questionId) {
                $idsFound[$questionId] = 1;
            }
            foreach ($optionInputNode->getMultiUnlocksQuestionIds() as $questionId) {
                $idsFound[$questionId] = 1;
            }
        }

        return array_keys($idsFound);
    }
}
