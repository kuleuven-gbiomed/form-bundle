<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage;

class IsoDateConverter
{
    public static function convertToIsoString(string $dateString, string $formatMask): string
    {
        $date = \DateTimeImmutable::createFromFormat(
            self::convertToPhpFormatMask($formatMask).' H:i:s',
            $dateString.'00:00:00'
        );

        if (false === $date) {
            throw new \InvalidArgumentException($dateString);
        }

        return $date->format(\DateTimeInterface::ISO8601);
    }

    /**
     * @throws \Exception
     */
    public static function formatFromIsoString(string $dateString, string $formatMask): string
    {
        $date = new \DateTimeImmutable($dateString);

        return $date->format(self::convertToPhpFormatMask($formatMask));
    }

    public static function convertToPhpFormatMask(string $formatMask): string
    {
        $phpFormatMask = str_replace('dd', 'd', $formatMask);
        $phpFormatMask = str_replace('mm', 'm', $phpFormatMask);

        return str_replace('yy', 'Y', $phpFormatMask);
    }
}
