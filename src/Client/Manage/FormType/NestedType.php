<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType;

use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\Manage\FormType\Contract\TypeInterface;
use KUL\FormBundle\Client\Manage\FormType\Util\TypeTrait;
use KUL\FormBundle\Client\Manage\FormTypeBuilder;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Utility\Collection\CategoryNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Utility\Role;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @psalm-suppress MissingTemplateParam
 * I don't see any template params on Abstract Type?
 */
class NestedType extends AbstractType implements TypeInterface
{
    use TypeTrait;

    /** @var string */
    final public const KEY_VALUES = 'values';
    /** @var string */
    final public const KEY_STEP = 'step';
    /** @var string */
    final public const KEY_ROLE = 'role';
    /** @var string */
    final public const KEY_USE_ERROR_ANCHORS = 'useErrorAnchors';
    /** @var string */
    final public const KEY_TEMP_VALUES = 'tempValues';
    /** @var string */
    final public const KEY_HISTORY = 'history';
    /** @var string */
    final public const KEY_TEMP_HISTORY = 'tempHistory';
    /** @var string */
    final public const KEY_SAVED_FILES = 'savedFiles';
    /** @var string */
    final public const KEY_NODES = 'nodes';

    /**
     * notice: need to declare & pass savedFiles in as option via resolver to be able to get it in method
     * buildForm: unlike in method buildView (where it is called on param $form), I can not call an option on the root
     * because in method buildForm, the root is NOT the @see BaseType . so since calling submitted files from root each
     * time they are needed for a specific formType, the submitted files are passed down down the tree to each nested
     * node (i.e. to each nested CategoryNode), which can then use those submitted files to build UploadTypes for each
     * of the child inputNodes (questions) that operate as uploadType in that nested Category Node. or pass them further
     * down to child CategoryNodes in that CategoryNode if needed. and so on...
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'kulFormBase',
                'csrf_protection' => false,
                self::KEY_NODES => new NodeCollection(),
                self::KEY_VALUES => [],
                self::KEY_SAVED_FILES => new SavedFileCollection(),
                self::KEY_STEP => null,
                self::KEY_ROLE => null,
                self::KEY_TEMP_VALUES => [],
                self::KEY_HISTORY => new SubmitHistory(),
                self::KEY_TEMP_HISTORY => new SubmitHistory(),
            ]
        );

        $this->setAdditionalDefaults($resolver);
        $this->setAdditionalRequired($resolver);

        $resolver->setRequired([
            self::KEY_NODES,
            self::KEY_VALUES,
            self::KEY_STEP,
            self::KEY_ROLE,
            self::KEY_TEMP_VALUES,
            self::KEY_HISTORY,
            self::KEY_TEMP_HISTORY,
        ]);

        $resolver->setAllowedTypes(self::KEY_STEP, [StepInterface::class, StepWithCalculatedDateStart::class, StepWithFixedDateStart::class]);
        $resolver->setAllowedTypes(self::KEY_ROLE, Role::class);
        $resolver->setAllowedTypes(self::KEY_HISTORY, SubmitHistory::class);
        $resolver->setAllowedTypes(self::KEY_TEMP_HISTORY, SubmitHistory::class);
        $resolver->setAllowedTypes(self::KEY_NODES, [NodeCollection::class, InputNodeCollection::class, CategoryNodeCollection::class]);
    }

    /**
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var NodeCollection<array-key, NodeInterface> $nodes */
        $nodes = $builder->getOption(self::KEY_NODES);
        /** @var array $values */
        $values = $builder->getOption(self::KEY_VALUES);
        /** @var SavedFileCollection $savedFiles */
        $savedFiles = $builder->getOption(self::KEY_SAVED_FILES);
        /** @var StepInterface $step */
        $step = $builder->getOption(self::KEY_STEP);
        /** @var Role $role */
        $role = $builder->getOption(self::KEY_ROLE);
        /** @var array $tempValues */
        $tempValues = $builder->getOption(self::KEY_TEMP_VALUES);
        /** @var SubmitHistory $history */
        $history = $builder->getOption(self::KEY_HISTORY);
        /** @var SubmitHistory $tempHistory */
        $tempHistory = $builder->getOption(self::KEY_TEMP_HISTORY);

        if (!$nodes->isEmpty()) {
            /** @var NodeInterface $node */
            foreach ($nodes as $node) {
                $builder = FormTypeBuilder::addNodeFormType(
                    $builder,
                    $node,
                    $values,
                    $savedFiles, // note: can not call option on the root (baseType)
                    $step,
                    $role,
                    $tempValues,
                    $history,
                    $tempHistory
                );
            }
        }
    }

    /**
     * @throws LocalizedStringException
     * @throws MissingNodeException
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $this->setViewVars($view, $form, $options);
    }
}
