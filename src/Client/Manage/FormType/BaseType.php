<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType;

use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\Manage\FormType\Util\TypeTrait;
use KUL\FormBundle\Client\Manage\QuestionLockChecker;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Utility\Role;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PreSubmitEvent;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @psalm-suppress MissingTemplateParam
 * I don't see any template params on Abstract Type?
 */
class BaseType extends AbstractType
{
    use TypeTrait;

    /** @var string */
    final public const FORM_NAME = 'base';
    /** @var string */
    final public const USED_STEP_HIDDEN_TYPE_FIELD_NAME = 'usedStepUid';
    /** @var string */
    final public const TEMPORARY_SAVE_SUBMIT_TYPE_FIELD_NAME = 'tempSave';

    /** @var string */
    final public const KEY_VALUES = 'values';
    /** @var string */
    final public const KEY_FORM_LIST = 'formList';
    /** @var string */
    final public const KEY_DISABLE_SUBMIT = 'disableSubmit';
    /** @var string */
    final public const KEY_STEP = 'step';
    /** @var string */
    final public const KEY_ROLE = 'role';
    /** @var string */
    final public const KEY_USE_ERROR_ANCHORS = 'useErrorAnchors';
    /** @var string */
    final public const KEY_TEMP_VALUES = 'tempValues';
    /** @var string */
    final public const KEY_HISTORY = 'history';
    /** @var string */
    final public const KEY_TEMP_HISTORY = 'tempHistory';
    /** @var string */
    final public const KEY_ONLY_VALIDATE_FOR_GLOBAL_SCORE_NODE = 'onlyValidateForGlobalScoreNode';
    /** @var string */
    final public const KEY_SAVED_FILES = 'savedFiles';
    /** @var string */
    final public const KEY_LOCALE = 'locale';

    /**
     * BaseType constructor.
     */
    public function __construct(private readonly QuestionLockChecker $questionLockChecker)
    {
    }

    public function getBlockPrefix(): string
    {
        return self::FORM_NAME;
    }

    /**
     * @return string
     */
    public static function getTemporarySaveSubmitTypeFieldName()
    {
        return self::TEMPORARY_SAVE_SUBMIT_TYPE_FIELD_NAME;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'kulFormBase',
                'csrf_protection' => false,
                self::KEY_VALUES => [],
                self::KEY_SAVED_FILES => new SavedFileCollection(),
                self::KEY_FORM_LIST => null,
                self::KEY_DISABLE_SUBMIT => false,
                self::KEY_STEP => null,
                self::KEY_ROLE => null,
                self::KEY_USE_ERROR_ANCHORS => false,
                self::KEY_TEMP_VALUES => [],
                self::KEY_HISTORY => new SubmitHistory(),
                self::KEY_TEMP_HISTORY => new SubmitHistory(),
                self::KEY_ONLY_VALIDATE_FOR_GLOBAL_SCORE_NODE => null,
                self::KEY_LOCALE => null,
            ]
        );

        $resolver->setRequired([
            self::KEY_VALUES,
            self::KEY_FORM_LIST,
            self::KEY_STEP,
            self::KEY_ROLE,
            self::KEY_TEMP_VALUES,
            self::KEY_HISTORY,
            self::KEY_TEMP_HISTORY,
            self::KEY_LOCALE,
        ]);

        $resolver->setAllowedTypes(self::KEY_FORM_LIST, FormList::class);
        $resolver->setAllowedTypes(self::KEY_STEP, [StepInterface::class, StepWithCalculatedDateStart::class, StepWithFixedDateStart::class]);
        $resolver->setAllowedTypes(self::KEY_ROLE, Role::class);
        $resolver->setAllowedTypes(self::KEY_HISTORY, SubmitHistory::class);
        $resolver->setAllowedTypes(self::KEY_TEMP_HISTORY, SubmitHistory::class);
    }

    /**
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var FormList $formList */
        $formList = $builder->getOption(self::KEY_FORM_LIST);
        /** @var ScorableInputNode|null $global */
        $global = $builder->getOption(self::KEY_ONLY_VALIDATE_FOR_GLOBAL_SCORE_NODE);
        $disableSubmit = (bool) $builder->getOption(self::KEY_DISABLE_SUBMIT);

        if ($global instanceof ScorableInputNode && !$formList->hasNode($global)) {
            throw new \InvalidArgumentException('global score node with uid '.$global->getUid().' is not a node in the formList!');
        }

        /** @var StepInterface $step */
        $step = $builder->getOption(self::KEY_STEP);
        $rootCategoryNode = $formList->getRootNode();

        $builder->add(
            $rootCategoryNode->getUid(),
            NestedType::class,
            [
                NestedType::KEY_VALUES => $builder->getOption(self::KEY_VALUES),
                NestedType::KEY_SAVED_FILES => $builder->getOption(self::KEY_SAVED_FILES),
                NestedType::KEY_STEP => $step,
                NestedType::KEY_ROLE => $builder->getOption(self::KEY_ROLE),
                NestedType::KEY_TEMP_VALUES => $builder->getOption(self::KEY_TEMP_VALUES),
                NestedType::KEY_HISTORY => $builder->getOption(self::KEY_HISTORY),
                NestedType::KEY_TEMP_HISTORY => $builder->getOption(self::KEY_TEMP_HISTORY),
                NestedType::KEY_NODES => $rootCategoryNode->getChildren(),
                NestedType::KEY_LOCALE => $builder->getOption(self::KEY_LOCALE),
                NestedType::KEY_NODE => $rootCategoryNode,
            ]
        );

        // add used step uid to track in which step the form was rendered when handling POST request of form
        $builder->add(
            self::USED_STEP_HIDDEN_TYPE_FIELD_NAME,
            HiddenType::class,
            [
                'data' => $step->getUid(),
            ]
        );

        $builder->add(
            'save',
            SubmitType::class,
            [
                'label' => 'fields.save.label',
                'attr' => [
                    'class' => 'btn btn-primary',
                    'formnovalidate' => true,
                ],
                'disabled' => $disableSubmit,
            ]
        );

        $builder->add(
            self::getTemporarySaveSubmitTypeFieldName(),
            SubmitType::class,
            [
                'validation_groups' => false,
                'label' => 'fields.tempSave.label',
                'attr' => [
                    'class' => 'btn btn-default primary-temp-save-btn',
                    // This prevents HTML5 validation from blocking a form that is not fully filled out.
                    'formnovalidate' => true,
                ],
                'disabled' => $disableSubmit,
            ]
        );

        $builder->addEventListener(FormEvents::PRE_SUBMIT, $this->onPreSubmitHandleLockedQuestions(...));
    }

    public function onPreSubmitHandleLockedQuestions(FormEvent $event): void
    {
        $rootCategoryForm = $event->getForm()->get(FormList::ROOT_UID);

        // check in the form, starting from root category node, which of locked questions are in use as fields in this
        // form, are NOT unlocked by an unlocker / chain of unlockers and reset the validation & their value
        $this->resetLockedQuestionFields($rootCategoryForm);
    }

    /**
     * recursively checks in the given form (associated with a category node), which of the child form fields
     * are associated with a locked question that is NOT unlocked by their unlocker or chain of unlockers (if any)
     * and if not unlocked, attaches a listener to reset the submitted value as 'blank' and sets the validation to
     * ignore any constraints on the field so that the whole of the form can still pass as valid (if the other fields
     * pass their validation).
     *
     * @throws MissingNodeException
     */
    private function resetLockedQuestionFields(FormInterface $form): void
    {
        // Don't foreach over an array you want to change.
        $formKeys = array_keys($form->all());

        foreach ($formKeys as $formKey) {
            /** @var FormInterface $child */
            $child = $form->get($formKey);
            $childConfig = $child->getConfig();
            /** @var NodeInterface $node */
            $node = $childConfig->getOption('node');

            // if node belonging to child form is category node = collection FormType, look further down the tree
            if ($node instanceof CategoryNode) {
                $this->resetLockedQuestionFields($child);
                continue;
            }

            // skip if node belonging to child form is not even lockable
            if (!($node instanceof ParentalInputNode)) {
                continue;
            }

            // skip if node belonging to child form is lockable but not locked by any unlocker
            if (!$node->isLocked()) {
                continue;
            }

            // skip if child belonging to child form is lockable node and it IS unlocked by its unlocker or chain of
            // unlockers, meaning we don't touch the value and let do its validation constraints (if any) do the work
            if ($this->questionLockChecker->isLockableQuestionUnlocked($node)) {
                continue;
            }

            // if child is lockable node and NOT unlocked by its unlocker or chain of unlockers, reset value & validation

            $options = $childConfig->getOptions();
            // need to set auto initialize to false, else it doesn't work to alter & rebuild the child form type
            $options['auto_initialize'] = false;
            // overrule any constraints on the child by skipping validation for this field only
            $options['validation_groups'] = false;
            $parent = $child->getParent();
            if (!$parent instanceof FormInterface) {
                throw new \Exception('Form not found');
            }

            // re-create a (same named!) builder for child with the altered options including validation overruled
            $builder = $child->getConfig()->getFormFactory()->createNamedBuilder(
                $child->getName(),
                $childConfig->getType()->getInnerType()::class,
                null,
                $options
            );
            // add & use a pre_submit event listener to set the submitted value to 'blank'
            $builder->addEventListener(FormEvents::PRE_SUBMIT, function (PreSubmitEvent $event): void {
                $event->setData(null);
            });
            // add the child (with the new listener) back to the form via the builder so that the original
            // child is overwritten with reset data and validation
            $parent->add($builder->getForm());
        }
    }

    /**
     * @throws LocalizedStringException
     * @throws MissingNodeException
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $this->setViewVars($view, $form, $options);

        /** @var string $locale */
        $locale = $form->getConfig()->getOption(self::KEY_LOCALE);
        /** @var FormList $formList */
        $formList = $form->getConfig()->getOption(self::KEY_FORM_LIST);
        $rootNode = $formList->getRootNode();

        $view->vars['root_node_uid'] = $rootNode->getUid();
        $view->vars['root_node_label'] = $rootNode->hasLabel($locale) ? $rootNode->getLabel($locale) : null;
        $view->vars['error_anchors'] = (bool) $form->getConfig()->getOption(self::KEY_USE_ERROR_ANCHORS);
    }
}
