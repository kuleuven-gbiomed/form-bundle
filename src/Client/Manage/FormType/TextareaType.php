<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType;

use KUL\FormBundle\Client\Manage\FormType\Contract\TypeInterface;
use KUL\FormBundle\Client\Manage\FormType\Util\TextPrefillTrait;
use KUL\FormBundle\Client\Manage\FormType\Util\TypeTrait;
use KUL\FormBundle\Client\Manage\WysiwygSanitizer;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use Symfony\Component\Form\Extension\Core\Type\TextareaType as SymfonyTextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TextareaType extends SymfonyTextareaType implements TypeInterface
{
    use TextPrefillTrait;
    use TypeTrait;

    public function __construct(
        private readonly WysiwygSanitizer $sanitizer,
    ) {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $this->setAdditionalDefaults($resolver);
    }

    /**
     * @throws LocalizedStringException
     * @throws MissingNodeException
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $this->setViewVars($view, $form, $options);
        $view->vars['node_input_form_type'] = 'ku_form_textarea';
        parent::buildView($view, $form, $options);
    }

    public function getParent(): ?string
    {
        return SymfonyTextareaType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'kut_form_'.parent::getBlockPrefix();
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, $this->onPreSetData(...));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, $this->onPreSubmit(...));
    }

    /**
     * we sanitize the submitted value (regardless if it is temporary saved or saved) before passing it further
     * to the form constraints like NotBlank, MinLength, MaxLength, etc.
     *
     * after successful form processing, all textual data is ALWAYS sanitized (purified) of disallowed HTML (including
     * its actual text content) before it is stored in DB. this also includes removing empty tags (e.g. <p></p> will be
     * removed completely, as it is empty)
     *
     * but a submitted answer here which has HTML tags without textual content or with very little textual content, could be
     * mistakenly considered as NotBlank and/or meat MinLength, MaxLength constraints.
     *
     * e.g. if the required textarea question (regardless if it allows HTML or not (a.k.a. if textarea is configured as wysiwyg) ),
     * configured with minLength = 4, was submitted with answer '<p></p>'  it would validate as having a value and that value
     * having 7 chars, thus meeting MinLength and NotBlank constraints.
     * HTML is only allowed in wysiwyg fields. hence, in this preSubmit, we sanitize the submitted value
     * of all HTML is the question is a textarea that does not allow HTML, and sanitize it of all disallowed HTML
     * if it is a textarea configured for wysiwyg.
     * so that the actual textual value can be checked by any configured constraints (which are always executed
     * after theis pre-submit is handled), including the @see MultiLineTextLengthValidator that is triggered on
     * multiline text inputs,that validates the purified answer on the actual textual content without any HTML.
     *
     * if the question is multiline text input (textarea), we do not allow HTML -> remove all HTML
     * if the question is multiline text input (textarea) that allows HTML (wysiwyg), we allow HTML, but we have to
     * remove the disallowed HTML,
     *
     * since this will sanitize the submitted value, it also has the added benefit that if the form returns with
     * validation errors on this textarea/wysiwyg input field, the answer in the field will be shown sanitized.
     */
    public function onPreSubmit(FormEvent $event): void
    {
        /** @var string|null $data */
        $data = $event->getData();

        if (null === $data || '' === $data) {
            return;
        }

        $textNode = $event->getForm()->getConfig()->getOption(TypeInterface::KEY_NODE);
        if (!$textNode instanceof TextInputNode) {
            return;
        }

        $sanitizedData = $textNode->getInput()->useWysiwyg() ?
            $this->sanitizer->sanitizeTextValue($data) :
            $this->sanitizer->getTextValueWithoutHTML($data);

        $event->setData($sanitizedData);
    }
}
