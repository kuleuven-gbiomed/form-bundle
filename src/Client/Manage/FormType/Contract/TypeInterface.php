<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType\Contract;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

interface TypeInterface
{
    /** @var string */
    public const KEY_LOCALE = 'locale';
    /** @var string */
    public const KEY_NODE = 'node';

    public function setAdditionalDefaults(OptionsResolver $resolver): void;

    public function setViewVars(FormView $view, FormInterface $form, array $options): FormView;
}
