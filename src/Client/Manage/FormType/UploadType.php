<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType;

use KUL\FormBundle\Client\Manage\FormType\Contract\TypeInterface;
use KUL\FormBundle\Client\Manage\FormType\Util\TypeTrait;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Validator\Constraints\SubmittedFilesAmount;
use Symfony\Component\Form\Extension\Core\Type\TextType as SymfonyTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UploadType.
 *
 * formType used to submit files
 *
 * NOTE: this type is no @see FileType and does not actually uploads files, it acts as a (hidden) text input, containing
 *       a concatenated (with char '|')string of uuids from the @see SavedFileCollection as text value.
 *       the symfony @see HiddenType could be used as well, but Symfony TextType has more out-of-the box options,
 *       and the client side will handle the visibility of the rendered input manually anyway.
 */
class UploadType extends SymfonyTextType implements TypeInterface
{
    use TypeTrait;

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $this->setAdditionalDefaults($resolver);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SUBMIT, $this->onPreSubmit(...));
    }

    /**
     * @throws LocalizedStringException
     * @throws MissingNodeException
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $this->setViewVars($view, $form, $options);
        $view->vars['node_input_form_type'] = 'ku_form_upload';
    }

    /**
     * on final submit or temporary save, always check & filter the submitted string value containing the
     * concatenated uids of the submitted files that user wants to declare as temporary saved or final submitted:
     * this string value of this hidden upload field is updated on client side upon each successful AJAX upload request
     * with the uids of the @see SavedFileCollection that were successfully attached by that AJAX call to
     * the @see StoredTemplateTargetInterface. this string value is also updated upon each client side remove of an uid from
     * the string value. in both cases an auto temporary save is fired immediately. whether it is that auto temporary
     * save or a manually temporary save or final submit, the attached submitted files are known and passed in to the
     * parent type of this form type (@see BaseType ). So if the string value in this pre-submit contains uids that are
     * not linked to a savedFile and/or not linked to the @see InputNode in this uploadType, it means user
     * tampered with the value: omit invalid uids silently and rebuild value.
     *
     * NOTE: we could do this also in the Constraint that checks for min & max amount @see SubmittedFilesAmount . but the
     *       constraints are only called on 'final' submits. for (auto) temporary save, all constraints are deliberately
     *       ignored (only basic validation via FormType option 'validation_groups => false') as users are allowed
     *       to temp save blank values or whatever they want (up to some level), including uploads exceeding the max amount
     */
    public function onPreSubmit(FormEvent $event): void
    {
        /** @var string|null $data */
        $data = $event->getData();

        if (null === $data || '' === $data) {
            return;
        }

        // get the submitted file uids from the concatenated submitted string
        $savedFileUids = UploadInput::extractSavedFileUidsFromFormValue($data);
        // get the node that this uploadType is using
        /** @var UploadInputNode $uploadNode */
        $uploadNode = $event->getForm()->getConfig()->getOption(TypeInterface::KEY_NODE);
        // get all savedFiles for all upload nodes as passed in via the root baseType
        /** @var SavedFileCollection $allSavedFiles */
        $allSavedFiles = $event->getForm()->getRoot()->getConfig()->getOption(BaseType::KEY_SAVED_FILES);
        // filter out the valid submitted files for the node and for submitted file uids
        $validSubmittedNodeFiles = $allSavedFiles
            ->getForInputNode($uploadNode)
            ->getWithValidFileSize()
            ->getWhereInUids($savedFileUids);
        // re-set the data with the valid files only
        $event->setData(UploadInput::makeFormValueFromSavedFileUids($validSubmittedNodeFiles->getUids()));
    }
}
