<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType;

use KUL\FormBundle\Client\Manage\FormType\Contract\TypeInterface;
use KUL\FormBundle\Client\Manage\FormType\Util\TextPrefillTrait;
use KUL\FormBundle\Client\Manage\FormType\Util\TypeTrait;
use KUL\FormBundle\Client\Manage\WysiwygSanitizer;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use Symfony\Component\Form\Extension\Core\Type\TextType as SymfonyTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TextType extends SymfonyTextType implements TypeInterface
{
    use TextPrefillTrait;
    use TypeTrait;

    public function __construct(
        private readonly WysiwygSanitizer $sanitizer,
    ) {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $this->setAdditionalDefaults($resolver);
    }

    /**
     * @throws LocalizedStringException
     * @throws MissingNodeException
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $this->setViewVars($view, $form, $options);
        $view->vars['node_input_form_type'] = 'ku_form_text';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, $this->onPreSetData(...));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, $this->onPreSubmit(...));
    }

    /**
     * we sanitize the submitted value (regardless if it is temporary saved or saved) before passing it further
     * to the form constraints like NotBlank, MinLength, MaxLength, etc.
     *
     * after successful form processing, all textual data is ALWAYS sanitized (purified) of disallowed HTML (including
     * its actual text content) before it is stored in DB. this also includes removing empty tags (e.g. <p></p> will be
     * removed completely, as it is empty)
     *
     * but a submitted answer here which has HTML tags without textual content or with very little textual content, could be
     * mistakenly considered as NotBlank and/or meat MinLength, MaxLength constraints.
     *
     * e.g. if the required single line text question, configured with minLength = 4, was submitted with answer '<p></p>'
     * it would validate as having a value and that value having 7 chars, thus meeting MinLength and NotBlank constraints.
     * furthermore, HTML is only allowed in wysiwyg fields. hence, in this preSubmit, we sanitize the submitted value
     * of all HTML so the actual textual value can be checked by any configured constraints (which are always executed
     * after theis pre-submit is handled)
     *
     * since this will sanitize the submitted value, it also has the added benefit that if the form returns with
     * validation errors on this text input field, the answer in the field will be shown sanitized.
     */
    public function onPreSubmit(FormEvent $event): void
    {
        /** @var string|null $data */
        $data = $event->getData();

        if (null === $data || '' === $data) {
            return;
        }

        // date questions also use text input underneath the hood. so let's skip those (it has its own validation)
        // basically, we only deal with text questions here (text, plain textarea & wysiwyg-textarea)
        $textNode = $event->getForm()->getConfig()->getOption(TypeInterface::KEY_NODE);
        if (!$textNode instanceof TextInputNode) {
            return;
        }

        $event->setData($this->sanitizer->getTextValueWithoutHTML($data));
    }
}
