<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType;

use KUL\FormBundle\Client\History\InputNodeSubmitEntry;
use KUL\FormBundle\Client\Manage\FormType\Contract\TypeInterface;
use KUL\FormBundle\Client\Manage\FormType\Util\TypeTrait;
use KUL\FormBundle\Client\Manage\SameAnswerChecker;
use KUL\FormBundle\Client\Manage\WysiwygSanitizer;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionWithAdditionalTextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Utility\Role;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as SymfonyChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ChoiceType extends SymfonyChoiceType implements TypeInterface
{
    use TypeTrait;

    final public const CHOICE_TYPE_SELECT = 'select';
    final public const CHOICE_TYPE_CHECKBOXES = 'checkboxes';
    final public const CHOICE_TYPE_RADIOS = 'radios';

    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly WysiwygSanitizer $sanitizer,
        private readonly SameAnswerChecker $sameAnswerChecker,
    ) {
        parent::__construct();
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $this->setAdditionalDefaults($resolver);
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, $this->onPreSetData(...));
    }

    /**
     * @throws LocalizedStringException
     */
    public function onPreSetData(FormEvent $event): void
    {
        // if one of the options uses the "other option" configuration, we have to add a TextType
        // with the correct name (OptionInputNode uid + 'otherValue') to the form
        // Additionally we need to prefill the text field if there already a temporary or permanently saved value
        // A lot of this code is semi-copied from {@see PreSetChoiceNodeObject}
        $form = $event->getForm();
        $choices = $form->getConfig()->getOption('choices');

        $node = $form->getConfig()->getOption('node');
        if (!$node instanceof ChoiceInputNode) {
            throw new \LogicException('The corresponding node for a ChoiceOptionType should be a ChoiceInputNode');
        }

        $nodeUid = $node->getUid();
        $root = $form->getRoot();
        $rootConfig = $root->getConfig();
        $submitValues = $rootConfig->getOption('values');
        $temporaryValues = $rootConfig->getOption('tempValues');
        $submitHistory = $rootConfig->getOption('history');
        $temporaryHistory = $rootConfig->getOption('tempHistory');

        $submitHistoryEntry = $submitHistory->getNotAutoAdded()->forNodeUid($nodeUid)->latest();

        $temporaryHistoryEntry = $temporaryHistory->getNotAutoAdded()->forNodeUid($nodeUid)->latest();

        // if this question is to be prefilled and has no (temporary) answer yet,
        // check if the prefilled questions are answered and if they are the same
        $prefillLabel = '';
        $tempAnswer = array_key_exists($nodeUid, $temporaryValues) ? $temporaryValues[$nodeUid] : false;
        $submittedAnswer = array_key_exists($nodeUid, $submitValues) ? $submitValues[$nodeUid] : false;

        if ($node->hasPrefillingQuestions()
            && ('' === $tempAnswer || false === $tempAnswer || null === $tempAnswer)
            && ('' === $submittedAnswer || false === $submittedAnswer || null === $submittedAnswer)
        ) {
            $prefillingQuestionUids = $node->getPrefillingQuestionUids();
            /** @var FormList $formList */
            $formList = $rootConfig->getOption(BaseType::KEY_FORM_LIST);

            // for now only choice questions with single answers can be used for prefilling
            $prefillLabel = $this->sameAnswerChecker->getLabelOfCommonChoiceQuestionsAnswer(
                $prefillingQuestionUids,
                'en',
                $formList,
                $submitValues
            );
        }

        foreach ($choices as $choice) {
            if ($choice instanceof OptionInputNode && $choice->getLabel('en') === $prefillLabel) {
                $event->setData($choice);
            }

            if (!$choice instanceof OptionInputNode || !$choice->usesAdditionalTextInput()) {
                continue;
            }

            $value = null;
            $uid = $choice->getUid();

            if (array_key_exists($uid, $submitValues) && !array_key_exists($uid, $temporaryValues)) {
                $value = $submitValues[$uid];
            } elseif (!array_key_exists($uid, $submitValues) && array_key_exists($uid, $temporaryValues)) {
                $value = $temporaryValues[$uid];
            } elseif (array_key_exists($uid, $submitValues) && array_key_exists($uid, $temporaryValues)) {
                /** @var InputNodeSubmitEntry $submitHistoryEntry */
                /** @var InputNodeSubmitEntry $temporaryHistoryEntry */
                $value = $temporaryHistoryEntry->getSubmitDate() > $submitHistoryEntry->getSubmitDate() ? $temporaryValues[$uid] : $submitValues[$uid];
            }

            $formParent = $form->getParent();
            if (!$formParent instanceof FormInterface) {
                throw new \Exception('ChoiceType Form not found');
            }

            $formParent->add(
                $choice->getUid().'_otherValue',
                TextType::class,
                [
                    'data' => $value,
                    'constraints' => [
                        new Callback(
                            [
                                'callback' => $this->validateRequiredAndLength(...),
                                // setting the nodeUid as private member is wrong, and will mess up the validation
                                // so pass it as payload to the validation callback
                                'payload' => $nodeUid,
                            ]
                        ),
                    ],
                    'attr' => [
                        'placeholder' => 'Please specify',
                    ],
                ]
            );
        }
    }

    public function validateRequiredAndLength(?string $object, ExecutionContextInterface $context, string $payload): void
    {
        $form = $context->getObject();
        if (!$form instanceof Form) {
            throw new \Exception('Form not found');
        }
        $formName = explode('_', $form->getName());
        $optionUid = reset($formName);
        $nodeUid = $payload;
        /** @var string $locale */
        $locale = $form->getRoot()->getConfig()->getOption(self::KEY_LOCALE);

        $formParent = $form->getParent();
        if (!$formParent instanceof FormInterface) {
            throw new \Exception('Form not found');
        }

        $data = $formParent->getData()[$nodeUid];
        $chosenOption = null;
        // if checkboxes, data will be an array of input nodes, else input node
        if (is_array($data)) {
            foreach ($data as $inputNode) {
                /** @var OptionInputNode $inputNode */
                // it is technically possible to make multiple options that 'usesAdditionalTextInput' -> check uid
                if ($inputNode->getUid() === $optionUid) {
                    $chosenOption = $inputNode;
                    break;
                }
            }
        } elseif ($data instanceof OptionInputNode && $data->getUid() === $optionUid) {
            $chosenOption = $data;
        }

        if (!($chosenOption instanceof OptionInputNode)) {
            return;
        }

        /** @var OptionWithAdditionalTextInput $input */
        $input = $chosenOption->getInput();
        // get the constraints from the input and check the inputted value against them
        $required = $input->isAdditionalTextRequired();
        $minLength = $input->getAdditionalTextMinLength();
        $maxLength = $input->getAdditionalTextMaxLength();
        $value = $context->getValue();
        $choiceForm = $formParent->get($nodeUid);
        $chosenOptionLabel = $chosenOption->getLabel($locale);

        // add the custom validation error messages (if any) not to the field itself but to the associated
        // choice question for UX consistency. we don't want to show & confuse user with both messages: 'field can
        // not be blank' + 'field must be between min & max chars' at the same time, and we need to verify length
        // of any non-empty string at all times, regardless of required: hence length is only validated when
        // a non-empty string is given while required is validated when an empty string or no string is given

        $value = $this->sanitizer->sanitizeTextValue($value);

        if ($required && '' === trim($value)) {
            // FormErrors expect a fully translated message (unlike the auto-translated violations added to context)
            $choiceForm->addError(
                new FormError(
                    $this->translator->trans('otherValue.required', [
                        '%other-option-label%' => $chosenOptionLabel,
                    ], 'validators')
                )
            );
        }

        if ('' === trim($value) || (mb_strlen($value) >= $minLength && mb_strlen($value) <= $maxLength)) {
            return;
        }

        $choiceForm->addError(
            new FormError(
                $this->translator->trans('otherValue.length', [
                    '%other-option-label%' => $chosenOptionLabel,
                    '%min%' => $minLength,
                    '%max%' => $maxLength,
                ], 'validators')
            )
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws MissingNodeException
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $this->setViewVars($view, $form, $options);
        $view->vars['node_input_form_type'] = 'ku_form_choice';

        $node = $form->getConfig()->getOption('node');
        if (!$node instanceof ChoiceInputNode) {
            throw new \LogicException('The corresponding node for a ChoiceOptionType should be a ChoiceInputNode');
        }

        $root = $form->getRoot();
        $rootConfig = $root->getConfig();
        $submitHistory = $rootConfig->getOption('history');

        if ($node->hasPrefillingQuestions()) {
            $preFillingQuestions = $node->getPrefillingQuestionUids();
            $prefillSources = [];

            foreach ($preFillingQuestions as $preFillingQuestion) {
                $submitEntry = $submitHistory->getNotAutoAdded()->forNodeUid($preFillingQuestion)->latest();
                if (!$submitEntry instanceof InputNodeSubmitEntry) {
                    continue;
                }
                /** @var ChoiceInputNode $prefillingNode */
                $prefillingNode = $submitEntry->getNode();
                $prefillingNodeLabel = $prefillingNode->getInfo();
                $value = $submitEntry->getValue();
                if (is_array($value)) {
                    $answers = $prefillingNode->getAvailableOptionsByUids(array_keys($value));
                } else {
                    $answers = $prefillingNode->getAvailableOptionsByUids([$value]);
                }

                $prefillSources[] = [
                    'nodeInfo' => $prefillingNodeLabel,
                    'answers' => $answers,
                    'person' => $submitEntry->getPerson(),
                    'role' => $submitEntry->getRole(),
                ];
            }

            $view->vars['prefillSources'] = $prefillSources;
        }

        /** @var ChoiceInputNode $node */
        $node = $form->getConfig()->getOption('node');
        /** @var Role $role */
        $role = $form->getRoot()->getConfig()->getOption('role');
        $view->vars['show_option_score_values'] = $node->allowsRoleToSeeOptionScoreValues($role);
        $view->vars['show_extrema_score_values'] = false;
        $view->vars['scoring_parameters'] = null;
        if ($node->allowsRoleToSeeExtremaScoreValues($role)) {
            $view->vars['show_extrema_score_values'] = true;
            $view->vars['scoring_parameters'] = $node->getInputScoringParameters();
        }

        parent::buildView($view, $form, $options);
    }

    /**
     * http://symfony.com/doc/2.7/reference/forms/types/choice.html#select-tag-checkboxes-or-radio-buttons.
     */
    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        $multiple = $view->vars['multiple'] ?? false;
        $expanded = $view->vars['expanded'] ?? false;
        $choiceType = self::CHOICE_TYPE_SELECT;
        if (true === $expanded) {
            $choiceType = true === $multiple ? self::CHOICE_TYPE_CHECKBOXES : self::CHOICE_TYPE_RADIOS;
        }

        $view->vars['choice_type'] = $choiceType;
        parent::finishView($view, $form, $options);
    }
}
