<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType\Node;

use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Question\CheckBoxInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\DateInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;

class PreSetInputNodeObjectBuilder
{
    /**
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public static function createFromNodeAndValues(
        InputNode $inputNode,
        array $formValues,
        array $temporaryFormValues,
        SubmitHistory $submitHistory,
        SubmitHistory $temporaryHistory,
        SavedFileCollection $savedFiles,
    ): AbstractPreSetInputNodeObject {
        if ($inputNode instanceof TextInputNode) {
            return new PreSetTextNodeObject(
                $inputNode,
                $formValues,
                $temporaryFormValues,
                $submitHistory,
                $temporaryHistory
            );
        }

        if ($inputNode instanceof NumberInputNode) {
            return new PreSetNumberNodeObject(
                $inputNode,
                $formValues,
                $temporaryFormValues,
                $submitHistory,
                $temporaryHistory
            );
        }

        if ($inputNode instanceof CheckBoxInputNode) {
            return new PreSetCheckboxNodeObject(
                $inputNode,
                $formValues,
                $temporaryFormValues,
                $submitHistory,
                $temporaryHistory
            );
        }

        if ($inputNode instanceof ChoiceInputNode) {
            return new PreSetChoiceNodeObject(
                $inputNode,
                $formValues,
                $temporaryFormValues,
                $submitHistory,
                $temporaryHistory
            );
        }

        if ($inputNode instanceof UploadInputNode) {
            return new PreSetUploadNodeObject(
                $inputNode,
                $formValues,
                $temporaryFormValues,
                $submitHistory,
                $temporaryHistory,
                $savedFiles
            );
        }

        if ($inputNode instanceof DateInputNode) {
            return new PreSetDateNodeObject(
                $inputNode,
                $formValues,
                $temporaryFormValues,
                $submitHistory,
                $temporaryHistory
            );
        }

        throw new \InvalidArgumentException('can not build preSetObject for input of class '.$inputNode::class);
    }
}
