<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType\Node;

use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\Manage\FormType\ChoiceType as CustomChoiceType;
use KUL\FormBundle\Domain\Template\Element\Node\Input\ChoiceInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionWithAdditionalTextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;

class PreSetChoiceNodeObject extends AbstractPreSetInputNodeObject
{
    private readonly OptionInputNodeCollection $availableOptionNodes;

    /**
     * @throws \Exception
     */
    public function __construct(
        private readonly ChoiceInputNode $node,
        array $submitValues,
        array $temporaryValues,
        SubmitHistory $submitHistory,
        SubmitHistory $temporaryHistory,
    ) {
        $this->submitValues = $submitValues;
        $this->temporaryValues = $temporaryValues;
        $this->submitHistory = $submitHistory;
        $this->temporaryHistory = $temporaryHistory;

        parent::__construct($submitValues, $temporaryValues, $submitHistory, $temporaryHistory);

        // child option nodes for the choice node could have been deactivated, hence use active ones only
        // based on older code that assumes an array, not a collection => hence the cast to array
        $this->availableOptionNodes = $this->node->getAvailableOptions();
    }

    public function getFormTypeAlias(): string
    {
        return CustomChoiceType::class;
    }

    public function getInput(): ChoiceInput
    {
        return $this->getNode()->getInput();
    }

    public function getNode(): ChoiceInputNode
    {
        return $this->node;
    }

    /**
     * in admin-side, a choice question should not be allowed to be build without at least 2 available options,
     * this is even asserted upon choice construct in @see ChoiceInputNode. but it is important that a choice
     * question without options is never rendered. so just in case of changes, we add this check to use later
     * to see if we can render the choice.
     */
    public function hasAvailableOptions(): bool
    {
        return !$this->getNode()->getAvailableOptions()->isEmpty();
    }

    /**
     * TODO following comments are copy-pasted from existing code. although still correct, it does not mention all,
     *      like the use of @see OptionWithAdditionalTextInput ! in case of those, this method will need updating
     *      if @see ChoiceInputNode::usesOptionsWithAdditionalTextInput() then this pre set data stuff must be revised:
     *      right now, this just add the options to an array to later use to pre-set data on choice question, but
     *      if an option(s) allows for a user to have added additional text to an option, this will no longer work...
     *
     * if a choice input node is submitted, its node uid is found in the (submitted or temporary saved) formValues
     * with value TRUE to indicate it has been submitted. the values that are submitted for this choice node are
     * nodes themselves (with @see OptionInput as input). the values (option nodes) that are submitted / temporary saved
     * are each found in the (submitted / temporary saved) formValues, keyed by their uid and with value TRUE.
     * if a choice node is not required or if not (yet) submitted & temporary saved, it is possible no values (option
     * nodes) were submitted / temporary saved. in that case the default value is returned.
     * if choice node allowed for multiple values (options nodes) to be submitted, then one or more (or none yet)
     * values (option nodes) could have been submitted / temporary saved.
     *
     * IMPORTANT: configuration of inputNodes can be updated/altered by admins at any time, resulting in a new version
     * of that node. in case of a choice node, the multiple configuration could have been updated/altered from multiple
     * to non-multiple or vice versa (also, optionNodes could have been added / deleted / deactivated). to accommodate
     * these changes in the already submitted / temporary saved values (option nodes) - if any - and as such flawlessly
     * make sure users do not find the whole form blocked and/or have to re-submit (partially a domain decision), a more
     * comprehensive processing of the submitted / temporary saved values (option nodes) is done. this processing is
     * based on some domain decisions to determine what to do in case of finding multiple submitted values while choice
     * node does no longer allow multiple, or the other way around. values submitted while choice was multiple are NEVER
     * deleted so if e.g. the multiple config was multiple, then changed to non-multiple and then a second time back
     * to multiple, the values can always be rebuild. in case of non-multiple but was previously multiple and multiple
     * values are found, the FIRST found value is returned (domain decision)!
     *
     * the correct default pre set data in case no values were submitted or temporary saved or choice is
     * not required (corresponding to empty data in Symfony's @see SymfonyChoiceType ) is:
     * - in case of multiple and no values found (yet):  return empty array or all default selected child option nodes if set
     * - in case of non-multiple and no values found (yet):  return null or (first) default selected child option node if set
     *
     * return type depends on choice being configured as multiple or not and if a submitted selection or default
     * selection is found. corresponds to what Symfony's ChoiceType (pre set) data expects.
     * could be array of InputNodes (not a collection!), an InputNode (if non-multiple) or null (if none found)
     *
     * @return OptionInputNode[]|OptionInputNode|null either array of OptionInputNodes or one OptionInputNode or null - according to choice
     *                                                question 'multiple' configuration - to accommodate Symfony's ChoiceType's 'multiple' option expectancies
     */
    public function getPreSetDataValue(): array|OptionInputNode|null
    {
        $values = $this->getPreSetDataFormValues();
        /** @var OptionInputNode[] $data */
        $data = []; // always start building an array. handling correct return type if non-multiple is done at the end

        /** @var OptionInputNode $childOptionNode */
        foreach ($this->availableOptionNodes as $childOptionNode) {
            // add child option node if choice node was submitted or temporary saved and child is part of the submitted/saved values.
            // using array_key_exists here and not isset or empty:  value could be validly null, false or empty string!
            if ($this->hasPreSetSubmittedOrTemporarySavedAnswer() && array_key_exists($childOptionNode->getUid(), $values)) {
                $data[] = $childOptionNode;
                continue;
            }

            // add child option node if choice node was never submitted or temporary saved but child is configured as default value.
            if (!$this->hasPreSetSubmittedOrTemporarySavedAnswer() && $childOptionNode->getInput()->isDefaultSelected()) {
                $data[] = $childOptionNode;
            }
        }

        // return all child option nodes if choice node is configured as multiple, or changed to it from non-multiple.
        // in which case the Symfony ChoiceType expects an array for its 'data' option
        if ($this->getInput()->isMultiple()) {
            return $data;
        }

        // return either single found value if choice is configured as non-multiple and only one value found. or first
        // value if choice node is configured as non-multiple but it was multiple before and multiple values were
        // submitted while it was multiple. Symfony ChoiceType expects a single object or null value.
        if (count($data) > 0) {
            return current($data);
        }

        // return default null value if choice is configured as non-multiple and no value found.
        return null;
    }
}
