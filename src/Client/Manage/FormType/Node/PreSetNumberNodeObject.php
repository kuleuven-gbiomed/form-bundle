<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType\Node;

use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\Manage\FormType\NumberType as CustomNumberType;
use KUL\FormBundle\Domain\Template\Element\Node\Input\NumberInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class PreSetNumberNodeObject extends AbstractPreSetInputNodeObject
{
    /**
     * @throws \Exception
     */
    public function __construct(
        private readonly NumberInputNode $node,
        array $submitValues,
        array $temporaryValues,
        SubmitHistory $submitHistory,
        SubmitHistory $temporaryHistory,
    ) {
        $this->submitValues = $submitValues;
        $this->temporaryValues = $temporaryValues;
        $this->submitHistory = $submitHistory;
        $this->temporaryHistory = $temporaryHistory;

        parent::__construct($submitValues, $temporaryValues, $submitHistory, $temporaryHistory);
    }

    public function getFormTypeAlias(): string
    {
        return CustomNumberType::class;
    }

    public function getInput(): NumberInput
    {
        return $this->getNode()->getInput();
    }

    public function getNode(): NumberInputNode
    {
        return $this->node;
    }

    /**
     * submitted or temporary saved number input value always is a numeric value or - if input is non-required or if not
     * (yet) submitted & temporary saved , is default NULL (corresponding empty data in Symfony's @see NumberType ).
     */
    public function getPreSetDataValue(): float|int|null
    {
        if (!$this->hasPreSetSubmittedOrTemporarySavedAnswer()) {
            return null;
        }

        $values = $this->getPreSetDataFormValues();

        /** @var float|int|null $value null if question is optional and blank submitted or temporary saved as blank */
        $value = $values[$this->getNode()->getUid()];

        return $value;
    }
}
