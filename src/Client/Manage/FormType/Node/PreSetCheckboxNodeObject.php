<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType\Node;

use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\Manage\FormType\CheckboxType as CustomCheckboxType;
use KUL\FormBundle\Domain\Template\Element\Node\Input\CheckboxInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\CheckBoxInputNode;

class PreSetCheckboxNodeObject extends AbstractPreSetInputNodeObject
{
    /**
     * @throws \Exception
     */
    public function __construct(
        private readonly CheckBoxInputNode $node,
        array $submitValues,
        array $temporaryValues,
        SubmitHistory $submitHistory,
        SubmitHistory $temporaryHistory,
    ) {
        $this->submitValues = $submitValues;
        $this->temporaryValues = $temporaryValues;
        $this->submitHistory = $submitHistory;
        $this->temporaryHistory = $temporaryHistory;

        parent::__construct($submitValues, $temporaryValues, $submitHistory, $temporaryHistory);
    }

    public function getFormTypeAlias(): string
    {
        return CustomCheckboxType::class;
    }

    public function getInput(): CheckboxInput
    {
        return $this->getNode()->getInput();
    }

    public function getNode(): CheckBoxInputNode
    {
        return $this->node;
    }

    /**
     * submitted or temporary saved checkbox value always is a boolean
     * if not (yet) submitted or temporary saved, check if input is checked by default.
     */
    public function getPreSetDataValue(): bool
    {
        if (!$this->hasPreSetSubmittedOrTemporarySavedAnswer()) {
            return $this->getInput()->isDefaultChecked();
        }

        $values = $this->getPreSetDataFormValues();

        /** @var bool $value */
        $value = $values[$this->getNode()->getUid()];

        return $value;
    }
}
