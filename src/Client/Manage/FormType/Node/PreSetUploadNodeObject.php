<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType\Node;

use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\Manage\FormType\UploadType as CustomUploadType;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Entity\SavedFile;

class PreSetUploadNodeObject extends AbstractPreSetInputNodeObject
{
    private readonly SavedFileCollection $savedNodeFiles;

    /**
     * @throws \Exception
     */
    public function __construct(
        private readonly UploadInputNode $node,
        array $submitValues,
        array $temporaryValues,
        SubmitHistory $submitHistory,
        SubmitHistory $temporaryHistory,
        SavedFileCollection $savedFiles,
    ) {
        $this->submitValues = $submitValues;
        $this->temporaryValues = $temporaryValues;
        $this->submitHistory = $submitHistory;
        $this->temporaryHistory = $temporaryHistory;

        parent::__construct($submitValues, $temporaryValues, $submitHistory, $temporaryHistory);

        // filter the submitted files on the given node
        $this->savedNodeFiles = $savedFiles->getForInputNode($node);
    }

    public function getFormTypeAlias(): string
    {
        return CustomUploadType::class;
    }

    public function getInput(): UploadInput
    {
        return $this->getNode()->getInput();
    }

    public function getNode(): UploadInputNode
    {
        return $this->node;
    }

    /**
     * the value of an upload field is a concatenated string of all submitted @see SavedFile::$uid . files are
     * uploaded separately via ajax and attached to the submitted form. ajax then returns the uid which is added
     * (concatenated) in the (hidden) @see CustomUploadType field on client side. user then submit / temporary save the form
     * to indicate he/she wishes these uploaded files to be considered as his/her submitted / temporary saved files.
     *
     * to avoid having invalid file uids in the (submitted / temporary saved & concatenated string ) values (for whatever
     * reason like deliberate manual DB tampering), the file uid values are hold against given submittedFiles to check
     * if a submittedFile exists for that uuid (and for that node). if not, they are silently omitted from the submitted
     * values. i.e. the submitted / temporary values are sanitized & rebuild to assert that only existing submitted files
     * are used. makes it easier to get & work with right files in the @see CustomUploadType . does not throw an exception:
     * that would block all further form processing, while this object is not used for the actual form-submit-validation.
     *
     * if the upload is not required or not (yet) submitted or temporary saved, the value can be the default = NULL.
     */
    public function getPreSetDataValue(): ?string
    {
        if (!$this->hasPreSetSubmittedOrTemporarySavedAnswer()) {
            return null;
        }

        $values = $this->getPreSetDataFormValues();
        // get submitted / temporary saved value: either a concatenated string of file uids or NULL
        /** @var string|null $savedFileUids null if question is optional and blank (i.e. no files) submitted or temporary saved as blank */
        $savedFileUids = $values[$this->getNode()->getUid()];

        // check & filter the uids from submitted value on existence in given submittedNodeFiles
        $filteredSavedFiles = $this->savedNodeFiles
            ->getWithValidFileSize()
            ->getWhereInUids(UploadInput::extractSavedFileUidsFromFormValue($savedFileUids));

        if ($filteredSavedFiles->isEmpty()) {
            return null;
        }

        // convert valid uids to the concatenated string the custom formType (and client side code) expect
        return UploadInput::makeFormValueFromSavedFileUids($filteredSavedFiles->getUids());
    }
}
