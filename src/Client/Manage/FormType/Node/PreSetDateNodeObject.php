<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType\Node;

use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\Manage\FormType\TextType;
use KUL\FormBundle\Client\Manage\IsoDateConverter;
use KUL\FormBundle\Domain\Template\Element\Node\Input\DateInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\DateInputNode;

class PreSetDateNodeObject extends AbstractPreSetInputNodeObject
{
    /**
     * @throws \Exception
     */
    public function __construct(
        private readonly DateInputNode $node,
        array $submitValues,
        array $temporaryValues,
        SubmitHistory $submitHistory,
        SubmitHistory $temporaryHistory,
    ) {
        $this->submitValues = $submitValues;
        $this->temporaryValues = $temporaryValues;
        $this->submitHistory = $submitHistory;
        $this->temporaryHistory = $temporaryHistory;

        parent::__construct($submitValues, $temporaryValues, $submitHistory, $temporaryHistory);
    }

    public function getFormTypeAlias(): string
    {
        return TextType::class;
    }

    public function getInput(): DateInput
    {
        return $this->getNode()->getInput();
    }

    public function getNode(): DateInputNode
    {
        return $this->node;
    }

    /**
     * submitted or temporary saved number input value always is a string value or - if input is non-required or if not
     * (yet) submitted & temporary saved , is default NULL (corresponding empty data in Symfony's @see TextType ).
     */
    public function getPreSetDataValue(): ?string
    {
        if (!$this->hasPreSetSubmittedOrTemporarySavedAnswer()) {
            return null;
        }
        $values = $this->getPreSetDataFormValues();
        /** @var string|null $value null if question is optional and blank submitted or temporary saved as blank */
        $value = $values[$this->getNode()->getUid()];
        $result = null === $value || '' === $value ? $value : IsoDateConverter::formatFromIsoString($value, $this->getNode()->getInput()->getFormatMask());

        return $result;
    }
}
