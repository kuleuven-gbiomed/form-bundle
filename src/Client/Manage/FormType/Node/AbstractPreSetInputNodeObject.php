<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType\Node;

use KUL\FormBundle\Client\History\InputNodeSubmitEntry;
use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;

abstract class AbstractPreSetInputNodeObject
{
    protected bool $useTemporaryValues;
    protected bool $hasSubmitValue;
    protected bool $hasTemporaryValue;
    protected ?InputNodeSubmitEntry $submitHistoryEntry = null;
    protected ?InputNodeSubmitEntry $temporaryHistoryEntry = null;

    abstract public function getPreSetDataValue(): mixed;

    abstract public function getFormTypeAlias(): string;

    abstract public function getNode(): InputNode;

    abstract public function getInput(): InputInterface;

    /**
     * @throws \Exception
     */
    public function __construct(
        protected array $submitValues,
        protected array $temporaryValues,
        protected SubmitHistory $submitHistory,
        protected SubmitHistory $temporaryHistory,
    ) {
        // check if a value is submitted. no usage of empty/isset as values can be null or empty
        // if the nodeUid exists in values, it means it is submitted, regardless of (any) (multiple) values)
        // (in case of node with ChoiceInput, the choice nodeUid will exist if submitted,
        // regardless of (any) (multiple) child optionNode values)
        $this->hasSubmitValue = array_key_exists($this->getNode()->getUid(), $this->submitValues);
        $this->hasTemporaryValue = array_key_exists($this->getNode()->getUid(), $this->temporaryValues);

        $nodeUid = $this->getNode()->getUid();

        $this->submitHistoryEntry = $this->submitHistory->getNotAutoAdded()->forNodeUid($nodeUid)->latest();

        $this->temporaryHistoryEntry = $this->temporaryHistory->getNotAutoAdded()->forNodeUid($nodeUid)->latest();

        $this->useTemporaryValues = $this->useTemporaryValuesExecute();
    }

    /**
     * is a (latest) submit history entry found.
     */
    protected function hasLatestSubmitHistoryEntry(): bool
    {
        return $this->submitHistoryEntry instanceof InputNodeSubmitEntry;
    }

    /**
     * is a (latest) temporary history entry found.
     */
    protected function hasLatestTemporaryHistoryEntry(): bool
    {
        return $this->temporaryHistoryEntry instanceof InputNodeSubmitEntry;
    }

    /**
     * get the (latest) submit history entry
     * or throws exception if none found.
     *
     * @throws \Exception
     */
    protected function getLatestSubmitHistoryEntry(): InputNodeSubmitEntry
    {
        if (!$this->submitHistoryEntry instanceof InputNodeSubmitEntry) {
            throw new \Exception(': no submit history entry found for node with uid '.$this->getNode()->getUid());
        }

        return $this->submitHistoryEntry;
    }

    /**
     * get the (latest) temporary history entry
     * or throws exception if none found.
     *
     * @throws \Exception
     */
    protected function getLatestTemporaryHistoryEntry(): InputNodeSubmitEntry
    {
        if (!$this->temporaryHistoryEntry instanceof InputNodeSubmitEntry) {
            throw new \Exception(': no temporary history entry found for node with uid '.$this->getNode()->getUid());
        }

        return $this->temporaryHistoryEntry;
    }

    /**
     * has a submit value submitted.
     */
    protected function hasSubmitValue(): bool
    {
        return $this->hasSubmitValue;
    }

    /**
     * has a temporary value submitted.
     */
    protected function hasTemporaryValue(): bool
    {
        return $this->hasTemporaryValue;
    }

    public function useTemporaryValues(): bool
    {
        return $this->useTemporaryValues;
    }

    /**
     * use temporary values for the formType pre-set-data-value if the value to be used for preSet
     * if temporary save is allowed and:
     * a temporary value exists, based on temporary history having a latest entry for given node
     * and the temporary value is NOT equal to the submit value (if any).
     * and the temporary value s more recent than the submit value (if any).
     *
     * @throws \Exception
     */
    public function useTemporaryValuesExecute(): bool
    {
        // if no temporary history entry (and thus no temporary value)
        if (!$this->hasLatestTemporaryHistoryEntry()) {
            return false;
        }

        // if temporary history entry but no submit history entry (and thus no submit value)
        if (!$this->hasLatestSubmitHistoryEntry()) {
            return true;
        }

        // if submit & temporary history entry (and thus temporary value & submit value)

        // # if submit & temporary values (as in submit & temporary history entry) are equal
        if ($this->getLatestSubmitHistoryEntry()->equalsNodeValue($this->getLatestTemporaryHistoryEntry())) {
            // no point in showing confusing temp message if values are equal
            // even if the temporary value is most recent
            return false;
        }

        // # if submit & temporary values (as in submit & temporary history entry) are NOT equal
        // # show temporary value if most recent
        $submitDate = $this->getLatestSubmitHistoryEntry()->getSubmitDate();
        $temporaryDate = $this->getLatestTemporaryHistoryEntry()->getSubmitDate();

        return $temporaryDate > $submitDate;
    }

    /**
     * TODO extend somewhat (e.g. textInput: if NO submitValue found but tempValue IS found and IS empty (null or string),
     *                            should the message be shown then (since there was never anything filled in) ?
     * show a temporary history entry for the formType pre-set-data-value.
     */
    public function usePreSetDataTemporaryHistoryEntry(): bool
    {
        return $this->useTemporaryValues();
    }

    /**
     * get the latest temporary history entry to be used to build a warning message for pre-set-data-value
     * throws an exception if showing such a message is not allowed or irrelevant.
     *
     * @throws \Exception
     */
    public function getPreSetDataTemporaryHistoryEntry(): InputNodeSubmitEntry
    {
        if (!$this->usePreSetDataTemporaryHistoryEntry()) {
            throw new \Exception(': node with uid '.$this->getNode()->getUid().' does not allow or does not has a temporary history entry to be shown on preSetData.');
        }

        return $this->getLatestTemporaryHistoryEntry();
    }

    /**
     * checks if a submitted or temporary answer was already given (saved) for the question (InputNode) in this object
     * instance, with priority given to temporary answer if both were given and temporary answer is most recently given.
     */
    public function hasPreSetSubmittedOrTemporarySavedAnswer(): bool
    {
        return $this->useTemporaryValues() ? $this->hasTemporaryValue() : $this->hasSubmitValue();
    }

    /**
     * returns the full set of already given submitted answers or already given temporary answers for all questions,
     * including for the question (InputNode) in this object instance, with priority given to full set of temporary
     * answers if for the question (InputNode) in this object instance both a submitted answer and temporary answer
     * was already given (saved) and the temporary one is most recently given.
     */
    public function getPreSetDataFormValues(): array
    {
        return $this->useTemporaryValues() ? $this->temporaryValues : $this->submitValues;
    }
}
