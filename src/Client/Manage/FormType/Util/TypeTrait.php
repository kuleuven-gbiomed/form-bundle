<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType\Util;

use KUL\FormBundle\Client\Manage\FormType\BaseType;
use KUL\FormBundle\Client\Manage\FormType\Contract\TypeInterface;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\ChoiceInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\CheckBoxInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\DateInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Domain\Utility\Role;
use Symfony\Component\Form\FormConfigInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait TypeTrait
{
    public function setAdditionalDefaults(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            TypeInterface::KEY_NODE => null,
            TypeInterface::KEY_LOCALE => null,
            'temporaryHistoryEntry' => null,
            'validation_groups' => function (FormInterface $form) {
                if ($this->requiresValidation($form)) {
                    return ['Default']; // validating = keep default validation for validation_groups
                }

                return false; // disable validation = explicitly return false for validation_groups
            },
            'help_html' => true, // node descriptions often contain HTML: don't escape (description is no user input but admin sided config).
        ]);
    }

    public function setAdditionalRequired(OptionsResolver $resolver): void
    {
        $resolver->setRequired([
            TypeInterface::KEY_NODE,
            TypeInterface::KEY_LOCALE,
        ]);

        $resolver->setAllowedTypes(TypeInterface::KEY_NODE, [
            NodeInterface::class,
            CategoryNode::class,
            InputNode::class,
            ParentalInputNode::class,
            TextInputNode::class,
            CheckBoxInputNode::class,
            UploadInputNode::class,
            NumberInputNode::class,
            ChoiceInputNode::class,
            DateInputNode::class,
        ]);
    }

    /**
     * determine if the (sub)form needs validation.
     *
     * if option 'onlyValidateForGlobalScoreNode' in the root form contains an inputNode which is a
     * globalScore node, it means the whole form is to be used to (re)calculate that global score node's value.
     * that means all validation on all fields must be removed, except for the score node fields that make
     * up the global score node's value (i.e. the dependencies of the global score node). of course, also
     * the validation on the global score node itself must be removed as this is the value that will be
     * calculated outside this form. this logic is used to allow the usage of this form(s) to validate the
     * the submitted values of the score nodes without having users to submit all values
     *
     * note:
     * it is possible that the global Score node has dependencies that are global score node themselves.
     * in that case, to do the calculation of the first 'main' global score node, those 'nested' secondary global
     * score nodes will be validated as well. meaning that users will have to first request a calculation of those
     * secondary global score or manually supply a value for those, before having a chance that the form will
     * consider all dependencies on first global score node as valid. this is intentional, users may only calculate
     * one global score node at the time. although it would not be that difficult to additionally & recursively assert
     * validation on the dependencies of those secondary global score nodes, it would - however - require
     * a complex set of logic inside this (root) form's (with event handlers) to auto calculate those secondary global
     * score nodes, then use those values as data for the respected fields and submit those (unless the value is
     * manually added), while all the time going up and down in the tree structure of the root form to find the nodes
     * & values. ergo, it's way too complex, too much can get out of control and it's anyway not the task of this form.
     *
     * @return bool
     */
    private function requiresValidation(FormInterface $form)
    {
        // get config of the root form
        $rootConfig = $form->getRoot()->getConfig();
        // check if validation has to be limited for a global score
        $globalScoreNode = $rootConfig->getOption(BaseType::KEY_ONLY_VALIDATE_FOR_GLOBAL_SCORE_NODE);

        // 1) if no global score input node is defined,
        // all fields must be validated (root form has asserted correct type/properties of node)
        if (!$globalScoreNode instanceof InputNode) {
            return true;
        }

        // 2) if a globalScoreNode is given, then we need to validate all it's score dependency questions
        // and skip validation on any other inputNodes
        // this method will be run recursively top-down through tree structure, skipping validation on inputNodes
        // that are not score dependencies of this global score node. and skipping validation on the global score node
        // itself because, well... it's the node that we want to get validated values for from its dependencies, so
        // if it's blank or invalid or ... is to be ignored. the categories are not skipped: because they are wrappers
        // and this method will be called recursively on each of the children of the category to find the global
        // score's dependencies

        // each (sub)form handles one node (input or category): get that node from the options of this (sub)form
        /** @var NodeInterface $node */
        $node = $form->getConfig()->getOption('node');

        // no skipping validation on category nodes (they are wrappers and their child forms/inputs should be handled
        // individually and will override the category validation anyway). this method will be called recursively on
        // all category's descendants
        if ($node instanceof CategoryNode) {
            return true;
        }

        // should be inputNode if not categoryNode, so anything else is either completely new code or some huge error
        if (!$node instanceof InputNode) {
            throw new \InvalidArgumentException('node must be an instance of '.InputNode::class);
        }

        // don't validate if it's the global node itself (we are trying to get validated values for its dependencies)
        if ($node->getUid() === $globalScoreNode->getUid()) {
            return false;
        }

        // don't validate if the node is an inputNode that can not even operate as a scorable node (dependency)
        if (!$node instanceof ScorableInputNode) {
            return false;
        }

        if ($globalScoreNode instanceof OperableForScoring) {
            // only validate if the input node is a scorable node that is a valid dependency of this global score node
            return $node->operatesAsScoreNodeForGlobalScoreNode($globalScoreNode);
        }

        throw new \InvalidArgumentException("David didn't expect the global score node to be an input node that's not OperableForScoring");
    }

    /**
     * @throws LocalizedStringException
     * @throws MissingNodeException
     */
    public function setViewVars(FormView $view, FormInterface $form, array $options): FormView
    {
        $name = $form->getName();

        $view->vars['name'] = $name;
        $view->vars['full_name'] = $name;
        $view->vars['id'] = $name;  // Symfony uses a ltrim on underscore + digits on id for HTML4 reasons, we use HTML5
        $view->vars['nested_name'] = $name;

        if ($view->parent) {
            $parentNestedName = $view->parent->vars['nested_name'];
            if (is_string($parentNestedName) && '' !== $parentNestedName) {
                $view->vars['nested_name'] = sprintf('%s[%s]', $parentNestedName, $name);
            }

            $this->setNodeVars(
                $form->getRoot()->getConfig(),
                $view,
                $options[TypeInterface::KEY_NODE],
                $options[TypeInterface::KEY_LOCALE],
                $options
            );
        }

        return $view;
    }

    /**
     * TODO DV use a decorator.
     *
     * @throws LocalizedStringException
     * @throws MissingNodeException
     */
    protected function setNodeVars(
        FormConfigInterface $rootConfig,
        FormView $view,
        NodeInterface $node,
        string $locale,
        array $options,
    ): FormView {
        /** @var Role|null $role */
        $role = $rootConfig->getOption(BaseType::KEY_ROLE);

        $view->vars['help'] = $node->hasFullDescription($locale, $role) ? $node->getFullDescription($locale, $role) : null;
        $view->vars['node_description'] = $node->hasFullDescription($locale, $role) ? $node->getFullDescription($locale, $role) : null;
        $view->vars['node_depth'] = $node->getDepth();
        $view->vars['parent_id'] = $node->hasParent() ? $node->getParent()->getUid() : null;

        if ($node instanceof CategoryNode) {
            $view->vars['node_type'] = 'category';
            $view->vars['label'] = $node->hasLabel($locale) ? $node->getLabel($locale) : null;

            return $view;
        }

        if ($node instanceof ParentalInputNode) {
            $view->vars['is_locked'] = $node->isLocked();
            $view->vars['multi_unlocking_question_ids'] = $node->getMultiUnlockingQuestions();
        }

        // should be inputNode if not categoryNode, so anything else is either completely new code or some huge error
        if (!$node instanceof InputNode) {
            throw new \InvalidArgumentException('node must be an instance of '.InputNode::class);
        }

        $view->vars['node_type'] = 'input';
        $view->vars['label'] = $node->getLabel($locale);
        $view->vars['node_description_display_type'] = $node->getDescriptionDisplayMode();
        $view->vars['node_input_type'] = $node->getInput()::getType();
        $view->vars['node_temp_history_entry'] = $options['temporaryHistoryEntry'];

        $view->vars['matrix_view'] = false;
        if ($node->hasAncestorWithDisplayModeMatrix()) {
            $view->vars['matrix_view'] = true;
        }

        $view->vars['position'] = $node->getPositionString();

        $view->vars['is_global_score_node'] = false;
        // only mark as global score node if it is valid to operate as global score node
        // e.g. a choice node could have several globalScoreDependencies defined. but if not all of its own
        // options have a score value to work with, the the choice does not meet scoringRequirements and thus does
        // not operates as globals score node. or it could meet scoringRequirements (have a score value for each of its
        // options) but non of its globalScoreDependencies meets scoring requirements for their own node...
        // so to avoid user  clicking buttons to get an ajax that throws an error or informs that the action is not valid
        // we only render valid global score nodes.
        if ($node instanceof ScorableInputNode && $node->operatesAsGlobalScoreNode()) {
            $view->vars['is_global_score_node'] = true;
            $view->vars['show_auto_calculate_btn'] = $node->getInputScoringParameters()->showAutoCalculateBtn();
            $view->vars['allow_non_auto_calculated_value'] = $node->getInputScoringParameters()->allowNonAutoCalculatedValue();
        }

        if ($node instanceof NumberInputNode) {
            $view->vars['number_scale'] = $node->getInput()->getScale();
            $view->vars['number_rounding_mode'] = $node->getInput()->getRoundingMode();
        }

        if ($node instanceof TextInputNode) {
            $view->vars['use_wysiwyg'] = $node->getInput()->useWysiwyg();
            $view->vars['has_predefined_answers'] = $node->getInput()->hasPredefinedAnswers();
            $view->vars['predefined_answers'] = $node->getInput()->getPredefinedAnswers();
            $view->vars['prefill_with'] = implode(' ', $node->getPrefillingQuestionUids());

            return $view;
        }

        if ($node instanceof ChoiceInputNode) {
            $view->vars['sort_options_alphabetically'] = $node->getInput()->sortOptionsAlphabetically();
            $view->vars['use_single_row_view'] = ChoiceInput::DISPLAY_MODE_SINGLE_TABLE_ROW === $node->getInput()->getDisplayMode();
            $view->vars['unlocks_questions'] = $node->getUnlocksQuestionIdsPerAnswer();

            return $view;
        }

        // pass submitted files to node's rendering view for building the upload's file managing dataTable. allows to assert
        // that only correct files will be shown and allows to make this bundle independent of the upload url needed for
        // managing the node's files (due to values being temp saved OR submitted , it's too damn risky & difficult to
        // rely on ajax to fetch files upon form rendering with just the node and the value's/data's uids (via POST))
        if ($node instanceof UploadInputNode) {
            /** @var SavedFileCollection $savedFiles */
            $savedFiles = $rootConfig->getOption(BaseType::KEY_SAVED_FILES);
            /** @var string|null $uidsString */
            $uidsString = $view->vars['data'];
            $view->vars['node_submitted_files'] = $savedFiles
                ->getForInputNode($node)
                ->getWhereInUids(UploadInput::extractSavedFileUidsFromFormValue($uidsString));
        }

        if ($node instanceof DateInputNode) {
            $view->vars['format_mask'] = $node->getInput()->getFormatMask();

            return $view;
        }

        return $view;
    }
}
