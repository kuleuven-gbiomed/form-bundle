<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType\Util;

use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use Symfony\Component\Form\FormEvent;

trait TextPrefillTrait
{
    public function onPreSetData(FormEvent $event): void
    {
        $form = $event->getForm();

        $node = $form->getConfig()->getOption('node');

        if (!($node instanceof TextInputNode)) {
            return;
        }

        if ($node->hasPrefillingQuestions()) {
            $nodeUid = $node->getUid();
            $root = $form->getRoot();
            $rootConfig = $root->getConfig();
            $submitValues = $rootConfig->getOption('values');
            $submittedAnswer = \array_key_exists($nodeUid, $submitValues);

            if (false === $submittedAnswer) {
                $prefillingQuestionUids = $node->getPrefillingQuestionUids();
                $prefillingAnswers = [];

                foreach ($prefillingQuestionUids as $prefillingQuestionUid) {
                    if (\array_key_exists($prefillingQuestionUid, $submitValues)) {
                        $prefillingAnswers[] = $submitValues[$prefillingQuestionUid];
                    }
                }

                $separator = $node->getInput()->isMultiLine() ? "\n\n" : ', ';

                $event->setData(implode($separator, $prefillingAnswers));
            }
        }
    }
}
