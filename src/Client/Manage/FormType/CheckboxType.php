<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage\FormType;

use KUL\FormBundle\Client\Manage\FormType\Contract\TypeInterface;
use KUL\FormBundle\Client\Manage\FormType\Util\TypeTrait;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType as SymfonyCheckboxType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CheckboxType extends SymfonyCheckboxType implements TypeInterface
{
    use TypeTrait;

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $this->setAdditionalDefaults($resolver);
    }

    /**
     * @throws LocalizedStringException
     * @throws MissingNodeException
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $this->setViewVars($view, $form, $options);
        $view->vars['node_input_form_type'] = 'ku_form_checkbox';
        parent::buildView($view, $form, $options);
    }
}
