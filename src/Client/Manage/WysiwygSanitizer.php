<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage;

/**
 * Class WysiwygSanitizer.
 *
 * FIXME: Maybe we can use the HTMLPurifierBundle which has built in Symfony integration?
 * https://github.com/Exercise/HTMLPurifierBundle
 */
class WysiwygSanitizer
{
    private readonly string $cacheSerializerPath;

    public function __construct(private readonly string $wysiwygAllowedTags, private readonly string $allowedFrameTargets, string $cacheSerializerPath)
    {
        if (!file_exists($cacheSerializerPath)) {
            mkdir($cacheSerializerPath, 0o770, true);
        }
        $this->cacheSerializerPath = $cacheSerializerPath;
    }

    public function sanitizeTextValue(?string $value): string
    {
        if (!is_string($value) || '' === $value) {
            return '';
        }

        $config = \HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', 'utf-8');
        $config->set('HTML.Doctype', 'HTML 4.01 Transitional');
        $config->set('AutoFormat.RemoveEmpty', true);
        $config->set('AutoFormat.Linkify', true);
        $config->set('HTML.Allowed', $this->wysiwygAllowedTags);
        $config->set('Attr.AllowedFrameTargets', $this->allowedFrameTargets);
        $config->set('Cache.SerializerPath', $this->cacheSerializerPath);
        $purifier = new \HTMLPurifier($config);
        $purifiedValue = $purifier->purify($value);

        return $purifiedValue;
    }

    public function getTextValueWithoutHTML(?string $value): string
    {
        if (!is_string($value) || '' === $value) {
            return '';
        }

        $config = \HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', 'utf-8');
        $config->set('HTML.Doctype', 'HTML 4.01 Transitional');
        $config->set('AutoFormat.RemoveEmpty', true);
        $config->set('HTML.Allowed', '');
        $config->set('Cache.SerializerPath', $this->cacheSerializerPath);
        $purifier = new \HTMLPurifier($config);
        $purifiedValue = $purifier->purify($value);

        return $purifiedValue;
    }
}
