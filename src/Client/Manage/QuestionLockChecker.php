<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage;

use KUL\FormBundle\Client\Manage\FormType\BaseType;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class QuestionLockChecker
{
    private ?LockedQuestions $lockedQuestions = null;

    private ?array $selectedAnswerIds = null;

    private ?array $unlockedLockableQuestionIds = null;

    private ?CategoryNode $rootNode = null;

    public function __construct(private readonly RequestStack $request)
    {
    }

    /**
     * checks if at least one of the unlockers for given locked question is selected & submitted (as a choice answer)
     * as well. if that selected & submitted unlocker' itself belongs to a choice question that on itself is lockable
     * by another unlocker, then that other unlocker must be selected & submitted as well, and so on, creating a chain
     * of unlockers that need to be selected & submitted to unlock given locked question.
     * if the the locked question complies to the above, it is considered unlocked and it's submitted value is to be
     * left untouched and any constraint validation on it is to be enforced. in the other case, it is considered
     * locked and it's value should be changed to 'blank', its validation is to be skipped and its constraints ignored.
     *
     * if more than one non-chain unlocker and/or chain of unlockers lock the same question, then it is sufficient that
     * one of those unlockers or chains of unlockers is submitted & selected as answer. see examples below
     * example 1:
     * question A is locked by unlocker B (i.e. B is an answer option on B's parent choice question) and also locked
     * by unlocker C. question A will be considered unlocked if one of the following conditions is met:
     * - only unlocker B is selected as answer
     * - only unlocker C is selected as answer
     * - both unlockers B & C are selected as answer
     * question A will be not considered unlocked if one of the following conditions is met:
     * - neither B & C are selected as answers
     *
     * example 2:
     * question A is locked by unlocker B and also locked by unlocker C who in turn is locked by unlocker D.
     * question A will be considered unlocked if one of the following conditions is met:
     * - only unlocker B is selected as answer
     * - only unlocker C AND unlocker D (i.e. the top-down unlocker chain D to C) is selected as answer
     * - both unlocker B & the unlocker chain C AND D are selected as answers
     * question A will not be considered unlocked if one of the following conditions is met:
     * - neither B & C are selected.
     * - B is NOT selected, C IS selected but D is NOT selected (i.e. full chain D tot C is not selected)
     * - B is NOT selected, D IS selected but C is NOT selected (i.e. full chain D tot C is not selected)
     *
     * @throws MissingNodeException
     *
     * @see BaseType::onPreSubmitHandleLockedQuestions() )
     */
    public function isLockableQuestionUnlocked(ParentalInputNode $lockableQuestion): bool
    {
        $this->setRootNode($lockableQuestion->getTreeRootNode());

        return in_array(
            $lockableQuestion->getUid(),
            $this->getUnlockedLockableQuestionIds(),
            true
        ) || ($lockableQuestion->isMultiLockable() && $this->isQuestionMultiUnlocked($lockableQuestion));
    }

    public function isQuestionMultiUnlocked(ParentalInputNode $node): bool
    {
        $unlockConditions = $node->getMultiUnlockingQuestions();

        $allUnlockingOptionIds = [];

        array_walk_recursive($unlockConditions, function (string $optionId) use (&$allUnlockingOptionIds): void {
            $allUnlockingOptionIds[] = $optionId;
        });

        $selectedAnswerIds = $this->getSelectedAnswerIds($this->getLockedQuestions()->getPartialUnlockers());

        $unlockingChoiceIds = array_keys($unlockConditions);
        $answeredUnlockingChoiceIds = [];

        foreach ($selectedAnswerIds as $answerId) {
            foreach ($unlockConditions as $choiceId => $optionIds) {
                if (in_array($answerId, $optionIds, true)) {
                    $answeredUnlockingChoiceIds[] = $choiceId;
                }
            }
        }

        return count($unlockingChoiceIds) === count($answeredUnlockingChoiceIds);
    }

    public function isAnsweredQuestionUnlocked(ParentalInputNode $node, array $submittedValues): bool
    {
        $this->setRootNode($node->getTreeRootNode());

        $this->setSelectedAnswerIds($submittedValues);

        return in_array(
            $node->getUid(),
            $this->getUnlockedLockableQuestionIds(),
            true
        ) || ($node->isMultiLockable() && $this->isQuestionMultiUnlocked($node));
    }

    private function setSelectedAnswerIds(array $submittedValues): void
    {
        $result = [];

        foreach ($submittedValues as $key => $value) {
            if (null === $value) {
                continue;
            }
            $result[] = $key;
        }

        $this->selectedAnswerIds = $result;
    }

    private function getSelectedAnswerIds(OptionInputNodeCollection $unlockers): array
    {
        if (is_array($this->selectedAnswerIds)) {
            return $this->selectedAnswerIds;
        }

        $selectedAnswerIds = [];

        // request holds array of raw POST data with all submitted questions @see FormHandler::handleRequest
        // with the keys being uid of question & corresponding value being its raw submitted answer
        $flattenedFormPostData = $this->getRequestData();

        foreach ($unlockers as $unlocker) {
            $questionId = $unlocker->getParent()->getUid();
            // if choice question of unlocker is not in form data, then choice question nor unlocker is submitted
            if (!array_key_exists($questionId, $flattenedFormPostData)) {
                continue;
            }

            $submittedValue = $flattenedFormPostData[$questionId];

            // depending on choice allowing multiple answers or not, its raw submitted value will be string or array of
            // strings, containing option uid(s) or, if no answer selected, an empty string, empty array or null
            if ('' === $submittedValue || null === $submittedValue || [] === $submittedValue) {
                continue;
            }

            if (is_string($submittedValue)) {
                $selectedAnswerIds[] = $submittedValue;
            }

            if (is_array($submittedValue)) {
                $selectedAnswerIds = array_merge($selectedAnswerIds, $submittedValue);
            }
        }

        return $this->selectedAnswerIds = array_unique($selectedAnswerIds);
    }

    private function getLockedQuestions(): LockedQuestions
    {
        if (null === $this->lockedQuestions) {
            if (!$this->rootNode instanceof CategoryNode) {
                throw new \LogicException("Can't have locked questions without root node.");
            }
            $this->lockedQuestions = new LockedQuestions($this->rootNode);
        }

        return $this->lockedQuestions;
    }

    private function getRequestData(): array
    {
        $request = $this->request->getCurrentRequest();

        // avoid false negatives when checking for submitted answers in request POST data -> don't return empty array!
        if (!($request instanceof Request)) {
            throw new \RuntimeException('no request found!');
        }

        return $request->request->all();
    }

    /**
     * @return string[]
     */
    private function getUnlockedLockableQuestionIds(): array
    {
        if (is_array($this->unlockedLockableQuestionIds)) {
            return $this->unlockedLockableQuestionIds;
        }

        $lockedQuestions = $this->getLockedQuestions();
        $selectedAnswerIds = $this->getSelectedAnswerIds($lockedQuestions->getAllUnlockers());

        $invisibleLockableQuestionIds = $lockedQuestions->getLockableQuestionIds();
        $visibleLockableQuestionsIds = [];

        do {
            $ready = true;
            foreach ($selectedAnswerIds as $selectedAnswerId) {
                if (!$lockedQuestions->getUnlockers()->hasOneByNodeUid($selectedAnswerId)) {
                    continue;
                }
                /** @var OptionInputNode $optionInputNode */
                $optionInputNode = $lockedQuestions->getUnlockers()->getOneByNodeUid($selectedAnswerId);
                $unlockingQuestionId = $optionInputNode->getParent()->getUid();
                if (in_array($unlockingQuestionId, $invisibleLockableQuestionIds, true)) {
                    // Ignore unlockers of invisible questions.
                    continue;
                }

                foreach ($optionInputNode->getUnlocksQuestionsIds() as $unlockedQuestionId) {
                    // GBMEQA-954
                    // if the $key is 0 the if evaluates to false
                    // so we have to do a strict true/false evaluation around the assignment
                    if (!(false === ($key = array_search($unlockedQuestionId, $invisibleLockableQuestionIds, true)))) {
                        unset($invisibleLockableQuestionIds[$key]);
                        $visibleLockableQuestionsIds[] = $unlockedQuestionId;
                        $ready = false;
                    }
                }
            }
        } while (false === $ready);

        $this->unlockedLockableQuestionIds = $visibleLockableQuestionsIds;

        return $visibleLockableQuestionsIds;
    }

    private function setRootNode(CategoryNode $rootNode): void
    {
        if ($this->rootNode === $rootNode) {
            return;
        }

        $this->rootNode = $rootNode;
        // Invalidate cache, since these things are calculated based on root node.
        $this->lockedQuestions = null;
        $this->selectedAnswerIds = null;
        $this->unlockedLockableQuestionIds = null;
    }
}
