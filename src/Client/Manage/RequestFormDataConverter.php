<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage;

use KUL\FormBundle\Client\Manage\FormType\BaseType;
use KUL\FormBundle\Client\Util\ArbitraryNestedArray;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class RequestFormDataConverter
{
    /**
     * maps & replaces the submitted flat array of key-value pairs in the POST data (with keys = uids of questions @see InputNode )
     * for given form back to the nested structure in that form (as set by form's option @see FormList ).
     * for the mapping it uses the uids of the ancestors in the tree structure of each inputNode and appends the
     * inputNodeUid, using array_merge and the ordering as set by @see InputNode::getAncestors() to keep correct order!
     *
     * IMPORTANT: - this is used for @see BaseType inside this Bundle!
     *
     * *****************************************************************************************************************
     * ********************************  EXAMPLE  **********************************************************************
     * *****************************************************************************************************************
     * NOTE: 'form' is always the name of the root category (hardCoded in @see FormList::ROOT_UID )
     * NOTE: "tempSave" is the name of the used submit button: will always be either 'tempSave' or 'save'
     * NOTE: this example uses the most simple questions (text questions) for demonstration purposes. other question types
     *       have different values but the keys are always the question uid and the nesting conversion is done for all.
     * NOTE: we're handling the request's POST data her, thus the data in 'request->request->all()'.
     *
     * assume: 'question_two' is nested in a sub category 'category_one'
     *         'question_one' is nested immediately under the root category 'form'
     *
     * * START: value of incoming 'request->request->all()'
     * [
     *   "uid_of_question_one" => "some answer"
     *   "uid_of_question_two" => "some other answer"
     *   "base" => [
     *      "usedStepUid" => "uid_of_step_in_which_user_loaded_the_form"
     *      "tempSave" => ""
     *    ]
     * ]
     *
     * * END: value of converted 'request->request->all()': result after conversion to nested data in the nested structure
     *        of the BaseType. the original form data is left as-is (in a way it's 'orphaned' as no longer needed for
     *        further successful form handling (storing answers, etc.), but still needed for quickly go through single
     *        dimension array list of submitted question data and for reloading form page with validation errors.
     *[
     *   "uid_of_question_one" => "some answer"
     *   "uid_of_question_two" => "some other answer"
     *   "base" => [
     *      "usedStepUid" => "uid_of_step_in_which_user_loaded_the_form"
     *      "tempSave" => ""
     *      "form" => [
     *         "uid_of_question_one" => "some answer",
     *         "category_one" => [
     *            "uid_of_question_two" => "some other answer"
     *         ]
     *      ]
     *    ]
     * ]
     *
     * @throws \Exception
     */
    public static function convertPostRequestBaseFormDataToNestedFormData(FormInterface $form, Request $request): Request
    {
        if (!$form->getConfig()->getType()->getInnerType() instanceof BaseType) {
            throw new \InvalidArgumentException('can not convert formData in request for form: form must be instance of '.BaseType::class.'. Got'.$form->getConfig()->getType()->getInnerType()::class);
        }

        if ('POST' === $request->getMethod()) {
            /** @var FormList $formList */
            $formList = $form->getConfig()->getOption('formList');
            $nestedUidValues = new ArbitraryNestedArray();

            foreach ($request->request->all() as $key => $value) {
                if (!$formList->hasNodeByUid($key)) {
                    // if node with key not in formlist, first check if we are dealing with an 'otherValue'-field
                    if (0 === substr_compare($key, ChoiceInputNode::OTHER_VALUE_FIELD_SUFFIX, -mb_strlen(ChoiceInputNode::OTHER_VALUE_FIELD_SUFFIX))) {
                        // get the uid from the string that was appended with '_otherValue'
                        $explodedString = explode('_', $key);
                        // get the optionNode
                        if ($formList->hasNodeByUid(reset($explodedString))) {
                            $node = $formList->getNodeByUid(reset($explodedString));
                            // order is important -> using array_merge with node uid appended!
                            $ancestorUids = $node->getAncestors()->getUids();
                            array_pop($ancestorUids);
                            // do the same as with another node (see below), but the other-value field resides in the parent
                            // of the choice type, so exclude the key of the choiceNode
                            $nestedKeys = array_merge($ancestorUids, [$key]);
                            $nestedUidValues->set($nestedKeys, $value);
                        }
                    }
                    continue;
                }

                $node = $formList->getNodeByUid($key);

                // order is important -> using array_merge with node uid appended!
                $nestedKeys = array_merge($node->getAncestors()->getUids(), [$node->getUid()]);
                $nestedUidValues->set($nestedKeys, $value);
            }

            // request object is part of symfony/http-foundation dependency. at time of writing, we allow versions ^4.4 or ^5.4
            // for this dependency. only from version 5 onwards does $request->request->all($key) allows for an optional
            // parameter $key to retrieve an array of values that belongs to that key. in version 4, this optional parameter
            // does not exist. hence we cannot use $submittedFormData = $request->request->all($formName);. we solve this by
            // duplicating the logic in symfony's http-foundation version 5 request->all(string $key = null); method. including returning
            // an empty array if the key doesn't exist and an exception if the key exist but the value returned is not an array
            $formName = $form->getName();
            $submittedFormData = $request->request->all()[$formName] ?? [];
            if (!is_array($submittedFormData)) {
                throw new \InvalidArgumentException(sprintf('Unexpected value for key "%s": in formData: expecting array, got "%s".', $formName, get_debug_type($submittedFormData)));
            }

            $submittedFormData = array_merge($submittedFormData, $nestedUidValues->get([]));

            $request->request->set($formName, $submittedFormData);
        }

        return $request;
    }
}
