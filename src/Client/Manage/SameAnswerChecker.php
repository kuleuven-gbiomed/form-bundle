<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Manage;

use KUL\FormBundle\Domain\Template\Element\FormListInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;

// When addressing feedback of phpa, I accidentally introduced a bug in this
// code. So I decided to extract it into a kind of service, to make it testable.

final class SameAnswerChecker
{
    /**
     * Given a set of choice questions, return the label of the common answer, if such a common answer exists.
     *
     * If no common answer exists, the empty string is returned.
     *
     * @param string[] $choiceQuestionIds
     * @param mixed[]  $submittedAnswers
     */
    public function getLabelOfCommonChoiceQuestionsAnswer(
        array $choiceQuestionIds,
        string $languageCode,
        FormListInterface $formList,
        array $submittedAnswers,
    ): string {
        $result = '';
        /** @var OptionInputNode[] $allSelectedOptions */
        $allSelectedOptions = [];
        $commonAnswerExists = true;

        foreach ($choiceQuestionIds as $choiceQuestionId) {
            /** @var ChoiceInputNode $choiceInputNode */
            $choiceInputNode = $formList->getNodeByUid($choiceQuestionId);
            $selectedOptions = $this->getSelectedOptions($choiceInputNode, $submittedAnswers);
            // for now only choice questions with single answers can be used for prefilling
            if (1 === count($selectedOptions)) {
                $allSelectedOptions = array_merge($allSelectedOptions, $selectedOptions);
                continue;
            }
            $commonAnswerExists = false;
            break;
        }

        if ($commonAnswerExists) {
            $questionCount = count($choiceQuestionIds);
            if (1 === $questionCount) {
                $result = $result = $allSelectedOptions[0]->getLabel($languageCode);
            } else {
                for ($i = 0; $i < $questionCount - 1; ++$i) {
                    if ($allSelectedOptions[$i]->isSameOptionAs($allSelectedOptions[$i + 1])) {
                        $result = $allSelectedOptions[0]->getLabel($languageCode);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param bool[] $submitValues
     *
     * @return OptionInputNode[]
     */
    private function getSelectedOptions(ChoiceInputNode $choiceInputNode, array $submitValues): array
    {
        $result = [];
        foreach ($choiceInputNode->getAvailableOptions() as $availableOption) {
            /** @var OptionInputNode $availableOption */
            if (array_key_exists($availableOption->getUid(), $submitValues)) {
                $result[] = $availableOption;
            }
        }

        return $result;
    }
}
