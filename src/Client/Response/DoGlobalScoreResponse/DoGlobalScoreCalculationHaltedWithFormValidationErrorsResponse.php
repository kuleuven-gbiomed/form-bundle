<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoGlobalScoreResponse;

use KUL\FormBundle\Client\Util\ValidationError\ValidationErrors;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;

class DoGlobalScoreCalculationHaltedWithFormValidationErrorsResponse extends AbstractDoGlobalScoreResponse implements \JsonSerializable
{
    final public const KEY_CALCULATION_VALIDATION_ERRORS = 'validation_errors';

    /**
     * DoGlobalScoreCalculationHaltedWithFormValidationErrorsResponse constructor.
     */
    public function __construct(private readonly ScorableInputNode $globalScoreNode, private readonly ValidationErrors $validationErrors)
    {
    }

    public function getValidationErrors(): ValidationErrors
    {
        return $this->validationErrors;
    }

    public function getGlobalScoreNode(): ScorableInputNode
    {
        return $this->globalScoreNode;
    }

    /**
     * @return array{calculated: false, validation_errors: ValidationErrors}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_CALCULATION_SUCCESS => false,
            self::KEY_CALCULATION_VALIDATION_ERRORS => $this->getValidationErrors(),
        ];
    }
}
