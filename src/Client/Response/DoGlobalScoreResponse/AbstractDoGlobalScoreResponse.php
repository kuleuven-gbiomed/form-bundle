<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoGlobalScoreResponse;

use KUL\FormBundle\Client\Response\AbstractDoResponse;

abstract class AbstractDoGlobalScoreResponse extends AbstractDoResponse
{
    public const KEY_CALCULATION_SUCCESS = 'calculated';
}
