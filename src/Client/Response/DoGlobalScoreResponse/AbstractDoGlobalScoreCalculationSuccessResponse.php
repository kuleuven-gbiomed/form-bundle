<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoGlobalScoreResponse;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\TemplateTarget\Calculation\AbstractGlobalScoreCalculator;

abstract class AbstractDoGlobalScoreCalculationSuccessResponse extends AbstractDoGlobalScoreResponse implements \JsonSerializable
{
    protected AbstractGlobalScoreCalculator $calculator;

    /**
     * @throws LocalizedStringException
     */
    public function __construct(AbstractGlobalScoreCalculator $calculator)
    {
        $this->validateCalculator($calculator);
        $this->calculator = $calculator;
    }

    /**
     * @throws LocalizedStringException
     */
    private function validateCalculator(AbstractGlobalScoreCalculator $calculator): void
    {
        if (!$calculator->canCalculateValue()) {
            $node = $calculator->getGlobalScoreNode();
            $locale = $node->getInfo()->getLocalizedLabel()->getFallbackLocale();
            throw new \InvalidArgumentException('can not build calculation success response: global score node ['.$node->getUid().' | '.$node->getNestedLabel($locale).'] is not calculable at this point');
        }
    }
}
