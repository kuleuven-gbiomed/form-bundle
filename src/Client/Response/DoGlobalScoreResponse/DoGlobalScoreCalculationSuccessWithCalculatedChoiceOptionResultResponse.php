<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoGlobalScoreResponse;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\TemplateTarget\Calculation\GlobalChoiceScoreValueCalculator;

class DoGlobalScoreCalculationSuccessWithCalculatedChoiceOptionResultResponse extends AbstractDoGlobalScoreCalculationSuccessResponse
{
    final public const KEY_CALCULATION_CALCULATED_OPTION_UID = 'calculated_option_uid';

    /**
     * DoGlobalScoreCalculationSuccessWithCalculatedChoiceOptionResultResponse constructor.
     *
     * @throws LocalizedStringException
     */
    public function __construct(GlobalChoiceScoreValueCalculator $calculator)
    {
        parent::__construct($calculator);
    }

    private function getCalculator(): GlobalChoiceScoreValueCalculator
    {
        // Notify phpstan that we're sure about the type by using an annotation.
        /** @var GlobalChoiceScoreValueCalculator $globalChoiceScoreValueCalculator * */
        $globalChoiceScoreValueCalculator = $this->calculator;

        return $globalChoiceScoreValueCalculator;
    }

    public function getCalculatedOptionNodeResult(): OptionInputNode
    {
        return $this->getCalculator()->getCalculatedOptionNode();
    }

    /**
     * @return array{calculated: true, calculated_option_uid: string}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_CALCULATION_SUCCESS => true,
            self::KEY_CALCULATION_CALCULATED_OPTION_UID => $this->getCalculatedOptionNodeResult()->getUid(),
        ];
    }
}
