<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoGlobalScoreResponse;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;

class DoGlobalScoreCalculationImpossibleWithInsufficientDataResponse extends AbstractDoGlobalScoreResponse implements \JsonSerializable
{
    final public const KEY_CALCULATION_INSUFFICIENT_DATA = 'insufficient_data';

    /**
     * DoGlobalScoreCalculationImpossibleWithInsufficientDataResponse constructor.
     */
    public function __construct(private readonly ScorableInputNode $globalScoreNode, private readonly string $message)
    {
    }

    public function getGlobalScoreNode(): ScorableInputNode
    {
        return $this->globalScoreNode;
    }

    /**
     * @throws LocalizedStringException
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return array{calculated: false, insufficient_data: string}
     *
     * @throws LocalizedStringException
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_CALCULATION_SUCCESS => false,
            self::KEY_CALCULATION_INSUFFICIENT_DATA => $this->getMessage(),
        ];
    }
}
