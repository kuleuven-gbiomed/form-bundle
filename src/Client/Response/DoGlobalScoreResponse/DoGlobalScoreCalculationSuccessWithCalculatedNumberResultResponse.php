<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoGlobalScoreResponse;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\TemplateTarget\Calculation\GlobalNumberScoreValueCalculator;
use KUL\FormBundle\Utility\LocalizedNumberFormatter;

class DoGlobalScoreCalculationSuccessWithCalculatedNumberResultResponse extends AbstractDoGlobalScoreCalculationSuccessResponse
{
    /** @var string */
    final public const KEY_CALCULATION_CALCULATED_NUMERIC_VALUE = 'calculated_numeric_value';
    /** @var string */
    final public const KEY_LOCALE = 'locale';
    /** @var string */
    final public const DEFAULT_LOCALE = 'en';

    /**
     * DoGlobalScoreCalculationSuccessWithCalculatedNumberResultResponse constructor.
     *
     * @throws LocalizedStringException
     */
    public function __construct(GlobalNumberScoreValueCalculator $calculator, private readonly string $locale = self::DEFAULT_LOCALE)
    {
        parent::__construct($calculator);
    }

    private function getCalculator(): GlobalNumberScoreValueCalculator
    {
        // Notify phpstan that we're sure about the type by using an annotation.
        /** @var GlobalNumberScoreValueCalculator $globalNumberScoreValueCalculator * */
        $globalNumberScoreValueCalculator = $this->calculator;

        return $globalNumberScoreValueCalculator;
    }

    public function getCalculatedNumericResult(): float
    {
        return $this->getCalculator()->getCalculatedRoundedNumericValue();
    }

    public function jsonSerialize(): array
    {
        $scoringParameters = $this->getCalculator()->getGlobalScoreNode()->getInputScoringParameters();

        return [
            self::KEY_CALCULATION_SUCCESS => true,
            // calculated number will have been scaled & rounded => we can return as exactly scaled & rounded string
            self::KEY_CALCULATION_CALCULATED_NUMERIC_VALUE => LocalizedNumberFormatter::format(
                $this->getCalculatedNumericResult(),
                $this->locale,
                null,
                $scoringParameters->getScale(),
                $scoringParameters->getRoundingMode()
            ),
            self::KEY_LOCALE => $this->locale,
        ];
    }
}
