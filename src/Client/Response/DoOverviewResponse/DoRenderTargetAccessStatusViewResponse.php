<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoOverviewResponse;

use KUL\FormBundle\Client\Overview\TargetAccessStatusView;

class DoRenderTargetAccessStatusViewResponse extends AbstractDoOverviewResponse
{
    public function __construct(private readonly TargetAccessStatusView $accessStatusView)
    {
    }

    public function getTargetAccessStatusView(): TargetAccessStatusView
    {
        return $this->accessStatusView;
    }
}
