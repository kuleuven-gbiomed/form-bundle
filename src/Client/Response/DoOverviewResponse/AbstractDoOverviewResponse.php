<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoOverviewResponse;

use KUL\FormBundle\Client\Response\AbstractDoResponse;

abstract class AbstractDoOverviewResponse extends AbstractDoResponse
{
}
