<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response;

abstract class AbstractDoResponse
{
    //    /** @var string */
    //    Const SHOULD_REDIRECT_TO_OVERVIEW = 'redirect_to_overview';

    //    /** @var string */
    //    Const SHOULD_RELOAD_AFTER_SUCCESS = 'reload_after_success'; // e.g. after success form submit, reload (redirect) to view page
    //    /** @var string */
    //    Const SHOULD_REDIRECT_TO_OVERVIEW_AFTER_SUCCESS = 'redirect_to_overview_after_success';
    //
    //    /** @var string */
    //    Const SHOULD_RELOAD_AFTER_FAIL = 'reload_after_fail'; // e.g. validation errors on form submit should reload view with those errors
    //    /** @var string */
    //    Const SHOULD_REDIRECT_AFTER_FAIL = 'redirect_after_fail';

    //    /** @var string */
    //    Const SHOULD_ADVISE_USER_TO_REFRESH_PAGE = 'advise_user_to_refresh_page';
    //    /** @var string */
    //    Const SHOULD_THROW_ACCESS_DENIED_EXCEPTION = 'throw_access_denied';
    //    /** @var string */
    //    Const SHOULD_SHOW_ACCESS_DENIED_MESSAGE = 'show_access_denied_message';

    //    # MESSAGES
    //    /** @var string */
    //    Const SHOULD_SHOW_FAIL_MESSAGE = 'show_fail_message';
    //    /** @var string */
    //    Const SHOULD_SHOW_SUCCESS_MESSAGE = 'show_success_message';

    //    /** @var array */
    //    private $showMessages;
    //    /** @var string */
    //    private $should;

    //    abstract public function should(): string;
    //
    //    abstract public function shouldShowMessages(): array;
}
