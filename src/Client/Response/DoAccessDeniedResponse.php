<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response;

use KUL\FormBundle\Client\AbstractTemplateTargetService;
use Webmozart\Assert\Assert;

class DoAccessDeniedResponse extends AbstractDoResponse
{
    /**
     * if e.g. user trying to submit or (auto) temp save in a step while the next step is already triggered by another user or deadline.
     *
     * @var string
     */
    final public const CLIENT_DOES_ACTION_IN_NOT_THE_SAME_STEP = 'client-does-action-in-not-the-same-step';
    /**
     * if not @see TemplateInterface::isValidForClientUse() (not active or no published current version).
     *
     * @var string
     */
    final public const TEMPLATE_NOT_VALID_FOR_CLIENT_USE = 'template-not-valid-for-client-use';
    /**
     * if not @see TemplateInterface::isValidForClientUse() (not active or no published current version).
     *
     * @var string
     */
    final public const TEMPLATE_NOT_VALID_FOR_CLIENT_USE_DUE_TO_CURRENT_VERSION_DEACTIVATED = 'template-not-valid-for-client-use_due_to_current_version_deactivated';
    /**
     * if client (end-user with one or more roles) is trying to access with roles for which non of those roles
     * could @see TemplateInterface::couldAllowClientWithTargetAccessToParticipate()
     * (via @see TemplateInterface::couldAllowAtLeastOneOfRolesToParticipate() ).
     *
     * @var string
     */
    final public const TEMPLATE_COULD_NOT_ALLOW_CLIENT_TO_PARTICIPATE = 'template-could-not-allow-client-to-participate';
    /**
     * if client (end-user with one or more roles) is trying to access with roles for which non of those roles
     * could @see TemplateInterface::allowsClientWithTargetAccessToParticipateInCurrentVersion()
     * (via @see TemplateInterface::allowsAtLeastOneOfRolesToParticipateInCurrentVersion() ).
     *
     * @var string
     */
    final public const TEMPLATE_DOES_NOT_ALLOW_CLIENT_TO_PARTICIPATE_IN_CURRENT_VERSION = 'template-does-not-allow-client-to-participate-in-current-version';
    /**
     * if client (end-user with one or more roles) is trying to access with roles for which non of those roles
     * has access in the current state of the workflow for the target entity/object
     * via checking each role fro at least one role that @see InputNodeCollection::getReadableForRoleInStep().
     *
     * @var string
     */
    final public const CLIENT_HAS_NO_ROLES_WITH_ACCESS_IN_CURRENT_STEP = 'client-has-no-roles-with-access-in-current-step';
    /**
     * if client is trying to access (as in submit, temp save, export, etc..) with a role that has no access in the
     * current state of the workflow for the target entity/object
     * via @see InputNodeCollection::getReadableForRoleInStep().
     *
     * @var string
     */
    final public const CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP = 'client-role-has-no-access-in-current-step';
    /**
     * if client is trying to access (as in submit, temp save, export, etc..) with a role that has no access in the
     * current state of the workflow for the target entity/object
     * via @see InputNodeCollection::getReadableForRoleInStep().
     *
     * @var string
     */
    final public const NON_EXISTING_FILE = 'non-existing-file';
    /**
     * if client is trying to access (as in submit, temp save, export, etc..) with a role that has no access in the
     * current state of the workflow for the target entity/object
     * via @see InputNodeCollection::getReadableForRoleInStep().
     *
     * @var string
     */
    final public const CLIENT_ROLE_HAS_NO_ACCESS_TO_FILE = 'client-role-has-no-access-to-file';
    /**
     * if client is trying to access (as in submit, temp save, export, etc..) with a role that has no access in the
     * current state of the workflow for the target entity/object
     * via @see InputNodeCollection::getReadableForRoleInStep().
     *
     * @var string
     */
    final public const CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP_TO_SPECIFIED_NODE = 'client-role-has-no-access-in-current-step-to-specified-node';
    final public const CLIENT_REQUESTS_INVALID_NODE = 'client-requests-invalid-node';
    /**
     * if client is trying to submit, temp save, export,... with a role in request that is invalid
     * via @see AbstractTemplateTargetService::getOneOrNullValidAccessingRoleFromRequest().
     *
     * @var string
     */
    final public const REQUEST_SLUG_ROLE_DOES_NOT_MATCH_TEMPLATE_CLIENT_TARGET_ACCESS = 'request-slug-role-does-not-match-template-client-target-access';

    /**
     * @var string
     */
    final public const AUTO_SAVE_DISABLED_FOR_CLIENT_ACCESSING_ROLE = 'auto_save_disabled_for_client_accessing_role';

    private readonly string $reason;

    public function __construct(string $reason)
    {
        Assert::notEmpty($reason);

        $this->reason = $reason;
    }

    public function getReason(): string
    {
        return $this->reason;
    }
}
