<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoStoringResponse;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

class DoStoringSaveSuccessResponse extends AbstractDoStoringResponse
{
    /**
     * DoStoringSaveSuccessResponse constructor.
     */
    public function __construct(
        private readonly StepInterface $usedStep,
        private readonly StepInterface $newStep,
        private readonly Role $submittingRole,
        private readonly string $locale,
    ) {
    }

    public function getUsedStep(): StepInterface
    {
        return $this->usedStep;
    }

    public function getNewStep(): StepInterface
    {
        return $this->newStep;
    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ///////////////////////////// HELPER METHODS FOR IMPLEMENTING APP ////////////////////////////////////////////////
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * helper method for implementing app to determine if some roles need to be notified that the successful save action
     * triggered a new step to start and that step is configured with roles to be notified of the start of that step.
     */
    public function findRolesToNotifyThatNewStepHasStarted(): RoleCollection
    {
        // no new step started
        if ($this->getUsedStep()->getUid() === $this->getNewStep()->getUid()) {
            return new RoleCollection();
        }

        // new step has no roles configured to be notified
        if (!$this->getNewStep()->shouldNotifyAtLeastOneRoleOfStart()) {
            return new RoleCollection();
        }

        return $this->getNewStep()->getRolesThatShouldBeNotifiedOfStart();
    }

    /**
     * helper method for implementing app to check for and fetch a custom message, consisting of the concatenated
     * custom general & custom submitting role based messages (if any) that needs to be
     * shown to the user after successful save action in the used step. both these messages are configured on the used
     * step in which the successful save action was executed.
     */
    public function findFullCustomSuccessMessage(string $glue = ' '): ?string
    {
        $generalMsg = $this->findGeneralCustomSuccessMessage();
        $roleBasedMsg = $this->findSubmittingRoleBasedCustomSuccessMessage();

        if (is_string($generalMsg) && is_string($roleBasedMsg)) {
            return $generalMsg.$glue.$roleBasedMsg;
        }

        if (is_string($generalMsg)) {
            return $generalMsg;
        }

        if (is_string($roleBasedMsg)) {
            return $roleBasedMsg;
        }

        return null;
    }

    /**
     * helper method for implementing app to check for and fetch a non-role specific custom message that needs to be
     * shown to the user after successful save action in the used step. this message is configured on the used
     * step in which the successful save action was executed.
     */
    public function findGeneralCustomSuccessMessage(): ?string
    {
        if ($this->getUsedStep()->hasGeneralSubmitSuccessMessage($this->locale)) {
            return $this->getUsedStep()->getGeneralSubmitSuccessMessage($this->locale);
        }

        return null;
    }

    /**
     * helper method for implementing app to check for and fetch a role-specific custom message specific that needs to be
     * shown to the user after successful save action in the used step. this message is configured on the used
     * step in which the successful save action was executed. the role is the role under which the user successfully
     * submitted in the used step.
     */
    public function findSubmittingRoleBasedCustomSuccessMessage(): ?string
    {
        if ($this->getUsedStep()->hasRoleSubmitSuccessMessage($this->submittingRole, $this->locale)) {
            return $this->getUsedStep()->getRoleSubmitSuccessMessage($this->submittingRole, $this->locale);
        }

        return null;
    }
}
