<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoStoringResponse;

class DoRenderViewWithParametersForStoringResponse extends AbstractDoStoringResponse
{
    /**
     * DoRenderViewWithParametersForStoringResponse constructor.
     */
    public function __construct(private readonly array $renderParameters)
    {
    }

    /**
     * This array will be passed to the view that will actually render the form.
     */
    public function getRenderParameters(): array
    {
        return $this->renderParameters;
    }
}
