<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoStoringResponse;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Utility\Role;

class DoStoringSaveSuccessRedirectBackToStoringViewResponse extends DoStoringSaveSuccessResponse
{
    /**
     * DoStoringSaveSuccessRedirectBackToStoringViewResponse constructor.
     */
    public function __construct(
        StepInterface $usedStep,
        StepInterface $newStep,
        Role $submittingRole,
        string $locale,
        private readonly string $redirectUrlWithRoleSlug,
    ) {
        parent::__construct($usedStep, $newStep, $submittingRole, $locale);
    }

    public function getRedirectUrlWithRoleSlug(): string
    {
        return $this->redirectUrlWithRoleSlug;
    }
}
