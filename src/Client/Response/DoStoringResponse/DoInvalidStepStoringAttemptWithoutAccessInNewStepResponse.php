<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoStoringResponse;

use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * deny access when trying to save: if user saved in different step than the one he/she originally loaded the form
 * in but has still access in that new step.
 * technically, the user had access when he loaded the form in the original step but no more access in the new step,
 * but we can not allow users to save answers in a different step than the one they loaded their form in, especially if they have no access
 * in that new step. the user will have had write access to one or more questions in the original step, but no more access at all to
 * all of those questions in the new step. which would mean they would overwrite answers to which they no longer have write (overwrite) access to.
 * in fact, no more access at all, means they don't even have read access anymore to at least one question in this new step.
 * this is too complex and too error sensitive to handle and it would be simply wrong to allow them to edit answers to which they don't have access now.
 */
class DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse extends AbstractDoInvalidStepStoringAttemptResponse
{
    public function getDefaultTranslatedMessage(TranslatorInterface $translator, string $locale): string
    {
        $steps = $this->template->getCurrentVersion()->getWorkFlow()->getSteps();

        return $translator->trans(
            'storingAttemptInDifferentStep.autoTemporarySaveAttempt.noMoreAccessMessage',
            [
                '%usedStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($this->getUsedStep(), $locale),
                '%currentStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($this->getCurrentStep(), $locale),
            ],
            'kulFormStepStoringAttempt'
        );
    }
}
