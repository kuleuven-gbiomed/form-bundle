<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoStoringResponse;

use KUL\FormBundle\Client\Response\AbstractDoResponse;

abstract class AbstractDoStoringResponse extends AbstractDoResponse
{
}
