<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoStoringResponse;

use KUL\FormBundle\Client\Response\AbstractDoResponse;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;

class DoStoringTemporarySaveSuccessResponse extends AbstractDoResponse
{
    public function __construct(private readonly StepInterface $usedStep)
    {
    }

    public function getUsedStep(): StepInterface
    {
        return $this->usedStep;
    }
}
