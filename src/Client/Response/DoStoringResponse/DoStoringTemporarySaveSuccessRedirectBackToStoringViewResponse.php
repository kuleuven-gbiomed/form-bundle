<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoStoringResponse;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;

class DoStoringTemporarySaveSuccessRedirectBackToStoringViewResponse extends DoStoringTemporarySaveSuccessResponse
{
    /**
     * DoStoringTemporarySaveSuccessRedirectBackToStoringViewResponse constructor.
     */
    public function __construct(StepInterface $usedStep, private readonly string $redirectUrlWithRoleSlug)
    {
        parent::__construct($usedStep);
    }

    public function getRedirectUrlWithRoleSlug(): string
    {
        return $this->redirectUrlWithRoleSlug;
    }
}
