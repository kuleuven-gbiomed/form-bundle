<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoStoringResponse;

class DoStoringTemporarySaveSuccessRedirectAwayResponse extends DoStoringTemporarySaveSuccessResponse
{
}
