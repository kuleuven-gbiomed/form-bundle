<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoStoringResponse;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * deny access when trying to save: if user saved in different step than the one he/she originally loaded the form in
 * but has still access in that new step. the access in the new step can - and most likely will - be different that the
 * access in the original step (access to different questions, different access type (read <-> write) to same questions, etc.)
 * technically, the user had access when he loaded the form in the original step and still has access in the new step,
 * but we can not allow users to save answers in a different step than the one they loaded their form in. the user will have
 * had write access to one or more questions in the original step, but - very likely - have no more write access to one or
 * more of those questions in the new step. which would mean they would overwrite answers to which they no longer have
 * write (overwrite) access to. this is too complex and too error sensitive to handle and it would be simply wrong to
 * allow them to edit answers to which they don't have access now.
 */
class DoInvalidStepStoringAttemptWithAccessInNewStepResponse extends AbstractDoInvalidStepStoringAttemptResponse
{
    public function __construct(
        StepInterface $usedStep,
        StepInterface $currentStep,
        TemplateInterface $template,
        private readonly string $reloadUrl,
    ) {
        parent::__construct($usedStep, $currentStep, $template);
    }

    public function getReloadUrl(): string
    {
        return $this->reloadUrl;
    }

    public function getDefaultTranslatedMessage(TranslatorInterface $translator, string $locale): string
    {
        $steps = $this->template->getCurrentVersion()->getWorkFlow()->getSteps();

        return $translator->trans(
            'storingAttemptInDifferentStep.autoTemporarySaveAttempt.stillAccessMessage',
            [
                '%usedStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($this->getUsedStep(), $locale),
                '%currentStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($this->getCurrentStep(), $locale),
            ],
            'kulFormStepStoringAttempt'
        );
    }
}
