<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoStoringResponse;

use KUL\FormBundle\Client\Response\DoAccessDeniedResponse;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * deny access when trying to save due to user saving in a different step than the one he loaded the form in (all type of saving: manual & automatic).
 */
abstract class AbstractDoInvalidStepStoringAttemptResponse extends DoAccessDeniedResponse
{
    abstract public function getDefaultTranslatedMessage(TranslatorInterface $translator, string $locale): string;

    public function __construct(
        private readonly StepInterface $usedStep,
        private readonly StepInterface $currentStep,
        protected readonly TemplateInterface $template,
    ) {
        parent::__construct(parent::CLIENT_DOES_ACTION_IN_NOT_THE_SAME_STEP);

        if (!$this->template->getCurrentVersion()->getWorkFlow()->hasOneStepByUid($this->usedStep->getUid())) {
            throw new \InvalidArgumentException('invalid storing attempt error: given used step is not found in the workflow!');
        }

        if (!$this->template->getCurrentVersion()->getWorkFlow()->hasOneStepByUid($this->currentStep->getUid())) {
            throw new \InvalidArgumentException('invalid storing attempt error: given current step is not found in the workflow!');
        }

        if ($this->usedStep->getUid() === $this->currentStep->getUid()) {
            throw new \DomainException('not an invalid storing attempt: used step equals current step!');
        }
    }

    public function getUsedStep(): StepInterface
    {
        return $this->usedStep;
    }

    public function getCurrentStep(): StepInterface
    {
        return $this->currentStep;
    }
}
