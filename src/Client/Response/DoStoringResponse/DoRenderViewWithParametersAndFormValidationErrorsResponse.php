<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoStoringResponse;

use KUL\FormBundle\Client\Response\AbstractDoResponse;

class DoRenderViewWithParametersAndFormValidationErrorsResponse extends AbstractDoResponse
{
    /**
     * DoRenderViewWithParametersForStoringResponse constructor.
     */
    public function __construct(private readonly array $renderParameters)
    {
    }

    public function getRenderParameters(): array
    {
        return $this->renderParameters;
    }
}
