<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoFileResponse;

use KUL\FormBundle\Client\SavedFile\SavedFileCollection;

final class DoFileAnswerUploadSuccessResponse extends AbstractDoFileResponse
{
    public function __construct(private readonly SavedFileCollection $savedFiles)
    {
    }

    public function getSavedFiles(): SavedFileCollection
    {
        return $this->savedFiles;
    }
}
