<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoFileResponse;

use KUL\FormBundle\Client\Response\AbstractDoResponse;
use KUL\FormBundle\Client\Util\File\NodeTempFileDataRowCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Entity\UploadedFile;
use Symfony\Contracts\Translation\TranslatorInterface;

class DoFileAnswerUploadInvalidFilesResponse extends AbstractDoResponse
{
    public function __construct(
        private readonly NodeTempFileDataRowCollection $nodeTempFilesData,
        private readonly UploadInputNode $uploadNode,
        private readonly TranslatorInterface $translator,
    ) {
    }

    /**
     * @return array{success: false, invalid_files_data: NodeTempFileDataRowCollection, title: string, info_messages: (never[]|string[]), invalid_mime_type_label: string, invalid_extension_label: string, invalid_size_label: string}
     */
    public function getUploadInvalidFilesResponseData(): array
    {
        /** @var UploadInput $uploadInput */
        $uploadInput = $this->uploadNode->getInput();
        $infoMessages = [];
        $invalidFilesData = $this->nodeTempFilesData->getInvalid();
        if ($invalidFilesData->hasWithInvalidSize()) {
            $infoMessages[] = $this->translator->trans(
                'fields.uploadType.failedUpload.invalidFiles.invalidSize.allowedSize',
                [
                    '%maxSize%' => UploadedFile::convertToReadableSize($uploadInput->getMaxFileSize()),
                    '%minSize%' => UploadedFile::convertToReadableSize($uploadInput->getMinFileSize()),
                ],
                'kulFormBase'
            );
        }
        if ($invalidFilesData->hasWithInvalidMimeType()) {
            $infoMessages[] = $this->translator->trans(
                'fields.uploadType.failedUpload.invalidFiles.invalidMimeType.allowedTypes',
                [
                    '%mimeTypes%' => implode(' - ', $uploadInput->getMimeTypes()),
                ],
                'kulFormBase'
            );
        }
        if ($invalidFilesData->hasWithInvalidExtension()) {
            $infoMessages[] = $this->translator->trans(
                'fields.uploadType.failedUpload.invalidFiles.invalidExtension.allowedExtensions',
                [
                    '%extensions%' => implode(' - ', $uploadInput->getExtensions()),
                ],
                'kulFormBase'
            );
        }
        $title = $this->translator->trans('fields.uploadType.failedUpload.invalidFiles.title', [], 'kulFormBase');

        return [
            'success' => false,
            'invalid_files_data' => $invalidFilesData,
            'title' => $title,
            'info_messages' => $infoMessages,
            'invalid_mime_type_label' => $this->translator
                ->trans('fields.uploadType.failedUpload.invalidFiles.invalidMimeType.label', [], 'kulFormBase'),
            'invalid_extension_label' => $this->translator
                ->trans('fields.uploadType.failedUpload.invalidFiles.invalidExtension.label', [], 'kulFormBase'),
            'invalid_size_label' => $this->translator
                ->trans('fields.uploadType.failedUpload.invalidFiles.invalidSize.label', [], 'kulFormBase'),
        ];
    }
}
