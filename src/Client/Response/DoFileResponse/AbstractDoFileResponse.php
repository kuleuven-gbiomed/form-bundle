<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoFileResponse;

use KUL\FormBundle\Client\Response\AbstractDoResponse;

abstract class AbstractDoFileResponse extends AbstractDoResponse
{
}
