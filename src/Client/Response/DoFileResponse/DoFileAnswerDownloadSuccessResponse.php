<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Response\DoFileResponse;

use KUL\FormBundle\Entity\UploadedFile;

class DoFileAnswerDownloadSuccessResponse extends AbstractDoFileResponse
{
    public function __construct(private readonly UploadedFile $file)
    {
    }

    public function getFile(): UploadedFile
    {
        return $this->file;
    }
}
