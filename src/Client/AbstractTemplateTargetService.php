<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client;

use KUL\FormBundle\Client\Access\AccessChecker;
use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\Access\ClientCombinedAccessObjectCollection;
use KUL\FormBundle\Client\Access\ClientTemplateTargetFormAccess;
use KUL\FormBundle\Client\Access\FixedStepOnlyAccessChecker;
use KUL\FormBundle\Client\Access\GeneralAccessChecker;
use KUL\FormBundle\Client\Access\InvalidAccessArgumentException;
use KUL\FormBundle\Client\Manage\FormManager;
use KUL\FormBundle\Client\Manage\FormType\BaseType;
use KUL\FormBundle\Client\Response\DoAccessDeniedResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoInvalidStepStoringAttemptWithAccessInNewStepResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse;
use KUL\FormBundle\Client\Revert\Rendering\RevertActionsBuilder;
use KUL\FormBundle\Client\Route\ClientRouteDataCollection;
use KUL\FormBundle\Client\Route\ClientRoutesBuilder;
use KUL\FormBundle\Client\Route\DownloadFileQuestionAnswerClientRouteData;
use KUL\FormBundle\Client\Route\GlobalScoreCalculatedValueClientRouteData;
use KUL\FormBundle\Client\Route\StoringAutoTemporarySaveClientRouteData;
use KUL\FormBundle\Client\Route\StoringSaveClientRouteData;
use KUL\FormBundle\Client\Route\StoringViewClientRouteData;
use KUL\FormBundle\Client\Route\UploadFileQuestionAnswerClientRouteData;
use KUL\FormBundle\Client\Util\File\AbstractFileUrl;
use KUL\FormBundle\Client\Util\GlobalScore\CalculateValueUrl;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Factory\DateTimeImmutableFactory;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use KUL\FormBundle\Entity\SubmissionRepository;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Webmozart\Assert\Assert;

abstract class AbstractTemplateTargetService
{
    protected RoleCollection $availableAdminRoles;

    protected ?EventDispatcherInterface $eventDispatcher = null;

    private ?RevertActionsBuilder $revertActionsBuilder = null;

    public function __construct(
        protected StoredTemplateTargetRepository $storedTemplateTargetRepository,
        protected SubmissionRepository $submissionRepository,
        protected ClientRoutesBuilder $clientRoutesBuilder,
        protected FormManager $formManager,
        protected string $defaultLocale,
        array $availableAdminRoles,
        protected int $autoSaveInterval,
        protected int $idleCheckInterval,
        protected TemplateRepository $templateRepository,
    ) {
        $this->availableAdminRoles = RoleCollection::fromRoleNames($availableAdminRoles);
    }

    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher): void
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * set via (lazy) calling in Symfony service configuration in client.yml. not set via constructor!
     * but psalm is not happy about this, wants it to be set via constructor, hence the nullable $revertActionsBuilder
     * prop and the getter checking if it's set.
     */
    public function setRevertActionsBuilder(RevertActionsBuilder $revertActionsBuilder): void
    {
        $this->revertActionsBuilder = $revertActionsBuilder;
    }

    public function getRevertActionsBuilder(): RevertActionsBuilder
    {
        if (!$this->revertActionsBuilder instanceof RevertActionsBuilder) {
            throw new \InvalidArgumentException('RevertActionsBuilder not set');
        }

        return $this->revertActionsBuilder;
    }

    /**
     * check route in request and other required routes or throw exception.
     *
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     * @throws \Exception
     */
    protected function assertBuildRequirements(
        Request $request,
        TemplateInterface $template,
        ?\DateTimeInterface $calculatedStepsCheckDate = null,
    ): void {
        // check route in request and other required routes or throw exception
        $this->clientRoutesBuilder->validateClientRouteDataSetForTemplateInRequest($request, $template);

        $this->assertReferenceDate($template, $calculatedStepsCheckDate);
    }

    protected function assertTemplateBaseBuildRequirements(
        TemplateInterface $template,
        ?\DateTimeInterface $calculatedStepsCheckDate = null,
    ): void {
        $this->clientRoutesBuilder->validateTargetingTypeIsConfigured($template->getTargetingType());

        $this->assertReferenceDate($template, $calculatedStepsCheckDate);
    }

    protected function assertReferenceDate(
        TemplateInterface $template,
        ?\DateTimeInterface $calculatedStepsCheckDate = null,
    ): void {
        $workFlow = $template->getCurrentVersion()->getWorkFlow();

        // if steps with calculated dates used, we require a check date for calculated steps otherwise all workflow
        // calls will most likely fail anyway & access can never be checked, let alone granted.
        // (in contrary to steps with fixed dates, where we can use NOW as the default check date for fixed steps)
        if ($workFlow->hasStepsOfCalculatedPeriodType() && !$calculatedStepsCheckDate instanceof \DateTimeInterface) {
            throw new InvalidAccessArgumentException('a date (derived from target) is needed to be able to calculate start- and end dates to check for and retrieve open step(s) in a workflow that makes use of steps of type '.StepWithCalculatedDateStart::class);
        }
    }

    /**
     * need do to this before building any @see ClientCombinedAccessObject or they will fail.
     */
    protected function getOneOrNullPreliminaryDoAccessDeniedResponse(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
    ): ?DoAccessDeniedResponse {
        // template must be active and have current version
        if (!$template->isValidForClientUse()) {
            $reason = $template->isDeactivated() ?
                DoAccessDeniedResponse::TEMPLATE_NOT_VALID_FOR_CLIENT_USE_DUE_TO_CURRENT_VERSION_DEACTIVATED :
                DoAccessDeniedResponse::TEMPLATE_NOT_VALID_FOR_CLIENT_USE;

            return new DoAccessDeniedResponse($reason);
        }

        // at least one of client's roles that give him access to the target, must be a role with possible template participation
        // = the client must have a role that COULD allow client to participate
        if (!$template->couldAllowClientWithTargetAccessToParticipate($clientWithTargetAccess)) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::TEMPLATE_COULD_NOT_ALLOW_CLIENT_TO_PARTICIPATE);
        }

        // at least one of client's roles that give him access to the target, must allow him access to at least one
        // question with at least read rights at at least one point in time in the current version
        // = the client must have role that WILL allow client to participate in at least one step of workflow (no, later or in past)
        if (!$template->allowsClientWithTargetAccessToParticipateInCurrentVersion($clientWithTargetAccess)) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::TEMPLATE_DOES_NOT_ALLOW_CLIENT_TO_PARTICIPATE_IN_CURRENT_VERSION);
        }

        return null;
    }

    protected function getOneOrNullValidAccessingRoleFromRequest(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
    ): ?Role {
        $roleSlug = $request->attributes->get('roleSlug');

        if ('' === $roleSlug || !is_string($roleSlug)) {
            return null;
        }

        $roleSlugifier = $template->getParticipatableRoleSlugifier();

        // no role exist for slug in template's roles
        if (!$roleSlugifier->hasRoleForSlug($roleSlug)) {
            return null;
        }

        $role = Role::fromName($roleSlugifier->getRoleForSlug($roleSlug));

        // role is not part of client's accessing roles
        if (!$clientWithTargetAccess->getAccessingRoles()->hasRole($role)) {
            return null;
        }

        return $role;
    }

    protected function getOneOrNullStoredTemplateTarget(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
    ): ?StoredTemplateTargetInterface {
        if (!$template instanceof Template) {
            throw new \InvalidArgumentException('template must be instance of '.Template::class);
        }

        /** @var StoredTemplateTargetInterface|null $result */
        $result = $this->storedTemplateTargetRepository->getOneOrNullByTargetAndTemplate($clientWithTargetAccess->getTarget(), $template);

        return $result;
    }

    /**
     * Find client combined access objects for any of the given roles in $clientWithTargetAccess at date $checkDate.
     *
     * The reference dates are used to calculate the periods of calculated steps.
     *
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    protected function getNullOrFormAccessForAllClientAccessingRoles(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        ?\DateTimeInterface $referenceDateForTargetWithoutSubmissions,
        ?\DateTimeInterface $referenceDateForTargetWithSubmissions,
    ): ?ClientTemplateTargetFormAccess {
        // if set, use the optional 'request' role (is set if coming via switching or overview or users sharing links).
        // if not set or not valid, the called method will fallback to first found according to template's role hierarchy.
        $preferredRelevantRole = $this->getOneOrNullValidAccessingRoleFromRequest($request, $template, $clientWithTargetAccess);

        return $this->getNullOrFormAccessForMostRelevantRoleBasedOnAllClientAccessingRoles(
            $template,
            $clientWithTargetAccess,
            $referenceDateForTargetWithoutSubmissions,
            $referenceDateForTargetWithSubmissions,
            $preferredRelevantRole
        );
    }

    protected function getNullOrFormAccessForMostRelevantRoleBasedOnAllClientAccessingRoles(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        ?\DateTimeInterface $referenceDateForTargetWithoutSubmissions,
        ?\DateTimeInterface $referenceDateForTargetWithSubmissions,
        ?Role $preferredRelevantRole = null,
    ): ?ClientTemplateTargetFormAccess {
        $accessObjects = $this->getClientCombinedAccessGrantedObjects(
            $template,
            $clientWithTargetAccess,
            $referenceDateForTargetWithoutSubmissions,
            $clientWithTargetAccess->getCurrentDate(),
            $referenceDateForTargetWithSubmissions,
            null
        );

        // if client has no access to current version template with at least one of client roles, deny access
        // NOTE: at this point, already verified that client has role(s) that will or did allow client to actively
        // participate to current version at some point in time, so this empty set of access objects
        // means client has no access at this point in workflow, based on given dates, but might have in the future or past.
        if ($accessObjects->isEmpty()) {
            return null;
        }

        // could be more than one object for client-target-accessingRoles. either use the optional one given in preferredRelevantRole
        // as 'request' role (could be set if coming via switching or overview or users sharing links). else use first found
        // according to template's role hierarchy (if any could be build). So no exception if 'request' role not set or invalid
        return new ClientTemplateTargetFormAccess($accessObjects, $preferredRelevantRole);
    }

    /**
     * Find a client combined access object for $clientWithTargetAddress and a role hidden somewhere in $request. 🤯.
     *
     * The reference dates are used to calculate the periods of calculated steps.
     *
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    protected function getNullOrFormAccessForSingleClientAccessingRoleInRequest(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        ?\DateTimeInterface $referenceDateForTargetWithoutSubmissions,
        ?\DateTimeInterface $referenceDateForTargetWithSubmissions,
    ): ?ClientTemplateTargetFormAccess {
        // saving (submit or temp saving) is always done for a specified role, set in the request's url (HTML form action) as slugRole.
        $requestAccessingRole = $this->getOneOrNullValidAccessingRoleFromRequest($request, $template, $clientWithTargetAccess);

        if (!$requestAccessingRole instanceof Role) {
            return null;
        }

        return $this->getNullOrFormAccessForSingleClientAccessingRole(
            $template,
            $clientWithTargetAccess,
            $requestAccessingRole,
            $referenceDateForTargetWithoutSubmissions,
            $referenceDateForTargetWithSubmissions
        );
    }

    /**
     * Find a client combined access object for $clientWithTargetAddress and specific role.
     *
     * The reference dates are used to calculate the periods of calculated steps.
     *
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    protected function getNullOrFormAccessForSingleClientAccessingRole(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        Role $accessingRole,
        ?\DateTimeInterface $referenceDateForTargetWithoutSubmissions,
        ?\DateTimeInterface $referenceDateForTargetWithSubmissions,
    ): ?ClientTemplateTargetFormAccess {
        // role is not part of client's accessing roles
        if (!$clientWithTargetAccess->getAccessingRoles()->hasRole($accessingRole)) {
            return null;
        }

        $accessObjects = $this->getClientCombinedAccessGrantedObjects(
            $template,
            $clientWithTargetAccess,
            $referenceDateForTargetWithoutSubmissions,
            $clientWithTargetAccess->getCurrentDate(),
            $referenceDateForTargetWithSubmissions,
            $accessingRole
        );

        if ($accessObjects->isEmpty() || !$accessObjects->hasOneForAccessingRole($accessingRole)) {
            return null;
        }

        return new ClientTemplateTargetFormAccess($accessObjects, $accessingRole);
    }

    /**
     * Return all possible ClientCombinedAccessObjects for user at a given moment $checkDate.
     *
     * The reference dates are the dates used to calculate the calculated steps.
     *
     * @throws MissingVersionException
     * @throws InvalidAccessArgumentException
     */
    protected function getClientCombinedAccessGrantedObjects(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        ?\DateTimeInterface $referenceDateForTargetWithoutSubmissions,
        \DateTimeInterface $checkDate,
        ?\DateTimeInterface $referenceDateForTargetWithSubmissions,
        ?Role $limitToRole,
    ): ClientCombinedAccessObjectCollection {
        $clientAccessingRoles = $clientWithTargetAccess->getAccessingRoles();
        // if role to limit on, is not one of client's role, then no point in continuing cause ClientCombinedAccess building will fail anyway
        if ($limitToRole instanceof Role && !$clientAccessingRoles->hasRole($limitToRole)) {
            return new ClientCombinedAccessObjectCollection();
        }

        $clientAccessingRoles = $limitToRole instanceof Role ?
            new RoleCollection([$limitToRole]) : $clientWithTargetAccess->getAccessingRoles();

        // filter out client roles that could not participate
        $accessingRolesToCheck = $template->getRolesThatParticipateInCurrentVersion()->getDuplicatesByRoles($clientAccessingRoles);

        if ($accessingRolesToCheck->isEmpty()) {
            return new ClientCombinedAccessObjectCollection();
        }

        $accessObjectsList = [];
        $firstAccessObject = null;
        $storedTemplateTarget = $this->getOneOrNullStoredTemplateTarget($template, $clientWithTargetAccess);

        /** @var Role $accessingRole */
        foreach ($accessingRolesToCheck as $accessingRole) {
            if ($firstAccessObject instanceof ClientCombinedAccessObject) {
                // As soon as an access object was created for one role, an access object will
                // also be created for all next roles:
                // this will allow to render a 'role switcher' in de view for the accessing user with 'view-form-as-role'
                // links for each of the user's roles for which he/she has (also) access.
                // usually, a minority of users are passed in to this service by the implementing app with more than one role.
                // those users need to be able to view the form under all their roles with at least some access.
                $accessObjectsList[] = $firstAccessObject->withDifferentAccessingRole($accessingRole);
            } else {
                $firstAccessObject = $this->getClientCombinedAccessObject(
                    $template,
                    $clientWithTargetAccess,
                    $checkDate,
                    $referenceDateForTargetWithoutSubmissions,
                    $referenceDateForTargetWithSubmissions,
                    $accessingRole,
                    $storedTemplateTarget
                );
                $accessObjectsList[] = $firstAccessObject;
            }
        }

        $accessObjects = new ClientCombinedAccessObjectCollection($accessObjectsList);

        // get all role-accesses for client-target-accessingRoles with access granted to template based on workflow
        // state as derived from given dates, sorted by template's roles hierarchy so that first object is primary access object
        return $accessObjects->getWhereGrantsAccess()->getSortedByTemplateHierarchicalRoles($template);
    }

    /**
     * @throws Route\InvalidClientRouteException
     */
    protected function getAutoCalculatePrototypeUrl(
        Request $request,
        ClientRouteDataCollection $clientRouteDataSet,
        ClientCombinedAccessObject $accessObject,
    ): string {
        return $this->clientRoutesBuilder->generateUrlFromRequestForClientRouteDataAndParameters(
            $request,
            $clientRouteDataSet->getOneForGlobalScoreCalculatedValue(),
            [
                GlobalScoreCalculatedValueClientRouteData::ROUTE_VARIABLE_NODE_UID => CalculateValueUrl::NODE_UID_PLACEHOLDER,
                GlobalScoreCalculatedValueClientRouteData::ROUTE_VARIABLE_ROLE_SLUG => $accessObject->getSlugifiedRole(),
            ]
        );
    }

    /**
     * @throws Route\InvalidClientRouteException
     */
    protected function getStoringViewUrl(
        Request $request,
        ClientRouteDataCollection $clientRouteDataSet,
    ): string {
        return $this->clientRoutesBuilder->generateUrlFromRequestForClientRouteDataAndParameters(
            $request,
            $clientRouteDataSet->getOneForStoringView()
        );
    }

    /**
     * @throws Route\InvalidClientRouteException
     */
    protected function getStoringViewUrlForAccessingRole(
        Request $request,
        ClientRouteDataCollection $clientRouteDataSet,
        ClientCombinedAccessObject $accessObject,
    ): string {
        return $this->clientRoutesBuilder->generateUrlFromRequestForClientRouteDataAndParameters(
            $request,
            $clientRouteDataSet->getOneForStoringView(),
            [
                StoringViewClientRouteData::ROUTE_VARIABLE_ROLE_SLUG => $accessObject->getSlugifiedRole(),
            ]
        );
    }

    /**
     * @throws Route\InvalidClientRouteException
     */
    protected function getAutoCalculateUrlForScorableNode(
        Request $request,
        ClientRouteDataCollection $clientRouteDataSet,
        ClientCombinedAccessObject $accessObject,
        ScorableInputNode $node,
    ): string {
        return $this->clientRoutesBuilder->generateUrlFromRequestForClientRouteDataAndParameters(
            $request,
            $clientRouteDataSet->getOneForGlobalScoreCalculatedValue(),
            [
                GlobalScoreCalculatedValueClientRouteData::ROUTE_VARIABLE_NODE_UID => $node->getUid(),
                GlobalScoreCalculatedValueClientRouteData::ROUTE_VARIABLE_ROLE_SLUG => $accessObject->getSlugifiedRole(),
            ]
        );
    }

    /**
     * @throws Route\InvalidClientRouteException
     */
    protected function getAutoTempSaveUrl(
        Request $request,
        ClientRouteDataCollection $clientRouteDataSet,
        ClientCombinedAccessObject $accessObject,
    ): string {
        return $this->clientRoutesBuilder->generateUrlFromRequestForClientRouteDataAndParameters(
            $request,
            $clientRouteDataSet->getOneForStoringAutoTemporarySave(),
            [
                StoringAutoTemporarySaveClientRouteData::ROUTE_VARIABLE_ROLE_SLUG => $accessObject->getSlugifiedRole(),
            ]
        );
    }

    /**
     * @return string
     *
     * @throws Route\InvalidClientRouteException
     */
    protected function getSaveUrl(
        Request $request,
        ClientRouteDataCollection $clientRouteDataSet,
        ClientCombinedAccessObject $accessObject,
    ) {
        return $this->clientRoutesBuilder->generateUrlFromRequestForClientRouteDataAndParameters(
            $request,
            $clientRouteDataSet->getOneForStoringSave(),
            [
                StoringSaveClientRouteData::ROUTE_VARIABLE_ROLE_SLUG => $accessObject->getSlugifiedRole(),
            ]
        );
    }

    /**
     * @return string
     *
     * @throws Route\InvalidClientRouteException
     */
    protected function getUploadFilePrototypeUrl(
        Request $request,
        ClientRouteDataCollection $clientRouteDataSet,
        ClientCombinedAccessObject $accessObject,
    ) {
        return $this->clientRoutesBuilder->generateUrlFromRequestForClientRouteDataAndParameters(
            $request,
            $clientRouteDataSet->getOneForUploadFileQuestionAnswer(),
            [
                UploadFileQuestionAnswerClientRouteData::ROUTE_VARIABLE_NODE_UID => AbstractFileUrl::NODE_UID_PLACEHOLDER,
                UploadFileQuestionAnswerClientRouteData::ROUTE_VARIABLE_ROLE_SLUG => $accessObject->getSlugifiedRole(),
            ]
        );
    }

    /**
     * @return string
     *
     * @throws Route\InvalidClientRouteException
     */
    protected function getDownloadFilePrototypeUrl(
        Request $request,
        ClientRouteDataCollection $clientRouteDataSet,
        ClientCombinedAccessObject $accessObject,
    ) {
        return $this->clientRoutesBuilder->generateUrlFromRequestForClientRouteDataAndParameters(
            $request,
            $clientRouteDataSet->getOneForDownloadFileQuestionAnswer(),
            [
                DownloadFileQuestionAnswerClientRouteData::ROUTE_VARIABLE_NODE_UID => AbstractFileUrl::NODE_UID_PLACEHOLDER,
                DownloadFileQuestionAnswerClientRouteData::ROUTE_VARIABLE_FILE_UID => AbstractFileUrl::SAVED_FILE_UID_PLACEHOLDER,
                DownloadFileQuestionAnswerClientRouteData::ROUTE_VARIABLE_ROLE_SLUG => $accessObject->getSlugifiedRole(),
            ]
        );
    }

    /**
     * Get a ClientCombinedAccessObject for the given $accessingRole on given $checkDate.
     *
     * FIXME: This is a dodgy function. The reference dates are only needed if there are calculated steps.
     *
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    private function getClientCombinedAccessObject(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        \DateTimeInterface $checkDate,
        ?\DateTimeInterface $referenceDateForTargetWithoutSubmissions,
        ?\DateTimeInterface $referenceDateForTargetWithSubmissions,
        Role $accessingRole,
        ?StoredTemplateTargetInterface $storedTemplateTarget,
    ): ClientCombinedAccessObject {
        $accessChecker = $this->getAccessChecker($template, $checkDate, $referenceDateForTargetWithoutSubmissions, $referenceDateForTargetWithSubmissions);

        return $accessChecker->getClientCombinedAccessObject(
            $template,
            $clientWithTargetAccess,
            $accessingRole,
            $storedTemplateTarget
        );
    }

    protected function getAccessChecker(
        TemplateInterface $template,
        \DateTimeInterface $checkDate,
        ?\DateTimeInterface $referenceDateForTargetWithoutSubmissions,
        ?\DateTimeInterface $referenceDateForTargetWithSubmissions,
    ): AccessChecker {
        if ($template->getCurrentVersion()->getWorkFlow()->hasStepsOfCalculatedPeriodType()) {
            if (!$referenceDateForTargetWithoutSubmissions instanceof \DateTimeInterface) {
                throw new \InvalidArgumentException('A reference date is required, since you have calculated steps');
            }
            if (!$referenceDateForTargetWithSubmissions instanceof \DateTimeInterface) {
                $referenceDateForTargetWithSubmissions = $referenceDateForTargetWithoutSubmissions;
            }

            return new GeneralAccessChecker(
                $this->availableAdminRoles,
                DateTimeImmutableFactory::createFromDate($checkDate),
                $referenceDateForTargetWithoutSubmissions,
                $referenceDateForTargetWithSubmissions
            );
        }

        return new FixedStepOnlyAccessChecker(
            $this->availableAdminRoles,
            DateTimeImmutableFactory::createFromDate($checkDate)
        );
    }

    /**
     * @throws Route\InvalidClientRouteException
     * @throws MissingVersionException
     */
    protected function getFormForWriteAccess(Request $request, ClientCombinedAccessObject $accessObject): FormInterface
    {
        Assert::true($accessObject->isWriteAccessAuthorized());

        $clientRouteDataSet = $this->clientRoutesBuilder->getClientRouteDataSetForTemplate($accessObject->getTemplate());

        return $this->formManager->createFormByAccessObject(
            $accessObject,
            $this->getSaveUrl($request, $clientRouteDataSet, $accessObject),
            $request->getLocale()
        );
    }

    /**
     * StepStoringAttempt: check if user saves (auto or manual temporary saving, manual non-temporary saving and when
     * uploading & storing files as potential answers for an upload question) in same step as he/she rendered the form view:
     * get the used step from the request in which user rendered the form he/she is submitting (rendered as hidden field in the rendered form)
     * retrieving used step directly from request's (POST) data. if value or step doesn't exist -> user tampered with form -> will throw exception
     * if user rendered in a step A, but now saves, temporary saves or auto temporary saves and the form was by now moved
     * to a next step (by deadline or by (an)other user(s)'s action(s)), then we need to break off the save. it still might work,
     * but is to risky since the user most likely has other (write) permissions in that next step.
     *
     * we don't have to check roles, submitting in another step with a different role is possible, but irrelevant if the steps are different.
     * submitting in same step with different role as in which the form was rendered is only possible if user tampered with the safe urls, which is caught hereafter.
     */
    protected function findInvalidStepStoringAttemptByCurrentAccess(
        Request $request,
        ClientCombinedAccessObject $accessObject,
    ): ?DoInvalidStepStoringAttemptWithAccessInNewStepResponse {
        $stepUsedInStoringAttempt = self::extractStepUsedInStoringAttemptFromRequestAndWorkflow(
            $request,
            $accessObject->getCurrentVersionWorkflow()
        );

        $reloadUrl = $this->getStoringViewUrlForAccessingRole(
            $request,
            $this->clientRoutesBuilder->getClientRouteDataSetForTemplate($accessObject->getTemplate()),
            $accessObject
        );

        if ($stepUsedInStoringAttempt->getUid() !== $accessObject->getOpenFlowStep()->getUid()) {
            return new DoInvalidStepStoringAttemptWithAccessInNewStepResponse(
                $stepUsedInStoringAttempt,
                $accessObject->getOpenFlowStep(),
                $accessObject->getTemplate(),
                $reloadUrl
            );
        }

        return null;
    }

    /**
     * also @see self::findInvalidStepStoringAttemptByCurrentAccess above.
     *
     * if user had write access when rendering the form, but the form has now moved to next step and user has no access
     * anymore in that next (current) step then we could/have to deny access with custom message about this.
     * This is more of an edge case: usually, when admins construct a form (Template), they give each user (roles) at
     * least read-only access in every steps following the first step in which that user (role) was given (usually write) access.
     * but uses cases to deny user access in intermediate steps could be found. So for those uses cases, we differentiate
     * the access denied more specifically to an InvalidStepStoringAttempt.
     */
    protected function findInvalidStepStoringAttemptByDates(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        ?\DateTimeInterface $calculatedStepsReferenceDate,
        ?\DateTimeInterface $secondaryCalculatedStepsCheckDate,
    ): ?DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse {
        // get the step from the workflow via its uid in the request. that way, we immediately assert it exists in current workflow.
        $stepUsedInStoringAttempt = self::extractStepUsedInStoringAttemptFromRequestAndWorkflow(
            $request,
            $template->getCurrentVersion()->getWorkFlow()
        );

        // if user has no access in current open step, and someone saved or temporary saved the template for this target
        // at least once before, check stepUsedToSubmit against the last known used step according to storage. if nobody
        // has yet (temp) saved yet, find the open step by dates and check stepUsedToSubmit against that
        $accessChecker = $this->getAccessChecker($template, $clientWithTargetAccess->getCurrentDate(), $calculatedStepsReferenceDate, $secondaryCalculatedStepsCheckDate);
        $currentOpenStep = $accessChecker->findOpenStepByDates($template, $this->getOneOrNullStoredTemplateTarget($template, $clientWithTargetAccess));

        // it's highly unlikely (impossible) that there would be no open step at this point, since the user managed to load the form in one.
        // still, let's assert just in case
        if (!$currentOpenStep instanceof StepInterface) {
            throw new \BadMethodCallException('no open step found to check step storing attempt against');
        }

        return $stepUsedInStoringAttempt->getUid() !== $currentOpenStep->getUid() ?
            new DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse($stepUsedInStoringAttempt, $currentOpenStep, $template) : null;
    }

    private static function extractStepUsedInStoringAttemptFromRequestAndWorkflow(Request $request, WorkFlow $workFlow): StepInterface
    {
        $allFormData = $request->request->all();
        if (!array_key_exists(BaseType::FORM_NAME, $allFormData)) {
            throw new \InvalidArgumentException('expected to find key '.BaseType::FORM_NAME.' in the request form POST data. got none!');
        }

        $baseFormData = $allFormData[BaseType::FORM_NAME];
        if (!is_array($baseFormData)) {
            throw new \InvalidArgumentException('expected an array value for key '.BaseType::FORM_NAME.' in the request form POST data. got '.get_debug_type($baseFormData).'!');
        }

        if (!array_key_exists(BaseType::USED_STEP_HIDDEN_TYPE_FIELD_NAME, $baseFormData)) {
            throw new \InvalidArgumentException('expected to find key '.BaseType::USED_STEP_HIDDEN_TYPE_FIELD_NAME.' in the request form POST data. got none!');
        }

        $usedStepUid = $baseFormData[BaseType::USED_STEP_HIDDEN_TYPE_FIELD_NAME];

        if (!is_string($usedStepUid) || '' === $usedStepUid) {
            throw new \InvalidArgumentException('expected to find a string representing a step uid in the request form POST data. got none!');
        }

        return $workFlow->getOneStepByUid($usedStepUid);
    }
}
