<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Storing;

use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use Webmozart\Assert\Assert;

/**
 * Class TargetReference.
 *
 * helper object to uniquely find and identify the (DB) records of stored data (answers, workflow, etc.) for a unique
 * entity/object (target).
 *
 * NOTE: an entity/objected @see TemplateTargetableInterface
 * targeted by a certain @see TemplateInterface can only have one set of stored data for that template.
 * i.e. this unique reference can be found more than once as a record of stored data in DB, but only once per template
 */
final class TargetReference
{
    /**
     * unique code of the entity/object's class in the application. usually short class name, unique within app's domain.
     */
    private readonly string $code;
    /**
     * unique id of the entity/object in the application's storage. e.g primary key, uid, ...
     */
    private readonly string $identifier;

    private function __construct(string $code, string $identifier)
    {
        Assert::notEmpty($code);
        Assert::notEmpty($identifier);

        $this->code = $code;
        $this->identifier = $identifier;
    }

    public static function fromCodeAndIdentifier(string $code, string $identifier): self
    {
        return new self($code, $identifier);
    }

    public static function fromTargetable(TemplateTargetableInterface $targetable): self
    {
        return new self($targetable::getTargetCode(), $targetable->getTargetIdentifier());
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function equals(self $storableTargetReference): bool
    {
        return $this->getCode() === $storableTargetReference->getCode()
            && $this->getIdentifier() === $storableTargetReference->getIdentifier();
    }
}
