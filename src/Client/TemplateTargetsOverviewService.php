<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\Access\ClientTemplateTargetFormAccess;
use KUL\FormBundle\Client\GlobalScore\Rendering\GlobalScoreQuestionResultViewsBuilder;
use KUL\FormBundle\Client\Overview\TargetAccessStatusView;
use KUL\FormBundle\Client\Response\DoAccessDeniedResponse;
use KUL\FormBundle\Client\Response\DoOverviewResponse\DoRenderTargetAccessStatusViewResponse;
use KUL\FormBundle\Client\Route\AbstractClientRouteData;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Utility\Role;
use Symfony\Component\HttpFoundation\Request;

class TemplateTargetsOverviewService extends AbstractTemplateTargetService
{
    /**
     * returns JSON serializable view object with basic status info:
     * - accessType: always present. one of @see ClientCombinedAccessObject::getAccessTypeDefinitions().
     * if access is granted (accessType !== 0), it also will contain:
     * - url: url to the form rendering page for the accessing role.
     * - accessingRole: role accessing the form.
     * - accessingRoleSlug.
     * - workflowStatus: label of current open step.
     * - button: button data & markup (path (same as url above), label, CSS classes).
     */
    public function getBasicTargetAccessStatusViewResponse(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        ?\DateTimeInterface $calculatedStepsCheckDate = null,
        ?\DateTimeInterface $secondaryCalculatedStepsCheckDate = null,
    ): DoRenderTargetAccessStatusViewResponse {
        return $this->getTargetAccessStatusViewResponse(
            $request,
            $template,
            $clientWithTargetAccess,
            false,
            false,
            false,
            $calculatedStepsCheckDate,
            $secondaryCalculatedStepsCheckDate,
        );
    }

    /**
     * returns JSON serializable view object with all available & allowed status info:
     * - accessType: always present. one of @see ClientCombinedAccessObject::getAccessTypeDefinitions().
     * if access is granted (accessType !== 0), it also will contain:
     * - url: url to the form rendering page for the accessing role.
     * - accessingRole: role accessing the form.
     * - accessingRoleSlug.
     * - workflowStatus: label of current open step.
     * - button: button data & markup (path (same as url above), label, CSS classes).
     * - lastSubmitted data:
     *      - lastSubmittedByFullName: full name of last submitter. null if form was never submitted.
     *      - lastSubmittedByUserId: implementing app's user identifier of last submitter. null if form was never submitted.
     *      - lastSubmittedOn: date of last submit. null if form was never submitted.
     * - revertActions: reset & rollback actions data (urls, labels, CSS, etc.) that the accessing (user) role is allowed to execute.
     * - globalScoreQuestionResultViews: result (answer) data of any global score question (question label, uid,
     * type. score result (if any), etc.) to which (user) role has access in the current open step. it contains both
     * global score questions that have an answer and those that not have an answer yet (data is marked with 'hasBeenSubmitted').
     *
     * note: lastSubmitted data, revertActions and even more so globalScoreQuestionResultViews can be heavy on performance.
     * if you don't need that data, use @see self::getBasicTargetAccessStatusViewResponse()
     * or @see self::getTargetAccessStatusViewResponse() to add some of these data.
     */
    public function getFullTargetAccessStatusViewResponse(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        ?\DateTimeInterface $calculatedStepsCheckDate = null,
        ?\DateTimeInterface $secondaryCalculatedStepsCheckDate = null,
    ): DoRenderTargetAccessStatusViewResponse {
        return $this->getTargetAccessStatusViewResponse(
            $request,
            $template,
            $clientWithTargetAccess,
            true,
            true,
            true,
            $calculatedStepsCheckDate,
            $secondaryCalculatedStepsCheckDate,
        );
    }

    /**
     * returns JSON serializable view object with all available & allowed status info:
     * - accessType: always present. one of @see ClientCombinedAccessObject::getAccessTypeDefinitions().
     * if access is granted (accessType !== 0), it also will contain:
     * - url: url to the form rendering page for the accessing role.
     * - accessingRole: role accessing the form.
     * - accessingRoleSlug.
     * - workflowStatus: label of current open step.
     * - button: button data & markup (path (same as url above), label, CSS classes).
     *
     * optionally, if access is granted, you can opt to add:
     * - lastSubmitted data:
     *      - lastSubmittedByFullName: full name of last submitter. null if form was never submitted.
     *      - lastSubmittedByUserId: implementing app's user identifier of last submitter. null if form was never submitted.
     *      - lastSubmittedOn: date of last submit. null if form was never submitted.
     * - revertActions: reset & rollback actions data (urls, labels, CSS, etc.) that the accessing (user) role is allowed to execute.
     * - globalScoreQuestionResultViews: result (answer) data of any global score question (question label, uid,
     * type. score result (if any), etc.) to which (user) role has access in the current open step. it contains both
     * global score questions that have an answer and those that not have an answer yet (data is marked with 'hasBeenSubmitted').
     *
     * note: lastSubmitted data, revertActions and even more so globalScoreQuestionResultViews can be heavy on performance.
     * if you don't need that data, use @see self::getBasicTargetAccessStatusViewResponse()
     * or add only what you need.
     */
    public function getTargetAccessStatusViewResponse(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        bool $addAllowedLastSubmittedInfo,
        bool $addAllowedApiRevertActions,
        bool $addAllowedGlobalScoreQuestionResultViews,
        ?\DateTimeInterface $calculatedStepsCheckDate = null,
        ?\DateTimeInterface $secondaryCalculatedStepsCheckDate = null,
    ): DoRenderTargetAccessStatusViewResponse {
        $this->assertBuildRequirements($request, $template, $calculatedStepsCheckDate);

        // check basic access for client to template. return with 'no-access' response if no basic access
        $doAccessDeniedResponse = $this->getOneOrNullPreliminaryDoAccessDeniedResponse($template, $clientWithTargetAccess);
        if ($doAccessDeniedResponse instanceof DoAccessDeniedResponse) {
            return new DoRenderTargetAccessStatusViewResponse(TargetAccessStatusView::createForNoAccessGranted());
        }

        $requestRoleSlug = $request->attributes->get('roleSlug');
        // if access is requested to be checked for a specific accessingRole only (via roleSlug in request), then
        // only check for that role, ignoring all other roles for which client might have access. if no specific
        // accessingRole is given, check with all roles of client. this specific role check will allow to use role
        // switching in overview and list targets with only access for that role. while if no specific role given,
        // then a list of targets with access for at least one (hierarchical) role of client can be shown
        if (is_string($requestRoleSlug) && '' !== $requestRoleSlug) {
            $formAccess = $this->getNullOrFormAccessForSingleClientAccessingRoleInRequest(
                $request,
                $template,
                $clientWithTargetAccess,
                $calculatedStepsCheckDate,
                $secondaryCalculatedStepsCheckDate
            );
        } else {
            $formAccess = $this->getNullOrFormAccessForAllClientAccessingRoles(
                $request,
                $template,
                $clientWithTargetAccess,
                $calculatedStepsCheckDate,
                $secondaryCalculatedStepsCheckDate
            );
        }

        if (!$formAccess instanceof ClientTemplateTargetFormAccess) {
            return new DoRenderTargetAccessStatusViewResponse(TargetAccessStatusView::createForNoAccessGranted());
        }

        $primaryAccessObject = $formAccess->getPrimaryAccess();
        $clientRouteDataSet = $this->clientRoutesBuilder->getClientRouteDataSetForTemplate($formAccess->getTemplate());
        $storingViewUrl = $this->clientRoutesBuilder->generateUrlFromRequestForClientRouteDataAndParameters(
            $request,
            $clientRouteDataSet->getOneForStoringView(),
            [
                AbstractClientRouteData::ROUTE_VARIABLE_ROLE_SLUG => $primaryAccessObject->getSlugifiedRole(),
            ]
        );

        $revertActions = $addAllowedApiRevertActions ?
            $this->getRevertActionsBuilder()->buildAllowedApiRevertActionsWithJsonResponseForClientAccess($primaryAccessObject, $request->getLocale()) :
            null;

        $globalScoreQuestionResultViews = $addAllowedGlobalScoreQuestionResultViews ?
            GlobalScoreQuestionResultViewsBuilder::buildAllowedGlobalScoreResultViewsForClientAccess($primaryAccessObject, $request->getLocale()) :
            null;

        return new DoRenderTargetAccessStatusViewResponse(
            TargetAccessStatusView::createForAccessGrantedByClientAccessObject(
                $primaryAccessObject,
                $storingViewUrl,
                $addAllowedLastSubmittedInfo,
                $revertActions,
                $globalScoreQuestionResultViews,
                $request->getLocale(),
            )
        );
    }

    /**
     * if you don't have a request object to pass to one of the 'get---Response' methods in this service that return
     * a @see DoRenderTargetAccessStatusViewResponse - or you simply don't want such a response and want to get
     * the @see TargetAccessStatusView returned directly - you can use this method. which returns the same
     * TargetAccessStatusView object as is returned as the sole object on each of the DoRenderTargetAccessStatusViewResponse
     * methods in this service.
     *
     * however, this means you might have to add custom parameters that are specific to your app for the url that is
     * build inside the returned  @see TargetAccessStatusView :
     * method arguments $additionalFormStoringViewRouteParameters & $additionalFormStoringViewUriParameters:
     * if access granted, the formBundle will build the url to the form page (storing_view route: renders the template-form
     * for that target for that user and role to fill in or consult) for the given target. if no access granted, no
     * url is build (since user has no access to thta template-form for that target with specified role (if given) or any
     * role (if not specified).(@see StoringViewClientRouteData )
     * the url is build based on:
     *  - the Template
     *  - the target (@see ClientWithTargetAccessInterface::getTarget() )
     *  - accessingRole (either the one specified in $specificAccessingRole or the most relevant/primary
     *    one in (@see ClientWithTargetAccessInterface::getAccessingRoles() if no specified role is given)
     * this building of the url will use the 'storing_view' route you specified for the @see TemplateInterface::getTargetingType()
     * in the kul_form.yml to determine which url to use.
     * but that route might - and almost certainly will - contain parameters that are specific to your app.
     * e.g. in SCONE that might be an internshipId, in EQA an labId, etc.
     * so you need to add those parameters to the $additionalFormStoringViewRouteParameters argument.
     * if you don't add those parameters that are required & defined in your app ass additional app-specific parameters
     * by the storing_view route (kul_form.yml -> targeting_type -> <template targeting type> -> storing_view, Symfony
     * will throw an exception for a missing route parmater (unless it is nullable or has a default off course).
     * if needed, you can also add uri parameters in the $additionalFormStoringViewRouteParameters.
     *
     * note: the api revert action urls that might be build (if requested to be build and access granted for the user-role
     * as configured in kul_form -> revert -> roles) are independend of your app. hence no additional parameters are needed nor allowed.
     */
    public function buildTargetAccessStatusViewWithAdditionalStoringViewRouteParameters(
        string $templateId,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        array $additionalFormStoringViewRouteParameters,
        array $additionalFormStoringViewUriParameters,
        bool $addAllowedLastSubmittedInfo,
        bool $addAllowedApiRevertActions,
        bool $addAllowedGlobalScoreQuestionResultViews,
        string $locale,
        ?\DateTimeInterface $calculatedStepsCheckDate = null,
        ?\DateTimeInterface $secondaryCalculatedStepsCheckDate = null,
        ?Role $specificAccessingRole = null,
    ): TargetAccessStatusView {
        $template = $this->templateRepository->getOneById($templateId);
        $this->assertTemplateBaseBuildRequirements($template, $calculatedStepsCheckDate);

        // check basic access for client to template. return with 'no-access' statusView if no basic access
        $doAccessDeniedResponse = $this->getOneOrNullPreliminaryDoAccessDeniedResponse($template, $clientWithTargetAccess);
        if ($doAccessDeniedResponse instanceof DoAccessDeniedResponse) {
            return TargetAccessStatusView::createForNoAccessGranted();
        }

        // if access is requested to be checked for a specific accessingRole only (via roleSlug in request), then
        // only check for that role, ignoring all other roles for which client might have access. if no specific
        // accessingRole is given, check with all roles of client. this specific role check will allow to use role
        // switching in overview and list targets with only access for that role. while if no specific role given,
        // then a list of targets with access for at least one (hierarchical) role of client can be shown
        if ($specificAccessingRole instanceof Role) {
            $formAccess = $clientWithTargetAccess->getAccessingRoles()->hasRole($specificAccessingRole) ?
                $this->getNullOrFormAccessForSingleClientAccessingRole($template, $clientWithTargetAccess, $specificAccessingRole, $calculatedStepsCheckDate, $secondaryCalculatedStepsCheckDate) :
                null;
        } else {
            $formAccess = $this->getNullOrFormAccessForMostRelevantRoleBasedOnAllClientAccessingRoles(
                $template,
                $clientWithTargetAccess,
                $calculatedStepsCheckDate,
                $secondaryCalculatedStepsCheckDate,
                null
            );
        }

        if (!$formAccess instanceof ClientTemplateTargetFormAccess) {
            return TargetAccessStatusView::createForNoAccessGranted();
        }

        $primaryAccessObject = $formAccess->getPrimaryAccess();
        $clientRouteDataSet = $this->clientRoutesBuilder->getClientRouteDataSetForTemplate($formAccess->getTemplate());
        $storingViewUrl = $this->clientRoutesBuilder->generateUrlFromRequestParametersForClientRouteDataAndParameters(
            $additionalFormStoringViewRouteParameters,
            $additionalFormStoringViewUriParameters,
            $clientRouteDataSet->getOneForStoringView(),
            [
                AbstractClientRouteData::ROUTE_VARIABLE_TEMPLATE_ID => $template->getId(),
                AbstractClientRouteData::ROUTE_VARIABLE_TARGET_CODE => $clientWithTargetAccess->getTarget()::getTargetCode(),
                AbstractClientRouteData::ROUTE_VARIABLE_TARGET_ID => $clientWithTargetAccess->getTarget()->getTargetIdentifier(),
                AbstractClientRouteData::ROUTE_VARIABLE_ROLE_SLUG => $primaryAccessObject->getSlugifiedRole(),
            ]
        );

        $revertActions = $addAllowedApiRevertActions ?
            $this->getRevertActionsBuilder()->buildAllowedApiRevertActionsWithJsonResponseForClientAccess($primaryAccessObject, $locale) :
            null;

        $globalScoreQuestionResultViews = $addAllowedGlobalScoreQuestionResultViews ?
            GlobalScoreQuestionResultViewsBuilder::buildAllowedGlobalScoreResultViewsForClientAccess($primaryAccessObject, $locale) :
            null;

        return TargetAccessStatusView::createForAccessGrantedByClientAccessObject(
            $primaryAccessObject,
            $storingViewUrl,
            $addAllowedLastSubmittedInfo,
            $revertActions,
            $globalScoreQuestionResultViews,
            $locale,
        );
    }
}
