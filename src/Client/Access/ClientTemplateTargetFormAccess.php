<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Access;

use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Utility\Role;

class ClientTemplateTargetFormAccess
{
    private readonly ClientCombinedAccessObjectCollection $accessObjects;
    private readonly ClientCombinedAccessObject $primaryAccessObject;

    /**
     * ClientTemplateTargetFormAccess constructor.
     */
    public function __construct(ClientCombinedAccessObjectCollection $accessObjects, ?Role $preferredRole)
    {
        if ($accessObjects->isEmpty()) {
            throw new \InvalidArgumentException('the collection of access objects does not contain any elements');
        }

        $validAccessObjects = $accessObjects->getWhereGrantsAccess();

        if ($validAccessObjects->isEmpty()) {
            throw new \InvalidArgumentException('the collection of access objects does not contain any valid elements that grant access');
        }

        if ($preferredRole instanceof Role && $validAccessObjects->hasOneForAccessingRole($preferredRole)) {
            $primaryAccess = $validAccessObjects->getOneForAccessingRole($preferredRole);
        } else {
            /** @var ClientCombinedAccessObject $primaryAccess */
            $primaryAccess = $validAccessObjects->first();
        }

        $commonOpenStepUid = $primaryAccess->getOpenFlowStep()->getUid();
        /** @var ClientCombinedAccessObject $validAccessObject */
        foreach ($validAccessObjects as $validAccessObject) {
            if (!($validAccessObject->getOpenFlowStep()->getUid() === $commonOpenStepUid)) {
                throw new \InvalidArgumentException('expected all access objects to share same open step with uid id ['.$commonOpenStepUid.']. But got at least one access object with different open step ['.$validAccessObject->getOpenFlowStep()->getUid().']');
            }
        }

        $this->accessObjects = $validAccessObjects;
        $this->primaryAccessObject = $primaryAccess;
    }

    /**
     * @return ClientCombinedAccessObjectCollection
     */
    public function getAccessObjects()
    {
        return $this->accessObjects;
    }

    /**
     * @return ClientCombinedAccessObject
     */
    public function getPrimaryAccess()
    {
        return $this->primaryAccessObject;
    }

    /**
     * get the common formEntity (same for each accessObject in the accessObjects).
     *
     * @return TemplateInterface
     */
    public function getTemplate()
    {
        return $this->getPrimaryAccess()->getTemplate();
    }
}
