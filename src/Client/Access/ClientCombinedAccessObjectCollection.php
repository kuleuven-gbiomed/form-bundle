<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Access;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Client\Storing\TargetReference;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * @extends ArrayCollection<array-key,ClientCombinedAccessObject>
 */
class ClientCombinedAccessObjectCollection extends ArrayCollection
{
    /**
     * @param ClientCombinedAccessObject[] $elements
     */
    public function __construct($elements = [])
    {
        $this->validateElements($elements);

        parent::__construct($elements);
    }

    /**
     * @param ClientCombinedAccessObject[] $elements
     */
    private function validateElements(array $elements): void
    {
        if (0 === count($elements) || 1 === count($elements)) {
            return;
        }

        /** @var ClientCombinedAccessObject $firstAccessObject */
        $firstAccessObject = reset($elements);

        $commonTemplateId = $firstAccessObject->getTemplate()->getId();
        $commonUserId = $firstAccessObject->getClientWithTargetAccess()->getFormUser()->getIdentifier();
        $commonTargetRef = TargetReference::fromTargetable($firstAccessObject->getClientWithTargetAccess()->getTarget());

        /** @var ClientCombinedAccessObject $element */
        foreach ($elements as $element) {
            if (!($element->getTemplate()->getId() === $commonTemplateId)) {
                throw new \InvalidArgumentException(self::class.'expects all elements to share same template. Expected template id ['.$commonTemplateId.']. Got ['.$element->getTemplate()->getId().']');
            }

            if (!($element->getClientWithTargetAccess()->getFormUser()->getIdentifier() === $commonUserId)) {
                throw new \InvalidArgumentException(self::class.'expects all elements to share same user. Expected user id ['.$commonUserId.']. Got ['.$element->getClientWithTargetAccess()->getFormUser()->getIdentifier().']');
            }

            if (!TargetReference::fromTargetable($element->getClientWithTargetAccess()->getTarget())->equals($commonTargetRef)) {
                $targetRef = TargetReference::fromTargetable($element->getClientWithTargetAccess()->getTarget());
                throw new \InvalidArgumentException(self::class.'expects all elements to share same target. Expected target [targetId: '.$commonTargetRef->getIdentifier().' | targetCode: '.$commonTargetRef->getCode().']. Got target [targetId: '.$targetRef->getIdentifier().' | targetCode: '.$targetRef->getCode().']');
            }
        }
    }

    public function getWhereGrantsAccess(): self
    {
        return $this->filter(fn (ClientCombinedAccessObject $object) => $object->grantsAccess());
    }

    public function getSortedByTemplateHierarchicalRoles(
        TemplateInterface $template,
    ): self {
        $sortedAccessObjects = [];

        /** @var Role $role */
        foreach ($template->getHierarchicalRolesThatCouldParticipate() as $role) {
            if ($this->hasOneForAccessingRole($role)) {
                $sortedAccessObjects[] = $this->getOneForAccessingRole($role);
            }
        }

        return new self($sortedAccessObjects);
    }

    public function getAccessingRoles(): RoleCollection
    {
        $roles = [];

        foreach ($this as $object) {
            $roles[] = $object->getAccessingRole();
        }

        return new RoleCollection($roles);
    }

    public function getForAccessingRole(Role $role): self
    {
        return $this->filter(fn (ClientCombinedAccessObject $object) => $object->getAccessingRole()->equals($role));
    }

    public function hasOneForAccessingRole(Role $role): bool
    {
        return 1 === $this->getForAccessingRole($role)->count();
    }

    public function getOneForAccessingRole(Role $role): ClientCombinedAccessObject
    {
        $result = $this->getForAccessingRole($role);

        $first = $result->first();
        if ($first instanceof ClientCombinedAccessObject && 1 === $result->count()) {
            return $first;
        }

        throw new \BadMethodCallException('Wrong amount of ClientCombinedAccessObjects found. Expected to find exactly one,'." found: {$result->count()}.");
    }
}
