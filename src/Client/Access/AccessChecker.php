<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Access;

use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * An access checker creates a ClientCombinedAccessObject for a user with a role at a certain point in time.
 */
interface AccessChecker
{
    /**
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    public function getClientCombinedAccessObject(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        Role $accessingRole,
        ?StoredTemplateTargetInterface $storedTemplateTarget,
    ): ClientCombinedAccessObject;

    public function findOpenStepByDates(
        TemplateInterface $template,
        ?StoredTemplateTargetInterface $storedTemplateTarget,
    ): ?StepInterface;
}
