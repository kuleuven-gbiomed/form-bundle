<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Access;

class InvalidAccessArgumentException extends \Exception
{
}
