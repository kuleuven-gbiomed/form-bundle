<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Access;

use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Contract\FormUserInterface;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleSet;

/**
 * Class ClientWithTargetAccess.
 *
 * person with roles that have access to a given @see TemplateTargetableInterface to be used for checking and retrieving
 * access to a certain @see TemplateInterface. intermediate object between your app and the bundle that allows to
 * pre define with what role or roles access for a target object/entity in a template must be checked
 *
 * IMPORTANT: this object is intentionally flexible. you can build and pass in any user with roles that don't even belong
 * to that user if you want. e.g. the accessing roles and preferred role could be dynamically assigned to that user.
 * this also allows admins to test templates with all kinds of roles and user before publishing them.
 * the bundle does NOT care if the user actually exists in your app. nor does it care if the role(s) belong to
 * that user! the only requirement is that the preferred role in that same set.
 *
 * if you don't pass accessing roles in, it basically means the form User (client) has no roles with access to the
 * target and this form-bundle will treat it as such. this form-bundle won't throw exceptions for missing roles, but it
 * will not be able to create access, the same way as when you pass in a role which has no access to questions.
 *
 * from the moment you pass in this user and role(s), that user and role(s) are what will be used for anything from
 * checking access, getting questions to storing questions and logging what was stored by that user and role on
 * given target.
 */
class ClientWithTargetAccess implements ClientWithTargetAccessInterface
{
    /**
     * ClientWithTargetAccess constructor.
     */
    private function __construct(
        readonly private FormUserInterface $formUser,
        readonly private TemplateTargetableInterface $target,
        readonly private RoleSet $accessingRoles,
        readonly private \DateTimeImmutable $currentDate,
    ) {
    }

    public static function fromTargetAndFormUserWithAccessingRoles(
        FormUserInterface $user,
        TemplateTargetableInterface $target,
        RoleSet $accessingRoles,
        \DateTimeImmutable $currentDate,
    ): self {
        return new self($user, $target, $accessingRoles, $currentDate);
    }

    public function getFormUser(): FormUserInterface
    {
        return $this->formUser;
    }

    public function getAccessingRoles(): RoleSet
    {
        return $this->accessingRoles;
    }

    public function getTarget(): TemplateTargetableInterface
    {
        return $this->target;
    }

    public function getCurrentDate(): \DateTimeImmutable
    {
        return $this->currentDate;
    }
}
