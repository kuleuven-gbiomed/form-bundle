<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Access;

use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Access checker for templates that only have fixed steps.
 *
 * I'm not even sure this is needed. But I created it to get rid of all the if structures and
 * nullable parameters in David's ClientCombinedAccessObject.
 */
final readonly class FixedStepOnlyAccessChecker implements AccessChecker
{
    /**
     * Create a new access checker.
     */
    public function __construct(private RoleCollection $availableAdminRoles, private \DateTimeImmutable $now)
    {
    }

    /**
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    public function getClientCombinedAccessObject(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        Role $accessingRole,
        ?StoredTemplateTargetInterface $storedTemplateTarget,
    ): ClientCombinedAccessObject {
        ClientCombinedAccessObject::guardTemplateWithCurrentVersion($template);

        $currentStep = $this->findOpenStepByDates($template, $storedTemplateTarget);

        return new ClientCombinedAccessObject(
            $template,
            $clientWithTargetAccess,
            $accessingRole,
            $this->availableAdminRoles,
            $this->now,
            $currentStep,
            null,
            null,
            $storedTemplateTarget
        );
    }

    private function guardFixedStepsOnly(WorkFlow $workflow): void
    {
        foreach ($workflow->getSteps() as $step) {
            if ($step instanceof StepWithFixedDateStart) {
                continue;
            }
            throw new \Exception('Steps are not fixed only');
        }
    }

    /**
     * @throws MissingVersionException
     * @throws InvalidAccessArgumentException
     */
    public function findOpenStepByDates(
        TemplateInterface $template,
        ?StoredTemplateTargetInterface $storedTemplateTarget,
    ): ?StepInterface {
        if (!$storedTemplateTarget instanceof StoredTemplateTargetInterface) {
            return $this->findOpenStepForTargetWithoutSubmissions(
                $template
            );
        }

        $workflow = $template->getCurrentVersion()->getWorkFlow();

        // Since there is a storedTemplateTarget, the last submission on it happened in a certain step.
        // We only check for open steps that are equal or higher than that last step.
        // We need to take care of this because there is a thing called 'prematurely started steps', steps
        // that started before their start date, because everything for the previous step had been done.

        // If the last used step on the (submitted) storedTemplateTarget doesn't exist anymore, an exception will be thrown.
        // TODO: David suggested to prevent admins from removing existing steps for templates that are in use.
        // Another suggestion was to deal with this edge case when republishing the form.

        $this->guardFixedStepsOnly($workflow);
        /** @var StepWithFixedDateStart $lastSubmissionStep */
        $lastSubmissionStep = $storedTemplateTarget->getLastUsedStep();

        // A step is considered open as long it's end date didn't pass. We don't look at premature starts here,
        // because a step can stay open after some user submits.

        if (!$lastSubmissionStep->isEndedByDate($this->now)) {
            return $lastSubmissionStep;
        }

        // The step of the last submission isn't open any more. Are there steps left?
        if ($workflow->getSteps()->doesFlowFinishWithStep($lastSubmissionStep)) {
            return null;
        }

        /** @var StepWithFixedDateStartCollection $followingSteps */
        $followingSteps = $workflow->getSteps()->getSortedStepsThatFollowStep($lastSubmissionStep);

        return $followingSteps->hasOpenStep($this->now) ?
            $followingSteps->getOpenStep($this->now) :
            null;
    }

    /**
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    private function findOpenStepForTargetWithoutSubmissions(TemplateInterface $template): ?StepWithFixedDateStart
    {
        ClientCombinedAccessObject::guardAllStepsHaveFixedPeriod($template);

        /** @var StepWithFixedDateStartCollection $steps */
        $steps = $template->getCurrentVersion()->getWorkFlow()->getSteps();

        return $steps->hasOpenStep($this->now) ?
            $steps->getOpenStep($this->now) :
            null;
    }
}
