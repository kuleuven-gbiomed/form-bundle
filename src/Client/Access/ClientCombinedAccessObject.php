<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Access;

use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Client\Slugifier\RoleSlugifier;
use KUL\FormBundle\Client\Storing\TargetReference;
use KUL\FormBundle\Client\Submission\SubmissionCollection;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Contract\FormUserInterface;
use KUL\FormBundle\Domain\Factory\DateTimeImmutableFactory;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Class ClientCombinedAccessObject.
 *
 * client (user-person) with (assumed) access to target by at one or more (accessing) roles,
 * requests access to the template (*) for that target and for given specified (accessing) role,
 * based on the template's workflow state (the 'open' step) for that target as derivable from given date(s),
 * taking in account the submit history of client-target-role inside that 'open' step in case that step has submit limits.
 *
 * *access to the template = there is at least one question that this client with specified role has
 * write and/or read (= "combined') access to in the current 'open' step for that target.
 * if none, no access is granted.
 */
final class ClientCombinedAccessObject
{
    /** @var int */
    public const ACCESS_TYPE_NONE = 0;
    /** @var int */
    public const ACCESS_TYPE_READ_ONLY = 1;
    /** @var int */
    public const ACCESS_TYPE_WRITE_FIRST_SUBMIT = 2;
    /** @var int */
    public const ACCESS_TYPE_WRITE_EDIT = 3;

    private readonly TemplateInterface $template;
    private readonly ?StoredTemplateTargetInterface $storedTemplateTarget;
    private readonly \DateTimeImmutable $checkDate;
    private ?\DateTimeImmutable $calculatedStepReferenceDate = null;
    private ?\DateTimeImmutable $secondaryCalculatedStepReferenceDate = null;
    private readonly ClientWithTargetAccessInterface $clientWithTargetAccess;
    private readonly Role $accessingRole;
    private ?RoleCollection $templateParticipatingAdminRoles = null;

    private readonly ?StepInterface $openFlowStep;
    private bool $isRoleWriteAccessAuthorized;
    private bool $isRoleReadAccessAuthorized;
    private ?RoleSlugifier $roleSlugifier = null;

    /**
     * ClientCombinedAccessObject constructor.
     *
     * FIXME: Get rid of secondCalculatedStepReferenceDate
     *
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    public function __construct(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        Role $accessingRole,
        private readonly RoleCollection $availableAdminRoles,
        \DateTimeInterface $checkDate,
        ?StepInterface $openFlowStep,
        ?\DateTimeInterface $calculatedStepReferenceDate,
        ?\DateTimeInterface $secondCalculatedStepReferenceDate,
        ?StoredTemplateTargetInterface $storedTemplateTarget,
    ) {
        self::validateArguments(
            $template,
            $clientWithTargetAccess,
            $accessingRole,
            $calculatedStepReferenceDate,
            $storedTemplateTarget,
            $openFlowStep
        );

        $this->template = $template;
        $this->clientWithTargetAccess = $clientWithTargetAccess;
        $this->checkDate = DateTimeImmutableFactory::createFromDate($checkDate);

        // validate argument will have asserted the need & presence of a calculated date
        if ($calculatedStepReferenceDate instanceof \DateTimeInterface) {
            $this->calculatedStepReferenceDate = DateTimeImmutableFactory::createFromDate($calculatedStepReferenceDate);
        }

        if ($secondCalculatedStepReferenceDate instanceof \DateTimeInterface) {
            $this->secondaryCalculatedStepReferenceDate = DateTimeImmutableFactory::createFromDate($secondCalculatedStepReferenceDate);
        }

        $this->accessingRole = $accessingRole;
        $this->storedTemplateTarget = $storedTemplateTarget;
        $this->openFlowStep = $openFlowStep;

        $this->setAccess();
    }

    /**
     * @return int[]
     */
    public static function getAccessTypeDefinitions(): array
    {
        return [
            self::ACCESS_TYPE_NONE,
            self::ACCESS_TYPE_READ_ONLY,
            self::ACCESS_TYPE_WRITE_FIRST_SUBMIT,
            self::ACCESS_TYPE_WRITE_EDIT,
        ];
    }

    /**
     * helper method to avoid having to the performance-heavy re-building for an open step (if any).
     *
     * if no open step in the original instance (and thus no access granted in that original),
     * then no access will be able to be granted on duplicate neither
     *
     * if original instance has an open step and it has granted access, then that does not mean access will be able to
     * be granted on duplicate for this different role! and vice versa: if original instance has an open step but
     * it has no access granted, then that does not necessarily means access will not be granted on duplicate for this
     * different role!
     *
     * this method only exists for performance reasons when having to build multiple access objects for a user accessing
     * with multiple roles. the open step is always the same if same dates are used, so that checks can be skipped here
     *
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    public function withDifferentAccessingRole(Role $accessingRole): self
    {
        if ($this->getAccessingRole()->equals($accessingRole)) {
            throw new \InvalidArgumentException('given accessing role must be different from original accessing role,  but they are the same role ['.$accessingRole->getName().']');
        }

        self::validateClientAccessingRole($this->getClientWithTargetAccess(), $accessingRole);

        return new self(
            $this->template,
            $this->clientWithTargetAccess,
            $accessingRole,
            $this->availableAdminRoles,
            $this->checkDate,
            $this->openFlowStep,
            $this->calculatedStepReferenceDate,
            $this->secondaryCalculatedStepReferenceDate,
            $this->storedTemplateTarget
        );
    }

    public function grantsAccess(): bool
    {
        return $this->isWriteAccessAuthorized() || $this->isReadAccessAuthorized();
    }

    public function isWriteAccessAuthorized(): bool
    {
        return $this->isRoleWriteAccessAuthorized;
    }

    public function isReadAccessAuthorized(): bool
    {
        return $this->isRoleReadAccessAuthorized;
    }

    /**
     * intentionally private scope. should neither check for nor call for an open step @see getOpenFlowStep
     * on this object if not first checked if access is granted (which is based, set and relies on having
     * an open step) @see grantsAccess().
     */
    private function hasOpenFlowStep(): bool
    {
        return $this->openFlowStep instanceof StepInterface;
    }

    public function getOpenFlowStep(): StepInterface
    {
        if (null === $this->openFlowStep) {
            throw new \BadMethodCallException('no openFlowStep found. which also means no access granted');
        }

        return $this->openFlowStep;
    }

    public function getOpenStepStartDate(): \DateTimeImmutable
    {
        if (null === $this->openFlowStep) {
            throw new \BadMethodCallException('no openFlowStep found. which also means no access granted');
        }

        if ($this->openFlowStep instanceof StepWithFixedDateStart) {
            return $this->openFlowStep->getPeriod()->getFixedStartDate();
        }

        if ($this->openFlowStep instanceof StepWithCalculatedDateStart) {
            return $this->calculateStartDate($this->openFlowStep);
        }

        throw new \DomainException('unknown type of step '.$this->openFlowStep::class);
    }

    private function calculateStartDate(StepWithCalculatedDateStart $step): \DateTimeImmutable
    {
        // if not using fixed dates, then step will use calculated dates
        // if step uses calculated dates, then the given getCalculatedStepReferenceDate will exist
        $calCheckDate = $this->getCalculatedStepReferenceDate();
        if (!$this->hasSecondaryCalculatedStepReferenceDate()) {
            return $step->getPeriod()->getCalculatedStartDate($calCheckDate);
        }

        $secondCalCheckDate = $this->getSecondaryCalculatedStepReferenceDate();
        // the first step in a workflow is always calculated via the primary calculated check date.
        // while if a secondary calculated check date is used, then that second date will be used to calculate start of
        // all steps except to calculate start of the FIRST step!
        if (!$this->getTemplate()->getCurrentVersion()->getWorkFlow()->getSteps()->doesFlowStartWithStep($step)) {
            return $step->getPeriod()->getCalculatedStartDate($secondCalCheckDate);
        }

        return $step->getPeriod()->getCalculatedStartDate($calCheckDate);
    }

    public function getOpenStepEndDate(): \DateTimeImmutable
    {
        if (null === $this->openFlowStep) {
            throw new \BadMethodCallException('no openFlowStep found. which also means no access granted');
        }

        if ($this->openFlowStep instanceof StepWithFixedDateStart) {
            return $this->openFlowStep->getPeriod()->getFixedEndDate();
        }

        // if not using fixed dates, then step will use calculated dates
        if (!$this->openFlowStep instanceof StepWithCalculatedDateStart) {
            throw new \DomainException('unknown type of step '.$this->openFlowStep::class);
        }

        // if step uses calculated dates, then the given getCalculatedStepReferenceDate will exist
        $calCheckDate = $this->getCalculatedStepReferenceDate();
        if (!$this->hasSecondaryCalculatedStepReferenceDate()) {
            return $this->openFlowStep->getPeriod()->getCalculatedEndDate($calCheckDate);
        }

        // if a secondary calculated check date is used, then that second date will be used to calculate end of
        // all steps, including end of FIRST step!
        $secondCalCheckDate = $this->getSecondaryCalculatedStepReferenceDate();

        return $this->openFlowStep->getPeriod()->getCalculatedEndDate($secondCalCheckDate);
    }

    public function getOpenStepNumberedLabel(?string $locale): string
    {
        // only use locale if valid
        $label = is_string($locale) && !('' === $locale) ?
            $this->getOpenFlowStep()->getLabel($locale) : $this->getOpenFlowStep()->getInfo()->getLocalizedLabel()->getFallback();

        return ((string) $this->getOpenStepPositionNumber()).' - '.$label;
    }

    public function getOpenStepPositionNumber(): int
    {
        return $this->getTemplate()->getCurrentVersion()->getWorkFlow()->getSteps()->getPositionNumber($this->getOpenFlowStep());
    }

    public function getClientWithTargetAccess(): ClientWithTargetAccessInterface
    {
        return $this->clientWithTargetAccess;
    }

    public function getUser(): FormUserInterface
    {
        return $this->getClientWithTargetAccess()->getFormUser();
    }

    public function getTemplate(): TemplateInterface
    {
        return $this->template;
    }

    /**
     * @throws MissingVersionException
     */
    public function getCurrentVersionFormList(): FormList
    {
        return $this->getTemplate()->getCurrentVersion()->getFormList();
    }

    /**
     * @throws MissingVersionException
     */
    public function getCurrentVersionWorkflow(): WorkFlow
    {
        return $this->getTemplate()->getCurrentVersion()->getWorkFlow();
    }

    public function getAccessingRole(): Role
    {
        return $this->accessingRole;
    }

    public function hasStoredTemplateTarget(): bool
    {
        return $this->storedTemplateTarget instanceof StoredTemplateTargetInterface;
    }

    public function getStoredTemplateTarget(): StoredTemplateTargetInterface
    {
        if (!$this->storedTemplateTarget instanceof StoredTemplateTargetInterface) {
            throw new \BadMethodCallException('no storedTemplateTarget found');
        }

        return $this->storedTemplateTarget;
    }

    public function getTargetSubmissions(): SubmissionCollection
    {
        if (!$this->hasStoredTemplateTarget()) {
            return new SubmissionCollection();
        }

        return $this->getStoredTemplateTarget()->getSubmissions();
    }

    public function getTargetTemporarySubmissions(): SubmissionCollection
    {
        if (!$this->hasStoredTemplateTarget()) {
            return new SubmissionCollection();
        }

        return $this->getStoredTemplateTarget()->getTemporarySubmissions();
    }

    public function hasCalculatedStepReferenceDate(): bool
    {
        return $this->calculatedStepReferenceDate instanceof \DateTimeImmutable;
    }

    public function getCalculatedStepReferenceDate(): \DateTimeImmutable
    {
        if (!$this->calculatedStepReferenceDate instanceof \DateTimeImmutable) {
            throw new \BadMethodCallException('no calculatedStepReferenceDate found');
        }

        return $this->calculatedStepReferenceDate;
    }

    public function getCheckDate(): \DateTimeImmutable
    {
        return $this->checkDate;
    }

    public function hasSecondaryCalculatedStepReferenceDate(): bool
    {
        return $this->secondaryCalculatedStepReferenceDate instanceof \DateTimeImmutable;
    }

    public function getSecondaryCalculatedStepReferenceDate(): \DateTimeImmutable
    {
        if (!$this->secondaryCalculatedStepReferenceDate instanceof \DateTimeImmutable) {
            throw new \BadMethodCallException('no secondaryCalculatedStepReferenceDate found');
        }

        return $this->secondaryCalculatedStepReferenceDate;
    }

    public function getTemplateParticipatingAdminRoles(): RoleCollection
    {
        if (!$this->templateParticipatingAdminRoles instanceof RoleCollection) {
            $this->templateParticipatingAdminRoles = $this->getTemplate()
                ->getRolesThatParticipateInCurrentVersion()
                ->getDuplicatesByRoles($this->availableAdminRoles);
        }

        return $this->templateParticipatingAdminRoles;
    }

    public function isAccessingAsAdmin(): bool
    {
        return $this->getTemplateParticipatingAdminRoles()->hasRole($this->getAccessingRole());
    }

    private function getTemplateParticipatableRoleSlugifier(): RoleSlugifier
    {
        if (!$this->roleSlugifier instanceof RoleSlugifier) {
            $this->roleSlugifier = $this->getTemplate()->getParticipatableRoleSlugifier();
        }

        return $this->roleSlugifier;
    }

    public function getSlugifiedRole(): string
    {
        return $this->getTemplateParticipatableRoleSlugifier()->getSlugForRole($this->getAccessingRole()->getName());
    }

    public function getAccessType(): int
    {
        // if no access whatsoever
        if (!$this->grantsAccess()) {
            return self::ACCESS_TYPE_NONE;
        }

        // if user has no write access (but has general access granted), then user has read access only
        if (!$this->isWriteAccessAuthorized()) {
            return self::ACCESS_TYPE_READ_ONLY;
        }

        // if user has write access, check if edit or first-time-submit in open step
        if (!$this->clientHasSubmittedBeforeInOpenStepWithAccessingRole()) {
            return self::ACCESS_TYPE_WRITE_FIRST_SUBMIT;
        }

        return self::ACCESS_TYPE_WRITE_EDIT;
    }

    private function setAccess(): void
    {
        $this->isRoleWriteAccessAuthorized = false;
        $this->isRoleReadAccessAuthorized = false;

        // no open step means not even possible to check access
        if (!$this->hasOpenFlowStep()) {
            return;
        }

        // is template published (asserted on construct) but not active anymore (deactivated), no client gets access
        if (!$this->getTemplate()->isActive()) {
            return;
        }

        // if role of client will never have at least one question to at least read at some point in time (= some step
        // in workflow) (at this point, construct has asserted given accessing role to be one of client's accessing roles)
        // if role will never have even the slightest access, no point in checking further
        if (!$this->getTemplate()->allowsRoleToParticipateInCurrentVersion($this->getAccessingRole())) {
            return;
        }

        $currentFormList = $this->getTemplate()->getCurrentVersion()->getFormList();
        $reachableQuestions = $currentFormList->getFlattenedReachableParentalInputNodes();

        // check kind of access: write access (give automatic read access) or read only access or no access
        // client with accessing role has write access to at least one question in open step
        if ($reachableQuestions->hasWritableForRoleInStep($this->getAccessingRole(), $this->getOpenFlowStep())) {
            // write gives automatic read access
            $this->isRoleReadAccessAuthorized = true;
            // if step only allows one successful submit per client-role-version
            // and user has used up that single submit -> deny write access anyway and only keep read access
            $this->isRoleWriteAccessAuthorized = $this->isRoleWriteAccessAuthorizedByStepSubmitLimitationsAndClientHistory();

            return;
        }

        // if no write access, client may have read only access (
        if ($reachableQuestions->hasReadableForRoleInStep($this->getAccessingRole(), $this->getOpenFlowStep())) {
            $this->isRoleReadAccessAuthorized = true;

            return;
        }

        // if no read access to questions, client may have read access to readOnlyView nodes and at least one of those
        // can be rendered based on currently existing submitted formValues (e.g. radar chart has plottable answers to render)
        // note: checking for 'renderability' on top of access is mainly for UX so that in case there is only one (or more)
        // read-only-view (radar chart) node(s) to which this user has access in this (e.g. first) step for this role,
        // but that user has no access at all to any other questions in that same step, and no one has yet submitted any
        // answers yet, then no answers can be rendered/plotted for those read-only-view (radar chart) nodes, which means
        // those read-only-view (radar chart) nodes won't be shown and the user ends up seeing a blank page, or a page
        // with at best an empty (nested) category box to which those read-only-view (radar chart) nodes belong. so
        // basically, if user has only access to read-only-view (radar chart) nodes but none can be rendered, we deny
        // access all together. this is rather edge case, and even then probably only likely to happen in a first step.
        // And it would be a rather odd - however still valid - choice to add a chart node like this by the admin building that form.
        $reachableReadOnlyViewNodes = $currentFormList->getFlattenedReachableReadOnlyViewNodes();
        $formValues = $this->hasStoredTemplateTarget() ? $this->getStoredTemplateTarget()->getSubmittedFormValues() : [];
        if ($reachableReadOnlyViewNodes->hasAccessibleAndRenderableFor($this->getOpenFlowStep(), $this->getAccessingRole(), $formValues)) {
            $this->isRoleReadAccessAuthorized = true;
        }

        // an open step found but client has not the least read (renderable) access to any questions/readOnlyView nodes in that open step
    }

    private function clientHasSubmittedBeforeInOpenStepWithAccessingRole(): bool
    {
        return $this->hasStoredTemplateTarget()
            && !$this->getStoredTemplateTarget()->getSubmissions()->getForUserStepRoleVersion(
                $this->getUser(),
                $this->getOpenFlowStep(), // will deliberately fail if no open step, should not call unless open step set
                $this->getAccessingRole(),
                $this->getTemplate()->getCurrentVersionNumber()
            )->isEmpty();
    }

    private function isRoleWriteAccessAuthorizedByStepSubmitLimitationsAndClientHistory(): bool
    {
        if (!$this->getOpenFlowStep()->limitsEachUniqueUserToOnlyOneSubmitUnderRole($this->getAccessingRole())) {
            return true;
        }

        if (!$this->clientHasSubmittedBeforeInOpenStepWithAccessingRole()) {
            return true;
        }

        return false;
    }

    /**
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    private static function validateArguments(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        Role $accessingRole,
        ?\DateTimeInterface $calculatedStepReferenceDate,
        ?StoredTemplateTargetInterface $storedTemplateTarget,
        ?StepInterface $openFlowStep,
    ): void {
        self::guardTemplateWithCurrentVersion($template);
        self::validateOpenStep($template, $openFlowStep);
        self::validateClientAccessingRole($clientWithTargetAccess, $accessingRole);
        // FIXME: The fact that $calculatedStepReferenceDate can be null in some cases, but can't be null in other cases.
        // indicates that we need to split this class into multiple classes.
        self::validateCalculatedStepDate($template, $calculatedStepReferenceDate);
        self::validateStoredTemplateTarget($template, $clientWithTargetAccess, $storedTemplateTarget);
    }

    /**
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    private static function validateOpenStep(TemplateInterface $template, ?StepInterface $openFlowStep): void
    {
        if (!$openFlowStep instanceof StepInterface) {
            return;
        }

        if (!$template->getCurrentVersion()->getWorkFlow()->hasOneStepByUid($openFlowStep->getUid())) {
            throw new InvalidAccessArgumentException('open step  ['.$openFlowStep->getUid().'] is not a step in the workflow of template ['.$template->getId().']');
        }
    }

    /**
     * @throws InvalidAccessArgumentException
     */
    private static function validateClientAccessingRole(
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        Role $accessingRole,
    ): void {
        if (!$clientWithTargetAccess->getAccessingRoles()->hasRole($accessingRole)) {
            throw new InvalidAccessArgumentException('accessingRole  ['.$accessingRole->getName().'] is not one of the given clients target accessing roles ['.implode('|', $clientWithTargetAccess->getAccessingRoles()->getNamesArray()).']');
        }
    }

    /**
     * @throws InvalidAccessArgumentException
     */
    private static function validateStoredTemplateTarget(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        ?StoredTemplateTargetInterface $storedTemplateTarget,
    ): void {
        if (!$storedTemplateTarget instanceof StoredTemplateTargetInterface) {
            return;
        }

        if (!($storedTemplateTarget->getTemplate()->getId() === $template->getId())) {
            throw new InvalidAccessArgumentException('template with id  ['.$storedTemplateTarget->getId().'] in given storedTemplateTarget with ['.$storedTemplateTarget->getId().'] does not match the given template with id '.$template->getId());
        }

        $clientTargetRef = TargetReference::fromTargetable($clientWithTargetAccess->getTarget());
        $storedTargetRef = $storedTemplateTarget->getTargetReference();

        if (!$clientTargetRef->equals($storedTargetRef)) {
            throw new InvalidAccessArgumentException('targetReference [code: '.$clientTargetRef->getCode().' id: '.$clientTargetRef->getIdentifier().'] in clientWithTargetAccess does not match targetReference.[code:'.$storedTargetRef->getCode().' id: '.$storedTargetRef->getIdentifier().'] in storedTemplateTarget.');
        }
    }

    /**
     * @throws InvalidAccessArgumentException
     */
    public static function guardTemplateWithCurrentVersion(TemplateInterface $template): void
    {
        // if template has a current version it will have valid workflow with at least one step and valid
        // questions, etc.. if template has no current version, either it was never published to a current version
        // or someone tampered with DB. in either case, client not only can not have access, but this object should
        // not even be constructed since it relies solely on a current version.
        if (!$template->hasCurrentVersion()) {
            throw new InvalidAccessArgumentException('template  ['.$template->getId().'] has no published current version (yet), making any client access impossible');
        }
    }

    /**
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    private static function validateCalculatedStepDate(
        TemplateInterface $template,
        ?\DateTimeInterface $calculatedStepReferenceDate,
    ): void {
        if (!$calculatedStepReferenceDate instanceof \DateTimeInterface) {
            self::guardAllStepsHaveFixedPeriod($template);
        }
    }

    /**
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    public static function guardAllStepsHaveFixedPeriod(TemplateInterface $template): void
    {
        $workflow = $template->getCurrentVersion()->getWorkFlow();

        if ($workflow->hasStepsOfCalculatedPeriodType()) {
            throw new InvalidAccessArgumentException('workflow in template  ['.$template->getId().'] makes use of at least one stepWithCalculatedStartDate but no calculatedStepReferenceDate is given to check for an open stepWithCalculatedStartDate!');
        }
    }

    public function getSavedFiles(): SavedFileCollection
    {
        return $this->storedTemplateTarget instanceof StoredTemplateTargetInterface ? $this->storedTemplateTarget->getSavedFiles() : new SavedFileCollection();
    }
}
