<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Access;

use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Access checker to be used for templates that might contain calculated steps.
 */
final class GeneralAccessChecker implements AccessChecker
{
    private readonly \DateTimeInterface $referenceDateForTargetWithSubmissions;

    /**
     * Create a new access checker.
     *
     * The reference date is used to calculate the periods of the calculated steps.
     *
     * FIXME: Get rid of the hacky $referenceDateForTargetWithoutSubmissions parameter.
     * I guess this hack was introduced in SCONE to quickly fiddle with the opening date of the first step.
     */
    public function __construct(
        private readonly RoleCollection $availableAdminRoles,
        private readonly \DateTimeImmutable $now,
        private readonly \DateTimeInterface $referenceDateForTargetWithoutSubmissions,
        ?\DateTimeInterface $referenceDateForTargetWithSubmissions,
    ) {
        $this->referenceDateForTargetWithSubmissions = $referenceDateForTargetWithSubmissions instanceof \DateTimeInterface
            ? $referenceDateForTargetWithSubmissions : $referenceDateForTargetWithoutSubmissions;
    }

    /**
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    public function getClientCombinedAccessObject(
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        Role $accessingRole,
        ?StoredTemplateTargetInterface $storedTemplateTarget,
    ): ClientCombinedAccessObject {
        ClientCombinedAccessObject::guardTemplateWithCurrentVersion($template);

        $currentStep = $this->findOpenStepByDates($template, $storedTemplateTarget);

        return new ClientCombinedAccessObject(
            $template,
            $clientWithTargetAccess,
            $accessingRole,
            $this->availableAdminRoles,
            $this->now,
            $currentStep,
            $this->referenceDateForTargetWithoutSubmissions,
            $this->referenceDateForTargetWithSubmissions,
            $storedTemplateTarget
        );
    }

    /**
     * @throws MissingVersionException
     * @throws InvalidAccessArgumentException
     */
    public function findOpenStepByDates(
        TemplateInterface $template,
        ?StoredTemplateTargetInterface $storedTemplateTarget,
    ): ?StepInterface {
        if (!$storedTemplateTarget instanceof StoredTemplateTargetInterface) {
            return $this->findOpenStepForTargetWithoutSubmissions(
                $template
            );
        }

        $workflow = $template->getCurrentVersion()->getWorkFlow();

        // Since there is a storedTemplateTarget, the last submission on it happened in a certain step.
        // We only check for open steps that are equal or higher than that last step.
        // We need to take care of this because there is a thing called 'prematurely started steps', steps
        // that started before their start date, because everything for the previous step had been done.

        // If the last used step on the (submitted) storedTemplateTarget doesn't exist anymore, an exception will be thrown.
        // TODO: David suggested to prevent admins from removing existing steps for templates that are in use.
        // Another suggestion was to deal with this edge case when republishing the form.

        $lastSubmissionStep = $storedTemplateTarget->getLastUsedStep();

        // A step is considered open as long it's end date didn't pass. We don't look at premature starts here,
        // because a step can stay open after some user submits.

        if ($lastSubmissionStep instanceof StepWithFixedDateStart && !$lastSubmissionStep->isEndedByDate($this->now)) {
            return $lastSubmissionStep;
        }

        if ($lastSubmissionStep instanceof StepWithCalculatedDateStart
            && !$lastSubmissionStep->isEndedByDate($this->now, $this->referenceDateForTargetWithSubmissions)) {
            return $lastSubmissionStep;
        }

        // The step of the last submission isn't open any more. Are there steps left?
        if ($workflow->getSteps()->doesFlowFinishWithStep($lastSubmissionStep)) {
            return null;
        }

        // Find one of the following steps that is open: there should be only one open step at the time.
        // If all steps are fixed or all steps are calculated, the uniqueness of a step has been taken care of
        // during the configuration of the template.
        // In case of mixed steps, overlap can happen. In that case the fixed step has priority
        // (see David's comments in MixedCalculatedAndFixedStepCollection::findOneStepThatIsOpenByFixedDateOrCalculatedDates).
        // Because of prematurely started steps, we need to make sure that we take a step after the step of the
        // last submission.

        /** @var MixedCalculatedAndFixedStepCollection $followingSteps */
        $followingSteps = $workflow->getSteps()->getSortedStepsThatFollowStep($lastSubmissionStep);

        return $followingSteps->findOpenStep(
            $this->now,
            $this->referenceDateForTargetWithSubmissions,
            $this->referenceDateForTargetWithSubmissions
        );
    }

    /**
     * @throws InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    private function findOpenStepForTargetWithoutSubmissions(TemplateInterface $template): ?StepInterface
    {
        $workFlow = $template->getCurrentVersion()->getWorkFlow();

        return $workFlow->hasOpenStep($this->now, $this->referenceDateForTargetWithoutSubmissions, $this->referenceDateForTargetWithSubmissions) ?
            $workFlow->getOpenStep($this->now, $this->referenceDateForTargetWithoutSubmissions, $this->referenceDateForTargetWithSubmissions) :
            null;
    }
}
