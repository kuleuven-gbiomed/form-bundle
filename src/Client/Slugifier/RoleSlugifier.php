<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Slugifier;

use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use Webmozart\Assert\Assert;

/**
 * Class RoleSlugifier.
 *
 * using known roles in urls as slugs
 */
class RoleSlugifier
{
    /**
     * @var string[] list of roles, each keyed by their slug
     *
     * @psalm-var array<string, string>
     */
    private array $slugRoles = [];

    /**
     * @var string[] list of slugs, each keyed by their role
     *
     * @psalm-var array<string, string>
     */
    private array $roleSlugs = [];

    /**
     * @param string[] $roles
     */
    public function __construct(array $roles)
    {
        foreach ($roles as $role) {
            Assert::stringNotEmpty($role, 'role must be a non-empty string. got '.gettype($role));

            // strtolower is faster than mb_strtolower and no need for mb_strtolower here
            $slug = mb_strtolower(str_ireplace(['ROLE_', 'ROLE', '_'], ['', '', '-'], $role));

            $this->roleSlugs[$role] = $slug;
            $this->slugRoles[$slug] = $role;
        }
    }

    public static function fromRoleCollection(RoleCollection $roleCollection): self
    {
        return new self($roleCollection->getNamesArray());
    }

    /**
     * get slugs, each keyed by their role.
     *
     * @return string[]
     *
     * @psalm-return array<string, string>
     */
    public function getRoleSlugs(): array
    {
        return $this->roleSlugs;
    }

    /**
     * get roles, each keyed by their slug.
     *
     * @return string[]
     *
     * @psalm-return array<string, string>
     */
    public function getSlugRoles(): array
    {
        return $this->slugRoles;
    }

    public function hasRoleForSlug(string $slug): bool
    {
        if ('' === $slug) {
            return false;
        }

        return isset($this->slugRoles[$slug]);
    }

    /**
     * @throws \BadMethodCallException
     */
    public function getRoleForSlug(string $slug): string
    {
        if (!$this->hasRoleForSlug($slug)) {
            throw new \BadMethodCallException('no role found for slug ['.$slug.']');
        }

        return $this->slugRoles[$slug];
    }

    public function hasSlugForRole(string $role): bool
    {
        if ('' === $role) {
            return false;
        }

        return isset($this->roleSlugs[$role]);
    }

    /**
     * @throws \BadMethodCallException
     */
    public function getSlugForRole(string $role): string
    {
        if (!$this->hasSlugForRole($role)) {
            throw new \BadMethodCallException('no slug found for role ['.$role.']');
        }

        return $this->roleSlugs[$role];
    }
}
