<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\Access\ClientTemplateTargetFormAccess;
use KUL\FormBundle\Client\Manage\FormManager;
use KUL\FormBundle\Client\Manage\FormType\BaseType;
use KUL\FormBundle\Client\Manage\RequestFormDataConverter;
use KUL\FormBundle\Client\Messaging\FormSubmitted;
use KUL\FormBundle\Client\Response\AbstractDoResponse;
use KUL\FormBundle\Client\Response\DoAccessDeniedResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoAutoTemporarySaveFailedWithFormValidationErrorsResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoAutoTemporarySaveHasNotDetectedChangesToSaveResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoInvalidStepStoringAttemptWithAccessInNewStepResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoRenderViewWithParametersAndFormValidationErrorsResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoRenderViewWithParametersForStoringResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoStoringAutoTemporarySaveSuccessResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoStoringSaveSuccessRedirectAwayResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoStoringSaveSuccessRedirectBackToStoringViewResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoStoringTemporarySaveSuccessRedirectAwayResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoStoringTemporarySaveSuccessRedirectBackToStoringViewResponse;
use KUL\FormBundle\Client\Revert\Rendering\RevertActions;
use KUL\FormBundle\Client\Route\ClientRouteDataCollection;
use KUL\FormBundle\Client\Route\ClientRoutesBuilder;
use KUL\FormBundle\Client\Util\File\FileUrls;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Entity\StoredTemplateTarget;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use KUL\FormBundle\Entity\SubmissionRepository;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Webmozart\Assert\Assert;

/**
 * Class TemplateTargetStoringService.
 *
 * process entry point for end users to store (submitting, temporary saving) forms based on
 * the combination of @see TemplateInterface - target entity/object targeted by @see TemplateInterface - accessing user (roles)
 *
 * entry point for rendering (questions in) form views & subsequently storing (submitting, temporary saving) answers
 * by clients (end-users) having (read/write) access to a selection of questions in a workflow for a specific user-role
 * as configured by a @see TemplateInterface that targets a certain entity (the subject of the questioning) for which the client is
 * authorized with at least one of his/her relevant user-roles to view and/or answer one or more questions.
 */
class TemplateTargetStoringService extends AbstractTemplateTargetService
{
    public function __construct(
        StoredTemplateTargetRepository $storedTemplateTargetRepository,
        SubmissionRepository $submissionRepository,
        ClientRoutesBuilder $clientRoutesBuilder,
        FormManager $formManager,
        string $defaultLocale,
        array $availableAdminRoles,
        int $autoSaveInterval,
        int $idleCheckInterval,
        TemplateRepository $templateRepository,
    ) {
        parent::__construct(
            $storedTemplateTargetRepository,
            $submissionRepository,
            $clientRoutesBuilder,
            $formManager,
            $defaultLocale,
            $availableAdminRoles,
            $autoSaveInterval,
            $idleCheckInterval,
            $templateRepository
        );
    }

    /**
     * build a view with a selection of questions from the given template and their answers (if any yet) to which
     * given client has access in the currently open step in the workflow as determined by given check date(s),
     * with submittable form fields for those question that given client has write access to (can give answers)
     * or only displaying the answer (if any) if given client only has read access.
     *
     * NOTE: param secondaryCalculatedStepsCheckDate: if set then this date will be used to check for open step by
     * calculating the start and end dates ( {@see StepCalculatedPeriod::isStartedByDate()}) for all steps in the workflow,
     * except to calculate the start date for the FIRST step in the workflow. for that start date, the param
     * calculatedStepsCheckDate will be used. this will avoid certain overlap by using calculatedStepsCheckDate to
     * calculate start date for every step while using secondaryCalculatedStepsCheckDate for all end dates
     *
     * $forceDisableAutoTemporarySave: used for forcefully disable autosave and overwriting the autosave enabling/diabling settings
     * as configured on the Template via
     *
     * @see TemplateInterface::isAutoTemporarySavingEnabled()  overall enabling/disabling of auto-save (default is FALSE; not enabled)
     * @see TemplateInterface::shouldDisableAutoTemporarySavingForRole() further disables auto-save for a specific accessing role
     * but sometimes the auto-save might have to be disabled. the primary use-case is when a user is impersonating another user
     * and thus is accessing the form with that impersonated role.
     * if set to true, the embeddable twigs in this form-bundle that you use in your implementing app, will not trigger the autosave
     *
     * @param TemplateInterface               $template                          required
     * @param ClientWithTargetAccessInterface $clientWithTargetAccess            required
     * @param \DateTimeInterface|null         $calculatedStepsReferenceDate      optional/required if template uses calculated steps, no default and no default should be used
     * @param \DateTimeInterface|null         $secondaryCalculatedStepsCheckDate optional
     *
     * @throws Access\InvalidAccessArgumentException
     * @throws Route\InvalidClientRouteException
     * @throws MissingVersionException
     */
    public function getStoringViewResponse(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        ?\DateTimeInterface $calculatedStepsReferenceDate,
        ?\DateTimeInterface $secondaryCalculatedStepsCheckDate,
        bool $forceDisableAutoTemporarySave = false,
    ): AbstractDoResponse {
        // assert base requirements like template published, routes known and required date set (if needed),
        // throw exceptions since we can not continue without this stuff
        $this->assertBuildRequirements($request, $template, $calculatedStepsReferenceDate);

        // check basic access for client to template. no exceptions thrown; it's not necessarily client's fault
        // return access denied response and let app choose what to do
        $doAccessDeniedResponse = $this->getOneOrNullPreliminaryDoAccessDeniedResponse($template, $clientWithTargetAccess);
        if ($doAccessDeniedResponse instanceof DoAccessDeniedResponse) {
            return $doAccessDeniedResponse;
        }

        $formAccess = $this->getNullOrFormAccessForAllClientAccessingRoles(
            $request,
            $template,
            $clientWithTargetAccess,
            $calculatedStepsReferenceDate,
            $secondaryCalculatedStepsCheckDate
        );

        if (!$formAccess instanceof ClientTemplateTargetFormAccess) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_HAS_NO_ROLES_WITH_ACCESS_IN_CURRENT_STEP);
        }

        // at this point, at least read access is granted. so if no write access granted, then only read access granted.
        // either because role has no write access on any questions in current open step, or unique client with role
        // used up its limited (singleSubmit) write access if configured as such on the current open step
        if (!$formAccess->getPrimaryAccess()->isWriteAccessAuthorized()) {
            return new DoRenderViewWithParametersForStoringResponse(
                $this->getRenderParametersForReadOnlyView($request, $formAccess)
            );
        }

        // if write access granted, build Symfony Form, based on primary access object
        $form = $this->getFormForWriteAccess($request, $formAccess->getPrimaryAccess());
        $renderParams = $this->getRenderParametersForCombinedWriteReadAccess($request, $formAccess, $form, $forceDisableAutoTemporarySave);

        return new DoRenderViewWithParametersForStoringResponse($renderParams);
    }

    /**
     * StepStoringAttempt: check if user tries to save in same step as he/she rendered the view:
     * if a user A had access loading the form (i.e. in the loaded or 'used' step, which was the current step at that time),
     * he/she will always have access in that same step as he/she loaded the form in (duh, kinda obvious: he/she is submitting
     * the form in that step). but if a next step (thus new current step) was started (by deadline or by another user's action)
     * between user A viewing the form and submitting it (saving, temporary saving or auto temporary saving) then we need to block
     * the saving operation. it might be possible that user might still have the same write access to the same set of questions
     * in the new current step, as he had in the step in which he loaded the form, but most of the time it won't be like that.
     * both Symfony forms and the actual storing of the answer the user submitted, will still only take the questions (thus answers)
     * in account to which the user had access when he rendered the form, but it is simply too risky and complex to deal with this.
     * it also is way too confusing when a user thought he submitted in step 1 because that was the step he loaded in,
     * but then sees he saved in step 2. so we block this with a message to reload. the downside is that reloading the page
     * means the user will lose the answers he entered in front end. but then again, thanks to autosave he won't lose much and
     * the autosave will also have warned him about this situation before he manually saved.
     * it is possible a user loaded the form in stap A (thus had write access) but when he submits and another user or deadline triggered
     * a next step, that the user has no more access in that next step. in that case, there will be no '$formAccess' (for any of his/here roles).
     * but reality has proven that admins always (never say always) built forms wherein once a user (i.e. a role) had write access in a step,
     * he/she always maintained at least minimal read access in all following steps. however, that is not a must and there could be use cases in which
     * a user-role is granted write access in a step but no access in the next step -> so we catch these edge cases
     * note: the @throws Access\InvalidAccessArgumentException.
     *
     * @throws Route\InvalidClientRouteException
     * @throws MissingVersionException
     *
     * $forceDisableAutoTemporarySave: used for forcefully disable autosave and overwriting the autosave enabling/diabling settings as configured on the Template
     * this argument is needed on this method becasue this method calls to reload the page if form contains validation errors
     * via a @see DoRenderViewWithParametersAndFormValidationErrorsResponse
     * in that case the front-end needs to know if autosave must be disabled
     * if set to true, the embeddable twigs in this form-bundle that you use in your implementing app, will not trigger the autosave
     */
    public function getStoringSaveResponse(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        ?\DateTimeInterface $calculatedStepsCheckDate,
        ?\DateTimeInterface $secondaryCalculatedStepsCheckDate,
        bool $forceDisableAutoTemporarySave = false,
    ): AbstractDoResponse {
        $this->assertBuildRequirements($request, $template, $calculatedStepsCheckDate);
        $doAccessDeniedResponse = $this->getOneOrNullPreliminaryDoAccessDeniedResponse($template, $clientWithTargetAccess);
        if ($doAccessDeniedResponse instanceof DoAccessDeniedResponse) {
            return $doAccessDeniedResponse;
        }

        // saving (submit or temp saving) is always done for a specified role, set in the request's url (HTML form action) as slugRole.
        $requestAccessingRole = $this->getOneOrNullValidAccessingRoleFromRequest($request, $template, $clientWithTargetAccess);
        if (!$requestAccessingRole instanceof Role) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::REQUEST_SLUG_ROLE_DOES_NOT_MATCH_TEMPLATE_CLIENT_TARGET_ACCESS);
        }

        // saving is done for a specified role, but if saving has validation errors, then we need to return same view
        // as was rendered before, this time with those errors. for that, we need all roles access so that we can
        // re-render same view, including with roles-switcher. so build for all and assert specified role right after
        $formAccess = $this->getNullOrFormAccessForAllClientAccessingRoles(
            $request,
            $template,
            $clientWithTargetAccess,
            $calculatedStepsCheckDate,
            $secondaryCalculatedStepsCheckDate
        );

        // no access at all for user or no more access in a new step if user had access when he/she loaded the form in
        // original step, but the form is now already in a next step, in which the user has no more access at all.
        if (!$formAccess instanceof ClientTemplateTargetFormAccess) {
            $doInvalidStepStoringAttemptWithoutAccessResponse = $this->findInvalidStepStoringAttemptByDates(
                $request,
                $template,
                $clientWithTargetAccess,
                $calculatedStepsCheckDate,
                $secondaryCalculatedStepsCheckDate
            );

            return $doInvalidStepStoringAttemptWithoutAccessResponse instanceof DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse ?
                $doInvalidStepStoringAttemptWithoutAccessResponse :
                new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_HAS_NO_ROLES_WITH_ACCESS_IN_CURRENT_STEP);
        }

        $primaryAccess = $formAccess->getPrimaryAccess();

        $doInvalidStepStoringAttemptResponse = $this->findInvalidStepStoringAttemptByCurrentAccess($request, $primaryAccess);
        // user had access when rendering the form, but the form has now moved to next step (by deadline or by a step-terminating action of another user)
        // the default way to handle this should be to 'hard' reload the page (=redirect to the form rendering url) and flash a message saying the system
        // could not allow the to submit the form he/she loaded due to step being ended.
        // this redirect means the user will lose whatever answers he was entering client-side, but there is no clean way around this:
        //  it's too risky to allow to submit answers in a step B, when those answers were entered in front-side while the form was loaded in step A.
        // (it's possible the user has overlapping write access in both steps, but it is far too complex to handle this)
        // the only other option would be to 'soft' reload the page with the front-end entered answer - similar to reloading with validation errors
        // - but that is kinda pointless: the user has most likely different access in the new current step, thus the reloading will show a different
        // set of questions (with input fields). the questions to which user had access in the step when first loading the form and still has write access to
        // in the new step , will then still have the front-end entered answers in the input fields, which seem handy. And theoretically, the user
        // could then submit that different set of question-answers in the new step. but that will require to replace the value hidden 'usedStepUid' field
        // with the uid of the new current step (easy to do), but far more importantly, it will be very confusing to the user to suddenly see a different
        // set of questions, of which a part show answers he entered in the related input fields. off course, 'hard' reloading the page (=redirect to
        // the form rendering url) will also show a different set of questions, so that won't make a difference, but at least
        // it will be clear to the user that the form is now in a different step in which he/she needs to restart answering, and that his/her
        // submit was broken off due to step being ended by the time he tried to submit.
        // all this is partially an edge case (user leaves a tab open with the form for a very very long time without (temp) saving),
        // and partially more occurring: multiple users loading form at same time, then submitting fast and in same timespan.
        // but the latter is also still a bit edge cass, since it means one all those users can end steps by submitting the answers
        // they have write access to in that step. usually, each user (role) has a different set of questions to answer in same step
        // and the step will only end if all users have submitted their steps. still, this is a necessary evil to handle, and there
        // is simply no clean way to handle it, apart from breaking off the submit and soft/hard reloading the form.
        // NOTE: when using auto-save, this is less of an issue, since the auto-save will catch such invalid step storing attempts
        // upon each auto-save and already send back an answer (pop-up) saying user has to reload the page, in which case the user
        // will most likely only use very little entered answers (e.g. if time between auto-save is 3 minutes (default))
        // DoInvalidStepStoringAttemptWithAccessInNewStepResponse is extended from access denied response, to make sure older apps (EQA) catch it
        if ($doInvalidStepStoringAttemptResponse instanceof DoInvalidStepStoringAttemptWithAccessInNewStepResponse) {
            return $doInvalidStepStoringAttemptResponse;
        }

        // if client has no access for specified role (never had or not anymore, does not matter: it means that client
        // has submitted form with another role than he/she rendered form in = hacking (or some major error))
        // this is a POST save attempt, so write access should be granted (a bit redundant)
        if (!$formAccess->getAccessObjects()->hasOneForAccessingRole($requestAccessingRole)
            || !$primaryAccess->getAccessingRole()->equals($requestAccessingRole)
            || !$primaryAccess->isWriteAccessAuthorized()) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP);
        }

        // if write access granted, build Symfony Form, based on primary access object
        $form = $this->getFormForWriteAccess($request, $primaryAccess);
        $this->formManager->handleRequest($form, $request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            // if user submitted or tempSaved with invalid form data, the response to do is EXACTLY the same rendering as
            // in the GET render action (@see getStoringViewResponse ), only difference is that the form will contain
            // validation errors that need to be rendered
            $renderParams = $this->getRenderParametersForCombinedWriteReadAccess($request, $formAccess, $form, $forceDisableAutoTemporarySave);

            return new DoRenderViewWithParametersAndFormValidationErrorsResponse($renderParams);
        }

        // if user submitted or tempSaved with valid form data (which means: for a submit the validation is always
        // done in full so all Constraints (both basic Symfony constraints like NotBlank(), Length() as well as
        // custom constraints like SubmittedFilesAmount(), etc.) are satisfied. while for temp saving, only basic
        // validation is done ( NotBlank(), Length(), SubmittedFilesAmount(), etc. Constraints are ignored, but e.g.
        // a string value temp saved for a number input field (NumberType) is not ignored! rarely, a temp save results
        // in an invalid form
        try {
            $isTempSave = $this->formManager->isTemporarySaveRequested($form);
            if ($isTempSave) {
                $this->updateStoredTemplateTargetWithTemporarySavedFormValues($form, $primaryAccess);
            } else {
                $this->updateStoredTemplateTargetWithSavedFormValues($form, $primaryAccess);
            }

            $usedStep = $primaryAccess->getOpenFlowStep();
            // check if a new step has opened and if the user-role has access to it:
            // the save action (submit) could trigger the next step to open prematurely (depending on the used step in which
            // was submitted was fulfilled by this submit action and the next step is configured to allow a premature start
            // (as in start before start date is reached). if no new step was opened, the user and his role with which he
            // saved (submitted) will still have at least read access in that same step in which he submitted. if a new step
            // is opened, it is possible that the user has no access in this next step with the role with which he saved
            // (submitted) in the used step. if the user has more than one role, but no access to the new step with the role
            // with which he submitted in the used step, he might have access with another role. but it is possible he has
            // no access at alle anymore in the new step. in reality, this is rather unlikely, since it is most sensible
            // to give users that could submit an answer to a question in one step, give at least read access to that answer
            // in all following steps. but it is perfectly legit and there are use cases to take away access in following steps.
            // if the save action was for a temporary save, then no new step will be triggered to open, since temporary answers
            // do not count for checking if a step was fulfilled (= all questions that could be answered by at least one role
            // in a step, got answered, either in this action and step or a previous one or step). but it is technically possible
            // however unlikely) that in the (milliseconds - seconds) time the temp save action was handled and this new access
            // check below is executed, that the step in which was temp saved just reached is deadline and thus a new step opened
            // by start date. that would be very unfortunate circumstances and has to happen in the seconds before the deadline,
            // but it is possible. it could also happen but that another user just executed a non-temp save action that finished the
            // step in which the first user temp saved in the same second or so. this is less unlikely, but it ahs been known to
            // happen in both cases
            $newFormAccess = $this->getNullOrFormAccessForAllClientAccessingRoles(
                $request,
                $template,
                $clientWithTargetAccess,
                $calculatedStepsCheckDate,
                $secondaryCalculatedStepsCheckDate
            );

            if ($isTempSave) {
                // contrary to a permanent save, a temporary save can not trigger a form to finish a step and go to the next step.
                // but it is possible and legitimate that in the split second between the temporary save being handled and the 'newFormAccess' was determined,
                // the used step was finished by deadline or by another user with a permanent save (submit) action.
                if (!$newFormAccess instanceof ClientTemplateTargetFormAccess) {
                    return new DoStoringTemporarySaveSuccessRedirectAwayResponse($usedStep);
                }

                $newPrimaryAccess = $newFormAccess->getPrimaryAccess();

                return new DoStoringTemporarySaveSuccessRedirectBackToStoringViewResponse(
                    $usedStep,
                    $this->getStoringViewUrlForAccessingRole(
                        $request,
                        $this->clientRoutesBuilder->getClientRouteDataSetForTemplate($newPrimaryAccess->getTemplate()),
                        $newFormAccess->getPrimaryAccess()
                    )
                );
            }

            // do redirect away from the form page if user-role has no longer access in the next step
            // or the step in which was submitted is configured to always redirect away if submit was successfull
            if (!$newFormAccess instanceof ClientTemplateTargetFormAccess || $usedStep->shouldRedirectAwayOnSubmitSuccess()) {
                /** @var StoredTemplateTarget $updatedStoredTargetAccess */
                $updatedStoredTargetAccess = $this->getOneOrNullStoredTemplateTarget($template, $clientWithTargetAccess);

                return new DoStoringSaveSuccessRedirectAwayResponse(
                    $usedStep,
                    $updatedStoredTargetAccess->getLastUsedStep(),
                    $primaryAccess->getAccessingRole(),
                    $request->getLocale()
                );
            }

            $newPrimaryAccess = $newFormAccess->getPrimaryAccess();

            // do reload form page with updated/same access and custom message
            return new DoStoringSaveSuccessRedirectBackToStoringViewResponse(
                $usedStep,
                $newPrimaryAccess->getOpenFlowStep(),
                $primaryAccess->getAccessingRole(),
                $request->getLocale(),
                $this->getStoringViewUrlForAccessingRole(
                    $request,
                    $this->clientRoutesBuilder->getClientRouteDataSetForTemplate($newPrimaryAccess->getTemplate()),
                    $newFormAccess->getPrimaryAccess()
                )
            );
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * IMPORTANT:
     * we can not use Symfony's $form->handleRequest($form, $request) @see FormInterface::handleRequest() for auto-save.
     * by default, FormInterface::handleRequest() will set non-present form fields to NULL when they are missing in
     * the submitted data.
     * i.e. handleRequest will add to the request POST data all missing fields (with blank values) that are build in
     * the FormType but are not present in the request POST data, before internally triggering a @see FormInterface::submit()
     * on the form.
     *
     * the parameter handling this is the second parameter 'clearMissing' in FormInterface::submit(), which
     * is by default TRUE. this parameter can not be overwritten when using FormInterface::handleRequest().
     * when such a missing field has e.g. a NotBlank() or NotNull() constraint, this would then trigger a validation error.
     * But when auto-temporary saving (also when manually temporary saving), all such custom validation constraints
     * in the Symfony formType are deliberately deactivated (by setting 'validation_groups' => false on the temp-save
     * button) so that users can temporary save required (cfr. Constraint NotBlank()) questions with blank values.
     * Only the basic (Symfony FormType) validation is active when (auto) temporary saving (e.g. no string values
     * allowed in a number input (FormType)).
     *
     * however, when auto temporary saving, the front-end will only add to the AJAX POST request data those questions
     * for which a changed answer is actually detected in front-end. Symfony form handling will silently add those
     * missing fields (questions) to its formData and bind them to a blank value. but we only want to deal with the
     * actually auto-saved questions! we can not simply ignore those blank values, because those could be deliberately
     * auto-save answers by users.
     *
     * To make sure those missing fields are NOT added to the formData (in the request), Symfony provides the option
     * to manually call $form->submit(submittedData, ['clearMissing']), with 'clearMissing' set to FALSE.
     * hence we manually submit the form.
     *
     * note that before calling  $form->submit(submittedData, ['clearMissing']), we need to make sure the submitted
     * POST data in the request is in the right nested form structure. this is not specific to auto-save. this is what
     * is done with all save action (including manual save or manual temporary save): see info on @see RequestFormDataConverter
     * this makes sure that the flat array of [questionId => answer] structure will be mapped back to the nested form structure
     * in the @see BaseType FormType.
     */
    public function getStoringAutoTemporarySaveResponse(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        ?\DateTimeInterface $calculatedStepsCheckDate,
        ?\DateTimeInterface $secondaryCalculatedStepsCheckDate,
    ): AbstractDoResponse {
        $this->assertBuildRequirements($request, $template, $calculatedStepsCheckDate);
        $doAccessDeniedResponse = $this->getOneOrNullPreliminaryDoAccessDeniedResponse($template, $clientWithTargetAccess);
        if ($doAccessDeniedResponse instanceof DoAccessDeniedResponse) {
            return $doAccessDeniedResponse;
        }

        // auto temporary saving (same as with manual (temp) saving) is always done for a specified role, set in the request's url (HTML form action) as slugRole.
        $requestAccessingRole = $this->getOneOrNullValidAccessingRoleFromRequest($request, $template, $clientWithTargetAccess);
        if (!$requestAccessingRole instanceof Role) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::REQUEST_SLUG_ROLE_DOES_NOT_MATCH_TEMPLATE_CLIENT_TARGET_ACCESS);
        }

        // get access for user with all; his roles
        $formAccess = $this->getNullOrFormAccessForAllClientAccessingRoles(
            $request,
            $template,
            $clientWithTargetAccess,
            $calculatedStepsCheckDate,
            $secondaryCalculatedStepsCheckDate
        );

        // no access at all for user or no more access in a new step if user had access when he/she loaded the form in
        // original step, but the form is now already in a next step, in which the user has no more access at all.
        if (!$formAccess instanceof ClientTemplateTargetFormAccess) {
            $doInvalidStepStoringAttemptWithoutAccessResponse = $this->findInvalidStepStoringAttemptByDates(
                $request,
                $template,
                $clientWithTargetAccess,
                $calculatedStepsCheckDate,
                $secondaryCalculatedStepsCheckDate
            );

            return $doInvalidStepStoringAttemptWithoutAccessResponse instanceof DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse ?
                $doInvalidStepStoringAttemptWithoutAccessResponse :
                new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_HAS_NO_ROLES_WITH_ACCESS_IN_CURRENT_STEP);
        }

        $primaryAccess = $formAccess->getPrimaryAccess();

        // check if user is auto-saving in the same step in which he loaded the form in. if the form is now in a next step,
        // then at his point we know the user still has access in that next step, but we cannot allow that user to auto-save
        // in a step other than the one the user loaded the form in (same as for other manual temporary or definitive save actions)
        $doInvalidStepStoringAttemptResponse = $this->findInvalidStepStoringAttemptByCurrentAccess($request, $primaryAccess);
        if ($doInvalidStepStoringAttemptResponse instanceof DoInvalidStepStoringAttemptWithAccessInNewStepResponse) {
            return $doInvalidStepStoringAttemptResponse;
        }

        // if client has no access for specified role (never had or not anymore, does not matter: it means that client
        // has submitted form with another role than he/she rendered form in = hacking (or some major error))
        // this is a POST save attempt, so write access should be granted (a bit redundant)
        if (!$formAccess->getAccessObjects()->hasOneForAccessingRole($requestAccessingRole)
            || !$primaryAccess->getAccessingRole()->equals($requestAccessingRole)
            || !$primaryAccess->isWriteAccessAuthorized()) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP);
        }

        if ($template->shouldDisableAutoTemporarySavingForRole($requestAccessingRole)) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::AUTO_SAVE_DISABLED_FOR_CLIENT_ACCESSING_ROLE);
        }

        // if write access granted, build Symfony Form, based on primary access object
        $form = $this->getFormForWriteAccess($request, $primaryAccess);

        // convert the submitted form data back to the nested form structure so Symfony can map it to the FormType
        $request = RequestFormDataConverter::convertPostRequestBaseFormDataToNestedFormData($form, $request);
        // submit the form manually, making sure the missing fields (not added by front-end to the request data) are NOT
        // added to the form data, by setting the second argument 'clearMissing' in Symfony's $form->submit to FALSE.
        /** @psalm-var array $submittedFormData keep psalm happy */
        $submittedFormData = $request->request->all(BaseType::FORM_NAME);
        $form->submit($submittedFormData, false);

        // auto temp save, like manual temp save, has only basic validation.
        // e.g. if question is required but user empty, it will deliberately pass. or if a number question field has a constraint
        // that allows only numbers lower than 20 and user entered 25, it will deliberately pass. but if users enters a
        // string e.g. 'blabla' into that same number question field, it will not pass = basic validation
        if (!$form->isSubmitted() || !$form->isValid()) {
            return new DoAutoTemporarySaveFailedWithFormValidationErrorsResponse();
        }

        if (!$this->formManager->isTemporarySaveRequested($form)) {
            throw new \DomainException('can not process auto-save because this is not an auto-save request: the button "'.BaseType::getTemporarySaveSubmitTypeFieldName().'" was not found in the submitted request data.');
        }

        if (!$this->detectsTemporarySavedChanges($primaryAccess, $form)) {
            return new DoAutoTemporarySaveHasNotDetectedChangesToSaveResponse();
        }

        try {
            $this->updateStoredTemplateTargetWithAutoTemporarySavedFormValues($form, $primaryAccess);

            return new DoStoringAutoTemporarySaveSuccessResponse();
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * form-values (both temp saved as final saved) are stored in DB as a single, one dimensional array, where each key is the
     * uuid of an answered question and the value represents the actual answer given which is represented by a
     * PHP base type (string (e.g for text and upload questions), float|int (e.g. for number questions), boolean, tec.)
     * for choice questions it somewhat different: the uid of an answerted choice question is stored as key in the form
     * values, like any other question and the value for that key is always boolean TRUE (indicating 'answered').
     * the selected options chosen as answers for that choice question are stored in the same form-values array, on the
     * same (one dimensional) level: each chosen answer option  (only one if choice is non-multiple) is stored with its
     * uid as key. the value is then either the boolean TRUE (indicating 'chosen'). Or - in case the option is of
     * type 'other option', allowing for a (required or optional) custom string action - it is a string or NULL.
     *
     * e.g. choice question uid = xxxxx. choice option answer uids are xxxxx-1, xxxxx-2, xxxx-3 (xxxx-3 being an 'other' option: allows for optional text input)
     * if choice question is answered, formValues will always contain (including when answered blank):
     * [xxxxx => true]
     * if choice question is answered with option xxxxx-1, formValues will contain:
     * [xxxxx => true, xxxxx-1 => true]
     * if multiple choice question is answered with option xxxxx-1 and xxxxx-1, formValues will contain:
     * [xxxxx => true, xxxxx-1 => true, xxxxx-2 => true]
     * if choice question is answered with option xxxxx-3, formValues will contain:
     * [xxxxx => true, xxxxx-3 => 'blabla']
     * or [xxxxx => true, xxxxx-3 => null] if additional text input was not provide by the use
     *
     * form-values (both temp saved as final saved), are single sources of truth. it means that when a user (temp) saves
     * an answer XXX to a question, that question with answer XXX is added to that form-values and if the question already
     * existed in that form-values with an answer YYY, then YYY will be overwritten.
     * the submitted auto temp saved questions answers, taken from the form via the form manager, is in the exact same syntax.
     * so all we need to do is loop over each question (key) in the submitted auto temp saved questions answers from the
     * submitted form, and compare those with the one in the stored temp saved form-values.
     *
     * IMPORTANT: note that stored temp saved form-values contains all questions-answers given until now,
     * meaning that it may - most likely will - also contain question (key) answer (values) pairs from
     * questions that the submitted form answer does NOT have, simply because the auto temp saving user has no write
     * access to those. this is important because it means the loop must be done over the submitted questions values
     * and compared to the ones in storages and not the other way around: any questions user has no write access to
     * and thus are not submitted in the form, are to be skipped in the comparison.
     *
     * note: one change is enough. even if it means only one answer is changed or an answer is given to a question that
     * was never answered before in a set of 1000 unchanged anwsers....
     */
    private function detectsTemporarySavedChanges(
        ClientCombinedAccessObject $primaryAccess,
        FormInterface $form,
    ): bool {
        $submittedAutoTempSavedFormValues = $this->formManager->getFlattenedFormValues($form);
        if (0 === count($submittedAutoTempSavedFormValues)) {
            // no changed detected: no questions submitted in this auto-save (unlikely. maybe if user tampers with JS/AJAX)
            return false;
        }

        if (!$primaryAccess->hasStoredTemplateTarget()) {
            // changed detected: nothing was ever stored, this auto temp save action is the first action ever for this template target
            return true;
        }

        $lastKnownTempSavedFormValues = $primaryAccess->getStoredTemplateTarget()->getTemporarySavedFormValues();

        $allInputNodes = $primaryAccess->getCurrentVersionFormList()->getFlattenedInputNodes();
        $choiceQuestions = $allInputNodes->getChoiceInputNodes();
        $choiceOptionNodes = $allInputNodes->getOptionInputNodes();

        $submittedAutoTempSavedFormChoiceQuestions = [];
        // compare the non-choice questions first. break and return as changes detected on any change found (first time temp-saved or edited answer)
        foreach ($submittedAutoTempSavedFormValues as $nodeUid => $value) {
            // skip the submitted auto temp saved choice answer option (values), those will be handled as part of their parental choice question later
            if ($choiceOptionNodes->hasOneByNodeUid($nodeUid)) {
                continue;
            }

            // collect the choice questions, because we'll need to check changes in which submitted auto temp saved choice answer option (values) were
            // added/removed/edited, compared to the last known temp saved choice answer option (values)
            if ($choiceQuestions->hasOneByNodeUid($nodeUid)) {
                $submittedAutoTempSavedFormChoiceQuestions[] = $choiceQuestions->getOneByNodeUid($nodeUid);
            }

            if (!array_key_exists($nodeUid, $lastKnownTempSavedFormValues)) {
                // changes detected: this auto save is first action to actually temp save this question (of any type)
                return true;
            }

            if ($value !== $lastKnownTempSavedFormValues[$nodeUid]) {
                // changes detected: this auto save provides an edited temp save answer to this question
                // in case of a choice question that was temp saved before, the value will not be edited but remain value
                // TRUE (to indicate it was temps saved). Its submitted auto temp saved choice answer option (values) are checked later.
                return true;
            }
        }

        // at this point we now the submittedAutoTempSavedFormValues contained no first time temp saved choice questions
        // but the submitted auto temp saved choice answer option (values) for each of those known choice temp saved  questions
        // might be changed (different answer option chosen in non-multiple choice), added (in case of multiple choice),
        // removed (in case of multiple choice or user chose to auto temp save it blank)  or have an edited 'other' choice
        // option answer text value (in case of an 'other' option with additional text input).
        foreach ($submittedAutoTempSavedFormChoiceQuestions as $choiceQuestion) {
            if (!$choiceQuestion instanceof ChoiceInputNode) {
                throw new \InvalidArgumentException('expected question to be of type choice. got '.$choiceQuestion::getType());
            }

            foreach ($choiceQuestion->getChildren()->toArray() as $optionInputNode) {
                $optionUid = $optionInputNode->getUid();
                $inLastKnownTempSavedValues = array_key_exists($optionUid, $lastKnownTempSavedFormValues);
                $inSubmittedAutoTempSavedValues = array_key_exists($optionUid, $submittedAutoTempSavedFormValues);

                // option not used then and not now -> skip this one
                if (false === $inSubmittedAutoTempSavedValues && false === $inLastKnownTempSavedValues) {
                    continue;
                }

                if ($inSubmittedAutoTempSavedValues !== $inLastKnownTempSavedValues) {
                    // changes detected:
                    // either option was auto temp saved now, but was not temp saved before -> thus new answer option chosen by user
                    // or option was not auto temp saved now, but was temp saved before -> thus this answer option was removed by user
                    return true;
                }

                // answer option was auto temp saved now and also was temp saved before (both vars TRUE at this point)
                // a chosen choice answer option is always stored/submitted with value TRUE to indicate this choice answer
                // option is chosen. but a choice answer option can also offer the user the option to provide additional
                // text input (optional or required) (i.e. the so called 'other' option).
                if ($submittedAutoTempSavedFormValues[$optionUid] !== $lastKnownTempSavedFormValues[$optionUid]) {
                    // changes detected: the value (i.e. string value of the additional input of this 'other' answer option was changed by user
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @throws MissingVersionException
     * @throws \JsonException
     */
    private function updateStoredTemplateTargetWithSavedFormValues(
        FormInterface $form,
        ClientCombinedAccessObject $accessObject,
    ): void {
        Assert::true($accessObject->isWriteAccessAuthorized());
        Assert::true($form->isSubmitted());
        Assert::true($form->isValid());
        Assert::false($this->formManager->isTemporarySaveRequested($form));

        // first save ever -> create new storage record for target-template
        if (!$accessObject->hasStoredTemplateTarget()) {
            $storedTemplateTarget = new StoredTemplateTarget(
                $accessObject,
                $this->formManager->getFlattenedFormValues($form),
                false,
                false
            );
        } else {
            $storedTemplateTarget = $accessObject->getStoredTemplateTarget();
            // merge currently given answers with previously given answers from storage
            $formValues = $this->formManager->getFlattenedFormValuesMergedWithUnWritableAndUnReachableStoredValues($form, $storedTemplateTarget);
            $storedTemplateTarget->update($accessObject, $formValues);
        }

        $this->storedTemplateTargetRepository->save($storedTemplateTarget);

        $this->eventDispatcher?->dispatch(FormSubmitted::create(
            $accessObject,
            $lastUsedStep = $storedTemplateTarget->getLastUsedStep(),
            $storedTemplateTarget->getTemplate()->getCurrentVersion()->getWorkFlow()->getSteps()->getPositionNumber(
                $lastUsedStep
            ),
        ));
    }

    private function updateStoredTemplateTargetWithTemporarySavedFormValues(
        FormInterface $form,
        ClientCombinedAccessObject $accessObject,
    ): void {
        Assert::true($accessObject->isWriteAccessAuthorized());
        Assert::true($this->formManager->isFormValidForTemporarySave($form));

        // first save ever -> create new storage record for target-template
        if (!$accessObject->hasStoredTemplateTarget()) {
            $storedTemplateTarget = new StoredTemplateTarget(
                $accessObject,
                $this->formManager->getFlattenedFormValues($form),
                true,
                false
            );

            $this->storedTemplateTargetRepository->save($storedTemplateTarget);

            return;
        }

        /** @var StoredTemplateTarget $storedTemplateTarget */
        $storedTemplateTarget = $accessObject->getStoredTemplateTarget();
        // merge currently given answers with previously given answers from storage
        $tempSavedFormValues = $this->formManager->getFlattenedFormValuesMergedWithUnWritableAndUnReachableStoredValues($form, $storedTemplateTarget);
        $storedTemplateTarget->updateTemporary($accessObject, $tempSavedFormValues, false);
        $this->storedTemplateTargetRepository->save($storedTemplateTarget);
    }

    /**
     * IMPORTANT: auto temporary saving is handled exactly like 'normal' temporary saving. except for one difference
     * regarding logging submissions (what answers were auto temp save when, by who, under what role, etc.):
     * storing a submission every time a user submits (e.g. every 5 minutes), will result in hundreds/thousands of
     * submissions in the DB and very long submission history lists in view pages. the DB will get very big, very fast.
     * to avoid this overkill (mainly for the DB) the domain decision is made to only keep one auto-saved submission
     * in DB for each UNIQUE combination of template-target-user-role-step-version. hence this submission gets 'replaced'
     * on every auto-save. which means the existing submission gets deleted first and then the 'normal' temporary save flow
     * will add a new submission in the usual way. submissions are set via many-to-many. handling this via
     * doctrine's cascade-remove, detaching from both sides and orphan-removal,.. is way too risky,
     * and the entity @see StoredTemplateTarget does not need to know how it gets handled.
     * note: there are edge cases where DB could be locked for a long time (over 3 minutes) while this autosave
     * is triggered and stores such a submission: that might result in there being more than 1 (max ever occured was 3)
     * 'auto' temp saved submissions in DB. that will intentionally not cause an exception, nor upon insert in DB,
     * nor in retrieval of one submission. simply because we only override submissions because of DB performance reasons,
     * not because we don't want that many.. @see SubmissionCollection::getOneAutoSavedForAccessObject().
     */
    private function updateStoredTemplateTargetWithAutoTemporarySavedFormValues(
        FormInterface $form,
        ClientCombinedAccessObject $accessObject,
    ): void {
        Assert::true($accessObject->isWriteAccessAuthorized());
        Assert::true($this->formManager->isFormValidForTemporarySave($form));

        // first save ever -> create new storage record for target-template (no submissions will yet exist)
        if (!$accessObject->hasStoredTemplateTarget()) {
            $storedTemplateTarget = new StoredTemplateTarget(
                $accessObject,
                $this->formManager->getFlattenedFormValues($form),
                true,
                true
            );

            $this->storedTemplateTargetRepository->save($storedTemplateTarget);

            return;
        }

        $storedTemplateTarget = $accessObject->getStoredTemplateTarget();
        $tempSavedFormValues = $this->formManager->getFlattenedFormValuesMergedWithUnWritableAndUnReachableStoredValues($form, $storedTemplateTarget);

        // update the temp saved values on the storedTemplateTarget. this will also add a new auto temp save submission
        // to the storedTemplateTarget's submission. the old one is removed AFTER having persisted & flushed teh storedTemplateTarget
        $storedTemplateTarget->updateTemporary($accessObject, $tempSavedFormValues, true);
        $this->storedTemplateTargetRepository->save($storedTemplateTarget);
    }

    private function getRenderParametersForReadOnlyView(Request $request, ClientTemplateTargetFormAccess $formAccess): array
    {
        Assert::true($formAccess->getPrimaryAccess()->isReadAccessAuthorized());

        return $this->getBaseRenderParameters(
            $request,
            $this->clientRoutesBuilder->getClientRouteDataSetForTemplate($formAccess->getTemplate()),
            $formAccess
        );
    }

    private function getRenderParametersForCombinedWriteReadAccess(
        Request $request,
        ClientTemplateTargetFormAccess $formAccess,
        FormInterface $form,
        bool $forceDisableAutoTemporarySave,
    ): array {
        $primaryAccess = $formAccess->getPrimaryAccess();

        // no need to check read access. is indirectly granted through write access
        Assert::true($primaryAccess->isWriteAccessAuthorized());

        $clientRouteDataSet = $this->clientRoutesBuilder->getClientRouteDataSetForTemplate($formAccess->getTemplate());
        $renderParameters = $this->getBaseRenderParameters($request, $clientRouteDataSet, $formAccess);
        $renderParameters['form'] = $form->createView();

        // check if auto-temporary save must be disabled for accessingRole. if so, don't let client side code trigger
        // automated temporary save from client side for this role
        // important for -usually - admins working in a form under own role or when (admins are) impersonating any other user.
        // this will allow these admins to test/view form without having to refresh page every x minutes to avoid undesired
        // auto temp saves and to avoid having undesired 'answers was auto temporary saved by <(impersonated)user>' mark
        // messages on answers afterwards (especially for the impersonation). also avoids edge case of answers overwritten
        // with blank values for impersonating admins leaving tabs open after ending impersonation in other tab. this just
        // disables client side triggering of AUTOMATED temporary save, manual temporary save remains available!
        // temporary save is needed for uploading files. however, disabling auto-save will not disable uploading files,
        // as the temporary saving for uploaded files is done in the back-end upon upload of files. when a file is uploaded,
        // the file is not yet marked as temporary saved answer, it only gets linked to the (in DB) stored filled-in form
        // instance (StoredTemplateTarget) record holding the filled-in form data. but when there is no record yet (that
        // file is the first answer ever given for that form), a temporary save is triggered so that a record can be created
        // to link the file to.
        // after successfully uploading and linking (and temp saving if needed) the file, the front-end will receive a
        // response about the success. if auto-save is then enabled in the form, then that response will immediately
        // trigger an unscheduled auto-save, making sure the uploaded file is then marked as a auto temporary saved answer
        // for its upload question. if the auto-save is disabled in the form, no such auto-save will be triggered and the
        // user who uploaded the file will have to manually temp save (or submit) to mark the uploaded file as temp-saved
        // or submitted answer, just like with any other type of question.
        $template = $formAccess->getTemplate();

        if (false === $forceDisableAutoTemporarySave
            && $template->isAutoTemporarySavingEnabled()
            && !$template->shouldDisableAutoTemporarySavingForRole($primaryAccess->getAccessingRole())
        ) {
            $renderParameters['triggerAutoSave'] = true;
            $renderParameters['autoSaveInterval'] = $this->autoSaveInterval;
            $renderParameters['autoSaveUrl'] = $this->getAutoTempSaveUrl($request, $clientRouteDataSet, $primaryAccess);
            // if back-end returns from an auto-save action with an 'access denied' due to 'form no longer in step user loaded the form in'
            // the form is locked on client side via a non-dismissible modal. this non-dismissible modal is also shown in some other
            // specific auto-save failures (e.g. session time-out) this 'allowToIgnoreFormLock' allows user to close that
            // pop-up anyway and continue filling in a form. naturally we set this to false. but the implementing app can decide to
            // overwrite this in the twig in which its renders the form. one reason to allow a user to dismiss this, is it will allow
            // that user to manually copy-paste some answers in a Word doc or so? since the user will most likely not be able
            // to (temp) save the form manually anymore (something of which he/she is notified in the non-dismissible modal).
            $renderParameters['allowToIgnoreFormLock'] = false;
        }

        return $renderParameters;
    }

    /**
     * Generates an array of variables to be passed to the twig that will actually render the form.
     *
     * FIXME: Remove dependency on $request, @see https://jira.gbiomed.kuleuven.be/browse/FORM-126.
     *
     * @return array{accessObject: ClientCombinedAccessObject, target: TemplateTargetableInterface, roleSlugs: array<string, string>, switchRoleUrls: array<string, string>, formValues: mixed[], fileUrls: FileUrls, overviewUrl: null, autoCalculatePrototypeUrl: string, revertActions: RevertActions, reloadViewUrl: string, idleCheckInterval: int}
     */
    private function getBaseRenderParameters(
        Request $request,
        ClientRouteDataCollection $clientRouteDataSet,
        ClientTemplateTargetFormAccess $formAccess,
    ): array {
        $primaryAccess = $formAccess->getPrimaryAccess();
        $template = $primaryAccess->getTemplate();
        $savedFormValues = $primaryAccess->hasStoredTemplateTarget() ?
            $primaryAccess->getStoredTemplateTarget()->getSubmittedFormValues() : [];
        $fileUrls = FileUrls::fromPrototypeUrls(
            $this->getDownloadFilePrototypeUrl($request, $clientRouteDataSet, $primaryAccess),
            $this->getUploadFilePrototypeUrl($request, $clientRouteDataSet, $primaryAccess)
        );

        $switchRoleUrls = [];
        /** @var ClientCombinedAccessObject $accessObject */
        foreach ($formAccess->getAccessObjects() as $accessObject) {
            $switchRoleUrls[$accessObject->getAccessingRole()->getName()] = $this->getStoringViewUrlForAccessingRole(
                $request,
                $clientRouteDataSet,
                $accessObject
            );
        }

        return [
            'accessObject' => $primaryAccess,
            'target' => $primaryAccess->getClientWithTargetAccess()->getTarget(),
            'roleSlugs' => $template->getParticipatableRoleSlugifier()->getRoleSlugs(),
            'switchRoleUrls' => $switchRoleUrls,
            // pass last known saved form values to twig to render in read-only questions. this is a domain decision:
            // only render saved answers for questions in read-only mode (= no write access). never temporary saved answers.
            'formValues' => $savedFormValues,
            'fileUrls' => $fileUrls,
            'overviewUrl' => null, // TODO can not generate this from bundle....
            'autoCalculatePrototypeUrl' => $this->getAutoCalculatePrototypeUrl($request, $clientRouteDataSet, $primaryAccess),
            'revertActions' => $this->getRevertActionsBuilder()->buildAllowedRevertActionsWithFormPageReloadResponseForClientAccess($primaryAccess, $request->getLocale()),
            'reloadViewUrl' => $this->getStoringViewUrl($request, $clientRouteDataSet),
            'idleCheckInterval' => $this->idleCheckInterval, // config -> kul_form.form_activity.idle_check_interval_in_minutes
        ];
    }
}
