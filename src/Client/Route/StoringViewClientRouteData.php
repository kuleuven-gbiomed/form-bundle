<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Route;

/**
 * Class StoringViewClientRouteData.
 *
 * info @see Configuration::getTemplateTypesClientRoutesStoringViewNode()
 */
class StoringViewClientRouteData extends AbstractClientRouteData
{
    /** @var string */
    final public const CONFIG_KEY = 'storing_view';

    public static function getConfigKey(): string
    {
        return self::CONFIG_KEY;
    }

    public static function allowsNullValueForDefinedRouteVariable(string $variable): bool
    {
        return $variable === static::ROUTE_VARIABLE_ROLE_SLUG;
    }
}
