<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Route;

/**
 * Class OverviewTargetAccessViewClientRouteData.
 *
 * info @see Configuration::getTemplateTypesClientRoutesOverviewTargetAccessViewNode()
 */
class OverviewTargetAccessViewClientRouteData extends AbstractClientRouteData
{
    /** @var string */
    final public const CONFIG_KEY = 'overview_target_access_view';

    public static function getConfigKey(): string
    {
        return self::CONFIG_KEY;
    }

    public static function allowsNullValueForDefinedRouteVariable(string $variable): bool
    {
        return $variable === static::ROUTE_VARIABLE_ROLE_SLUG;
    }
}
