<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Route;

class InvalidClientRouteException extends \Exception
{
}
