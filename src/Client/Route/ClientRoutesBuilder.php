<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Route;

use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouterInterface;

class ClientRoutesBuilder
{
    private array $routeConfigKeyToRouteClassMapping = [
        StoringViewClientRouteData::CONFIG_KEY => StoringViewClientRouteData::class,
        StoringSaveClientRouteData::CONFIG_KEY => StoringSaveClientRouteData::class,
        StoringAutoTemporarySaveClientRouteData::CONFIG_KEY => StoringAutoTemporarySaveClientRouteData::class,
        DownloadFileQuestionAnswerClientRouteData::CONFIG_KEY => DownloadFileQuestionAnswerClientRouteData::class,
        UploadFileQuestionAnswerClientRouteData::CONFIG_KEY => UploadFileQuestionAnswerClientRouteData::class,
        GlobalScoreCalculatedValueClientRouteData::CONFIG_KEY => GlobalScoreCalculatedValueClientRouteData::class,
        OverviewTargetAccessViewClientRouteData::CONFIG_KEY => OverviewTargetAccessViewClientRouteData::class,
    ];

    public function __construct(private readonly RouterInterface $router, private readonly array $configTemplateTypes)
    {
    }

    /**
     * build all required routes for the given @see TemplateInterface::getTargetingType(), based on
     * the configuration in your app or fail hard if no configuration exist or one of those routes is invalid.
     *
     * NOTE: unfortunately, we cannot validate the configured client route name upon bundle compile to be real routes
     * with correct named parameters. so it is done here on all required routes and a @see InvalidConfigurationException
     * is thrown to indicate that error is due to invalid config
     *
     * @throws InvalidConfigurationException
     * @throws InvalidClientRouteException
     */
    public function getClientRouteDataSetForTargetingType(string $targetingType): ClientRouteDataCollection
    {
        $this->validateTargetingTypeIsConfigured($targetingType);

        $appRoutes = $this->router->getRouteCollection();
        $clientRouteDataSet = [];

        // if implementing app has configured the set of config parameters for the targeting type to which given template
        // belongs, then the compile of this bundle will have asserted that a (route name) string for all client
        // routes will be configure. so we can assume this set of client-routes-config will exist and we only need to
        // if a route exists for it and that it has correct parameters
        foreach ($this->configTemplateTypes[$targetingType]['client']['routes'] as $configKey => $routeName) {
            $fullConfigKey = 'kul_form.template_types.'.$targetingType.'.client.routes.'.$configKey;

            // check if a route is declared in implementing app or fail if not
            $route = $appRoutes->get($routeName);
            if (!$route instanceof Route) {
                throw new InvalidConfigurationException('invalid configuration for a route reference at config parameter ['.$fullConfigKey.']: the given string refers to a route with routeName ['.$routeName.'], but no route with that name is found. please make sure you have a route defined for this config parameter');
            }

            /** @var AbstractClientRouteData $routeClass */
            $routeClass = $this->routeConfigKeyToRouteClassMapping[$configKey];

            $clientRouteDataSet[] = $routeClass::fromRouteAndRouteName($route, $routeName);
        }

        return new ClientRouteDataCollection($clientRouteDataSet);
    }

    /**
     * build all required routes for the @see TemplateInterface::getTargetingType() of given template, based on
     * the configuration in implementing app or fail hard if no configuration exist or one of those routes is invalid.
     *
     * @throws InvalidConfigurationException
     * @throws InvalidClientRouteException
     */
    public function getClientRouteDataSetForTemplate(TemplateInterface $template): ClientRouteDataCollection
    {
        return $this->getClientRouteDataSetForTargetingType($template->getTargetingType());
    }

    /**
     * @internal
     * we need to make sure the template called by the request is linked to the correct routes
     * because we're going to use the request later on to generically build other routes
     * based on the @see TemplateInterface::getTargetingType()
     *
     * @throws \Exception
     */
    public function validateClientRouteDataSetForTemplateInRequest(
        Request $request,
        TemplateInterface $template,
    ): void {
        try {
            // validate if config for targeting type exists
            $this->validateTargetingTypeIsConfigured($template->getTargetingType());
            // get the config for template targeting type
            $templateTargetingTypeRoutesConfig = $this->configTemplateTypes[$template->getTargetingType()]['client']['routes'];

            $requestRouteName = $request->attributes->get('_route');
            // validate if the route in the request is the one associated with the template targeting type
            if (!in_array($requestRouteName, $templateTargetingTypeRoutesConfig, true)) {
                throw new \Exception('the route on the given request with route name ['.$requestRouteName.' does not match any of the routes defined & configured for the given template.');
            }

            // validate all routes all valid (including the one from request) by trying to build all
            $this->getClientRouteDataSetForTargetingType($template->getTargetingType());
        } catch (InvalidConfigurationException $exception) {
            throw new \DomainException('the template ['.$template->getId().'] can not be called on given request due to configuration issues: '.$exception->getMessage());
        } catch (InvalidClientRouteException $exception) {
            throw new \DomainException('the template ['.$template->getId().'] can not be called on given request due to invalid route data: '.$exception->getMessage());
        } catch (\Exception $exception) {
            throw new \DomainException('the template ['.$template->getId().'] can not be called on given request due to this error: '.$exception->getMessage());
        }
    }

    /**
     * @internal
     *
     * WARNING: really assuming a lot here... and doing some risky magic here, hoping that the developer of the
     * implementing app will have made the correct routes. but the fact that I have to rely on request from
     * implementing app whilst having to build urls for some (POST) actions from inside this bundle to routes
     * declared outside this bundle, makes this necessary. otherwise I would have to ask the urls to be injected
     * on runtime in the services, making it even more harder for some actions to do generic stuff.
     * WARNING: the 'internal' marking is also set because of the fact that this might not work for all url generating,
     * based on requests: the defined & configured routes will all contain the same variables the formBundle knows about
     * (template-id, target-id, target-code) and the other ones the formBundle knows about will be set inside the
     * services of this bundle of which all know what to set when with what. but it is strictly speaking possible,
     * although unlikely (and definitely not advised), that the route parameters from outside the bundle, so the ones
     * only known to your app's domain have different app-related variables per request sent to this bundle. again,
     * this is unlikely and I cannot think of a point for it, but if it does, all bets are off...!
     *
     * this method is supposed to be for internal use and if routes are build correctly in implementing app and
     * this method is called with correct combo of request & client route data, then all should be fine...
     *
     * the passed in @see AbstractClientRouteData is (should) have been build by this same service route builder
     * which has asserted that a route with @see AbstractClientRouteData::getRouteName() exist in the router
     * and  -hopefully - that the request has the variable values needed.
     *
     * the assumption here is reasonably safe, because the building from a given request for a given clientRouteData
     * is done by this bundle on the fact that both routes (request-route and clientRouteData-route) are closely
     * linked and depending on each other, and on the fact that the bundle uses the same set of re-occurring
     * variables for all its actions and the implementing app will have no choice to follow that assumption
     * both on those re-occurring variables (which are asserted) and it's own variables. in the sense that all
     * actions for getting a form view, storing answers, etc. all depend on the same variables,
     * both in- and outside this bundle. otherwise it would simply not work. hence the assertions here on
     * parameters in url to build being present in the request params.
     *
     * @param array $parameters optional if a parameter value needs to be overwritten (has priority over request param)
     *
     * @throws InvalidClientRouteException
     */
    public function generateUrlFromRequestForClientRouteDataAndParameters(
        Request $request,
        AbstractClientRouteData $clientRouteData,
        array $parameters = [],
    ): string {
        return $this->generateUrlFromRequestParametersForClientRouteDataAndParameters(
            $request->attributes->get('_route_params'),
            $request->query->all(),
            $clientRouteData,
            $parameters
        );
    }

    public function generateUrlFromRequestParametersForClientRouteDataAndParameters(
        array $requestRouteParams,
        array $requestUriParams,
        AbstractClientRouteData $clientRouteData,
        array $additionalParameters = [],
    ): string {
        $destinationRouteParams = [];
        $destinationRouteName = $clientRouteData->getRouteName();
        // at this point, via this builder's route building, the route is (should) have been verified as an existing
        // route in implementing app. we get the route here at the beginning already and if assert that it does exist
        $destinationRoute = $this->router->getRouteCollection()->get($destinationRouteName);
        if (!$destinationRoute instanceof Route) {
            throw new InvalidConfigurationException('no route found with routeName ['.$destinationRouteName.']');
        }

        // get the variables that are defined and asserted for this bundle
        // note: could also just get them from the router- route, but it's better to separate and check
        // the variables defined by this bundle from the ones part of the implementing app
        $clientRouteDefinedVariables = $clientRouteData::getDefinedRouteVariables();

        // for all defined variables for this route a parameter must be found and set (set even if nullable in case no
        // default null was set on route declaration in implementing app. if no value found for it in request or
        // parameters we will eventually throw an exception. if a parameter was specified in method argument parameters,
        // then that has priority over the the parameter in the request (if set). that allows to use placeholder
        // parameters for certain actions (like global score calculation ajax actions that use stuff like __node_uid__)
        foreach ($clientRouteDefinedVariables as $variable) {
            /** @var bool $foundParam */
            $foundParam = false;
            $value = null;
            if (array_key_exists($variable, $additionalParameters)) {
                $foundParam = true;
                $value = $additionalParameters[$variable];
            } elseif (array_key_exists($variable, $requestRouteParams)) {
                $value = $requestRouteParams[$variable];
                $foundParam = true;
            } elseif ($clientRouteData::allowsNullValueForDefinedRouteVariable($variable)) {
                // just to make sure all params will be set: if variable is optional (nullable) for this client route
                // we mark it as found and leave the value as null
                $foundParam = true;
            }

            if (false === $foundParam) {
                throw new InvalidClientRouteException('cannot find a parameter value for route variable ['.$variable.'] to be used in route with name ['.$destinationRouteName.']. this route is keyed as ['.$clientRouteData::getConfigKey().'] inside FormBundle, which requires a value for FormBundle specific variables: ['.implode('|', $clientRouteDefinedVariables).']');
            }

            if (null === $value && !$clientRouteData::allowsNullValueForDefinedRouteVariable($variable)) {
                throw new InvalidClientRouteException('parameter value for route variable ['.$variable.'] to be used in route with name ['.$destinationRouteName.'] cannot be NULL.');
            }

            $destinationRouteParams[$variable] = $value;
        }

        // get the remaining parameters from the request that are not defined by this bundle but are part of the domain
        // in the implementing app and use those to set the parameters of the destination client route that are
        // part of the implmenting app (this is the real tricky, overly assuming part) (at this point, via this builder,
        // the route is (should) have been verified as an existing route in implementing app, else it let it fail)
        foreach ($destinationRoute->compile()->getVariables() as $variable) {
            // if already added via the defined form bundle variables, skip
            if (array_key_exists($variable, $destinationRouteParams)) {
                continue;
            }

            // going to have to assume that if param is not found in request, it will be optional and can be left out
            // otherwise, a route exception will be thrown (usually, even the optionals
            if (array_key_exists($variable, $requestRouteParams)) {
                $destinationRouteParams[$variable] = $requestRouteParams[$variable];
            }
        }

        // probably not that important, but no harm in adding the uri (query) params, if any
        foreach ($requestUriParams as $variable => $value) {
            // let's make sure no unwanted overwrites of defined variables or conflicts or...  happens
            if (array_key_exists($variable, $destinationRouteParams)) {
                continue;
            }

            $destinationRouteParams[$variable] = $value;
        }

        return $this->router->generate($destinationRouteName, $destinationRouteParams);
    }

    public function validateTargetingTypeIsConfigured(string $targetingType): void
    {
        // for each (template) targeting type, implementing app will have configured a set of parameters,
        // fails if implementing app has failed to do so
        if (!array_key_exists($targetingType, $this->configTemplateTypes)) {
            throw new InvalidConfigurationException('missing configuration: there is no configuration (including the configuration for client
                 routes), provided for templates of targeting type ['.$targetingType.']. please provide a parameter set for this targeting type under config key [kul_form.template_types].');
        }
    }
}
