<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Route;

/**
 * Class StoringAutoTemporarySaveClientRouteData.
 *
 * info @see Configuration::getTemplateTypesClientRoutesStoringAutoTemporarySaveNode()
 */
class StoringAutoTemporarySaveClientRouteData extends AbstractClientRouteData
{
    /** @var string */
    final public const CONFIG_KEY = 'storing_auto_temporary_save';

    public static function getConfigKey(): string
    {
        return self::CONFIG_KEY;
    }

    public static function allowsNullValueForDefinedRouteVariable(string $variable): bool
    {
        return false;
    }
}
