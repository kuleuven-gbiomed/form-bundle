<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Route;

use KUL\FormBundle\Client\Slugifier\RoleSlugifier;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use Symfony\Component\Routing\Route;

/**
 * Class AbstractClientRouteData.
 *
 * @see     ClientRouteDataCollection
 */
abstract class AbstractClientRouteData
{
    /**
     * corresponds to @see TemplateInterface::getId().
     *
     * @var string
     */
    final public const ROUTE_VARIABLE_TEMPLATE_ID = 'templateId';
    /**
     * corresponds to @see TemplateTargetableInterface::getTargetCode().
     *
     * @var string
     */
    final public const ROUTE_VARIABLE_TARGET_CODE = 'targetCode';
    /**
     * corresponds to @see TemplateTargetableInterface::getTargetIdentifier().
     *
     * @var string
     */
    final public const ROUTE_VARIABLE_TARGET_ID = 'targetId';
    /**
     * url slug corresponds to a role via  @see RoleSlugifier::getSlugForRole().
     *
     * @var string
     */
    final public const ROUTE_VARIABLE_ROLE_SLUG = 'roleSlug';

    private readonly string $routeName;

    abstract public static function getConfigKey(): string;

    abstract public static function allowsNullValueForDefinedRouteVariable(string $variable): bool;

    final protected function __construct(string $routeName)
    {
        if ('' === $routeName) {
            throw new \InvalidArgumentException('routeName must be a non-empty string');
        }

        $this->routeName = $routeName;
    }

    /**
     * this is and should remain the only way to construct any of the child implementations, because this
     * will guarantee that every route is validated for defined variables.
     *
     * NOTE: can not get the routeName from a @see Route in Symfony which is ridiculous and forces me to pass it along
     *
     * @param Route  $route     the Route to use for validating the defined variables
     * @param string $routeName the name of the route (can not be retrieved from route, for some stupid Symfony reason)
     *
     * @throws InvalidClientRouteException
     */
    public static function fromRouteAndRouteName(Route $route, string $routeName): self
    {
        // easiest way to get variables is to compile the route
        // each variable defined by the static must be in the given route's variables
        static::validateVariables($route->compile()->getVariables());

        return new static($routeName);
    }

    /**
     * returns the route variables defined for this route to be able to be used (auto-generated) by this bundle
     * (these route variables must be in the route variables, aside from any other variables from implementing app).
     *
     * @return string[]
     */
    public static function getDefinedRouteVariables(): array
    {
        return [
            self::ROUTE_VARIABLE_TEMPLATE_ID,
            self::ROUTE_VARIABLE_TARGET_CODE,
            self::ROUTE_VARIABLE_TARGET_ID,
            self::ROUTE_VARIABLE_ROLE_SLUG,
        ];
    }

    public function getRouteName(): string
    {
        return $this->routeName;
    }

    /**
     * @throws InvalidClientRouteException
     */
    protected static function validateVariables(array $variablesToValidate): void
    {
        // each defined variable must be in the route variables
        foreach (static::getDefinedRouteVariables() as $definedRouteVariable) {
            if (!in_array($definedRouteVariable, $variablesToValidate, true)) {
                throw new InvalidClientRouteException('missing route variable: a route defined as a ['.static::getConfigKey().'] route must
                    contain the route variable ['.$definedRouteVariable.']. but did not find this variable in
                    variables ['.implode('|', $variablesToValidate).']');
            }
        }
    }
}
