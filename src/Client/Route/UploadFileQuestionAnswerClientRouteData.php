<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Route;

/**
 * Class UploadFileQuestionAnswerClientRouteData.
 *
 * info @see Configuration::getTemplateTypesClientRoutesUploadFileQuestionAnswerNode()
 */
class UploadFileQuestionAnswerClientRouteData extends AbstractClientRouteData
{
    /** @var string */
    final public const CONFIG_KEY = 'upload_file_question_answer';
    /**
     * corresponds to @see UploadInputNode::getUid() to upload answer for.
     *
     * @var string
     */
    final public const ROUTE_VARIABLE_NODE_UID = 'nodeUid';

    public static function getConfigKey(): string
    {
        return self::CONFIG_KEY;
    }

    public static function allowsNullValueForDefinedRouteVariable(string $variable): bool
    {
        return false;
    }

    public static function getDefinedRouteVariables(): array
    {
        return array_merge(
            parent::getDefinedRouteVariables(),
            [
                self::ROUTE_VARIABLE_NODE_UID,
            ]
        );
    }
}
