<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Route;

/**
 * Class StoringSaveClientRouteData.
 *
 * info @see Configuration::getTemplateTypesClientRoutesStoringSaveNode()
 */
class StoringSaveClientRouteData extends AbstractClientRouteData
{
    /** @var string */
    final public const CONFIG_KEY = 'storing_save';

    public static function getConfigKey(): string
    {
        return self::CONFIG_KEY;
    }

    public static function allowsNullValueForDefinedRouteVariable(string $variable): bool
    {
        return false;
    }
}
