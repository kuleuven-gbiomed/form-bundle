<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Route;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @extends ArrayCollection<array-key,AbstractClientRouteData>
 */
class ClientRouteDataCollection extends ArrayCollection
{
    public function getByClientRouteDataClass(string $className): self
    {
        return $this->filter(fn (AbstractClientRouteData $route) => $route instanceof $className);
    }

    public function getOneByClientRouteClass(string $className): AbstractClientRouteData
    {
        $result = $this->getByClientRouteDataClass($className);
        $first = $result->first();
        if ($first instanceof AbstractClientRouteData && 1 === $result->count()) {
            return $first;
        }

        throw new \BadMethodCallException("expected to find one AbstractClientRouteData. found {$result->count()}.");
    }

    public function getOneForStoringView(): StoringViewClientRouteData
    {
        $result = $this->getOneByClientRouteClass(StoringViewClientRouteData::class);
        if ($result instanceof StoringViewClientRouteData) {
            return $result;
        }

        throw new \BadMethodCallException('expected to find instance of StoringViewClientRouteData. got '.$result::class);
    }

    public function getOneForStoringSave(): StoringSaveClientRouteData
    {
        $result = $this->getOneByClientRouteClass(StoringSaveClientRouteData::class);
        if ($result instanceof StoringSaveClientRouteData) {
            return $result;
        }

        throw new \BadMethodCallException('expected to find instance of StoringSaveClientRouteData. got '.$result::class);
    }

    public function getOneForStoringAutoTemporarySave(): StoringAutoTemporarySaveClientRouteData
    {
        $result = $this->getOneByClientRouteClass(StoringAutoTemporarySaveClientRouteData::class);
        if ($result instanceof StoringAutoTemporarySaveClientRouteData) {
            return $result;
        }

        throw new \BadMethodCallException('expected to find instance of StoringAutoTemporarySaveClientRouteData. got '.$result::class);
    }

    public function getOneForDownloadFileQuestionAnswer(): DownloadFileQuestionAnswerClientRouteData
    {
        $result = $this->getOneByClientRouteClass(DownloadFileQuestionAnswerClientRouteData::class);
        if ($result instanceof DownloadFileQuestionAnswerClientRouteData) {
            return $result;
        }

        throw new \BadMethodCallException('expected to find instance of DownloadFileQuestionAnswerClientRouteData. got '.$result::class);
    }

    public function getOneForUploadFileQuestionAnswer(): UploadFileQuestionAnswerClientRouteData
    {
        $result = $this->getOneByClientRouteClass(UploadFileQuestionAnswerClientRouteData::class);
        if ($result instanceof UploadFileQuestionAnswerClientRouteData) {
            return $result;
        }

        throw new \BadMethodCallException('expected to find instance of UploadFileQuestionAnswerClientRouteData. got '.$result::class);
    }

    public function getOneForGlobalScoreCalculatedValue(): GlobalScoreCalculatedValueClientRouteData
    {
        $result = $this->getOneByClientRouteClass(GlobalScoreCalculatedValueClientRouteData::class);
        if ($result instanceof GlobalScoreCalculatedValueClientRouteData) {
            return $result;
        }

        throw new \BadMethodCallException('expected to find instance of GlobalScoreCalculatedValueClientRouteData. got '.$result::class);
    }

    public function getOneForOverviewTargetAccessView(): OverviewTargetAccessViewClientRouteData
    {
        $result = $this->getOneByClientRouteClass(OverviewTargetAccessViewClientRouteData::class);
        if ($result instanceof OverviewTargetAccessViewClientRouteData) {
            return $result;
        }

        throw new \BadMethodCallException('expected to find instance of OverviewTargetAccessViewClientRouteData. got '.$result::class);
    }
}
