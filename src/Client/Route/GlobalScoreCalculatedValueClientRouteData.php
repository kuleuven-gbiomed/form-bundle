<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Route;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;

/**
 * Class GlobalScoreCalculatedValueClientRouteData.
 *
 * info @see Configuration::getTemplateTypesClientRoutesGlobalScoreCalculatedValueNode()
 */
class GlobalScoreCalculatedValueClientRouteData extends AbstractClientRouteData
{
    /** @var string */
    final public const CONFIG_KEY = 'global_score_calculated_value';
    /**
     * corresponds to @see ScorableInputNode::operatesAsGlobalScoreNode()::getUid() to (try) get (re)calculated value for.
     *
     * @var string
     */
    final public const ROUTE_VARIABLE_NODE_UID = 'nodeUid';

    public static function getConfigKey(): string
    {
        return self::CONFIG_KEY;
    }

    public static function allowsNullValueForDefinedRouteVariable(string $variable): bool
    {
        return false;
    }

    public static function getDefinedRouteVariables(): array
    {
        return array_merge(
            parent::getDefinedRouteVariables(),
            [
                self::ROUTE_VARIABLE_NODE_UID,
            ]
        );
    }
}
