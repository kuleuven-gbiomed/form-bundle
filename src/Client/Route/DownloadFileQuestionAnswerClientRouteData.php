<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Route;

use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;

/**
 * Class DownloadFileQuestionAnswerClientRouteData.
 *
 * info @see Configuration::getTemplateTypesClientRoutesDowloadFileQuestionAnswerNode()
 */
class DownloadFileQuestionAnswerClientRouteData extends AbstractClientRouteData
{
    /** @var string */
    final public const CONFIG_KEY = 'download_file_question_answer';
    /**
     * corresponds to @see UploadInputNode::getUid() to download answer from.
     *
     * @var string
     */
    final public const ROUTE_VARIABLE_NODE_UID = 'nodeUid';
    /**
     * corresponds to the file id to download within the given @see UploadInputNode::getUid().
     *
     * @var string
     */
    final public const ROUTE_VARIABLE_FILE_UID = 'fileUid';

    public static function getConfigKey(): string
    {
        return self::CONFIG_KEY;
    }

    public static function allowsNullValueForDefinedRouteVariable(string $variable): bool
    {
        return false;
    }

    public static function getDefinedRouteVariables(): array
    {
        return array_merge(
            parent::getDefinedRouteVariables(),
            [
                self::ROUTE_VARIABLE_NODE_UID,
                self::ROUTE_VARIABLE_FILE_UID,
            ]
        );
    }
}
