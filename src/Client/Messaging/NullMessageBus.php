<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Messaging;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\StampInterface;

class NullMessageBus implements MessageBusInterface
{
    /**
     * @param object|Envelope  $message
     * @param StampInterface[] $stamps
     */
    public function dispatch($message, array $stamps = []): Envelope
    {
        return new Envelope($message, $stamps);
    }
}
