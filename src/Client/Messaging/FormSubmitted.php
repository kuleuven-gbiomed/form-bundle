<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client\Messaging;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;

readonly class FormSubmitted
{
    public function __construct(
        private string $templateId,
        private string $targetType,
        private string $targetId,
        private StepInterface $currentStep,
        private ?StepInterface $changedFromStep,
        private string $submittingUserId,
        private \DateTimeImmutable $submissionTime,
        private string $accessingRole,
        private int $sequence,
    ) {
    }

    public static function create(ClientCombinedAccessObject $accessObject, StepInterface $currentStep, int $sequence): self
    {
        // when a user was granted access to (render and) submit a form, then there was an open step when that user submitted that form.
        $originalStep = $accessObject->getOpenFlowStep();

        return new self(
            $accessObject->getTemplate()->getId(),
            $accessObject->getTemplate()->getTargetingType(),
            $accessObject->getClientWithTargetAccess()->getTarget()->getTargetIdentifier(),
            $currentStep,
            ($currentStep->getUid() === $originalStep->getUid()) ? null : $originalStep,
            $accessObject->getUser()->getIdentifier(),
            $accessObject->getClientWithTargetAccess()->getCurrentDate(),
            $accessObject->getAccessingRole()->getName(),
            $sequence
        );
    }

    public function getTemplateId(): string
    {
        return $this->templateId;
    }

    public function getTargetType(): string
    {
        return $this->targetType;
    }

    public function getTargetId(): string
    {
        return $this->targetId;
    }

    public function getCurrentStep(): StepInterface
    {
        return $this->currentStep;
    }

    public function getChangedFromStep(): ?StepInterface
    {
        return $this->changedFromStep;
    }

    public function stepChanged(): bool
    {
        return !(null === $this->changedFromStep);
    }

    public function getSubmittingUserId(): string
    {
        return $this->submittingUserId;
    }

    public function getSubmissionTime(): \DateTimeImmutable
    {
        return $this->submissionTime;
    }

    public function getAccessingRole(): string
    {
        return $this->accessingRole;
    }

    public function getSequence(): int
    {
        return $this->sequence;
    }
}
