<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\Access\ClientTemplateTargetFormAccess;
use KUL\FormBundle\Client\Manage\FormManager;
use KUL\FormBundle\Client\Response\AbstractDoResponse;
use KUL\FormBundle\Client\Response\DoAccessDeniedResponse;
use KUL\FormBundle\Client\Response\DoFileResponse\DoFileAnswerDownloadSuccessResponse;
use KUL\FormBundle\Client\Response\DoFileResponse\DoFileAnswerUploadInvalidFilesResponse;
use KUL\FormBundle\Client\Response\DoFileResponse\DoFileAnswerUploadSuccessResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\AbstractDoInvalidStepStoringAttemptResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoInvalidStepStoringAttemptWithAccessInNewStepResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse;
use KUL\FormBundle\Client\Route\ClientRoutesBuilder;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Client\Util\File\NodeTempFileDataRowCollection;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Entity\StoredTemplateTarget;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use KUL\FormBundle\Entity\SubmissionRepository;
use KUL\FormBundle\Entity\TemplateRepository;
use KUL\FormBundle\Entity\Upload\TempFileCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Webmozart\Assert\Assert;

class TemplateTargetFileAnswerService extends AbstractTemplateTargetService
{
    public function __construct(
        StoredTemplateTargetRepository $storedTemplateTargetRepository,
        SubmissionRepository $submissionRepository,
        ClientRoutesBuilder $clientRoutesBuilder,
        FormManager $formManager,
        string $defaultLocale,
        array $availableAdminRoles,
        int $autoSaveInterval,
        int $idleCheckInterval,
        TemplateRepository $templateRepository,
        private readonly string $uploadDirectory,
        private readonly TranslatorInterface $translator,
        private readonly bool $useDestinationSubDirectoriesBasedOnFileNameLetters,
    ) {
        parent::__construct(
            $storedTemplateTargetRepository,
            $submissionRepository,
            $clientRoutesBuilder,
            $formManager,
            $defaultLocale,
            $availableAdminRoles,
            $autoSaveInterval,
            $idleCheckInterval,
            $templateRepository
        );
    }

    /**
     * no invalidStepStoringAttempt (@see AbstractDoInvalidStepStoringAttemptResponse) checking is not needed here, nor is
     * is breaking stuff: as long as the user has write access in the step he/she loaded the form in or the new step
     * if the original step is already finished, he/she can upload a file.
     * this upload will ONLY add the files to the storedTemplateTarget record, it won't actually store the file as
     * temp saved answer, that's for the user to submit as (temp) save answer or for an auto-save to store.
     * So as long as user doesn't do that (or no auto-save triggered), no uploaded files are actually changed or added as
     * answers to the upload question on the record.
     *
     * @throws Access\InvalidAccessArgumentException
     * @throws MissingVersionException
     */
    public function getUploadFileQuestionAnswerResponse(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        string $nodeUid,
        ?\DateTimeInterface $calculatedStepsReferenceDate,
        ?\DateTimeInterface $secondaryCalculatedStepsCheckDate,
    ): AbstractDoResponse {
        $this->assertBuildRequirements($request, $template, $calculatedStepsReferenceDate);
        $doAccessDeniedResponse = $this->getOneOrNullPreliminaryDoAccessDeniedResponse($template, $clientWithTargetAccess);
        if ($doAccessDeniedResponse instanceof DoAccessDeniedResponse) {
            return $doAccessDeniedResponse;
        }

        $formAccess = $this->getNullOrFormAccessForSingleClientAccessingRoleInRequest(
            $request,
            $template,
            $clientWithTargetAccess,
            $calculatedStepsReferenceDate,
            $secondaryCalculatedStepsCheckDate
        );

        // no access at all for user or no more access in a new step if user had access when he/she loaded the form in
        // original step, but the form is now already in a next step, in which the user has no more access at all.
        if (!$formAccess instanceof ClientTemplateTargetFormAccess) {
            $doInvalidStepStoringAttemptWithoutAccessResponse = $this->findInvalidStepStoringAttemptByDates(
                $request,
                $template,
                $clientWithTargetAccess,
                $calculatedStepsReferenceDate,
                $secondaryCalculatedStepsCheckDate
            );

            return $doInvalidStepStoringAttemptWithoutAccessResponse instanceof DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse ?
                $doInvalidStepStoringAttemptWithoutAccessResponse :
                new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_HAS_NO_ROLES_WITH_ACCESS_IN_CURRENT_STEP);
        }

        $primaryAccess = $formAccess->getPrimaryAccess();

        // check if user is uploading file in the same step in which he loaded the form in. if the form is now in a next step,
        // then at his point we know the user still has access in that next step, but we cannot allow that user to upload files
        // in that next step, same way as we don't allow any (temporary) save action in a step other than the one the user loaded the form in
        $doInvalidStepStoringAttemptResponse = $this->findInvalidStepStoringAttemptByCurrentAccess($request, $primaryAccess);
        if ($doInvalidStepStoringAttemptResponse instanceof DoInvalidStepStoringAttemptWithAccessInNewStepResponse) {
            return $doInvalidStepStoringAttemptResponse;
        }

        $uploadNode = $this->getReachableUploadNodeOrNull($primaryAccess, $nodeUid);
        if (!$uploadNode instanceof UploadInputNode) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_REQUESTS_INVALID_NODE);
        }

        // check if write (it's upload so check write) access is given to upload node for user (role) in current step
        if (!$primaryAccess->isWriteAccessAuthorized() || !$uploadNode->isStepRoleWriteAccessGranted($primaryAccess->getOpenFlowStep(), $primaryAccess->getAccessingRole())) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP_TO_SPECIFIED_NODE);
        }

        $tempCollection = TempFileCollection::createFromFileBag($request->files);

        // check if files to upload meet size & mimeType requirements as set by node. if not return with invalid files data
        $nodeTempFilesData = NodeTempFileDataRowCollection::fromTempFilesAndUploadNode($tempCollection, $uploadNode);
        if ($nodeTempFilesData->hasInvalid()) {
            return new DoFileAnswerUploadInvalidFilesResponse($nodeTempFilesData, $uploadNode, $this->translator);
        }

        // store files and attach them to the node
        $uploadedFiles = $tempCollection->finishUpload(
            $clientWithTargetAccess->getFormUser(),
            $this->uploadDirectory,
            $this->useDestinationSubDirectoriesBasedOnFileNameLetters
        );
        $savedFiles = SavedFileCollection::fromUploadedFilesAndInputNode($uploadedFiles, $uploadNode);

        // Note that SCONE sends a AutoTemporarySavedWithFilesTargetForm-command to the tactician
        // command bus, to handle this. (speaking of which, AutoTemporarySavedWithFilesTargetForm
        // sounds more like an event than as a command, but let's not try to fix SCONE here.)
        // Since we don't have a command bus in form-bundle, I'll just use a member function.

        $this->addUploadedFilesToAutoSavedTemplateTarget($formAccess->getPrimaryAccess(), $savedFiles);

        return new DoFileAnswerUploadSuccessResponse($savedFiles);
    }

    /**
     * @throws Access\InvalidAccessArgumentException
     * @throws MissingVersionException
     * @throws \Exception
     */
    public function getDownloadFileQuestionAnswerResponse(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        string $nodeUid,
        string $fileUid,
        ?\DateTimeInterface $calculatedStepsCheckDate,
        ?\DateTimeInterface $secondaryCalculatedStepsCheckDate,
    ): AbstractDoResponse {
        Assert::notEmpty($nodeUid);
        Assert::notEmpty($fileUid);
        $this->assertBuildRequirements($request, $template, $calculatedStepsCheckDate);
        $doAccessDeniedResponse = $this->getOneOrNullPreliminaryDoAccessDeniedResponse($template, $clientWithTargetAccess);
        if ($doAccessDeniedResponse instanceof DoAccessDeniedResponse) {
            return $doAccessDeniedResponse;
        }

        $formAccess = $this->getNullOrFormAccessForSingleClientAccessingRoleInRequest(
            $request,
            $template,
            $clientWithTargetAccess,
            $calculatedStepsCheckDate,
            $secondaryCalculatedStepsCheckDate
        );

        if (!$formAccess instanceof ClientTemplateTargetFormAccess) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP);
        }

        $primaryAccess = $formAccess->getPrimaryAccess();
        $uploadNode = $this->getReachableUploadNodeOrNull($primaryAccess, $nodeUid);
        if (!$uploadNode instanceof UploadInputNode) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_REQUESTS_INVALID_NODE);
        }

        // check if at least read (it's download so check read) access is given to upload node for user (role) in current step
        if (!$primaryAccess->isReadAccessAuthorized() || !$uploadNode->isStepRoleReadAccessGranted($primaryAccess->getOpenFlowStep(), $primaryAccess->getAccessingRole())) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP_TO_SPECIFIED_NODE);
        }

        if (!$primaryAccess->hasStoredTemplateTarget()) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::NON_EXISTING_FILE);
        }

        $savedFiles = $primaryAccess->getStoredTemplateTarget()->getSavedFiles()->getForInputNode($uploadNode);
        if ($savedFiles->isEmpty() || !$savedFiles->hasOneByUid($fileUid)
        ) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::NON_EXISTING_FILE);
        }

        $uploadedFile = $savedFiles->getOneByUid($fileUid)->getUploadedFile();

        return new DoFileAnswerDownloadSuccessResponse($uploadedFile);
    }

    private function getReachableUploadNodeOrNull(ClientCombinedAccessObject $accessObject, string $nodeUid): ?UploadInputNode
    {
        // get all inputNodes that are reachable (not directly hidden and not part of hidden category)
        $reachableInputNodes = $accessObject->getCurrentVersionFormList()->getFlattenedReachableParentalInputNodes();

        // check if a node exists and is reachable for given nodeUid
        if (!$reachableInputNodes->hasOneByNodeUid($nodeUid)) {
            return null;
        }

        $uploadInputNode = $reachableInputNodes->getOneByNodeUid($nodeUid);

        // check if node is an upload question
        if (!$uploadInputNode instanceof UploadInputNode) {
            return null;
        }

        return $uploadInputNode;
    }

    /**
     * Store uploaded files in the database, and add them to the (auto)saved template target.
     *
     * Note this does not change the given answer for the upload question!
     */
    private function addUploadedFilesToAutoSavedTemplateTarget(
        ClientCombinedAccessObject $accessObject,
        SavedFileCollection $savedFiles,
    ): void {
        Assert::true($accessObject->isWriteAccessAuthorized());

        if ($accessObject->hasStoredTemplateTarget()) {
            $storedTemplateTarget = $accessObject->getStoredTemplateTarget();
        } else {
            $storedTemplateTarget = new StoredTemplateTarget(
                $accessObject,
                [],
                true,
                true
            );
        }

        // The code below is expensive (slow), which is why uploading multiple files should happen
        // sequentially, see STAGES-3521.

        $storedTemplateTarget->addSavedFiles($savedFiles);

        // Don't update the answer for the node, because the user didn't click submit (temporarily/definitively).
        // Just make sure the files are here.

        $this->storedTemplateTargetRepository->save($storedTemplateTarget);
    }

    public function getUploadDirectory(): string
    {
        return $this->uploadDirectory;
    }
}
