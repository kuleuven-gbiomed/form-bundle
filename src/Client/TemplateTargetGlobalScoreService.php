<?php

declare(strict_types=1);

namespace KUL\FormBundle\Client;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\Access\ClientTemplateTargetFormAccess;
use KUL\FormBundle\Client\Access\InvalidAccessArgumentException;
use KUL\FormBundle\Client\Manage\FormManager;
use KUL\FormBundle\Client\Response\AbstractDoResponse;
use KUL\FormBundle\Client\Response\DoAccessDeniedResponse;
use KUL\FormBundle\Client\Response\DoGlobalScoreResponse\DoGlobalScoreCalculationHaltedWithFormValidationErrorsResponse;
use KUL\FormBundle\Client\Response\DoGlobalScoreResponse\DoGlobalScoreCalculationImpossibleWithInsufficientDataResponse;
use KUL\FormBundle\Client\Response\DoGlobalScoreResponse\DoGlobalScoreCalculationSuccessWithCalculatedChoiceOptionResultResponse;
use KUL\FormBundle\Client\Response\DoGlobalScoreResponse\DoGlobalScoreCalculationSuccessWithCalculatedNumberResultResponse;
use KUL\FormBundle\Client\Route\ClientRoutesBuilder;
use KUL\FormBundle\Client\Util\ValidationError\ValidationErrors;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\TemplateTarget\Calculation\AbstractGlobalScoreCalculator;
use KUL\FormBundle\Domain\TemplateTarget\Calculation\GlobalChoiceScoreValueCalculator;
use KUL\FormBundle\Domain\TemplateTarget\Calculation\GlobalNumberScoreValueCalculator;
use KUL\FormBundle\Domain\TemplateTarget\Calculation\GlobalScoreCalculatorFactory;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use KUL\FormBundle\Entity\SubmissionRepository;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Webmozart\Assert\Assert;

class TemplateTargetGlobalScoreService extends AbstractTemplateTargetService
{
    public function __construct(
        StoredTemplateTargetRepository $storedTemplateTargetRepository,
        SubmissionRepository $submissionRepository,
        ClientRoutesBuilder $clientRoutesBuilder,
        FormManager $formManager,
        string $defaultLocale,
        array $availableAdminRoles,
        int $autoSaveInterval,
        int $idleCheckInterval,
        TemplateRepository $templateRepository,
        private readonly TranslatorInterface $translator,
    ) {
        parent::__construct(
            $storedTemplateTargetRepository,
            $submissionRepository,
            $clientRoutesBuilder,
            $formManager,
            $defaultLocale,
            $availableAdminRoles,
            $autoSaveInterval,
            $idleCheckInterval,
            $templateRepository
        );
    }

    /**
     * @throws InvalidAccessArgumentException
     * @throws Route\InvalidClientRouteException
     * @throws MissingNodeException
     * @throws MissingVersionException
     */
    public function getGlobalScoreCalculatedValueResponse(
        Request $request,
        TemplateInterface $template,
        ClientWithTargetAccessInterface $clientWithTargetAccess,
        string $nodeUid,
        ?\DateTimeInterface $calculatedStepsCheckDate,
        ?\DateTimeInterface $secondaryCalculatedStepsCheckDate,
    ): AbstractDoResponse {
        Assert::notEmpty($nodeUid);
        $this->assertBuildRequirements($request, $template, $calculatedStepsCheckDate);
        $doAccessDeniedResponse = $this->getOneOrNullPreliminaryDoAccessDeniedResponse($template, $clientWithTargetAccess);
        if ($doAccessDeniedResponse instanceof DoAccessDeniedResponse) {
            return $doAccessDeniedResponse;
        }

        $formAccess = $this->getNullOrFormAccessForSingleClientAccessingRoleInRequest(
            $request,
            $template,
            $clientWithTargetAccess,
            $calculatedStepsCheckDate,
            $secondaryCalculatedStepsCheckDate
        );

        if (!$formAccess instanceof ClientTemplateTargetFormAccess) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP);
        }

        $primaryAccess = $formAccess->getPrimaryAccess();
        $globalScoreNode = $this->getWritableGlobalScoreNodeOrNull($primaryAccess, $nodeUid);

        // check if node found and node is a valid global score node ;)
        if (!$globalScoreNode instanceof ScorableInputNode) {
            return new DoAccessDeniedResponse('invalid global score node');
        }

        // check if write access is given to global score node for user (role) in current step
        if (!$globalScoreNode->isStepRoleWriteAccessGranted($primaryAccess->getOpenFlowStep(), $primaryAccess->getAccessingRole())) {
            return new DoAccessDeniedResponse(DoAccessDeniedResponse::CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP_TO_SPECIFIED_NODE);
        }

        $postUrl = $this->getAutoCalculateUrlForScorableNode(
            $request,
            $this->clientRoutesBuilder->getClientRouteDataSetForTemplate($primaryAccess->getTemplate()),
            $primaryAccess,
            $globalScoreNode
        );

        $form = $this->formManager->createFormByAccessObjectForGlobalScore($primaryAccess, $postUrl, $globalScoreNode, $request->getLocale());
        $this->formManager->handleRequest($form, $request);

        if ($form->isSubmitted() && $form->isValid()) {
            // fire up a calculator for the global score and the form values, containing the write-accessible submitted
            // 'fresh' values, merged with any old non-write-accessible previously submitted values.
            $calculator = $this->getCalculator($globalScoreNode, $primaryAccess, $form);

            // if calculation could not be executed (due to insufficient data: not all dependency question values are
            // submitted at this time in current or previous steps by same or other users...)
            if (!$calculator->canCalculateValue()) {
                return new DoGlobalScoreCalculationImpossibleWithInsufficientDataResponse(
                    $globalScoreNode,
                    $this->translator->trans(
                        'action.failedAutoCalculate.insufficientDataMessage',
                        ['%question%' => $globalScoreNode->getNestedLabel($request->getLocale())],
                        'kulFormCalculation'
                    )
                );
            }

            // all dependency question values known & valid; calculation can be executed. based on calculator, the
            // global score question to fill in in client side (with calculated value) will be either a number or a choice
            // (radio/select) question. in case of number question: calculated numeric value is all we need. in case of
            // choice question, the (uid of) the calculated choice-option node will be need to be passed to client side
            // so client side can mark correct choice option as checked/selected. (we can NOT (also) pass ther calculated
            // numeric value to client side for a choice question: user might not have right to see option score values!)
            if ($calculator instanceof GlobalNumberScoreValueCalculator) {
                return new DoGlobalScoreCalculationSuccessWithCalculatedNumberResultResponse($calculator, $request->getLocale());
            } elseif ($calculator instanceof GlobalChoiceScoreValueCalculator) {
                return new DoGlobalScoreCalculationSuccessWithCalculatedChoiceOptionResultResponse($calculator);
            }

            // just in case a new kind of scorable node and/or calculator is introduced during development lifecycle
            throw new \DomainException('Unknown & unhandled calculator class: '.$calculator::class);
        }

        // form contains invalid/missing values for one or more dependencies to which user currently has write access
        return new DoGlobalScoreCalculationHaltedWithFormValidationErrorsResponse(
            $globalScoreNode,
            new ValidationErrors($form, $request->getLocale())
        );
    }

    /**
     * @param string $nodeUid
     *
     * @return ScorableInputNode|null
     *
     * @throws \Exception
     */
    private function getWritableGlobalScoreNodeOrNull(ClientCombinedAccessObject $accessObject, $nodeUid)
    {
        // if an accessObject could be contructed, then user has general access at this point.
        // check if user has write access for roleSlug or first role for which user has access
        if (!$accessObject->isWriteAccessAuthorized()) {
            return null;
        }

        // get all inputNodes that are reachable (not directly hidden and not part of hidden category)
        $reachableInputNodes = $accessObject->getCurrentVersionFormList()->getFlattenedReachableParentalInputNodes();

        // check if a node exists and is reachable for given nodeUid
        if (!$reachableInputNodes->hasOneByNodeUid($nodeUid)) {
            return null;
        }

        /** @var ScorableInputNode $globalScoreNode */
        $globalScoreNode = $reachableInputNodes->getOneByNodeUid($nodeUid);

        // check if node is a fully valid global score
        if (!$globalScoreNode->operatesAsGlobalScoreNode()) {
            return null;
        }

        return $globalScoreNode;
    }

    /**
     * @throws MissingNodeException
     * @throws MissingVersionException
     */
    private function getCalculator(
        ScorableInputNode $globalScoreNode,
        ClientCombinedAccessObject $accessObject,
        FormInterface $form,
    ): AbstractGlobalScoreCalculator {
        if (!$accessObject->hasStoredTemplateTarget()) {
            return GlobalScoreCalculatorFactory::createFromGlobalScoreNodeAndScoreNodesAndFormValues(
                $globalScoreNode,
                $this->formManager->getFlattenedFormValues($form)
            );
        }

        return GlobalScoreCalculatorFactory::createFromGlobalScoreNodeAndScoreNodesAndFormValues(
            $globalScoreNode,
            $this->formManager->getFlattenedFormValuesMergedWithUnWritableAndUnReachableStoredValues(
                $form,
                $accessObject->getStoredTemplateTarget()
            )
        );
    }
}
