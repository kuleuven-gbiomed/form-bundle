<?php

declare(strict_types=1);

namespace KUL\FormBundle\DependencyInjection;

use KUL\FormBundle\Client\Route\DownloadFileQuestionAnswerClientRouteData;
use KUL\FormBundle\Client\Route\GlobalScoreCalculatedValueClientRouteData;
use KUL\FormBundle\Client\Route\OverviewTargetAccessViewClientRouteData;
use KUL\FormBundle\Client\Route\StoringAutoTemporarySaveClientRouteData;
use KUL\FormBundle\Client\Route\StoringSaveClientRouteData;
use KUL\FormBundle\Client\Route\StoringViewClientRouteData;
use KUL\FormBundle\Client\Route\UploadFileQuestionAnswerClientRouteData;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\ScalarNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    final public const DEPRECATED_CLIENT_ROUTES_KEYS = ['copy_sources', 'copy_view', 'copy_temporary_save'];

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('kul_form');
        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $this->addTranslationSection($rootNode);
        $this->addUploadSection($rootNode);
        $this->addRolesSection($rootNode);
        $this->addInputPurifyingSection($rootNode);
        $this->addAutoSaveSection($rootNode);
        $this->addTargetablesSection($rootNode);
        $this->addTemplateTypesSection($rootNode);
        $this->addBuilderSection($rootNode);
        $this->addFormActivitySection($rootNode);

        return $treeBuilder;
    }

    private function addTranslationSection(ArrayNodeDefinition $rootNode): void
    {
        // This used to be one chaining call, but since we can't really be sure that every
        // call returns something of the correct type, I split this up, and use
        // docblocks to indicate the expected types of the intermediate results.
        // FIXME: These are a lot of assumptions; this might need refactoring.

        /** @var ArrayNodeDefinition $arrayNodeDefinition */
        $arrayNodeDefinition = $rootNode
            ->children()
            ->arrayNode('translation')->isRequired();

        /** @var NodeBuilder $nodeBuilder */
        $nodeBuilder = $arrayNodeDefinition
            ->children()
            ->scalarNode('default_locale')->isRequired()->cannotBeEmpty()->info('default locale (e.g. en or nl) to use in questions & all other translatable template elements')->end();

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeBuilder
            ->arrayNode('available_locales')->isRequired()->requiresAtLeastOneElement()->prototype('scalar')->info('array of locales that will available to be used for providing translated questions, descriptions, etc.');

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end sub arrayNode available_locales prototype scalar
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end sub arrayNode available_locales
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode translation children
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode translation
        $nodeDefinition->end(); // end children
    }

    private function addUploadSection(ArrayNodeDefinition $rootNode): void
    {
        /** @var ArrayNodeDefinition $arrayNodeDefinition */
        $arrayNodeDefinition = $rootNode
            ->children()
            ->arrayNode('upload')->isRequired();

        /** @var NodeBuilder $nodeBuilder */
        $nodeBuilder = $arrayNodeDefinition
            ->children()
            ->scalarNode('submitted_files_dir')
            ->isRequired()
            ->cannotBeEmpty()
            ->info('full path (kernel dir) to directory where files uploaded as answers must be stored')
            ->end();

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeBuilder
            ->booleanNode('use_destination_sub_directories_based_on_file_name_letters')
            ->defaultFalse()
            ->info('uses (up to) the first three letters of the file name to store the file in (up to) 3 sub directories below the submitted_files_dir. for example file abcdefgh.jpeg will be stored under submitted_files_dir/a/b/c/abcdefgh.jpg')
            ->end();

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeBuilder
            ->booleanNode('remove_files_from_storage_when_resetting_submitted_forms')
            ->defaultTrue()
            ->info('(optional). remove the files that were uploaded and saved as upload-question-answers from storage when their associated submitted form record (@see StoredTemplateTarget) is reset (hard deleted from DB). default is true, always remove from storage. if set to false, the files will not be removed and be left orphaned in storage without connection to the reset/deleted StoredTemplateTarget. if you omit this setting it will default to true.')
            ->end();

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end();
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end();
        $nodeDefinition->end();
    }

    private function addRolesSection(ArrayNodeDefinition $rootNode): void
    {
        // FIXME: Those docblocks are a lot of assumptions; this might need refactoring.

        /** @psalm-suppress PossiblyNullReference,UndefinedInterfaceMethod todo */
        $rootNode
            ->children()
            ->arrayNode('roles')
            ->isRequired()
            ->children()
            ->arrayNode('available_roles')
            ->info($this->getAvailableRolesInfo())
            ->isRequired()
            ->requiresAtLeastOneElement()
            ->prototype('scalar')
            ->end()
            ->end()
            ->arrayNode('available_admin_roles')
            ->info($this->getAvailableAdminRolesInfo())
            ->isRequired()
            ->requiresAtLeastOneElement()
            ->prototype('scalar')
            ->end()
            ->end()
            ->arrayNode('roles_allowed_to_revert')
            ->info($this->getRevertRolesInfo())
            ->requiresAtLeastOneElement()
            ->arrayPrototype()
            ->enumPrototype()
            ->values([true, false])
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end();
    }

    private function getAvailableRolesInfo(): string
    {
        return 'array of all roles known (to the domain) in your app that will be available for participation in all templateTypes to -amongst other things - set access rights to questions. NOTE: the roles can be any strings you want. they do not have to be configured as security roles. this is a general setting to determine what roles you have available in your app for possible template participation. most common is to provide all roles you have in your app domain and/or want to use in kul_forms. if you leave a role out, then that role - in theory - will never be able to participate in any template form.';
    }

    private function getAvailableAdminRolesInfo(): string
    {
        return 'array of roles to identify admins and help building admin rights. any of these roles MUST also be a role in config setting available_roles. NOTE: bundle handles any role on the same level. an admin role is just like any other role. this set of admin roles is mainly to help in the admin-side to build templates and their access for roles.';
    }

    private function getRevertRolesInfo(): string
    {
        return "
            which roles can revert (reset and/or rollback) already filled-in (submitted) forms in the end-user side to a previous step in the form's workflow.
            per role, you determine if that role can reset (fully delete) a filled-in form to allow a complete do-over from the first step and/or can roll back to a preceding step.
            you can also indicate if that role is allowed to revert if the step to roll back to or the first step in case of a reset, is already passed its deadline when the revert is executed.
            usually, only admin roles are added here.
            this revert is not an admin side operation. revert is executed in the end-user side, and it requires the role to have access to the form & the relevant steps, based on the form's (question) (step-role-)permissions, which is identical to access determining when a user requests a form to be rendered or user submits, temp-saves, or uploads a file as a n upload question for the rendered form, etc.. It is thus the role that you pass in to the form-bundle upon requesting to render a form that will be used to check for revert access & consequently render revert options. Same when a revert action is to be executed. this basically means that you do not need to provide any additional paramConverter for revert, as this bundle will handle both rendering of the revert options and the access to the associated actions when executed.

            if you do not want to use, allow & display revert, you can omit 'roles_allowed_to_revert' param in your kul_form (yaml) config.
            if you do define the 'roles_allowed_to_revert' param in your config, then at least one role is required & will be asserted.
            for each role you define, you must also define the 4 sub key-value pairs 'reset', 'rollback', 'includeRevertToAccessDeniedSteps' and 'includeRevertToDeadlinedSteps', each with a boolean value.
            - reset: if role is allowed to reset
            - rollback: if role is allowed to roll back (to available steps for rollback)
            - includeRevertToAccessDeniedSteps: one or more steps available for rollback and/or first step for reset do not grant access to this role (not at least read access to at least one question in step). if this parameter is set to false, then those steps will not be made available for rollback/reset, and any reset/rollback action to those steps will be denied. usually, forms are build as such that admin roles have access to all steps, so this rarely has affect. it is not a problem if you allow rollback/reset to steps to which this role has no access, because other roles will have access (templates with steps without any role access are not publishable), it will just be that after such a rollback/reset, this roll-backing-user (with this role) will get an access denied page.
            - includeRevertToDeadlinedSteps: if a step available for rollback and/or first step for reset is no longer open (passed deadline) at time of request for rollback/reset, then rollback or reset to that (first) step is still perfectly possible, but once the rollback or reset is done and (same or other user) opens the form again, then it will not be opened in that step, nor will the form be answerable/editable in that step and instead the form will be immediately open in the next open step. such a rollback/reset could place the form is a sort of limbo, in which any subsequent submitting actions (especially by non)-admin users) would no longer trigger premature opening of next steps due to previous deadlined steps no longer be (able to be) fulfilled. example use case is when an admin 'forgets' to extend the deadline of such a step in admin-side (before doing the rollback) or admin first wants to rollback and then changes the deadline, etc. it is better to set this to FALSE and only allow super roles (developers, super admins) to allow this.

            example config (the - symbols represent (yaml) indentation (Symfony Configuration 'info' node can not deal with tabs, space or other indentation):
            (this is added in your form-bundle config under kul_form.roles)

            roles_allowed_to_revert:
            -- ROLE_ADMIN:
            ---- reset: true
            ---- rollback: true
            ---- includeRevertToAccessDeniedSteps: false
            ---- includeRevertToDeadlinedSteps: false
            -- ROLE_DEVELOPER:
            ---- reset: true
            ---- rollback: true
            ---- includeRevertToAccessDeniedSteps: true
            ---- includeRevertToDeadlinedSteps: true
        ";
    }

    private function addInputPurifyingSection(ArrayNodeDefinition $rootNode): void
    {
        // FIXME: Those docblocks are a lot of assumptions; this might need refactoring.

        /** @var NodeBuilder $nodeBuilder */
        $nodeBuilder = $rootNode
            ->children()
            ->arrayNode('input_purifying')->addDefaultsIfNotSet()
            ->children();

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeBuilder
            ->scalarNode('html_allowed_tags')->defaultValue('p,a[href|title|target|rel],ul,ol,li,strong,em,br')->info('list with HTML tags used to purify submitted values for textual (text/textarea/wysiwyg) questions.');

        /** @var NodeBuilder $nodeBuilder */
        $nodeBuilder = $nodeDefinition->end();

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeBuilder->scalarNode('html_allowed_frame_targets')->defaultValue('_blank,_self,_parent,_top')->info('list with HTML tag attributes used to purify submitted values for textual (text/textarea/wysiwyg) questions.');
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end();
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode input_purifying children
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode input_purifying
        $nodeDefinition->end(); // end children
    }

    private function addAutoSaveSection(ArrayNodeDefinition $rootNode): void
    {
        // FIXME: Those docblocks are a lot of assumptions; this might need refactoring.

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $rootNode
            ->children()
            ->arrayNode('auto_save')->addDefaultsIfNotSet()
            ->children()
            ->integerNode('interval_in_milliseconds')
            ->defaultValue(180000)
            ->min(10000)
            ->info('interval in milliseconds between automatic draft saving actions being triggered. defaults to 180000 milliseconds (3 minutes). minimum 10000 milliseconds (10 seconds)');
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end();
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode auto_save children
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode auto_save
        $nodeDefinition->end(); // end children
    }

    private function addFormActivitySection(ArrayNodeDefinition $rootNode): void
    {
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $rootNode
            ->children()
            ->arrayNode('form_activity')->addDefaultsIfNotSet()
            ->children()
            ->integerNode('idle_check_interval_in_minutes')
            ->min(5)
            ->defaultValue(60)
            ->info('interval in minutes between idle checks (inactivity in form). defaults to 60 minutes. minimum 5 minutes');
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end();
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode form_activity children
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode form_activity
        $nodeDefinition->end(); // end children
    }

    private function addTargetablesSection(ArrayNodeDefinition $rootNode): void
    {
        // FIXME: Those docblocks are a lot of assumptions; this might need refactoring.

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $rootNode
            ->children()
            ->arrayNode('targetables')->info('list of entities/objects in your app that are usable by this bundle as target entities/objects for form templates. elements in this list & their parameters will be validated on bundle compile')
            ->arrayPrototype(/* @var NodeDefinition $nodeDefinition */)
            ->children()
            ->scalarNode('class')->isRequired()->cannotBeEmpty()->info('fqcn reference to class of the targetable object/entity');

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end();
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode targetables arrayPrototype
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode targetables children
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode targetables
        $nodeDefinition->end(); // end children
    }

    private function addTemplateTypesSection(ArrayNodeDefinition $rootNode): void
    {
        // FIXME: Those docblocks are a lot of assumptions; this might need refactoring.

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $rootNode
            ->children()
            ->arrayNode('template_types')->info('list of configs for templates per targeting type (config both for client related stuff as well as for admin-side stuff). elements in this list & their parameters will be validated on bundle compile')
            ->arrayPrototype()
            ->children()
            ->append($this->getTemplateTypesTargetingTypeNode())
            ->append($this->getTemplateTypesClientNode())
            ->append($this->getTemplateTypesAdminNode());

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode template_types arrayPrototype
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode template_types children
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end arrayNode template_types
        /* @var NodeDefinition $nodeDefinition */
        $nodeDefinition->end(); // end children
    }

    private function addBuilderSection(ArrayNodeDefinition $rootNode): void
    {
        /** @psalm-suppress PossiblyNullReference,UndefinedInterfaceMethod todo */
        $rootNode
            ->children()
            ->arrayNode('builder')
            ->isRequired()
            ->children()
            ->scalarNode('logo')->isRequired()->cannotBeEmpty()
            ->end()
            ->arrayNode('available_permissions')->isRequired()
            ->scalarPrototype()->end()
            ->end()
            ->arrayNode('permissions_for_roles')
            ->arrayPrototype()
            ->enumPrototype()
            ->values(['WRITE', 'READ', 'NONE'])
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end();
    }

    private function getTemplateTypesTargetingTypeNode(): ScalarNodeDefinition
    {
        $node = new ScalarNodeDefinition('targeting_type');
        $node
            ->isRequired()
            ->cannotBeEmpty()
            ->info('unique type of template. groups templates and allows to query and use same set of client controller actions for each template of this targeting type.');

        return $node;
    }

    private function getTemplateTypesClientNode(): ArrayNodeDefinition
    {
        $node = new ArrayNodeDefinition('client');
        $node
            ->info('configuration for the end-users (client front) side linked to this targeting type')
            ->isRequired()
            ->children()
            // client-side routes
            ->append($this->getTemplateTypesClientRoutesNode())
            ->end(); // end sub arrayNode children

        return $node;
    }

    /**
     * the routes are only basically validated her. the full validation (a route exists for this route-name in your app
     * and it has the required variables, next to your own) is done on route-calls via @see ClientRoutesBuilder.
     *
     * routes that MUST be provided per template targeting type in your app with required route-variables for each template (targeting) type (or compile of bundle will fail):
     * - storing_view                               (get) client gets detail read-write combined (form) view for a template-target allowing client to see, submit, temp-save, export, copy, ...
     * - storing_save                               (post) client submits or temporary saves answers to questions to which client has write access for template-target in storing_view.
     * - storing_auto_temporary_save                (post AJAX) automated temporary saves answers to questions to which client has write access for template-target in storing_view.
     * - upload_file_question_answer                (post AJAX) client uploads a file selected as potential answer to a file-question for template-target in storing_view and couples file to that template-target.
     * - download_file_question_answer              (get) client downloads a file stored as submitted or temporary saved answer to a file-question for template-target.
     * - overview_target_access_view                (get AJAX) client gets (e.g. table-row) data-set/sub-view with link to a storing_view and optional extra info (e.g. workflow status) based on client access
     * - global_score_calculated_value              (post AJAX) client clicks a button linked to a global score question and gets (re-)calculated value for that question (or insufficient/invalid data messages)
     */
    private function getTemplateTypesClientRoutesNode(): ArrayNodeDefinition
    {
        $node = new ArrayNodeDefinition('routes');
        $node
            ->isRequired()
            ->info('required set of routes with specific names and specific variables for the client (end-user) side for this template (targeting) type')
            ->children()
            // FIXME: use container tags to inject all client routes.
            // IMPORTANT: If you want to add another route, don't forget to add it to ClientRoutesBuilder as well!
            ->append($this->getTemplateTypesClientRoutesStoringViewNode())
            ->append($this->getTemplateTypesClientRoutesStoringSaveNode())
            ->append($this->getTemplateTypesClientRoutesStoringAutoTemporarySaveNode())
            ->append($this->getTemplateTypesClientRoutesUploadFileQuestionAnswerNode())
            ->append($this->getTemplateTypesClientRoutesDowloadFileQuestionAnswerNode())
            ->append($this->getTemplateTypesClientRoutesGlobalScoreCalculatedValueNode())
            ->append($this->getTemplateTypesClientRoutesOverviewTargetAccessViewNode());

        // add the deprecated routes with a warning so implementing apps using the key won't fail but still get a warning
        foreach (self::DEPRECATED_CLIENT_ROUTES_KEYS as $routeKey) {
            $node->append($this->getTemplateTypesClientCopyRouteNode($routeKey));
        }

        $node->end(); // end children

        return $node;
    }

    private function getTemplateTypesClientCopyRouteNode(string $routeKey): ScalarNodeDefinition
    {
        $node = new ScalarNodeDefinition($routeKey);
        $node
            ->setDeprecated(
                'kuleuven/form-bundle',
                'v1.0 dev-master',
                'The '.$routeKey.' option is deprecated. Its associated copy functionality allowing end-users to copy answers '
                .'between filled-in forms was not fully implemented, unused and its dead code is removed. '
                .'You should remove usages of this key in your kul_form.yaml file '
                .'under client.routes in each targeting_type you defined under the main key template_types. '
                .'The value of this key likely points to a route & controller action in your app. '
                .'It is advised to remove this route & controller action as well, as they no longer - and never did - serve any purpose.'
            );

        return $node;
    }

    /**
     * @see StoringViewClientRouteData
     */
    private function getTemplateTypesClientRoutesStoringViewNode(): ScalarNodeDefinition
    {
        $node = new ScalarNodeDefinition('storing_view');
        $node
            ->isRequired()
            ->cannotBeEmpty()
            ->info('(get) route for client to get detail read-write combined (form) view for a template-target allowing client to see, submit, temp-save, export, copy, ...');

        return $node;
    }

    /**
     * @see StoringSaveClientRouteData
     */
    private function getTemplateTypesClientRoutesStoringSaveNode(): ScalarNodeDefinition
    {
        $node = new ScalarNodeDefinition('storing_save');
        $node
            ->isRequired()
            ->cannotBeEmpty()
            ->info('(post) route for client for (draft) submission of answers to questions to which client has write access for template-target in storing_view');

        return $node;
    }

    /**
     * @see StoringAutoTemporarySaveClientRouteData
     */
    private function getTemplateTypesClientRoutesStoringAutoTemporarySaveNode(): ScalarNodeDefinition
    {
        $node = new ScalarNodeDefinition('storing_auto_temporary_save');
        $node
            ->isRequired()
            ->cannotBeEmpty()
            ->info('(post AJAX) route for automated periodic saving draft answers to questions to which client has write access for template-target in storing_view.');

        return $node;
    }

    /**
     * @see UploadFileQuestionAnswerClientRouteData
     */
    private function getTemplateTypesClientRoutesUploadFileQuestionAnswerNode(): ScalarNodeDefinition
    {
        $node = new ScalarNodeDefinition('upload_file_question_answer');
        $node
            ->isRequired()
            ->cannotBeEmpty()
            ->info('(post AJAX) route for client to upload a file as potential answer to a file-question for template-target in storing_view and couples file to that template-target.');

        return $node;
    }

    /**
     * @see DownloadFileQuestionAnswerClientRouteData
     */
    private function getTemplateTypesClientRoutesDowloadFileQuestionAnswerNode(): ScalarNodeDefinition
    {
        $node = new ScalarNodeDefinition('download_file_question_answer');
        $node
            ->isRequired()
            ->cannotBeEmpty()
            ->info('(get) route for client to download a file stored as submitted answer or draft answer to a file-question for template-target.');

        return $node;
    }

    /**
     * @see GlobalScoreCalculatedValueClientRouteData
     */
    private function getTemplateTypesClientRoutesGlobalScoreCalculatedValueNode(): ScalarNodeDefinition
    {
        $node = new ScalarNodeDefinition('global_score_calculated_value');
        $node
            ->isRequired()
            ->cannotBeEmpty()
            ->info('(post AJAX) route linked to a global-score question for a client to get a (re-)calculated value for that question (or insufficient/invalid data messages) to prefil input field of that question in client-side code in storing_view with (re)calculated value as the (new) answer');

        return $node;
    }

    /**
     * @see OverviewTargetAccessViewClientRouteData
     */
    private function getTemplateTypesClientRoutesOverviewTargetAccessViewNode(): ScalarNodeDefinition
    {
        $node = new ScalarNodeDefinition('overview_target_access_view');
        $node
            ->isRequired()
            ->cannotBeEmpty()
            ->info('(get AJAX) route for usage in overviews on each available target for a client to get a (e.g. table-row) sub-view with a link to a storing_view and optional extra info(e . g . workflow status) based on and limited by client access to the target in the overview-template');

        return $node;
    }

    private function getTemplateTypesAdminNode(): ArrayNodeDefinition
    {
        $node = new ArrayNodeDefinition('admin');
        $node
            ->info('configuration for the admin side. mostly help parameters to help admins creating templates and/or enforce defaults for some template properties to be used for every template to be created for this targeting type')
            ->children()
            // admin-side pre set setting
            ->append($this->getTemplateTypesAdminPreSetSettingsNode())
            // admin-side pre fill setting
            ->append($this->getTemplateTypesAdminPreFillSettingsNode())
            // admin-side questions setting
            ->append($this->getTemplateTypesAdminQuestionSettingsNode())
            ->end(); // end sub arrayNode admin children

        return $node;
    }

    private function getTemplateTypesAdminPreSetSettingsNode(): ArrayNodeDefinition
    {
        // FIXME: Those docblocks are a lot of assumptions; this might need refactoring.

        $node = new ArrayNodeDefinition('pre_set_settings');

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $node
            ->info('(optional) settings to help enforce/set some template properties upon new templates creation of this targeting type, overriding admins the opportunity to set them in admin-side themselves')
            ->children()
            ->arrayNode('pre_set_hierarchical_roles_that_could_participate')->requiresAtLeastOneElement()->prototype('scalar')->info('(optional). if given, helps to force set the roles that could participate (in given hierarchical order) in the templates of this targeting types. dis-allowing admins to set or choose themselves. WARNING if set, then both kul_form.roles.available_roles and kul_form.admin.templateTypes.pre_fill_settings.available_roles_hierarchy should/will be ignored! WARNING: roles in this set are checked and asserted to be valid roles in your app via the kul_form.roles.available_roles setting');

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end sub arrayNode pre_set_hierarchical_roles prototype scalar
        /** @var NodeBuilder $nodeBuilder */
        $nodeBuilder = $nodeDefinition->end(); // end sub arrayNode pre_set_hierarchical_roles
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeBuilder->scalarNode('pre_set_category_uid')->cannotBeEmpty()->info('(optional). if given and category is found by uid in DB, helps to force set this as template category and do not show/allow admin to choose. WARNING: make sure it exists in DB when using this');
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end();
        $nodeDefinition->end(); // end children

        return $node;
    }

    private function getTemplateTypesAdminPreFillSettingsNode(): ArrayNodeDefinition
    {
        // FIXME: Those docblocks are a lot of assumptions; this might need refactoring.

        $node = new ArrayNodeDefinition('pre_fill_settings');
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $node
            ->info('(optional) settings to help to pre-fill some of the dropdowns/options in the admin side with custom and/or limited (dropdown) options. allows the admin to set some properties (usually role related properties) via these (limiting) set of options (usually roles) ')
            ->children()
            ->arrayNode('available_roles_hierarchy')->requiresAtLeastOneElement()->prototype('scalar')->info('(optional).  if given, used to pre-fill the roles_hierarchy dropdown for admins to choose from (based on the ordering in this array), instead of using ALL kul_form.roles.available_roles. basically an override to limit the roles the admin can select from. WARNING: any role in this set must still be an available role in your app via the kul_form.roles.available_roles setting.');
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end sub arrayNode available_roles_hierarchy prototype scalar
        /** @var NodeBuilder $nodeBuilder */
        $nodeBuilder = $nodeDefinition->end(); // end sub arrayNode available_roles_hierarchy
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeBuilder->arrayNode('available_admin_roles')->requiresAtLeastOneElement()->prototype('scalar')->info('(optional).  if given, used to pre-fill the admin-roles dropdown for admins to choose from, instead of using ALL kul_form.roles.available_admin_roles. basically an override to limit the admin-roles the admin can select from. WARNING: any role in this set must still be an available role in your app via the kul_form.roles.available_admin_roles setting.');
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end sub arrayNode available_admin_roles prototype scalar
        /** @var NodeBuilder $nodeBuilder */
        $nodeBuilder = $nodeDefinition->end(); // end sub arrayNode available_admin_roles
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeBuilder->arrayNode('available_roles_for_auto_write_access')->requiresAtLeastOneElement()->prototype('scalar')->info('(optional).  if given, used to pre-fill the roles dropdown for admins to choose from and for which each of the selected roles will be used to set write access automatically.WARNING: any role in this set must still be an available role in your app via the kul_form.roles.available_roles setting (or in the pre_fill_settings.available_roles_hierarchy if that is set).');
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end sub arrayNode available_roles_for_auto_write_access prototype scalar
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end sub arrayNode available_roles_for_auto_write_access
        /* @var NodeDefinition $nodeDefinition */
        $nodeDefinition->end(); // end children

        return $node;
    }

    private function getTemplateTypesAdminQuestionSettingsNode(): ArrayNodeDefinition
    {
        // FIXME: Those docblocks are a lot of assumptions; this might need refactoring.

        $node = new ArrayNodeDefinition('questions_settings');
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $node
            ->info('(optional) settings to help admins in admin-side with what could or could not with creating questions. ')
            ->children()
            ->arrayNode('allowed_questions')->requiresAtLeastOneElement()->prototype('scalar')->info('(optional).  if given, admins making questions in the admin side are/should be limited to only these questions. if not given, they can make all question types. NOTE: strings must be the TYPE from the InoutNodes.');

        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end sub arrayNode available_roles_for_auto_write_access prototype scalar
        /** @var NodeDefinition $nodeDefinition */
        $nodeDefinition = $nodeDefinition->end(); // end sub arrayNode available_roles_for_auto_write_access
        $nodeDefinition->end(); // end children

        return $node;
    }
}
