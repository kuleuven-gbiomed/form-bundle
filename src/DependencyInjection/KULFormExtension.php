<?php

declare(strict_types=1);

namespace KUL\FormBundle\DependencyInjection;

use KUL\FormBundle\Client\Route\ClientRoutesBuilder;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class KULFormExtension.
 */
class KULFormExtension extends Extension
{
    /**
     * override to match alias naming convention in Symfony 3.4 as well as prepare to enforce this alias in symfony 4.
     */
    public function getAlias(): string
    {
        return 'kul_form';
    }

    /**
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        // IMPORTANT: loading services at any time in this load method won't fail/break stuff. but for the services that
        // are configurable / overridable (and aliased further in this load method )in the app depending on this bundle,
        // it's important to load them directly to assure the same overridden service (if given) is always returned by
        // the alias and - even more important - always that service is injected in other services that depend on it.
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services/admin.yml');
        $loader->load('services/client.yml');
        $loader->load('form-types.yml');

        $configuration = $this->getConfiguration($configs, $container);
        /* @phpstan-ignore-next-line */
        if (!$configuration instanceof Configuration) {
            throw new \Exception('Form configuration not found.');
        }
        $config = $this->processConfiguration($configuration, $configs);

        // upload
        $configUpload = $config['upload'];
        $container->setParameter('kul_form.upload.submitted_files_dir', $configUpload['submitted_files_dir']);
        $container->setParameter('kul_form.upload.use_destination_sub_directories_based_on_file_name_letters', $configUpload['use_destination_sub_directories_based_on_file_name_letters']);
        $container->setParameter('kul_form.upload.remove_files_from_storage_when_resetting_submitted_forms', $configUpload['remove_files_from_storage_when_resetting_submitted_forms']);

        // translation
        $configTranslation = $config['translation'];
        $container->setParameter('kul_form.translation.available_locales', $configTranslation['available_locales']);
        $container->setParameter('kul_form.translation.default_locale', $configTranslation['default_locale']);

        // input purifying
        $configPurifying = $config['input_purifying'];
        $container->setParameter('kul_form.input_purifying.html_allowed_tags', $configPurifying['html_allowed_tags']);
        $container->setParameter('kul_form.input_purifying.html_allowed_frame_targets', $configPurifying['html_allowed_frame_targets']);

        // auto save
        $configAutoSave = $config['auto_save'];
        $container->setParameter('kul_form.auto_save.interval_in_milliseconds', $configAutoSave['interval_in_milliseconds']);

        // form_activity
        $configFormActivity = $config['form_activity'];
        $container->setParameter('kul_form.form_activity.idle_check_interval_in_minutes', $configFormActivity['idle_check_interval_in_minutes']);

        // roles
        $configRoles = $this->getValidConfigRolesOrFail($config['roles']);
        $container->setParameter('kul_form.roles.available_roles', $configRoles['available_roles']);
        $container->setParameter('kul_form.roles.available_admin_roles', $configRoles['available_admin_roles']);
        $container->setParameter('kul_form.roles.roles_allowed_to_revert', $configRoles['roles_allowed_to_revert']);

        // targetables
        $container->setParameter('kul_form.targetables', $this->getValidConfigTargetablesOrFail($config['targetables']));

        // templateTypes
        $container->setParameter('kul_form.template_types', $this->getValidConfigTemplateTypesOrFail(
            $config['template_types'],
            $configRoles['available_roles']
        ));

        // builder
        $container->setParameter('kul_form.builder.available_permissions', $config['builder']['available_permissions']);
        $container->setParameter('kul_form.builder.permissions_for_roles', $config['builder']['permissions_for_roles']);
        $container->setParameter('kul_form.builder.logo', $config['builder']['logo']);

        // TODO add the twig globals needed
    }

    //    public function prepend(ContainerBuilder $container)
    //    {
    //        // todo add & override orm mapping for template & targets e.g. https://codeburst.io/5-tips-for-developing-symfony-reusable-bundles-2276480e99a4
    //    }

    private function getValidConfigTemplateTypesOrFail(
        array $configTemplateTypes,
        array $availableRoles,
    ): array {
        /**
         * TODO a lot more to do for 'admin' keys and possibly 'client' keys.
         */
        $templateTypeConfigsPerTargetingType = [];
        $processedClientRoutes = [];

        foreach ($configTemplateTypes as $configTemplateType) {
            $data = [];
            $targetingType = $configTemplateType['targeting_type'];

            // for storage purposes in MySQL it should not exceed 255 chars
            if (mb_strlen((string) $targetingType) > 255) {
                throw new InvalidConfigurationException('targeting Type ['.$targetingType.'] configured as template type in [kul_form.template_types] may not exceed 255 chars, but its length is ['.((string) mb_strlen((string) $targetingType)).'] chars.');
            }

            // if targeting_type is used by another template type class, break it of:  one targeting type corresponds
            // to one template type to guarantee only one set of routes will be found for each targeting type when
            // building urls inside this bundle for templates!
            if (array_key_exists($targetingType, $templateTypeConfigsPerTargetingType)) {
                throw new InvalidConfigurationException('targeting Type ['.$targetingType.'] configured as template type in [kul_form.template_types] must be unique, but it is already used');
            }

            $data['admin'] = $this->getValidAdminDataOrFail($configTemplateType, $targetingType, $availableRoles);
            $clientData = $this->getValidClientDataOrFail($configTemplateType, $targetingType, $processedClientRoutes);
            $data['client'] = $clientData;
            $processedClientRoutes = array_merge($processedClientRoutes, $clientData['routes']);

            $templateTypeConfigsPerTargetingType[$targetingType] = $data;
        }

        return $templateTypeConfigsPerTargetingType;
    }

    private function getValidAdminDataOrFail(
        array $configAdminData,
        string $targetingType,
        array $availableRoles,
    ): array {
        // at time of writing, all admin data is optional, so check key
        $adminData = array_key_exists('admin', $configAdminData) ? $configAdminData['admin'] : [];

        $adminData['pre_set_settings'] = $this->getValidAdminPreSetDataOrFail(
            $adminData,
            $targetingType,
            $availableRoles
        );

        // TODO OTHER 'ADMIN' DATA STUFF

        return $adminData;
    }

    /**
     * NOTE: the @see Configuration will have asserted that a string exist for each required client.routes in the
     * config.yml of your app. but unfortunately, we can not check here or anywhere else during compile of this bundle,
     * if each of those strings is associated with a 'real', valid route in your app, let alone that that route has
     * the required route variables. so this will be done on-the-fly in @see ClientRoutesBuilder when calling
     * the routes for a template for the first time. regardless of which routes are needed upon
     * calling @see ClientRoutesBuilder::getClientRouteDataSetForTemplate() for a template, all routes required for this
     * template's @see TemplateInterface::getTargetingType() will be checked and asserted, so that we can catch quickly
     * if your app's config.yml has correct route names and avoid that an end user gets a whoops on an unchecked
     * route.
     */
    private function getValidClientDataOrFail(
        array $configTemplateType,
        string $targetingType,
        array $processedClientRoutes,
    ): array {
        // the client routes attached to a targeting type must be unique per targeting type! theoretically, could allow
        // re-use of same set of routes for different targeting types. although this is a generic bundle,it's far
        // more easier to control templates if each route inside a set of routes attached to a targeting type
        // will be used only by that targeting type. might/will result in devs having to copy paste
        // controller-route-actions for different targeting types, even if nothing has to be changed to the copied
        // controller-actions. but then again, if you would have a route has the same parameters and/or that 'acts'
        // the same for two different targeting type, the probably they should be reduced to just one targeting type
        // (it is also preferable, but definitely not required, that a route is unique inside one targeting type,
        // i.e. a route is preferably unique altogether)
        foreach ($configTemplateType['client']['routes'] as $routeKey => $routeName) {
            // ignore and remove the keys for the deprecated copy routes
            if (in_array($routeKey, Configuration::DEPRECATED_CLIENT_ROUTES_KEYS, true)) {
                unset($configTemplateType['client']['routes'][$routeKey]);
                continue;
            }

            if (in_array($routeName, $processedClientRoutes, true)) {
                throw new InvalidConfigurationException('the route name ['.$routeName.'] for route key ['.$routeKey.'] for targeting type ['.$targetingType.'] and configured in in [kul_form.template_types.x.client.routes.' /*                    . ' in [kul_form.template_types.' . $targetingType . '.client.routes.' . $routeKey . '],' */.$routeKey.'], is already assigned to another targeting type. a route(name) can not be shared between different targeting types. make sure each route(name) is used in only one targeting type!');
            }
        }

        return $configTemplateType['client'];
    }

    private function getValidAdminPreSetDataOrFail(
        array $adminData,
        string $targetingType,
        array $availableRoles,
    ): array {
        if (!array_key_exists('pre_set_settings', $adminData)) {
            return [];
        }

        $adminPreSetData = $adminData['pre_set_settings'];

        // only add pre-set data if explicitly set (helps to ignore in admin side if not exist)
        // but if it is set, then make sure it's valid
        if (array_key_exists('pre_set_hierarchical_roles_that_could_participate', $adminPreSetData)) {
            $adminPreSetData['pre_set_hierarchical_roles_that_could_participate'] =
                $this->getValidAdminSidePreSetHierarchicalRolesThatCouldParticipateDataOrFail(
                    $adminPreSetData,
                    $targetingType,
                    $availableRoles
                );
        }

        // TODO OTHER ADMIN PRE SET DATA

        return $adminPreSetData;
    }

    private function getValidAdminSidePreSetHierarchicalRolesThatCouldParticipateDataOrFail(
        array $adminPreSetData,
        string $targetingType,
        array $availableRoles,
    ): array {
        if (!isset($adminPreSetData['pre_set_hierarchical_roles_that_could_participate'])) {
            throw new InvalidConfigurationException('the optional roles to pre set the roles that could participate in admin side for targeting type ['.$targetingType.'], and configured in [kul_form.template_types.'.$targetingType.'.admin.pre_set_hierarchical_roles_that_could_participate] can not be empty and must contain at least one role (string) if set! if you do not want to use pre set roles, then omit this config key [kul_form.template_types.'.$targetingType.'.admin.pre_set_hierarchical_roles_that_could_participate] in you app config.');
        }

        $invalidPreSetRoles = [];
        // validate roles are all in available roles.
        foreach ($adminPreSetData['pre_set_hierarchical_roles_that_could_participate'] as $preSetRole) {
            if (!in_array($preSetRole, $availableRoles, true)) {
                $invalidPreSetRoles[] = $preSetRole;
            }
        }

        if (0 !== count($invalidPreSetRoles)) {
            throw new InvalidConfigurationException('each of the roles provided for targeting type ['.$targetingType.'] and configured in [kul_form.template_types.'.$targetingType.'.admin.pre_set_hierarchical_roles_that_could_participate] must be configured as an available role in [kul_form.available_roles], but the following roles are not configured as such: ['.implode('|', $invalidPreSetRoles).']');
        }

        return $adminPreSetData['pre_set_hierarchical_roles_that_could_participate'];
    }

    /**
     * assert any targetables objects/entities configured by app implement the interface and have a unique code.
     *
     * @see TemplateTargetableInterface::getTargetCode()
     *
     * NOTE: this bundle internally handles DB storing of stored data (answers, state, etc.) for a certain
     * entity/object (@see TemplateTargetableInterface) instance linked to a certain
     * template @see TemplateInterface in a uniform manner:
     * per combination of template and entity/object instance, their can only be one stored record (or none yet)
     * (stored record: holds the submitted answers, temporary saved answers, workflow state, user submissions, etc.
     * that are given/set for that entity/object instance, based on the template's configuration.)
     * assuming the entity/object instance has an unique identifier within the full app's domain and linking that
     * identifier in a stored record to a template is not sufficient and it is a very bad idea! e.g. the app could
     * use primary keys for its DB mapped targeted entities, which means the identifiers are not app wide unique!
     * hence to be able to uniquely identify a stored record for an entity/object instance within a template,
     * a code assigned to the entity/object class is used in conjunction with the entity/object instance's identifier
     * this method asserts that that code is unique per targeted entity/object class and in the same go checks if
     * a class überhaupt exists for the targeted entity/object class and that it implements the correct interface.
     */
    private function getValidConfigTargetablesOrFail(array $configTargetables): array
    {
        $targetablesConfigsPerCode = [];

        foreach ($configTargetables as $configTargetable) {
            $class = $configTargetable['class'];

            try {
                $reflection = new \ReflectionClass($class);
            } catch (\ReflectionException $exception) {
                // class does not exist or is (re)moved
                throw new InvalidConfigurationException($exception->getMessage().', but is configured as targetable (abstract) class or interface in [kul_form.targetables]! Did you remove or move this class?');
            }

            if (!$reflection->implementsInterface(TemplateTargetableInterface::class)) {
                $type = match (true) {
                    $reflection->isInterface() => 'interface',
                    $reflection->isAbstract() => 'abstract class',
                    default => 'class (object/entity)',
                };

                throw new InvalidConfigurationException('class ['.$class.'] configured as targetable '.$type.' in [kul_form.targetables] does not implement ['.TemplateTargetableInterface::class.']');
            }

            // if the targetable class is an abstract or an interface implementing TemplateTargetableInterface, then
            // each implementation of that class should have the same targetCode to avoid uniqueness issues when storing filled-in
            // forms using target code with target id (in DB table where filled-in forms are stored (kuleuven_form_stored_template_target)
            // each combo of target_code,target_id,template_id must be unique). but although it is possible to check each implementation, there
            // is no other way than getting all declared classes from implementing app here and filtering out the ones for that interface/abstract.
            // that is just way too heavy to do, knowing this check is done on each compiling of this bundle, and thus on
            // each request in the implementing app. therefore, if the class is an abstract or interface, we do not check
            // the uniqueness of the target code of each implementation to avoid performance issues.
            // another reason to not check this is because there are use cases in which you want to use a target code on
            // more than one class. e.g. if you have an entity that you want to use as targetable, but alongside that
            // entity, you also want to use a value object that is derived from that entity. depending on some
            // domain logic in your app, you pass either the entity or the value object in to the FormBundle as target.
            // in that case you would have the same target code (and the same target id) on both the entity and the
            // value object, because both point to the same target object (probably the entity) and must be stored as
            // the same filled-in form for such target in the db with same target code and id. in that case, you cannot
            // add both entity and value object in to the list of targetables, because this method would detect
            // non-unique usage of the targetcode. so you can then use an abstract on which you place the unique target
            // code (which the TemplateTargetableInterface enforces to be a public statis function). or use an interface
            // on the entity and value object. in both cases (abstract or interface), the uniqueness of the target code
            // is bypassed here. keep in min that in that case, that it is your responsibility in your implementing app
            // to make sure the target code is the same for all implementations of your interface or abstract.
            // it is also not required to provide each targetable class in this bundle's config. and technically
            // it is not a problem if you use the same target code on multiple targetable classes in you targetables,
            // or not the same target code on each implementation of a targetableabstract/interface), if you know the
            // combo target code + target id is always unique in your app.
            if (!$reflection->isInstantiable()) {
                continue;
            }

            /** @var TemplateTargetableInterface $instance */
            $instance = $reflection->newInstanceWithoutConstructor();
            $code = $instance::getTargetCode();

            // for storage purposes in MySQL where this code is used in an unique constraint alongside the
            // targetIdentifier, it should not exceed 255 chars
            if (mb_strlen($code) > 255) {
                throw new InvalidConfigurationException('Target code ['.$code.'] for class ['.$class.'] configured as targetable object/entity in [kul_form.targetables] may not exceed 255 chars, due to its intended use in MYSQL indexes, but its length is ['.((string) mb_strlen($code)).'] chars.');
            }

            // if code is used by another targeted class, can not & will not handle that complexity!
            if (array_key_exists($code, $targetablesConfigsPerCode)) {
                throw new InvalidConfigurationException('Target code ['.$code.'] for class ['.$class.'] configured as targetable object/entity in [kul_form.targetables] must be unique, but it is also assigned to class ['.$targetablesConfigsPerCode[$code]['class'].']');
            }

            // re-key on code for easier parameter access
            $targetablesConfigsPerCode[$code] = $configTargetable;
        }

        return $targetablesConfigsPerCode;
    }

    /**
     * validate the roles.
     *
     * admin roles must be in available roles to avoid having to merge both sets where needed (like when building
     * access based on roles). basically this is asserted so that the (this) developer has not to worry about both
     * sets when having to build @see TemplateInterface::getHierarchicalRolesThatCouldParticipate() and
     * only having to use the adminRoles to help admins to build the template's role access.
     * e.g. when presenting admin in admin-side with the option to select from available_roles to build
     * the @see TemplateInterface::getHierarchicalRolesThatCouldParticipate(), we can use the available_admin_roles
     * to exclude it from the roles that admin can select so that we are certain that the admin roles will
     * always be one of those @see TemplateInterface::getHierarchicalRolesThatCouldParticipate(). and that will
     * guarantee that we can then use those available_admin_roles to build the write access right on each question
     * automatically without having to rely on the admin to given himself and other admin roles write access on
     * questions as soon as they set write access to that question for any other, non-admin role.
     * same for automatically adding admin roles to @see ChoiceInputNode::getRolesAllowedToSeeOptionScoreValues()
     * without having to worry that these admin roles are not part of the available roles. etc..
     */
    private function getValidConfigRolesOrFail(array $configRoles): array
    {
        $allRoles = $configRoles['available_roles'];
        /** @var string[] $adminRoles */
        $adminRoles = $configRoles['available_admin_roles'];
        // admin roles must be in available roles to avoid further confusion and having to merge both sets where needed.
        $invalidAdminRoles = [];
        foreach ($adminRoles as $adminRole) {
            if (!in_array($adminRole, $allRoles, true)) {
                $invalidAdminRoles[] = $adminRole;
            }
        }

        if (0 !== count($invalidAdminRoles)) {
            throw new InvalidConfigurationException('invalid admin role(s) in config parameter [kul_form.available_admin_roles]: admin role(s) ['.implode(' | ', $invalidAdminRoles).'] must also be added as available role(s) in config parameter [kul_form.available_roles] but were not found');
        }

        return $configRoles;
    }
}
