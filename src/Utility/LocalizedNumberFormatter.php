<?php

declare(strict_types=1);

namespace KUL\FormBundle\Utility;

use KUL\FormBundle\Domain\Template\Element\Node\Rounding\RoundingModes;
use KUL\FormBundle\Validator\Constraints\NumberScaleValidator;

final class LocalizedNumberFormatter
{
    /** @var int */
    public const MAX_AMOUNT_DECIMALS = 15;

    /**
     * parameter $value: comment from @see \NumberFormatter::create() : "The value to format. Can be integer or float,
     * other values will be converted to a numeric value." but it's better to assert that value must be at least numeric
     * (otherwise the numberformatter will convert it, resulting in unwanted result like a 0 (zero) if value is NULL,etc...).
     * NOTE: this formatting uses the same 'scale' & 'roundingMode' logic as the PHP Numberformatter does.
     * NOTE: 'scale' & 'roundingMode' arguments are optional to allow (form) validation on user-inputted values where
     * no silent rounding is done but instead user are given opportunity to manually correct scale & rounding mode @see NumberScaleValidator.
     *
     * @param mixed    $value        numeric value
     * @param int|null $minimumScale format to at least this amount of decimals (adds zero's) (ignored if exact scale set)
     * @param int|null $exactScale   format to exact amount of decimals (rounds or adds zero's) (overrules minimal scale if set)
     * @param int|null $roundingMode rounds to specified mode if too many significant decimals (in accordance with scale rounding)
     *
     * @phpstan-param RoundingModes::ROUND_* $roundingMode
     *
     * @throws \Exception
     */
    public static function format(
        mixed $value,
        string $locale,
        ?int $minimumScale = null,
        ?int $exactScale = null,
        ?int $roundingMode = null,
    ): string {
        if (!is_numeric($value)) {
            throw new \InvalidArgumentException('the value to format must be numeric got ['.(is_object($value) ? $value::class : gettype($value).']'));
        }

        // for scale, the formatter will '...ignore negative values but do not raise an error...' -> we enforce a positive
        if (!(null === $exactScale) && !($exactScale >= 0)) {
            throw new \InvalidArgumentException('exact scale must be a positive integer or zero. got a negative integer.');
        }

        // for scale, the formatter will '...ignore negative values but do not raise an error...' -> we enforce a positive
        if (!(null === $minimumScale) && !($minimumScale >= 0)) {
            throw new \InvalidArgumentException('minimum scale must be a positive integer or zero. got a negative integer.');
        }

        // formatter always uses ROUND_HALFEVEN as default roundingMode -> override and use default (HALF_UP)
        $roundingMode ??= RoundingModes::DEFAULT;
        // rounding mode comes from question, the value can/should be only one of the allowed (PHP) Rounding Modes -> assert
        if (!in_array($roundingMode, RoundingModes::getAllowedModes(), true)) {
            throw new \InvalidArgumentException('rounding mode must be one of the allowed modes: '.implode(',', RoundingModes::getAllowedModes()).". got {$roundingMode}.");
        }

        $formatter = \NumberFormatter::create($locale, \NumberFormatter::DECIMAL);
        /** @psalm-suppress PossiblyNullReference */
        $formatter->setAttribute(\NumberFormatter::ROUNDING_MODE, RoundingModes::mapAllowedModeToNumberFormatterMode($roundingMode));

        // we allow users to use either comma or dot as decimal separation when inputting to accommodate both english and
        // 'non-english' users. however, choice of decimal separator also affects choice of thousands separator used in
        // digit grouping. so as compromise & keep it universal: no separator for thousands, millions, etc in formatting
        // for display usage.
        // notes: all numbers are stored in DB in english format (decimal point and dot thousands separator). this
        // formatter just makes sure they get displayed in a uniform way to all users. users using both a dot and comma
        // in a number in an input field, will always get an 'invalid number' validation number (via Symfony validation).
        // using no thousands separator avoids difference between what user inputs and what he gets back after submit.
        $formatter->setAttribute(\NumberFormatter::GROUPING_SEPARATOR_SYMBOL, 0);

        // if exact scale and/or minimal scale are higher that the (sensible) MAX, formatter will silently use MAX
        $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, self::MAX_AMOUNT_DECIMALS);

        if (!(null === $minimumScale)) {
            $formatter->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, $minimumScale);
        }

        // if exact scale is set and minimal scale as well, the minimal scale will be ignored by the formatter
        if (!(null === $exactScale)) {
            $formatter->setAttribute(\NumberFormatter::FRACTION_DIGITS, $exactScale);
        }

        $formattedValue = $formatter->format(+$value);

        $valueString = (string) $value;
        if (!is_string($formattedValue)) {
            throw new \Exception('could not number format value ['.$valueString.']');
        }

        return $formattedValue;
    }
}
