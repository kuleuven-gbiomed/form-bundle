<?php

declare(strict_types=1);

namespace KUL\FormBundle\Utility;

class NumberScaleChecker
{
    /**
     * param $number: if value comes from user input via Symfony Contraint Validator: it could be int, float or even numeric string.
     */
    public static function doesNumberExceedMaxAmountOfDecimals(int|float|string $number, int $maxAmountOfDecimals): bool
    {
        return self::getAmountOfDecimalsForNumber($number) > $maxAmountOfDecimals;
    }

    /**
     * param $number: if value comes from user input via Symfony Contraint Validator: it could be int, float or even numeric string:
     * The NumberFormatter is smart enough to convert all of those values to correct numeric string, with the result always being
     * in the english notation: comma (,) for thousand(s) separator and dot (.) for decimals.
     *
     * Examples:
     *
     * user inputs: 0 OR 0.00 OR 0,00
     * => $number: 0.0
     * => $formattedValue: "0"
     * => $amountOfDecimals: 0
     *
     * user inputs: 2 OR 2.000 OR 2,000
     * => $number: 2.0
     * => $formattedValue: "2"
     * => $amountOfDecimals: 0
     *
     * user inputs: 20
     * => $number: 20.0
     * => $formattedValue: "20"
     * => $amountOfDecimals: 0
     *
     * user inputs: 2.01 OR 2,01 OR 2.01000 OR 2,01000
     * => $number: 2.01
     * => $formattedValue: "2.01"
     * => $amountOfDecimals: 2
     *
     * user inputs: 2.0001 OR 2,0001 OR 2.0001000 OR 2,0001000
     * => $number: 2.0001
     * => $formattedValue: "2.0001"
     * => $amountOfDecimals: 4
     *
     * user inputs: 2000000 OR
     * => $number: 2000000.0
     * => $formattedValue: "2,000,000"
     * => $amountOfDecimals: 0
     *
     * user inputs: 2000000.1 OR 2000000.10 OR 2000000,1 OR 2000000,10
     * => $number: 2000000.1
     * => $formattedValue: "2,000,000.1"
     * => $amountOfDecimals: 1
     *
     * user inputs: 0.00000000123 OR 0,00000000123
     * => $number: 1.23E-9
     * => $formattedValue: "0.00000000123"
     * => $amountOfDecimals: 11
     */
    public static function getAmountOfDecimalsForNumber(int|float|string $number): int
    {
        if (!is_numeric($number)) {
            throw new \InvalidArgumentException('value for number is not numeric');
        }

        // for internal, uniform validation, we format explicitly with point as decimal symbol (i.e. using locale 'en').
        // if value is a numeric string instead of int/float, NumberFormatter is smart enough to deal with it and always
        // convert value to correct numeric string. same if in scientific format (string or float). There is no chance on
        // 'false zero' after format, because non-numeric values are caught above. formatter will also drop any trailing
        // zero's. if only zero's after comma, formatter will convert to numeric (integer) string and pass.
        // using default rounding with no limit on scale (amount of decimal) at this point: need the value formatted to
        // a string with its original amount of decimals to compare max amount of decimals in regex after the formatting.
        // any rounding of last decimal does not matter here: method doesn't care about which decimals, only amount of them.
        $formattedValue = LocalizedNumberFormatter::format($number, 'en');

        // formatted value in english format: commas (,) for thousand(s) separator and dot (.) for decimals separator.
        $decimalsString = strrchr($formattedValue, '.');

        return match ($decimalsString) {
            false => 0,  // if no decimals in formatted number, strrchr will not have found the dot (.) needle and returned FALSE
            default => strlen(substr($decimalsString, 1)), // strrchr will have returned substring. e.g. .123
        };
    }
}
