<?php

declare(strict_types=1);

namespace KUL\FormBundle\Utility;

use KUL\FormBundle\Entity\UploadedFile;
use Stringy\Stringy as S;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Http response for a file download.
 *
 * File copied and adapted from SCONE.
 */
final class DownloadFileResponse extends StreamedResponse
{
    public function __construct(UploadedFile $uploadedFile, string $storageDirectory)
    {
        // verify file hash, just for safety
        if (!$uploadedFile->verifyHash($storageDirectory)) {
            parent::__construct(null, self::HTTP_INTERNAL_SERVER_ERROR, []);

            return;
        }

        // allow to download large file thanks to the streaming
        parent::__construct(function () use ($uploadedFile, $storageDirectory): void {
            readfile($uploadedFile->getFilePath($storageDirectory));
        }, 200, []);

        $this->headers->set('Content-type', $uploadedFile->getMimeType());

        // The Content-Disposition is indicating if the content is expected to be displayed inline in the browser,
        // that is, as a Web page or as part of a Web page, or as an attachment, that is downloaded and saved locally.
        $this->headers->set(
            'Content-Disposition',
            $this->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $this->removeSlashes($uploadedFile->getOriginalName()),
                $this->createFilenameFallback($uploadedFile)
            )
        );
    }

    /**
     * Create filename fallback for ResponseHeaderBag::makeDisposition() method.
     *
     * @see ResponseHeaderBag::makeDisposition
     */
    protected function createFilenameFallback(UploadedFile $uploadedFile): string
    {
        return $this->removeSlashes(
            S::create($uploadedFile->getOriginalName())
                ->toAscii()
                ->replace('%', '-')->__toString()
        );
    }

    protected function removeSlashes(string $src): string
    {
        return S::create($src)
            ->replace('/', '-')
            ->replace('\\', '-')->__toString();
    }
}
