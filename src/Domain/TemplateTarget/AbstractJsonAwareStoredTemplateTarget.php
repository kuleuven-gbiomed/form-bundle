<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\Compress\CompressibleSavedFiles;
use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\Manage\FormManager;
use KUL\FormBundle\Client\Manage\FormValuesMerger;
use KUL\FormBundle\Client\Revert\FormAnswersRollbackHelper;
use KUL\FormBundle\Client\Revert\RollbackService;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Client\Storing\TargetReference;
use KUL\FormBundle\Client\Submission\SubmissionCollection;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepCollectionInterface;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Domain\TemplateTarget\Fulfilment\FulfilmentChecker;
use KUL\FormBundle\Domain\TemplateTarget\Update\AutoTemporaryUpdated;
use KUL\FormBundle\Domain\TemplateTarget\Update\TemporaryUpdated;
use KUL\FormBundle\Domain\TemplateTarget\Update\Updated;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Entity\SavedFile;
use KUL\FormBundle\Entity\Submission;
use Ramsey\Uuid\Uuid;

/**
 * Class AbstractJsonAwareStoredTemplateTarget.
 *
 * NOTE: scope of properties must be protected for mapping purposes of extending classes
 * NOTE: locally cached properties (prefixed with 'cached') are intentionally nullable for cache clearing & rebuilding
 * NOTE: scope of properties must be protected for mapping purposes of extending classes
 *
 * FIXME: This abstract class brings no added value.
 */
abstract class AbstractJsonAwareStoredTemplateTarget implements StoredTemplateTargetInterface
{
    protected string $id;
    protected TemplateInterface $template;
    /**
     * refers to a @see TemplateTargetableInterface::getTargetCode()
     * e.g.  a SCONE 'placement' and 'internship_student', a EQA 'datasheet' or 'setRound' or ....
     */
    protected string $targetCode;
    /**
     * refers to a @see TemplateTargetableInterface::getTargetIdentifier()
     * e.g.  a SCONE placement->id (PK) or internshipStudent->id (PK) or combo internshipPart->id (PK) + internshipStudent->id (PK) or ...
     * a EQA datasheet->uid or set-round->uid or ....
     * must be whatever that can uniquely identify a target in both your app and this bundle in combo with @see $targetCode.
     * why not just only an id? because if your app uses non-unique ids like auto-incremented database PK integers,
     * additional info is needed to uniquely identify between instances of this class, thus by using this targetCode.
     */
    protected string $targetId;

    protected int $lastUsedTemplateVersionNumber;

    protected string $lastUsedStepUid;
    /**
     * files uploaded for questions (@see UploadInputNode ) asking for files as 'answer(s)'.
     *
     * @var Collection<array-key, SavedFile>
     */
    protected ?Collection $savedFiles = null;

    protected string|false $serializedSubmittedFormValues;

    protected string|false $serializedTemporarySavedFormValues;

    protected string $serializedLatestSavedQuestionEntries;

    protected string $serializedLatestTemporarySavedQuestionEntries;

    /**
     * tracking info (& history) of which user submitted which nodes with what value, at what time & in what version.
     *
     * @var Collection<array-key, Submission>
     */
    protected Collection $submissions;

    /**
     * tracking info (& history) of which user temporary submitted which nodes with what value, at what time & in what version.
     *
     * @var Collection<array-key, Submission>
     */
    protected Collection $temporarySubmissions;

    public function __construct(
        ClientCombinedAccessObject $accessObject,
        array $formValues,
        bool $isTemporarySave,
        bool $isAutoSave,
    ) {
        $this->id = Uuid::uuid4()->toString();
        $this->template = $accessObject->getTemplate();
        $target = $accessObject->getClientWithTargetAccess()->getTarget();
        $this->targetCode = $target::getTargetCode();
        $this->targetId = $target->getTargetIdentifier();
        $this->submissions = new SubmissionCollection();
        $this->temporarySubmissions = new SubmissionCollection();
        $this->serializedSubmittedFormValues = json_encode([], \JSON_THROW_ON_ERROR);
        $this->serializedTemporarySavedFormValues = json_encode([], \JSON_THROW_ON_ERROR);
        $this->serializedLatestSavedQuestionEntries = json_encode([], \JSON_THROW_ON_ERROR);
        $this->serializedLatestTemporarySavedQuestionEntries = json_encode([], \JSON_THROW_ON_ERROR);

        if ($isTemporarySave) {
            $this->updateTemporary($accessObject, $formValues, $isAutoSave);
        } else {
            $this->update($accessObject, $formValues);
        }
    }

    /**
     * @throws LocalizedStringException
     * @throws MissingVersionException
     */
    public function update(ClientCombinedAccessObject $accessObject, array $formValues): void
    {
        $this->validateAccessObject($accessObject);
        $updated = Updated::createFromAccessObject(
            $accessObject,
            $formValues,
            $accessObject->getClientWithTargetAccess()->getCurrentDate()
        );

        $this->lastUsedTemplateVersionNumber = $updated->getVersionNumber();
        $this->serializedSubmittedFormValues = json_encode($updated->getFormValues(), \JSON_THROW_ON_ERROR);

        $submission = $updated->getSubmission();
        $submissionEntries = $submission->getSubmitHistoryForFormList($accessObject->getCurrentVersionFormList());
        // add/update last save action for each question touched by this save action
        $this->serializedLatestSavedQuestionEntries = json_encode(
            $this->getLatestSavedQuestionEntriesForCurrentFormList()->getLatestPerQuestionUpdatedByEntries($submissionEntries),
            \JSON_THROW_ON_ERROR
        );
        $this->submissions->add($submission);
        $this->lastUsedStepUid = $this->getNextOrUsedFlowStepForLastKnownWorkflowStatusUpdate(
            $accessObject,
            $updated->getFormValues()
        )->getUid();
    }

    /**
     * - $autoSaved: auto temporary saving is handled exactly like manual temporary saving. except for one difference
     * regarding logging submissions (what answers were auto temp saved when, by who, under what role, etc.):
     * storing a submission every time the same user with same role in same step submits (every x minutes), will result
     * in hundreds/thousands of submissions in the DB and very long submission history lists in view pages. the DB will
     * get very big, very fast. to avoid this overkill, the domain decision is made to only keep one auto-saved submission
     * in DB for each UNIQUE combination of template-target-user-role-step-version. hence this unique submission gets
     * updated on every auto-save with a new date and auto saved form values.
     *
     * - $formValues: the formValues passed in to this method and used in the Updated objects are already merged with stored
     * previously values for questions to which user had no write access when he loaded the form and triggered this (auto)
     * temporary save. @see FormManager::getFlattenedFormValuesMergedWithUnWritableAndUnReachableStoredValues()
     *
     * - about the additional formValues merging with stored values
     * ( @see FormValuesMerger::mergeNewFormValuesWithReachableWritableOldFormValues() )
     * when auto-temporary saving, only the questions to which user made changes in the answer(s) in the front-end
     * (compared to the answers when user loaded the form page) are submitted and thus present in the submitted
     * form values. this means there will be most likely questions to which user had write access at the time he
     * loaded the form, but for which no changed answer were submitted. i.e. some questions (with values) will not
     * have been in the request POST form-data. which in turn means those questions are not present in the form values
     * passed in to this method and Updated objects. so we check for those missing questions to which user had write access
     * and that were not submitted but for which a temporary answer was already stored by older temp save actions, and we
     * merge those old answers in the new data, to make sure they are not lost (deleted).
     * NOTE: at time of writing, only auto temporary save actions submit only questions to which user made changes,
     * while manual temporary save actions always submit all questions to which user has write access in the open step.
     * thus this merging will have no effect on the manual temporary save action, hence we only trigger it when needed
     */
    public function updateTemporary(
        ClientCombinedAccessObject $accessObject,
        array $formValues,
        bool $autoSaved,
    ): void {
        $submitDate = $accessObject->getClientWithTargetAccess()->getCurrentDate();
        $formList = $accessObject->getCurrentVersionFormList();
        $this->validateAccessObject($accessObject);

        if ($autoSaved) {
            $updated = AutoTemporaryUpdated::createFromAccessObject($accessObject, $formValues, $submitDate);
        } else {
            $updated = TemporaryUpdated::createFromAccessObject($accessObject, $formValues, $submitDate);
        }

        $this->lastUsedTemplateVersionNumber = $updated->getVersionNumber();

        $tempFormValues = $updated->getFormValues();
        // check & merge answers for missing questions to which user had write access and that were not submitted but
        // for which a temporary answer was already stored by older temp save actions. although it won't affect manual
        // temporary saving, this can only occur when auto temporary saving, so let's limit on that.
        if ($autoSaved) {
            $tempFormValues = FormValuesMerger::mergeNewFormValuesWithReachableWritableOldFormValues(
                $formList,
                $tempFormValues,
                $this->getTemporarySavedFormValues(),
                $accessObject->getOpenFlowStep(),
                $accessObject->getAccessingRole()
            );
        }

        $this->serializedTemporarySavedFormValues = json_encode($tempFormValues, \JSON_THROW_ON_ERROR);

        // technically speaking, we do not need to update the last known workflow status,
        // but it is better to do so, for those real edge case that multiple user(s) are tempSaving and submitting
        // at the same time. this could be left out, but it seems better to check if workflow status needs update
        // on every attempt to save (temporary or not) on this object.
        $this->lastUsedStepUid = $accessObject->getOpenFlowStep()->getUid();

        $submission = $updated->getSubmission();
        $submissionEntries = $submission->getSubmitHistoryForFormList($formList);
        // add/update last (auto) temporary save action for each question touched by this (auto) temporary save action
        $this->serializedLatestTemporarySavedQuestionEntries = json_encode(
            $this->getLatestTemporarySavedQuestionEntriesForCurrentFormList()->getLatestPerQuestionUpdatedByEntries($submissionEntries),
            \JSON_THROW_ON_ERROR
        );

        // only update submission if an auto save is triggered and an auto save submission for this unique
        // (template-target-)user-role-step-version already existed. in all other cases, add a new submission
        if ($autoSaved && $this->getTemporarySubmissions()->hasOneAutoSavedForAccessObject($accessObject)) {
            // Doctrine will take care of the DB updating via the many-to-many relation between Submission and AbstractJsonAwareStoredTemplateTarget
            $this->getTemporarySubmissions()->getOneAutoSavedForAccessObject($accessObject)->updateBySubmissionForAutoSave($submission);

            return;
        }

        // Doctrine will take care of the DB adding via the many-to-many relation between Submission and AbstractJsonAwareStoredTemplateTarget
        $this->temporarySubmissions->add($submission);
    }

    /**
     * @throws \InvalidArgumentException
     */
    protected function validateAccessObject(ClientCombinedAccessObject $accessObject): void
    {
        if (!($accessObject->getTemplate()->getId() === $this->getTemplate()->getId())) {
            throw new \InvalidArgumentException('Template on access object does not match Template on submittedForm.');
        }

        if (!$accessObject->hasStoredTemplateTarget()) {
            return;
        }

        if ($accessObject->getStoredTemplateTarget()->getId() === $this->getId()) {
            return;
        }

        throw new \InvalidArgumentException('StoredTemplateTarget on access object does not match this instance of StoredTemplateTarget.');
    }

    /**
     * @internal
     *
     * this will get either the step-used-in-update if it is still the currently open-step
     * or the next step if it is open by date
     * or the next step if it is not open by date, but open by the fact that it allows to start prematurely (before
     * its fixed or calculated start date) and/if step-used-in-update is fulfilled (fully submitted = all question
     * with write access for at least one role in that step are submitted at this point in time)
     *
     * NOTE: the step returned (and it will always return one) will be used to set a 'last known workflow status' on
     * this storage record for template-target
     * NOTE: given formValues are already updated by the @see update()
     * or @see updateTemporary() methods before calling this method
     *
     * FLOW TO DETERMINE if either used FormWorkFlowStep (usedStep) or next FormWorkFlowStep (nextStep) will be returned:
     * 1 if usedStep is lastStep
     *   => use usedStep again (regardless of it still being 'open' or not)
     * 2   using the nextStep (as retrieved based on usedStep)
     * 2.1 if NOT nextStep @see StepInterface::allowsToStartPrematurelyIfPreviousStepIsFulFilled()
     *     and nextStep HAS started by date
     *     => use nextSTep
     * 2.2 if NOT nextStep @see StepInterface::allowsToStartPrematurelyIfPreviousStepIsFulFilled()
     *     and nextStep HAS NOT started by date
     *     => use usedStep again
     * 2.3 if nextStep @see StepInterface::allowsToStartPrematurelyIfPreviousStepIsFulFilled()
     *     and usedStep is NOT fulFilled
     *     => use usedStep again
     * 2.4 if nextStep @see StepInterface::allowsToStartPrematurelyIfPreviousStepIsFulFilled()
     *     and usedStep IS fulFilled
     *     => use nextStep
     * 2.5 if used step is not last step but next step is not started yet nor can it start prematurely,
     *     => use usedStep
     *
     * FIXME: A convoluted function like this needs tests.
     *
     * @return StepInterface
     *
     * @throws LocalizedStringException
     * @throws MissingVersionException
     */
    private function getNextOrUsedFlowStepForLastKnownWorkflowStatusUpdate(
        ClientCombinedAccessObject $accessObject,
        array $formValues,
    ) {
        $usedFlowStep = $accessObject->getOpenFlowStep();
        $currentVersion = $accessObject->getTemplate()->getCurrentVersion();
        $steps = $currentVersion->getWorkFlow()->getSteps();

        if (!$steps->hasOneStepByUid($usedFlowStep->getUid())) {
            throw new \InvalidArgumentException(': step with uid '.$usedFlowStep->getUid().' does not exists in the current version WorkFlow of the template with id '.$this->getTemplate()->getId());
        }

        // if last step, return it (does not matter if this last step is ended by date: this is not concern of this
        // setting last used step status, and whether is still 'open by date' (not ended) or not will be checked
        // upon each front side access  anyway.  also not checking if step 'is started by date': it could be that
        // step was triggered to start prematurely, effectively bypassing it's (fixed or calculated) start date!)
        if ($steps->doesFlowFinishWithStep($usedFlowStep)) {
            return $usedFlowStep;
        }

        // basically same check as doesFlowFinishesWithStep, but cannot harm to double check
        if (!$steps->hasOneStepThatDirectlyFollowsStep($usedFlowStep)) {
            return $usedFlowStep;
        }

        $nextFlowStep = $steps->getOneStepThatDirectlyFollowsStep($usedFlowStep);
        $fixedCheckDate = $accessObject->getCheckDate();
        $calCheckDate = $accessObject->hasCalculatedStepReferenceDate() ? $accessObject->getCalculatedStepReferenceDate() : null;
        // at this point, it's not the first step anymore, so if a secondary calculated check date is used for access, use that one
        if ($accessObject->hasSecondaryCalculatedStepReferenceDate()) {
            $calCheckDate = $accessObject->getSecondaryCalculatedStepReferenceDate();
        }

        // if next step is not configured to allow to start prematurely if its previous step (= 'usedFlowStep' here)
        // is 'fulfilled', then don't bother checking if previous is fulfilled, but check if that next step has
        // 'started by date'. if so, return that next step, else return the used one (don't bother checking if next step
        // is not yet 'ended by date'; that does not matter for setting last used step status and will be checked upon
        // each end-user side access  anyway, and on update, we only check the used and next step, not further: that's
        // done upon each end-user side access)
        if (!$nextFlowStep->allowsToStartPrematurelyIfPreviousStepIsFulFilled()) {
            if ($nextFlowStep instanceof StepWithFixedDateStart && $nextFlowStep->isStartedByDate($fixedCheckDate)) {
                return $nextFlowStep;
            }

            // if next step is one using calculated dates, then the access object will have a calCheckDate
            if ($nextFlowStep instanceof StepWithCalculatedDateStart
                && $calCheckDate instanceof \DateTimeInterface
                && $nextFlowStep->isStartedByDate($fixedCheckDate, $calCheckDate)) {
                return $nextFlowStep;
            }

            // if next step not yet started, return the used one, regardless if that one is still open or not
            return $usedFlowStep;
        }

        // if next step is configured to start if its previous step (= 'usedFlowStep' here) is fulfilled, check
        // if conditions for premature start are met based on given formValues.

        // FIXME: Why can't we just use an empty RoleCollection?
        $rolesToExcludeForFulfilmentCheck = !$accessObject->getTemplateParticipatingAdminRoles()->isEmpty() ?
            $accessObject->getTemplateParticipatingAdminRoles() : null;
        $isUsedStepFullySubmitted = FulfilmentChecker::isStepFulfilledByFormValuesInPublishedVersion(
            $currentVersion,
            $usedFlowStep,
            $formValues,
            $rolesToExcludeForFulfilmentCheck
        );

        if ($isUsedStepFullySubmitted) {
            return $nextFlowStep;
        }

        // nextStep neither started by date nor conditions met for a premature start, return used step
        return $usedFlowStep;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTemplate(): TemplateInterface
    {
        return $this->template;
    }

    public function getTargetCode(): string
    {
        return $this->targetCode;
    }

    public function getTargetId(): string
    {
        return $this->targetId;
    }

    public function getTargetReference(): TargetReference
    {
        return TargetReference::fromCodeAndIdentifier($this->getTargetCode(), $this->getTargetId());
    }

    public function getLastUsedTemplateVersionNumber(): int
    {
        return $this->lastUsedTemplateVersionNumber;
    }

    /**
     * get last known state of the workflow.
     *
     * returns the last step that was set as workflow state upon last submit or temp save.
     * for any action on this target in the template, we will start checking and using the workflow's steps
     * from this last used step
     *
     * @throws MissingVersionException if for some reason (DB tampering, invalid actions on template) no current version exists
     */
    public function getLastUsedStep(): StepInterface
    {
        // if there was ever a submit or tempSave done, then that was done on a published template. the template
        // might be de-activated in meanwhile but there will still be a published version (otherwise let fail hard)
        $workflow = $this->getTemplate()->getCurrentVersion()->getWorkFlow();

        if ($workflow->hasOneStepByUid($this->lastUsedStepUid)) {
            return $workflow->getOneStepByUid($this->lastUsedStepUid);
        }

        // if a new version was published in meanwhile and the step no longer exist in that new version,
        // then we have a problem: the step will still exist in the workflow in previous, now archived published version
        // on the template. but what do we do: do we fail or do we return that step from the previous version? we can
        // not really do the latter, because it will be useless in the new version. then again, removing steps is a
        // really really bad idea and in the 1.5 years that the old code on which this is based, was used, it has
        // never happened (adding a step has happened rarely but has no such effect), so for now, throw an exception..
        if ($this->getLastUsedTemplateVersionNumber() === $this->getTemplate()->getCurrentVersionNumber()) {
            // this should never happen, unless someone started messing in DB JSON
            throw new \DomainException('no last used step with uid ['.$this->lastUsedStepUid.'] found in the current version workflow of template ['.$this->getTemplate()->getId().']. version data is corrupt!');
        }

        throw new \DomainException('no last used step with uid ['.$this->lastUsedStepUid.'] found in current workflow:  the template ['.$this->getTemplate()->getId().'] has been re-published to a newer version and this step is removed in this new current version. DEVELOPER NOTICE: can not handle a workflow state that points to a step that no longer exist. a step should not be allowed Tto be removed in a newly published version -> TODO try to catch/handle this somehow ');
    }

    public function getSavedFiles(): SavedFileCollection
    {
        // doctrine hydrates collection relation to generic ArrayCollection in property -> convert to child collection
        // But as long as doctrine is not configured for saved files
        // (see src/Resources/config/doctrine/StoredTemplateTarget.orm.xml, line #65),
        // we should handle the case that there are no saved files.

        if (!$this->savedFiles instanceof Collection) {
            return new SavedFileCollection();
        }

        if ($this->savedFiles instanceof SavedFileCollection) {
            return $this->savedFiles;
        }

        return new SavedFileCollection($this->savedFiles->toArray());
    }

    /**
     * retrieves the file(s) that are given as the last known non-temporary answer(s) to the given upload question.
     */
    public function getSubmittedFilesForUploadQuestion(UploadInputNode $uploadQuestion): SavedFileCollection
    {
        // check if upload question has an answer
        $allQuestionsAnswers = $this->getSubmittedFormValues();
        if (!array_key_exists($uploadQuestion->getUid(), $allQuestionsAnswers)) {
            return new SavedFileCollection();
        }

        // check if answer is  blank (possible if question is not required)
        $answer = $this->getSubmittedFormValues()[$uploadQuestion->getUid()];
        if (!is_string($answer) || '' === $answer) {
            return new SavedFileCollection();
        }

        return $this->getSavedFiles()
            ->getForInputNode($uploadQuestion)
            ->getWhereInUids(UploadInput::extractSavedFileUidsFromFormValue($answer));
    }

    public function getCompressibleSubmittedFilesForQuestion(
        UploadInputNode $uploadQuestion,
        string $submittedFilesDirectory,
        string $locale,
        ?int $limitQuestionLabelLength,
    ): CompressibleSavedFiles {
        $submittedFiles = $this->getSubmittedFilesForUploadQuestion($uploadQuestion);

        if ($submittedFiles->isEmpty()) {
            return CompressibleSavedFiles::empty();
        }

        return CompressibleSavedFiles::createFromSavedFiles(
            $submittedFiles,
            $uploadQuestion,
            $submittedFilesDirectory,
            $locale,
            $limitQuestionLabelLength
        );
    }

    public function getCompressibleSubmittedFilesForAllCurrentVersionUploadQuestions(
        string $submittedFilesDirectory,
        string $locale,
        ?int $limitQuestionLabelLength,
    ): CompressibleSavedFiles {
        $uploadQuestions = $this->getTemplate()
            ->getCurrentVersion()
            ->getFormList()
            ->getFlattenedParentalInputNodes()
            ->getUploadInputNodes();

        if ($uploadQuestions->isEmpty()) {
            return CompressibleSavedFiles::empty();
        }

        $compressibleSavedFiles = new CompressibleSavedFiles();

        /** @var UploadInputNode $uploadQuestion */
        foreach ($uploadQuestions as $uploadQuestion) {
            $questionCompressibleSavedFiles = $this->getCompressibleSubmittedFilesForQuestion(
                $uploadQuestion,
                $submittedFilesDirectory,
                $locale,
                $limitQuestionLabelLength
            );

            if ($questionCompressibleSavedFiles->isEmpty()) {
                continue;
            }

            $compressibleSavedFiles = $compressibleSavedFiles->mergedWith($questionCompressibleSavedFiles);
        }

        return $compressibleSavedFiles;
    }

    public function addSavedFiles(SavedFileCollection $savedFiles): void
    {
        $newSavedFiles = !$this->savedFiles instanceof Collection
            ? $savedFiles
            : $savedFiles->getMergedWith(new SavedFileCollection($this->savedFiles->toArray()));

        $this->savedFiles = $newSavedFiles;
    }

    public function getSubmittedFormValues(): array
    {
        // catch null & empty string values in DB. just in case (e.g. default null value in DB). constructor sets a
        // serialized empty array, thus JSON should always be serialized array. but I prefer to catch this explicitly
        if ('' === $this->serializedSubmittedFormValues || false === $this->serializedSubmittedFormValues) {
            return [];
        }

        return json_decode($this->serializedSubmittedFormValues, true, 512, \JSON_THROW_ON_ERROR);
    }

    public function getTemporarySavedFormValues(): array
    {
        // catch null & empty string values in DB. just in case (e.g. default null value in DB). constructor sets a
        // serialized empty array, thus JSON should always be serialized array. but I prefer to catch this explicitly
        if ('' === $this->serializedTemporarySavedFormValues || false === $this->serializedTemporarySavedFormValues) {
            return [];
        }

        return json_decode($this->serializedTemporarySavedFormValues, true, 512, \JSON_THROW_ON_ERROR);
    }

    public function getSubmissions(): SubmissionCollection
    {
        if ($this->submissions instanceof SubmissionCollection) {
            return $this->submissions;
        }

        // doctrine hydrates collection relation to generic ArrayCollection in property -> convert to child collection
        return new SubmissionCollection($this->submissions->toArray());
    }

    public function getTemporarySubmissions(): SubmissionCollection
    {
        if ($this->temporarySubmissions instanceof SubmissionCollection) {
            return $this->temporarySubmissions;
        }

        // doctrine hydrates collection relation to generic ArrayCollection in property -> convert to child collection
        return new SubmissionCollection($this->temporarySubmissions->toArray());
    }

    /**
     * @throws MissingVersionException
     * @throws \Exception
     */
    public function getSubmitHistoryForCurrentFormList(): SubmitHistory
    {
        return $this->getSubmissions()->getSubmitHistoryForFormList(
            $this->getTemplate()->getCurrentVersion()->getFormList()
        );
    }

    /**
     * @throws MissingVersionException
     * @throws \Exception
     */
    public function getTemporarySubmitHistoryForCurrentFormList(): SubmitHistory
    {
        return $this->getTemporarySubmissions()->getSubmitHistoryForFormList(
            $this->getTemplate()->getCurrentVersion()->getFormList()
        );
    }

    public function getLatestSavedQuestionEntriesForCurrentFormList(): SubmitHistory
    {
        // property added later. no non-empty string default value possible for TEXT fields in DB -> catch empty string
        if ('' === $this->serializedLatestSavedQuestionEntries) {
            return new SubmitHistory();
        }

        return SubmitHistory::fromFormListAndEntriesData(
            $this->getTemplate()->getCurrentVersion()->getFormList(),
            JsonDecoder::toAssocArray($this->serializedLatestSavedQuestionEntries)
        );
    }

    public function getLatestTemporarySavedQuestionEntriesForCurrentFormList(): SubmitHistory
    {
        // property added later. no non-empty string default value possible for TEXT fields in DB -> catch empty string
        if ('' === $this->serializedLatestTemporarySavedQuestionEntries) {
            return new SubmitHistory();
        }

        return SubmitHistory::fromFormListAndEntriesData(
            $this->getTemplate()->getCurrentVersion()->getFormList(),
            JsonDecoder::toAssocArray($this->serializedLatestTemporarySavedQuestionEntries)
        );
    }

    /**
     * @throws MissingVersionException
     * @throws LocalizedStringException
     */
    public function isFulfilledForStep(
        StepInterface $step,
        ?RoleCollection $rolesToExcludeFromQuestionRoleWriteAccessChecks,
    ): bool {
        return FulfilmentChecker::isStepFulfilledByFormValuesInPublishedVersion(
            $this->getTemplate()->getCurrentVersion(),
            $step,
            $this->getSubmittedFormValues(),
            $rolesToExcludeFromQuestionRoleWriteAccessChecks
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws MissingVersionException
     */
    public function isFulfilledForAllPrecedingStepsUpToAndIncludingStep(
        StepInterface $step,
        ?RoleCollection $rolesToExcludeFromQuestionRoleWriteAccessChecks,
    ): bool {
        return FulfilmentChecker::areStepAndPrecedingStepsFulfilledByFormValuesInPublishedVersion(
            $this->getTemplate()->getCurrentVersion(),
            $step,
            $this->getSubmittedFormValues(),
            $rolesToExcludeFromQuestionRoleWriteAccessChecks
        );
    }

    /**
     * this comment & method body has been copied from SCONE with minimal changes to keep originally intended logic working.
     *
     * get the steps to which a rollback, based on current version of the associated @see Template is possible & relevant,
     * starting from the current status (last used step) of this filled-in-form and going down all steps preceding
     * that last used step, all the way up to and including the first step in the form's FormWorkFlow.
     *
     * the list of steps where admins can roll back to is based on the goals for rollback, which basically does 3
     * major things. 1) it 'converts' submitted answers in to temporary answers. 2) it converts logs about submit actions
     * in to logs about temporary saved actions. 3) rewinds  the status of this form to the step to roll back to (status
     * = last used step as set by the last submit, either the step in which that action occurred or a next step if that
     * action triggered a next step to open up prematurely). what is changed/converted for all 3 goals is based on and
     * limited by the step to roll back to.
     *
     * any rollback is pointless if no answers were ever submitted (temp saved answers are irrelevant here) and the form
     * status shows that last used step is still the first step: the rollback would not change anything.
     * However, if no answers were ever submitted in any step, but last used step is NOT first step, then a rollback
     * becomes opportune, because although no answers or logs will be converted, the rollback will set the status back
     * to the step to roll back to, giving user(s) a chance to restart from that step. this is useful in cases where a
     * step 1 ended by deadline passed, without any users having done any actions in that step 1. because if a user then
     * temp saves in the step 2, the status of the form will be updated to step 2, since the action (temp save) is done
     * in step 2. it then becomes relevant to allow rollback to step 1, effectively updating the status back to step 1,
     * allowing users to restart from step 1. but that does assume that the deadline of step 1 was by then already
     * changed prolonged or will be changed/prolonged by an admin as well so that when a user accesses the form again,
     * it will not ope up in step 2 because step 1's deadline will not be over yet. if the deadline is not prolonged,
     * the rollback will be pointless, since the user accessing the form, will again open it in step 2 and any action
     * will again update it to step 2.
     * this means a rollback - especially when required because of passed deadlines - often has to go hand in hand with
     * an admin updating/prolonging the deadline of the step to which the rollback has to happen.
     *
     * rollback always depart from current version of the form's template (template-currentVersion-FormWorkFlow).
     * version changes are a common reason to do rollbacks. usually, version changes do not affect rollback options
     * if order of steps was not changed or any steps added. But if the last used step was deleted in the new version,
     * then the only rollback allowed will be to the current version's first step, simply because it would be way too
     * complicated to deal with that. at time of writing, no step was ever deleted, only new onces added in new versions.
     *
     * including the current, last used step as a valid rollback option is pointless and not relevant if no one ever
     * submitted any answers in that step; there is nothing to rollback! It could also be considered pointless
     * if there are already submitted answers but users can still edit in that step. because users can then change answers
     * without the need for a rollback. but such a rollback could be considered as a 'clean restart' of that current
     * step, with those already given submitted answers converted to temporary answers. and if there are already submitted
     * answers and users can no longer edit (when @see StepInterface::limitsEachUniqueUserToOnlyOneSubmitUnderRole
     * is true), then the rollback to current step becomes very relevant, so that users get their 'singleSubmit' right once more!
     * thus, rollback to current step is added if there are submitted answers, regardless if users can edit or not.
     * if this current step is the last step in workflow, that this rollback will most of the time not convert any
     * submitted answers in to temporary answers: simply because in the last step, all questions were answered in
     * preceding steps, making all questions not convertible. but rollback to the last step will still always convert
     * submission logs about submit actions into ones about temp save actions, which is important for the 'single submit'
     * access for users in that current step: it will give users the chance to re-use that one-time right to submit once more.
     */
    public function getStepsAvailableForRollback(): StepCollectionInterface
    {
        $currentWorkFlow = $this->getTemplate()->getCurrentVersion()->getWorkFlow();
        $steps = $currentWorkFlow->getSteps();
        $firstStep = $steps->getStartingStep();
        $lastStatusStep = $this->getLastUsedStep();

        // rollback is pointless if no answers were ever submitted (regardless of temp-saved answers) and form status
        // shows last action was in first step
        if (0 === count($this->getSubmittedFormValues()) && $lastStatusStep->getUid() === $firstStep->getUid()) {
            return $steps->withAllStepsRemoved();
        }

        // not dealing with deleted last used step in version changes -> only allow rollback to first step
        // if last used step no longer exists in current workflow, we have no idea of knowing what the position of that
        // deleted step would be in the current workflow. even if we could figure it out by comparing this workflow version
        // with the previous version, then still it would be too difficult/unsafe to find out what answers should be rolled back.
        if (!$currentWorkFlow->hasOneStepByUid($lastStatusStep->getUid())) {
            return $steps->withOnlyStep($firstStep);
        }

        // get all steps preceding the last status step, in order of ascending workflow
        $rollbackSteps = $currentWorkFlow->getSteps()->getSortedStepsThatPrecedeStep($lastStatusStep);

        // only relevant to add current step as rollback option if at least one user has submitted (thus not only temp saved) in the current step.
        // this also means that a form that is still in first step and there are submitted answers, then a rollback to that first step
        // will be available which will basically restart the first step with all submitted answers converted to temp saved ones.
        // this is intentional: allows users to re-submit answers in first step if they only had 'single-submit' rights in first step.
        // see \KUL\FormBundle\Domain\Template\Contract\StepInterface::limitsEachUniqueUserToOnlyOneSubmitUnderRole
        if (!$this->getSubmissions()->getForStep($lastStatusStep)->isEmpty()) {
            $rollbackSteps->withNewStepAddedAtEnd($lastStatusStep);
        }

        return $rollbackSteps;
    }

    /**
     * this comment & method body has been copied from SCONE with minimal changes to keep originally intended logic working.
     *
     * after execution, need to save this instance to DB -> use @see RollbackService::rollBackStoredTemplateTargetFormToStep.
     *
     * rollback to given step, with converting submitted answers given in current step, given step and all steps in
     * between, in to temporary answers, with converting logs about submit actions in current step, given step and all
     * steps in between, in to logs about temporary saved actions and deleting (auto-calculated) global score answers
     * were necessary to allow those to be re-calculated.
     *
     * the submitted answers that are converted are only those of questions that were NOT answered in one of the steps
     * preceding the given step to roll back to. the logs that need converting to logs about temporary save actions
     * are only those about submit actions done in given step to rollback to and the steps following that step.
     *
     * @see FormAnswersRollbackHelper
     */
    public function rollBackToStep(StepInterface $stepToRollbackTo): void
    {
        $template = $this->getTemplate();
        $currentVersion = $template->getCurrentVersion();
        $currentWorkFlow = $currentVersion->getWorkFlow();

        // check if rollback is relevant. if not relevant, throw exception, cause then someone messed with a url or so..
        if (!$this->getStepsAvailableForRollback()->hasOneStepByUid($stepToRollbackTo->getUid())) {
            throw new \DomainException('step ['.$stepToRollbackTo->getUid().' - '.$stepToRollbackTo->getLabel('en').'] is not available as rollback option.');
        }

        // overwrite status (last used step) to given step, deliberately bypassing normal status update
        $this->lastUsedStepUid = $stepToRollbackTo->getUid();
        // set form version used for the rollback, to the current version if it was a different version
        if ($this->lastUsedTemplateVersionNumber !== $currentVersion->getVersionNumber()) {
            $this->lastUsedTemplateVersionNumber = $currentVersion->getVersionNumber();
        }

        // get an updated set of the temporary saved answers with relevant submitted answers converted to temp saved answers, based on submission history
        $rolledBackAnswers = FormAnswersRollbackHelper::createFromStoredTemplateTarget($this)->getAnswersRolledBackToStep($stepToRollbackTo);

        // set the rolled back temporary form values, deliberately bypassing normal temporary form values update!
        $this->serializedTemporarySavedFormValues = json_encode(
            $rolledBackAnswers[FormAnswersRollbackHelper::KEY_ROLLED_BACK_TEMPORARY_SAVED_ANSWERS],
            \JSON_THROW_ON_ERROR
        );
        // set the rolled back submitted form values, deliberately bypassing normal submitted form values update!
        $this->serializedSubmittedFormValues = json_encode(
            $rolledBackAnswers[FormAnswersRollbackHelper::KEY_ROLLED_BACK_SUBMITTED_ANSWERS],
            \JSON_THROW_ON_ERROR
        );

        // convert save (non-temp save submit) action submissions that were done in or after given step, to a temp
        // save action submission, with an updated date so that they become the most recent temp save action logs,
        // tagged as converted by rollback. we only convert those about actions in and after the step-to-rollback-to
        // and skip all save action logs that were done in a preceding step.
        // note: to make sure all converted submissions are the most recent ones (thus more recent than any existing
        // (temp-)save action submissions) while also making sure their original chronological order is kept, we use date
        // NOW to guarantee recentness while adding a difference of 1 second on each. this way we guarantee the original
        // chronological order of the converted submissions is kept.
        // this does present a bit of an annoyance: it will create temp-save submissions that are some seconds in the future.
        // not ideal but in reality, there are never that many non-temp-save submissions and the chance a new submission
        // will be logged by a user's action within seconds after the rollback is executed is almost nonexistent: there
        // will not be that many converted submissions and more importantly the rollback is a last resort thing, it messes
        // up the logical flow of a form anyway and an admin only does rollback after warning users and only when really needed.
        $precedingStepsWorkFLow = $currentWorkFlow->getSteps()->getSortedStepsThatPrecedeStep($stepToRollbackTo);
        $additionalTempSaveSubmissionRollbackDate = new \DateTimeImmutable();
        // loop over each existing submission by ascending flow (the oldest first), so that we can convert each submission
        // in the existing chronological order, with a difference of a second between each
        foreach ($this->getSubmissions()->getSortedByAscendingSubmitDate()->toArray() as $submission) {
            if ($precedingStepsWorkFLow->hasOneStepByUid($submission->getStepUid())) {
                continue;
            }

            $additionalTempSaveSubmissionRollbackDate = $additionalTempSaveSubmissionRollbackDate->modify('+1 seconds');
            $submission->convertForRollbackByDate($additionalTempSaveSubmissionRollbackDate);
            $this->submissions->removeElement($submission);
            $this->temporarySubmissions->add($submission);
        }

        // - update the latest question tempSave action entries, based on the updated tempSave history.
        $this->serializedLatestTemporarySavedQuestionEntries = json_encode(
            $this->getTemporarySubmitHistoryForCurrentFormList()->getLatestPerQuestion(),
            \JSON_THROW_ON_ERROR
        );

        // - update the latest question save action entries, based on the updated save history.
        $this->serializedLatestSavedQuestionEntries = json_encode(
            $this->getSubmitHistoryForCurrentFormList()->getLatestPerQuestion(),
            \JSON_THROW_ON_ERROR
        );
    }
}
