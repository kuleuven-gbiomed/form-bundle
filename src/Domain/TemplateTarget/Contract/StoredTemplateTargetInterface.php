<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Contract;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\History\InputNodeSubmitEntry;
use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Client\Storing\TargetReference;
use KUL\FormBundle\Client\Submission\SubmissionCollection;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepCollectionInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;

/**
 * Interface StoredTemplateTargetInterface.
 *
 * root domain object alongside @see TemplateInterface
 *
 * this is the storage of a targetObject-template combo: the end-user (client) side of a @see TemplateInterface
 * that is linked to a 'target subject/object'. it holds the answers stored (i.e submitted and/or temporary saved)
 * for the form (questions) as build on the configurations of the related template, as well as the state of that form.
 * it stores the most recent submitted/tempSaved answers as given by a client for a target entity for questions
 * rendered for that last client, based on template-workflow-step-role-read-write-access, with logging & tracking
 * of all clients that had access at some point and have had submitted/tempSaved (updated) answers on same and/or
 * different questions in same and/or different template-workflow-step-role-read-write-access.
 *
 * the combination targetObject-template is unique: each target object/entity can only be coupled once to a template.
 * that coupling represents the storage space of submitted and/or temporary saved answers to the questions in the
 * template for that target object/entity (this storage is extended with stuff like submit history, workflow state, etc.).
 * e.g. in SCONE there can be several templates with category 'mid-term evaluation' for an internship, resulting in
 * one (or none yet) placement-templateOfCategoryMidTermEvaluation combo for each unique placement and unique template
 * in that internship.
 *
 * Basically links a template to a target object and tracks answers given to questions as defined by the
 * template's form question list and with object as 'subject' for those questions. it's implemented instance is first
 * constructed on the moment that a user - with write access based on & restricted by the template's configuration - has
 * called for a form to be rendered for a target object and template, and successfully submitted or temporary saved
 * some answers with intention to 'store' those answers as submitted or temporary saved answers to the questions
 * he/she was allowed to answer at this point in the workflow for this TemplateTarget.
 * this stores the given - temporary and/or submitted - answers, where each question can have either one or none
 * temporary saved answer and one or none submitted answer. Note that a question is shared between users that have
 * access to it in the workflow: multiple users with write access in the same or different step(s) in the workflow,
 * can thus overwrite the (submitted or temp saved) answer. but this contract also tracks what answers to what questions
 * were  submitted or temporary saved by which users, in which workflow step, under what user-role, at what date-time and
 * in what version of the template. it also tracks where this target-template is in the workflow as defined by the template's workflow and based on the object's state (e.g the startDate of the period that 'opens' the object to
 * users, etc), and checks, gets & saves the - possibly next - current step upon each submit or temporary save
 * by a user for this targetObject-template combo. It uses that current step as starting point on each call to
 * this combo to load whatever is available/accessible in that step for the logged-in user, possibly skipping to a next
 * step if it that current step is closed by now and loading the stuff for that next step instead (but never going back
 * to previous step in the workflow, as that would eliminate the whole idea of a continuing workflow)
 *
 * e.g. the 'target' can be an entity like a Student that needs to be evaluated by a certain template. in that case
 * the @see TemplateInterface could be an instance of (e.g.) StudentTemplate with category (e.g.) 'evaluation'
 * and the @see StoredTemplateTargetInterface could be an instance of (e.g. StudentTemplateTarget).
 * if there is a need for another form that targets Student but with a different intention, there can be a same
 * StudentTemplate but with category (e.g.) 'academic preferences info'. the stored answers fot those template-student
 * combo forms will also be the same StudentTemplateTarget.
 * there could be a need for more specific Student evaluations.
 * i.e. the 'target' could be more complex. like a combination of a student for a certain faculty in a certain
 * academicYear. in that case there will need to be a new template instance like (e.g.) StudentFacultyYearTemplate
 * which targets (e.g.) StudentFacultyYearTemplateTarget. because in that case the target will have to have knowledge
 * of entities Student + Faculty + AcademicYear (where example StudentTemplateTarget only knows about Student)
 *
 * FIXME: Since these class contains form submissions for some target, I would rather name it TargetFormSubmissions
 * or FormSubmissionsForTarget or something like that.
 */
interface StoredTemplateTargetInterface
{
    public function getId(): string;

    public function update(ClientCombinedAccessObject $accessObject, array $formValues): void;

    public function updateTemporary(
        ClientCombinedAccessObject $accessObject,
        array $formValues,
        bool $autoSaved,
    ): void;

    public function getTemplate(): TemplateInterface;

    /**
     * refers to a @see TemplateTargetableInterface::getTargetCode()
     * e.g.
     * in SCONE for a placement as target it could be: 'placement' which ( set on the Placement entity)
     * or for an internshipStudent as target: 'internshipStudent'  ( set on the InternshipStudent entity)
     * in EQA for datasheet as target:  'datasheet' or if target is a labo in datasheet: 'datasheet_labo'
     * (set on the Datasheet object or DatasheetLabo Value Object,..).
     */
    public function getTargetCode(): string;

    public function getTargetReference(): TargetReference;

    /**
     * refers to a @see TemplateTargetableInterface::getTargetIdentifier()
     * e.g.
     * in SCONE for a placement as target in evaluation forms it could be: placement->id (PK) or a combo of
     * placement->internshipPart->id + placement->internshipStudent->student->id in case placement-wid can change
     * ( set on the Placement entity)
     * or for an internshipStudent as target for extra info forms: internshipStudent->id (PK)
     * ( set on the InternshipStudent entity)
     * in EQA for a datsheet as target: datasheet->uid or datasheetlabo->uid or ....
     * (set on the Datasheet object or DatasheetLabo Value Object,..).
     *
     * if implementing app uses integer primary keys, it will be converted to a string
     */
    public function getTargetId(): string;

    /**
     * keep track of @see TemplateInterface::getCurrentVersionNumber() last used on a submit or (auto) temp save.
     */
    public function getLastUsedTemplateVersionNumber(): int;

    /**
     * keep track of the step in the  template's current @see WorkFlow() that was last set on a succesful submit
     * or (auto) temp save.
     *
     * IMPORTANT: this is not necessary the step in which the submit was executed! it could be the next step!
     * if the step in which was submitted (but NOT temp saved) resulted in that  step being @see isFulfilledForStep
     * and the following step (if any) allows @see StepInterface::allowsToStartPrematurelyIfPreviousStepIsFulFilled()
     * then the step that will be stored in DB here will be that next step.
     * because this last used step is a sort of 'state' in the workflow. every time a form is called for a target
     * we check if a record - i.e. a DB saved record object mapped to this class - exist. if none exist (yet), then
     * it is safe to assume no one ever accessed, let alone submitted or temp saved, anything for this target. in
     * that case, we will call for an open step @see WorkFlow::getOneStepThatIsOpenByDate() for this target and
     * work with that step to render a form that user can use to submit or temp save in. (if no step found, then
     * obviously access is denied due to not open (yet or anymore)). if however a record exist for this target,
     * we use this 'last used' step to start with:
     * we check if that 'last used' step is still open (by using @see StepInterface::isEndedByDate()) and if so,
     * work with that step to render a form that user can use to submit or temp save in. if that 'last used' step
     * is no longer open, we do NOT go back to steps preceding this now closed 'last used' step! meaning we then
     * do NOT simply call @see WorkFlow::getOneStepThatIsOpenByDate() but instead, we check each and every following
     * step one by one in the correct ascending workflow order (@see StepCollectionInterface::getSortedByAscendingFlow())
     * and check until we find one that is open (by using @see StepInterface::isOpenByDate() on each of those steps).
     * if we find one, we then use that found step to work with to render a form that user can use to submit or temp
     * save in.
     *
     * so it is important to see that if a last used step is found (as set upon a last submit or temp save)
     * that step is considered blindly as the step to check from, without checking any previous steps. because
     * that allows us to save a step  as 'last used' step on a submit or temp save EVEN IF THAT STEP IS NOT EVEN BEGAN
     * based on @see isFulfilledForStep and on a next call start from that last used step to continue in the workflow
     * (@see isFulfilledForStep checks only submiited and not temp saved values, but on a temp save by a user, the
     * check using @see isFulfilledForStep is executed anyway. mainly because there is a chance that multiple users
     * with access to different and/or same questions in the same step are submitting and/or temp saving at +/-
     * the same moment and doing this check on every temp save as well helps to quickly trigger a next step in the
     * workflow so that we can send warning about step changes faster to the other multiple users, halting their
     * saving attempts quicker to avoid any overwriting of answers because of simultanuous savings. that said, other
     * hooks are in palce that catch this even before it gets here, but these hooks work better if this 'last used'
     * is updated as fast as possible. just as info: running this check on temp save is more or less paranaoid, as it
     * occurs really, really rarely and is catched sooner)
     */
    public function getLastUsedStep(): StepInterface;

    /**
     * files uploaded for questions (@see UploadInputNode ) asking for files as submitted or (auto) temp saved 'answer(s)'.
     */
    public function getSavedFiles(): SavedFileCollection;

    public function addSavedFiles(SavedFileCollection $savedFiles): void;

    /**
     * most recent - including hidden - json-decoded-to-array values for all submitted questions (= @see InputNode )
     * contains a single dimension array with one pair of uid (array key) and latest value (array value) per
     * inputNode that was at least once submitted with a valid (by inputNode constraints) value at the time of submission.
     * REMARK: submitted values are shared (visible and/or (re-)submittable) between all users with access to the
     * associated inputNode (business decision).
     */
    public function getSubmittedFormValues(): array;

    /**
     * most recent - including hidden - json-decoded-to-array values for all temporary saved questions (= @see InputNode )
     * contains a single dimension array with one pair of uid (array key) and latest value (array value)
     * per inputNode that was at least once temporary saved.
     * REMARK: temporary values do not necessarily have to be valid according to inputNode constraints
     * REMARK: temporary values are shared (visible and/or (re-)savable) between all users with access to the
     * associated inputNode (business decision).
     */
    public function getTemporarySavedFormValues(): array;

    /**
     * keep track of which user submitted which nodes with what value, at what time & in what version.
     */
    public function getSubmissions(): SubmissionCollection;

    /**
     * keep track of which user temporary saved which nodes with what value, at what time & in what version.
     */
    public function getTemporarySubmissions(): SubmissionCollection;

    /**
     * returns a filtered, sorted by date (and more human readable) history of who did what when,
     * based of the @see getSubmissions
     * and for the formList in the current version @see TemplateInterface::getCurrentVersion().
     */
    public function getSubmitHistoryForCurrentFormList(): SubmitHistory;

    /**
     * returns a filtered, sorted by date (and more human readable) history of who did what when,
     * based of the @see getTemporarySubmissions
     * and for the formList in the current version @see TemplateInterface::getCurrentVersion().
     */
    public function getTemporarySubmitHistoryForCurrentFormList(): SubmitHistory;

    /**
     * list of latest save action info per question (= @see InputNodeSubmitEntry ).
     * redundancy list for quickly tracking when the latest save action was done on each unique question,
     * by whom, when, under what role, in what version, etc.
     * the list is updated to the latest action info for each question upon each save action @see self::update(),
     * which updates/replaces the last known save action for any question that was submitted in that save action.
     *
     * note that the @see self::getSubmitHistoryForCurrentFormList contains all save action entries per question,
     * thus the whole action/log history, not only the latest, while this list only contains the latest save action
     * per question. this list, stored directly on the form-record, is more performant than querying and looking
     * through the whole action/log history. it also has the benefit that if the whole action/log history is not complete
     * (or even corrupted) for any reason, this list will always contain the last known save action per question.
     * this is important for determining which answer - the saved or the temporary saved one - that is to be shown in
     * an input field for each question in front-end. formerly, this was done by looking through and comparing the
     * saved & temporary saved SubmitHistory. but this list is more performant and less sensitive to incomplete logs.
     */
    public function getLatestSavedQuestionEntriesForCurrentFormList(): SubmitHistory;

    /**
     * list of last temporary save action info per question.
     * same as @see self::getLatestSavedQuestionEntriesForCurrentFormList but for temporary save actions.
     */
    public function getLatestTemporarySavedQuestionEntriesForCurrentFormList(): SubmitHistory;

    /**
     * check if all questions in given step that meet conditions are submitted.
     *
     * in human language: a step is considered fulfilled if all it's questions that can be answered (or given answer
     * updated) by at least one person who has the right to do so, are actually submitted. if one or more of these
     * questions is NOT required, then they still must be submitted by such a person so that we can track that that
     * question is 'fulfilled' also, regardless if it was validly answered blank or not.
     *
     * whether a step is fulfilled or not is of importance to see if a next step can start prematurely if configured so
     *
     * @see StepInterface::allowsToStartPrematurelyIfPreviousStepIsFulFilled()
     *
     * technically, it checks if all reachable questions in the template's current @see FormList to which at least
     * one role has write access in given step, are submitted.
     * which in practice means that the uid's of ALL @see FormList::getFlattenedReachableParentalInputNodes()
     * in that formlist that are also in the sub set @see InputNodeCollection::getWritableInStep() of that set
     * are found as keys in the single dimension array in @see getSubmittedFormValues() . regardless of their
     * answer value, because if a question is not required, it still needs to be submitted with or without answer.
     * the latter is a decision made because one could have a step with only non-required questions, and non-required
     * questions still have to be explicitly submitted (with blank values) to define a question as 'fulfilled'
     *
     * NOTE $rolesToExcludeFromQuestionRoleWriteAccessChecks are used to skip those questions in that step if write access
     * to those questions is ONLY granted for these given roles. if left empty or NULL, then the normal check on
     * all questions with at-least-role-has-write-access is used. this is mainly used when the given step is
     * set as @see StepInterface::skipAdminRolesForFulFilmentCheck() . in that case we skip the check on
     * questions that require to be submitted in that step by one or more roles where ALL those role(s) are
     * only available admin roles in your app as configured in the config.yml, then those questions are skipped
     * in the fulfillment check process. this comes in handy in steps (usually from the second step and further)
     * in the workflow that have questions that granted write access to some-admin roles in preceding steps,
     * but in this step only grant read or no access to those admin-roles and full write access to admin-roles
     * only to allow admins to change answers to questions that are by then 'closed' for write access for other
     * non-admin roles. because then, often / most of the time, you only consider the admins as backup people
     * to make changes by exception, not as people who have to 'do' something 'normal' in that step
     * NOTE: we can not use a bool 'skipAdminRolesForFulFilmentCheck' argument in this method, because then we
     * need to inject a config parameter 'available_admin_roles' from the container, which we can not or only
     * via dubious entity-listener do in an entity.
     */
    public function isFulfilledForStep(
        StepInterface $step,
        ?RoleCollection $rolesToExcludeFromQuestionRoleWriteAccessChecks,
    ): bool;

    /**
     * checks if for each step that precede the given step in the logical order of
     * the workflow (@see StepCollectionInterface::getSortedByAscendingFlow() ) and the given step itself
     * are fulfilled @see isFulfilledForStep and the.
     *
     * NOTE: depending how rights on questions in workflow, it could be that given step is fulfilled but
     * the ones preceding that stp were not. e.g. if one or more preceding steps were never submitted but
     * their deadline passed, causing those steps to close. then, in given step, some users did submit
     * all questions that are 'writable' in that step, causing that given step to be fulfilled. but
     * the preceding steps never were. sometimes you just want to know if just given step is fulfilled
     * (which is what is done when checking if a next step can @see StepInterface::allowsToStartPrematurelyIfPreviousStepIsFulFilled(),
     * but sometimes you want to check if all steps until then were also submitted, e.g. if you as an admin wants
     * to know quickly if all stakeholders in a form have done there job and some or all stakeholder with
     * the necessary rights have at least once filled in the questions in those steps.
     */
    public function isFulfilledForAllPrecedingStepsUpToAndIncludingStep(
        StepInterface $step,
        ?RoleCollection $rolesToExcludeFromQuestionRoleWriteAccessChecks,
    ): bool;

    /** @return StepCollectionInterface<array-key, StepInterface> */
    public function getStepsAvailableForRollback(): StepCollectionInterface;

    public function rollBackToStep(StepInterface $stepToRollbackTo): void;
}
