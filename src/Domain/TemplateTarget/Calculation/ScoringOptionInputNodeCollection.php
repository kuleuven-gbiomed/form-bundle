<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Calculation;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;

/**
 * @extends ArrayCollection<array-key, OptionInputNode>
 */
final class ScoringOptionInputNodeCollection extends ArrayCollection
{
    /**
     * @param OptionInputNode[] $elements
     */
    public function __construct(array $elements = [])
    {
        // all must have a scoring value
        foreach ($elements as $optionInputNode) {
            if (!$optionInputNode->hasScoringValue()) {
                throw new \InvalidArgumentException('all options ['.OptionInputNode::class.'] to use as collection of  scoring options must have a valid numeric scoring value. found none for option with uid ['.$optionInputNode->getUid().']');
            }
        }

        parent::__construct($elements);
    }

    /**
     * sort the OptionInputNodes by their score, from lower scoring to higher scoring.
     *
     * if multiple scoringOptionNodes have the same score (which is completely valid and not uncommon use case!),
     * the ordering on appearance in the collection will be kept basically assuming that those are added & ordered
     * in the collection as intended by the admin who added them
     */
    public function getSortedByAscendingScore(): self
    {
        /** @var \ArrayIterator<array-key, OptionInputNode> $iterator */
        $iterator = $this->getIterator();
        // Psalm complain about this, but it shouldn't. Suppress the error.
        $iterator->uasort(
            fn (OptionInputNode $a, OptionInputNode $b) => $a->getScoringValue() < $b->getScoringValue() ? -1 : 1
        );

        return new self(iterator_to_array($iterator));
    }

    /**
     * use the given score value to get the scoringOptionNode that scores the closest to that given score value,.
     *
     * if given score value is exactly between two scoringOptionNodes and the rounding is set to round half up,
     * then the scoringOptionNode that scores higher is taken, else the one that scores lower is taken
     *
     * throws exception if the collection is empty, because in a non-empty collection, there is always a closest node
     * for any given score value.
     *
     * NOTE: there could be more than one scoringOptionNode with the same score and thus more tan one could be closest
     * to given score value. if the given score value is no exact match, falls between two scoringOptionNodes and there
     * is more than one scoringOptionNode with the same score that are closest to given score vale, then there is no
     * real issue, since we just take the one closest in the 'natural' order of appearance.
     * if however there is an exact match for given score value to multiple scoringOptionNodes with that score,
     * then we use the rounding to determine which to return (as a domain decision): either take the last or the first
     * of that set of Options with same scores, based on the requested rounding being respectively up or down and based
     * on the sorting of those nodes as they appear in the collection.
     * This is because the order of appearance in the collection may or may not have significance for the end-users.
     * and/or for the admins that build this scoring options set for a choice question. the labels of two option nodes
     * with the same score will usually be different and hence could be meaningful in terms of 'scorable' value.
     *
     * @param float $score       given score value to find closest scoringOptionNode with
     * @param bool  $roundHalfUp if true and given score is exactly between two scoringOptionNodes, take the higher scoring one
     *                           FIXME: This kind of rounding is hacky; the use of a bool to select a rounding mode is confusing.
     *                           The function  supports ROUND_HALF_UP and ROUND_HALF_DOWN. It should ask the rounding mode as argument, and throw
     *                           an exception if the caller provides an unsupported rounding mode.
     *
     * @throws \BadMethodCallException if none is found (i.e. if the collection is empty)
     */
    public function getOneWhereScoreIsClosestTo(float $score, bool $roundHalfUp = true): OptionInputNode
    {
        //        // there will always be a closest option, except if the collection is empty
        //        if ($this->isEmpty()) {
        //            throw new \BadMethodCallException('cannot find a scoring option closest to score ['.((string) $score).']: there are no scoring options in the collection');
        //        }

        // try exact match first. if exact match for one or more than one, return last or first according to rounding
        $optionsWithExactMatch = $this->getWhereScoreEquals($score);
        $firstExact = $optionsWithExactMatch->first();
        $lastExact = $optionsWithExactMatch->last();
        // if $optionsWithExactMatch is not empty, there will be at least one option, hence both a first AND last option
        if ($firstExact instanceof OptionInputNode && $lastExact instanceof OptionInputNode) {
            return $roundHalfUp ? $lastExact : $firstExact;
        }

        // get the option with the nearest lower score and the option with the nearest higher score, using two sets of
        // options, one with scores lower than given score and one with scores higher than given score, then sort each
        // set by scores (and order of appearance for multiple options with same score), then taking last of the options
        // with lower scores and first of options with higher scores to get nearest lower resp. higher scoring option
        $nearestLowerScoringOption = $this->getWhereScoreIsLowerThan($score)->getSortedByAscendingScore()->last();
        $nearestHigherScoringOption = $this->getWhereScoreIsHigherThan($score)->getSortedByAscendingScore()->first();

        // if we have both a nearest lower and nearest higher scoring option, get closest, taking rounding in account
        if ($nearestLowerScoringOption instanceof OptionInputNode && $nearestHigherScoringOption instanceof OptionInputNode) {
            // if we have both a nearest lower and nearest higher scoring option, get closest, taking rounding in account
            $halfScoreBetween = ($nearestLowerScoringOption->getScoringValue() + $nearestHigherScoringOption->getScoringValue()) / 2.0;

            if ($halfScoreBetween === $score) {
                return $roundHalfUp ? $nearestHigherScoringOption : $nearestLowerScoringOption;
            }

            return $score < $halfScoreBetween ? $nearestLowerScoringOption : $nearestHigherScoringOption;
        }

        // if we have only a higher scoring option
        if ($nearestHigherScoringOption instanceof OptionInputNode) {
            return $nearestHigherScoringOption;
        }

        // if we have only only a higher lower option
        if ($nearestLowerScoringOption instanceof OptionInputNode) {
            return $nearestLowerScoringOption;
        }

        // there will always be a closest option, except if the collection is empty (which should not occur at this
        // point since this method is used for calculations, thus after successful template publishing, which should
        // have asserted non-empty collection. but there's always DB tampering or assertions being removed or badly altered.
        throw new \BadMethodCallException('cannot find a scoring option closest to score ['.((string) $score).']: there are no scoring options in the collection');
    }

    /**
     * get all option Nodes that use the given score.
     */
    public function getWhereScoreEquals(float $score): self
    {
        return $this->filter(fn (OptionInputNode $optionNode) => $optionNode->getScoringValue() === $score);
    }

    /**
     * get all option Nodes that have a score that is lower (NOT equal) than the given score.
     */
    public function getWhereScoreIsLowerThan(float $score): self
    {
        return $this->filter(fn (OptionInputNode $optionNode) => $optionNode->getScoringValue() < $score);
    }

    /**
     * get all option Nodes that have a score that is higher (NOT equal) than the given score.
     */
    public function getWhereScoreIsHigherThan(float $score): self
    {
        return $this->filter(fn (OptionInputNode $optionNode) => $optionNode->getScoringValue() > $score);
    }
}
