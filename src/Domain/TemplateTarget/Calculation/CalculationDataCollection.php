<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Calculation;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;

/**
 * Class CalculationDataCollection.
 *
 * collection that aids in calculating the global score value of a @see ScorableInputNode::operatesAsGlobalScoreNode()
 *
 * @see     CalculationData
 *
 * @extends ArrayCollection<array-key, CalculationData>
 */
class CalculationDataCollection extends ArrayCollection
{
    /**
     * get the ones that are calculable.
     */
    public function getCalculable(): self
    {
        return $this->filter(fn (CalculationData $item) => $item->isCalculable());
    }

    /**
     * check if all calculations use the same weighingType.
     */
    public function hasCommonWeighingType(): bool
    {
        $first = $this->first();

        if (!$first instanceof CalculationData) {
            return false;
        }

        $weighingType = $first->getWeighingType();
        /** @var CalculationData $item */
        foreach ($this->getIterator() as $item) {
            if (!($item->getWeighingType() === $weighingType)) {
                return false;
            }
        }

        return true;
    }

    /**
     * get the common weighingType.
     *
     * @throws \BadMethodCallException if no common weighing type used in collection
     */
    public function getCommonWeighingType(): string
    {
        $first = $this->first();

        if (!$first instanceof CalculationData || !$this->hasCommonWeighingType()) {
            throw new \BadMethodCallException('not all elements in the collection share the same weighingType');
        }

        return $first->getWeighingType();
    }

    /**
     * get the total added weighing values from the items in the collection.
     */
    public function getTotalWeighingSum(): float
    {
        $totalWeighingSum = 0.0;
        /** @var CalculationData $item */
        foreach ($this->getIterator() as $item) {
            $totalWeighingSum += $item->getWeighing();
        }

        return $totalWeighingSum;
    }

    /**
     * get the sum of total weighted values in the collection that uses a common weighingType
     * where the common weighingType is of type @see OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE.
     *
     * @throws \BadMethodCallException if no common weighing type or incorrect common weighing type used in collection
     */
    public function getTotalWeightedValuesSumForCommonWeighingTypePercentage(): float
    {
        if (!(OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE === $this->getCommonWeighingType())) {
            throw new \BadMethodCallException('weighingType is not '.OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE);
        }

        if (100.0 === $this->getTotalWeighingSum()) {
            return $this->getTotalWeightedValuesSumForCommonWeighingType();
        }

        throw new \BadMethodCallException('the total sum of the weighings of the score dependencies does not equal 100%');
    }

    /**
     * get the sum of total weighted values in the collection that uses a common weighingType
     * where the common weighingType is of type @see OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE.
     *
     * @throws \BadMethodCallException if no common weighing type or incorrect common weighing type used in collection
     */
    public function getTotalWeightedValuesSumForCommonWeighingTypeAbsolute(): float
    {
        if (!(OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE === $this->getCommonWeighingType())) {
            throw new \BadMethodCallException('weighingType is not '.OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE);
        }

        return $this->getTotalWeightedValuesSumForCommonWeighingType();
    }

    /**
     * TODO FIXME? improve formula if ever there are uses cases where max score is zero and max score != weighing.
     */
    private function getTotalWeightedValuesSumForCommonWeighingType(): float
    {
        $totalWeightedSum = 0.0;

        /** @var CalculationData $item */
        foreach ($this->getIterator() as $item) {
            // in most cases with absolute weighing calculation (also in some cases of percentual weighing), max score
            // of item(s) will be equal to its weighing, in that case we can skip the division/multiplying in formula below.
            // also avoids division by zero in valid use case where max score and weighing are equal and zero.
            if ($item->getMaxScore() === $item->getWeighing()) {
                $totalWeightedSum += $item->getNumericValue();
                continue;
            }

            $totalWeightedSum += $item->getNumericValue() / $item->getMaxScore() * $item->getWeighing();
        }

        return $totalWeightedSum;
    }
}
