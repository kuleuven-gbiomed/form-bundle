<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Calculation;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\ScorableInputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\GlobalScoring\CalculationScoreNodeConfig;
use Webmozart\Assert\Assert;

/**
 * Class CalculationData.
 *
 * helper object that aids in building calculation data based on a @see ScorableInputNode::operatesAsScoreNode()
 * that can then be used to calculate the global score value of a @see ScorableInputNode::operatesAsGlobalScoreNode()
 *
 * @see     CalculationDataCollection
 * @see     ScorableInputInterface
 */
final class CalculationData
{
    private readonly string $weighingType;

    private function __construct(private readonly float $weighing, string $weighingType, private readonly float $maxScore, private readonly ?float $value = null)
    {
        Assert::stringNotEmpty($weighingType);
        $this->weighingType = $weighingType;
    }

    /**
     * create object containing data for calculation of a score node, based on the given submitted form values.
     */
    public static function fromCalculationScoreNodeConfigWithWeighingTypeAndValues(
        CalculationScoreNodeConfig $scoreNodeConfig,
        string $weighingType,
        array $formValues,
    ): self {
        /** @var ScorableInputNode $scoreNode */
        $scoreNode = $scoreNodeConfig->getScoreNode();

        // important business decision:only make calculationData (= consider for calculation) if score node is submitted,
        // regardless (at this point ) of submitted value being (not) NULL in case of non-required score node.
        if (!array_key_exists($scoreNode->getUid(), $formValues)) {
            throw new \InvalidArgumentException('node with uid '.$scoreNode->getUid().' is not found in submitted form values');
        }

        // submitted value could be null if node is not required & submitted with empty value
        return new self(
            $scoreNodeConfig->getConfig()->getWeighing(),
            $weighingType,
            $scoreNode->getInputScoringParameters()->getMaxScore(),
            ScoreValueFinder::findNumericScoreValueForScorableNodeInFormValues($scoreNode, $formValues)
        );
    }

    /**
     * if value is not numeric, it should be ignored in calculations.
     *
     * this is not something to assert upon construct! a value can be validly null/non-numeric. e.g. if instance is
     * created based on inputNode and the inputNode is NOT required, the value could have been submitted with value NULL
     */
    public function isCalculable(): bool
    {
        return is_numeric($this->value);
    }

    public function getWeighing(): float
    {
        return $this->weighing;
    }

    public function getWeighingType(): string
    {
        return $this->weighingType;
    }

    /**
     * get the maximum score.
     */
    public function getMaxScore(): float
    {
        return $this->maxScore;
    }

    /**
     * get the (submitted) numeric value
     * for consistent and easier work in calculations, always cast to float.
     */
    public function getNumericValue(): float
    {
        if (!$this->isCalculable()) {
            throw new \BadMethodCallException('no calculable value available');
        }

        return (float) $this->value;
    }
}
