<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Calculation;

use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\ChoiceInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Rounding\RoundingModes;

/**
 * @IgnoreAnnotation("psalm")
 * Class GlobalChoiceScoreValueCalculator
 *
 * helper to aid in the calculation of the (rounded) numeric value of a @see ChoiceInputNode
 * that operates as a globalScore node and having a @see ChoiceInput attached to the inputNode
 *
 * INFO: An InputNode that has a ChoiceInput attached to it (hereafter named ChoiceGlobalScoreNode),
 * has children that are inputNodes as well. each of these children has an @see OptionInput associated
 * with it (herafter named ChildOptionInputNode).
 * the OptionInput of each ChildOptionInputNode has a predefined score value (as set by the maker of the form template).
 * how it gets calculated:
 * the value of the ChoiceGlobalScoreNode is calculated in the same way as an @see NumberInputNode
 * that operates as global score node, is calculated (@see GlobalNumberScoreValueCalculator ) and
 * as such simply returns a raw numeric value as the calculated global score value. with that raw numeric calculated
 * global score value, an ChildOptionInputNode can then be found amongst the children of the ChoiceGlobalScoreNode,
 * by using the score value of the OptionInputs of the ChildOptionInputNodes. it is important that the raw numeric
 * value is used to find the right ChildOptionInputNode(read: the one with the closest score value). because the
 * rounding, as defined on the ChoiceGlobalScoreNode, is used to determine which child has the score value that
 * is 'closest' to the raw numeric calculated global score value. thus no rounding is done on the raw value.
 * e.g. suppose the rounding is defined to do 'half even down'. suppose if raw numeric global score value is
 * calculated to be 11.2. suppose there are two ChildOptionInputNodes, one with score 10 and one with score 12.
 * we want to have the one with score 12, obviously. and raw value 11.2 is closer to the 12 ChildOptionInputNode
 * than the 10 ChildOptionInputNode, so without first rounding 11.2 to 11, we will get the 12 ChildOptionInputNode.
 * if 11.2 would first be rounded down to 11, it would would eventually return the 10 ChildOptionInputNode, due to
 * the rounding being set to 'half even down' and 10 being the 'half even down' between 10 & 12 for 11.
 *
 * @see     ScorableInputNode::operatesAsGlobalScoreNode()
 * @see     ChoiceInput
 */
class GlobalChoiceScoreValueCalculator extends AbstractGlobalScoreCalculator
{
    private readonly ScoringOptionInputNodeCollection $scoringOptions;

    /**
     * GlobalChoiceScoreValueCalculator constructor.
     *
     * a @see ChoiceInputNode has to meet more requirements to operate as global score node
     * than a @see NumberInputNode , in the sense that it needs to have answer options, and each of those answer options
     * must have a scoring value available. @see ChoiceInputNode::meetsScoringRequirements() which also checks the
     * options via @see ChoiceInputNode::hasAllOptionsValidForScoring()
     * the call to parent constructor will have checked and catched all this via a
     * full @see ScorableInputNode::operatesAsGlobalScoreNode() assert on the choice question. so at this point we
     * can assume all available options are valid (i.e. have a scoring values)
     *
     * @throws MissingNodeException
     */
    public function __construct(ChoiceInputNode $globalScoreNode, array $formValues)
    {
        parent::__construct($globalScoreNode, $formValues);

        // get the choice's answer options. if any of those are not valid (i.e. one or more option is missing a scoring
        // value, the call to parent constructor will have catched this via the full 'operatesAsGlobalScore' assert on
        // the choice question. so at this point we can assume all available options are valid (i.e. have a scoring values)
        $this->scoringOptions = new ScoringOptionInputNodeCollection($globalScoreNode->getAvailableOptions()->toArray());
    }

    /**
     * get the answer option (child InputNode) from the global choice question (parent global score Inputnode) that has
     * the score value that is the most closely corresponding to the calculated numeric value.
     *
     * throws exception if the raw numeric value could not be calculated
     * via @see AbstractGlobalScoreCalculator::getCalculatedRawNumericValue()
     * or if no option node could be found due to the collection of scoring options being empty.
     *
     * IMPORTANT: we use the un-rounded, unscaled calculated numeric value from @see getCalculatedRawNumericValue here
     * to determine which available option lies closed to that calculated numeric value.
     * we could first round the calculated value, using @see getCalculatedRoundedNumericValue , but that could cause
     * issues with finding the closest option, depending how the scale (@see ScorableInputInterface::getScale() )
     * for the rounding is set by the choice question.
     * e.g. the choice question has 1 option with score value 15, one with score value 15.8 and one with score value 16
     * the scale of the choice question is set to 0 (no decimals after komma) and rounding to 'HALF-EVEN-UP'
     * suppose the raw calulated numeric value turns out to be: 15.7
     * if we do not first round the raw value and use that raw value, the closed option is the one with score value 15.8
     * if we first round that raw value of 15.7 with the scale 0 and rounding mode HALF-UP as set by the choice option,
     * we will get a rounded calculated value of 16. and then the closest option will be the option with score value 16
     * so rounding or not rounding the raw numeric value before finding closest option has its consequences. it is
     * more logical to not first round the raw calculated value but - as with any calculations - do as less rounding
     * as possible before retrieving the final result.
     */
    public function getCalculatedOptionNode(): OptionInputNode
    {
        return $this->scoringOptions->getOneWhereScoreIsClosestTo(
            $this->getCalculatedRawNumericValue(), // don't round the numeric value here.
            RoundingModes::ROUND_HALF_UP === $this->roundingMode
        );
    }
}
