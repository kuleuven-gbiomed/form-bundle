<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Calculation;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Rounding\RoundingModes;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\GlobalScoring\CalculationScoreNodeConfig;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\CalculationScoreNodeConfigCollection;
use Webmozart\Assert\Assert;

/**
 * Class AbstractGlobalScoreCalculator.
 *
 * helper to aid in the calculation of the value of a @see ScorableInputNode that operates as a globalScore node
 *
 * WARNING: the constructor does NOT assert on availability of all required score nodes being all being
 *          submitted (i.e. in passed in formValue), and -via  calculationDataSet - wheter calculation is possible!
 *          because this processor object is used to TRY to calculate the value of one or more global score nodes in a
 *          form when that form is submitted. because of the distribution of submittable score nodes over different
 *          steps in the form's workflow, not having all score nodes present or submitted is then not a reason for
 *          breaking the form submit. the calculator has a single bool-returning public method to check if calculation
 *          is possible (@see AbstractGlobalScoreCalculator::canCalculateValue ). the actual public 'get' method(s)
 *          that return a calculated value, will ALWAYS (use that same check method to) assert if calculation is
 *          possible, throwing exceptions if not! use canCalculateValue to check if calculator can do the calculation!
 */
abstract class AbstractGlobalScoreCalculator
{
    private readonly ScorableInputNode $globalScoreNode;
    private readonly float $minScore;
    private readonly float $maxScore;
    private readonly int $scale;

    /**
     * visibility not private: needed in child classes.
     *
     * @phpstan-var RoundingModes::ROUND_* $roundingMode
     */
    protected int $roundingMode;
    /** collection of valid score node dependency configs to use for the calculation */
    private readonly CalculationScoreNodeConfigCollection $validCalculationScoreNodeConfigs;
    /** data derived from the score nodes, for the calculation */
    private readonly CalculationDataCollection $calculationDataSet;
    /** calculation is blocked due to (a) locked & required score dependency question(s) not being unlocked */
    private bool $blockedByLockedRequiredQuestions = false;

    /**
     * AbstractGlobalScoreCalculator constructor.
     *
     * @param ScorableInputNode $globalScoreNode the global score node to calculate
     * @param array             $formValues      the submitted values for the score nodes
     *
     * @throws MissingNodeException
     */
    public function __construct(ScorableInputNode $globalScoreNode, array $formValues)
    {
        Assert::true($globalScoreNode->operatesAsGlobalScoreNode());
        $scoringParams = $globalScoreNode->getInputScoringParameters();

        $this->maxScore = $scoringParams->getMaxScore();
        $this->minScore = $scoringParams->getMinScore();
        $this->scale = $scoringParams->getScale();
        $this->roundingMode = $scoringParams->getRoundingMode();

        $this->validCalculationScoreNodeConfigs = $globalScoreNode->getValidGlobalScoreCalculationScoreNodeConfigs();

        // get the calculation data for each score node, only using those that are submitted (i.e. having submitted value).
        // whether that submitted value is NULL (e.g. in case of a non-required score node submitted blank) or not, is not
        // important (upon calculation at least one submitted score node with numeric, non-null value will be required)
        $calculationDataSetArray = [];
        $ignoreLocked = $globalScoreNode->ignoreAllNotUnlockedDependencyQuestionsInGlobalScoreCalculation();
        /** @var CalculationScoreNodeConfig $scoreNodeConfig */
        foreach ($this->validCalculationScoreNodeConfigs as $scoreNodeConfig) {
            $scoreDependency = $scoreNodeConfig->getScoreNode();
            if (!array_key_exists($scoreDependency->getUid(), $formValues)) {
                continue;  // skip if score node was never submitted yet
            }

            $calculationData = CalculationData::fromCalculationScoreNodeConfigWithWeighingTypeAndValues(
                $scoreNodeConfig,
                $globalScoreNode->getGlobalScoreWeighingType(),
                $formValues
            );

            // domain decision: if calculation depends on one or more locked & required score dependency question(s)
            // that were NOT unlocked (i.e. having 'blank/empty' - no calculable - value) then those questions block
            // the entire calculation, unless the global score is configured to ignore any locked questions that are
            // not unlocked and allow calculation to continue with remaining not locked & unlocked calculable questions.
            // if such score dependency question was configured as locked but was never unlocked, then it will have been
            // indirectly submitted with an 'blank' value during server side submit process. a full check on the question
            // being unlocked by its chain is NOT needed here: too heavy an operation for something we can safely deduct
            // from: if question is configured as locked & required but submitted value is 'blank', then assume question
            // was not unlocked and its value was forcefully set as 'blank' during server side form submit processing.
            // if the locked question was unlocked but it is also configured as NOT required and it was validly submitted
            // without answer, then the value will also be 'blank'. in that case the question is not blocking calculation
            // and we can continue calculation with remaining score dependencies as is done with all non-required questions.
            if (false === $ignoreLocked && !$calculationData->isCalculable() && $scoreDependency->isLocked() && $scoreDependency->isRequired()) {
                // mark this as reason why calculation will not be possible and break off
                $this->blockedByLockedRequiredQuestions = true;
                break;
            }

            $calculationDataSetArray[] = $calculationData;
        }

        $this->globalScoreNode = $globalScoreNode;
        $this->calculationDataSet = new CalculationDataCollection($calculationDataSetArray);
    }

    public function getGlobalScoreNode(): ScorableInputNode
    {
        return $this->globalScoreNode;
    }

    /**
     * check if calculation is possible: all score node dependencies must be reachable, submitted & with calculable value
     * use this method to check if any calculation can be done to avoid exceptions when retrieving calculated values.
     */
    public function canCalculateValue(): bool
    {
        if ($this->isBlockedByLockedRequiredQuestions()) {
            return false;
        }

        // can not calculate if no score nodes or no calculation data derived from those score nodes is available
        if ($this->validCalculationScoreNodeConfigs->isEmpty() || $this->calculationDataSet->isEmpty()) {
            return false;
        }

        // need to have calculation data defined for each score node. (basically same as making sure that all the score nodes
        // are submitted before calculation can even be considered, because each score node calculation object is build
        // in constructor based on score node being submitted and thus having some submitted value available for calculation.
        if (!($this->validCalculationScoreNodeConfigs->count() === $this->calculationDataSet->count())) {
            return false;
        }

        // filter calculation data for score node dependencies that have a valid (numeric) value to calculate with.
        $calculableSet = $this->calculationDataSet->getCalculable();

        // if no usable calculation data for the score nodes available whatsoever, no calculation can be done:
        // at least one submitted score node dependency must have a calculable value (i.e. a required score node will
        // always have a calculable (numeric) value but a non-required score node can have a NULL (non-numeric) value.
        // hence, at least one numeric value must be submitted amongst the submitted values to calculate (business decision))
        if ($calculableSet->isEmpty()) {
            return false;
        }

        // if the calculation uses weighingType Absolute, then the calculation can go through with at least one
        // score node dependencies having a calculable value. the ones without a calculable value will simply be
        // omitted (including their weighings in the total weighing sum) from the calculation.
        if (ScorableInputNode::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE === $calculableSet->getCommonWeighingType()) {
            return true;
        }

        // but if the calculation uses weighingType Percentage, then all score node dependencies must have a calculable
        // value because omitting one or more, would also result in omitting their weighings in the calculation
        // and thus result in a total weighing sum falling below 100%, which would break calculation! (decision to
        // disallow a total weighing below or above 100% when calculating with weighingType Percentage was added later
        // and is both enforced upon publish of Template as well as when trying to calculate (in this class), hence we
        // need to validate that we have all we need to make a '100%' percentual calculation possible)
        return $calculableSet->count() === $this->calculationDataSet->count();
    }

    public function isBlockedByLockedRequiredQuestions(): bool
    {
        return $this->blockedByLockedRequiredQuestions;
    }

    /**
     * get the raw, NOT rounded, calculated numeric value, if it can be calculated.
     *
     * DOMAIN DECISION: one or more of the allowed min scores on the dependencies could be set to a value lower than
     * the min score allowed by the global score that uses those dependencies. that could result in a calculated value
     * based on answers over all those dependencies, being lower than the min score allowed by the global score. if so,
     * the domain decision is made to take the min score allowed by the global score. same goes for max scores(s).
     * there are use cases (EQA) where admins add a score dependency question that is meant to 'subtract a small value'
     * from the calculation, while all other score dependencies questions only add a value or at least stay above the
     * min score allowed by the global score. They assume that the users giving the answers will never score as low on
     * all the dependencies as to trigger a calculation to go below the min score allowed by the global score. we could
     * just store those invalid 'lower than min score' values as answer in back-end calculations. but that brings a lot
     * of problems: that global score could be a dependency for another global score, the value could be displayed in
     * read-only mode and no one has write access left to change it (and even then, manual submit of a value below
     * allowed min score is disallowed). hence we catch it with this domain decision. (it's anyway ip to admins
     * to provide 'reasonable min & max scores for both score dependencies and global score questions)
     *
     * example of this use case with 4 score dependency questions and one global score :
     *          score dependency question 1: allowed min score = 0,    allowed max score = 2
     *          score dependency question 2: allowed min score = 0,    allowed max score = 2
     *          score dependency question 3: allowed min score = 0,    allowed max score = 2
     *          score dependency question 4: allowed min score = -0.5, allowed max score = 0
     *          global score question:       allowed min score = 0,    allowed max score = 10
     *      CALCULATION RESULT:
     *      suppose users gave following score answers
     *          for question 1: 1, for question 2: 0,for question 3: 2, for question 4: -0.5
     *      then the calculated result (= auto calculated answer for global score) will be: 4.2/10
     *      suppose users gave following score answers
     *          for question 1: 0, for question 2: 0,for question 3: 0, for question 4: -0.5
     *      then the calculated result will be: -0.8/10
     *      but the min score allowed is 0. so here the -0.8/10 is changed to 0/10
     *
     * @throws \BadMethodCallException if the value can not be calculated
     */
    public function getCalculatedRawNumericValue(): float
    {
        if (!$this->canCalculateValue()) {
            throw new \BadMethodCallException('can not calculate value');
        }

        $rawCalculatedValue = $this->getRawNumericCalculatedValueForCommonWeighingType();

        // domain decision: if calculated result lower than allowed min score, take the min score instead
        if ($rawCalculatedValue < $this->minScore) {
            $rawCalculatedValue = $this->minScore;
        }

        // domain decision: if calculated result greater than allowed max score, take the max score instead
        if ($rawCalculatedValue > $this->maxScore) {
            $rawCalculatedValue = $this->maxScore;
        }

        return $rawCalculatedValue;
    }

    /**
     * get the rounded (based on global score node settings) calculated numeric value, if it can be calculated.
     */
    public function getCalculatedRoundedNumericValue(): float
    {
        return round($this->getCalculatedRawNumericValue(), $this->scale, $this->roundingMode);
    }

    /**
     * calculates the raw global score value for global score node using a common weighingType.
     */
    private function getRawNumericCalculatedValueForCommonWeighingType(): float
    {
        // get the common weighingType from the entire calculation set
        $weighingType = $this->calculationDataSet->getCommonWeighingType();
        // only calculate value with the non-null, numeric data
        // (non-required score nodes, having valid NULL value if user submitted blank, can be ignored. domain decision)
        $calculations = $this->calculationDataSet->getCalculable();

        $rawGlobalScoreValue = null;

        switch ($weighingType) {
            case OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE:
                $rawGlobalScoreValue = (
                    $calculations->getTotalWeightedValuesSumForCommonWeighingTypeAbsolute() * $this->maxScore / $calculations->getTotalWeighingSum()
                );
                break;
            case OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE:
                // calculate raw (not rounded/scaled) globalScore value against globalScore maxScore & totalWeighing
                $rawGlobalScoreValue = (
                    $calculations->getTotalWeightedValuesSumForCommonWeighingTypePercentage() * $this->maxScore / $calculations->getTotalWeighingSum()
                );
                break;
        }

        // should always get numeric value
        if (!is_numeric($rawGlobalScoreValue)) {
            throw new \RuntimeException('expected the raw global score node value to be numeric. Got'.gettype($rawGlobalScoreValue));
        }

        return $rawGlobalScoreValue;
    }
}
