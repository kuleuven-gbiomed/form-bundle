<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Calculation;

use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;

class GlobalScoreCalculatorFactory
{
    /**
     * @throws MissingNodeException
     */
    public static function createFromGlobalScoreNodeAndScoreNodesAndFormValues(
        ScorableInputNode $globalScoreNode,
        array $formValues,
    ): AbstractGlobalScoreCalculator {
        $calculator = null;

        if ($globalScoreNode instanceof ChoiceInputNode) {
            $calculator = new GlobalChoiceScoreValueCalculator($globalScoreNode, $formValues);
        }

        if ($globalScoreNode instanceof NumberInputNode) {
            $calculator = new GlobalNumberScoreValueCalculator($globalScoreNode, $formValues);
        }

        if (!($calculator instanceof AbstractGlobalScoreCalculator)) {
            throw new \InvalidArgumentException('can not create calculator for global score of type '.$globalScoreNode::class);
        }

        return $calculator;
    }
}
