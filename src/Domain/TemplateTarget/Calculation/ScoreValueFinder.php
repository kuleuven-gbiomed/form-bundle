<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Calculation;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;

/**
 * final class ScoreValueFinder.
 */
final class ScoreValueFinder
{
    /**
     * use given node associated with a question that operates as a 'scorable question' to find the stored numeric
     * answer (if any) in the given single dimension array of submitted or temporary saved answers (= form values).
     *
     * @return float|null
     */
    public static function findNumericScoreValueForScorableNodeInFormValues(
        ScorableInputNode $scorableNode,
        array $formValues,
    ) {
        if ($scorableNode instanceof NumberInputNode) {
            return static::findNumericScoreValueForNumberScorableNodeInFormValues($scorableNode, $formValues);
        }

        if ($scorableNode instanceof ChoiceInputNode) {
            return static::findNumericScoreValueForChoiceScorableNodeInFormValues($scorableNode, $formValues);
        }

        throw new \InvalidArgumentException('can not serach for numeric score value for a node of type '.$scorableNode::class);
    }

    /**
     * use given node associated with a number question that operates as a 'scorable question' to find the numeric
     * answer (if any) in the given single dimension array of submitted or temporary saved answers (= form values).
     *
     * if the given node associated with a number question is not a required question, then the answer could be blank (null)
     *
     * @return float|null
     *
     * @throws \InvalidArgumentException
     */
    public static function findNumericScoreValueForNumberScorableNodeInFormValues(
        NumberInputNode $numberScorableNode,
        array $formValues,
    ) {
        // if inputNode is never submitted ot temp saved, no numeric value will exist at all
        if (!array_key_exists($numberScorableNode->getUid(), $formValues)) {
            return null;
        }

        // could validly be null if the question is not required and submitted blank, so do not cast to float here!
        return $formValues[$numberScorableNode->getUid()];
    }

    /**
     * use given node associated with a choice question that operates as a 'scorable question', to find amongst the
     * answer options available for that choice question, the one option that is used (submitted / temporary saved)
     * as answer (if any) in the given single dimension array of submitted or temporary saved answers (= form values)
     * and return the numeric score value associated with that option used as answer (if any).
     *
     * NOTE: a scorable Choice Node can NEVER be configured as 'multiple select'. so only one option can ever be
     * given ans answer (or none if question is not required). so as soon as one (active) option node is found in
     * the given single dimension array of submitted or temporary saved answers, we take that one and stop iterating
     * over the options available to that choice question. if the given node associated with a choice question is not
     * a required question, then it is possible that it was submitted or tempoaray saved as 'blank': no option selected
     * as answer. which also means no numeric value will be available.
     * if an option is found as answer, we take the score value, if it exists. but the option could be configured
     * (intentionally or unintentionally) without score value. no score value for the selected option is no cause
     * to break and throw exception: it simply means no numeric score value is available for whatever reason.
     * NOTE: it doesn't make much sense to make a choice question with a set of answer options and then not set a
     * numeric score value for each of these options, but it still is no cause to break: admin side should warn about
     * lack of score value in one or more options in a scorable choice question, and either disallow or allow publish
     * of the form template.
     *
     * @return float|null
     *
     * @throws \InvalidArgumentException
     */
    public static function findNumericScoreValueForChoiceScorableNodeInFormValues(
        ChoiceInputNode $choiceScorableNode,
        array $formValues,
    ) {
        // if inputNode is never submitted ot temp saved, no option will be found and thus no numeric value will exist at all
        if (!array_key_exists($choiceScorableNode->getUid(), $formValues)) {
            return null;
        }

        /** @var OptionInputNode $optionInputNode */
        foreach ($choiceScorableNode->getAvailableOptions() as $optionInputNode) {
            // the submitted option(InputNode) for a Choice Node is found in formValues with its uid as key and value TRUE
            // if the Choice Node is not-required, it's possible that no submitted child option(InputNode) is found
            if (!array_key_exists($optionInputNode->getUid(), $formValues)) {
                continue;
            }

            // don't fail if no score value is set for this option, just return valid null value
            if (!$optionInputNode->hasScoringValue()) {
                return null;
            }

            return $optionInputNode->getScoringValue();
        }

        return null;
    }
}
