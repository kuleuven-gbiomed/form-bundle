<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Calculation;

use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\NumberInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;

/**
 * Class GlobalNumberScoreValueCalculator.
 *
 * helper to aid in the calculation of the (rounded) numeric value of a @see NumberInputNode
 * that operates as a globalScore node and having a @see NumberInput attached to the inputNode
 *
 * @see     ScorableInputNode::operatesAsGlobalScoreNode()
 * @see     NumberInput
 */
class GlobalNumberScoreValueCalculator extends AbstractGlobalScoreCalculator
{
    /**
     * GlobalNumberScoreValueCalculator constructor.
     *
     * @throws MissingNodeException
     */
    public function __construct(NumberInputNode $globalScoreNode, array $formValues)
    {
        parent::__construct($globalScoreNode, $formValues);
    }
}
