<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Fulfilment;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Version\PublishedVersion;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;

/**
 * Class FulfilmentChecker.
 *
 * very specific helper class to check if steps are 'fulfilled' = all question with write access for at least one role
 * in a certain step have been answered
 */
class FulfilmentChecker
{
    /**
     * check if all questions in given @see PublishedVersion, for which at least one role has write access in given step,
     * are answered according to the given set of (submitted) form values.
     *
     * WARNING: this method assumes a lot (hence why it's a helper) and it is intended to be used with submitted
     * values @see StoredTemplateTargetInterface::getSubmittedFormValues()
     * and not on @see StoredTemplateTargetInterface::getTemporarySavedFormValues().
     * although there might be use case to use this on temp saved values
     *
     * NOTE: at start, we can not check array formValues on empty and return a quick false here: if nothing was ever
     * submitted in any step, then yes, it's definitely not fulfilled, since a published template version has at least one writable
     * question in at least one step for at least one role, but we can rely on that here since the array form values
     * to check are explicitly passed in here as argument and it could be that this step has simply no questions to
     * which no one has write access (which is perfectly valid: each step is required to contain at the minimum
     * at least one question with at least READ access for at least one role, not necessarily one with WRITE access
     * NOTE: if a step has no questions with write access, then no one will ever be able to submit (duh, there's not
     * even going to be a submit button for users accessing that step), but the step IS considered fulfilled since
     * no one has to 'fulfill' anything. now, usually, at least an admin role should be configured to have write access
     * in that step to those questions that were writable in any previous step. which is why this method excepts roles
     * to exclude. but it is perfectly possible and a valid use case to have a step with read-only access for all roles,
     * including admin roles, because that means that step is intended to just run its course until deadline and then
     * the next step will start, purely on start date bases, even if that next step is configured to start
     * prematurely @see StepInterface::allowsToStartPrematurelyIfPreviousStepIsFulFilled() . the lack of writable
     * questions - and thus lack of something to submit - will prevent the premature start, because internally,
     * this/these isFulFilled check methods are triggered upon each submit or temp save in the current published
     * version of a template. (but these methods can be used externally in your app of course. e.g to notify someone
     * or do something if not all steps are fulfilled for all roles.)
     * anyway, one must take care when using the rolesToExclude here.
     *
     * @throws LocalizedStringException
     */
    public static function isStepFulfilledByFormValuesInPublishedVersion(
        PublishedVersion $publishedVersion,
        StepInterface $step,
        array $formValues,
        ?RoleCollection $rolesToExcludeFromQuestionRoleWriteAccessChecks,
    ): bool {
        // if there was ever a submit or tempSave done, then that was done on a published version in a template. template
        // might be de-activated in meanwhile but there will still be a published version (otherwise let fail hard)
        if (!$publishedVersion->getWorkFlow()->hasOneStepByUid($step->getUid())) {
            throw new \InvalidArgumentException('no step with uid ['.$step->getUid().'] found in the workflow of published version with label ['.$publishedVersion->getInfo()->getLocalizedLabel()->getFallback().']');
        }

        // get non-hidden (=reachable) questions (=parental) for which at least one role has write access in given step.
        // if no questions writable for any role, then consider fulfilled (occurs rarely to never) (this is a domain decision)
        $writableQuestions = $publishedVersion->getFormList()->getFlattenedReachableParentalInputNodes()->getWritableInStep($step);
        if ($writableQuestions->isEmpty()) {
            return true;
        }

        $rolesToExclude = $rolesToExcludeFromQuestionRoleWriteAccessChecks instanceof RoleCollection ?
            $rolesToExcludeFromQuestionRoleWriteAccessChecks->getNamesArray() : [];

        // loop over writable questions, filter out those those to which only the roles to exclude have write access,
        // check if the question is 'submitted' according to form values and as soon as one does not comply, return false
        /** @var InputNode $question */
        foreach ($writableQuestions as $question) {
            // the above filtering on $writableQuestions has guaranteed that at least one role has write access
            // get those roles that have write access to this question and filter out the roles to exclude
            $roles = array_diff(
                $question->getFlowPermission()->getRolesWithWriteAccessInStepUid($step->getUid()),
                $rolesToExclude
            );

            // if no roles after filtering on roles to exclude, then question was only writable for the given
            // roles to exclude, which means this question can be skipped for submit check
            if (0 === count($roles)) {
                continue;
            }

            // check if question is 'submitted' in formValues. if not, don't bother checking further; bail quickly
            // note: cannot use isset/empty here, the submitted value
            // can be null, empty array, empty string, etc as well, which are all perfectly valid submitted answers
            if (!array_key_exists($question->getUid(), $formValues)) {
                return false;
            }
        }

        // all questions 'submitted' according to formValues, for given step, taking in account roles to excludes
        return true;
    }

    /**
     * @throws LocalizedStringException
     */
    public static function areStepAndPrecedingStepsFulfilledByFormValuesInPublishedVersion(
        PublishedVersion $publishedVersion,
        StepInterface $step,
        array $formValues,
        ?RoleCollection $rolesToExcludeFromQuestionRoleWriteAccessChecks,
    ): bool {
        // if there was ever a submit or tempSave done, then that was done on a published version in a template. template
        // might be de-activated in meanwhile but there will still be a current version with workflow-steps (otherwise let fail hard)
        $steps = $publishedVersion->getWorkFlow()->getSteps();

        // loop over the steps preceding and including given step and check each for fulfilment
        // as soon as one does not 'fulfil', bail
        foreach ($steps->getSortedStepsThatPrecedeStep($step) as $precedingStep) {
            if (!static::isStepFulfilledByFormValuesInPublishedVersion(
                $publishedVersion,
                $precedingStep,
                $formValues,
                $rolesToExcludeFromQuestionRoleWriteAccessChecks
            )
            ) {
                return false;
            }
        }

        // all preceding steps are fulfilled, now check given step
        return static::isStepFulfilledByFormValuesInPublishedVersion(
            $publishedVersion,
            $step,
            $formValues,
            $rolesToExcludeFromQuestionRoleWriteAccessChecks
        );
    }
}
