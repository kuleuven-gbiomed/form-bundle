<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Update;

class TemporaryUpdated extends AbstractUpdated
{
    /**
     * @see Updated but without auto calculation of global scores, which is only done on submit, never on temp save!
     */
    public static function markAutoSaved(): bool
    {
        return false;
    }
}
