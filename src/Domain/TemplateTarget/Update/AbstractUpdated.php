<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Update;

use Exception;
use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\AnswerValue\Value;
use KUL\FormBundle\Client\AnswerValue\ValueCollection;
use KUL\FormBundle\Domain\Contract\FormUserInterface;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionWithAdditionalTextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;
use KUL\FormBundle\Domain\TemplateTarget\Calculation\AbstractGlobalScoreCalculator;
use KUL\FormBundle\Domain\TemplateTarget\Calculation\GlobalChoiceScoreValueCalculator;
use KUL\FormBundle\Domain\TemplateTarget\Calculation\GlobalNumberScoreValueCalculator;
use KUL\FormBundle\Domain\TemplateTarget\Calculation\GlobalScoreCalculatorFactory;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Entity\Submission;

/**
 * FIXME: I have no clue what this class is about.
 */
abstract class AbstractUpdated
{
    /** values explicitly submitted by user, possibly extended with global score values */
    protected ValueCollection $userFormValues;

    /** if the submission needs to be marked as automated save by the system */
    protected bool $markAutoSaved;

    /**
     * the question nodes marked as not hidden (excluding the non-hidden ones in a hidden category).
     *
     * @var InputNodeCollection<array-key, InputNode>
     */
    protected InputNodeCollection $reachableParentalInputNodes;

    final protected function __construct(
        FormList $formList,
        protected array $formValues,
        /** the (current) version number the form is being submitted under */
        protected int $versionNumber,
        /** the submitting user */
        protected FormUserInterface $user,
        protected Role $role,
        protected StepInterface $step,
        private readonly \DateTimeImmutable $submitDate,
    ) {
        $this->markAutoSaved = static::markAutoSaved();
        // only get the 'real' questions (i.e. not including optionNodes, those are only relevant to choice questions)
        $this->reachableParentalInputNodes = $formList->getFlattenedReachableParentalInputNodes();

        // set the values for the inputNodes explicitly submitted by given user
        $this->buildUserSavedAnswerValues();

        if (Updated::class === static::class) {
            // calculate global score values if required and add to formValues and tracking/history
            $this->processGlobalScoreInputNodes();
        }
    }

    abstract public static function markAutoSaved(): bool;

    /**
     * @throws MissingVersionException
     */
    public static function createFromAccessObject(
        ClientCombinedAccessObject $accessObject,
        array $formValues,
        \DateTimeImmutable $submitDate,
    ): self {
        $currentVersion = $accessObject->getTemplate()->getCurrentVersion();

        return new static(
            $currentVersion->getFormList(),
            $formValues,
            $currentVersion->getVersionNumber(),
            $accessObject->getUser(),
            $accessObject->getAccessingRole(),
            $accessObject->getOpenFlowStep(),
            $submitDate
        );
    }

    public function getFormValues(): array
    {
        return $this->formValues;
    }

    public function getVersionNumber(): int
    {
        return $this->versionNumber;
    }

    /**
     * @throws \Exception
     */
    public function getSubmission(): Submission
    {
        return Submission::createFromUserWithRoleInStepForValuesByVersionOnDate(
            $this->user,
            $this->role,
            $this->step,
            $this->userFormValues,
            $this->versionNumber,
            $this->submitDate,
            $this->markAutoSaved
        );
    }

    /**
     * build & add a @see Value for each node the user has actually submitted.
     */
    private function buildUserSavedAnswerValues(): void
    {
        $this->userFormValues = new ValueCollection();

        foreach ($this->formValues as $nodeUid => $value) {
            if (!$this->reachableParentalInputNodes->hasOneByNodeUid($nodeUid)) {
                continue;
            } // skip if node is directly or indirectly (via hidden category) hidden (user could not have submitted this one)

            /** @var InputNode $node */
            $node = $this->reachableParentalInputNodes->getOneByNodeUid($nodeUid);

            if (!$node->isStepRoleWriteAccessGranted($this->step, $this->role)) {
                continue;
            } // skip if form(List) is flow dependant and no write access was granted (user could not have submitted)

            $this->addUserSavedAnswerValue($node, $value);
        }
    }

    /**
     * get & process the globalScore nodes for which the value has to be auto calculated and added to the formValues
     * and for which an entry has to be added to tracking history, marked as auto-added.
     * skips the calculation of those global score nodes for which calculation is not (yet) possible.
     *
     * REMARK: auto-calculate and auto-add of globals should be done only once! never auto-re-calculate on server side!
     * REMARK: if there are globalScores, only take those that do net yet exist in the submitted formValues.
     *         i.e. if the node is found in (now or previously) submitted formValues, don't touch it!
     *         in more detail, the nodes are filtered to only process those global score nodes
     *              - that are NOT submitted directly by the submitting user (i.e. manually inserted value by
     *                submitting user due to user having write access, which overrules auto-calculate & add)
     *              - and that have NEVER been submitted previously (i.e. submitted manually or auto-added, by another
     *                or same user in a previous submit. or auto-added in current submit during processing of another
     *                global score that successfully calculates & added its depending nested global score node(s))
     *
     * @throws \RuntimeException
     * @throws MissingNodeException
     */
    protected function processGlobalScoreInputNodes(): void
    {
        /** @var ScorableInputNode $node */
        foreach ($this->reachableParentalInputNodes->getOperatingAsGlobalScoreNodes() as $node) {
            $this->processGlobalScoreInputNode($node);
        }
    }

    /**
     * process the globalScore node for which the value has to be auto calculated and added to the formValues and an
     * entry has to be added to tracking history, marked as auto-added. skips the process if calculation is not
     * possible or if already done (never re-calculate already added global score value! domain decision).
     *
     * INFO: a global score node has (at least one) depending score nodes on which it depends for calculation.
     * the calculation of the global score node can be done only if FOR ALL ITS DEPENDING SCORE NODES A VALUE IS KNOWN
     * (i.e. a value is manually submitted at one time or another - meaning in current submit request or in a previous
     * submit - and thus making that value available NOW). in most cases, these depending score nodes are not global
     * score nodes themselves, in which case the calculation of the target global score node is straight forward.
     * sometimes one or more of these depending score nodes are global score nodes themselves ('depending global-score nodes').
     * if all those 'depending global-score nodes' already have a known value available (i.e. manually submitted before
     * or now or auto-calculated before), then calculation of the requested 'main' global score node is also straight
     * forward (calculation just checks and fetches values from the available values set). but if one or more of these
     * 'depending global-score nodes' has no value yet but ALL of its OWN depending score nodes DO HAVE known values
     * available at this point, it must be auto calculated itself, before the requested 'main' target global score node
     * can be considered for auto-calculation. hence the recursive processing
     *
     * TODO DV in case of nested globals, each will store a rounded value which the next will use as such: is this meant in domain?
     *
     * @throws MissingNodeException
     */
    private function processGlobalScoreInputNode(ScorableInputNode $globalScoreNode): void
    {
        // never calculate if the user had write access to the global score and thus submitted it manually (with a
        // custom, valid value or by a re-calculation via client-side). because manual input by the submitting user
        // ALWAYS has priority over any calculated value, even if step is configured to always recalculate!
        if ($this->isNodeManuallySubmittedByUser($globalScoreNode)) {
            return;
        }

        // don't calculate & add value if global score is already manually submitted or auto-added at this point in a
        // previous submit action.
        // unless the step in which is being submitted is explicitly configured to always (re)calculate and override
        // manually or auto-calculated global score value that is or is not already manually submitted or auto-added
        // (regardless if that submitted value is blank or not, which can occur in case of manual submits of
        // not-required global score question)
        // suppose global score depends on locked question(s) and suppose it has been calculated (auto submitted)
        // in a previous submit action because those locked question(s) were all unlocked. suppose also no recalculating-
        // OnEachSubmitInStep is configured. then, in the current submit action, the calculation will be skipped. this
        // also means that if submitting user has re-locked those question, then the previously calculated value stored
        // for this global score node is left untouched, regardless of the fact that calculations with not-unlocked
        // questions always block the calculation! this is fully intended and in correspondance with the domain
        // decision that (if NOT isRecalculatingOnEachSubmitInStep) an automated calculation upon a submit action is
        // done only once and any re-calcuation after that can only be triggered by a client-side, noncommittal
        // re-calculation request via a button by users with write access to the global score!
        // but in the exact same situtation, with recalculatingOnEachSubmitInStep configured, a re-calcuate will be
        // triggered here, which will then be blocked by the locked questions! and in that case, the previously
        // calculated (submitted) value will be reset (also a domain decision)!
        if ($this->isNodeSubmitted($globalScoreNode) && !$globalScoreNode->isRecalculatingOnEachSubmitInStep($this->step)) {
            return;
        }

        $scoreNodes = $globalScoreNode->getValidGlobalScoreCalculationScoreNodeConfigs()->getScoreNodes();
        // if given global score node depends on score nodes that are global score nodes themselves, recursively check
        // for each if a value is submitted / auto-added at this point or else try to calculate & auto-add them.
        /** @var ScorableInputNode $nestedGlobalScoreNode */
        foreach ($scoreNodes->getOperatingAsGlobalScoreNodes() as $nestedGlobalScoreNode) {
            $this->processGlobalScoreInputNode($nestedGlobalScoreNode);
        }

        $calculator = GlobalScoreCalculatorFactory::createFromGlobalScoreNodeAndScoreNodesAndFormValues(
            $globalScoreNode,
            $this->formValues
        );

        // skip if a value can not be calculated yet (usually because not all depending values are yet available now)
        if (!$calculator->canCalculateValue()) {
            // if calculation is blocked by locked questions, reset the calcuated value if conditions met
            $this->resetGlobalScoreValueByBlockedCalculator($calculator);

            return;
        }

        // it will either be a number calculator or a choice calculator;

        // for number, we can use the rounded float value (stan forces me to check instance on NumberInputNode)
        if ($globalScoreNode instanceof NumberInputNode && $calculator instanceof GlobalNumberScoreValueCalculator) {
            $value = $calculator->getCalculatedRoundedNumericValue();
            $this->formValues[$globalScoreNode->getUid()] = $value;
            $this->addUserSavedAnswerValue($globalScoreNode, $value, true);

            return;
        }

        // for a choice, we use the 'calculated' option object (stan forces me to check instance on choiceInputNode)
        if ($globalScoreNode instanceof ChoiceInputNode && $calculator instanceof GlobalChoiceScoreValueCalculator) {
            // always mark the choice question itself as 'submitted' in it's own separate key-value pair!
            $this->formValues[$globalScoreNode->getUid()] = true;

            // use 'calculated' option node to mark it as the auto submitted option with relevant value
            $optionNode = $calculator->getCalculatedOptionNode();

            // a GlobalChoiceScoreValueCalculator can only be build with a ChoiceInputNode, which has children (options)
            // first, if global score is already manually submitted or previously auto-added, the auto calculate processing
            // will never have been triggered to get this far. but just to be sure, clear any option(s) previously
            // given as answer(s) to this global choice question so we are sure the answer is a sole option node
            /** @var OptionInputNode $optionInputNode */
            foreach ($globalScoreNode->getChildren() as $optionInputNode) {
                unset($this->formValues[$optionInputNode->getUid()]);
            }

            // the 'calculated' option as answer for the global score node should never be an option that allows or
            // requires additional text input. this is domain decision and enforced upon publish of template and also
            // checked when fetching valid global choice questions. but I think is a good idea to check and use an
            // exception here to make sure it never happens, just in case template publish validation is changed or
            // the soft validation when fetching valid global choice questions is changed. this exception will make sure
            // any changes in regards to this domain decision will force development to take it in account here.
            if ($optionNode->usesAdditionalTextInput()) {
                throw new \DomainException('the calculated result for a global score as choice question, can not be an option with additional text input (a.k.a. an "other" option');
            }

            // mark option as chosen answer with boolean TRUE
            $this->formValues[$optionNode->getUid()] = true;
            $this->addUserSavedAnswerValue($globalScoreNode, true, true);

            return;
        }

        throw new \DomainException('unknown & unhandled calculator '.$calculator::class);
    }

    /**
     * if node is of the choice type, add to tracking with array of submitted optionInputNode uid's (if any)
     * as array keys and associated values as array values: if a optionInputNode (OptionWithAdditionalTextInput)
     * allowed for a user to provide a custom string, then the answer value could be string or null. if it didn't
     * allow that (most common use), then value will be boolean TRUE.
     *
     * the answer of a choice question is always a set of @see OptionInputNode items
     * that value is then always an array with those option uids as array keys. the array value for
     * each option uid is then - in far most cases - the boolean TRUE (to indicate that that option was
     * chosen as answer). if the optionInputNode uses @see OptionInput. but it can also be a string
     * or NULL if the inputOptionNode uses @see OptionWithAdditionalTextInput, requiring or allowing users
     * to 'add' an additional string alongside choosing the option as answer.
     *
     * @see OptionInputNode::usesAdditionalTextInput()
     */
    private function getUserSavedAnswerValueForChoice(ChoiceInputNode $choiceInputNode): array
    {
        $value = []; // if node is not required, resulting array could stay empty
        /** @var OptionInputNode $optionInputNode */
        foreach ($choiceInputNode->getAvailableOptions() as $optionInputNode) {
            // using array_key_exists, because isset & empty cannot be used: answer value could be valid NULL or empty string
            if (array_key_exists($optionInputNode->getUid(), $this->formValues)) {
                $value[$optionInputNode->getUid()] = $this->formValues[$optionInputNode->getUid()];
            } // if value was submitted, it's uid will be in array keys of formValues with a value
        } // only use active (non-hidden) optionInput nodes

        return $value;
    }

    private function resetGlobalScoreValueByBlockedCalculator(AbstractGlobalScoreCalculator $calculator): void
    {
        $globalScoreNode = $calculator->getGlobalScoreNode();

        // never reset if a value for this global score question is manually submitted by user in current action,
        if ($this->isNodeManuallySubmittedByUser($globalScoreNode)) {
            return;
        }

        // but the a submitted value to reset must already exists (i.e. from a previous submit action).
        // prevents setting a blank value on a question that was never manually/auto submitted anyway
        if (!$this->isNodeSubmitted($globalScoreNode)) {
            return;
        }

        // only reset in the use case where the submitting open step is configured for always recalculating.
        // because a recalculate will only happen in case a step is configured as such!
        if (!$globalScoreNode->isRecalculatingOnEachSubmitInStep($this->step)) {
            return;
        }

        // only reset in the use case where calculation is blocked by one ore more locked & required questions.
        // because a blank value on possibly required questions is only valid/acceptable in this use case
        if (!$calculator->isBlockedByLockedRequiredQuestions()) {
            return;
        }

        // reset the calculated value to blank, but leave it marked as submitted (keep uid of node in formValues,
        // which at this point is asserted to already exist via the isNodeSubmitted check ).
        // if node is configured as required, this could mean it now has 'invalid' blank value. but this reset if
        // for use with global score nodes depending on locked questions, in which case the blank value is valid
        if ($globalScoreNode instanceof NumberInputNode) {
            $this->formValues[$globalScoreNode->getUid()] = null;
            // as with any change of value, log what has happened and by who (in this case 'auto-added)
            $this->addUserSavedAnswerValue($globalScoreNode, null, true);
        }

        // same ammo for choice, but for choice global score, blank means removing all the answer option(s) given
        // (however, this will keep the choice question's uid in formValues to mark it as submitted)
        if ($globalScoreNode instanceof ChoiceInputNode) {
            /** @var OptionInputNode $optionInputNode */
            foreach ($globalScoreNode->getChildren() as $optionInputNode) {
                unset($this->formValues[$optionInputNode->getUid()]);
            }
            // as with any change of value, log what has happened and by who (in this case 'auto-added)
            $this->addUserSavedAnswerValue($globalScoreNode, [], true);
        }
    }

    private function addUserSavedAnswerValue(InputNode $node, mixed $value, bool $autoAdded = false): void
    {
        $filteredValue = $value;

        if ($node instanceof ChoiceInputNode) {
            $filteredValue = $this->getUserSavedAnswerValueForChoice($node);
        }

        $value = Value::createFromUidAndValue($node->getUid(), $filteredValue, $autoAdded);

        $this->userFormValues->add($value);
    }

    /**
     * check if node is considered submitted
     * (i.e. submitted form values is a 1 dimensional array where the array keys are the uids of the nodes that have
     * been submitted. if the uid is in the keys of the submitted form values, it is considered submitted).
     *
     * @see AbstractSubmittedForm::$formValues for more info
     */
    private function isNodeSubmitted(InputNode $node): bool
    {
        return in_array($node->getUid(), array_keys($this->formValues), true);
    }

    /**
     * check if node is considered manually submitted in current action by the submitting user: i.e.
     * the submitting role of the submitting user has write access in current open step in which submit occurs.
     *
     *@see AbstractSubmittedForm::$formValues for more info
     */
    private function isNodeManuallySubmittedByUser(InputNode $node): bool
    {
        // node uuid must be present in submitted values and the submitting (user)role must have had write access
        // in the current open step (role and step as passed along in this object via access object)
        return $this->isNodeSubmitted($node) && $node->isStepRoleWriteAccessGranted($this->step, $this->role);
    }
}
