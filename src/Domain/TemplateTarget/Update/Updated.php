<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Update;

class Updated extends AbstractUpdated
{
    public static function markAutoSaved(): bool
    {
        return false;
    }
}
