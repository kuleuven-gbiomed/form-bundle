<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\TemplateTarget\Update;

class AutoTemporaryUpdated extends AbstractUpdated
{
    /**
     * @see TemporaryUpdated mark every answer in history as 'autosaved' (and as with 'normal' tempsave,
     * no auto calculation of global scores is done, which is only done on submit, never on temp save)!
     */
    public static function markAutoSaved(): bool
    {
        return true;
    }
}
