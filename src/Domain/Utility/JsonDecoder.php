<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Utility;

use KUL\FormBundle\Domain\Contract\Deserializable;

/**
 * Class JsonDecoder.
 *
 * specific helper to decode (via @see \JsonSerializable ) json encoded/serialized elements to associative arrays
 * back to array syntax used in @see Deserializable
 *
 * DEVELOPER NOTICE: methods are  marked internally as it is used mainly for elements implementing @see Deserializable
 * and for which it is known that a specific (associative) array is to be returned based on a JSON string!
 * apart from @see Role, all elements implemnting this contract return such associative arrays (or empty array if
 * allowed bt the class). do not use lightly!
 */
final class JsonDecoder
{
    // a depth of 512 should be more than enough for the nested category-question tree list in forms. a standard (question)
    // node can reach a certain depth on its own (somewhere between depth 2 to 7 (maybe a bit more?)) because of all its
    // 'options'. ChildrenAware nodes will off course always have a lot more depth. but to reach a total depth of 512 on
    // the rootNode (node tree) in the FormList JSON to decode, is highly unlikely, has never happened (yet), and it would
    // mean a very, very big nested - thus UX unfriendly - form. depth of the nodes tree is determined by nesting categories
    // in other categories, and a depth of 512 would mean very deep category nesting. so assuming the default 512 will
    // suffice. we'll also decode here with JSON_THROW_ON_ERROR which includes the JSON_ERROR_DEPTH (maximum stack depth
    // exceeded) error as well.
    public const JSON_DEPTH = 512;

    /**
     * @internal
     *
     * decode the given json string to an associative array, assuming that an array is what is supposed to be returned
     * if given json is no string or decoded json is no array, return an empty array as default
     *
     * NOTE: this is an internal method, based on (assumption that we have full control on) how this bundle serializes
     * data to json. performance is important and since the bundle handles both the serializing and de-serializing,
     * no performance heavy valid JSON checks are done here!
     */
    public static function toAssocArray(?string $json = null): array
    {
        if (!is_string($json)) {
            return [];
        }

        $jsonDecoded = json_decode($json, true, self::JSON_DEPTH, \JSON_THROW_ON_ERROR);

        return is_array($jsonDecoded) ? $jsonDecoded : [];
    }
}
