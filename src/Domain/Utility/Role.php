<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Utility;

use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;

/**
 * Class Role.
 *
 * @see     RoleCollection
 */
class Role implements \JsonSerializable, \Stringable
{
    private readonly string $name;

    /**
     * Role constructor.
     *
     * @throws \InvalidArgumentException
     */
    private function __construct(string $name)
    {
        if ('' === $name) {
            throw new \InvalidArgumentException(self::class.' expects name to be a non-empty string');
        }

        $this->name = $name;
    }

    public static function fromName(string $name): self
    {
        return new self($name);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function equals(self $role): bool
    {
        return $this->getName() === $role->getName();
    }

    public function jsonSerialize(): string
    {
        return $this->__toString();
    }
}
