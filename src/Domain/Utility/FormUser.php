<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Utility;

use KUL\FormBundle\Domain\Contract\FormUserInterface;
use Webmozart\Assert\Assert;

/**
 * Class FormUser.
 *
 * default @see FormUserInterface
 * IMPORTANT: the formBundle needs a user to track what he/she has done (has answered which questions, when, in what
 * step, with what  answer, as a temp answer or a final answer, etc.), to show the user's full name if needed
 * (e.g. to show 'last answer given or altered  by...' on questions) and sometimes to determine if a user can still
 * submit an answer in a step based on with what role you let user access the form ( @see StepInterface::limitsEachUniqueUserToOnlyOneSubmitUnderRole()
 * the formBundle does not know what a 'user' is. so whenever you access the formBundle's services you will
 * have to create such a user on-the-fly in your app with a identifier (id or username or whatever that can UNIQUELY
 * identify that user in  your app) and a @see Person which is just a first and last name.
 * in your app, you probably get the logged-in user, get some UNIQUE identifier for it and its full name
 * and create a @see FormUser on-the-fly
 * for your app's sake, make sure the identifier is a unique string in your app, because this formBundle uses it to
 * store it in DB and it does not care, nor can it determine if that is unique
 *
 * e.g. in a controller action
 * $yourAppsLoggedInUser = $this(->getContainer()->getTokenStorage)->getUser() // or wherever you get it from
 * $identifier = $yourAppsLoggedInUser->getUsername() // or whatever is unique (e.g. a uid or email or even primaryKey, if those are unique)
 * $formUserToPassToBundle = FormUser::fromIdentifierAndPerson(
 *      $identifier,
 *      Person::fromNames($yourAppsLoggedInUser->getFirstName(), $yourAppsLoggedInUser->getLastName())
 * );
 */
class FormUser implements FormUserInterface
{
    private readonly string $identifier;

    private function __construct(string $identifier, private readonly Person $person)
    {
        Assert::notEmpty($identifier);

        // going to use this in Mysql DB, for convenience, do not let exceed the reasonable 255 chars
        if (mb_strlen($identifier) > 255) {
            throw new \InvalidArgumentException('identifier ['.$identifier.'] may not exceed 255 chars, got ['.((string) mb_strlen($identifier)).'] chars.');
        }

        $this->identifier = $identifier;
    }

    public static function fromIdentifierAndPerson(string $identifier, Person $person): self
    {
        return new self($identifier, $person);
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }
}
