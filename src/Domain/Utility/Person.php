<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Utility;

use KUL\FormBundle\Domain\Contract\FormUserInterface;

/**
 * Class Person.
 *
 * VO that bundles non-unique person data like first name, etc. for @see FormUserInterface in a way that
 * this bundle understands and knows to use. the reason of existence for this VO is that contracting getters like
 * getFirstName directly on @see FormUserInterface could lead to issues with return type: e.g. one project might
 * return a string, while another may return an object or for getFirstName. hence this intermediate object.
 */
class Person
{
    private readonly string $firstName;

    private readonly string $lastName;

    private function __construct(string $firstName, string $lastName)
    {
        if ('' === $firstName) {
            throw new \InvalidArgumentException('parameter [firstName] must be a non-empty string');
        }

        if ('' === $lastName) {
            throw new \InvalidArgumentException('parameter [lastName] must be a non-empty string');
        }

        // going to use this in Mysql DB, for convenience, do not let exceed the reasonable 512 chars
        if ((mb_strlen($firstName) + mb_strlen($lastName)) > 512) {
            $lengthString = (string) (mb_strlen($firstName) + mb_strlen($lastName));
            throw new \InvalidArgumentException('firstName + lastName  ['.$firstName.' '.$lastName.'] may not exceed 512 chars, got ['.$lengthString.'] chars.');
        }

        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public static function fromNames(string $firstName, string $lastName): self
    {
        return new self($firstName, $lastName);
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getFullName(): string
    {
        return $this->firstName.' '.$this->lastName;
    }
}
