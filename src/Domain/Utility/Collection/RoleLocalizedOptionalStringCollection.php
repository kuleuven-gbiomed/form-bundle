<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Utility\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Class RoleLocalizedOptionalStringCollection.
 *
 * @see     RoleLocalizedOptionalString
 *
 * @extends ArrayCollection<array-key, RoleLocalizedOptionalString>
 */
final class RoleLocalizedOptionalStringCollection extends ArrayCollection implements \JsonSerializable, Deserializable
{
    public static function createEmpty(): self
    {
        return new self();
    }

    /**
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $elements = [];

        foreach ($data as $roleLocaleTranslations) {
            $elements[] = RoleLocalizedOptionalString::fromJsonDecodedArray($roleLocaleTranslations);
        }

        return new self($elements);
    }

    public function hasOneForRole(Role $role): bool
    {
        $roleStrings = $this->filter(fn (RoleLocalizedOptionalString $roleLocalizedString) => $roleLocalizedString->getRole()->equals($role));

        return 1 === $roleStrings->count();
    }

    /**
     * @throws \BadMethodCallException
     */
    public function getOneForRole(Role $role): RoleLocalizedOptionalString
    {
        $roleStrings = $this->filter(fn (RoleLocalizedOptionalString $roleLocalizedString) => $roleLocalizedString->getRole()->equals($role));

        $first = $roleStrings->first();
        if ($first instanceof RoleLocalizedOptionalString && 1 === $roleStrings->count()) {
            return $first;
        }

        throw new \BadMethodCallException('expected one result. got '.((string) $roleStrings->count()));
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
