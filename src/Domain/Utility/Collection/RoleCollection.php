<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Utility\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Utility\Role;
use Webmozart\Assert\Assert;

/**
 * @extends ArrayCollection<array-key, Role>
 */
class RoleCollection extends ArrayCollection implements \JsonSerializable, Deserializable
{
    final public function __construct(array $elements = [])
    {
        parent::__construct($this->guardUnique($elements));
    }

    /**
     * @param array<array-key, Role> $roles
     */
    protected function guardUnique(array $roles): array
    {
        return $roles; // For the regular Role Collection we don't mind duplicate. @see RoleSet
    }

    public static function createEmpty(): static
    {
        return new static();
    }

    public static function fromJsonDecodedArray(array $data): self
    {
        return static::fromRoleNames($data);
    }

    /**
     * @param string[] $roleNames
     */
    public static function fromRoleNames(array $roleNames): static
    {
        $elements = [];

        foreach ($roleNames as $roleName) {
            $elements[] = Role::fromName($roleName);
        }

        return new static($elements);
    }

    /**
     * @return array<int, string>
     */
    public function getNamesArray(): array
    {
        $names = [];

        /** @var Role $role */
        foreach ($this->getIterator() as $role) {
            $names[] = $role->getName();
        }

        return $names;
    }

    public function hasRole(Role $role): bool
    {
        /** @var Role $collectionRole */
        foreach ($this->getIterator() as $collectionRole) {
            if ($collectionRole->equals($role)) {
                return true;
            }
        }

        return false;
    }

    public function hasOneByName(string $name): bool
    {
        Assert::stringNotEmpty($name);

        $result = $this->filter(fn (Role $role) => $role->getName() === $name);

        return 1 === $result->count();
    }

    public function getOneByName(string $name, bool $mustHaveResult = true): ?Role
    {
        /** @var self $filtered */
        $filtered = $this->filter(fn (Role $role) => $name === $role->getName());

        $first = $filtered->first();
        if ($first instanceof Role && 1 === $filtered->count()) {
            return $first;
        }

        if ($mustHaveResult) {
            throw new \BadMethodCallException('Wrong amount of Role objects found. Expected to find exactly one,'." found: {$filtered->count()}.");
        }

        return null;
    }

    public function hasAtLeastOneOfRoles(self $roles): bool
    {
        /** @var Role $role */
        foreach ($roles as $role) {
            if ($this->hasRole($role)) {
                return true;
            }
        }

        return false;
    }

    /**
     * returns new collection with intersection of only the given roles that also are present in this collection.
     */
    public function getDuplicatesByRoles(self $roles): self
    {
        $duplicates = [];

        /** @var Role $role */
        foreach ($roles as $role) {
            if ($this->hasRole($role)) {
                $duplicates[] = $role;
            }
        }

        return new self($duplicates);
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function getMergedWith(self $collection): self
    {
        return new self(array_merge($this->toArray(), $collection->toArray()));
    }

    public function orderByRoleHierarchy(array $roleHierarchy): self
    {
        if (1 === $this->count()) {
            return $this;
        }

        $result = new self();

        // Loop over the defined hierarchy and add to result if matched
        foreach ($roleHierarchy as $roleFromHierarchy) {
            $resolvedRole = $this->getOneByName($roleFromHierarchy, false);
            if (isset($resolvedRole)) {
                $result->add($resolvedRole);
                if ($result->count() === $this->count()) {
                    return $result;
                }
            }
        }

        // Add roles that could not be found in hierarchy to the end
        foreach ($this->getIterator() as $formBundleRole) {
            if (!$result->hasRole($formBundleRole)) {
                $result->add($formBundleRole);
            }
        }

        return $result;
    }
}
