<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Utility\Collection;

final class RoleSet extends RoleCollection
{
    protected function guardUnique(array $roles): array
    {
        return array_unique($roles);
    }

    public function set($key, $value): void
    {
        // If the Role is already in the set, we remove it at its existing key to prevent duplicates.
        foreach ($this->getIterator() as $existingKey => $role) {
            if ($value->equals($role)) {
                $this->remove($existingKey);
            }
        }

        parent::set($key, $value);
    }

    public function add($element): void
    {
        if ($this->hasRole($element)) {
            return;
        }

        parent::add($element);
    }
}
