<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Utility\Localized;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;

/**
 * Class LocalizedOptionalString.
 *
 * version of @see AbstractLocalizedString for optional strings (e.g. a label or description).
 * the localized string can be build without any locale translations
 * for required strings with at least one translation @see LocalizedRequiredString
 */
final class LocalizedOptionalString extends AbstractLocalizedString
{
    public static function fallbackRequired(): bool
    {
        return false;
    }

    /**
     * checks if at least one translation is available (the string is optional so there could be no translations at all).
     */
    public function hasTranslations(): bool
    {
        return 0 < count($this->getAvailableLocales());
    }

    /**
     * create an optional string without any translations.
     *
     * @throws LocalizedStringException
     */
    public static function createEmpty(): self
    {
        return new self([]);
    }

    /**
     * @throws LocalizedStringException
     */
    public static function fromLocaleTranslations(array $localeTranslations): self
    {
        return new self($localeTranslations);
    }

    /**
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        return self::fromLocaleTranslations($data);
    }
}
