<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Utility\Localized;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Class RoleLocalizedOptionalString.
 *
 * links a @see LocalizedOptionalString to a role for usage in e.g. optional descriptions, messages, etc
 * that are translated and can be shown for a specific role only. the description, message, etc.
 * is assumed optional, meaning that the localized string can have no locale translations at all
 *
 * @see     RoleLocalizedOptionalStringCollection
 */
final readonly class RoleLocalizedOptionalString implements \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_ROLE = 'role';
    /** @var string */
    public const KEY_TRANSLATIONS = 'translations';

    private function __construct(
        private Role $role, // role for which this optional translated string is relevant.
        private LocalizedOptionalString $localizedString,
    ) {
    }

    public static function fromRoleAndLocalizedOptionalString(Role $role, LocalizedOptionalString $localizedString): self
    {
        return new self($role, $localizedString);
    }

    /**
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        return new self(
            Role::fromName($data[self::KEY_ROLE]),
            LocalizedOptionalString::fromJsonDecodedArray($data[self::KEY_TRANSLATIONS])
        );
    }

    public function withUpdatedLocalizedOptionalString(LocalizedOptionalString $localizedOptionalString): self
    {
        return new self($this->getRole(), $localizedOptionalString);
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    public function getLocalizedString(): LocalizedOptionalString
    {
        return $this->localizedString;
    }

    /**
     * the string is optional for the role. this checks if at least one translation is available.
     */
    public function hasTranslations(): bool
    {
        return $this->getLocalizedString()->hasTranslations();
    }

    /**
     * @return array{role: Role, translations: LocalizedOptionalString}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_ROLE => $this->getRole(),
            self::KEY_TRANSLATIONS => $this->getLocalizedString(),
        ];
    }
}
