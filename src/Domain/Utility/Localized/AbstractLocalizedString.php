<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Utility\Localized;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;

/**
 * Class AbstractLocalizedString.
 *
 * allows to work in a unified manner with translated non-empty strings for labels, descriptions, etc. on
 * template elements such as form questions list, categories, info, etc. Includes a fallback to the first found
 * translation (if any) and assuming that a NULL value or empty string can be ignored as valid translated string value.
 *
 * admins do not always have to provide labels, descriptions, etc. when constructing (template) elements.
 * this abstraction thus allows to be constructed without any translations.
 *
 * if the involved string (label, description, etc.) is optional, then @see LocalizedOptionalString ,
 * which allows to construct without any locale translations
 * if the involved string (label, description, etc.) is required, @see LocalizedRequiredString ,
 * which asserts at least one valid (non-empty, non-NULL) locale translation
 *
 * only non-empty strings are considered as valid translations. therefor only translated strings that are not empty
 * string and not NULL will be added to the object's usable strings upon construct. But for convenience & consistency
 * in (de)serialization and rendering (avoid showing empty strings), this object can also be constructed from an array
 * with empty string or NULL value for a locale translation, which then is silently ignored & omitted from valid
 * translations upon construction. this is mainly done to catch any inconsistencies in the values in the source array
 * used to build this object. because that source array could come from different sources that could - completely
 * validly - not have filtered out NULL values or empty strings, like if it comes from a form that allows to submit a
 * label blank for each translation but does not filter out the locales with NULL or empty string values after submit.
 * relying solely on each action/service/formDataMapper to remove those empty locale before storing those translations,
 * is difficult to impose, a bit of overhead and wishful thinking, knowing that this code/bundle is build to take into
 * consideration that old (DB) sources exists at time of writing.
 */
abstract class AbstractLocalizedString implements \JsonSerializable, Deserializable
{
    /** @var string[] set of translations, each keyed by a locale */
    private array $localeStrings = [];

    /** @var string|null fall back translation, equal to first one found in */
    private ?string $fallbackString = null;

    /** @var string|null fall back locale, equal to first valid locale found in */
    private ?string $fallbackLocale = null;

    /**
     * AbstractLocalizedString constructor.
     *
     * @throws LocalizedStringException if any locale or translation in given array is invalid
     */
    final protected function __construct(array $localeTranslations)
    {
        foreach ($localeTranslations as $locale => $translation) {
            // locale must be a non-empty string
            if (!is_string($locale) || '' === $locale) {
                throw new LocalizedStringException('invalid locale: must be a non-empty string!');
            }

            // empty strings & NULL values are not considered as translations, so silently filter these out. empty values
            // could be coming from commands, older DB data but also from input fields in admin-side forms - used for
            // building template elements (e.g. question labels) - that allow to submit those field as blank and whose
            // submitted blank (empty) value(s) were not filtered at that point. NOTE: can not use PHP's empty function:
            // can be a valid string like '0' (e.g. labels of options in choice question can be numeric strings).
            if ('' === $translation || null === $translation) {
                continue;
            }

            // anything else than a string or NULL value for translation is considered invalid enough to throw an exception
            if (!is_string($translation)) {
                throw new LocalizedStringException('invalid translation: must be a string or NULL!');
            }

            $this->localeStrings[$locale] = $translation;

            // always set first found valid locale translation as fallback (domain decision)
            if (!is_string($this->fallbackString)) {
                $this->fallbackString = $translation;
                $this->fallbackLocale = $locale;
            }
        }

        if (static::fallbackRequired() && !$this->hasFallback()) {
            throw new LocalizedStringException('need at least one non-empty locale translation to build a localized required string!');
        }
    }

    abstract public static function fallbackRequired(): bool;

    public function withUpdatedLocaleTranslation(string $locale, ?string $translation): static
    {
        return new static(array_merge($this->localeStrings, [$locale => $translation]));
    }

    public function withAddedLocaleTranslation(string $locale, ?string $translation): static
    {
        return new static(array_merge($this->localeStrings, [$locale => $translation]));
    }

    public function withRemovedLocale(string $locale): static
    {
        $localeStrings = $this->localeStrings;

        if ($this->hasForLocale($locale)) {
            unset($localeStrings[$locale]);
        }

        return new static($localeStrings);
    }

    /**
     * get all locales for which a non-empty translated string exits.
     *
     * @return string[]
     */
    public function getAvailableLocales(): array
    {
        return array_keys($this->localeStrings);
    }

    /**
     * check if there is a translation available for given locale.
     *
     * @throws LocalizedStringException if locale is invalid
     */
    public function hasForLocale(string $locale): bool
    {
        if ('' === $locale) {
            throw new LocalizedStringException('invalid locale: must be a non-empty string!');
        }

        return isset($this->localeStrings[$locale]);
    }

    /**
     * @throws LocalizedStringException if no translation is available for locale
     */
    public function getForLocale(string $locale): string
    {
        if (!$this->hasForLocale($locale)) {
            throw new LocalizedStringException('no non-empty translated string exist for locale ['.$locale.']');
        }

        return $this->localeStrings[$locale];
    }

    /**
     * checks if there is a fall back. i.e. same as checking if there is at least one non-empty translation available.
     */
    public function hasFallback(): bool
    {
        return is_string($this->fallbackString);
    }

    /**
     * @throws LocalizedStringException if no fallback is available
     */
    public function getFallback(): string
    {
        if (!is_string($this->fallbackString)) {
            throw new LocalizedStringException('no non-empty translated string as fallback exists');
        }

        return $this->fallbackString;
    }

    public function hasFallbackLocale(): bool
    {
        return is_string($this->fallbackLocale);
    }

    /**
     * @throws LocalizedStringException if no locale is available
     */
    public function getFallbackLocale(): string
    {
        if (is_string($this->fallbackLocale)) {
            return $this->fallbackLocale;
        }

        throw new LocalizedStringException('no fallback (locale) exists');
    }

    /**
     * @throws LocalizedStringException indirectly if locale is invalid
     */
    public function hasForLocaleOrFallback(string $locale): bool
    {
        return $this->hasForLocale($locale) || $this->hasFallback();
    }

    /**
     * @throws LocalizedStringException indirectly if locale is invalid
     */
    public function getForLocaleOrFallback(string $locale): string
    {
        if (!$this->hasForLocaleOrFallback($locale)) {
            throw new LocalizedStringException('no non-empty translated string for locale ['.$locale.'] or fallback found');
        }

        if ($this->hasForLocale($locale)) {
            return $this->getForLocale($locale);
        }

        return $this->getFallback();
    }

    /**
     * compares a given localized string on having matching amount of translations,
     * for the same locales and with the same case insensitive translation for each locale.
     * the order of the translations per locale is of no importance.
     *
     * @throws LocalizedStringException indirectly if locale or a translation is invalid
     */
    public function equals(self $localizedString): bool
    {
        if (!(count($this->getAvailableLocales()) === count($localizedString->getAvailableLocales()))) {
            return false;
        }

        foreach ($this->localeStrings as $locale => $translation) {
            if (!$localizedString->equalsLocaleTranslation($locale, $translation)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return string[]
     */
    public function getLocaleStrings(): array
    {
        return $this->localeStrings;
    }

    /**
     * checks if there is a locale with the same, case insensitive translation.
     *
     * @throws LocalizedStringException if translation is invalid or indirectly if locale is invalid
     */
    public function equalsLocaleTranslation(string $locale, string $translation): bool
    {
        if ('' === $translation) {
            throw new LocalizedStringException('invalid translation for equality check: must be a non-empty string!');
        }

        if (!$this->hasForLocale($locale)) {
            return false;
        }

        return mb_strtolower($translation) === mb_strtolower($this->getForLocale($locale));
    }

    public function jsonSerialize(): array
    {
        return $this->localeStrings;
    }
}
