<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Utility\Localized;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;

/**
 * Class LocalizedRequiredString.
 *
 * version of @see AbstractLocalizedString for required strings (e.g. a label or description).
 * asserts that at least one valid non-empty locale translation exist. the locale of that minimal given locale
 * translation is not important (domain decision). as long as one locale translation is provided, the
 * required string is considered valid.
 * for optional strings for with no minimal translation is required @see LocalizedOptionalString
 */
final class LocalizedRequiredString extends AbstractLocalizedString
{
    public static function fallbackRequired(): bool
    {
        return true;
    }

    /**
     * @param array<string, ?string> $localeTranslations
     *
     * @throws LocalizedStringException
     */
    public static function fromLocaleTranslations(array $localeTranslations): self
    {
        return new self($localeTranslations);
    }

    /**
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        return self::fromLocaleTranslations($data);
    }

    /**
     * @throws LocalizedStringException
     */
    public function toLocalizedOptionalString(): LocalizedOptionalString
    {
        return LocalizedOptionalString::fromJsonDecodedArray($this->getLocaleStrings());
    }
}
