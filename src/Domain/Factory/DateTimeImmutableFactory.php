<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Factory;

/**
 * Class DateTimeImmutableFactory.
 *
 * The fact that the conversion from DateTimeInterface to DateTimeImmutable gets rid of the microseconds, is
 * a hack, because David was testing code that depended on DateTime::now().
 *
 * I think we don't need this hack anymore.
 */
class DateTimeImmutableFactory
{
    /**
     * use a given amount of days to add or subtract from given date and return a modified immutable
     * date version of the given date (without changing the original date).
     *
     * @psalm-suppress InvalidFalsableReturnType
     * @psalm-suppress FalsableReturnStatement
     *
     * @throws \InvalidArgumentException if modifyingDays is not a valid integer
     */
    public static function createFromDateModifiedWithDays(\DateTimeInterface $date, int $modifyingDays): \DateTimeImmutable
    {
        // build the modify parameter manually to make absolutely sure it is in correct string format
        $modifyString = ($modifyingDays < 0 ? '-' : '+').((string) abs($modifyingDays)).' days';
        $result = static::createFromDate($date)->modify($modifyString);

        return $result;
    }

    /**
     * use a given date to return an immutable date version for it.
     */
    public static function createFromDate(\DateTimeInterface $date): \DateTimeImmutable
    {
        $result = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $date->format('Y-m-d H:i:s'));

        if (false === $result) {
            // Normally this can't happen.
            throw new \InvalidArgumentException('Invalid DateTime format.');
        }

        return $result;
    }
}
