<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Contract;

use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleSet;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface ClientWithTargetAccessInterface.
 *
 * person with roles of which each role has access to given target object/entity @see TemplateTargetableInterface
 * used as intermediate object between your app and the bundle that allows to pre define with what role or roles access
 * for the target object/entity must be considered & checked in a certain @see TemplateInterface to filter out
 * the questions with read and/or write access for that role(s)
 *
 * defines a user (basically a user identifier with some person data like firstName, etc.) @see FormUserInterface
 * that is considered to have access to a @see TemplateTargetableInterface
 * for the specified set of @see getAccessingRoles()
 *
 * when checking for access to the template for a target object/entity by a user, your app must first build this
 * client access object, based on your app's domain for access to that target by the logged-in (or other) user.
 * so it is your app that decides if a user has access to that specific target and if so, for what roles of that user.
 * your app then passes this ClientWithTargetAccess to the bundle. the bundle will then consider this accessing client
 * object as a user-person with access for given accessing roles to given target. the bundle only needs the user with
 * its id to log action and possibly limit write-to-read access in certain template-steps, but the accessing roles will
 * be the sole truth to determine further if any access to a @see TemplateInterface
 * for a @see TemplateTargetableInterface can be given and what questions can be presented for the user.
 *
 * most users (or all, depending on your app) will only have one role in these accessing roles. any access & rendering
 * of questions is always based on ONE role. it is sheer impossible to get, render and process the questions for multiple
 * roles at once. hence a user with multiple roles (that grant actual question access) will get the opportunity to
 * 'switch' between roles for which he/she has at least some access to questions. this set of accessing roles instead
 * of a single role, helps for that switching between accessing roles. the switching itself is regulated by this
 * bundle, by using (optional or required, depending on route intention) 'roleSlugs' in urls which are retrieved
 * by a @see Request object that is passed in the services handling the access. if multiple accessing roles are given
 * for a user, and no roleSlug is given or the given roleSlug is no (valid) role of user, then the first role in
 * accessing roles is used by default (e.g. on first landing by a user and/or until user switches roles), after
 * these accessing roles are re-ordered by @see TemplateInterface::getHierarchicalRolesThatCouldParticipate()
 * on the template for which access is requested for the target
 *
 * note that the name indicates that the client (user with roles) is considered accessing the target, but that
 * it does not mention actual access to a template (i.e. its questions at current step in its workflow) for that target!
 */
interface ClientWithTargetAccessInterface
{
    public function getFormUser(): FormUserInterface;

    public function getTarget(): TemplateTargetableInterface;

    public function getAccessingRoles(): RoleSet;

    public function getCurrentDate(): \DateTimeImmutable;
}
