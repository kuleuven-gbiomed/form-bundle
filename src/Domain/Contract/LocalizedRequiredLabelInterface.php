<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Contract;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;

/**
 * Interface LocalizedRequiredLabelInterface.
 *
 * contracts an object to have a required label. hence at least one locale translation is required
 */
interface LocalizedRequiredLabelInterface
{
    /**
     * returns a localized version of a label with non-empty translations for at least one locale.
     *
     * @throws LocalizedStringException if there is no translation for at least one locale as fallback
     */
    public function getLocalizedLabel(): LocalizedRequiredString;

    /**
     * get label for the requested locale or use the fallback if none exist for locale.
     * throws exception if locale is invalid or requested locale translation does not exist and no fallback is available.
     *
     * @throws LocalizedStringException if invalid locale or if there is no translated label for locale and no fallback
     */
    public function getLabel(string $locale): string;
}
