<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Contract;

use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Utility\JsonDecoder;

/**
 * Interface Deserializable.
 *
 * helps to mark objects as being deserializable directly from associative, nested arrays containing simple PHP data
 * types (string, integer, float, boolean, array, null but no object or resource) coming from JSON decoded strings from
 * DB and having dedicated indices that those objects rely on.
 *
 * the sole purpose of this contract is just to explicitly mark objects as deserializable from JSON decoded-to-array strings
 * so that I can always refer to this contract to indicate how all deserializing of this kind is done. implementations of this
 * contract always assume that all required associative array indices are present in given array data, without any index exist
 * check (or try catch) since (de)serializing is done internally and it should fail big time if an index is absent!
 * main usage: all elements (Value Objects) in @see TemplateInterface are JSON serializable as simple data types and
 * stored as JSON in DB. The ORM mapped entity associated with @see TemplateInterface is responsible for the basic
 * JSON decoding to an associative array of those element/elements that are needed for certain activities on templates
 * via json_decode($json, true) (@see JsonDecoder::toAssocArray ). That associative array is then the serializedArray
 * version of such an element/value object.
 *
 * note: it is possible to let (de)serializing be done by the dbal layer, or using something like JMS serializer with
 * YAML config to indicate how (de)serializing should be done automatically on each DB call. but I want full control on
 * how (de)serializing is done and I want it to be baked in the object class itself. I had some bad experiences
 * with letting some sort of (dbal) intermediate do it for me (same with using Doctrine's json type). also, performance
 * is an issue for everything related to templates; e.g. JMS Serializer supported object (de) serializing has
 * proven performance heavy and further more deserializing is not always needed and for performance it is
 * better that it can be called from/on the object itself only when it is needed
 */
interface Deserializable
{
    /**
     * dedicated method that uses the given associative array data, coming from decoded (de-serialized) JSON that was
     * originally JSON encoded via @see \JsonSerializable::jsonSerialize() , to build the implementing object.
     * all required indices in the array data to build the object are assumed to be present and all values are
     * presumed to be in known (basic PHP) data types.
     *
     * NOTES:
     *    - the array's syntax, keys & values are the result of PHP's @see json_decode() to associative (!) array
     *      from JSON data where that JSON data itself is the result of PHP's @see json_encode() on the original object
     *      implementing @see \JsonSerializable::jsonSerialize()
     *    - all values in the array - from top-level to deeply nested - are assumed to be one of the following
     *      PHP Data Types: string, integer, float, boolean, array or NULL. no other complex object/resource
     *      data type is used as this makes both (DB) storing as well as (de-) serializing easier & maintainable
     */
    public static function fromJsonDecodedArray(array $data): self;
}
