<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Contract;

use KUL\FormBundle\Domain\Utility\Person;

/**
 * Interface FormUserInterface.
 *
 * IMPORTANT: the formBundle needs a user to track what he/she has done (has answered which questions, when, in what
 * step, with what  answer, as a temp answer or a final answer, etc.), to show the user's full name if needed
 * (e.g. to show 'last answer given or altered  by...' on questions) and sometimes to determine if a user can still
 * submit an answer in a step based on with what role you let user access the form ( @see StepInterface::limitsEachUniqueUserToOnlyOneSubmitUnderRole()
 * the formBundle does not know what a 'user' is. so whenever you access the formBundle's services you will
 * have to create such a user on-the-fly in your app with a identifier (id or username or whatever that can UNIQUELY
 * identify that user in  your app) and a @see Person which is just a first and last name.
 * in your app, you probably get the logged-in user, get some UNIQUE identifier for it and its full name
 * and create a @see FormUser on-the-fly
 * for your app's sake, make sure the identifier is a unique string in your app, because this formBundle uses it to
 * store it in DB and it does not care, nor can it determine if that is unique
 *
 * custom VO
 *
 * FIXME: This interface has a confusing name. Can't we rename it as FormBundleUser or something?
 */
interface FormUserInterface
{
    /**
     * get the identifier (usually the) username of the user in a string-format that this domain understands & uses.
     *
     * this will be used to log actions & do some ha-already-submitted-before checks
     */
    public function getIdentifier(): string;

    /**
     * get data like firstName, lastName from the user in the object-format that this domain understands & uses.
     */
    public function getPerson(): Person;
}
