<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Contract;

use Ramsey\Uuid\Uuid;

/**
 * Interface UIdentifiableInterface.
 *
 * Indicates that the object can be identified by the uid string
 *
 * note: usually this is based on a string generated @see Uuid::uuid4() . This contract returns a string and does not
 *       assumes it will be an Uuid object or convertable in to one, because this contract has to deal with existing
 *       code with uid/getUid as naming for the identifying and because existing DB data for which a string can be
 *       expected as uid which was not necessarily generated (or is valid) as Uuid::uuid4 syntax.
 */
interface UIdentifiableInterface
{
    /**
     * @var string
     *
     * @psalm-suppress AmbiguousConstantInheritance this is a bug in psalm https://github.com/vimeo/psalm/issues/7818
     */
    public const KEY_UID = 'uid';

    /**
     * Returns uid string allowing to identify the object.
     */
    public function getUid(): string;
}
