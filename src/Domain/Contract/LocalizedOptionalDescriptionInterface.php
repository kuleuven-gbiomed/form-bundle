<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Contract;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;

/**
 * Interface LocalizedOptionalDescriptionInterface.
 *
 * contracts an object to have an optional description. hence all locale translations are optional.
 */
interface LocalizedOptionalDescriptionInterface
{
    /**
     * returns a localized version of a description with non-empty translations for locales (if any). the
     * localized version could be 'empty' (i.e. no translations at all available).
     *
     * @throws LocalizedStringException if there are invalid locale translations when building the localized string
     */
    public function getLocalizedDescription(): LocalizedOptionalString;

    /**
     * check if a translation is available for given locale or if there is a fallback.
     *
     * @throws LocalizedStringException if invalid locale
     */
    public function hasDescription(string $locale): bool;

    /**
     * get description for the requested locale or use the fallback if none exist for locale.
     * throws exception if locale is invalid or requested locale translation does not exist and no fallback is available.
     *
     * @throws LocalizedStringException if invalid locale or if there is no translated description for locale and no fallback
     */
    public function getDescription(string $locale): string;
}
