<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Exception;

/**
 * Class LocalizedStringException.
 */
class LocalizedStringException extends \Exception
{
}
