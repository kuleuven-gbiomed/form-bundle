<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Target\Utility;

use KUL\FormBundle\Client\Manage\IsoDateConverter;
use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Client\Util\File\DownloadFileUrl;
use KUL\FormBundle\Domain\Template\Element\Node\Input\CheckboxInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\ChoiceInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\NumberInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\DateInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Entity\StoredTemplateTarget;

class TargetAnswerExtractor
{
    /**
     * @var string
     */
    final public const CHOICES_VALUE_DELIMITER = ' # ';

    private readonly \NumberFormatter $numberFormatter;

    private \HTMLPurifier $htmlPurifier;

    /** @param array<array-key, mixed> $submittedValues */
    private function __construct(
        private readonly array $submittedValues,
        private readonly SavedFileCollection $savedFiles,
        private readonly DownloadFileUrl $downloadFileUrl,
        string $locale,
    ) {
        $this->numberFormatter = new \NumberFormatter($locale, \NumberFormatter::DECIMAL);
        $this->htmlPurifier = new \HTMLPurifier();
    }

    public static function createForTarget(
        StoredTemplateTarget $target,
        DownloadFileUrl $downloadFileUrl,
        string $locale,
    ): self {
        return new self(
            $target->getSubmittedFormValues(),
            $target->getSavedFiles(),
            $downloadFileUrl,
            $locale
        );
    }

    public static function fromValues(
        array $submittedValues,
        SavedFileCollection $savedFiles,
        DownloadFileUrl $downloadFileUrl,
        string $locale,
    ): self {
        return new self(
            $submittedValues,
            $savedFiles,
            $downloadFileUrl,
            $locale,
        );
    }

    public function setHtmlPurifier(\HTMLPurifier $htmlPurifier): void
    {
        $this->htmlPurifier = $htmlPurifier;
    }

    public function getSubmittedValueForNode(InputNode $node, bool $exportScores = false): string
    {
        $uid = $node->getUid();
        $input = $node->getInput();

        if (!array_key_exists($uid, $this->submittedValues)) {
            return '';
        }

        switch (true) {
            case $input instanceof TextInput:
                return $this->htmlPurifier->purify((string) $this->submittedValues[$uid]);
            case $input instanceof NumberInput:
                // in case of submitted blank (e.g. locked but not unlocked question)
                if (!is_numeric($this->submittedValues[$uid])) {
                    return '';
                }

                if (NumberInput::DATA_TYPE_INTEGER === $input->getDataType()) {
                    return (string) $this->submittedValues[$uid];
                }

                // I don't know what's going on here:

                $formatted = $this->numberFormatter->format(+$this->submittedValues[$uid]);
                if (!is_string($formatted)) {
                    throw new \Exception('NumberFormatter::format failed');
                }

                return $formatted;
                // double check to attempt to satisfy phpstan that $input::isMultiple() exists
            case $node instanceof ChoiceInputNode && $input instanceof ChoiceInput:
                $selectedOutputs = [];
                foreach ($node->getAvailableOptions() as $optionInputNode) {
                    /** @var OptionInputNode $optionInputNode */
                    if (!array_key_exists($optionInputNode->getUid(), $this->submittedValues)) {
                        continue;
                    }

                    if ($exportScores && $node->meetsScoringRequirements()) {
                        $selectedOutput = $optionInputNode->getScoringValue();
                    } else {
                        $selectedOutput = $optionInputNode->getLabel('en'); // an optionInput should ALWAYS have a label
                        if ($optionInputNode->usesAdditionalTextInput()) {
                            $selectedOutput .= ': '.(string) $this->submittedValues[$optionInputNode->getUid()];
                        }
                    }

                    $selectedOutputs[] = $selectedOutput;

                    if (!$input->isMultiple()) {
                        break;
                    } // if not multiple, only take label of first found active optionInputNode child
                }

                return implode(self::CHOICES_VALUE_DELIMITER, $selectedOutputs);
            case $input instanceof CheckboxInput:
                if ((bool) $this->submittedValues[$uid]) {
                    return '✓';
                }

                return '✗';
            case $node instanceof UploadInputNode:
                $uids = UploadInput::extractSavedFileUidsFromFormValue($this->submittedValues[$uid]);
                $strings = [];

                foreach ($this->savedFiles->getForInputNode($node)->getWhereInUids($uids) as $file) {
                    $string = $file->getOriginalName();
                    // TODO DV for excel export, check for a way to add multiple hyperlinks in a cell instead of plain url texts
                    $string .= ' (URL: '.$this->downloadFileUrl->getUrlBySavedFile($file).')';

                    $strings[] = $string;
                }

                return implode(' | ', $strings);
            case $node instanceof DateInputNode:
                $rawDate = $this->submittedValues[$uid];
                if (null === $rawDate) {
                    return '';
                }
                if (0 === mb_strlen((string) $rawDate)) {
                    return '';
                }
                $date = new \DateTimeImmutable($rawDate);
                $format = IsoDateConverter::convertToPhpFormatMask($node->getInput()->getFormatMask());

                return $date->format($format);
            default:
                return '';
        }
    }
}
