<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Target\Utility;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Client\Storing\TargetReference;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;

/**
 * Class TemplateTargetableCollection.
 *
 * @see     TemplateTargetableInterface
 *
 * @extends ArrayCollection<array-key,TemplateTargetableInterface>
 */
class TemplateTargetableCollection extends ArrayCollection
{
    public function hasTargetable(TemplateTargetableInterface $targetable): bool
    {
        /** @var TemplateTargetableInterface $collectionTargetable */
        foreach ($this->getIterator() as $collectionTargetable) {
            if (TargetReference::fromTargetable($targetable)->equals(TargetReference::fromTargetable($collectionTargetable))) {
                return true;
            }
        }

        return false;
    }
}
