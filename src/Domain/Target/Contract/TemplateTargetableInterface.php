<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Target\Contract;

use KUL\FormBundle\DependencyInjection\Configuration;
use KUL\FormBundle\DependencyInjection\KULFormExtension;
use KUL\FormBundle\Domain\Target\Utility\TemplateTargetableCollection;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;

/**
 * Interface TemplateTargetableInterface.
 *
 * make all instances of an entity/object contracted with this interface, available for building and storing forms
 * configured by templates
 *
 * NOTE: combo of identifier and targetCode makes an object unique in your app as well as in this bundle (which for
 * the targetCode is guaranteed upon compile of this bundle: @see KULFormExtension::getValidConfigTargetablesOrFail()
 *
 * @see     TemplateTargetableCollection
 */
interface TemplateTargetableInterface
{
    /**
     * get unique app-wide code for the implementing class to uniquely save & identify the stored records of
     * template related answers (and workflow state, etc.) for all instances of this implementing entity/object.
     *
     * in conjunction with an instance-level @see getTargetIdentifier, this will help to identify
     * the single storage record with the form-answers for a target entity/object for a specific @see TemplateInterface
     *
     * most common is to use the (lower cased, with underscore separated) short class name, if unique enough in your app
     * but it can be anything, as long as it is unique per object/entity class. the uniqueness of this code per
     * entity/object in your app is asserted on bundle compiling in @see KULFormExtension::getValidConfigTargetablesOrFail()
     *
     * WARNING: this code is used is DB in an unique index, on which we will also query, so it should not exceed 255
     * chars. also asserted upon compile in @see KULFormExtension::getValidConfigTargetablesOrFail
     * (in unique index to identify and assert one unique record of answers for target in template
     * together with @see getTargetIdentifier
     * and a @see TemplateInterface::getId() )
     */
    public static function getTargetCode(): string;

    /**
     * get unique entity/object identifier (usually the (u)id property) to uniquely save & identify the stored records
     * of template related answers (and workflow state, etc.) for each unique instance of this implementing entity/object
     * and to use for building template-form-target related routes witch rely on a parameter to identify the target.
     *
     * storing: in conjunction with an app-wide level @see getTargetCode, this will help to identify
     * the single storage record with the form-answers for a target entity/object for a specific @see TemplateInterface .
     * routing: this bundle will use routes (configured by your app) for all form related actions (getting views,
     * posting forms, calculating score via ajax, etc.) these dynamic route building will add the necessary
     * parameters related to those actions. each @see TemplateInterface::getTargetingType() has its own
     * set or routes because the access to the entities/objects that are targeted are to be determined by
     * your app and can (and will) be different per @see TemplateInterface::getTargetingType() . although the routes
     * are build by this bundle, it is your app that will have to supply the controller-route-actions
     * and handle non-directly-template related stuff itself. (preliminary access to the target object, redirecting after
     * success storing, rendering the returned view/parameters in dedicated twigs, etc.)
     * therefore, each route has to be able to fetch the target from the route parameter so it can do
     * all this other and for that the identifier of the target is required to build those routes, in combination
     * with the routes defined per Template targetingType and required via config @see Configuration .
     *
     * NOTE: in case your app uses mapped entities with (auto incremented integer) primary keys, this identifier
     * can be simply that PK id. Primary keys of the auto incremented integer type are only unique to the entity,
     * which is why the @see getTargetCode is needed to identify specific entities.
     * if your app uses objects that are dynamically build, composites, coming from events, ..., then
     * the @see getTargetCode might be overkill, but you still have to provide it!
     * it is up to your app to make sure the identifier for these kinds of objects are unique, at least to the
     * instances of this implementing object or -  better even - on an app-level (which will not be possible
     * if you useDB entities with integer primary keys)
     *
     * WARNING: this id is used is DB in an unique index, on which we will also query, so it should not exceed 255 chars.
     * unfortunately this can not be asserted and if it exceeds, it will be cut of in MySQL inserts.
     * (in unique index to identify and assert one unique record of answers for target in template
     * together with @see getTargetCode
     * and a @see TemplateInterface::getId() )
     */
    public function getTargetIdentifier(): string;

    /**
     * I guess this is the reference data that will be used to calculate the dates of the calculated periods?
     */
    public function getReferenceDate(): \DateTime;
}
