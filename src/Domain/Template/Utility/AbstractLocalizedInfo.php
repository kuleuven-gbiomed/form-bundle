<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Contract\LocalizedOptionalDescriptionInterface;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\AbstractLocalizedString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Class AbstractLocalizedInfo.
 *
 * allows to retrieve & store serializable localized information texts (label, descriptions and role related
 * descriptions) in a generic manner on all elements in a template that use this kind of information
 *
 * developer notice: apart from re-occurring use of 'info' consisting of (optional) label + optional description +
 * optional set of role-descriptions in elements all over the place in a template, the main reason for this bundling
 * 'info' class is because in a sense, this remains somewhat WIP code: I learned that additional optional texts
 * could/will have to be added that neither we nor client has thought of and suddenly become (optionally) needed.
 * hence, bundling these dynamically translated info strings allows serialization as one bundle, makes adding additional
 * parameters later on easier
 */
abstract class AbstractLocalizedInfo implements LocalizedOptionalDescriptionInterface, \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_LABEL = 'label';
    /** @var string */
    public const KEY_DESCRIPTION = 'description';
    /** @var string */
    public const KEY_ROLE_DESCRIPTIONS = 'roleDescriptions';

    protected function __construct(
        protected AbstractLocalizedString $label,
        protected LocalizedOptionalString $description,
        protected RoleLocalizedOptionalStringCollection $roleDescriptions,
    ) {
    }

    public function getLocalizedLabel(): AbstractLocalizedString
    {
        return $this->label;
    }

    /**
     * @throws LocalizedStringException if no valid locale string given
     */
    public function getLabel(string $locale): string
    {
        return $this->getLocalizedLabel()->getForLocaleOrFallback($locale);
    }

    public function getLocalizedDescription(): LocalizedOptionalString
    {
        return $this->description;
    }

    /**
     * @throws LocalizedStringException if no valid locale string given
     */
    public function hasDescription(string $locale): bool
    {
        return $this->getLocalizedDescription()->hasForLocaleOrFallback($locale);
    }

    /**
     * @throws LocalizedStringException if no valid locale string given or no translation or fallback found
     */
    public function getDescription(string $locale): string
    {
        return $this->getLocalizedDescription()->getForLocaleOrFallback($locale);
    }

    public function getRoleLocalizedDescriptions(): RoleLocalizedOptionalStringCollection
    {
        return $this->roleDescriptions;
    }

    /**
     * @throws LocalizedStringException if no valid locale string given
     */
    public function hasRoleDescription(Role $role, string $locale): bool
    {
        if (!$this->getRoleLocalizedDescriptions()->hasOneForRole($role)) {
            return false;
        }

        return $this->getRoleLocalizedDescriptions()
            ->getOneForRole($role)
            ->getLocalizedString()
            ->hasForLocaleOrFallback($locale);
    }

    /**
     * @throws LocalizedStringException if no valid locale string given or no translation or fallback found
     */
    public function getRoleDescription(Role $role, string $locale): string
    {
        return $this->getRoleLocalizedDescriptions()
            ->getOneForRole($role)
            ->getLocalizedString()
            ->getForLocaleOrFallback($locale);
    }

    /**
     * @return array{label: AbstractLocalizedString, description: LocalizedOptionalString, roleDescriptions: RoleLocalizedOptionalStringCollection}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_LABEL => $this->getLocalizedLabel(),
            self::KEY_DESCRIPTION => $this->getLocalizedDescription(),
            self::KEY_ROLE_DESCRIPTIONS => $this->getRoleLocalizedDescriptions(),
        ];
    }
}
