<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility;

use KUL\FormBundle\Domain\Contract\LocalizedRequiredLabelInterface;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;

/**
 * Class LocalizedInfoWithRequiredLabel.
 *
 * extended version of @see AbstractLocalizedInfo that asserts at least one locale translation for the label text
 * all other translated texts (descriptions) are optional
 */
class LocalizedInfoWithRequiredLabel extends AbstractLocalizedInfo implements LocalizedRequiredLabelInterface
{
    final protected function __construct(
        LocalizedRequiredString $label,
        LocalizedOptionalString $description,
        RoleLocalizedOptionalStringCollection $roleDescriptions,
    ) {
        parent::__construct($label, $description, $roleDescriptions);
    }

    /**
     * create with only the required label and adding only empty descriptions.
     *
     * @throws LocalizedStringException
     */
    public static function fromLocalizedRequiredLabel(LocalizedRequiredString $label): static
    {
        return new static(
            $label,
            LocalizedOptionalString::createEmpty(),
            RoleLocalizedOptionalStringCollection::createEmpty()
        );
    }

    public static function fromLocalizedStrings(
        LocalizedRequiredString $label,
        LocalizedOptionalString $description,
        RoleLocalizedOptionalStringCollection $roleDescriptions,
    ): static {
        return new static($label, $description, $roleDescriptions);
    }

    /**
     * build info based on data with minimal requirements; assuming & asserting that the required label is present
     * with at least one valid translation, and only adding the optional description / role descriptions if present.
     *
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        // (role)descriptions are optional, so absence of their respective array keys is allowed
        $descriptionData = $data[self::KEY_DESCRIPTION] ?? [];
        $roleDescriptionsData = $data[self::KEY_ROLE_DESCRIPTIONS] ?? [];

        return new static(
            LocalizedRequiredString::fromJsonDecodedArray($data[self::KEY_LABEL]),
            LocalizedOptionalString::fromJsonDecodedArray($descriptionData),
            RoleLocalizedOptionalStringCollection::fromJsonDecodedArray($roleDescriptionsData)
        );
    }

    /**
     * overwritten for type hint on specific return type.
     */
    public function getLocalizedLabel(): LocalizedRequiredString
    {
        /** @var LocalizedRequiredString $label */
        $label = parent::getLocalizedLabel();

        return $label;
    }
}
