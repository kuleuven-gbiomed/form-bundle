<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\StepPeriodInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Factory\StepFactory;

/**
 * Class MixedCalculatedAndFixedStepCollection.
 *
 * special kind of collection of workflow steps. combining @see StepWithCalculatedDateStart steps that use dynamic
 * start & end dates calculated using some custom date knwon to the target entities with @see StepWithFixedDateStart
 * that have fixed start and end dates baked in the step itself and do not use dates from the target entities.
 *
 * IMPORTANT DOMAIN DECISION TO KEEP IN MIND: a step with fixed dates @see StepWithFixedDateStart has always priority
 * over a step with calculated dates @see StepWithCalculatedDateStart (e.g. when deciding which step is open based on a date )
 *
 * IMPORTANT: this kind of collection of steps can cause overlap between steps so it overrides several methods!
 * normally in a valid @see WorkFlow , all steps should be of the same type. either using calculated dates or
 * fixed dates. this avoids any possibility of overlap between steps because in a valid workflow, only one or no step
 * can be open (= started and not yet finished) at any time. on request of app owners/clients, this collection class with
 * mixture of steps is provided. because it is not possible to fully avoid overlap and order the steps based on
 * both calculated ad fixed dates , this collection makes assumptions and does not goes all the way on its
 * checks regarding overlap. in fact, several methods are overridden to make it work, assuming that the collection was
 * made by admins who knew the target entities and that no overlap would occur at time of publish. Admins making such
 * a collection of steps in a workflow are warned about all this and are assumed to know overlap is not fully avoidable.
 * on publish, it is possible to check for overlap for all known target entities and their state. but if the target
 * entities are changed afterwards (i.e. the date associated with the changed target entities is changed) or new are
 * added to the DB, no guarantee of overlap can be made.in other words, the admins have to be responsible when creating
 * such a collection of steps for a workflow.
 *
 * it is also of the most importance when looking at this collection class, to remember that a step with fixed dates
 * always has priority over a step with calculated dates. we can check in this collection for overlap between steps of
 * the same type. but we can not decisively check for overlap between steps of different type. hence when we look for
 * steps being open by their dates and we find two and they are not of the same type, the one with the fixed dates will
 * be considered as the one @see MixedCalculatedAndFixedStepCollection::getOneStepThatIsOpenByDate() while the one
 * with the calculated dates will be ignored! for one part because the dates on the target entities used to calculate
 * start and end dates can change after template is published. and for another part because this combo of step type can
 * be intentional; e.g. to ensure that a last step - having fixed dates as only step in the workflow - is always started,
 * regardless if for some target entities the preceding step (with calculated dates) is not yet ended.
 *
 * EXAMPLE:
 * a mix of @see StepWithFixedDateStart and @see StepWithCalculatedDateStart was to be allowed
 * because it was be made possible to have a workflow with steps with the calculated dynamic dates based on the app's
 * entities but also with a (usually the last) step or more steps that have fixed (usually start dates) so that those
 * steps with the fixed dates can overrule the other steps with the calculated dates.
 * a simple example is where a template target entities that all have some different date like - for example -
 * 'showToUsersStartDate'. the workflow in that template has three steps of which the first two are based on calculated
 * dates and a third step with a fixed start (and end) date. the idea is that for each target entities, the first and
 * second steps are to open up and close down, based on the showToUsersStartDate of the target. but on a fixed moment
 * in time - namely the fixed start date in the third step - the first and second step must be ignored and the third
 * step MUST start, regardless if the first and/or second is still open for the individual target entities.
 *
 * @extends AbstractStepCollection<array-key, StepInterface>
 */
final class MixedCalculatedAndFixedStepCollection extends AbstractStepCollection
{
    public static function createEmpty(): self
    {
        return new self();
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $elements = [];

        foreach ($data as $stepData) {
            $elements[] = StepFactory::createFromSerializedArray($stepData);
        }

        return new self($elements);
    }

    /** @var string */
    public const TYPE = 'stepsWithCalculatedDateStartOrFixedDateStart';

    /**
     * get the subset of steps using calculated dates @see StepWithCalculatedDateStart.
     */
    public function getStepsWithCalculatedDateStart(): StepWithCalculatedDateStartCollection
    {
        /** @var StepWithCalculatedDateStart[] $steps */
        $steps = $this->filter(fn (StepInterface $step) => $step instanceof StepWithCalculatedDateStart)->toArray();

        return new StepWithCalculatedDateStartCollection($steps);
    }

    /**
     * get the subset of steps using fixed dates @see StepWithFixedDateStart.
     */
    public function getStepsWithFixedDateStart(): StepWithFixedDateStartCollection
    {
        /** @var StepWithFixedDateStart[] $steps */
        $steps = $this->filter(fn (StepInterface $step) => $step instanceof StepWithFixedDateStart)->toArray();

        return new StepWithFixedDateStartCollection($steps);
    }

    /**
     * checks if at least one step uses calculated dates to start.
     */
    public function hasStepsWithCalculatedDateStart(): bool
    {
        return !$this->getStepsWithCalculatedDateStart()->isEmpty();
    }

    /**
     * checks if at least one step uses fixed dates to start.
     */
    public function hasStepsWithFixedDateStart(): bool
    {
        return !$this->getStepsWithFixedDateStart()->isEmpty();
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    /**
     * sorts the steps (if any) by their order in the flow, based on whether or not steps of different types are used.
     *
     * WARNING: if no mix of steps with different types is used, we can use the sorting from the single type of sets.
     * but if a mix of different types is used, it is impossible to sort the steps of different type against each other,
     * since the steps with calculated dates need a date that is not fixed and only known to the target entity/object.
     * while we can use an arbitrary date if only steps with calculated dates have to be sorted, we can not do that if
     * those steps are combined with steps with fixed dates. hence it is assumed that in the admin-side upon construction
     * of the @see WorkFlow to which this collection belongs, the sorting is checked and handled upon construct and
     * update (i.e. when new steps are added). in other words: using this type of collection with mixture of step types
     * requires sensible responsibility by those workflow creating admins (and of the developer making the admin side)
     * while on client (end-user) side, we can also use @see getSortedByAscendingFlowAndTargetDate if we know the target
     * entity/object to get a date from to use.
     *
     * @see StepInterface::hasOverlapWithStep()
     */
    public function getSortedByAscendingFlow(): self
    {
        $fixedSteps = $this->getStepsWithFixedDateStart();
        $calculatedSteps = $this->getStepsWithCalculatedDateStart();

        // if no mix of steps and no fixed steps, return new mixed collection with the sorted calculated ones (if any)
        if ($fixedSteps->isEmpty()) {
            return new self($calculatedSteps->getSortedByAscendingFlow()->toArray());
        }

        // if no mix of steps and no calculated steps, return new mixed collection with the sorted fixed ones (if any)
        if ($calculatedSteps->isEmpty()) {
            return new self($fixedSteps->getSortedByAscendingFlow()->toArray());
        }

        /*
         * we can not decisively sort if two types of steps are used =>  assume they are sorted upon construct
         * in admin-side.
         * however, at run-time in client side, the @see getSortedByAscendingFlowAndTargetDate can be used to sort
         * based on custom date coming from target, getting some sensible sorting custom to that target.
         * consequently calling @see getSortedByAscendingFlow inside other method on that filtered result
         * (e.g. @see AbstractStepCollection::doesFlowStartWithStep() )will then always maintain the sorting
         * filtered by @see getSortedByAscendingFlowAndTargetDate.
         */
        return new self($this->toArray());
    }

    /**
     * TODO: when mixed, test this and find a better way / algorithm to sort this as good as it gets: first sort both step types separably,
     * TODO: then hold both sorted subsets against the existing order the mixed steps, matching positions of mixed types.
     *
     * sorts the steps (if any) by their order in the flow, based on @see StepInterface::startsBeforeStep() to compare
     * the steps of the same type and based on a given date (coming from the entity/object that is being targeted by
     * the @see TemplateInterface ) to compare & order the steps with fixed dates with steps with calculated dates
     *
     * WARNING: in this collection, a mix of steps with fixed startDate and calculated startDate is used. overlap is
     * very much possible and there is no (decisive) way to sort based on a combination of fixed and calculated dates,
     * using some arbitrary date to compare the steps with calculated start date with the steps with fixed start date.
     * hence the extra parameter $targetDate so we can compare calculated dates with fixed dates, assuming this
     * targetDate is coming from the targeted entity/object that has a date to calculate with.
     * keep in mind that if two steps of different types start on exactly the same time, the step with fixed date
     * always has priority and thus is placed before the step with calculated dates.
     *
     * NOTE: this is a very specific method for usage. it your app, to enforce an ordering by logical workflow, you
     * could use this by calling it on the target (@see TemplateTargetableInterface ) being targeted by
     * the @see TemplateInterface that this steps collection in a workflow belongs to.
     * it could be used indirectly to validate if a workflow has no overlap in the admin-side before publishing
     * the workflow as part of a template. for that you you could call this method on EVERY
     * target (@see TemplateTargetableInterface ) that you know of (in SCONE this is e.g. every placement) at the time.
     * once you called it on a target, you could chain it with @see AbstractStepCollection::hasOverlap() as a way to
     * check if for a date associated with a specified target, this workflow does or does not has overlap at that time
     */
    public function getSortedByAscendingFlowAndTargetDate(\DateTimeImmutable $targetCalculatedCheckDate): self
    {
        /** @var \ArrayIterator<array-key, StepInterface> $iterator */
        $iterator = $this->getIterator();

        $iterator->uasort(
            function (StepInterface $a, StepInterface $b) use ($targetCalculatedCheckDate) {
                // use normal comparison if steps are of same type

                if ($a instanceof StepWithFixedDateStart && $b instanceof StepWithFixedDateStart) {
                    return $a->startsBeforeFixedStep($b) ? -1 : 1;
                }

                if ($a instanceof StepWithCalculatedDateStart && $b instanceof StepWithCalculatedDateStart) {
                    return $a->startsBeforeCalculatedStep($b) ? -1 : 1;
                }

                // if steps are of different type, sort as good as it gets
                /** @var StepFixedPeriod|StepCalculatedPeriod $aPeriod */
                $aPeriod = $a->getPeriod();
                /** @var StepFixedPeriod|StepCalculatedPeriod $bPeriod */
                $bPeriod = $b->getPeriod();

                $aStartDate = $aPeriod instanceof StepFixedPeriod ? $aPeriod->getFixedStartDate() : $aPeriod->getCalculatedStartDate($targetCalculatedCheckDate);
                $bStartDate = $bPeriod instanceof StepFixedPeriod ? $bPeriod->getFixedStartDate() : $bPeriod->getCalculatedStartDate($targetCalculatedCheckDate);

                // for equal starts, the step with fixed date has priority and must be placed before step with calculated dates
                // cannot really estimate the slim chance that two dates from different step types will match, but can't harm to include it

                // We should just do if ($aStartDate == $bStartDate), but phpa complains about this.
                // So I submitted an issue: https://github.com/rskuipers/php-assumptions/issues/35
                // and for the moment I use this ugly workaround :-(
                if ($aStartDate <= $bStartDate && $aStartDate >= $bStartDate) { // intentional non-strict object equality comparison for dates
                    return $a instanceof StepWithFixedDateStart ? -1 : 1;
                }

                return $aStartDate < $bStartDate ? -1 : 1;
            }
        );

        return new self(iterator_to_array($iterator));
    }

    /**
     * Returns true if for every reference date the collection has overlapping steps.
     */
    public function hasReferenceDateIndependentOverlappingSteps(): bool
    {
        $fixedSteps = $this->getStepsWithFixedDateStart();
        $calculatedSteps = $this->getStepsWithCalculatedDateStart();

        return $fixedSteps->hasOverlappingSteps() || $calculatedSteps->hasOverlappingSteps();
    }

    public function getReferenceDateIndependentOverlappingSteps(): self
    {
        $overlappingFixedSteps = $this->getStepsWithFixedDateStart()->getOverlappingSteps();
        $overlappingCalculatedSteps = $this->getStepsWithCalculatedDateStart()->getOverlappingSteps();

        return new self(array_merge(
            $overlappingFixedSteps->toArray(),
            $overlappingCalculatedSteps->toArray()
        ));
    }

    /**
     * find open steps (steps already started but not ended) on the given $checkDate.
     *
     * The given $referenceDateForFirstStepStart is used to calculate
     * the start date of the first AND ONLY THE FIRST step that uses calculated dates.
     * The end date of the first step AND BOTH START AND END DATES OF ALL OTHER STEPS use
     * $generalReferenceDate to calculate start and end dates for finding the 'open' steps.
     *
     * Note that in David's comments below, $referenceDateForFistStepStart is called 'primary date',
     * and $generalReferenceDate is called 'secondary date'. I renamed this because I thought
     * that would be clearer.
     *
     * this is done because of the use case where admins want to use a different date to calculate the end date of
     * the (first) step than the date used to calculate the start date of that (first) step. but we can only allow
     * such use of two different dates on the FIRST step! because if we were to use the given 'primary' date to
     * calculate the start date of every step and the secondary date to calculate the end date of every step, there
     * will ALWAYS BE OVERLAP between steps and thus never a one single step that is open by date, which is needed
     * to make a valid workflow work!.
     *
     * EXAMPLE
     * (keep in mind that this method returns all 'open' steps, but for a workable Workflow, only one or none can be
     * open at the time.
     *
     * @see AbstractStepCollection::getOneStepThatIsOpenByDate())
     *
     * suppose our given 'primary' date is 01/03/2019. Suppose step 1 is configured to start 0 days after a given
     * date and end 10 days after a given date while step 2 is configured to start 10 days after a given date and end 60
     * days after a given date. if we then use that one 'primary' date on both steps, there will be no overlap:
     * step 1 will start on 01/03/2019 (01/03/2019 + 10 days) and end on 11/03/2019 (01/03/2019 + 10 days)
     * step 2 will start on 11/03/2019 (01/03/2019 + 10 days) and end on 01/05/2019 (01/03/2019 + 60 days)
     * is date NOW is e.g. 13/03/2019, then only step 1 is open (date NOW falls between start and end date)!
     *
     * but if we use a primary date for all start dates and a secondary for all end dates, there will be overlap
     * between end of step 1 and start of step 2. suppose secondary date is 05/03/2019:
     * step 1 will start on 01/03/2019 (01/03/2019 + 10 days) and end on 15/03/2019 (05/03/2019 + 10 days)
     * step 2 will start on 11/03/2019 (01/03/2019 + 10 days) and end on 05/05/2019 (05/03/2019 + 60 days)
     * is date NOW is e.g. 13/03/2019, then BOTH step 1 and step 2 are open, which is unworkable for a workflow!
     */
    private function findStepsThatAreOpenByDates(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $referenceDateForFirstStepStart,
        \DateTimeInterface $generalReferenceDate,
    ): self {
        // get all calculatedSteps (collection could be only calculatedSteps or a combo of calculatedSteps and fixedSteps)
        $calculatedSteps = $this->filter(fn (StepInterface $step) => $step instanceof StepWithCalculatedDateStart);

        // if no calculated steps are used, reference dates are of no use.
        if ($calculatedSteps->isEmpty()) {
            return $this->filter(fn (StepInterface $step) => $step instanceof StepWithFixedDateStart && $step->isOpenByDate($checkDate));
        }

        // take uid of the starting step of the sub set of calculatedSteps for further filtering
        $startingCalculatedStepUid = $calculatedSteps->getStartingStep()->getUid();

        return $this->filter(function (StepInterface $step) use ($referenceDateForFirstStepStart, $generalReferenceDate, $startingCalculatedStepUid, $checkDate) {
            if ($step instanceof StepWithFixedDateStart) {
                return $step->isOpenByDate($checkDate);
            } elseif ($step instanceof StepWithCalculatedDateStart) {
                if ($step->getUid() === $startingCalculatedStepUid) {
                    return $step->isStartedByDate($checkDate, $referenceDateForFirstStepStart)
                        && !$step->isEndedByDate($checkDate, $generalReferenceDate);
                }

                return $step->isOpenByDate($checkDate, $generalReferenceDate);
            }

            throw new StepException('unknown step type');
        });
    }

    public function findDeadlinedStepsForDates(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $calculatedStepReferenceDate,
    ): self {
        return $this->filter(function (StepInterface $step) use ($calculatedStepReferenceDate, $checkDate) {
            if ($step instanceof StepWithFixedDateStart) {
                return $step->isEndedByDate($checkDate);
            } elseif ($step instanceof StepWithCalculatedDateStart) {
                return $step->isEndedByDate($checkDate, $calculatedStepReferenceDate);
            }

            throw new StepException('unknown step type');
        });
    }

    /**
     * Returns the single open step at a given $date.
     *
     * @see PublishedVersion::getWorkFlow() )there can either be only one step open
     * at the time or no step open at the time.
     *
     * IMPORTANT:
     * Since a mix of step types is used,
     * it is impossible to avoid overlap between two steps of different types.
     * If two steps are found of different types, that are open by date,
     * then the STEP WITH FIXED DATES HAS PRIORITY OVER A STEP WITH CALCULATED DATES.
     */
    public function findOpenStep(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $referenceDateForFirstStepStart,
        \DateTimeInterface $generalReferenceDate,
    ): ?StepInterface {
        $openSteps = $this->findStepsThatAreOpenByDates($checkDate, $referenceDateForFirstStepStart, $generalReferenceDate);

        // if none found or if only one found, then it does not matter what type that one step is
        if ($openSteps->count() <= 1) {
            return $openSteps->getFirst();
        }

        // in case collection mixes steps, get each sub set of step type so we can check & assert no overlap within same type
        $openFixedSteps = $openSteps->filter(fn (StepInterface $step) => $step instanceof StepWithFixedDateStart);
        $openCalculatedSteps = $openSteps->filter(fn (StepInterface $step) => $step instanceof StepWithCalculatedDateStart);

        // in any case either only one or none of each type of step may exist!
        if ($openFixedSteps->count() > 1) {
            $dateString = $referenceDateForFirstStepStart->format(StepPeriodInterface::DATE_FORMAT);
            throw new \BadMethodCallException('expected to find exactly one or no step of type  ['.StepWithFixedDateStart::class.'] open by date ['.$dateString.'], but found '.((string) $openFixedSteps->count()).' elements!');
        }

        // in any case either only one or none of each type of step may exist!
        if ($openCalculatedSteps->count() > 1) {
            $dateString = 'open by date ['.$referenceDateForFirstStepStart->format(StepPeriodInterface::DATE_FORMAT).']';
            $dateString .= ' and secondary date ['.$generalReferenceDate->format(StepPeriodInterface::DATE_FORMAT).']';

            throw new \BadMethodCallException('expected to find exactly one or no step of type  ['.StepWithCalculatedDateStart::class.'] '.$dateString.', but found '.((string) $openCalculatedSteps->count()).' elements!');
        }

        // at this point, there is either only one step of one type found or exactly one of each type
        // the step with fixed dates always has priority, de facto ignoring the one with calculated dates (if any)
        if (1 === $openFixedSteps->count()) {
            return $openFixedSteps->getFirst();
        }

        // if no step with fixed dates, then return the one with calculated dates (which at this point is only one left)
        return $openCalculatedSteps->getFirst();
    }

    public function hasOpenStep(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $referenceDateForFirstStepStart,
        \DateTimeInterface $generalReferenceDate,
    ): bool {
        return $this->findOpenStep($checkDate, $referenceDateForFirstStepStart, $generalReferenceDate) instanceof StepInterface;
    }

    public function getOpenStep(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $referenceDateForFirstStepStart,
        \DateTimeInterface $generalReferenceDate,
    ): StepInterface {
        $step = $this->findOpenStep($checkDate, $referenceDateForFirstStepStart, $generalReferenceDate);

        if (!$step instanceof StepInterface) {
            $dateString = 'open by date ['.$referenceDateForFirstStepStart->format(StepPeriodInterface::DATE_FORMAT).']';
            $dateString .= ' and secondary date ['.$generalReferenceDate->format(StepPeriodInterface::DATE_FORMAT).']';

            throw new \BadMethodCallException('expected to find exactly one step '.$dateString.', but found none');
        }

        return $step;
    }

    /**
     * alternative for @see static::first() that returns null if no elements instead of false.
     */
    public function getFirst(): ?StepInterface
    {
        $first = parent::first();

        return $first instanceof StepInterface ? $first : null;
    }
}
