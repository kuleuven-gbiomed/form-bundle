<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\StepPeriodInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Factory\StepFactory;

/**
 * @extends AbstractStepCollection<array-key, StepWithCalculatedDateStart>
 *
 * @implements SortableStepCollection<array-key, StepWithCalculatedDateStart>
 * @implements OverlapAwareStepCollection<array-key, StepWithCalculatedDateStart>
 */
final class StepWithCalculatedDateStartCollection extends AbstractStepCollection implements SortableStepCollection, OverlapAwareStepCollection
{
    /** @var string */
    public const TYPE = 'stepsWithCalculatedDateStart';

    public static function createEmpty(): self
    {
        return new self();
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $elements = [];

        foreach ($data as $stepData) {
            $element = StepFactory::createFromSerializedArray($stepData);

            if (!$element instanceof StepWithCalculatedDateStart) {
                throw new StepException('Elements must be StepWithCalculatedDateStart');
            }
            $elements[] = $element;
        }

        return new self($elements);
    }

    protected static function guardStepClass(StepInterface ...$elements): void
    {
        $type = StepWithCalculatedDateStart::getType();
        foreach ($elements as $element) {
            if (!($element::getType() === $type)) {
                throw new \Exception('expect step to be of type '.$type);
            }
        }
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    /**
     * sort steps by comparing each against the other based on the calculated start dates of each step, using an arbitrary date.
     */
    public function getSortedByAscendingFlow(): self
    {
        /** @var \ArrayIterator<array-key, StepWithCalculatedDateStart> $iterator */
        $iterator = $this->getIterator();

        $iterator->uasort(
            fn (StepWithCalculatedDateStart $a, StepWithCalculatedDateStart $b) => $a->startsBeforeCalculatedStep($b) ? -1 : 1
        );

        return new self(iterator_to_array($iterator));
    }

    public function isOverlappingStep(StepWithCalculatedDateStart $step): bool
    {
        /** @var StepWithCalculatedDateStart $controlStep */
        foreach ($this->getIterator() as $controlStep) {
            // don't get false positives by comparing to self if given step is already known to collection
            if ($controlStep->getUid() === $step->getUid()) {
                continue;
            }

            if ($controlStep->overlapsWithCalculatedStep($step)) {
                return true;
            }
        }

        return false;
    }

    public function sort(): self
    {
        /** @var \ArrayIterator<array-key, StepWithCalculatedDateStart> $iterator */
        $iterator = $this->getIterator();

        $iterator->uasort(function (StepWithCalculatedDateStart $step1, StepWithCalculatedDateStart $step2) {
            if ($step1->startsBeforeCalculatedStep($step2)) {
                return -1;
            }

            if ($step2->startsBeforeCalculatedStep($step1)) {
                return 1;
            }

            return 0;
        });

        return new self(iterator_to_array($iterator));
    }

    public function hasOverlappingSteps(): bool
    {
        $sorted = $this->sort();

        /** @var StepWithCalculatedDateStart|null $previous */
        $previous = null;

        /** @var StepWithCalculatedDateStart $current */
        foreach ($sorted->getIterator() as $current) {
            if ($previous instanceof StepWithCalculatedDateStart && $previous->overlapsWithCalculatedStep($current)) {
                return true;
            }
            $previous = $current;
        }

        return false;
    }

    public function getOverlappingSteps(): self
    {
        /** @var StepWithCalculatedDateStart[] $overlappingSteps */
        $overlappingSteps = [];

        $sorted = $this->sort();

        /** @var StepWithCalculatedDateStart|null $previous */
        $previous = null;

        /** @var StepWithCalculatedDateStart $current */
        foreach ($sorted->getIterator() as $current) {
            if ($previous instanceof StepWithCalculatedDateStart && $previous->overlapsWithCalculatedStep($current)) {
                // using ID's as key, to avoid duplicates.
                $overlappingSteps[$previous->getUid()] = $previous;
                $overlappingSteps[$current->getUid()] = $current;
            }
            $previous = $current;
        }

        return new self($overlappingSteps);
    }

    /**
     * find open steps (steps already started but not ended) on the given $checkDate.
     *
     * This is a private function, because normally there can be only one step open at some date.
     *
     * The given $referenceDateForFistStepStart is used
     * to calculate the start date of the first AND ONLY THE FIRST step.
     * The end date of the first step AND BOTH START AND END DATES OF ALL OTHER STEPS use
     * $generalReferenceDate to calculate start and end dates for finding the 'open' steps.
     *
     * Note that in David's comments below, $referenceDateForFistStepStart is called 'primary date',
     * and $generalReferenceDate is called 'secondary date'. I renamed this because I thought
     * that would be clearer.
     *
     * this is done because of the use case where admins want to use a different date to calculate the end date of
     * the (first) step than the date used to calculate the start date of that (first) step. but we can only allow
     * such use of two different dates on the FIRST step! because if we were to use the given 'primary' date to
     * calculate the start date of every step and the secondary date to calculate the end date of every step, there
     * will ALWAYS BE OVERLAP between steps and thus never a one single step that is open by date, which is needed
     * to make a valid workflow work!.
     *
     * EXAMPLE
     * (keep in mind that this method returns all 'open' steps, but for a workable Workflow, only one or none can be
     * open at the time.
     *
     * @see AbstractStepCollection::getOneStepThatIsOpenByDate())
     *
     * suppose our given 'primary' date is 01/03/2019. Suppose step 1 is configured to start 0 days after a given
     * date and end 10 days after a given date while step 2 is configured to start 10 days after a given date and end 60
     * days after a given date. if we then use that one 'primary' date on both steps, there will be no overlap:
     * step 1 will start on 01/03/2019 (01/03/2019 + 0 days) and end on 11/03/2019 (01/03/2019 + 10 days)
     * step 2 will start on 11/03/2019 (01/03/2019 + 10 days) and end on 01/05/2019 (01/03/2019 + 60 days)
     * is date NOW is e.g. 13/03/2019, then only step 1 is open (date NOW falls between start and end date)!
     *
     * but if we use a primary date for all start dates and a secondary for all end dates, there will be overlap
     * between end of step 1 and start of step 2. suppose secondary date is 05/03/2019:
     * step 1 will start on 01/03/2019 (01/03/2019 + 0 days) and end on 15/03/2019 (05/03/2019 + 10 days)
     * step 2 will start on 11/03/2019 (01/03/2019 + 10 days) and end on 05/05/2019 (05/03/2019 + 60 days)
     * is date NOW is e.g. 13/03/2019, then BOTH step 1 and step 2 are open, which is unworkable for a workflow!
     */
    private function findStepsThatAreOpenByDates(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $referenceDateForFirstStepStart,
        \DateTimeInterface $generalReferenceDate,
    ): self {
        if ($this->isEmpty()) {
            // fire self again to trigger the default filtering without secondary date
            return $this;
        }

        // take uid of the starting step for further filtering
        $startingCalculatedStepUid = $this->getStartingStep()->getUid();

        return $this->filter(function (StepWithCalculatedDateStart $step) use (
            $referenceDateForFirstStepStart,
            $generalReferenceDate,
            $startingCalculatedStepUid,
            $checkDate
        ) {
            // if calculated step is the first (= starting) step with calculated dates, use both dates
            if ($step->getUid() === $startingCalculatedStepUid) {
                return $step->isStartedByDate($checkDate, $referenceDateForFirstStepStart) && !$step->isEndedByDate($checkDate, $generalReferenceDate);
            }

            // if calculated step is NOT first (= starting) step, use secondary date to calculate start & end date
            // to avoid causing overlap if first date would be used for calculating start date
            return $step->isOpenByDate($checkDate, $generalReferenceDate);
        });
    }

    public function findDeadlinedStepsForDates(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $calculatedStepReferenceDate,
    ): self {
        return $this->filter(fn (StepWithCalculatedDateStart $step) => $step->isEndedByDate($checkDate, $calculatedStepReferenceDate));
    }

    public function findOpenStep(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $referenceDateForFirstStepStart,
        \DateTimeInterface $generalReferenceDate,
    ): ?StepWithCalculatedDateStart {
        $openSteps = $this->findStepsThatAreOpenByDates($checkDate, $referenceDateForFirstStepStart, $generalReferenceDate);

        // if none found or if only one found, then it does not matter what type that one step is
        if ($openSteps->count() <= 1) {
            return $openSteps->getFirst();
        }

        $dateString = 'open by date ['.$referenceDateForFirstStepStart->format(StepPeriodInterface::DATE_FORMAT).']';
        $dateString .= ' and secondary date ['.$generalReferenceDate->format(StepPeriodInterface::DATE_FORMAT).']';

        throw new \BadMethodCallException('expected to find exactly one or no step of type  ['.StepWithCalculatedDateStart::class.'] '.$dateString.', but found '.((string) $openSteps->count()).' elements!');
    }

    public function hasOpenStep(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $referenceDateForFirstStepStart,
        \DateTimeInterface $generalReferenceDate,
    ): bool {
        return $this->findOpenStep($checkDate, $referenceDateForFirstStepStart, $generalReferenceDate) instanceof StepInterface;
    }

    public function getOpenStep(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $referenceDateForFirstStepStart,
        \DateTimeInterface $generalReferenceDate,
    ): StepWithCalculatedDateStart {
        $step = $this->findOpenStep($checkDate, $referenceDateForFirstStepStart, $generalReferenceDate);

        if (!$step instanceof StepInterface) {
            $dateString = 'open by date ['.$referenceDateForFirstStepStart->format(StepPeriodInterface::DATE_FORMAT).']';
            $dateString .= ' and secondary date ['.$generalReferenceDate->format(StepPeriodInterface::DATE_FORMAT).']';

            throw new \BadMethodCallException('expected to find exactly one step '.$dateString.', but found none');
        }

        return $step;
    }

    /**
     * alternative for @see self::first() that returns null if no elements instead of false.
     */
    public function getFirst(): ?StepWithCalculatedDateStart
    {
        $step = parent::first();

        return $step instanceof StepWithCalculatedDateStart ? $step : null;
    }
}
