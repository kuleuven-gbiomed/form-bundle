<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\StepPeriodInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Factory\StepFactory;

/**
 * @extends AbstractStepCollection<array-key, StepWithFixedDateStart>
 *
 * @implements SortableStepCollection<array-key, StepWithFixedDateStart>
 * @implements OverlapAwareStepCollection<array-key, StepWithFixedDateStart>
 */
final class StepWithFixedDateStartCollection extends AbstractStepCollection implements SortableStepCollection, OverlapAwareStepCollection
{
    /** @var string */
    public const TYPE = 'stepsWithFixedDateStart';

    public static function createEmpty(): self
    {
        return new self();
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $elements = [];

        foreach ($data as $stepData) {
            $element = StepFactory::createFromSerializedArray($stepData);

            if (!$element instanceof StepWithFixedDateStart) {
                throw new StepException('Elements must be StepWithFixedDateStart');
            }
            $elements[] = $element;
        }

        return new self($elements);
    }

    protected static function guardStepClass(StepInterface ...$elements): void
    {
        $type = StepWithFixedDateStart::getType();
        foreach ($elements as $element) {
            if (!($element::getType() === $type)) {
                throw new \Exception('expect step to be of type '.$type);
            }
        }
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    /**
     * sort steps by comparing each against the other based on the fixed start dates of each step.
     */
    public function getSortedByAscendingFlow(): self
    {
        /** @var \ArrayIterator<array-key, StepWithFixedDateStart> $iterator */
        $iterator = $this->getIterator();

        $iterator->uasort(
            fn (StepWithFixedDateStart $a, StepWithFixedDateStart $b) => $a->startsBeforeFixedStep($b) ? -1 : 1
        );

        return new self(iterator_to_array($iterator));
    }

    /**
     * find open steps (steps already started but not ended) based of given dates.
     *
     * Private function, because there can only be one open step.
     */
    private function findStepsThatAreOpenByDates(
        \DateTimeInterface $checkDate,
    ): self {
        return $this->filter(fn (StepWithFixedDateStart $collectionStep) => $collectionStep->isOpenByDate($checkDate));
    }

    /** @return StepCollectionInterface<array-key, StepWithFixedDateStart> */
    public function findDeadlinedStepsForDate(
        \DateTimeInterface $checkDate,
    ): StepCollectionInterface {
        return $this->filter(fn (StepWithFixedDateStart $collectionStep) => $collectionStep->isEndedByDate($checkDate));
    }

    public function findOpenStep(
        \DateTimeInterface $date,
    ): ?StepWithFixedDateStart {
        $openSteps = $this->findStepsThatAreOpenByDates($date);

        if ($openSteps->count() <= 1) {
            return $openSteps->getFirst();
        }

        $dateString = $date->format(StepPeriodInterface::DATE_FORMAT);
        throw new \BadMethodCallException('expected to find exactly one or no step of type  ['.StepWithFixedDateStart::class.'] open by date ['.$dateString.'], but found '.((string) $openSteps->count()).' elements!');
    }

    public function isOverlappingStep(StepWithFixedDateStart $step): bool
    {
        /** @var StepWithFixedDateStart $controlStep */
        foreach ($this->getIterator() as $controlStep) {
            // don't get false positives by comparing to self if given step is already known to collection
            if ($controlStep->getUid() === $step->getUid()) {
                continue;
            }

            if ($controlStep->overlapsWithFixedStep($step)) {
                return true;
            }
        }

        return false;
    }

    public function sort(): self
    {
        /** @var \ArrayIterator<array-key, StepWithFixedDateStart> $iterator */
        $iterator = $this->getIterator();

        $iterator->uasort(function (StepWithFixedDateStart $step1, StepWithFixedDateStart $step2) {
            if ($step1->startsBeforeFixedStep($step2)) {
                return -1;
            }

            if ($step2->startsBeforeFixedStep($step1)) {
                return 1;
            }

            return 0;
        });

        return new self(iterator_to_array($iterator));
    }

    public function hasOverlappingSteps(): bool
    {
        $sorted = $this->sort();

        /** @var StepWithFixedDateStart|null $previous */
        $previous = null;

        /** @var StepWithFixedDateStart $current */
        foreach ($sorted->getIterator() as $current) {
            if ($previous instanceof StepWithFixedDateStart && $previous->overlapsWithFixedStep($current)) {
                return true;
            }
            $previous = $current;
        }

        return false;
    }

    public function getOverlappingSteps(): self
    {
        /** @var StepWithFixedDateStart[] $overlappingSteps */
        $overlappingSteps = [];

        $sorted = $this->sort();

        /** @var StepWithFixedDateStart|null $previous */
        $previous = null;

        /** @var StepWithFixedDateStart $current */
        foreach ($sorted->getIterator() as $current) {
            if ($previous instanceof StepWithFixedDateStart && $previous->overlapsWithFixedStep($current)) {
                // using ID's as key, to avoid duplicates.
                $overlappingSteps[$previous->getUid()] = $previous;
                $overlappingSteps[$current->getUid()] = $current;
            }
            $previous = $current;
        }

        return new self($overlappingSteps);
    }

    /**
     * @throws \BadMethodCallException indirectly if more than one found
     */
    public function hasOpenStep(\DateTimeInterface $date): bool
    {
        return $this->findOpenStep($date) instanceof StepInterface;
    }

    public function getOpenStep(
        \DateTimeInterface $date,
    ): StepWithFixedDateStart {
        $step = $this->findOpenStep($date);

        if (!$step instanceof StepInterface) {
            $dateString = 'open by date ['.$date->format(StepPeriodInterface::DATE_FORMAT).']';

            throw new \BadMethodCallException('expected to find exactly one step '.$dateString.', but found none');
        }

        return $step;
    }

    /**
     * alternative for @see self::first() that returns null if no elements instead of false.
     */
    public function getFirst(): ?StepWithFixedDateStart
    {
        $step = parent::first();

        return $step instanceof StepWithFixedDateStart ? $step : null;
    }
}
