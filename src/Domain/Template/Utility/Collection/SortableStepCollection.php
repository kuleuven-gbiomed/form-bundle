<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

/**
 * @psalm-template TKey of array-key
 *
 * @template-covariant T
 *
 * @template-extends StepCollectionInterface<TKey, T>
 */
interface SortableStepCollection extends StepCollectionInterface
{
    /** @return self<TKey,T> */
    public function sort(): self;
}
