<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\AbstractReadOnlyViewNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\BasicViewNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * @see AbstractReadOnlyViewNode
 *
 * @template TKey of array-key
 * @template T of AbstractReadOnlyViewNode
 *
 * @extends NodeCollection<array-key, AbstractReadOnlyViewNode>
 */
final class ReadOnlyViewNodeCollection extends NodeCollection
{
    /** @return ReadOnlyViewNodeCollection<TKey, RadarChartViewNode> */
    public function getRadarChartNodes(): self
    {
        /** @var RadarChartViewNode[] $nodes */
        $nodes = $this->filter(fn (AbstractReadOnlyViewNode $node) => $node instanceof RadarChartViewNode)->toArray();

        return new static($nodes);
    }

    public function hasAccessibleAndRenderableFor(StepInterface $step, Role $role, array $formValues): bool
    {
        /** @var AbstractReadOnlyViewNode $readOnlyViewNode */
        foreach ($this->getIterator() as $readOnlyViewNode) {
            if ($readOnlyViewNode instanceof RadarChartViewNode && $readOnlyViewNode->isAccessibleAndRenderableFor($step, $role, $formValues)) {
                return true;
            }

            if ($readOnlyViewNode instanceof BasicViewNode) {
                return true;
            }
        }

        return false;
    }

    public function getReadableForAtLeastOneOfRoles(RoleCollection $roles): static
    {
        return $this->filter(fn (AbstractReadOnlyViewNode $node) => $node->getFlowPermission()->isReadAccessGrantedForAtLeastOneStepAndOneOfRoles($roles->getNamesArray()));
    }

    public function getReadableInStep(StepInterface $step): static
    {
        return $this->filter(fn (AbstractReadOnlyViewNode $node) => $node->getFlowPermission()->isStepReadAccessGrantedForAtLeastOneRole($step->getUid()));
    }

    public function hasReadableForRole(Role $role): bool
    {
        foreach ($this->getIterator() as $readOnlyViewNode) {
            if ($readOnlyViewNode->getFlowPermission()->isRoleReadAccessGrantedForAtLeastOneStep($role->getName())) {
                return true;
            }
        }

        return false;
    }

    public function getReadableInAtLeastOneStep(): static
    {
        return $this->filter(fn (AbstractReadOnlyViewNode $node) => $node->getFlowPermission()->isReadAccessGrantedForAtLeastOneRoleInAtLeastOneStep());
    }

    public function getUnReadableForNoneOfRoles(RoleCollection $roles): static
    {
        $roleNames = $roles->getNamesArray();

        return $this->filter(fn (AbstractReadOnlyViewNode $node) => !$node->getFlowPermission()->isReadAccessGrantedForAtLeastOneStepAndOneOfRoles($roleNames));
    }
}
