<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

/**
 * @psalm-template TKey of array-key
 *
 * @template-covariant T
 *
 * @template-extends StepCollectionInterface<TKey, T>
 */
interface OverlapAwareStepCollection extends StepCollectionInterface
{
    /**
     * Checks whether the collection has overlapping steps.
     *
     * in a published workflow, two steps may not have overlapping periods (applies for both calculated or fixed dates)
     * while in a working version of a workflow, overlap is allowed until publish.
     * this means that for each step that has a next step, the - fixed or calculated - end date of the step must be
     * EQUAL TO OR COME BEFORE the start date of the next step.
     *
     * NOTE: it is allowed to have a gap between two steps (i.e. there is time between end date last step and start date
     * next step), effectively denying everybody any access to questions during the gap.
     */
    public function hasOverlappingSteps(): bool;

    /** @return self<TKey,T> */
    public function getOverlappingSteps(): self;
}
