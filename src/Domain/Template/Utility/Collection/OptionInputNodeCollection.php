<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;

/** @extends InputNodeCollection<array-key, OptionInputNode> */
final class OptionInputNodeCollection extends InputNodeCollection
{
    /**
     * get the options having a score value so they can be used as options in a @see ChoiceInputNode for scoring purposes.
     */
    public function getHavingScoringValue(): self
    {
        return $this->filter(fn (OptionInputNode $optionInputNode) => $optionInputNode->hasScoringValue());
    }

    public function getHavingNoScoringValue(): self
    {
        return $this->filter(fn (OptionInputNode $optionInputNode) => !$optionInputNode->hasScoringValue());
    }

    public function getUsingAdditionalTextInput(): self
    {
        return $this->filter(fn (OptionInputNode $optionInputNode) => $optionInputNode->usesAdditionalTextInput());
    }

    public function getByLocalizedLabel(string $label, string $locale): self
    {
        return $this->filter(function (OptionInputNode $optionInputNode) use ($label, $locale) {
            if (!$optionInputNode->getInfo()->getLocalizedLabel()->hasForLocale($locale)) {
                return false;
            }

            return $label === $optionInputNode->getInfo()->getLocalizedLabel()->getForLocale($locale);
        });
    }

    /** @return NodeCollection<array-key, NodeInterface> */
    public function getQuestions(): NodeCollection
    {
        $uniqueQuestions = array_reduce(
            $this->toArray(),
            function (array $uniqueParents, OptionInputNode $answer) {
                $question = $answer->getParent();
                $uniqueParents[$question->getUid()] = $question;

                return $uniqueParents;
            },
            []
        );

        return new NodeCollection($uniqueQuestions);
    }

    public function hasSameOptionsAs(self $otherOptions): bool
    {
        if (!($this->count() === $otherOptions->count())) {
            return false;
        }

        foreach ($this->getIterator() as $index => $option) {
            if (!$option->isSameOptionAs($otherOptions->toArray()[$index])) {
                return false;
            }
        }

        return true;
    }

    public function mergedWith(self $collection): self
    {
        return new self(
            array_merge($this->toArray(), $collection->toArray())
        );
    }
}
