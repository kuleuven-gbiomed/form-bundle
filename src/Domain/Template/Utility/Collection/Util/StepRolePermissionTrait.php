<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection\Util;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\StepPermission;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Trait StepRolePermissionTrait.
 *
 * @see     StepPermission
 */
trait StepRolePermissionTrait
{
    /**
     * check for at least one node for which given role has read access (either direct read access or indirect access
     * via write access) in given step.
     *
     * @param Role          $role the role for which access must be granted in step
     * @param StepInterface $step the step for which the role must have read access
     *
     * @return bool
     */
    public function hasReadableForRoleInStep(Role $role, StepInterface $step)
    {
        /** @var NodeInterface $node */
        foreach ($this->getIterator() as $node) {
            if ($node->isStepRoleReadAccessGranted($step, $role)) {
                return true;
            }
        }

        return false;
    }

    /**
     * get nodes that are readable in given step for given role.
     *
     * @param Role          $role the role for which access must be granted in step
     * @param StepInterface $step the step for which the role must have read access
     *
     * @return NodeCollection<array-key, NodeInterface>
     */
    public function getReadableForRoleInStep(Role $role, StepInterface $step)
    {
        return $this->filter(fn (NodeInterface $node) => $node->isStepRoleReadAccessGranted($step, $role));
    }

    /**
     * check for at least one node for which given role has write access in given step for at least one node.
     *
     * @param Role          $role the role for which write access must be granted in step
     * @param StepInterface $step the step for which the role must have write access
     *
     * @return bool
     */
    public function hasWritableForRoleInStep(Role $role, StepInterface $step)
    {
        /** @var NodeInterface $node */
        foreach ($this->getIterator() as $node) {
            if ($node->isStepRoleWriteAccessGranted($step, $role)) {
                return true;
            }
        }

        return false;
    }

    /**
     * get nodes that are writable in given step for given role.
     *
     * @param Role          $role the role for which write access must be granted in step
     * @param StepInterface $step the step for which the role must have write access
     *
     * @return static
     */
    public function getWritableForRoleInStep(Role $role, StepInterface $step)
    {
        /** @var static $result */
        $result = $this->filter(fn (NodeInterface $node) => $node->isStepRoleWriteAccessGranted($step, $role));

        return $result;
    }

    /**
     * check if given role has at least one node to which  read access is granted and no nodes for which write access
     * is given to any other role.
     *
     * @param Role          $role the role for which access must be granted in step
     * @param StepInterface $step the step for which the role must have read access
     *
     * @return bool
     */
    public function hasOnlyReadableForRoleInStep(Role $role, StepInterface $step)
    {
        if ($this->hasWritableForRoleInStep($role, $step)) {
            return false;
        }

        return $this->hasReadableForRoleInStep($role, $step);
    }
}
