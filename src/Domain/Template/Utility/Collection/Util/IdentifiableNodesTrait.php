<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection\Util;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;

trait IdentifiableNodesTrait
{
    /**
     * Get uids of all nodes in the collection.
     *
     * @return string[]
     */
    public function getUids(): array
    {
        return array_map(fn (NodeInterface $node) => $node->getUid(), $this->toArray());
    }

    /**
     * has one node for given uid.
     */
    public function hasOneByNodeUid(string $uid): bool
    {
        if ('' === $uid) {
            throw new \InvalidArgumentException('parameter uid must be a non-empty string');
        }

        $nodes = $this->filter(fn (NodeInterface $node) => $node->getUid() === $uid);

        return 1 === $nodes->count();
    }

    public function hasNode(NodeInterface $node): bool
    {
        return $this->hasOneByNodeUid($node->getUid());
    }

    public function getOneByNodeUid(string $uid): NodeInterface
    {
        if ('' === $uid) {
            throw new \InvalidArgumentException('parameter uid must be a non-empty string');
        }

        $nodes = $this->filter(fn (NodeInterface $node) => $node->getUid() === $uid);
        $first = $nodes->first();
        if ($first instanceof NodeInterface && 1 === $nodes->count()) {
            return $first;
        }

        throw new \BadMethodCallException("Expected to find exactly one node for {$uid}, found: {$nodes->count()}.");
    }

    /**
     * filter out the nodes that have an uid that matches one of the given uids.
     *
     * @param string[] $uids
     */
    public function getByNodeUids(array $uids): static
    {
        return $this->filter(fn (NodeInterface $node) => in_array($node->getUid(), $uids, true));
    }

    /**
     * checks if none of the nodes, based on its unique identifier, occurs more than once in the collection.
     *
     * NOTE: if collection of nodes is empty, it is considered to have unique nodes since no duplicates can exist
     */
    public function hasUniqueNodes(): bool
    {
        return 0 === count($this->getDuplicateNodeUids());
    }

    /**
     * @return string[]
     */
    public function getDuplicateNodeUids(): array
    {
        $uniqueUids = [];
        $duplicateUids = [];

        /** @var NodeInterface $node */
        foreach ($this->getIterator() as $node) {
            if (in_array($node->getUid(), $uniqueUids, true)) {
                $duplicateUids[] = $node->getUid();
            }

            $uniqueUids[] = $node->getUid();
        }

        return $duplicateUids;
    }

    /**
     * Map to array indexed by node uids.
     *
     * @return NodeInterface[]
     */
    public function toArrayIndexedByUids(): array
    {
        return array_combine(
            $keys = $this->getUids(),
            $values = $this->toArray()
        );
    }
}
