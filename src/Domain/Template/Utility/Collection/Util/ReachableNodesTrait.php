<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection\Util;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;

/**
 * Trait ReachableNodesTrait.
 *
 * @see     NodeInterface::isReachableInTree()
 */
trait ReachableNodesTrait
{
    /**
     * get nodes that are neither marked as hidden nor a descendant of an ancestor that is marked as hidden.
     */
    public function getReachableInTree(): static
    {
        return $this->filter(fn (NodeInterface $node) => $node->isReachableInTree());
    }

    /**
     * get nodes that are marked as hidden or a descendant of an ancestor that is marked as hidden.
     */
    public function getUnReachableInTree(): static
    {
        return $this->filter(fn (NodeInterface $node) => !$node->isReachableInTree());
    }

    /**
     * get nodes that are not marked as hidden, ignoring any ancestry that might or might not be marked as hidden.
     */
    public function getNotMarkedAsHidden(): static
    {
        return $this->filter(fn (NodeInterface $node) => !$node->isHidden());
    }
}
