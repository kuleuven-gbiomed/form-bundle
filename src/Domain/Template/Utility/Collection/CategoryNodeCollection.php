<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;

/**
 * @see CategoryNode
 *
 * @template-extends NodeCollection<array-key,CategoryNode>
 */
final class CategoryNodeCollection extends NodeCollection
{
    public function getMergedWith(self $collection): self
    {
        return new self(array_merge($this->toArray(), $collection->toArray()));
    }
}
