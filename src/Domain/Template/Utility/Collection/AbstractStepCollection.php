<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\StepPeriodInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;

/**
 * Class AbstractStepCollection.
 *
 * holds a collection of steps that effectively can be used to make up a @see WorkFlow
 *
 * when admins are making a workflow with steps, not all steps have to be 'valid' in regards to each other. this mainly
 * means that in a working version of the workflow, steps can overlap each other in terms of @see StepPeriodInterface
 * in which steps can be open because only one step (or none) can be open at the time. when a workflow is to be
 * published as part of a published current version of a template, then no overlap will be asserted.
 * UX wise, in the admin side, admins working on a workflow will/should be notified early whenever they create overlap.
 *
 * DEVELOPER NOTICE:
 * a collection of steps in a workflow - for both working and published versions - should consist of steps of the same
 * type (i.e. steps implementing same 'dates' contract, either @see StepWithCalculatedDateStart (dynamic dates)
 * or @see StepWithFixedDateStart (hardcoded in step dates)). as it is very difficult to impossible to avoid
 * overlap between two or more steps if a mix of those two types are used, while knowing that only one step (or none)
 * can be 'open' at the time in a (valid published) workflow. combining steps that start based on calculated start
 * dates derived from target entities with steps that start based on hardcoded fixed dates can never decisively be
 * asserted to have no overlap (it is difficult but possible to check overlap with mixed steps upon publish with the
 * then known dates for ALL target entities e.g. SCONE placement-internshipPeriod start dates) but not once the template
 * workflow is published. after publish, the target entity's date used to create dynamic start date is changed
 * towards future or past. this could cause issues with overlap and ordering based on date.
 * however a mix of @see StepWithFixedDateStart and @see StepWithCalculatedDateStart needs to be allowed on request of
 * the owners/clients of this code.
 * for this purpose the @see MixedCalculatedAndFixedStepCollection is created.
 *
 * @see     AbstractStep
 * @see     WorkFlow
 *
 * @template TKey of array-key
 * @template T of StepInterface
 *
 * @extends ArrayCollection<TKey, T>
 *
 * @implements StepCollectionInterface<TKey, T>
 */
abstract class AbstractStepCollection extends ArrayCollection implements StepCollectionInterface, \JsonSerializable, Deserializable
{
    /**
     * sort steps by comparing each against the other to create a logical flow, based on the fixed or calculated start
     * dates of each step and/or the order they appear in the collection.
     *
     * NOTE: using @see StepInterface::startsBeforeStep() helps to sort steps based on starting dates, but this only
     * works if steps are of same type (i.e. use same @see StepPeriodInterface type). hence if two steps are not of
     * the same type (@see MixedCalculatedAndFixedStepCollection ) then using @see StepInterface::startsBeforeStep()
     * will throw exception.
     */
    abstract public function getSortedByAscendingFlow(): self;

    /**
     * AbstractStepCollection Constructor.
     *
     * IMPORTANT: no two steps can have the same @see StepInterface::getUid() in a workflow. a lot of methods in this
     * collection class - especially ones checking for overlap, following and preceding steps - require that no
     * duplicate steps (i.e. no duplicate step identifying uids) exist. this is asserted in the constructor for
     * convenience to avoid having to check/assert/handle false negatives/positives in every method
     * duplicates should never occur anyway when admin side works properly, even in a working version.
     * developers are to use provided methods for adding/removing/updating steps in this collection, which always
     * return new instances that pass this constructor check, asserting no duplicates are ever added/updated
     */
    final public function __construct(array $elements = [])
    {
        static::guardStepClass(...$elements);

        parent::__construct($elements);

        // check for duplicate uids to assert that every uid of any step that is added is unique
        if (!$this->hasUniqueSteps()) {
            throw new \InvalidArgumentException('the steps must be unique (must have unique uids), but found duplicate steps for uids ['.implode(' | ', $this->getDuplicateStepUids()).'].');
        }
    }

    protected static function guardStepClass(StepInterface ...$elements): void
    {
    }

    /**
     * checks if none of the steps, based on its unique identifier, occurs more than once in the collection.
     *
     * NOTE: if collection of nodes is empty, it is considered to have unique nodes since no duplicates can exist
     */
    public function hasUniqueSteps(): bool
    {
        return 0 === count($this->getDuplicateStepUids());
    }

    public function getDuplicateStepUids(): array
    {
        $uniqueUids = [];
        $duplicateUids = [];

        /** @var StepInterface $step */
        foreach ($this->getIterator() as $step) {
            if (in_array($step->getUid(), $uniqueUids, true)) {
                $duplicateUids[] = $step->getUid();
            }

            $uniqueUids[] = $step->getUid();
        }

        return $duplicateUids;
    }

    public function hasSteps(): bool
    {
        return [] !== $this->toArray();
    }

    public function findOneStepByUid(string $uid): ?StepInterface
    {
        $result = $this->filter(fn (StepInterface $collectionStep) => $collectionStep->getUid() === $uid);

        // constructor has asserted no duplicate steps but no harm to double check in this base method
        // (e.g. in case developer adds steps to collection without using provided methods)
        if ($result->count() > 1) {
            throw new \BadMethodCallException("expected to find one or no step for uid [{$uid}], found {$result->count()} elements!");
        }

        $step = $result->first();

        return $step instanceof StepInterface ? $step : null;
    }

    public function hasOneStepByUid(string $uid): bool
    {
        return $this->findOneStepByUid($uid) instanceof StepInterface;
    }

    public function getOneStepByUid(string $uid): StepInterface
    {
        $result = $this->findOneStepByUid($uid);

        if ($result instanceof StepInterface) {
            return $result;
        }

        throw new \BadMethodCallException("expected to find exactly one step for uid [{$uid}], found more than one or none at all!");
    }

    public function withNewStepAddedAtBeginning(StepInterface $step): static
    {
        if ($this->hasOneStepByUid($step->getUid())) {
            throw new \BadMethodCallException("cannot add step [{$step->getUid()}]. it's already in the collection");
        }

        /** @var array<TKey, T> $steps */
        $steps = array_merge([$step], $this->toArray());

        return new static($steps);
    }

    public function withNewStepAddedAtEnd(StepInterface $step): static
    {
        if ($this->hasOneStepByUid($step->getUid())) {
            throw new \BadMethodCallException("cannot add step [{$step->getUid()}]. it's already in the collection");
        }

        /** @var array<TKey, T> $steps */
        $steps = array_merge($this->toArray(), [$step]);

        return new static($steps);
    }

    public function withNewStepAddedAfterExistingStep(StepInterface $step, StepInterface $afterExistingStep): static
    {
        if ($this->hasOneStepByUid($step->getUid())) {
            throw new \BadMethodCallException("cannot add step [{$step->getUid()}]. it's already in the collection");
        }

        if (!$this->hasOneStepByUid($afterExistingStep->getUid())) {
            throw new \BadMethodCallException("cannot add after existing step [{$afterExistingStep->getUid()}]. it's not known to the collection");
        }

        $newSet = [];
        foreach ($this as $collectionStep) {
            $newSet[] = $collectionStep;

            if ($collectionStep->getUid() === $afterExistingStep->getUid()) {
                /* @var T $step */
                $newSet[] = $step;
            }
        }

        /** @var array<TKey, T> $newSet */
        return new static($newSet);
    }

    public function withRemovedStep(StepInterface $step): static
    {
        if (!$this->hasOneStepByUid($step->getUid())) {
            throw new \BadMethodCallException("cannot remove step [{$step->getUid()}]. it's not known to the collection");
        }

        $newSet = [];
        /** @var T $collectionStep */
        foreach ($this as $collectionStep) {
            if ($collectionStep->getUid() === $step->getUid()) {
                continue;
            }

            $newSet[] = $collectionStep;
        }

        return new static($newSet);
    }

    /**
     * returns an empty new instance of the implementing child StepCollection class.
     * useful to avoid having to loop over child classes to find right class to instantiate empty.
     */
    public function withAllStepsRemoved(): static
    {
        return new static();
    }

    public function withOnlyStep(StepInterface $step): static
    {
        /** @var T $result */
        $result = $step;

        return new static([$result]);
    }

    public function withUpdatedStep(StepInterface $step): static
    {
        if (!$this->hasOneStepByUid($step->getUid())) {
            throw new \BadMethodCallException("cannot update step [{$step->getUid()}]. it's not known to the collection");
        }

        $newSet = [];
        foreach ($this as $collectionStep) {
            if ($collectionStep->getUid() === $step->getUid()) {
                $newSet[] = $step;
                continue;
            }

            $newSet[] = $collectionStep;
        }

        /** @var array<TKey, T> $result */
        $result = $newSet;

        return new static($result);
    }

    public function hasOneStepThatDirectlyPrecedesStep(StepInterface $step): bool
    {
        if (!$this->hasOneStepByUid($step->getUid())) {
            throw new \InvalidArgumentException("step [{$step->getUid()}] is unknown to collection");
        }

        $previousStep = null;
        /** @var StepInterface $sortedStep */
        foreach ($this->getSortedByAscendingFlow() as $sortedStep) {
            if ($step->getUid() === $sortedStep->getUid() && $previousStep instanceof StepInterface) {
                return true;
            }

            $previousStep = $sortedStep;
        }

        return false;
    }

    public function getOneStepThatDirectlyPrecedesStep(StepInterface $step): StepInterface
    {
        if (!$this->hasOneStepByUid($step->getUid())) {
            throw new \InvalidArgumentException("step [{$step->getUid()}] is unknown to collection");
        }

        $previousStep = null;

        foreach ($this->getSortedByAscendingFlow() as $sortedStep) {
            if ($step->getUid() === $sortedStep->getUid()) {
                if (null === $previousStep) {
                    break;
                }

                return $previousStep;
            }

            // track current as previous step
            $previousStep = $sortedStep;
        }

        throw new \BadMethodCallException("step [{$step->getUid()}] has no preceding step");
    }

    /**
     * @throws \InvalidArgumentException if given step in unknown in collection
     * @throws \BadMethodCallException   if no preceding step found
     */
    public function getSortedStepsThatPrecedeStep(StepInterface $step): static
    {
        if (!$this->hasOneStepByUid($step->getUid())) {
            throw new \InvalidArgumentException("step [{$step->getUid()}] is unknown to collection");
        }

        $previousSteps = [];
        /** @var T $sortedStep */
        foreach ($this->getSortedByAscendingFlow() as $sortedStep) {
            if ($step->getUid() === $sortedStep->getUid()) {
                break;
            }

            $previousSteps[] = $sortedStep;
        }

        return new static($previousSteps);
    }

    public function hasOneStepThatDirectlyFollowsStep(StepInterface $step): bool
    {
        if (!$this->hasOneStepByUid($step->getUid())) {
            throw new \InvalidArgumentException("step [{$step->getUid()}] is unknown to collection");
        }

        /** @var bool $hit */
        $hit = false;
        /** @var StepInterface $sortedStep */
        foreach ($this->getSortedByAscendingFlow() as $sortedStep) {
            // if given step was hit and loop continued, it means there's a next
            if ($hit) {
                return true;
            }

            // track if given step was hit
            if ($step->getUid() === $sortedStep->getUid()) {
                $hit = true;
            }
        }

        return false;
    }

    public function getOneStepThatDirectlyFollowsStep(StepInterface $step): StepInterface
    {
        if (!$this->hasOneStepByUid($step->getUid())) {
            throw new \InvalidArgumentException("step [{$step->getUid()}] is unknown to collection");
        }

        $hit = false;
        foreach ($this->getSortedByAscendingFlow() as $sortedStep) {
            // if given step was hit and loop continued, it means there's a next
            if ($hit) {
                return $sortedStep;
            }

            // track if given step was hit
            if ($step->getUid() === $sortedStep->getUid()) {
                $hit = true;
            }
        }

        throw new \BadMethodCallException("step [{$step->getUid()}] has no following step");
    }

    /**
     * @throws \InvalidArgumentException if given step in unknown in collection
     */
    public function getSortedStepsThatFollowStep(StepInterface $step): static
    {
        if (!$this->hasOneStepByUid($step->getUid())) {
            throw new \InvalidArgumentException("step [{$step->getUid()}] is unknown to collection");
        }

        $followingSteps = [];
        $hit = false;
        /** @var T $sortedStep */
        foreach ($this->getSortedByAscendingFlow() as $sortedStep) {
            if ($hit) {
                $followingSteps[] = $sortedStep;
            }

            if ($step->getUid() === $sortedStep->getUid()) {
                $hit = true;
            }
        }

        return new static($followingSteps);
    }

    public function hasStartingStep(): bool
    {
        return $this->getSortedByAscendingFlow()->first() instanceof StepInterface;
    }

    public function getStartingStep(): StepInterface
    {
        $step = $this->getSortedByAscendingFlow()->first();

        if ($step instanceof StepInterface) {
            return $step;
        }

        throw new \BadMethodCallException('no starting step found!');
    }

    public function doesFlowStartWithStep(StepInterface $step): bool
    {
        if (!$this->hasOneStepByUid($step->getUid())) {
            throw new \InvalidArgumentException("step [{$step->getUid()}] is unknown to collection");
        }

        return $this->getStartingStep()->getUid() === $step->getUid();
    }

    public function hasFinishingStep(): bool
    {
        return $this->getSortedByAscendingFlow()->last() instanceof StepInterface;
    }

    public function getFinishingStep(): StepInterface
    {
        $step = $this->getSortedByAscendingFlow()->last();

        if ($step instanceof StepInterface) {
            return $step;
        }

        throw new \BadMethodCallException('no finishing step found!');
    }

    /**
     * @throws \InvalidArgumentException if given step unknown in collection
     */
    public function doesFlowFinishWithStep(StepInterface $step): bool
    {
        if (!$this->hasOneStepByUid($step->getUid())) {
            throw new \InvalidArgumentException("step [{$step->getUid()}] is unknown to collection");
        }

        return $this->getFinishingStep()->getUid() === $step->getUid();
    }

    public function getPositionNumber(StepInterface $step): int
    {
        if (!$this->hasOneStepByUid($step->getUid())) {
            throw new \InvalidArgumentException("step [{$step->getUid()}] is unknown to collection");
        }

        $count = 1;
        /** @var StepInterface $sortedStep */
        foreach ($this->getSortedByAscendingFlow() as $sortedStep) {
            if ($step->getUid() === $sortedStep->getUid()) {
                break;
            }

            ++$count;
        }

        return $count;
    }

    public function getStepLabelPrefixedWithPositionNumber(
        StepInterface $step,
        string $locale,
        string $glue = ' - ',
    ): string {
        return (string) $this->getPositionNumber($step).$glue.$step->getLabel($locale);
    }

    public function getStepUids(): array
    {
        $uids = [];

        /** @var StepInterface $step */
        foreach ($this->getIterator() as $step) {
            $uids[] = $step->getUid();
        }

        return $uids;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
