<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * @see     RoleNotificationConfig
 *
 * @extends ArrayCollection<array-key, RoleNotificationConfig>
 */
final class RoleNotificationConfigCollection extends ArrayCollection implements \JsonSerializable, Deserializable
{
    public static function createEmpty(): self
    {
        return new self();
    }

    /**
     * @throws StepNotificationException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $elements = [];

        foreach ($data as $roleNotificationConfigData) {
            $elements[] = RoleNotificationConfig::fromJsonDecodedArray($roleNotificationConfigData);
        }

        return new self($elements);
    }

    public function hasOneForRole(Role $role): bool
    {
        $result = $this->filter(fn (RoleNotificationConfig $roleNotificationConfig) => $roleNotificationConfig->getRole()->equals($role));

        return 1 === $result->count();
    }

    public function getOneForRole(Role $role): RoleNotificationConfig
    {
        $result = $this->filter(fn (RoleNotificationConfig $roleNotificationConfig) => $roleNotificationConfig->getRole()->equals($role));

        $first = $result->first();
        if ($first instanceof RoleNotificationConfig && 1 === $result->count()) {
            return $first;
        }

        throw new \BadMethodCallException('expected one result. got '.((string) $result->count()));
    }

    public function getRoles(): RoleCollection
    {
        $roles = [];

        /** @var RoleNotificationConfig $roleNotificationConfig */
        foreach ($this->getIterator() as $roleNotificationConfig) {
            $roles[] = $roleNotificationConfig->getRole();
        }

        return new RoleCollection($roles);
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
