<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use IteratorAggregate;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;

/**
 * @template TKey of array-key
 *
 * @template-covariant T
 *
 * @extends IteratorAggregate<TKey, T>
 */
interface StepCollectionInterface extends \IteratorAggregate
{
    public static function getType(): string;

    public function hasSteps(): bool;

    /**
     * @return bool cannot set type explicit because of ArrayCollection usage in implementations
     */
    public function isEmpty();

    public function hasUniqueSteps(): bool;

    /**
     * @return int cannot set type explicit because of ArrayCollection usage in implementations
     */
    public function count();

    /**
     * @return string[]
     */
    public function getDuplicateStepUids(): array;

    /**
     * get either one or no for given uid (in a valid workflow, every step is unique (by unique uid)).
     *
     * @throws \BadMethodCallException if more than one found for same uid
     */
    public function findOneStepByUid(string $uid): ?StepInterface;

    public function hasOneStepByUid(string $uid): bool;

    /**
     * @throws \BadMethodCallException if none found
     */
    public function getOneStepByUid(string $uid): StepInterface;

    /**
     * check if given step is in collection and has a step that comes DIRECTLY before it.
     *
     * note: constructor has asserted no duplicate steps (duplicate step uids) exist, so no false negative possible
     *
     * @throws \InvalidArgumentException if given step in unknown in collection
     */
    public function hasOneStepThatDirectlyPrecedesStep(StepInterface $step): bool;

    /** @return self<TKey,T> */
    public static function createEmpty(): self;

    /**
     * returns new collection with given step replacing the existing step with same uid identifier, with asserting that
     * new collection will contain only steps of correct type (the latter is indirectly asserted via constructor).
     */
    public function withUpdatedStep(StepInterface $step): static;

    /**
     * returns new collection with given step added as first element to the collection, with asserting that no duplicate
     * is added and that new collection will contain only steps of correct type (the latter is indirectly asserted via constructor).
     */
    public function withNewStepAddedAtBeginning(StepInterface $step): static;

    /**
     * returns new collection with given step added as last element to the collection, with asserting that no duplicate is added and that new
     * collection  will contain only steps of correct type (the latter is indirectly asserted via constructor).
     */
    public function withNewStepAddedAtEnd(StepInterface $step): static;

    /**
     * returns new collection with given step added to the collection right after the existing step, with asserting that
     * no duplicate is added and that new collection will contain only steps of correct type (the latter is indirectly
     * asserted via constructor).
     *
     * NOTE: adding a new step on a certain position in the collection is not so relevant for the ordering if the
     * collection can be ordered based on the start dates of each step via @see getSortedByAscendingFlow()
     * but if the collection mixes steps of different type, then ordering is limited to the subsets of steps of same type.
     * the ordering between two steps of different type is then based on their position in the collection. hence this method
     */
    public function withNewStepAddedAfterExistingStep(StepInterface $step, StepInterface $afterExistingStep): static;

    /**
     * returns new collection with given step removed, with asserting that given step exists in collection.
     */
    public function withRemovedStep(StepInterface $step): static;

    public function withAllStepsRemoved(): static;

    public function withOnlyStep(StepInterface $step): static;

    public function getSortedStepsThatFollowStep(StepInterface $step): static;

    /**
     * get the step that is in collection and comes DIRECTLY before (i.e. the previous step) the given step.
     *
     * note: constructor has asserted no duplicate steps (duplicate step uids) exist, so no false negative possible
     *
     * @throws \InvalidArgumentException if given step in unknown in collection
     * @throws \BadMethodCallException   if no preceding step found
     */
    public function getOneStepThatDirectlyPrecedesStep(StepInterface $step): StepInterface;

    /**
     * check if given step is in collection and is DIRECTLY followed by another step in the flow.
     *
     * note: constructor has asserted no duplicate steps (duplicate step uids) exist, so no false negative possible
     *
     * @throws \InvalidArgumentException if given step in unknown in collection
     */
    public function hasOneStepThatDirectlyFollowsStep(StepInterface $step): bool;

    public function getSortedStepsThatPrecedeStep(StepInterface $step): static;

    /**
     * get the one step that comes DIRECTLY after the given step that is in the collection.
     *
     * note: constructor has asserted no duplicate steps (duplicate step uids) exist, so no false negative possible
     *
     * @throws \InvalidArgumentException if given step in unknown in collection
     * @throws \BadMethodCallException   if no following  step found
     */
    public function getOneStepThatDirectlyFollowsStep(StepInterface $step): StepInterface;

    /**
     * in a valid workflow, there should be a step without a preceding step.
     */
    public function hasStartingStep(): bool;

    /**
     * get the first step = the step that starts the workflow (has no other steps preceding it).
     *
     * there should always be a first step in a valid workflow unless a new form is in creation process and has
     * empty workflow (developers choice!)). use @see hasSteps to check for steps first
     *
     * @throws \BadMethodCallException if none found
     */
    public function getStartingStep(): StepInterface;

    /**
     * @throws \InvalidArgumentException if given step unknown in collection
     */
    public function doesFlowStartWithStep(StepInterface $step): bool;

    /**
     * in a valid workflow, there should be a step without a following step.
     */
    public function hasFinishingStep(): bool;

    /**
     * get the last step = the step that finishes the workflow (has no other steps following it).
     *
     * A valid (non-empty) workflow always has last step. throws \BadMethodCallException if collection is empty
     * use @see hasSteps to check for steps first
     *
     * @throws \BadMethodCallException if none found
     */
    public function getFinishingStep(): StepInterface;

    /**
     * @throws \InvalidArgumentException if given step unknown in collection
     */
    public function doesFlowFinishWithStep(StepInterface $step): bool;

    /**
     * get numbered position of given step in the workflow.
     *
     * IMPORTANT: for UX consistency in views, the numbering starts at 1!
     *
     * @return int numbered position of step in flow, starting from 1 (starting step) to ...
     *
     * @throws \InvalidArgumentException if given step unknown in collection
     */
    public function getPositionNumber(StepInterface $step): int;

    public function getStepLabelPrefixedWithPositionNumber(StepInterface $step, string $locale, string $glue = ' - '): string;

    /**
     * sorts the steps (if any) by their logical order in the flow, based on the step start dates and/or the order they
     * appear in the collection, depending if the collection contains only steps of the same type or a mix of types.
     *
     * WARNING: in a valid workflow, there can be no overlap between steps (no two steps can be 'open' at the same time)
     * which means that in a valid published @see Workflow no two steps should have the same (fixed or calculated)
     * startDate. but this method purposely doesn't assert this since this method is also used on working versions of
     * workflows, where overlap is allowed until the working version workflow is validated/published in to a valid
     * published workflow version). this means it is perfectly legitimate (although preferably to be avoided) in a
     * working version to have two steps with same startDate until that working version is published. the order of
     * appearing in the elements will than determine which step that will come before the other, which also indirectly
     * impacts starting & finishing step.
     *
     * WARNING: when a mix of steps with fixed start dates and steps with calculated start dates is used, the ordering
     * based on start dates can only be done between the sub sets of steps of the same type. the
     * ordering between two steps of different type is not possible or not decisively enough. Hence, in a collection
     * that mixes steps of different type, the ordering between two steps of different type is then based on their position
     * in the collection. hence this method e.g. @see MixedCalculatedAndFixedStepCollection
     *
     * @return static
     */
    public function getSortedByAscendingFlow(): self;

    public function getStepUids(): array;

    /**
     * @return StepInterface[] cannot set type explicit to array because of ArrayCollection usage in implementations
     */
    public function toArray();
}
