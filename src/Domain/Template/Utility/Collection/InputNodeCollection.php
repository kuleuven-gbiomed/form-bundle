<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\AbstractParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * @see InputNode
 *
 * @template TKey of array-key
 * @template T of InputNode
 *
 * @extends NodeCollection<TKey, T>
 */
class InputNodeCollection extends NodeCollection
{
    /**
     * check for nodes for which given role has direct or indirect (via write) read access for at least one step.
     *
     * @throws \Exception
     */
    public function hasReadableForRole(Role $role): bool
    {
        /** @var InputNode $inputNode */
        foreach ($this->toArray() as $inputNode) {
            if ($inputNode->getFlowPermission()->isRoleReadAccessGrantedForAtLeastOneStep($role->getName())) {
                return true;
            }
        }

        return false;
    }

    /**
     * get nodes for which given role has direct or indirect (via write) read access for at least one step.
     *
     * @return InputNodeCollection<TKey,T>
     */
    public function getReadableForRole(Role $role): self
    {
        return $this->filter(fn (InputNode $node) => $node->getFlowPermission()->isRoleReadAccessGrantedForAtLeastOneStep($role->getName()));
    }

    /**
     * check for nodes for which at least one role in given roles has direct or indirect (via write) read access
     * for at least one step.
     *
     * @throws \Exception
     */
    public function hasReadableForAtLeastOneOfRoles(RoleCollection $roles): bool
    {
        $roleNames = $roles->getNamesArray();

        /** @var InputNode $inputNode */
        foreach ($this->toArray() as $inputNode) {
            if ($inputNode->getFlowPermission()->isReadAccessGrantedForAtLeastOneStepAndOneOfRoles($roleNames)) {
                return true;
            }
        }

        return false;
    }

    /**
     * get nodes for which at least one role in given roles has direct or indirect (via write) read access
     * for at least one step.
     */
    public function getReadableForAtLeastOneOfRoles(RoleCollection $roles): static
    {
        $roleNames = $roles->getNamesArray();

        return $this->filter(fn (InputNode $node) => $node->getFlowPermission()->isReadAccessGrantedForAtLeastOneStepAndOneOfRoles($roleNames));
    }

    /**
     * get nodes for which none of given roles has direct or indirect (via write) read access
     * for at least one step.
     */
    public function getUnReadableForNoneOfRoles(RoleCollection $roles): static
    {
        $roleNames = $roles->getNamesArray();

        return $this->filter(fn (InputNode $node) => !$node->getFlowPermission()->isReadAccessGrantedForAtLeastOneStepAndOneOfRoles($roleNames));
    }

    /**
     * get nodes for which at least one role in given roles has write access for at least one step.
     */
    public function getWritableForAtLeastOneOfRoles(RoleCollection $roles): static
    {
        $roleNames = $roles->getNamesArray();

        return $this->filter(fn (InputNode $node) => $node->getFlowPermission()->isWriteAccessGrantedForAtLeastOneStepAndOneOfRoles($roleNames));
    }

    /**
     * get nodes for which none of given roles has write access for at least one step.
     */
    public function getUnWritableForNoneOfRoles(RoleCollection $roles): static
    {
        $roleNames = $roles->getNamesArray();

        return $this->filter(fn (InputNode $node) => !$node->getFlowPermission()->isWriteAccessGrantedForAtLeastOneStepAndOneOfRoles($roleNames));
    }

    /**
     * get nodes that are (directly or indirectly) readable in given step for at least one role.
     *
     * @param StepInterface $step the step for which at least one role has write access
     */
    public function getReadableInStep(StepInterface $step): static
    {
        return $this->filter(fn (InputNode $node) => $node->getFlowPermission()->isStepReadAccessGrantedForAtLeastOneRole($step->getUid()));
    }

    public function getUnReadableInStepForRole(StepInterface $step, Role $role): static
    {
        return $this->filter(fn (InputNode $node) => !$node->getFlowPermission()->isStepRoleReadAccessGranted($step->getUid(), $role->getName()));
    }

    /**
     * get nodes for which at least one role has direct or indirect (via write) read access in at least one step.
     */
    public function getReadableInAtLeastOneStep(): static
    {
        return $this->filter(fn (InputNode $node) => $node->getFlowPermission()->isReadAccessGrantedForAtLeastOneRoleInAtLeastOneStep());
    }

    /**
     * get nodes that are writable in given step for at least one role.
     *
     * @param StepInterface $step the step for which at least one role has write access
     */
    public function getWritableInStep(StepInterface $step): static
    {
        return $this->filter(fn (InputNode $node) => $node->getFlowPermission()->isStepWriteAccessGrantedForAtLeastOneRole($step->getUid()));
    }

    /**
     * get only  the nodes that are not of the upload type
     * this is used when copying answers to exclude copying 'answers' for 'upload file questions'.
     */
    public function getWithoutUploadNodes(): static
    {
        return $this->filter(fn (InputNode $node) => !$node->isUploadNode());
    }

    /**
     * get an array with the nested label of each element in the collection.
     *
     * NOTE: nested label: string label of element, prefixed & dashed-glued with ancestral labels
     *
     * @return string[]
     *
     * @throws LocalizedStringException if locale is not a non-empty string
     */
    public function getNestedLabelsArray(string $locale): array
    {
        $labels = [];

        /** @var InputNode $inputNode */
        foreach ($this->toArray() as $inputNode) {
            if (!$inputNode->hasNestedLabel($locale)) {
                continue;
            }

            $labels[] = $inputNode->getNestedLabel($locale);
        }

        return $labels;
    }

    /**
     * get an array with the label of each element in the collection.
     *
     * @return string[]
     *
     * @throws LocalizedStringException if locale is not a non-empty string
     */
    public function getLabelsArray(string $locale): array
    {
        $labels = [];

        /** @var InputNode $inputNode */
        foreach ($this->toArray() as $inputNode) {
            if (!$inputNode->hasLabel($locale)) {
                continue;
            }

            $labels[] = $inputNode->getLabel($locale);
        }

        return $labels;
    }

    /**
     * get unique collection of all ancestral categoryNodes attached to the items in this collection.
     */
    public function getAncestralCategoryNodes(): CategoryNodeCollection
    {
        $categoryNodes = new CategoryNodeCollection();

        foreach ($this->getIterator() as $inputNode) {
            $categoryNodes = $categoryNodes->getMergedWith($inputNode->getAncestors()->getCategoryNodes());
        }

        return $categoryNodes->getUnique();
    }

    /**
     * get collection of option nodes.
     */
    public function getOptionInputNodes(): OptionInputNodeCollection
    {
        $optionNodes = [];

        /** @var InputNode $inputNode */
        foreach ($this->toArray() as $inputNode) {
            if (!$inputNode instanceof OptionInputNode) {
                continue;
            }

            $optionNodes[$inputNode->getUid()] = $inputNode;
        }

        return new OptionInputNodeCollection($optionNodes);
    }

    /**
     * get the nodes that meet score requirements and that are actually used as global scoring dependency by at
     * least one global score node.
     */
    public function getOperatingAsScoreNodes(): static
    {
        return $this->filter(fn (InputNode $node) => $node instanceof OperableForScoring && $node->operatesAsScoreNode());
    }

    /**
     * get the nodes that meet score requirements and that are actually used as global scoring dependency for
     * the given global score node.
     */
    public function getOperatingAsScoreNodesForGlobalScoreNode(OperableForScoring $globalScoreNode): static
    {
        return $this->filter(fn (InputNode $node) => $node instanceof OperableForScoring && $node->operatesAsScoreNodeForGlobalScoreNode($globalScoreNode));
    }

    /**
     * get the nodes that meet global score requirements and that are actually used as global score node by having
     * at least one valid scoring node dependency.
     */
    public function getOperatingAsGlobalScoreNodes(): static
    {
        return $this->filter(fn (InputNode $node) => $node instanceof OperableForScoring && $node->operatesAsGlobalScoreNode());
    }

    /**
     * revokes any permission that given role has in any step in the flow permission for all questions
     * in this collection and returns a new InputNodeCollection with all permissions removed for given role.
     */
    public function withFlowPermissionsRevokedForRoleInAllSteps(Role $role): static
    {
        $nodes = $this->getIterator();

        /** @var InputNode $inputNode */
        foreach ($nodes as $inputNode) {
            $inputNode->revokeFlowPermissionForRoleInAllSteps($role);
        }

        return new static(iterator_to_array($nodes));
    }

    public function getScorableInputNodes(): static
    {
        return $this->filter(fn (InputNode $node) => $node instanceof ScorableInputNode);
    }

    /**
     * overwritten for typeHinting purposes.
     *
     * @return InputNode[]
     */
    public function toArrayIndexedByUids(): array
    {
        /** @var InputNode[] $result */
        $result = parent::toArrayIndexedByUids();

        return $result;
    }

    /** @return InputNodeCollection<array-key,TextInputNode|ChoiceInputNode> */
    public function getPrefillableInputNodesHavingPrefillingQuestions(): self
    {
        $nodes = $this->filter(
            fn (InputNode $node) => ($node instanceof ChoiceInputNode || $node instanceof TextInputNode) && $node->hasPrefillingQuestions()
        )->toArray();

        /** @var InputNodeCollection<array-key,TextInputNode|ChoiceInputNode> $nodeCollection */
        $nodeCollection = new self($nodes);

        return $nodeCollection;
    }

    /** @return InputNodeCollection<TKey, ChoiceInputNode> */
    public function getPrefillableChoiceInputNodesHavingPrefillingQuestions(): self
    {
        /** @var ChoiceInputNode[] $nodes */
        $nodes = $this->filter(
            fn (InputNode $node) => $node instanceof ChoiceInputNode && $node->hasPrefillingQuestions()
        )->toArray();

        return new static($nodes);
    }

    /** @return InputNodeCollection<TKey, ChoiceInputNode> */
    public function getChoiceInputNodes(): self
    {
        /** @var ChoiceInputNode[] $nodes */
        $nodes = $this->filter(fn (InputNode $node) => $node instanceof ChoiceInputNode)->toArray();

        return new static($nodes);
    }

    /** @return InputNodeCollection<TKey, TextInputNode> */
    public function getTextInputNodes(): self
    {
        /** @var TextInputNode[] $nodes */
        $nodes = $this->filter(fn (InputNode $node) => $node instanceof TextInputNode)->toArray();

        return new static($nodes);
    }

    /** @return InputNodeCollection<TKey, UploadInputNode> */
    public function getUploadInputNodes(): self
    {
        /** @var UploadInputNode[] $uploadNodes */
        $uploadNodes = $this->filter(fn (InputNode $node) => $node instanceof UploadInputNode)->toArray();

        return new static($uploadNodes);
    }

    public function getLocked(): static
    {
        return $this->filter(fn (InputNode $node) => $node instanceof ParentalInputNode && $node->isLocked());
    }

    public function getNonLocked(): static
    {
        return $this->filter(fn (InputNode $node) => $node instanceof ParentalInputNode && !$node->isLocked());
    }

    /** @return InputNodeCollection<TKey, AbstractParentalInputNode> */
    public function getMultiLocked(): self
    {
        /** @var AbstractParentalInputNode[] $multiLocked */
        $multiLocked = $this->filter(fn (InputNode $node) => $node instanceof AbstractParentalInputNode && $node->isMultiLockable())->toArray();

        return new static($multiLocked);
    }

    /**
     * @param self<TKey, T> $collection
     **/
    public function getMergedWith(self $collection): static
    {
        return new static(
            array_merge($this->toArray(), $collection->toArray())
        );
    }

    public function getOptional(): static
    {
        return $this->filter(fn (InputNode $node) => !$node->isRequired());
    }
}
