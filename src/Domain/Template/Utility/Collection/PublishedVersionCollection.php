<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Version\PublishedVersion;

/**
 * @see     PublishedVersion
 *
 * @extends ArrayCollection<array-key,PublishedVersion>
 */
final class PublishedVersionCollection extends ArrayCollection implements \JsonSerializable, Deserializable
{
    /**
     * @throws LocalizedStringException
     * @throws InputInvalidArgumentException
     * @throws StepException
     * @throws StepNotificationException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $versions = [];

        foreach ($data as $versionData) {
            $versions[] = PublishedVersion::fromJsonDecodedArray($versionData);
        }

        return new self($versions);
    }

    public function hasOneForVersionNumber(int $versionNumber): bool
    {
        $versions = $this->filter(fn (PublishedVersion $publishedVersion) => $publishedVersion->getVersionNumber() === $versionNumber);

        return 1 === $versions->count();
    }

    public function getOneForVersionNumber(int $versionNumber): PublishedVersion
    {
        $versions = $this->filter(fn (PublishedVersion $publishedVersion) => $publishedVersion->getVersionNumber() === $versionNumber);

        $first = $versions->first();
        if ($first instanceof PublishedVersion && 1 === $versions->count()) {
            return $first;
        }

        throw new \BadMethodCallException('expected 1 published version. found '.((string) $versions->count()));
    }

    public function withVersionAddedAsFirstElement(PublishedVersion $publishedVersion): self
    {
        return new self([$publishedVersion] + $this->toArray());
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
