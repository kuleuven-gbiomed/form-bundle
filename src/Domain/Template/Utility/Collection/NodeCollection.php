<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\AbstractReadOnlyViewNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\Util\IdentifiableNodesTrait;
use KUL\FormBundle\Domain\Template\Utility\Collection\Util\ReachableNodesTrait;
use KUL\FormBundle\Domain\Template\Utility\Collection\Util\StepRolePermissionTrait;

/**
 * @template TKey of array-key
 * @template T of NodeInterface
 *
 * @extends ArrayCollection<TKey,T>
 */
class NodeCollection extends ArrayCollection implements \JsonSerializable
{
    use IdentifiableNodesTrait;
    use ReachableNodesTrait;
    use StepRolePermissionTrait;

    final public static function createEmpty(): static
    {
        return new static();
    }

    public function getCategoryNodes(): CategoryNodeCollection
    {
        /** @var CategoryNode[] $categoryNodes */
        $categoryNodes = [];

        foreach ($this->getIterator() as $node) {
            if (!$node instanceof CategoryNode) {
                continue;
            }

            $categoryNodes[] = $node;
        }

        return new CategoryNodeCollection($categoryNodes);
    }

    /** @return InputNodeCollection<array-key, InputNode> */
    public function getInputNodes(): InputNodeCollection
    {
        /** @var InputNode[] $inputNodes */
        $inputNodes = [];

        foreach ($this->getIterator() as $node) {
            if (!$node instanceof InputNode) {
                continue;
            }

            $inputNodes[] = $node;
        }

        return new InputNodeCollection($inputNodes);
    }

    /** @return ReadOnlyViewNodeCollection<array-key, AbstractReadOnlyViewNode> */
    public function getReadOnlyViewNodes(): ReadOnlyViewNodeCollection
    {
        /** @var AbstractReadOnlyViewNode[] $readOnlyViewNodes */
        $readOnlyViewNodes = [];

        foreach ($this->getIterator() as $node) {
            if (!$node instanceof AbstractReadOnlyViewNode) {
                continue;
            }

            $readOnlyViewNodes[] = $node;
        }

        return new ReadOnlyViewNodeCollection($readOnlyViewNodes);
    }

    /**
     * get all inputNodes that don't depend on other inputNodes (= are not child of another inputNode).
     *
     * @see ParentalInputNode
     *
     * @phpstan-return InputNodeCollection<array-key, InputNode>
     */
    public function getParentalInputNodes(): InputNodeCollection
    {
        /** @var InputNode[] $inputNodes */
        $inputNodes = [];

        /** @var InputNode $node */
        foreach ($this->getIterator() as $node) {
            if (!$node instanceof ParentalInputNode) {
                continue;
            }

            $inputNodes[] = $node;
        }

        return new InputNodeCollection($inputNodes);
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * @phpstan-return static
     *
     * @psalm-return static<TKey, T>
     */
    public function getUnique(): static
    {
        $elements = $this->toArray();

        $uniqueElements = [];
        foreach ($elements as $key => $element) {
            // check whether element is a duplicate
            if (!in_array($element, $uniqueElements, true)) {
                $uniqueElements[$key] = $element; // preserve the first encountered key (follows array_unique behaviour)
            }
        }

        return new static($uniqueElements);
    }
}
