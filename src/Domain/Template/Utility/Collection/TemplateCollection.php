<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;
use Webmozart\Assert\Assert;

/**
 * @extends ArrayCollection<array-key,TemplateInterface>
 */
final class TemplateCollection extends ArrayCollection
{
    public function hasOneById(string $id): bool
    {
        Assert::notEmpty($id);

        $result = $this->filter(fn (TemplateInterface $template) => $template->getId() === $id);

        return 1 === $result->count();
    }

    public function getOneById(string $id): TemplateInterface
    {
        $result = $this->filter(fn (TemplateInterface $template) => $template->getId() === $id);
        $first = $result->first();
        if ($first instanceof TemplateInterface && 1 === $result->count()) {
            return $first;
        }

        throw new \BadMethodCallException("expected to find one Template. found {$result->count()}.");
    }

    public function getThoseValidForClientUse(): self
    {
        return $this->filter(fn (TemplateInterface $template) => $template->isValidForClientUse());
    }

    /**
     * gets the templates for which a role could have some access at some point in time
     * (i.e. at least read access to at least one Input Node (= question) in at least one workflow step).
     */
    public function getThoseThatCouldAllowRoleToParticipate(Role $role): self
    {
        return $this->filter(fn (TemplateInterface $template) => $template->couldAllowRoleToParticipate($role));
    }

    /**
     * gets the templates for which a role could have some access at some point in time
     * (i.e. at least read access to at least one Input Node (= question) in at least one workflow step).
     */
    public function getThoseThatCouldAllowAtLeastOneOfRolesToParticipate(RoleCollection $roles): self
    {
        return $this->filter(fn (TemplateInterface $template) => $template->couldAllowAtLeastOneOfRolesToParticipate($roles));
    }

    /**
     * TODO DV consider dropping this and use @see getThoseThatCouldAllowAtLeastOneRoleToParticipate().
     *
     * gets the templates for which a client (assumed having target access) could have some access at some point in time
     * (i.e. at least read access to at least one Input Node (= question)
     * and for at least one of his/her roles and in at least one workflow step)
     *
     * REMARK: this is primarily useful/meaningful if each template in this collection is targeting the same set of
     * entities, but it can be used in a broader sense as well, since this only checks COULD participate
     */
    public function getThoseThatCouldAllowClientWithTargetAccessToParticipate(
        ClientWithTargetAccessInterface $clientWithTargetAccess,
    ): self {
        return $this->filter(fn (TemplateInterface $template) => $template->couldAllowClientWithTargetAccessToParticipate($clientWithTargetAccess));
    }

    /**
     * get array of all unique roles from given user, for which a role could have some access at some point in time
     * (i.e. at least read access to at least one Input Node (= question) in at least one workflow step) for at least
     * one template in the collection.
     * each template will also order the returned roles by the template's
     * target @see TemplateInterface::getHierarchicalRolesThatCouldParticipate() . this method is useful for overview
     * pages where all templates in collection target the same entities/objects, making the ordering same for each
     * template and thus also the resulting unique array.
     *
     * note that this does not mean the user will actually have access to a @see TemplateTargetableInterface for a template,
     * only that user has this set of roles that could participate in one or more of the templates. used for performance
     * reasons in overview to further filtering & building roleSwitcher.
     *
     * REMARK: this is primarily useful/meaningful if each template in this collection is targeting the same set of
     * entities, but it can be used in a broader sense as well, since this only checks COULD participate
     */
    public function getUniqueRolesThatCouldAllowClientWithTargetAccessToParticipate(
        ClientWithTargetAccessInterface $clientWithTargetAccess,
    ): RoleCollection {
        $roles = [];

        /** @var TemplateInterface $template */
        foreach ($this->getIterator() as $template) {
            $roles = array_merge(
                $roles,
                $template->getRolesThatCouldAllowClientWithTargetAccessToParticipate($clientWithTargetAccess)->toArray()
            );
        }

        // array_unique strict comparison converts relies on __toString() for comparing, which Role implements
        return new RoleCollection(array_unique($roles));
    }
}
