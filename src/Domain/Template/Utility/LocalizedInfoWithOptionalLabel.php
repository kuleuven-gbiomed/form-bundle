<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Utility;

use KUL\FormBundle\Domain\Contract\LocalizedOptionalLabelInterface;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;

/**
 * Class LocalizedInfoWithOptionalLabel.
 *
 * extended version of @see AbstractLocalizedInfo for which all translated info texts are optional, including the label
 */
final class LocalizedInfoWithOptionalLabel extends AbstractLocalizedInfo implements LocalizedOptionalLabelInterface
{
    protected function __construct(
        LocalizedOptionalString $label,
        LocalizedOptionalString $description,
        RoleLocalizedOptionalStringCollection $roleDescriptions,
    ) {
        parent::__construct($label, $description, $roleDescriptions);
    }

    /**
     * @throws LocalizedStringException
     */
    public static function createEmpty(): self
    {
        return new self(
            LocalizedOptionalString::createEmpty(),
            LocalizedOptionalString::createEmpty(),
            RoleLocalizedOptionalStringCollection::createEmpty()
        );
    }

    /**
     * create with only the optional (thus possibly also empty) label and adding only empty descriptions.
     *
     * @throws LocalizedStringException
     */
    public static function fromLocalizedOptionalLabel(LocalizedOptionalString $label): self
    {
        return new self(
            $label,
            LocalizedOptionalString::createEmpty(),
            RoleLocalizedOptionalStringCollection::createEmpty()
        );
    }

    public static function fromLocalizedStrings(
        LocalizedOptionalString $label,
        LocalizedOptionalString $description,
        RoleLocalizedOptionalStringCollection $roleDescriptions,
    ): self {
        return new self($label, $description, $roleDescriptions);
    }

    /**
     * build form info based on data with all optional translated texts (label, descriptions).
     *
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        // all strings (labels & (role)descriptions) are optional, so absence of their respective array keys is allowed
        $labelData = $data[self::KEY_LABEL] ?? [];
        $descriptionData = $data[self::KEY_DESCRIPTION] ?? [];
        $roleDescriptionsData = $data[self::KEY_ROLE_DESCRIPTIONS] ?? [];

        return new self(
            LocalizedOptionalString::fromJsonDecodedArray($labelData),
            LocalizedOptionalString::fromJsonDecodedArray($descriptionData),
            RoleLocalizedOptionalStringCollection::fromJsonDecodedArray($roleDescriptionsData)
        );
    }

    /**
     * overwritten for type hint on specific return type.
     */
    public function getLocalizedLabel(): LocalizedOptionalString
    {
        /** @var LocalizedOptionalString $label */
        $label = parent::getLocalizedLabel();

        return $label;
    }

    /**
     * @throws LocalizedStringException if no valid locale string given
     */
    public function hasLabel(string $locale): bool
    {
        return $this->getLocalizedLabel()->hasForLocaleOrFallback($locale);
    }
}
