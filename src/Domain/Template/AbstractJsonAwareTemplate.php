<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template;

use KUL\FormBundle\Admin\Tools\AutoQuestionsAccessBuilder;
use KUL\FormBundle\Client\Slugifier\RoleSlugifier;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Exception\TemplateException;
use KUL\FormBundle\Domain\Template\Exception\VersionException;
use KUL\FormBundle\Domain\Template\Utility\Collection\PublishedVersionCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Template\Version\PublishedVersion;
use KUL\FormBundle\Domain\Template\Version\Validator\InvalidPublishMessageCollection;
use KUL\FormBundle\Domain\Template\Version\Validator\WorkingVersionValidator;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Role;
use Webmozart\Assert\Assert;

/**
 * Class AbstractJsonAwareTemplate.
 *
 * abstract version of @see TemplateInterface, containing everything needed for the generic processing of Templates
 * and implementable as mapped superclass.
 *
 * NOTE: this class uses JSON to store elements internally: most template element properties in this class contain
 * JSON representation of the associated element because of implementation of this class as mapped entity (stored in DB)
 * with serialized data in dedicated DB columns: while it is possible to use Doctrine's JSON type (or Array type),
 * the en-/decoding of JSON on object level gives more control & easier need-only deserialization than relying on
 * Doctrine's JSON type for the en-/decoding via dbal layer.
 * NOTE: this abstract is meant to store templates in DB with JsonAware elements only! other template implementations
 * needs to be stored in DB or other means of storage without JsonAware elements, will require own implementation
 *
 * IMPORTANT: the @see AbstractJsonAwareTemplate::$currentVersionNumber is stored redundantly upon publish of a new
 * current version (via @see AbstractJsonAwareTemplate::publishNewCurrentVersion() ). the current version number could
 * be retrieved by checking if a serialized current version exist and if one exists, de-serialize that current version
 * to get the current version number. but this number is stored redundantly for performance reasons:  it allow us to not
 * have to completely de-serialize the current published version if we don't need it that current version and only want
 * the current version number and/or if we just want to check if there is a current version.
 *
 * IMPORTANT: the 'cached' props are used to avoid having to de-serialize the associated JSON properties when working
 * on instances of templates. this 'local caching' greatly improves performance especially when working on end-users-side
 * with multiple @see StoredTemplateTargetInterface targets all related to the same template, which in end-users-side
 * usage doesn't ever change. these 'cached' props are therefore always nullable and - when needed in e.g. admin-side
 * template building - are resettable to their null values.
 *
 * IMPORTANT: some (non local cached) properties (mostly the serialized ones) are nullable for the simple reason that
 * building a template by admins is a big, complex and time consuming job, which need to happen in several steps.
 * it also uses a 'working version' to allow admins to 1) build templates that are not fully valid for publish yet
 * and 2) allow to make alterations to a possible currently published version of a template without having to
 * disable the template in all end-users logic until alterations are done. therefore, not all properties can be set upon
 * first construct and only on publish are all properties and the working version fully validated.
 * this also implies that developers should ONLY set a published version (both a new or existing ones) via the provided
 * method @see AbstractJsonAwareTemplate::publishNewCurrentVersion whioch is the SOLE point of entry to set a published
 * version. this method takes the working version and validates it before turning it into a new published version!
 *
 * NOTE: scope of properties must be protected for mapping purposes of extending classes
 * NOTE: locally cached properties (prefixed with 'cached') are intentionally nullable for cache clearing & rebuilding
 */
abstract class AbstractJsonAwareTemplate implements TemplateInterface
{
    protected string $id;
    protected string $targetingType;

    /**
     * only active templates are accessible for end-users (clients)
     * a template is active as soon as a working version is successfully published as current version,
     * or if the current version is re-activated after a (temporary) deactivation.
     */
    protected bool $active = false;
    /** number from current published version (null if no current version) (redundantly stored for quick querying). */
    protected ?int $currentVersionNumber = null;

    // elements serialized & stored in DB as JSON and locally cached once after first call de-serialization of JSON
    // (caching for performance reasons and only when de-serialized element is called for)

    /** DB JSON representation of roles used for preliminary checking possible role participation & building roles access to questions */
    protected string $serializedHierarchicalRolesThatCouldParticipate;
    /** (locally cached) roles used for preliminary checking possible role participation & building roles access to questions */
    private ?RoleCollection $cachedHierarchicalRolesThatCouldParticipate = null;

    /** DB stored JSON representation of roles for which auto temporary saving is always disabled */
    protected string $serializedRolesWithDisabledAutoTemporarySave;
    /** (locally cached) roles for which auto temporary saving is always disabled (usually admin level roles) */
    private ?RoleCollection $cachedRolesWithDisabledAutoTemporarySave = null;

    /** DB stored JSON representation of roles allowed to copy answer between two template target forms */
    protected string $serializedRolesAllowedToCopyAnswers;

    /** DB stored JSON representation of roles allowed to export answers of submitted template target forms */
    protected string $serializedRolesAllowedToExport;
    /** (locally cached) roles allowed to export answers of submitted template target forms */
    private ?RoleCollection $cachedRolesAllowedToExport = null;

    /**
     * admin-side helper property
     * DB stored JSON representation of roles (usually the admin-roles) that should get automatic write access assigned
     * in admin-side template building, to all questions from the first step in the workflow in which at least one
     * (same or other) role (admin or non-admin) has at least read access (or write) access and continued to get
     * write access to those questions in all steps that follow that first found step.
     */
    protected string $serializedRolesForAutoQuestionWriteAccessInAdminSide;
    /** (locally cached) roles that should get automatic write access in admin-side template building */
    private ?RoleCollection $cachedRolesForAutoQuestionWriteAccessInAdminSide = null;

    /** DB stored JSON representation of working version */
    protected string $serializedWorkingVersion;
    /** (locally cached) contains all locally cached de-serialized working version elements */
    private ?WorkingVersion $cachedWorkingVersion = null;

    /** DB storedJSON representation of current published version (null if none yet) */
    protected ?string $serializedCurrentVersion = null;
    /** (locally cached) contains all locally cached de-serialized currently published version elements */
    private ?PublishedVersion $cachedCurrentVersion = null;

    /** DB stored JSON representation of previously published and now unused versions (null if none yet) */
    protected ?string $serializedPreviousVersions = null;
    /** (locally cached) contains all locally cached de-serialized previously published versions */
    private ?PublishedVersionCollection $cachedPreviousVersions = null;

    // elements filtered/derived from other elements and locally cached once after first call for performance reasons

    /** (locally cached) roles with at active access at some point in current version */
    private ?RoleCollection $cachedRolesThatParticipateInCurrentVersion = null;

    /**
     * AbstractJsonAwareTemplate constructor.
     *
     * NOTE: in admin-side, the templates are build in bits and pieces, due to the huge amount of stuff needed to
     * build a template. the first things that will be asked and minimally required from the admin when the admin start
     * making a new template, will be a category and a working version. the working version does not has to be fully valid
     * or even have all/any of the required elements (workFlow, formList, info).
     * however usually the admin side will ask the admin to immediately provide a working version label for the template
     *
     * @param WorkingVersion $workingVersion a working version - possibly completely empty and/or invalid - is required
     *
     * @throws TemplateException
     *
     * @see LocalizedInfoWithRequiredLabel::getLabel() on the workingVersion) but that is not expected/required as well.
     *
     * - id and targetingType are hardcoded or in some other way generated by internal logic in admin-side.
     * - hierarchicalRolesThatCouldParticipate is usually hardcoded or in some other way generated by internal logic
     *   in the admin side, but it could be admin inputable as well.
     *
     * $enableReadOnlyChoiceAnswerOptionsToggling:
     * end-user can toggle (show/hide) the non-chosen answer options of a read-only choice question in the view if relevant.
     * if toggled to shown, the user will see the chosen answer option(s) highlighted between the non-chosen ones.
     * this is not version related. this front-end related option: serves to render button & functionality for the toggle.
     *
     * $enableAutoTemporarySaving: overall enabling or disabling of autosave (for all roles)
     * $rolesWithDisabledAutoTemporarySaving: role specific disabling of autosave (if overall autosave enabled)
     * note: $enableAutoTemporarySaving is false by default to avoid unwanted enabling of autosave on older forms that
     * were already generated and in use before the autosave functionality was implemented (activated) in this form-bundle
     */
    public function __construct(
        string $id,
        string $targetingType,
        WorkingVersion $workingVersion,
        RoleCollection $hierarchicalRolesThatCouldParticipate,
        RoleCollection $rolesAllowedToCopyAnswers,
        RoleCollection $rolesWithDisabledAutoTemporarySaving,
        RoleCollection $rolesAllowedToExport,
        RoleCollection $rolesForAutoQuestionWriteAccessInAdminSide,
        protected bool $usePageBreaks = false,
        protected bool $enableReadOnlyChoiceAnswerOptionsToggling = false,
        protected bool $enableAutoTemporarySaving = false,
        protected bool $showPositionNumbers = false,
    ) {
        Assert::stringNotEmpty($id);
        Assert::stringNotEmpty($targetingType);

        $this->id = $id;
        $this->targetingType = $targetingType;

        // ## START SETTING ROLES

        // first set the roles_that_could_participate. these roles are the ones that will be used on ANY access attempts
        // by clients to validate if the client COULD participate in this template, and if the role COULD NOT participate,
        // it means no further client-role access to questions will even be considered! (performance gain when using
        // these roles to quickly check if a role COULD participate) also used for building question-access in admin-side!
        $this->updateHierarchicalRolesThatCouldParticipate($hierarchicalRolesThatCouldParticipate);
        // then set the others. any role that is used here, MUST BE in above set of RolesThatCouldParticipate. because
        // ANY access by clients first has to pass those RolesThatCouldParticipate. if you add role(s) that are not in
        // RolesThatCouldParticipate, they will be silently filtered (intersected) out of the given roles.
        $this->updateRolesWithDisabledAutoTemporarySaving($rolesWithDisabledAutoTemporarySaving);
        $this->updateRolesAllowedToExport($rolesAllowedToExport);
        // IMPORTANT: these are roles to be used in admin side only to 'pre-set' some write access for the given roles.
        // but like any other set of roles, the roles in this set MUST BE in RolesThatCouldParticipate, for the simple
        // reason that you might use these roles to set legitimate access to questions, but if the role is not in the
        // RolesThatCouldParticipate, a client with such a role would not even get past the 'could participate' check.
        // like with other sets of roles, any role that is not in RolesThatCouldParticipate, will be silently filtered out.
        // (when you take the diff between RolesThatCouldParticipate and this RolesForAutomaticQuestionWriteAccess, you
        // get the roles for which no automatic write access setting is to be done in admin side ;) duh)
        $this->updateRolesForAutomaticQuestionWriteAccessBuildingInAdminSide($rolesForAutoQuestionWriteAccessInAdminSide);
        // ## END SETTING ROLES

        /* @deprecated the functionality allowing end-users to copy answers between (filled-in) forms (StoredTemplateTargets) is not implemented. */
        $this->serializedRolesAllowedToCopyAnswers = json_encode($rolesAllowedToCopyAnswers, \JSON_THROW_ON_ERROR);

        // set the working version, with as much or as little it may contain at this time (usually only the info)
        $this->updateWorkingVersion($workingVersion);
        // somewhat redundant, but make sure the template is not set to active
        $this->deactivate();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTargetingType(): string
    {
        return $this->targetingType;
    }

    public function getHierarchicalRolesThatCouldParticipate(): RoleCollection
    {
        if (!$this->cachedHierarchicalRolesThatCouldParticipate instanceof RoleCollection) {
            $this->cachedHierarchicalRolesThatCouldParticipate = RoleCollection::fromJsonDecodedArray(
                JsonDecoder::toAssocArray($this->serializedHierarchicalRolesThatCouldParticipate)
            );
        }

        return $this->cachedHierarchicalRolesThatCouldParticipate;
    }

    public function getParticipatableRoleSlugifier(): RoleSlugifier
    {
        return RoleSlugifier::fromRoleCollection($this->getHierarchicalRolesThatCouldParticipate());
    }

    /**
     * what roles have to be used to check initial potential access (performance-fast access check)
     * and have to be used to build and set the matrix of role-workflow-access write/read/no questions access in admin-side.
     *
     * IMPORTANT: any updating of this list automatically DE-ACTIVATES THE TEMPLATE because changes in this set of roles
     * has too much potential impact on the role-workflow-access questions access in the current version.
     * by de-activating here we can enforce a re-validation of the current version via @see reactivateCurrentVersion
     * in case the template already has/had a current version and thus was active. if the template does not yet
     * has a current version, then the usual @see publishNewCurrentVersion will validate the version
     *
     * IMPORTANT: this also has impact on the other role related properties such as roles allowed to export,
     * disable auto temporary saving and more. those are filtered on hierarchical roles via getters and update methods
     *
     * @throws TemplateException
     */
    public function updateHierarchicalRolesThatCouldParticipate(RoleCollection $roles): void
    {
        $this->clearAllLocalCaches();

        if ($roles->isEmpty()) {
            throw new TemplateException('rolesThatCouldParticipate: at least one role is required for configuring possible participation and access building');
        }

        $this->cachedHierarchicalRolesThatCouldParticipate = $roles;
        $this->serializedHierarchicalRolesThatCouldParticipate = json_encode($roles, \JSON_THROW_ON_ERROR);
    }

    public function couldAllowRoleToParticipate(Role $role): bool
    {
        return $this->getHierarchicalRolesThatCouldParticipate()->hasRole($role);
    }

    public function couldAllowAtLeastOneOfRolesToParticipate(RoleCollection $roles): bool
    {
        return $this->getHierarchicalRolesThatCouldParticipate()->hasAtLeastOneOfRoles($roles);
    }

    public function couldAllowClientWithTargetAccessToParticipate(
        ClientWithTargetAccessInterface $clientWithTargetAccess,
    ): bool {
        return $this->couldAllowAtLeastOneOfRolesToParticipate($clientWithTargetAccess->getAccessingRoles());
    }

    public function getRolesThatCouldAllowClientWithTargetAccessToParticipate(
        ClientWithTargetAccessInterface $clientWithTargetAccess,
    ): RoleCollection {
        $roles = [];

        foreach ($clientWithTargetAccess->getAccessingRoles() as $accessingRole) {
            if ($this->couldAllowRoleToParticipate($accessingRole)) {
                $roles[] = $accessingRole;
            }
        }

        return new RoleCollection($roles);
    }

    /**
     * @throws MissingVersionException
     * @throws \Exception
     */
    public function getRolesThatParticipateInCurrentVersion(): RoleCollection
    {
        if (!$this->cachedRolesThatParticipateInCurrentVersion instanceof RoleCollection) {
            $currentFormList = $this->getCurrentVersion()->getFormList();
            $reachableInputNodes = $currentFormList->getFlattenedReachableParentalInputNodes();
            $reachableReadOnlyViewNodes = $currentFormList->getFlattenedReachableReadOnlyViewNodes();
            $roles = [];

            /** @var Role $role */
            foreach ($this->getHierarchicalRolesThatCouldParticipate() as $role) {
                if ($reachableInputNodes->hasReadableForRole($role) || $reachableReadOnlyViewNodes->hasReadableForRole($role)) {
                    $roles[] = $role;
                }
            }

            $this->cachedRolesThatParticipateInCurrentVersion = new RoleCollection($roles);
        }

        return $this->cachedRolesThatParticipateInCurrentVersion;
    }

    /**
     * @throws MissingVersionException
     */
    public function allowsRoleToParticipateInCurrentVersion(Role $role): bool
    {
        return $this->getRolesThatParticipateInCurrentVersion()->hasRole($role);
    }

    public function allowsRoleToParticipateInCurrentVersionStep(Role $role, StepInterface $step): bool
    {
        // check general access
        if (!$this->getHierarchicalRolesThatCouldParticipate()->hasRole($role)) {
            return false;
        }

        $currentFormList = $this->getCurrentVersion()->getFormList();

        // check role has read access (direct or indirect via write access) to at least one question or readOnlyView node in given step
        return $currentFormList->getFlattenedReachableParentalInputNodes()->hasReadableForRoleInStep($role, $step)
            || $currentFormList->getFlattenedReachableReadOnlyViewNodes()->hasReadableForRoleInStep($role, $step);
    }

    /**
     * @throws MissingVersionException
     */
    public function allowsAtLeastOneOfRolesToParticipateInCurrentVersion(RoleCollection $roles): bool
    {
        return $this->getRolesThatParticipateInCurrentVersion()->hasAtLeastOneOfRoles($roles);
    }

    /**
     * @throws MissingVersionException
     */
    public function allowsClientWithTargetAccessToParticipateInCurrentVersion(
        ClientWithTargetAccessInterface $clientWithTargetAccess,
    ): bool {
        return $this->allowsAtLeastOneOfRolesToParticipateInCurrentVersion($clientWithTargetAccess->getAccessingRoles());
    }

    /**
     * NOTE : the roles for which auto temporary saving has to be disabled, are only useful if they uberhaupt could
     * allow to participate, hence the filtering on roles-that-could-participate. this filtering is also already done
     * in @see updateRolesWithDisabledAutoTemporarySaving but needs repeating here in case of updating of roles-that-could-participate.
     */
    public function getRolesWithDisabledAutoTemporarySaving(): RoleCollection
    {
        if (!$this->cachedRolesWithDisabledAutoTemporarySave instanceof RoleCollection) {
            $this->cachedRolesWithDisabledAutoTemporarySave = RoleCollection::fromJsonDecodedArray(
                JsonDecoder::toAssocArray($this->serializedRolesWithDisabledAutoTemporarySave)
            )->getDuplicatesByRoles($this->getHierarchicalRolesThatCouldParticipate());
        }

        return $this->cachedRolesWithDisabledAutoTemporarySave;
    }

    public function updateRolesWithDisabledAutoTemporarySaving(RoleCollection $roles): void
    {
        $this->clearAllLocalCaches();

        // no point in adding roles that have no possible participation anyway
        $roles = $roles->getDuplicatesByRoles($this->getHierarchicalRolesThatCouldParticipate());

        $this->cachedRolesWithDisabledAutoTemporarySave = $roles;
        $this->serializedRolesWithDisabledAutoTemporarySave = json_encode($roles, \JSON_THROW_ON_ERROR);
    }

    public function shouldDisableAutoTemporarySavingForRole(Role $role): bool
    {
        return !$this->isAutoTemporarySavingEnabled()
            || $this->getRolesWithDisabledAutoTemporarySaving()->hasRole($role);
    }

    public function getRolesAllowedToExport(): RoleCollection
    {
        if (!$this->cachedRolesAllowedToExport instanceof RoleCollection) {
            $this->cachedRolesAllowedToExport = RoleCollection::fromJsonDecodedArray(
                JsonDecoder::toAssocArray($this->serializedRolesAllowedToExport)
            )->getDuplicatesByRoles($this->getHierarchicalRolesThatCouldParticipate());
        }

        return $this->cachedRolesAllowedToExport;
    }

    public function updateRolesAllowedToExport(RoleCollection $roles): void
    {
        $this->clearAllLocalCaches();

        // no point in adding roles that have no possible participation anyway -> filter out
        $roles = $roles->getDuplicatesByRoles($this->getHierarchicalRolesThatCouldParticipate());

        $this->cachedRolesAllowedToExport = $roles;
        $this->serializedRolesAllowedToExport = json_encode($roles, \JSON_THROW_ON_ERROR);
    }

    public function allowsRoleToExport(Role $role): bool
    {
        return $this->getRolesAllowedToExport()->hasRole($role);
    }

    /**
     * the admin roles that can be used to set write access on every question in those steps following, and
     * including, the first step in which at least one role (admin or non-admin) has at least read access.
     *
     * to use these roles to set auto-access in admin-side (which can be done on any moment during template building,
     * even if not everything in template's working version is already valid): @see AutoQuestionsAccessBuilder
     */
    public function getRolesForAutomaticQuestionWriteAccessBuildingInAdminSide(): RoleCollection
    {
        if (!$this->cachedRolesForAutoQuestionWriteAccessInAdminSide instanceof RoleCollection) {
            $this->cachedRolesForAutoQuestionWriteAccessInAdminSide = RoleCollection::fromJsonDecodedArray(
                JsonDecoder::toAssocArray($this->serializedRolesForAutoQuestionWriteAccessInAdminSide)
            )->getDuplicatesByRoles($this->getHierarchicalRolesThatCouldParticipate());
        }

        return $this->cachedRolesForAutoQuestionWriteAccessInAdminSide;
    }

    public function updateRolesForAutomaticQuestionWriteAccessBuildingInAdminSide(RoleCollection $roles): void
    {
        $this->clearAllLocalCaches();

        // no point in adding roles that have no possible participation anyway
        $roles = $roles->getDuplicatesByRoles($this->getHierarchicalRolesThatCouldParticipate());

        $this->cachedRolesForAutoQuestionWriteAccessInAdminSide = $roles;
        $this->serializedRolesForAutoQuestionWriteAccessInAdminSide = json_encode($roles, \JSON_THROW_ON_ERROR);
    }

    public function shouldConsiderToSetAutomaticQuestionWriteAccessInAdminSideForRole(Role $role): bool
    {
        return $this->getRolesForAutomaticQuestionWriteAccessBuildingInAdminSide()->hasRole($role);
    }

    /**
     * returns the version on which admins are working; adding, removing or updating steps, question, access rights,
     * info, etc.
     *
     * NOTE: the working version always is either ahead or equal to the current published version.
     *
     * @throws InputInvalidArgumentException
     * @throws StepException
     * @throws StepNotificationException
     * @throws LocalizedStringException
     */
    public function getWorkingVersion(): WorkingVersion
    {
        if (!$this->cachedWorkingVersion instanceof WorkingVersion) {
            $this->cachedWorkingVersion = WorkingVersion::fromJsonDecodedArray(
                JsonDecoder::toAssocArray($this->serializedWorkingVersion)
            );
        }

        return $this->cachedWorkingVersion;
    }

    public function hasUnpublishedChanges(): bool
    {
        // blank working version (nothing ever added or everything removed)
        if ($this->getWorkingVersion()->isEmpty()) {
            return false;
        }

        // no published version (and at least something in the working version), then working version is always ahead
        if (!$this->hasCurrentVersion()) {
            return true;
        }

        // check any change between latest and working versions (every change from e.g. fully altered formList and
        // workflow to a single  moved question to the smallest spelling error, is cause for unpublished changes)
        return !(json_encode($this->getWorkingVersion(), \JSON_THROW_ON_ERROR) === json_encode($this->getCurrentVersion()->toWorkingVersion(), \JSON_THROW_ON_ERROR));
    }

    /**
     * store the changes made to the steps, questions, access, info, etc. in the working version made by admins.
     *
     * NOTE: this has no effect on the currently published version (if any), which keep being used by end-users (clients)
     * until this working version is used to create a new current version
     */
    public function updateWorkingVersion(WorkingVersion $workingVersion): void
    {
        $this->clearAllLocalCaches();

        $this->cachedWorkingVersion = $workingVersion;
        $this->serializedWorkingVersion = json_encode($workingVersion, \JSON_THROW_ON_ERROR);
    }

    public function updateCurrentVersion(PublishedVersion $currentVersion): void
    {
        $this->clearAllLocalCaches();

        $this->cachedCurrentVersion = $currentVersion;
        $this->serializedCurrentVersion = json_encode($currentVersion, \JSON_THROW_ON_ERROR);
    }

    //    /**
    //     * use the @see AbstractJsonAwareTemplate::getRolesForAutomaticQuestionWriteAccessBuildingInAdminSide() set of
    //     * roles and (try to) set access
    //     *
    //     * NOTE: this has no effect on the currently published version (if any), which keep being used by end-users (clients)
    //     * until this working version is used to create a new current version
    //     *
    //     * NOTE: this won't break if working version is not yet valid, has not yet questions, workflow, etc. it will just
    //     * do nothing as in it will alter nothing or only set the writes on questions that are valid (have at least read
    //     * access somewhere in the flow).
    //     *
    //     * NOTE: due to the fact that in admin-side, questions & workflow steps change, are added and updated, this method
    //     * may have to be called a few times. in this developer's opinion, this shouldn't be called without automatically
    //     * in background, without admin knowing about it. because this overwrites/resets all access for these roles.
    //     * an admin might have set some custom access for these roles on some questions, like no or only read access for
    //     * one or more of these (admin) roles in one or more later steps after admin has set write access in an earlier
    //     * step or whatever.... if you would call this method then upon an publish attempt or after every change, these
    //     * rights will be overwritten and reset. so, in my opinion, this could be callable via a button in admin-side,
    //     * e.g. right after all questions are made and all non-admin role access has been set, so that admin than can still
    //     * change or add any custom access.
    //     * or even revoke all admin role access if needed @see InputNodeCollection::withFlowPermissionsRevokedForRoleInAllSteps()
    //     * if this method is to drastic you can use other methods in @see AutoQuestionsAccessBuilder to get you
    //     * something that can be used in front-end code to pre-fill access there
    //     *
    //     * i.e. USE WITH CARE
    //     *
    //     * @throws \Exception
    //     */
    //    public function updateWorkingVersionWithAutoAssignedTimedWriteAccessForDesignatedRoles()
    //    {
    //        $this->updateWorkingVersion(
    //            AutoQuestionsAccessBuilder::createUpdatedWorkingVersionWithAutoAssignedTimedWriteAccessByWorkFlowForRoles(
    //                $this->getWorkingVersion(),
    //                $this->getRolesForAutomaticQuestionWriteAccessBuildingInAdminSide()
    //            )
    //        );
    //    }

    /**
     * check if a current version is set.
     *
     * NOTE: for performance reasons, the check is done on the redundantly stored versionNumber and additionally - as
     * an extra check to make sure no-one tampered with DB JSON data - if there is serialized JSON data for the current
     * version, both of which should exist and not be empty if a current version was published! De-serializing the
     * current version here would be to heavy on performance if the only thing we want to know is if there is one.
     */
    public function hasCurrentVersion(): bool
    {
        return is_int($this->currentVersionNumber) && (null !== $this->serializedCurrentVersion && '' !== $this->serializedCurrentVersion);
    }

    public function isValidForClientUse(): bool
    {
        // if active, it will have a current version, but no harm in asserting it here, just in - the very slim and
        // very unlikely - case that this ever changes (in development)
        return $this->isActive() && $this->hasCurrentVersion();
    }

    /**
     * get the version number of the current published version.
     *
     * NOTE: fails if there is no (serialized) current version and/or no (redundantly stored) current version number.
     * NOTE: for performance reasons, the serialized current version is not de-serialized here to check if its version
     * number matches the redundantly stored version number. is it reasonably safe to assume that they will match and if
     * not, the first time the current version is called via @see AbstractJsonAwareTemplate::getCurrentVersion(), an
     * exception will be thrown, as it anyway that methods job to assert the correct version is the current one.
     *
     * @throws MissingVersionException
     */
    public function getCurrentVersionNumber(): int
    {
        if (!is_int($this->currentVersionNumber) || (null === $this->serializedCurrentVersion || '' === $this->serializedCurrentVersion)) {
            throw new MissingVersionException('no current version available');
        }

        return $this->currentVersionNumber;
    }

    /**
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws MissingVersionException
     * @throws StepException
     * @throws StepNotificationException
     * @throws VersionException
     */
    public function getCurrentVersion(): PublishedVersion
    {
        if (!$this->hasCurrentVersion()) {
            throw new MissingVersionException('no current version available');
        }

        if (!$this->cachedCurrentVersion instanceof PublishedVersion) {
            $currentVersion = PublishedVersion::fromJsonDecodedArray(
                JsonDecoder::toAssocArray($this->serializedCurrentVersion)
            );

            // avoid tampering with numbering in DB, assert version numbering is correct
            if ($currentVersion->getVersionNumber() === $this->getCurrentVersionNumber()) {
                $this->cachedCurrentVersion = $currentVersion;
            } else {
                throw new VersionException('the version number in the current version ['.((string) $currentVersion->getVersionNumber()).'] does not match the redundantly stored current version number ['.((string) $this->getCurrentVersionNumber()).']');
            }
        }

        return $this->cachedCurrentVersion;
    }

    /**
     * build the validator based on the workingVersion and roles that could participate in this template.
     *
     * NOTE: could locally cache this and add it to the @see clearAllLocalCaches() so that the exact same valicator
     * is never build twice e.g. when checking for publish and then use it again to get messages. but somehow it
     * seems better (safer) to re-build something as a validator on every call.
     * NOTE: public visibility gives more freedom to developers and internal methods using this are only helper methods anyway
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public function getWorkingVersionValidator(): WorkingVersionValidator
    {
        return WorkingVersionValidator::fromWorkingVersionAndParticipatableRoles(
            $this->getWorkingVersion(),
            $this->getHierarchicalRolesThatCouldParticipate()
        );
    }

    /**
     * check if the working version can be used to publish a new current published version.
     *
     * WARNING: this check can be heavy on performance since it will run a load of checks on the elements in the
     * working version! especially if there are lots of workFlow steps, questions and complex access rights on the
     * questions in the workingVersion.
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public function canPublishNewCurrentVersion(): bool
    {
        return $this->getWorkingVersionValidator()->allowsPublish();
    }

    public function canRePublishAsExistingCurrentVersion(): bool
    {
        return $this->hasCurrentVersion() && $this->getWorkingVersionValidator()->allowsPublish();
    }

    /**
     * helper method to retrieve the (translatable) error messages (if any)
     * that could arise when trying to publish the working version in to the new current version.
     *
     * use in conjunction with @see canPublishNewCurrentVersion
     * and @see publishNewCurrentVersion .
     * both of which use the same validator based on the same working version to do their respective jobs
     *
     * to get string message, chain on @see InvalidPublishMessageCollection::getMessages() with optional translator
     * parameter if you want them translated instead of getting the hardcoded english error messages
     *
     * NOTE: can be used as replacement for @see canPublishNewCurrentVersion , since both methods build same error
     * messages and the latter does an empty check. so if this method returns empty collection, then template is publishable
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public function getInvalidPublishMessages(): InvalidPublishMessageCollection
    {
        return $this->getWorkingVersionValidator()->getErrorMessages();
    }

    /**
     * use the workingVersion to make a new current published version and activate the Template.
     *
     * IMPORTANT:
     * - this is the only entry point were the current version is set! developers should keep to this!
     * - this is also the only entry point were the previously published versions are updated! developers should keep to this!
     * - to make sure no locally cached properties are invalid, especially the ones derived form the previous current
     *   version, all locally cached props are invalidated and only the json properties are update here.
     * - as a domain decision (on request of app owners), a publish of a new current version automatically activates the
     *   Template. admins can choose to deactivate the Template at any time, so also immediately after a publish
     * - the working version remains untouched and from the moment that the new current version is published based on the
     *   working version, the working version will either be equal to the current version or ahead (if admin makes a change)
     *   until a new publish is done.
     * - when a new current version is published, the current version number is incremented and stored both on the
     *   new current version as well as redundantly. this allows to rely on that redundantly stored current version
     *   number for better performance checks of there being a current version or not, without the need to de-serialize
     *   the current version
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws MissingVersionException
     * @throws StepException
     * @throws StepNotificationException
     * @throws VersionException
     */
    public function publishNewCurrentVersion(): void
    {
        // make a new published version with a new version number or let it fail hard if working version is not valid for publish!
        $newCurrentVersion = PublishedVersion::createForTemplateWithIncrementedVersionNumber($this);

        // get the versions once used as published versions
        $previousVersions = $this->getPreviouslyPublishedVersions();

        // if there was already a current version, store it as a previously published version with the others for later use
        // will be added as alst in array, so that most recent previous version will be last element
        if ($this->hasCurrentVersion()) {
            $previousVersions->add($this->getCurrentVersion());
        }

        // serialize the new current version and the updated previous versions
        $serializedNewVersion = json_encode($newCurrentVersion, \JSON_THROW_ON_ERROR);
        $serializedPreviousVersions = json_encode($previousVersions, \JSON_THROW_ON_ERROR);

        // make sure all local caches will be rebuild on first calls, based on the new serialized current version
        // TODO DV does it really matter that the cache clearing is done here and not at beginning of method (does for performance)?
        $this->clearAllLocalCaches();

        // update the serialized data and store version number of new current version redundantly
        $this->serializedCurrentVersion = $serializedNewVersion;
        $this->serializedPreviousVersions = $serializedPreviousVersions;
        $this->currentVersionNumber = $newCurrentVersion->getVersionNumber();

        // activate the template
        $this->active = true;
    }

    /**
     * IMPORTANT: please note that (re)publishing as existing version means the current published version will be overwritten
     * and lost (i.e. not back-upped as previous version). It therefore should be avoided as much as possible. it should be
     * used only to (re)publish a working version that has undergone small, non-important changes that do not affect
     * the workflow, access rights,  etc. basically it could be used to fix typo's. anything else - from adding, editing
     * or deleting questions, question options, steps, access rights, etc. or anything that might affect the workflow,
     * access, logic or even significantly impact UX experience - should follow the normal publish procedure: which is
     * to publish as new version via @see AbstractJsonAwareTemplate::publishNewCurrentVersion.
     *
     * publishing as existing version is basically updating & replacing the current version. this (re)publish still follows
     * the same procedure as publish as new version, thus starting from the working version and validating that for publish.
     * But contrary to publish as new version, it does not update the current version number, and it does not store the current
     * version as a previous version (previous versions have unique version numbers and storing on (re)publish would have a
     * version with same number twice in previous versions, resulting in all sorts of errors). the idea is to 'replace' the
     * existing current version with an updated one, and make it look like it was always that updated version (hence no new
     * version number and no backing up existing version to previous versions). as a result, end-users will never know their
     * past and future actions happened in other versions. and more importantly, due to not having a 'new' current version,
     * any 'single submit' restrictions for end-user-roles in steps (@see StepInterface::getRolesLimitingUserToOneSubmitPerRole())
     * that already 'used up' their 'single-submit' right will not trigger & grant a new 'single submit' (which happens
     * when new versions - by version number - are published).
     * publishing as existing version also requires the template to already have an existing current version published.
     */
    public function rePublishAsExistingCurrentVersion(): void
    {
        // to publish as existing version, then there must an existing published version available to overwrite!
        if (!$this->hasCurrentVersion()) {
            throw new MissingVersionException('can not (re)publish as existing current version: no such current version found.');
        }

        // make a new published version with same version number as current version or fail hard if working version is not valid for publish!
        $newCurrentVersion = PublishedVersion::createForTemplateWithExistingCurrentVersionNumber($this);

        // serialize the new current version
        $serializedNewVersion = json_encode($newCurrentVersion, \JSON_THROW_ON_ERROR);

        // make sure all local caches will be rebuild on first calls, based on the new serialized current version
        $this->clearAllLocalCaches();

        // let's double check version number of new version is same as current version
        if ($this->getCurrentVersionNumber() !== $newCurrentVersion->getVersionNumber()) {
            throw new VersionException('expected number of new version to (re)publish ['.(string) $newCurrentVersion->getVersionNumber().'] to match existing current version number ['.(string) $this->getCurrentVersionNumber().'].');
        }

        // update the serialized data (no updating previous versions & no need to update version number (same as existing current version)
        $this->serializedCurrentVersion = $serializedNewVersion;

        // activate the template
        $this->active = true;
    }

    /**
     * gets the previously used published versions that were once used as current versions.
     *
     * TODO: in admin side: option to use a previous version to replace working version, allowing admin to alter & re-publish previous version as new current version (including new version number)
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public function getPreviouslyPublishedVersions(): PublishedVersionCollection
    {
        if (!$this->cachedPreviousVersions instanceof PublishedVersionCollection) {
            $this->cachedPreviousVersions = PublishedVersionCollection::fromJsonDecodedArray(
                JsonDecoder::toAssocArray($this->serializedPreviousVersions)
            );
        }

        return $this->cachedPreviousVersions;
    }

    /**
     * check for a previously used published version by version number.
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public function hasPreviouslyPublishedVersionByNumber(int $versionNumber): bool
    {
        return $this->getPreviouslyPublishedVersions()->hasOneForVersionNumber($versionNumber);
    }

    /**
     * gets the previously used published version by version number  or fail.
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public function getPreviouslyPublishedVersionByNumber(int $versionNumber): ?PublishedVersion
    {
        return $this->getPreviouslyPublishedVersions()->getOneForVersionNumber($versionNumber);
    }

    /**
     * gets the previously used published version by version number (or fails if none exists),
     * use that version to set the working version and then (tries to) publishes it.
     *
     * NOTE: in theory, if the requested previous version exists, nothing will go wrong because that previous version
     * was validated upon it's first publish.
     * but if tampered with in DB or if @see getHierarchicalRolesThatCouldParticipate() has been altered so heavely
     * that it completely misses roles that were used in previous version,
     * then the publish could throw @see InvalidPublishMessageCollection exceptions
     *
     * so in admin-side it is advised to just get the previous version first using @see getPreviouslyPublishedVersions()
     * and @see PublishedVersionCollection::getOneForVersionNumber() first
     * convert it to a working version @see PublishedVersion::toWorkingVersion()
     * and use that converted working version to manually updated @see updateWorkingVersion() with it
     * so that you can then call @see AbstractJsonAwareTemplate::canPublishNewCurrentVersion()
     * and - if needed - show admin some invalid publish messages
     * before finally calling @see publishNewCurrentVersion()
     * but, hey, if you like exceptions or prefer a try-catch, go ahead and use this
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws MissingVersionException
     * @throws StepException
     * @throws StepNotificationException
     * @throws VersionException
     */
    public function publishNewCurrentVersionFromPreviouslyPublishedVersionWithNumber(int $versionNumber): void
    {
        $this->updateWorkingVersion(
            $this->getPreviouslyPublishedVersions()->getOneForVersionNumber($versionNumber)->toWorkingVersion()
        );

        $this->publishNewCurrentVersion();
    }

    /**
     * for end-users (clients) to see anything of this template - even just be aware of its existence - it must be active.
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * deny end-users (clients) any access & knowledge of this template, regardless.
     *
     * IMPORTANT: need to clear all local caches in case the deactivate is triggered by some important change on
     * the published version of the template
     */
    public function deactivate(): void
    {
        $this->clearAllLocalCaches();

        $this->active = false;
    }

    public function isDeactivated(): bool
    {
        return $this->hasCurrentVersion() && !$this->isActive();
    }

    /**
     * check if we can make the current version of this template again accessible for end-users (clients).
     *
     * re-activation implies that the template has a current published version and was at least once activated (
     * the successful publish of a current version, automatically activates the template), but
     * later either intentionally de-activated via @see deactivate or automatically de-activated by an admin action
     * on the template that has too much impact on the published version for it to not be re-validated
     * at time of writing @see updateHierarchicalRolesThatCouldParticipate is such an action which could effect
     * the minimal role-based access to the questions and steps in the current published version of this template
     *
     *
     * IMPORTANT: requirements for re-activation:
     * - a current version is required to make a template active.
     * - the current version must still be valid for publish (basically it must pass the validation as a working version)
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws MissingVersionException
     * @throws StepException
     * @throws StepNotificationException
     * @throws TemplateException
     * @throws VersionException
     */
    public function canReactivateCurrentVersion(): bool
    {
        $this->clearAllLocalCaches();

        if (!$this->hasCurrentVersion()) {
            throw new TemplateException('can not re-activate template ['.$this->getId().']: a published version is missing');
        }

        return WorkingVersionValidator::fromWorkingVersionAndParticipatableRoles(
            $this->getCurrentVersion()->toWorkingVersion(),
            $this->getRolesThatParticipateInCurrentVersion()
        )->allowsPublish();
    }

    /**
     * make this template, with its current version, again accessible for end-users (clients).
     *
     * re-activate asserts that there is a current version, then extracts a working version from that current version,
     * validate that extracted working version (checks if it could be published), and if extracted working
     * version is valid, will do nothing more than set template->active back to true. changing nothing to
     * the real working version.
     *
     * all that validation is needed because (TODO in the future) we will use the de-activate and re-activate
     * ALSO to allow admins to make changes DIRECTLY on the current version so that NO re-publish is needed.
     * these direct-on-current-version changes will be VERY limited in what is allowed s such a change: spelling errors,
     * adding an optional description to a step or question, maybe moving a question to another position in the tree
     * structure  in the @see FormList BUT no workflow changes (e.g. adding, removing steps, changes dates on steps)
     * or access changes (e.g changing role-access on questions) or formList changes (adding, removing questions)
     * will be allowed to be made directly on current version! for those drastic changes, admin has to use normal flow
     * and make them on the working version and then do a re-publish!
     *
     * @see canReactivateCurrentVersio
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws MissingVersionException
     * @throws StepException
     * @throws StepNotificationException
     * @throws TemplateException
     * @throws VersionException
     */
    public function reactivateCurrentVersion(): void
    {
        $this->clearAllLocalCaches();

        if (!$this->hasCurrentVersion()) {
            throw new TemplateException('can not re-activate template ['.$this->getId().']: a published version is missing');
        }

        $validator = WorkingVersionValidator::fromWorkingVersionAndParticipatableRoles(
            $this->getCurrentVersion()->toWorkingVersion(),
            $this->getRolesThatParticipateInCurrentVersion()
        );

        if (!$validator->allowsPublish()) {
            $errors = implode(' ### ', $validator->getErrorMessages()->getMessages());
            throw new VersionException('can not re-activate the current published version of this template ['.$this->getId().'] the following issues in the working version are blocking the re-activation: '.$errors);
        }

        $this->active = true;
    }

    //    /**
    //     * TODO we can not just allow enabling at random. like @see reactivateCurrentVersion , we need to re-validate published version, if one exists, because of the need for categories-only on first child-level in FormList
    //     */
    //    public function enablePageBreaks()
    //    {
    //        $this->usePageBreaks = true;
    //    }

    public function isPageBreaksEnabled(): bool
    {
        return $this->usePageBreaks;
    }

    public function disablePageBreaks(): void
    {
        $this->usePageBreaks = false;
    }

    /**
     * check whether showing form as (tab) views with nex/previous buttons is enabled and possible.
     *
     * NOTE: use @see AbstractJsonAwareTemplate::hasCurrentVersion() before calling this method!
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     * @throws MissingVersionException       if there is no current version
     * @throws StepException
     * @throws StepNotificationException
     * @throws VersionException
     */
    public function usesPageBreaksInCurrentVersion(): bool
    {
        return $this->isPageBreaksEnabled() && $this->getCurrentVersion()->getFormList()->canShowCategoriesAsPageBreaks();
    }

    public function isReadOnlyChoiceAnswerOptionsTogglingEnabled(): bool
    {
        return $this->enableReadOnlyChoiceAnswerOptionsToggling;
    }

    public function disableReadOnlyChoiceAnswerOptionsToggling(): void
    {
        $this->enableReadOnlyChoiceAnswerOptionsToggling = false;
    }

    public function enableReadOnlyChoiceAnswerOptionsToggling(): void
    {
        $this->enableReadOnlyChoiceAnswerOptionsToggling = true;
    }

    public function isAutoTemporarySavingEnabled(): bool
    {
        return $this->enableAutoTemporarySaving;
    }

    public function disableAutoTemporarySaving(): void
    {
        $this->enableAutoTemporarySaving = false;
    }

    public function enableAutoTemporarySaving(): void
    {
        $this->enableAutoTemporarySaving = true;
    }

    public function isShowPositionNumbers(): bool
    {
        return $this->showPositionNumbers;
    }

    public function setShowPositionNumbers(bool $showPositionNumbers): void
    {
        $this->showPositionNumbers = $showPositionNumbers;
    }

    /**
     * clears all locally cached elements.
     *
     * WARNING: intentional null assigning here to allow to rebuild cache based on instance checks!
     */
    protected function clearAllLocalCaches(): void
    {
        $this->cachedRolesThatParticipateInCurrentVersion = null;
        $this->cachedRolesWithDisabledAutoTemporarySave = null;
        $this->cachedHierarchicalRolesThatCouldParticipate = null;
        $this->cachedRolesForAutoQuestionWriteAccessInAdminSide = null;
        $this->cachedRolesAllowedToExport = null;
        $this->cachedWorkingVersion = null;
        $this->cachedCurrentVersion = null;
        $this->cachedPreviousVersions = null;
    }
}
