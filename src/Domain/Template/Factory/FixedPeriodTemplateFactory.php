<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Factory;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Exception\TemplateException;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Entity\Template;
use Ramsey\Uuid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * A factory to quickly create a template for a form.
 *
 * This factory creates a template for a form with fixed periods, with each period directly followed
 * by the next period.
 */
final class FixedPeriodTemplateFactory
{
    public const STEP_UUID_NAMESPACE = 'fd199db3-bfa4-485e-80ec-b358c8e4fdfc';

    private readonly string $defaultLocale;

    public function __construct(
        string $defaultLocale,
        private readonly TranslatorInterface $translator,
    ) {
        if ('' === $defaultLocale) {
            throw new \InvalidArgumentException('Default locale was not provided.');
        }

        $this->defaultLocale = $defaultLocale;
    }

    /**
     * Creates a template for a form.
     *
     * The $stepDates are supposed to be sorted. They indicate when a step ends and the next one begins.
     *
     * @param \DateTimeImmutable[] $stepDates
     * @param string[]             $relevantRoles           names of the roles that are relevant for this form
     * @param int[]                $hideReadOnlyStepIndexes indexes (starting from 1) of steps that should hide read-only nodes
     *
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     * @throws TemplateException
     */
    public function createTemplate(
        string $templateUid,
        string $formName,
        string $targetingType,
        array $stepDates,
        array $relevantRoles,
        array $hideReadOnlyStepIndexes = [],
        string $extraLocale = '',
    ): Template {
        $formNames = [
            $this->defaultLocale => $formName,
        ];

        if ('' !== $extraLocale && $extraLocale !== $this->defaultLocale) {
            $formNames = array_merge($formNames, [$extraLocale => $formName]);
        }

        if (count($stepDates) < 2) {
            throw new \InvalidArgumentException('Need at least two dates to create a form step.');
        }

        $formInfo = FormInfo::fromLocalizedStrings(
            LocalizedRequiredString::fromLocaleTranslations($formNames),
            LocalizedOptionalString::createEmpty(),
            RoleLocalizedOptionalStringCollection::createEmpty()
        );

        $steps = [];
        $stepStartDate = null;

        // Create the steps.
        // (Maybe I should move this for-loop to another factory)
        // I don't know if the form-bundle should support steps that don't end.
        // If so, we could allow null as the last value in the $stepDates array.

        // Using array_values in the foreach causes the steps to be numbered 1, 2, 3, ...
        foreach (array_values($stepDates) as $dateIndex => $stepEndDate) {
            if (null === $stepStartDate) {
                $stepStartDate = $stepEndDate;
                continue;
            }

            $stepNames = [
                $this->defaultLocale => $this->translator->trans('adminManage.general.step', [], 'kulFormAdminManage', $this->defaultLocale).' '.((string) $dateIndex),
            ];

            if ('' !== $extraLocale && $extraLocale !== $this->defaultLocale) {
                $stepNames = array_merge(
                    $stepNames,
                    [$extraLocale => $this->translator->trans('adminManage.general.step', [], 'kulFormAdminManage', $extraLocale).' '.((string) $dateIndex)]
                );
            }

            $steps[] = StepWithFixedDateStart::fromUidAndDependencies(
                Uuid::uuid5(self::STEP_UUID_NAMESPACE, $templateUid.(string) $dateIndex)->toString(),
                StepFixedPeriod::fromDates(
                    $stepStartDate,
                    $stepEndDate
                ),
                false,
                RoleCollection::createEmpty(),
                LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                    LocalizedRequiredString::fromLocaleTranslations($stepNames),
                    LocalizedOptionalString::createEmpty(),
                    RoleLocalizedOptionalStringCollection::createEmpty()
                ),
                StepSubmitSuccessUserInteraction::createEmptyWithDefaults(),
                in_array($dateIndex, $hideReadOnlyStepIndexes, true),
                []
            );

            $stepStartDate = $stepEndDate;
        }

        $workflow = WorkFlow::fromSteps(
            new StepWithFixedDateStartCollection($steps)
        );

        $workingVersion = WorkingVersion::createWithEmptyFormListAndEmptyFixedStepsWorkFlowAndNoInfo()
            ->withInfo($formInfo)
            ->withWorkFlow($workflow)
            ->withFormList(FormList::createWithEmptyRootNode());

        return new Template(
            $templateUid,
            $targetingType,
            $workingVersion,
            RoleCollection::fromRoleNames($relevantRoles),
            // FIXME: build a better system to grant permissions to roles.
            RoleCollection::createEmpty(),
            RoleCollection::createEmpty(),
            RoleCollection::createEmpty(),
            RoleCollection::createEmpty(),
            // FIXME: I don't know what this is:
            false
        );
    }
}
