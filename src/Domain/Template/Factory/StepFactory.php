<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Factory;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;

/**
 * Class StepFactory.
 *
 * allows to create new steps and convert existing steps to a different type and/or update the step to a new
 * position (i.e. update the preceding step)
 */
class StepFactory
{
    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public static function createFromSerializedArray(array $data): StepInterface
    {
        $type = $data[StepInterface::KEY_TYPE];

        return match ($type) {
            StepWithCalculatedDateStart::getType() => StepWithCalculatedDateStart::fromJsonDecodedArray($data),
            StepWithFixedDateStart::getType() => StepWithFixedDateStart::fromJsonDecodedArray($data),
            default => throw new StepException('unknown step type ['.$type.']'),
        };
    }
}
