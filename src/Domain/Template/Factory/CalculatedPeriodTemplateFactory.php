<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Factory;

use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Entity\Template;
use Ramsey\Uuid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Factory for template with calculated steps.
 *
 * This is more or less a copy of {@see FixedPeriodTemplateFactory}.
 */
final class CalculatedPeriodTemplateFactory
{
    public const STEP_UUID_NAMESPACE = '8bf82756-36a1-4b6f-b1cb-4edec8a05079';

    private readonly string $defaultLocale;

    public function __construct(
        string $defaultLocale,
        private readonly TranslatorInterface $translator,
    ) {
        if ('' === $defaultLocale) {
            throw new \InvalidArgumentException('Default locale was not provided.');
        }

        $this->defaultLocale = $defaultLocale;
    }

    /**
     * @param int[]    $stepDays
     * @param string[] $relevantRoles names of the roles that are relevant for this form
     */
    public function createTemplate(
        string $templateUid,
        string $formName,
        string $targetingType,
        array $stepDays,
        array $relevantRoles,
    ): Template {
        if (count($stepDays) < 1) {
            throw new \InvalidArgumentException('Need at least a number of days for one step.');
        }

        $formInfo = FormInfo::fromLocalizedStrings(
            LocalizedRequiredString::fromLocaleTranslations([
                $this->defaultLocale => $formName,
            ]),
            LocalizedOptionalString::createEmpty(),
            RoleLocalizedOptionalStringCollection::createEmpty()
        );

        $steps = [];
        $endDay = 0;

        // Create the steps.
        /** @var int $days */
        foreach (array_values($stepDays) as $index => $days) {
            $startDay = $endDay;
            $endDay += $days;

            $steps[] = StepWithCalculatedDateStart::fromUidAndDependencies(
                Uuid::uuid5(self::STEP_UUID_NAMESPACE, $templateUid.(string) $index)->toString(),
                StepCalculatedPeriod::fromCalculatingAmounts($startDay, $endDay),
                false,
                RoleCollection::createEmpty(),
                LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                    LocalizedRequiredString::fromLocaleTranslations([
                        $this->defaultLocale => $this->translator->trans('adminManage.general.step', [], 'kulFormAdminManage', $this->defaultLocale).' '.((string) $index),
                    ]),
                    LocalizedOptionalString::createEmpty(),
                    RoleLocalizedOptionalStringCollection::createEmpty()
                ),
                StepSubmitSuccessUserInteraction::createEmptyWithDefaults(),
                false,
                []
            );
        }

        $workflow = WorkFlow::fromSteps(
            new StepWithCalculatedDateStartCollection($steps)
        );

        $workingVersion = WorkingVersion::createWithEmptyFormListAndEmptyFixedStepsWorkFlowAndNoInfo()
            ->withInfo($formInfo)
            ->withWorkFlow($workflow)
            ->withFormList(FormList::createWithEmptyRootNode());

        return new Template(
            $templateUid,
            $targetingType,
            $workingVersion,
            RoleCollection::fromRoleNames($relevantRoles),
            // FIXME: build a better system to grant permissions to roles.
            RoleCollection::createEmpty(),
            RoleCollection::createEmpty(),
            RoleCollection::createEmpty(),
            RoleCollection::createEmpty(),
            // FIXME: I don't know what this is:
            false
        );
    }
}
