<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Exception\VersionException;
use KUL\FormBundle\Domain\Template\Utility\Collection\PublishedVersionCollection;
use KUL\FormBundle\Domain\Template\Version\Validator\WorkingVersionValidator;

/**
 * Class PublishedVersion.
 *
 * published version of the configuration of the elements needed to make a @see TemplateInterface
 *
 * IMPORTANT: validating if a version is fully valid inside a template is heavy on performance. So once a published
 * version is constructed, it is considered fully valid.
 * a new published version object is always build form a @see WorkingVersion
 * inside a template @see TemplateInterface::publishNewCurrentVersion()
 * via this object's method @see PublishedVersion::createForTemplateWithIncrementedVersionNumber()
 * which validates and asserts if the workingVersion can be used to make a new published version via @see WorkingVersionValidator
 * inside the template, the published version is then JSON serialized and every de-serializing
 * via @see PublishedVersion::fromJsonDecodedArray() from inside the template.
 * In short: a new published version instance is created once with asserted validation inside a template,
 * then serialized once and stored (in DB or..), and from then is always rebuild from the serialized data with the
 * assumption it is valid.
 * when a new published version is made as the new 'current' published version inside a template, the existing
 * published version, if any, is added to the @see TemplateInterface::getPreviouslyPublishedVersions() . retrieval of
 * previous published version happens in the same manner as tyhe current one: via de-serializing with assumption it is valid
 *
 * the trade-off here to not re-validate on every de-serialize, is to gain performance, knowing that there is only one
 * point where a published version is made and the only way to invalidate one is by deliberate tampering in DB with JSON data!
 * however, because admins do not want to follow protocol and publish a new version for something small like e.g. a spelling
 * error in e.g. a translation for the label in @see PublishedVersion::getInfo(), there will be override methods here
 * that alter the published version without full validation! those methods will be very very strict in what is changeable
 * and will/should never allow to change anything that touches access rights, flow, question configs, score calculations, etc..
 * TODO DV admin side: if needed, temporally can make a working version from an published version to re-validate or create a seperate published verion validator!
 *
 * @see     PublishedVersionCollection
 */
final class PublishedVersion extends AbstractVersion
{
    /** @var string */
    public const KEY_VERSION_NUMBER = 'versionNumber';

    private readonly int $versionNumber;

    private function __construct(int $versionNumber, FormInfo $info, WorkFlow $workFlow, FormList $formList)
    {
        if ($versionNumber < TemplateInterface::VERSION_INITIAL_NUMBER) {
            throw new \InvalidArgumentException('version number must be an numeric value corresponding to an integer greater than or equal to '.((string) TemplateInterface::VERSION_INITIAL_NUMBER));
        }

        $this->info = $info;
        $this->workFlow = $workFlow;
        $this->formList = $formList;
        $this->versionNumber = $versionNumber;
    }

    /**
     * takes the WorkingVersion in the given template, validates if and asserts that this workingVersion can be
     * published to create a new publishedVersion with a new version number, based on the latest versionNumber
     * (if any) known in the template, so that it can be used as the new @see TemplateInterface::getCurrentVersion().
     *
     * @see TemplateInterface::publishNewCurrentVersion()
     *
     * @throws VersionException
     * @throws MissingVersionException
     */
    public static function createForTemplateWithIncrementedVersionNumber(TemplateInterface $template): self
    {
        $versionNumber = TemplateInterface::VERSION_INITIAL_NUMBER;
        if ($template->hasCurrentVersion()) {
            $versionNumber = $template->getCurrentVersionNumber() + 1;
        }

        return self::createForTemplateWithVersionNumber($template, $versionNumber);
    }

    public static function createForTemplateWithExistingCurrentVersionNumber(TemplateInterface $template): self
    {
        if (!$template->hasCurrentVersion()) {
            throw new MissingVersionException('cannot use current version number: no current version found.');
        }

        return self::createForTemplateWithVersionNumber($template, $template->getCurrentVersionNumber());
    }

    private static function createForTemplateWithVersionNumber(TemplateInterface $template, int $versionNumber): self
    {
        $validator = WorkingVersionValidator::fromWorkingVersionAndParticipatableRoles(
            $template->getWorkingVersion(),
            $template->getHierarchicalRolesThatCouldParticipate()
        );

        if (!$validator->allowsPublish()) {
            throw new VersionException('can not create published version for Template with id ['.$template->getId().'] based on the working version. the following issues in the working version are blocking the publish: '.implode(' ### ', $validator->getErrorMessages()->getMessages()));
        }

        $workingVersion = $template->getWorkingVersion();

        return new self(
            $versionNumber,
            $workingVersion->getInfo(),
            $workingVersion->getWorkFlow(),
            $workingVersion->getFormList()
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     * @throws InputInvalidArgumentException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $versionNumber = (int) $data[self::KEY_VERSION_NUMBER];
        /** @var FormInfo $formInfo */
        $formInfo = FormInfo::fromJsonDecodedArray($data[self::KEY_INFO]);
        $workFlow = WorkFlow::fromJsonDecodedArray($data[self::KEY_WORKFLOW]);
        $formList = FormList::fromJsonDecodedArray($data[self::KEY_FORM_LIST]);

        return new self($versionNumber, $formInfo, $workFlow, $formList);
    }

    public function getVersionNumber(): int
    {
        return $this->versionNumber;
    }

    /**
     * sometimes we need to extract a working version from this published version. e.g. when restoring a
     * template to a previous published version or when changing something on the template. in most cases
     * that requires a re-validation of that published version, and for that it, needs to become a workingVersion.
     */
    public function toWorkingVersion(): WorkingVersion
    {
        return WorkingVersion::fromElements($this->getInfo(), $this->getWorkFlow(), $this->getFormList());
    }

    public function getLabel(?string $locale): string
    {
        if (!is_string($locale)) {
            return $this->getInfo()->getLocalizedLabel()->getFallback();
        }

        return $this->getInfo()->getLabel($locale);
    }

    /**
     * @returns array<string, string>
     */
    public function getLocalizedLabels(): array
    {
        return $this->getInfo()->getLocalizedLabel()->getLocaleStrings();
    }

    /**
     * @return array{versionNumber: int, info: FormInfo, workFlow: WorkFlow, formList: FormList}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_VERSION_NUMBER => $this->getVersionNumber(),
            self::KEY_INFO => $this->getInfo(),
            self::KEY_WORKFLOW => $this->getWorkFlow(),
            self::KEY_FORM_LIST => $this->getFormList(),
        ];
    }

    public function withInfo(FormInfo $info): self
    {
        return new self(
            $this->getVersionNumber(),
            $info,
            $this->getWorkFlow(),
            $this->getFormList(),
        );
    }
}
