<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Version\Validator\WorkingVersionValidator;

/**
 * Class WorkingVersion.
 *
 * working version of the configuration of the elements needed to make a @see TemplateInterface
 *
 * NOTE: the working version does not have to be fully valid (allowed to have missing elements
 * or incomplete / non-publishable elements) until it is used to create a new valid @see PublishedVersion
 * via @see TemplateInterface::publishNewCurrentVersion()
 * on which time it is validated using @see WorkingVersionValidator
 */
final class WorkingVersion extends AbstractVersion
{
    private function __construct(FormList $formList, ?WorkFlow $workFlow, ?FormInfo $info)
    {
        $this->info = $info;
        $this->workFlow = $workFlow;
        $this->formList = $formList;
    }

    public static function fromElements(?FormInfo $info, ?WorkFlow $workFlow, ?FormList $formList): self
    {
        if ($formList instanceof FormList) {
            return new self($formList, $workFlow, $info);
        }

        return new self(FormList::createWithEmptyRootNode(), $workFlow, $info);
    }

    public static function createWithEmptyFormListAndEmptyCalculatedStepsWorkFlowAndNoInfo(): self
    {
        return new self(
            FormList::createWithEmptyRootNode(),
            WorkFlow::createEmptyForCalculatedDatesSteps(),
            null
        );
    }

    public static function createWithEmptyFormListAndEmptyFixedStepsWorkFlowAndNoInfo(): self
    {
        return new self(
            FormList::createWithEmptyRootNode(),
            WorkFlow::createEmptyForFixedDatesSteps(),
            null
        );
    }

    public static function createWithEmptyFormListAndEmptyMixedStepsWorkFlowAndNoInfo(): self
    {
        return new self(
            FormList::createWithEmptyRootNode(),
            WorkFlow::createEmptyForMixedCalculatedAndFixedDatesSteps(),
            null
        );
    }

    public function isEmpty(): bool
    {
        return !$this->hasInfo()
            && $this->getFormList()->isEmpty()
            && (!$this->hasWorkFlow() || !$this->getWorkFlow()->hasSteps());
    }

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     * @throws InputInvalidArgumentException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        /** @var FormInfo|null $formInfo */
        $formInfo = isset($data[self::KEY_INFO]) ? FormInfo::fromJsonDecodedArray($data[self::KEY_INFO]) : null;
        $workFlow = isset($data[self::KEY_WORKFLOW]) ? WorkFlow::fromJsonDecodedArray($data[self::KEY_WORKFLOW]) : null;
        $formList = isset($data[self::KEY_FORM_LIST]) ? FormList::fromJsonDecodedArray($data[self::KEY_FORM_LIST]) : null;

        if (!$formList instanceof FormList) {
            throw new \InvalidArgumentException('Form list missing');
        }

        return new self($formList, $workFlow, $formInfo);
    }

    public function hasInfo(): bool
    {
        return $this->info instanceof FormInfo;
    }

    public function withInfo(?FormInfo $info): self
    {
        return self::fromElements(
            $info,
            $this->hasWorkFlow() ? $this->getWorkFlow() : null,
            $this->getFormList()
        );
    }

    public function hasWorkFlow(): bool
    {
        return $this->workFlow instanceof WorkFlow;
    }

    public function withWorkFlow(WorkFlow $workFlow): self
    {
        return self::fromElements(
            $this->hasInfo() ? $this->getInfo() : null,
            $workFlow,
            $this->getFormList()
        );
    }

    public function withFormList(FormList $formList): self
    {
        return self::fromElements(
            $this->hasInfo() ? $this->getInfo() : null,
            $this->hasWorkFlow() ? $this->getWorkFlow() : null,
            $formList
        );
    }

    /**
     * @return array{info: (FormInfo|null), workFlow: (WorkFlow|null), formList: FormList}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_INFO => $this->hasInfo() ? $this->getInfo() : null,
            self::KEY_WORKFLOW => $this->hasWorkFlow() ? $this->getWorkFlow() : null,
            self::KEY_FORM_LIST => $this->getFormList(),
        ];
    }

    public function withEarlyStartingStep(string $stepUid): self
    {
        $currentSteps = $this->getWorkFlow()->getSteps();
        $updatedStep = $currentSteps->getOneStepByUid($stepUid)->withPreviousStepFulFilledStartOption();

        return $this->withWorkFlow(
            WorkFlow::fromSteps(
                $this->getWorkFlow()->getSteps()->withUpdatedStep($updatedStep)
            )
        );
    }
}
