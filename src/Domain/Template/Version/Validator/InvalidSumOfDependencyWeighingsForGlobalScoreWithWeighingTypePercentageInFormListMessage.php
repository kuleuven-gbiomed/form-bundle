<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

class InvalidSumOfDependencyWeighingsForGlobalScoreWithWeighingTypePercentageInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidSumOfDependencyWeighingsForGlobalScoreWithWeighingTypePercentage';

    public static function createMessageForChoiceGlobalScoreNode(ScorableInputNode $globalScoreNode): self
    {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        $globalScoreUid = $globalScoreNode->getUid();
        Assert::false(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->isEmpty(),
            ' can not build message '.self::class
            ."; the given node [$globalScoreQuestionLabel][$globalScoreUid] is not configured as a global score node (no dependency configurations found)"
        );

        Assert::true(
            OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE === $globalScoreNode->getGlobalScoreWeighingType(),
            ' can not build message '.self::class
            .'; the choice global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] does not use weighingType "percentage" !'
        );

        Assert::false(
            $globalScoreNode->hasValidSumOfDependencyWeighingsForGlobalScoring(),
            ' can not build message '.self::class
            .'; the choice global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] has a valid sum of 100 % for its dependency weighings !'
        );

        $invalidWeighingSumString = (string) $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->getSumOfWeighings();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the question ['.$globalScoreQuestionLabel.'] is configured as a global score question and its calculation is configured to use the percentual global scoring weighing type to spread the weight of answers given for each of its dependency questions when calculating the resulting global score: Thus the weighing you defined on each dependency question refers to a percentage of the total weighing of 100%, but the sum of the weighings is ['.$invalidWeighingSumString.'] instead of 100! Please make sure the sum of the weighings for the dependencies is exactly 100 or change the weighing type for calculation on this global score question to the absolute weighing type instead of the percentual weighing type.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%invalidWeighingSum%' => $invalidWeighingSumString,
            ]
        );
    }
}
