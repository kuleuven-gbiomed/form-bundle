<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use Webmozart\Assert\Assert;

class MissingOptionScoreValuesForChoiceGlobalScoreInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.missingOptionScoreValuesForChoiceGlobalScore';

    public static function createMessageForChoiceGlobalScoreNode(ChoiceInputNode $globalScoreNode): self
    {
        $locale = $globalScoreNode->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        $globalScoreUid = $globalScoreNode->getUid();
        Assert::false(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->isEmpty(),
            ' can not build message '.self::class
            ."; the given node [$globalScoreQuestionLabel][$globalScoreUid] is not configured as a global score node (no dependency configurations found)"
        );

        Assert::false(
            $globalScoreNode->getChildren()->getHavingNoScoringValue()->isEmpty(),
            ' can not build message '.self::class
            .'; the choice global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] does not has any child option nodes with missing score value!'
        );

        $invalidOptionLabels = implode(
            ' | ',
            $globalScoreNode->getChildren()->getHavingNoScoringValue()->getLabelsArray($locale)
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the choice question ['.$globalScoreQuestionLabel.'] is configured as a global score question but it has answer options that have no score value defined. All answer options must have a numeric score value if the choice question is to be used for global scoring calculations. Please remove those answer Options or provide a score value for those options. those invalid answer options are: ['.$invalidOptionLabels.'].';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%invalidOptionLabels%' => $invalidOptionLabels,
            ]
        );
    }
}
