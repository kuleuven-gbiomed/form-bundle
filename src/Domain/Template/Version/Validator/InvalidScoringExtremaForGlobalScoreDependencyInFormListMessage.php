<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class InvalidScoringExtremaForGlobalScoreDependencyInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidScoringExtremaForGlobalScoreDependency';

    public static function createMessageForGlobalScoreNodeAndDependencyScoreNode(
        ScorableInputNode $globalScoreNode,
        ScorableInputNode $dependencyScoreNode,
    ): self {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();
        $scoreQuestionLabel = $dependencyScoreNode->getNestedLabelForFallBackLocale();

        Assert::true(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->hasOneByNodeUid($dependencyScoreNode->getUid()),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyScoreNode->getUid()
            .'] is not referenced as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        $scoringParameters = $dependencyScoreNode->getInputScoringParameters();

        Assert::true(
            $scoringParameters->hasScoringExtrema(),
            ' can not build message '.self::class
            .'; the extrema scoring parameters needed to make scoring possible for score node ['
            .$scoreQuestionLabel.']['.$dependencyScoreNode->getUid().'] as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] are missing!'
        );

        Assert::false(
            $scoringParameters->hasValidScoringExtrema(),
            ' can not build message '.self::class
            .'; the extrema scoring parameters needed to make scoring possible for score node ['
            .$scoreQuestionLabel.']['.$dependencyScoreNode->getUid().'] as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] are not invalid!'
        );

        $minScore = (string) $scoringParameters->getMinScore();
        $maxScore = (string) $scoringParameters->getMaxScore();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the score question [$scoreQuestionLabel] is configured as a calculation dependency for the global score question [$globalScoreQuestionLabel]. but its maximum score [$maxScore] and minimum score [$minScore] are not valid: the maximum score must be greater than the minimum score. Please make sure the maximum score is greater than the minimum score. Please make sure to provide a value for each or, alternatively, remove the question as dependency for the global score node.";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $scoreQuestionLabel,
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%minScore%' => $minScore,
                '%maxScore%' => $maxScore,
            ]
        );
    }
}
