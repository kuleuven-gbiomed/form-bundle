<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart\ParticipationQuestionEligibility;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;

class MissingAnswerOptionScoringValuesOnParticipatingChoiceQuestionForRadarChartNodeInFormListMessage extends InvalidFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidRadarChartNodeInFormList.missingAnswerOptionScoringValuesOnParticipatingChoiceQuestionForRadarChartNode';

    public static function createMessageForRadarChartAndParticipationEligibility(
        RadarChartViewNode $chartViewNode,
        ParticipationQuestionEligibility $questionEligibility,
    ): self {
        $locale = $chartViewNode->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $chartViewNodeLabel = $chartViewNode->getNestedLabel($locale);
        $participatingChoiceQuestion = $questionEligibility->getQuestion();

        if (!$participatingChoiceQuestion instanceof ChoiceInputNode) {
            throw new \InvalidArgumentException(' can not build message '.self::class.'; the ParticipationQuestionEligibility [questionUid: '.$questionEligibility->getReferencedQuestionUid().'] does refer to a choice question for radarChartNode ['.$chartViewNodeLabel.']!');
        }

        if (!$questionEligibility->hasChoiceQuestionAnswerOptionsWithMissingScoreValues()) {
            throw new \InvalidArgumentException(' can not build message '.self::class.' for radarChartNode ['.$chartViewNodeLabel.']: the given ParticipationQuestionEligibility [questionUid: '.$questionEligibility->getReferencedQuestionUid().'] does refer to a participating choice question with all its answer options having valid scoring values!');
        }

        $invalidOptionLabels = implode(
            ' | ',
            $questionEligibility->getChoiceQuestionAnswerOptionsWithMissingScoreValues()->getLabelsArray($locale)
        );

        $participatingQuestion = $questionEligibility->getQuestion();
        $participatingQuestionLabel = $participatingQuestion->hasNestedLabel($locale) ? $participatingQuestion->getNestedLabel($locale) : 'unknown label';

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': The list of participating questions for the radar chart node ['.$chartViewNodeLabel.'] refers to a choice question ['.$participatingQuestionLabel.'] which is not eligible as participating choice question in radar charts: one or more of its answer options is missing the required scoring value to make the whole choice question plottable in the radar chart; those answer options with missing scoring values are: ['.$invalidOptionLabels.']';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%chartViewNodeLabel%' => $chartViewNodeLabel,
                '%participatingQuestionLabel%' => $participatingQuestionLabel,
                '%invalidOptionLabels%' => $invalidOptionLabels,
            ]
        );
    }
}
