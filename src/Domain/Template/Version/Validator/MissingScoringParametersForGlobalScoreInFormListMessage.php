<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class MissingScoringParametersForGlobalScoreInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.missingScoringParametersForGlobalScore';

    public static function createMessageForGlobalScoreNode(ScorableInputNode $globalScoreNode): self
    {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        $globalScoreUid = $globalScoreNode->getUid();
        Assert::false(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->isEmpty(),
            ' can not build message '.self::class
            ."; the given node [$globalScoreQuestionLabel][$globalScoreUid] is not configured as a global score node (no dependency configurations found)"
        );

        Assert::false(
            $globalScoreNode->getInputScoringParameters()->makesScoringPossible(),
            ' can not build message '.self::class
            .'; the scoring parameters needed to make scoring possible for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] are not missing!'
        );

        $scoringParameterLabels = implode(
            ' | ',
            self::getMissingScoringParameterLabels($globalScoreNode->getInputScoringParameters())
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the question ['.$globalScoreQuestionLabel.'] is configured as a global score question but it misses the following crucial parameters that are needed for its global score calculation to work: ['.$scoringParameterLabels.'] . Please make sure to provide a value for each or, alternatively, demote this global score question to a normal question by removing its dependencies on the questions it depends on for its calculation.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%scoringParameterLabels%' => $scoringParameterLabels,
            ]
        );
    }
}
