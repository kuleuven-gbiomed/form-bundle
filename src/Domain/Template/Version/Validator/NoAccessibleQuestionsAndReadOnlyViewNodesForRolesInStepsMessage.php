<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

final class NoAccessibleQuestionsAndReadOnlyViewNodesForRolesInStepsMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.NoAccessibleQuestionsAndReadOnlyViewNodesForRolesInSteps';

    /**
     * @param StepInterface[] $steps
     *
     * @throws LocalizedStringException
     */
    public static function createMessageForStepsAndRoles(
        array $steps,
        RoleCollection $roles,
    ): self {
        $stepLabelsString = implode(' | ', array_map(fn (StepInterface $step) =>
            // a step always has a label asserted for at least one locale (i.e. fallback, usually default locale)
            $step->getInfo()->getLocalizedLabel()->getFallback(), $steps));

        $rolesString = implode(' | ', array_map(fn (Role $role) => $role->getName(), $roles->toArray()));

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': Geen enkele vraag heeft leesrechten'
            .' voor de volgende stappen:  ['.$stepLabelsString.'].'
            .' Zorg ervoor dat voor iedere stap in de workflow minstens één vraag leesrechten heeft voor minstens één rol.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%stepLabels%' => $stepLabelsString, '%roles%' => $rolesString]
        );
    }
}
