<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

class InvalidWeighedZeroMaxScoreForGlobalScoreDependencyInFormList extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidWeighedZeroMaxScoreForGlobalScoreDependency';

    public static function createMessageForGlobalScoreNodeAndDependencyScoreNode(
        ScorableInputNode $globalScoreNode,
        ScorableInputNode $dependencyScoreNode,
    ): self {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();
        $scoreQuestionLabel = $dependencyScoreNode->getNestedLabelForFallBackLocale();

        Assert::true(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->hasOneByNodeUid($dependencyScoreNode->getUid()),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyScoreNode->getUid()
            .'] is not referenced as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        Assert::true(
            $dependencyScoreNode->getInputScoringParameters()->hasMaxScore(),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyScoreNode->getUid()
            .'] as a dependency for global score node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] has no max score set!'
        );

        $maxScore = $dependencyScoreNode->getInputScoringParameters()->getMaxScore();

        Assert::true(
            0.0 === $maxScore,
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyScoreNode->getUid()
            .'] as a dependency for global score node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] does not have a max score of zero, got ['.((string) $maxScore).']!'
        );

        $weighing = $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()
            ->getOneByNodeUid($dependencyScoreNode->getUid())->getWeighing();

        Assert::false(
            $maxScore === $weighing,
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyScoreNode->getUid()
            .'] as a dependency for global score node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] has a max score of zero, but it equals the weighing!'
        );

        $weighingString = (string) $weighing;

        if (ScorableInputNode::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE === $globalScoreNode->getGlobalScoreWeighingType()) {
            $weighingString .= '%';
        }

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the score question ['.$scoreQuestionLabel.'] is configured as a calculation dependency for the global score question ['.$globalScoreQuestionLabel.'] with a weighing of ['.$weighingString.'] for the weighted calculation of the global score. but it is also configured to allow a maximum score of 0.0 (zero), which is not allowed when the weighing differs from the maximum score: Please change the maximum score to any other number than zero or alternatively change the weighing to be 0.0 (zero) as well';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $scoreQuestionLabel,
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%weighing%' => $weighingString,
            ]
        );
    }
}
