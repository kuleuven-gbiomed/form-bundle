<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use Webmozart\Assert\Assert;

class InvalidAmountOfOptionsWithAdditionalTextInputForChoiceInFormListMessage extends InvalidFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidAmountOfOptionsWithAdditionalTextInputForChoiceInFormListMessage';

    public static function createMessageForDropdownChoiceNode(ChoiceInputNode $dropdownChoiceInputNode): self
    {
        $label = $dropdownChoiceInputNode->getLabelForFallBackLocale();
        $uid = $dropdownChoiceInputNode->getUid();

        $otherOptions = $dropdownChoiceInputNode->getChildren()->getUsingAdditionalTextInput();

        Assert::greaterThan(
            $otherOptions->count(),
            1,
            ' can not build message '.self::class
            .'; the choice node ['.$label.']['.$uid.'] does not exceed the maximum of 1 other option with additional text!'
        );

        $otherOptionsString = (string) $otherOptions->count();
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the choice question ['.$label.'] has ['.$otherOptionsString."] answer options which allow users to add additional text input when opting for these answer option(a.k.a. 'other' options). But a choice question can have only one answer option allowing users to add additional text input. Rreduce the amount of those answer options allowing for additional text input to one or zero.";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $label,
                '%amountOfOtherOptions%' => $otherOptionsString,
            ]
        );
    }
}
