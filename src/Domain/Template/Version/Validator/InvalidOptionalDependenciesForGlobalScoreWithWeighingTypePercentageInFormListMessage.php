<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class InvalidOptionalDependenciesForGlobalScoreWithWeighingTypePercentageInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidOptionalDependenciesForGlobalScoreWithWeighingTypePercentageInFormListMessage';

    public static function createMessageForGlobalScoreNode(ScorableInputNode $globalScoreNode): self
    {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        // get all dependencies (= score nodes this global score node depends on)
        $dependencyQuestions = $globalScoreNode->getAllGlobalScoreCalculationScoreNodeConfigs()->getScoreNodes();

        Assert::true(
            OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE === $globalScoreNode->getGlobalScoreWeighingType(),
            ' can not build message '.self::class
            .'; the node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] is not using "'.OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE.'" as weighingType!'
        );

        Assert::false(
            $dependencyQuestions->isEmpty(),
            ' can not build message '.self::class
            .'; the node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] is not operating as global score question; it has no scoring questions on which it depends!'
        );

        Assert::false(
            $dependencyQuestions->getOptional()->isEmpty(),
            ' can not build message '.self::class
            .'; the node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] has no optional dependencies!'
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the question ['.$globalScoreQuestionLabel.'] is configured as a global score question with percentage weighingType.'
            .' But at least one scoring questions on which this global score question depends is optional.'
            .' a global score question with percentage weighingType can not have optional questions as dependencies.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%globalQuestionUid%' => $globalScoreNode->getUid(),
            ]
        );
    }
}
