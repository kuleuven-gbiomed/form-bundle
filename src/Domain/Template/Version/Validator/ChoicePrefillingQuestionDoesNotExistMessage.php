<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;

final class ChoicePrefillingQuestionDoesNotExistMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.ChoicePrefillingQuestionDoesNotExist';

    public static function createForPrefillableQuestion(
        ChoiceInputNode $prefillableQuestion,
        string $prefillingQuestionUid,
    ): self {
        $uid = $prefillableQuestion->getUid();
        $label = $prefillableQuestion->getLabelForFallBackLocale();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': you want to prefill the question with uid '.$uid
            .' and label "'.$label.'"'
            .', with another question with uid '.$prefillingQuestionUid
            .', but it does not exist in the form.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%label%' => $label, '%uid%' => $uid, '%prefillingQuestionUid%']
        );
    }
}
