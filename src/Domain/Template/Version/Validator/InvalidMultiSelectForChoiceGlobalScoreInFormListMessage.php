<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use Webmozart\Assert\Assert;

final class InvalidMultiSelectForChoiceGlobalScoreInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidMultiSelectForChoiceGlobalScore';

    public static function createMessageForChoiceGlobalScoreNode(ChoiceInputNode $globalScoreNode): self
    {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        Assert::true(
            $globalScoreNode->getInput()->isMultiple(),
            ' can not build message '.self::class
            .'; the choice global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] is not configured as multiple!'
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the choice question ['.$globalScoreQuestionLabel.'] is configured as a global score question but it is configured to allow multiple answers to be given by users, which is not allowed when the question is to be used for global scoring calculations.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%globalQuestionLabel%' => $globalScoreQuestionLabel]
        );
    }
}
