<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class InvalidZeroMaxScoreForGlobalScoreInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidZeroMaxScoreForGlobalScore';

    public static function createMessageForGlobalScoreNode(ScorableInputNode $globalScoreNode): self
    {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        $globalScoreUid = $globalScoreNode->getUid();
        Assert::false(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->isEmpty(),
            ' can not build message '.self::class
            ."; the given node [$globalScoreQuestionLabel][$globalScoreUid] is not configured as a global score node (no dependency configurations found)"
        );

        $scoringParameters = $globalScoreNode->getInputScoringParameters();
        Assert::true(
            $scoringParameters->hasMaxScore(),
            ' can not build message '.self::class
            .'; maximum score in scoring parameters for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] is not set!'
        );

        Assert::true(
            0.0 === $scoringParameters->getMaxScore(),
            ' can not build message '.self::class
            .'; the maximum score in scoring parameters for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] does not equals to 0.0 (zero)!'
        );

        $maxScore = (string) $scoringParameters->getMaxScore();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the question [$globalScoreQuestionLabel] is configured as a global score question. but the maximum score [$maxScore] configured on this global score question equals to 0 (zero): Due to calculation limitations, the maximum score is not allowed to be zero. Please make sure the max score is less or greater than zero. alternatively, you can demote this global score question to a normal question by removing its dependencies on the questions it depends on for its calculation.";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%maxScore%' => $maxScore,
            ]
        );
    }
}
