<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @see     InvalidPublishMessage
 *
 * @extends ArrayCollection<array-key,InvalidPublishMessage>
 */
class InvalidPublishMessageCollection extends ArrayCollection
{
    /**
     * @return string[]
     */
    public function getMessages(?TranslatorInterface $translator = null): array
    {
        $messages = [];

        /** @var InvalidPublishMessage $invalidPublishMessage */
        foreach ($this->getIterator() as $invalidPublishMessage) {
            $messages[] = $invalidPublishMessage->getMessage($translator);
        }

        return $messages;
    }

    /**
     * Creates a new collection based on the current one and merges it with the given one.
     */
    public function getMergedWith(self $collection): self
    {
        return new self(
            array_merge($this->toArray(), $collection->toArray())
        );
    }
}
