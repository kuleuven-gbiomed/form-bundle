<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class InvalidScoringExtremaForGlobalScoreInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidScoringExtremaForGlobalScore';

    public static function createMessageForGlobalScoreNode(ScorableInputNode $globalScoreNode): self
    {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        $scoringParameters = $globalScoreNode->getInputScoringParameters();
        Assert::true(
            $scoringParameters->hasScoringExtrema(),
            ' can not build message '.self::class
            .'; the extrema scoring parameters needed to make scoring possible for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] are missing!'
        );

        Assert::false(
            $scoringParameters->hasValidScoringExtrema(),
            ' can not build message '.self::class
            .'; the extrema scoring parameters needed to make scoring possible for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] are not invalid!'
        );

        $minScore = (string) $scoringParameters->getMinScore();
        $maxScore = (string) $scoringParameters->getMaxScore();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the question [$globalScoreQuestionLabel] is configured as a global score question but its maximum score [$maxScore] and minimum score [$minScore] are not valid: the maximum score must be greater than the minimum score. Please make sure the maximum score is greater than the minimum score, alternatively, demote this global score question to a normal question by removing its dependencies on the questions it depends on for its calculation.";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%minScore%' => $minScore,
                '%maxScore%' => $maxScore,
            ]
        );
    }
}
