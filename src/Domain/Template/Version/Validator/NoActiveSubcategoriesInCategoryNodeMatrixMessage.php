<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;

final class NoActiveSubcategoriesInCategoryNodeMatrixMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.noActiveSubcategoriesInCategoryNodeMatrix';

    public static function createMessageForCategoryNodeMatrix(CategoryNode $categoryNode): self
    {
        $uid = $categoryNode->getUid();
        $matrixLabel = '[label unknown]';
        if ($categoryNode->getInfo()->getLocalizedLabel()->hasFallback()) {
            $matrixLabel = ' ['.$categoryNode->getInfo()->getLocalizedLabel()->getFallback().']';
        }

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": de tabel [$matrixLabel] heeft nog geen rijen. Je kan een rij toevoegen aan een tabel door een nieuwe categorie binnen de tabel te slepen. Deze zal dan dienen als een nieuwe rij.";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%uid%' => $uid,
            ]
        );
    }
}
