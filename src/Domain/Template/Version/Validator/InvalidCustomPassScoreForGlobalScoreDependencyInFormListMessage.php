<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class InvalidCustomPassScoreForGlobalScoreDependencyInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidCustomPassScoreForGlobalScoreDependency';

    public static function createMessageForGlobalScoreNodeAndDependencyScoreNode(
        ScorableInputNode $globalScoreNode,
        ScorableInputNode $dependencyScoreNode,
    ): self {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();
        $scoreQuestionLabel = $dependencyScoreNode->getNestedLabelForFallBackLocale();

        $globalScoreUid = $globalScoreNode->getUid();
        Assert::false(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->isEmpty(),
            ' can not build message '.self::class
            ."; the given node [$globalScoreQuestionLabel][$globalScoreUid] is not configured as a global score node (no dependency configurations found)"
        );

        Assert::true(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->hasOneByNodeUid($dependencyScoreNode->getUid()),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyScoreNode->getUid()
            .'] is not referenced as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        $scoringParameters = $dependencyScoreNode->getInputScoringParameters();

        Assert::true(
            $scoringParameters->hasCustomPassScore(),
            ' can not build message '.self::class
            .'; the custom pass score in scoring parameters for score node ['
            .$scoreQuestionLabel.']['.$dependencyScoreNode->getUid().'] as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] is not set!'
        );

        Assert::true(
            $scoringParameters->hasValidScoringExtrema(),
            ' can not build message '.self::class
            .'; the extrema scoring parameters needed to make check the custom pass score and scoring possible for score node ['
            .$scoreQuestionLabel.']['.$dependencyScoreNode->getUid().'] as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] are missing or invalid!'
        );

        $customPassScore = $scoringParameters->getCustomPassScore();
        $minScore = $scoringParameters->getMinScore();
        $maxScore = $scoringParameters->getMaxScore();

        Assert::false(
            $customPassScore >= $minScore && $customPassScore <= $maxScore,
            ' can not build message '.self::class
            .'; the custom pass score does fall within the extrema range in the scoring parameters for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        $customPassScore = (string) $scoringParameters->getCustomPassScore();
        $minScore = (string) $scoringParameters->getMinScore();
        $maxScore = (string) $scoringParameters->getMaxScore();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the score question [$scoreQuestionLabel] is configured as a calculation dependency for the global score question [$globalScoreQuestionLabel]. A custom pass score [$customPassScore] was configured for this score question. but this custom pass score does not fall within the range of the minimum score [%minScore%] and maximum score [%maxScore%] configured on this score question: the custom pass score must be less than or equal to the maximum score and greater than or equal to the minimum score. Please make sure the custom pass score is set within this range. alternatively, you can remove the custom pass score to allow pass score to fall back to the half between minimum and maximum score. or remove the question as dependency for the global score node.";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $scoreQuestionLabel,
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%customPassScore%' => $customPassScore,
                '%minScore%' => $minScore,
                '%maxScore%' => $maxScore,
            ]
        );
    }
}
