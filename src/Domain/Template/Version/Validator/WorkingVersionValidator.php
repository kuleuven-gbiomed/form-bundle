<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Version\PublishedVersion;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;

/**
 * Class WorkingVersionValidator.
 *
 * checks if a @see WorkingVersion and all its elements
 * in a @see TemplateInterface
 * is valid to be published as a @see PublishedVersion in that Template
 *
 * used as single point of truth to validate that the template has a set of questions in its FormList that
 * will be minimally (read) usable in each step of its workflow by at least one end-user (client) that has one of the given roles
 * by building up a set or custom errors that can be collected or used to throw exceptions.
 * this validator is both used to assert valid publish attempts of a working version by throwing Exceptions
 * as well as a UX friendly way to build translatable validation messages in the admin side
 *
 * requires:
 * INFO: @see WorkingVersionFormInfoValidator
 * - info with label @see FormInfo
 * WORKFLOW: @see WorkingVersionWorkFlowValidator
 * - a workflow @see WorkFlow
 * - at least one step in workflow so there is at least a flow
 * - no overlapping periods in workflow steps to avoid having two steps open at the same time in a workflow
 * FORMLIST: @see WorkingVersionFormListValidator
 * - a form list with questions & categories @see FormList
 * - at least one reachable (not (in)directly hidden) question in the formList so that there at least one question
 *   to be shown and/or filled in at some point
 * - for each of the steps in the workFlow there must be at least one reachable question in the formList that grants
 *   at least read access (either directly granted or indirectly via the higher ranked write access) for at least one of
 *   the given participatable roles (as set by @see TemplateInterface::getHierarchicalRolesThatCouldParticipate()) so that there is
 *   something to show and/or do in each step by at least one (role)
 *
 * NOTE: the errorMessages per element (FormInfo, WorkFlow, FormList) are build on first call to improve performance
 */
final class WorkingVersionValidator implements WorkingVersionValidatorInterface
{
    /** roles to verify minimal accessibility to questions in each step of the workingVersion's workflow */
    private readonly RoleCollection $participatableRoles;

    /** collection of all error message */
    private ?InvalidPublishMessageCollection $errorMessages = null;

    private ?InvalidPublishMessageCollection $formInfoErrorMessages = null;

    private ?InvalidPublishMessageCollection $workFlowErrorMessages = null;

    private ?InvalidPublishMessageCollection $formListErrorMessages = null;

    private function __construct(private readonly WorkingVersion $workingVersion, RoleCollection $participatableRoles)
    {
        if ($participatableRoles->isEmpty()) {
            throw new \InvalidArgumentException('at least one role [participatableRoles] required to verify accessibility in the workingVersion');
        }
        $this->participatableRoles = $participatableRoles;
    }

    public static function fromWorkingVersionAndParticipatableRoles(
        WorkingVersion $workingVersion,
        RoleCollection $participatableRoles,
    ): self {
        return new self($workingVersion, $participatableRoles);
    }

    /**
     * Checks whether the working version can be published.
     *
     * This does all kind of checks, but it doesn't verify whether all steps in the flow permissions of the questions
     * actually exist in the template.
     * FIXME: I'm not sure this is a bug or a feature, see https://jira.gbiomed.kuleuven.be/browse/FORM-132.
     */
    public function allowsPublish(): bool
    {
        return $this->getErrorMessages()->isEmpty();
    }

    public function getErrorMessages(): InvalidPublishMessageCollection
    {
        if (!$this->errorMessages instanceof InvalidPublishMessageCollection) {
            $this->errorMessages = $this->getFormInfoErrorMessages()
                ->getMergedWith($this->getWorkFlowErrorMessages())
                ->getMergedWith($this->getFormListErrorMessages());
        }

        return $this->errorMessages;
    }

    public function getFormInfoErrorMessages(): InvalidPublishMessageCollection
    {
        if (!$this->formInfoErrorMessages instanceof InvalidPublishMessageCollection) {
            $this->formInfoErrorMessages = WorkingVersionFormInfoValidator::fromWorkingVersion($this->workingVersion)
                ->getErrorMessages();
        }

        return $this->formInfoErrorMessages;
    }

    public function getWorkFlowErrorMessages(): InvalidPublishMessageCollection
    {
        if (!$this->workFlowErrorMessages instanceof InvalidPublishMessageCollection) {
            $this->workFlowErrorMessages = WorkingVersionWorkFlowValidator::fromWorkingVersion($this->workingVersion)
                ->getErrorMessages();
        }

        return $this->workFlowErrorMessages;
    }

    public function getFormListErrorMessages(): InvalidPublishMessageCollection
    {
        if (!$this->formListErrorMessages instanceof InvalidPublishMessageCollection) {
            $this->formListErrorMessages = WorkingVersionFormListValidator::fromWorkingVersionAndParticipatableRoles(
                $this->workingVersion,
                $this->participatableRoles
            )->getErrorMessages();
        }

        return $this->formListErrorMessages;
    }
}
