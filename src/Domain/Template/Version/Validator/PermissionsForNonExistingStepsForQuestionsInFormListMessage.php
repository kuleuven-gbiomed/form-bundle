<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Version\Validator\Helper\QuestionWithPermissionsForNonExistingSteps;

final class PermissionsForNonExistingStepsForQuestionsInFormListMessage extends InvalidFormListMessage
{
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.permissionsForNonExistingStepsForQuestionsInFormList';

    /**
     * @param array<array-key, QuestionWithPermissionsForNonExistingSteps> $questionsWithPermissionsForNonExistingSteps
     */
    public static function createMessageForQuestionsWithPermissionsForNonExistingSteps(array $questionsWithPermissionsForNonExistingSteps): self
    {
        $questionLabelStepsStrings = [];
        foreach ($questionsWithPermissionsForNonExistingSteps as $questionWithPermissionsForNonExistingSteps) {
            $questionLabel = $questionWithPermissionsForNonExistingSteps->getQuestionLabel();
            $stepPermissionUidsString = $questionWithPermissionsForNonExistingSteps->getStepPermissionUidsString();
            $questionLabelStepsStrings[] = "['$questionLabel' with permissions referring to non-existing steps '$stepPermissionUidsString']";
        }

        $list = implode(' || ', $questionLabelStepsStrings);

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": one or more questions use permissions that refer to steps that do not (or no longer) exist in the workFlow. those questions and their non-existing step permissions are: $list. "
            .'Those references to non-existing steps in the permissions for all those questions must be removed.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%list%' => $list,
            ]
        );
    }
}
