<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

final class MissingWorkFlowForQuestionsReleaseMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.missingWorkFlowForQuestionsRelease';

    public static function createMessage(): self
    {
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': a workFlow with at least one step is required to make a formList with any questions available.';

        return new self($message, self::TRANSLATION_KEY, self::TRANSLATION_DOMAIN);
    }
}
