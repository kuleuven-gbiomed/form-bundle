<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;

final class PrefillableChoiceIsMultipleMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.PrefillableChoiceIsMultiple';

    public static function createMessageForPrefillableChoice(ChoiceInputNode $prefillableChoice): self
    {
        $uid = $prefillableChoice->getUid();
        $label = $prefillableChoice->getLabelForFallBackLocale();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': you want to prefill the question with uid '.$uid
            .' and label "'.$label.'"'
            .', but this functionality is not available for multiselect choice questions';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%label%' => $label, '%uid%' => $uid]
        );
    }
}
