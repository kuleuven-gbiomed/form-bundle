<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;

final class PrefillingChoiceIsMultipleMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.PrefillingChoiceIsMultiple';

    public static function createMessageForPrefillingChoice(ChoiceInputNode $prefillingChoice): self
    {
        $uid = $prefillingChoice->getUid();
        $label = $prefillingChoice->getLabelForFallBackLocale();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': you want to use the question with uid '.$uid
            .' and label "'.$label.'" to prefill another choice question'
            .', but multiselect questions cannot be used to prefill';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%label%' => $label, '%uid%' => $uid]
        );
    }
}
