<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

class InvalidMultiSelectForGlobalScoreChoiceDependencyInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidMultiSelectForChoiceGlobalScoreChoiceDependency';

    public static function createMessageForGlobalScoreNodeAndDependencyChoiceScoreNode(
        ScorableInputNode $globalScoreNode,
        ChoiceInputNode $dependencyChoiceScoreNode,
    ): self {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();
        $scoreQuestionLabel = $dependencyChoiceScoreNode->getNestedLabelForFallBackLocale();

        Assert::true(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->hasOneByNodeUid($dependencyChoiceScoreNode->getUid()),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyChoiceScoreNode->getUid()
            .'] is not referenced as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        Assert::true(
            $dependencyChoiceScoreNode->getInput()->isMultiple(),
            ' can not build message '.self::class
            .'; the choice score node ['.$scoreQuestionLabel.']['.$dependencyChoiceScoreNode->getUid()
            .'] as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] is not configured as multiple!'
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the choice question ['.$scoreQuestionLabel.'] is configured as a calculation dependency for the global score question ['.$globalScoreQuestionLabel.']. but it is configured to allow multiple answers to be given by users, which is not allowed when the question is to be used as a dependency for global scoring calculations.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $scoreQuestionLabel,
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
            ]
        );
    }
}
