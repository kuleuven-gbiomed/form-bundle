<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

class InvalidOptionsWithTextInputForGlobalScoreChoiceDependencyInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidOptionsWithTextInputForGlobalScoreChoiceDependency';

    public static function createMessageForGlobalScoreNodeAndDependencyChoiceScoreNode(
        ScorableInputNode $globalScoreNode,
        ChoiceInputNode $dependencyChoiceScoreNode,
    ): self {
        $locale = $globalScoreNode->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();
        $scoreQuestionLabel = $dependencyChoiceScoreNode->getNestedLabelForFallBackLocale();

        Assert::true(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->hasOneByNodeUid($dependencyChoiceScoreNode->getUid()),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyChoiceScoreNode->getUid()
            .'] is not referenced as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        Assert::true(
            $dependencyChoiceScoreNode->usesOptionsWithAdditionalTextInput(),
            ' can not build message '.self::class
            .'; the choice score node ['.$scoreQuestionLabel.']['.$dependencyChoiceScoreNode->getUid()
            .'] as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] does not use any options with additional text input!'
        );

        $otherOptionLabels = implode(
            ' | ',
            $dependencyChoiceScoreNode->getChildren()->getUsingAdditionalTextInput()->getLabelsArray($locale)
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the choice question ['.$scoreQuestionLabel.'] is configured as a calculation dependency for the global score question ['.$globalScoreQuestionLabel.']. but it has answer options that allow for additional text input (a.k.a. \'other\' options). Answer options allowing users to add additional text input are not allowed when the choice question is to be used as a dependency for global scoring calculations. Please remove those answer Options or replace them by answer options that do not allow for additional text input. those invalid answer options are: ['.$otherOptionLabels.'].';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $scoreQuestionLabel,
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%otherOptionLabels%' => $otherOptionLabels,
            ]
        );
    }
}
