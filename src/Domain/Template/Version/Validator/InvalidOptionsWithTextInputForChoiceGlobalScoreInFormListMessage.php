<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use Webmozart\Assert\Assert;

class InvalidOptionsWithTextInputForChoiceGlobalScoreInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidOptionsWithTextInputForChoiceGlobalScore';

    public static function createMessageForChoiceGlobalScoreNode(ChoiceInputNode $globalScoreNode): self
    {
        $locale = $globalScoreNode->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        Assert::true(
            $globalScoreNode->usesOptionsWithAdditionalTextInput(),
            ' can not build message '.self::class
            .'; the choice global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] does not use any options with additional text input!'
        );

        $otherOptionLabels = implode(
            ' | ',
            $globalScoreNode->getChildren()->getUsingAdditionalTextInput()->getLabelsArray($locale)
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the choice question ['.$globalScoreQuestionLabel.'] is configured as a global score question but it has answer options that allow for additional text input (a.k.a. \'other\' options). Answer options allowing users to add additional text input are not allowed when the choice question is to be used for global scoring calculations. Please remove those answer Options or replace them by answer options that do not allow for additional text input. those invalid answer options are: ['.$otherOptionLabels.'].';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%otherOptionLabels%' => $otherOptionLabels,
            ]
        );
    }
}
