<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormInfo;

/**
 * Class InvalidFormInfoMessage.
 *
 * error message for invalid @see FormInfo
 * when validating a @see TemplateInterface::getWorkingVersion() for publish
 */
class InvalidFormInfoMessage extends InvalidPublishMessage
{
    /** @var string */
    final public const TRANSLATION_BASE_KEY = 'invalidFormInfo';
}
