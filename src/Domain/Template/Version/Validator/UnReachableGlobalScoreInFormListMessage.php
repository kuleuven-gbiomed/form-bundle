<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class UnReachableGlobalScoreInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.unReachableGlobalScore';

    public static function createMessageForGlobalScoreNode(ScorableInputNode $globalScoreNode): self
    {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        $globalScoreUid = $globalScoreNode->getUid();
        Assert::false(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->isEmpty(),
            ' can not build message '.self::class
            ."; the given node [$globalScoreQuestionLabel][$globalScoreUid] is not configured as a global score node (no dependency configurations found)"
        );

        Assert::false(
            $globalScoreNode->isReachableInTree(),
            ' can not build message '.self::class
            .'; the globals score node['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] is reachable in the form list tree!'
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the question ['.$globalScoreQuestionLabel.'] is configured as a global score question but it is either marked as hidden or part of a hidden category. Please make sure the global score is not hidden or part of a hidden category';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%globalQuestionLabel%' => $globalScoreQuestionLabel]
        );
    }
}
