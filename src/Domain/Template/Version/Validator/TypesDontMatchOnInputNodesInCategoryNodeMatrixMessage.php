<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;

final class TypesDontMatchOnInputNodesInCategoryNodeMatrixMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.typesDontMatchOnInputNodesInCategoryNodeMatrixMessage';

    public static function createMessageForCategoryNodeMatrixAndQuestionTypes(CategoryNode $matrix, array $uniqueSubcategoryQuestionsTypes): self
    {
        $matrixUid = $matrix->getUid();
        $subcategories = $matrix->getChildren()->getCategoryNodes();

        $invalidQuestionMessages = [];
        $questionCount = is_countable($uniqueSubcategoryQuestionsTypes[0]) ? count($uniqueSubcategoryQuestionsTypes[0]) : 0;
        for ($questionKey = 0; $questionKey < $questionCount; ++$questionKey) {
            if (count(array_unique(array_column($uniqueSubcategoryQuestionsTypes, $questionKey), \SORT_REGULAR)) > 1) {
                $invalidSubcategoryMessages = [];
                foreach ($uniqueSubcategoryQuestionsTypes as $categoryKey => $uniqueSubcategoryQuestionsType) {
                    /** @var CategoryNode $subcategory */
                    $subcategory = $subcategories->get($categoryKey);
                    $label = '';
                    if ($subcategory->getInfo()->getLocalizedLabel()->hasFallback()) {
                        $label = ' ['.$subcategory->getInfo()->getLocalizedLabel()->getFallback().']';
                    }

                    $nonMatchingType = $uniqueSubcategoryQuestionsType[$questionKey];
                    $invalidSubcategoryMessages[] = '['.$nonMatchingType.'] in subcategory '.((string) ((int) $categoryKey + 1)).$label.'.';
                }
                // FIXME: messages are nested so how to handle translation??
                $invalidQuestionMessages[] = 'Question '.((string) ($questionKey + 1)).' uses the following types for questions: '
                    .implode(' | ', $invalidSubcategoryMessages);
            }
        }

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the matrix with id [$matrixUid] should use the same type for the questions in each subcategory in the same order but they don't match: "
            .implode(' | ', $invalidQuestionMessages);

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%matrixId%' => $matrixUid,
            ]
        );
    }
}
