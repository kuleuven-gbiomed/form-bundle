<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

final class MissingWorkFlowMessage extends InvalidWorkFlowMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.missingWorkFlow';

    public static function createMessage(): self
    {
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG.': a workFlow is missing';

        return new self($message, self::TRANSLATION_KEY, self::TRANSLATION_DOMAIN);
    }
}
