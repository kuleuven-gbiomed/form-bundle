<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class InvalidOptionalDependenciesForWritableGlobalScoreWithWeighingTypeAbsoluteInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidOptionalDependenciesForWritableGlobalScoreWithWeighingTypeAbsoluteInFormListMessage';

    public static function createMessageForGlobalScoreNode(ScorableInputNode $globalScoreNode): self
    {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        // get all dependencies (= score nodes this global score node depends on)
        $dependencyQuestions = $globalScoreNode->getAllGlobalScoreCalculationScoreNodeConfigs()->getScoreNodes();

        Assert::true(
            $globalScoreNode->getFlowPermission()->isWriteAccessGrantedForAtLeastOneRoleInAtLeastOneStep(),
            ' can not build message '.self::class
            .'; the node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] does not grant write access to at least one role in at least one step!'
        );

        Assert::true(
            $globalScoreNode->isRequired(),
            ' can not build message '.self::class
            .'; the global score node ['.$globalScoreNode->getUid()
            .'] is already optional, which makes the global score node having only optional dependencies valid!'
        );

        Assert::true(
            OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE === $globalScoreNode->getGlobalScoreWeighingType(),
            ' can not build message '.self::class
            .'; the node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] is not using "'.OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE.'" as weighingType!'
        );

        Assert::false(
            $dependencyQuestions->isEmpty(),
            ' can not build message '.self::class
            .'; the node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] is not operating as global score question; it has no scoring questions on which it depends!'
        );

        $optionalDependencyQuestions = $dependencyQuestions->getOptional();

        Assert::true(
            $dependencyQuestions->count() === $optionalDependencyQuestions->count(),
            ' can not build message '.self::class
            .'; the node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] has no invalid amount of optional dependencies;'
            .' there is at least one dependency that is not optional!'
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the question ['.$globalScoreQuestionLabel.'] is configured as a global score question with absolute weighingType.'
            .' But all the scoring questions on which this global score question depends are optional questions.'
            .' if a global score question with absolute weighingType has only optional questions as dependencies,'
            .' and it grants write access to at least one role in at least one step,
            then the global score question must be optional as well.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%globalQuestionUid%' => $globalScoreNode->getUid(),
            ]
        );
    }
}
