<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Webmozart\Assert\Assert;

/**
 * Class InvalidPublishMessage.
 *
 * custom message used in the @see WorkingVersionValidator
 * message used as custom message for Exception as well as translatable message for usage in admin-side for displaying
 * validation error messages on requests to publish @see TemplateInterface::getWorkingVersion()
 *
 * @see InvalidPublishMessageCollection
 */
class InvalidPublishMessage
{
    /** @var string */
    public const BASE_CANNOT_PUBLISH_VERSION_MSG = 'Kan (werkende) versie niet publiceren';
    /** @var string */
    public const TRANSLATION_DOMAIN = 'kulFormVersionPublishValidators';

    protected function __construct(
        /** plain error message */
        private readonly string $message,
        /** reference to a translation sheet key */
        private readonly ?string $translationKey = null,
        /** reference to a translation sheet domain */
        private readonly ?string $translationDomain = null,
        /** set of keys with values to be used as translation parameters */
        private readonly array $translationParameters = [],
    ) {
        Assert::stringNotEmpty($message);
        Assert::nullOrStringNotEmpty($translationKey);
        Assert::nullOrStringNotEmpty($translationDomain);
    }

    public function getMessage(?TranslatorInterface $translator = null): string
    {
        if (!$translator instanceof TranslatorInterface) {
            return $this->message;
        }

        if (null === $this->translationKey || '' === $this->translationKey) {
            return $this->message;
        }

        return $translator->trans($this->translationKey, $this->translationParameters, $this->translationDomain);
    }
}
