<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart\ParticipationQuestionEligibility;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;

class MissingRangeRequirementsOnParticipatingQuestionForRadarChartNodeInFormListMessage extends InvalidFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidRadarChartNodeInFormList.missingRangeRequirementsOnParticipatingQuestionForRadarChartNode';

    public static function createMessageForRadarChartAndParticipationEligibility(
        RadarChartViewNode $chartViewNode,
        ParticipationQuestionEligibility $questionEligibility,
    ): self {
        $locale = $chartViewNode->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $chartViewNodeLabel = $chartViewNode->getNestedLabel($locale);

        $noMin = $questionEligibility->isMissingMinimumAnswerValue();
        $noMax = $questionEligibility->isMissingMaximumAnswerValue();
        if (false === $noMin && false === $noMax) {
            throw new \InvalidArgumentException(' can not build message '.self::class.' for radarChartNode ['.$chartViewNodeLabel.']: the given ParticipationQuestionEligibility [questionUid: '.$questionEligibility->getReferencedQuestionUid().'] does refer to a participating question with a valid minimumAnswerValue and maximumAnswerValue!');
        }

        $participatingQuestion = $questionEligibility->getQuestion();
        $participatingQuestionLabel = $participatingQuestion->hasNestedLabel($locale) ? $participatingQuestion->getNestedLabel($locale) : 'unknown label';

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': The list of participating questions the radar chart node ['.$chartViewNodeLabel.'] refers to a question/node ['.$participatingQuestionLabel.' which is not eligible as participating question in radar charts: the minimum and/or maximum answer answer value is missing;Both a minimum and maximum answer answer value are required';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%chartViewNodeLabel%' => $chartViewNodeLabel,
                '%participatingQuestionLabel%' => $participatingQuestionLabel,
            ]
        );
    }
}
