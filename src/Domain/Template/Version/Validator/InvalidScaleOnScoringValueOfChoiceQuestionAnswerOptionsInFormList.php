<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Utility\NumberScaleChecker;

final class InvalidScaleOnScoringValueOfChoiceQuestionAnswerOptionsInFormList extends InvalidFormListMessage
{
    public const EXTREMUM_TYPE_MINIMUM = 'minimum';
    public const EXTREMUM_TYPE_MAXIMUM = 'maximum';

    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidScaleOnScoringValueOfChoiceQuestionAnswerOptions';

    public static function createMessageForChoiceQuestion(
        ChoiceInputNode $question,
    ): self {
        $locale = $question->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $questionLabel = $question->getNestedLabelForFallBackLocale();
        $scale = self::getScaleForChoiceQuestion($question);

        $answerOptionsLabels = '';
        foreach ($question->getChildren()->getHavingScoringValue()->toArray() as $optionNode) {
            if (NumberScaleChecker::doesNumberExceedMaxAmountOfDecimals($optionNode->getScoringValue(), $scale)) {
                $answerOptionsLabels .= $optionNode->getLabel($locale);
            }
        }

        $scaleString = (string) $scale;
        if ('' === $answerOptionsLabels) {
            throw new \BadMethodCallException(' can not build message '.self::class.'; NONE of the score values on the answer options of choice question ['.$questionLabel.']['.$question->getUid().'] exceed the max amount of decimals ['.$scaleString.'] configured on this question!');
        }

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the choice question [{$questionLabel}] has one or more answer options configured with a score value that exceed the amount of [{$scaleString}] decimal(s) that this choice question is configured to allow. Please either reduce the amount of decimals in the score values of those answer options or adjust the amount of decimals that this choice question is configured to allow. those answer options with invalid score decimals are: [{$answerOptionsLabels}]";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $questionLabel,
                '%scale%' => $scaleString,
                '%answerOptions%' => $answerOptionsLabels,
            ]
        );
    }

    private static function getScaleForChoiceQuestion(ChoiceInputNode $question): int
    {
        if (!$question->getInputScoringParameters()->hasScale()) {
            throw new \InvalidArgumentException('no scale (amount of decimals) set on choice question');
        }

        return $question->getInputScoringParameters()->getScale();
    }
}
