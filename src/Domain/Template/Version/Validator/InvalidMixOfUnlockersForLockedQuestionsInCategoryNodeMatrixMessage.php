<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;

class InvalidMixOfUnlockersForLockedQuestionsInCategoryNodeMatrixMessage extends InvalidFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidMixOfUnlockersForLockedQuestionsInCategoryNodeMatrix';

    public static function createMessageForCategoryNodeMatrix(
        CategoryNode $matrix,
    ): self {
        $allQuestions = $matrix->getFlattenedParentalInputNodeDescendants();
        $allQuestionsAmount = $allQuestions->count();
        if (0 === $allQuestionsAmount) {
            throw new \InvalidArgumentException('matrix cannot have invalid mix of unlockers for locked questions: it has no questions at all.');
        }

        $lockedQuestions = $allQuestions->getLocked();
        if (0 === $lockedQuestions->count()) {
            throw new \InvalidArgumentException('matrix cannot have invalid mix of unlockers for locked questions: it has no locked questions.');
        }

        $lockedQuestionsByUnlockers = [];
        /** @var InputNode $lockedQuestion */
        foreach ($lockedQuestions as $lockedQuestion) {
            if ($lockedQuestion instanceof ParentalInputNode) {
                $lockedQuestionsByUnlockers[implode(' | ', $lockedQuestion->getUnlockers()->getUids())][] = $lockedQuestion;
            }
        }

        if (1 === count($lockedQuestionsByUnlockers)) {
            throw new \InvalidArgumentException('matrix cannot have invalid mix of unlockers for locked questions: all locked questions have exact same chain of unlockers.');
        }

        $unlockersdata = '';
        foreach ($lockedQuestionsByUnlockers as $unlockersString => $_lockedQuestions) {
            $lockedQuestionsUids = array_map(fn (InputNode $question) => $question->getUid(), $_lockedQuestions);

            $unlockersdata .= '<< unlocker ['.$unlockersString.'] which is used by questions ['.implode('|', $lockedQuestionsUids).'] >>';
        }

        $amountString = (string) count($lockedQuestionsByUnlockers);
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': all locked questions in a matrix must depend on the same unlocker or chain of unlockers, a mix of unlockers is not allowed: '
            .$amountString.' unlockers or chain of unlockers are used by the locked questions in matrix ['.$matrix->getUid().']'
            .'. Those are: ['.$unlockersdata.'].';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%matrix%' => $matrix->getUid(),
                '%amountOfUnlockers%' => $amountString,
                '%unlockers%' => $unlockersdata,
            ]
        );
    }
}
