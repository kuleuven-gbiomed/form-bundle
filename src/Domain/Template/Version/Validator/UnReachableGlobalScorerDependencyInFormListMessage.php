<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class UnReachableGlobalScorerDependencyInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.unReachableGlobalScoreDependency';

    public static function createMessageForGlobalScoreNodeAndDependencyScoreNode(
        ScorableInputNode $globalScoreNode,
        ScorableInputNode $dependencyScoreNode,
    ): self {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();
        $scoreQuestionLabel = $dependencyScoreNode->getNestedLabelForFallBackLocale();

        Assert::true(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->hasOneByNodeUid($dependencyScoreNode->getUid()),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyScoreNode->getUid()
            .'] is not referenced as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        Assert::false(
            $dependencyScoreNode->isReachableInTree(),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyScoreNode->getUid()
            .'] is reachable in the form list tree!'
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the score question ['.$scoreQuestionLabel.'] is configured as a calculation dependency for the global score question ['.$globalScoreQuestionLabel.']. but it is either marked as hidden or part of a hidden category. Please make sure the score question is not hidden or part of a hidden category';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%questionLabel%' => $scoreQuestionLabel,
            ]
        );
    }
}
