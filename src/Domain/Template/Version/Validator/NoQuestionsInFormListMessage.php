<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

final class NoQuestionsInFormListMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.noQuestionsInFormList';

    public static function createMessage(): self
    {
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG.': no questions at all found in formList, but at least one question is needed to make it workable.';

        return new self($message, self::TRANSLATION_KEY, self::TRANSLATION_DOMAIN);
    }
}
