<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator\Helper;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\StepPermissionCollection;

final readonly class QuestionWithPermissionsForNonExistingSteps
{
    public function __construct(
        private InputNode $question,
        private StepPermissionCollection $stepPermissionsForNonExistingSteps,
    ) {
        if (!$this->question instanceof ParentalInputNode) {
            throw new \InvalidArgumentException('given inputNode is not a question');
        }

        if (0 === $stepPermissionsForNonExistingSteps->count()) {
            throw new \InvalidArgumentException('given set of stepPermissionsForNonExistingSteps is empty');
        }

        $invalidQuestionStepPermissions = $this->question->getFlowPermission()->getStepPermissions()->getByStepUids($this->stepPermissionsForNonExistingSteps->getStepUids());
        if ($invalidQuestionStepPermissions->count() !== $this->stepPermissionsForNonExistingSteps->count()) {
            throw new \InvalidArgumentException('not all given stepPermissionsForNonExistingSteps are step permissions for given question');
        }
    }

    public function getQuestionLabel(): string
    {
        return $this->question->getNestedLabelForFallBackLocale();
    }

    public function getStepPermissionUidsString(): string
    {
        return implode(' | ', $this->stepPermissionsForNonExistingSteps->getStepUids());
    }
}
