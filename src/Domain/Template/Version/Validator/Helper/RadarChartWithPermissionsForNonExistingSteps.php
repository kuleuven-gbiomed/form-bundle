<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator\Helper;

use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\StepPermissionCollection;

final readonly class RadarChartWithPermissionsForNonExistingSteps
{
    public function __construct(
        private RadarChartViewNode $radarChart,
        private StepPermissionCollection $stepPermissionsForNonExistingSteps,
    ) {
        if (0 === $stepPermissionsForNonExistingSteps->count()) {
            throw new \InvalidArgumentException('given set of stepPermissionsForNonExistingSteps is empty');
        }

        $invalidQuestionStepPermissions = $this->radarChart->getFlowPermission()->getStepPermissions()->getByStepUids($this->stepPermissionsForNonExistingSteps->getStepUids());
        if ($invalidQuestionStepPermissions->count() !== $this->stepPermissionsForNonExistingSteps->count()) {
            throw new \InvalidArgumentException('not all given stepPermissionsForNonExistingSteps are step permissions for given radar chart');
        }
    }

    public function getRadarChartLabel(): string
    {
        return $this->radarChart->getNestedLabelForFallBackLocale();
    }

    public function getStepPermissionUidsString(): string
    {
        return implode(' | ', $this->stepPermissionsForNonExistingSteps->getStepUids());
    }
}
