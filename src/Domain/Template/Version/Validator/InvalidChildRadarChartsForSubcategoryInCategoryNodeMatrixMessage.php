<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;

final class InvalidChildRadarChartsForSubcategoryInCategoryNodeMatrixMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidChildRadarChartsForSubcategoryInCategoryNodeMatrix';

    public static function createMessageForCategoryNodeMatrixAndSubcategory(CategoryNode $matrix, CategoryNode $subcategory): self
    {
        $matrixUid = $matrix->getUid();
        $matrixLabel = '[label unknown]';
        if ($matrix->getInfo()->getLocalizedLabel()->hasFallback()) {
            $matrixLabel = ' ['.$matrix->getInfo()->getLocalizedLabel()->getFallback().']';
        }

        $subcategoryUid = $subcategory->getUid();
        $subcategoryLabel = '[label unknown]';
        if ($subcategory->getInfo()->getLocalizedLabel()->hasFallback()) {
            $subcategoryLabel = ' ['.$subcategory->getInfo()->getLocalizedLabel()->getFallback().']';
        }

        $invalidNodesCount = $subcategory->getChildren()->getReadOnlyViewNodes()->getRadarChartNodes()->count();

        if (0 === $invalidNodesCount) {
            throw new \Exception(' can not build message '.self::class.": the subcategory [$subcategoryLabel] with id [$subcategoryUid] in matrix [$matrixLabel] with id [$matrixUid] has no radar charts as children.");
        }

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the subcategory [$subcategoryLabel] with id [$subcategoryUid] in matrix [$matrixLabel] with id [$matrixUid] has "
            ."[$invalidNodesCount] radar chart(s) as children, but it can only have input nodes (questions) as children.";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%matrixId%' => $matrixUid,
                '%subcategoryUid%' => $subcategoryUid,
                '%subcategoryLabel%' => $subcategoryLabel,
                '%invalidNodesCount%' => $invalidNodesCount,
                '%matrixLabel%' => $matrixLabel,
            ]
        );
    }
}
