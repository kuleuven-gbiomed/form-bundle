<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

class NonUniqueNodeIdsUsedInFormListMessage extends InvalidFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.nonUniqueNodeIdsUsedInFormList';

    public static function createMessageForNodeIds(array $nodeIds): self
    {
        $idsString = implode(' | ', $nodeIds);

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': Duplicate node ids used: Make sure all nodes have an unique id. The ids of those invalid nodes are: ['.$idsString.'].';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%nodeIds%' => $idsString]
        );
    }
}
