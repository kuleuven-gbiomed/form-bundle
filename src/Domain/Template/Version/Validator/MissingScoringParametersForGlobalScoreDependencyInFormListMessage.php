<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class MissingScoringParametersForGlobalScoreDependencyInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.missingScoringParametersForGlobalScoreDependency';

    public static function createMessageForGlobalScoreNodeAndDependencyScoreNode(
        ScorableInputNode $globalScoreNode,
        ScorableInputNode $dependencyScoreNode,
    ): self {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();
        $scoreQuestionLabel = $dependencyScoreNode->getNestedLabelForFallBackLocale();

        Assert::true(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->hasOneByNodeUid($dependencyScoreNode->getUid()),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyScoreNode->getUid()
            .'] is not referenced as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        Assert::false(
            $dependencyScoreNode->getInputScoringParameters()->makesScoringPossible(),
            ' can not build message '.self::class
            .'; the scoring parameters needed to make scoring possible for score node ['
            .$scoreQuestionLabel.']['.$dependencyScoreNode->getUid().'] as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] are not missing!'
        );

        $scoringParameterLabels = implode(
            ' | ',
            self::getMissingScoringParameterLabels($dependencyScoreNode->getInputScoringParameters())
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the score question ['.$scoreQuestionLabel.'] is configured as a calculation dependency for the global score question ['.$globalScoreQuestionLabel.']. but it misses the following crucial requirements that are needed to be used as calculation dependency: ['.$scoringParameterLabels.'] . Please make sure to provide a value for each or, alternatively, remove the question as dependency for the global score node.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $scoreQuestionLabel,
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%scoringParameterLabels%' => $scoringParameterLabels,
            ]
        );
    }
}
