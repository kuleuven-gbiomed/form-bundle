<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart\ParticipationQuestionEligibility;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;

class NonPlottableParticipatingQuestionForRadarChartNodeInFormListMessage extends InvalidFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidRadarChartNodeInFormList.nonPlottableParticipatingQuestionForRadarChartNode';

    public static function createMessageForRadarChartAndParticipationEligibility(
        RadarChartViewNode $chartViewNode,
        ParticipationQuestionEligibility $questionEligibility,
    ): self {
        $locale = $chartViewNode->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $chartViewNodeLabel = $chartViewNode->getNestedLabel($locale);

        if (!$questionEligibility->refersToNonPlottableQuestion()) {
            throw new \InvalidArgumentException(' can not build message '.self::class.' for radarChartNode ['.$chartViewNodeLabel.']: the given ParticipationQuestionEligibility [questionUid: '.$questionEligibility->getReferencedQuestionUid().'] does refer to a plottable question!');
        }

        $participatingQuestion = $questionEligibility->getQuestion();
        $participatingQuestionLabel = $participatingQuestion->hasNestedLabel($locale) ? $participatingQuestion->getNestedLabel($locale) : 'unknown label';
        $participatingQuestionType = $participatingQuestion::getType();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': The list of participating questions the radar chart node ['.$chartViewNodeLabel.'] refers to a question/node ['.$participatingQuestionLabel.' which is not eligible as participating question in radar charts: questions/nodes of type ['.$participatingQuestionType.'] cannot be used as participating question';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%chartViewNodeLabel%' => $chartViewNodeLabel,
                '%participatingQuestionLabel%' => $participatingQuestionLabel,
                '%participatingQuestionType%' => $participatingQuestionType,
            ]
        );
    }
}
