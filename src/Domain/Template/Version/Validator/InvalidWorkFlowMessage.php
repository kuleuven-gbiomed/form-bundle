<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;

/**
 * Class InvalidWorkFlowMessage.
 *
 * error message for invalid @see WorkFlow
 * when validating a @see TemplateInterface::getWorkingVersion() for publish
 */
class InvalidWorkFlowMessage extends InvalidPublishMessage
{
    /** @var string */
    final public const TRANSLATION_BASE_KEY = 'invalidWorkFlow';
}
