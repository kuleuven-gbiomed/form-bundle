<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;

/**
 * Class InvalidFormListMessage.
 *
 * error message for invalid @see FormList
 * when validating a @see TemplateInterface::getWorkingVersion() for publish
 */
class InvalidFormListMessage extends InvalidPublishMessage
{
    /** @var string */
    final public const TRANSLATION_BASE_KEY = 'invalidFormList';
}
