<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

final class NoReachableAccessibleQuestionsAndReadOnlyViewNodesForRolesInStepsMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.NoReachableAccessibleQuestionsAndReadOnlyViewNodesForRolesInSteps';

    /**
     * @param StepInterface[] $steps
     *
     * @throws LocalizedStringException
     */
    public static function createMessageForStepsAndRoles(array $steps, RoleCollection $roles): self
    {
        $stepLabelsString = implode(' | ', array_map(fn (StepInterface $step) =>
            // a step always has a label asserted for at least one locale (i.e. fallback, usually default locale)
            $step->getInfo()->getLocalizedLabel()->getFallback(), $steps));

        $rolesString = implode(' | ', array_map(fn (Role $role) => $role->getName(), $roles->toArray()));

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the steps ['.$stepLabelsString.'] have one or more accessible questions and/or read-only items (views combining answers from questions in e.g. radar charts)'
            .' with at least read access for at least one of the participating roles ['.$rolesString.'], '
            .' but none of those questions and read-only items are made available;'
            .' each of these questions and read-only items is marked as hidden/inactive'
            .' and/or part of an ancestral category that is marked as hidden/inactive.'
            .' Make sure at least one question or read-only item is reachable (not hidden).';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%stepLabels%' => $stepLabelsString, '%roles%' => $rolesString]
        );
    }
}
