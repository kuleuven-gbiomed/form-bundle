<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

/**
 * interface WorkingVersionValidatorInterface.
 */
interface WorkingVersionValidatorInterface
{
    /**
     * @return bool
     */
    public function allowsPublish();

    /**
     * @return InvalidPublishMessageCollection
     */
    public function getErrorMessages();
}
