<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\AbstractReadOnlyViewNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\ReadOnlyViewNodeCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

final class UnReadableReachableReadOnlyViewNodesForRolesInformListMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.UnReadableReachableReadOnlyViewNodesForRolesInformList';

    /** @param ReadOnlyViewNodeCollection<array-key, AbstractReadOnlyViewNode> $unreadableReadOnlyViewNodes */
    public static function createMessageForReadOnlyViewNodesAndRoles(
        ReadOnlyViewNodeCollection $unreadableReadOnlyViewNodes,
        RoleCollection $roles,
    ): self {
        $rolesString = implode(' | ', array_map(fn (Role $role) => $role->getName(), $roles->toArray()));

        $nodeLabels = implode(' | ', array_map(fn (AbstractReadOnlyViewNode $readOnlyViewNode): string => $readOnlyViewNode->getNestedLabel($readOnlyViewNode->getInfo()->getLocalizedLabel()->getFallbackLocale()), $unreadableReadOnlyViewNodes->toArray()));

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': One or more available (reachable) read-only items (views combining answers from questions in e.g. radar charts) are found in formList, but at least one of those read-only items does not has read access granted in at least one step in the workflow for at least one of the participating roles ['.$rolesString.'], rendering these read-only items useless & unable to be viewed by any user. those un-viewable read-only are: ['.$nodeLabels.']. make sure that for each of those read-only items, read access is granted for at least one of these participating roles in at least one step of the workflow.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%nodeLabels%' => $nodeLabels, '%roles%' => $rolesString]
        );
    }
}
