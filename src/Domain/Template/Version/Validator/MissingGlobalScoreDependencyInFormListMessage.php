<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

class MissingGlobalScoreDependencyInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.missingGlobalScoreDependency';

    public static function createMessageForGlobalScoreNodeAndDependencyScoreNodeUid(
        ScorableInputNode $globalScoreNode,
        string $dependencyScoreNodeUid,
    ): self {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        Assert::true(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->hasOneByNodeUid($dependencyScoreNodeUid),
            ' can not build message '.self::class
            .'; the score node reference ID ['.$dependencyScoreNodeUid
            .'] is not found in the dependency references for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        Assert::false(
            $globalScoreNode->getTreeRootNode()->hasDescendantByUid($dependencyScoreNodeUid),
            ' can not build message '.self::class
            .'; the score node referencing ID ['.$dependencyScoreNodeUid.'] is not missing from the form list nodes tree!'
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the question ['.$globalScoreQuestionLabel.'] is configured as a global score question but one of its calculation dependencies point to a question that does not or no longer exist in the form list. Please remove the calculation dependency that refers to a question wit ID ['.$dependencyScoreNodeUid.']';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%dependencyScoreNodeUid%' => $dependencyScoreNodeUid,
            ]
        );
    }
}
