<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;

final class InvalidNumberOfQuestionsInSubcategoriesInCategoryNodeMatrix extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidNumberOfQuestionsInSubcategoriesInCategoryNodeMatrix';

    public static function createMessageForCategoryNodeMatrixAndQuestionCount(CategoryNode $matrix, array $questionCount): self
    {
        $matrixUid = $matrix->getUid();
        $subcategories = $matrix->getChildren()->getCategoryNodes();
        $questionCountStrings = [];
        foreach ($questionCount as $key => $count) {
            /** @var CategoryNode $subcategory */
            $subcategory = $subcategories->get($key);
            $label = '[label unknown]';
            if ($subcategory->getInfo()->getLocalizedLabel()->hasFallback()) {
                $label = ' ['.$subcategory->getInfo()->getLocalizedLabel()->getFallback().']';
            }
            // FIXME: messages are nested so how to handle translation??
            $questionCountStrings[] = 'Subcategory '.((string) $key).$label." has $count question(s)";
        }
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the matrix with id [$matrixUid] should have an equal number of questions in each subcategory but they aren't equal: "
            .implode(' | ', $questionCountStrings);

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%matrixId%' => $matrixUid,
            ]
        );
    }
}
