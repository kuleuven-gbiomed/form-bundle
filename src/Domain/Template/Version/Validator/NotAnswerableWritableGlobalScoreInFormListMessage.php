<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class NotAnswerableWritableGlobalScoreInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.notAnswerableWritableGlobalScore';

    public static function createMessageForGlobalScoreNode(ScorableInputNode $globalScoreNode): self
    {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        $globalScoreUid = $globalScoreNode->getUid();
        Assert::false(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->isEmpty(),
            ' can not build message '.self::class
            ."; the given node [$globalScoreQuestionLabel][$globalScoreUid] is not configured as a global score node (no dependency configurations found)"
        );

        Assert::true(
            $globalScoreNode->getFlowPermission()->isWriteAccessGrantedForAtLeastOneRoleInAtLeastOneStep(),
            ' can not build message '.self::class
            .'; the global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] does not grant write access to at least one role in at least one step!'
        );

        $scoringParameters = $globalScoreNode->getInputScoringParameters();

        Assert::false(
            $scoringParameters->showAutoCalculateBtn(),
            ' can not build message '.self::class
            .'; the global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] is configured to display a (re)calculation button!'
        );

        Assert::false(
            $scoringParameters->allowNonAutoCalculatedValue(),
            ' can not build message '.self::class
            .'; the global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] does allow manual input!'
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the question [$globalScoreQuestionLabel] is configured as a global score question and it grants write access to at least one role in at least one step. But it is also configured to not display the (re)calculation button and to not allow manual input. this combination of write access but no (re)calculation button and no manual input, might make this global question unanswerable at some point in the workflow for the users with write access and - if no value could be auto-calculated - block the ability for those users to submit the form all together. Please either remove all write access or enable either the button or the manual input or both. alternatively, you can demote this global score question to a normal question by removing its dependencies on the questions it depends on for its calculation.";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
            ]
        );
    }
}
