<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;

/**
 * Class WorkingVersionFormInfoValidator.
 *
 * checks if a @see FormInfo
 * in a @see WorkingVersion
 * in a @see TemplateInterface
 * is valid to be published as part of a @see PublishedVersion in that Template
 *
 * requires:
 * - info with label @see FormInfo to be set on the working version
 *
 * @see     WorkingVersionValidator
 */
final readonly class WorkingVersionFormInfoValidator implements WorkingVersionValidatorInterface
{
    /** collection of all error message */
    private InvalidPublishMessageCollection $errorMessages;

    private function __construct(private WorkingVersion $workingVersion)
    {
        $this->errorMessages = new InvalidPublishMessageCollection();

        if (!$this->workingVersion->hasInfo()) {
            $this->errorMessages->add(MissingFormInfoMessage::createMessage());
        }
    }

    public static function fromWorkingVersion(WorkingVersion $workingVersion): self
    {
        return new self($workingVersion);
    }

    public function allowsPublish(): bool
    {
        return $this->getErrorMessages()->isEmpty();
    }

    public function getErrorMessages(): InvalidPublishMessageCollection
    {
        return $this->errorMessages;
    }
}
