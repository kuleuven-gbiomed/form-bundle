<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;

final class LabelsDontMatchOnInputNodesInCategoryNodeMatrixMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.labelsDontMatchOnInputNodesInCategoryNodeMatrix';

    public static function createMessageForCategoryNodeMatrixAndQuestionLabelsAndCount(CategoryNode $matrix, array $uniqueQuestionsLocalizedLabels, int $questionCount): self
    {
        $matrixUid = $matrix->getUid();
        $subcategories = $matrix->getChildren()->getCategoryNodes();

        $invalidQuestionMessages = [];
        for ($questionKey = 0; $questionKey < $questionCount; ++$questionKey) {
            if (count(array_unique(array_column($uniqueQuestionsLocalizedLabels, $questionKey), \SORT_REGULAR)) > 1) {
                $invalidSubcategoryMessages = [];
                foreach ($uniqueQuestionsLocalizedLabels as $categoryKey => $uniqueQuestionsLocalizedLabel) {
                    /** @var CategoryNode $subcategory */
                    $subcategory = $subcategories->get($categoryKey);
                    $label = '[label unknown]';
                    if ($subcategory->getInfo()->getLocalizedLabel()->hasFallback()) {
                        $label = ' ['.$subcategory->getInfo()->getLocalizedLabel()->getFallback().']';
                    }

                    $nonMatchingLabel = $uniqueQuestionsLocalizedLabel[$questionKey];
                    // FIXME: print for all locales without using json_encode
                    $invalidSubcategoryMessages[] = 'category '.((string) ((int) $categoryKey + 1)).$label.': '.json_encode($nonMatchingLabel, \JSON_THROW_ON_ERROR);
                }
                // FIXME: messages are nested so how to handle translation??
                $invalidQuestionMessages[] = 'Question '.((string) ($questionKey + 1)).' uses the following labels for the questions: '
                .implode(' | ', $invalidSubcategoryMessages);
            }
        }

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the matrix with id [$matrixUid] should use the same label for the questions in each subcategory in the same order for all languages but they don't match: "
            .implode(' | ', $invalidQuestionMessages);

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%matrixId%' => $matrixUid,
            ]
        );
    }
}
