<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class InvalidOptionScoreValuesForGlobalScoreChoiceDependencyInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidOptionScoreValuesForGlobalScoreChoiceDependency';

    public static function createMessageForGlobalScoreNodeAndDependencyChoiceScoreNode(
        ScorableInputNode $globalScoreNode,
        ChoiceInputNode $dependencyChoiceScoreNode,
    ): self {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();
        $scoreQuestionLabel = $dependencyChoiceScoreNode->getNestedLabelForFallBackLocale();

        Assert::true(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->hasOneByNodeUid($dependencyChoiceScoreNode->getUid()),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyChoiceScoreNode->getUid()
            .'] is not referenced as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        $scoringParameters = $dependencyChoiceScoreNode->getInputScoringParameters();
        Assert::true(
            $scoringParameters->hasScoringExtrema(),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyChoiceScoreNode->getUid()
            .'] for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] has no minScore and/or maxScore configured!'
        );

        $scoringOptions = $dependencyChoiceScoreNode->getAvailableOptions()->getHavingScoringValue();
        Assert::false(
            $scoringOptions->isEmpty(),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyChoiceScoreNode->getUid()
            .'] for global score node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] has no available options with a scoring value!'
        );

        $minScoreValue = $scoringParameters->getMinScore();
        $maxScoreValue = $scoringParameters->getMaxScore();
        $invalidOptions = [];
        /** @var OptionInputNode $scoringOption */
        foreach ($scoringOptions as $scoringOption) {
            if ($scoringOption->getScoringValue() < $minScoreValue || $scoringOption->getScoringValue() > $maxScoreValue) {
                $invalidOptions[] = $scoringOption->getLabelForFallBackLocale();
            }
        }

        Assert::false(
            0 === count($invalidOptions),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyChoiceScoreNode->getUid()
            .'] for global score node ['.$globalScoreQuestionLabel.']['.$globalScoreNode->getUid()
            .'] has valid scoring values for each of its available scoring options!'
        );

        $invalidOptionLabels = implode(' | ', $invalidOptions);

        $minScore = (string) $minScoreValue;
        $maxScore = (string) $maxScoreValue;
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the choice question [$scoreQuestionLabel] is configured as a calculation dependency for the global score question [$globalScoreQuestionLabel]. but it has answer options that have a score value defined which does not fall within the minimum score [$minScore] and maximum score [$maxScore] range as configured on this choice question as calculation dependency. All answer options must have a numeric score value between the configured minimum score and maximum score if the choice question is to be used as a dependency for global scoring calculations. Please remove those answer Options or provide a valid score value for those options. those invalid answer options are: [$invalidOptionLabels]";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $scoreQuestionLabel,
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%invalidOptionLabels%' => $invalidOptionLabels,
                '%minScore%' => $minScore,
                '%maxScore%' => $maxScore,
            ]
        );
    }
}
