<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use Webmozart\Assert\Assert;

class InvalidOptionScoreValuesForChoiceGlobalScoreInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidOptionScoreValuesForChoiceGlobalScore';

    public static function createMessageForChoiceGlobalScoreNode(ChoiceInputNode $globalScoreNode): self
    {
        $locale = $globalScoreNode->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        $globalScoreUid = $globalScoreNode->getUid();
        Assert::false(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->isEmpty(),
            ' can not build message '.self::class
            ."; the given node [$globalScoreQuestionLabel][$globalScoreUid] is not configured as a global score node (no dependency configurations found)"
        );

        $scoringParameters = $globalScoreNode->getInputScoringParameters();
        Assert::true(
            $scoringParameters->hasScoringExtrema(),
            ' can not build message '.self::class
            .'; the choice global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] has no minSCore and/or maxScore configured to check the option scoring values against!'
        );

        $scoringOptions = $globalScoreNode->getAvailableOptions()->getHavingScoringValue();
        Assert::false(
            $scoringOptions->isEmpty(),
            ' can not build message '.self::class
            .'; the choice global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] has no available options with a scoring value!'
        );

        $minScoreValue = $scoringParameters->getMinScore();
        $maxScoreValue = $scoringParameters->getMaxScore();
        $invalidOptions = [];
        /** @var OptionInputNode $scoringOption */
        foreach ($scoringOptions as $scoringOption) {
            if ($scoringOption->getScoringValue() < $minScoreValue || $scoringOption->getScoringValue() > $maxScoreValue) {
                $invalidOptions[] = $scoringOption->getLabelForFallBackLocale();
            }
        }

        Assert::false(
            0 === count($invalidOptions),
            ' can not build message '.self::class
            .'; the choice global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] has valid scoring values for each of its available scoring options!'
        );

        $invalidOptionLabels = implode(' | ', $invalidOptions);

        $minScore = (string) $minScoreValue;
        $maxScore = (string) $maxScoreValue;
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the choice question [$globalScoreQuestionLabel] is configured as a global score question but it has answer options that have a score value defined which does not fall within the minimum score [$minScore] and maximum score [$maxScore] range as configured on the global score question. All answer options must have a numeric score value between the configured minimum score and maximum score if the choice question is to be used for global scoring calculations. Please remove those answer Options or provide a valid score value for those options. those invalid answer options are: [$invalidOptionLabels]";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%invalidOptionLabels%' => $invalidOptionLabels,
                '%minScore%' => $minScore,
                '%maxScore%' => $maxScore,
            ]
        );
    }
}
