<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

class InvalidSumOfDependencyWeighingsForGlobalScoreWithWeighingTypeAbsoluteInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidSumOfDependencyWeighingsForGlobalScoreWithWeighingTypeAbsolute';

    public static function createMessageForChoiceGlobalScoreNode(ScorableInputNode $globalScoreNode): self
    {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        $globalScoreUid = $globalScoreNode->getUid();
        Assert::false(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->isEmpty(),
            ' can not build message '.self::class
            ."; the given node [$globalScoreQuestionLabel][$globalScoreUid] is not configured as a global score node (no dependency configurations found)"
        );

        Assert::true(
            OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE === $globalScoreNode->getGlobalScoreWeighingType(),
            ' can not build message '.self::class
            .'; the choice global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] does not use weighingType "absolute" !'
        );

        Assert::false(
            $globalScoreNode->hasValidSumOfDependencyWeighingsForGlobalScoring(),
            ' can not build message '.self::class
            .'; the choice global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] has a valid sum for its dependency weighings: it is not 0 (zero)!'
        );

        $invalidWeighingSumString = (string) $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->getSumOfWeighings();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the question ['.$globalScoreQuestionLabel.'] is configured as a global score question and its calculation is configured to use the absolute global scoring weighing type to spread the weight of answers given for each of its dependency questions when calculating the resulting global score: the sum of the weighings you defined on each dependency question totals to 0 (zero), but that is not allowed when using absolute global scoring weighing type. Please make sure the sum of the weighings for the dependencies is not 0 (zero).';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%invalidWeighingSum%' => $invalidWeighingSumString,
            ]
        );
    }
}
