<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;

class InvalidSingleAndMultiLockedMessage extends InvalidFormListMessage
{
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidMultiLockedInFormList.bothSingleAndMultiLocked';

    public static function createForMultiLockedQuestion(ParentalInputNode $question): self
    {
        $locale = $question->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $questionLabel = $question->getNestedLabel($locale);
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": Question [$questionLabel][{$question->getUid()}] is locked with both OR and AND logic";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%',
                '%questionId%',
            ],
        );
    }
}
