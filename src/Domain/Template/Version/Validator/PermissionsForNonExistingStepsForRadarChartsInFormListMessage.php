<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Version\Validator\Helper\RadarChartWithPermissionsForNonExistingSteps;

final class PermissionsForNonExistingStepsForRadarChartsInFormListMessage extends InvalidFormListMessage
{
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.permissionsForNonExistingStepsForRadarChartsInFormList';

    /**
     * @param array<array-key, RadarChartWithPermissionsForNonExistingSteps> $radarChartsWithPermissionsForNonExistingSteps
     */
    public static function createMessageForRadarChartsWithPermissionsForNonExistingSteps(array $radarChartsWithPermissionsForNonExistingSteps): self
    {
        $questionLabelStepsStrings = [];
        foreach ($radarChartsWithPermissionsForNonExistingSteps as $radarChartWithPermissionsForNonExistingSteps) {
            $radarChartLabel = $radarChartWithPermissionsForNonExistingSteps->getRadarChartLabel();
            $stepPermissionUidsString = $radarChartWithPermissionsForNonExistingSteps->getStepPermissionUidsString();
            $questionLabelStepsStrings[] = "['$radarChartLabel' with permissions referring to non-existing steps '$stepPermissionUidsString']";
        }

        $list = implode(' || ', $questionLabelStepsStrings);

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": one or more radar charts use permissions that refer to steps that do not (or no longer) exist in the workFlow. those radar charts and their non-existing step permissions are: $list. "
            .'Those references to non-existing steps in the permissions for all those radar charts must be removed.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%list%' => $list,
            ]
        );
    }
}
