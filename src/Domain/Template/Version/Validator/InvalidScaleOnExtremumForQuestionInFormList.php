<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Utility\NumberScaleChecker;

final class InvalidScaleOnExtremumForQuestionInFormList extends InvalidFormListMessage
{
    public const EXTREMUM_TYPE_MINIMUM = 'minimum';
    public const EXTREMUM_TYPE_MAXIMUM = 'maximum';

    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidScaleOnExtremumForQuestion';

    public static function createMessageForQuestionAndExtremumType(
        InputNode $question,
        string $extremumType,
    ): self {
        $questionLabel = $question->getNestedLabelForFallBackLocale();

        if ($question instanceof ChoiceInputNode) {
            $extremumValue = self::getValueForExtremumForChoiceQuestion($question, $extremumType);
            $scale = self::getScaleForChoiceQuestion($question);
        } elseif ($question instanceof NumberInputNode) {
            $extremumValue = self::getValueForExtremumForNumberQuestion($question, $extremumType);
            $scale = $question->getInput()->getScale();
        } else {
            throw new \InvalidArgumentException('cannot handle questions of '.$question::class);
        }

        $scaleString = (string) $scale;
        if (!NumberScaleChecker::doesNumberExceedMaxAmountOfDecimals($extremumValue, $scale)) {
            throw new \BadMethodCallException(' can not build message '.self::class.'; the amount of decimals on the configured ['.$extremumType.'] of question ['.$questionLabel.']['.$question->getUid().'] does NOT exceed the max amount of decimals ['.$scaleString.'] configured on this question!');
        }

        $questionType = $question::getType();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the question [{$questionLabel}] of type [{$questionType}] has a [{$extremumType}] configured with number [{$extremumValue}]. But the amount of decimal(s) used in this number exceed the amount of [{$scaleString}] decimals that this question is
    configured to allow. This could become be at the very least confusing for the users consulting or filling in the form. Please either reduce the amount of decimals in this number or adjust the amount of decimals that this question is configured to allow.";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $questionLabel,
                '%questionType%' => $questionType,
                '%extremumType%' => $extremumType,
                '%extremumValue%' => $extremumValue,
                '%scale%' => $scaleString,
            ]
        );
    }

    private static function getValueForExtremumForNumberQuestion(NumberInputNode $question, string $extremumType): float
    {
        return match ($extremumType) {
            self::EXTREMUM_TYPE_MINIMUM => $question->getInput()->getMin(),
            self::EXTREMUM_TYPE_MAXIMUM => $question->getInput()->getMax(),
            default => throw new \InvalidArgumentException("unknown extremum type {$extremumType}"),
        };
    }

    private static function getValueForExtremumForChoiceQuestion(ChoiceInputNode $question, string $extremumType): float
    {
        if (self::EXTREMUM_TYPE_MINIMUM === $extremumType) {
            if (!$question->getInputScoringParameters()->hasMinScore()) {
                throw new \InvalidArgumentException('no minimum set on choice question');
            }

            return $question->getInputScoringParameters()->getMinScore();
        }

        if (self::EXTREMUM_TYPE_MAXIMUM === $extremumType) {
            if (!$question->getInputScoringParameters()->hasMaxScore()) {
                throw new \InvalidArgumentException('no maximum set on choice question');
            }

            return $question->getInputScoringParameters()->getMaxScore();
        }

        throw new \InvalidArgumentException("unknown extremum type {$extremumType}");
    }

    private static function getScaleForChoiceQuestion(ChoiceInputNode $question): int
    {
        if (!$question->getInputScoringParameters()->hasScale()) {
            throw new \InvalidArgumentException('no scale (amount of decimals) set on choice question');
        }

        return $question->getInputScoringParameters()->getScale();
    }
}
