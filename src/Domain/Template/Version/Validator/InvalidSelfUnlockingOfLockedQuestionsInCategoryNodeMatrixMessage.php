<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;

class InvalidSelfUnlockingOfLockedQuestionsInCategoryNodeMatrixMessage extends InvalidFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidSelfUnlockingOfLockedQuestionsInCategoryNodeMatrixMessage';

    public static function createMessageForCategoryNodeMatrix(
        CategoryNode $matrix,
    ): self {
        $allNodes = $matrix->getFlattenedWithAllDescendants();

        $allQuestions = $allNodes->getParentalInputNodes();
        $lockedQuestions = $allQuestions->getLocked();
        if (0 === $lockedQuestions->count()) {
            throw new \InvalidArgumentException('matrix cannot have self unlocking for locked questions: it has no locked questions at all.');
        }

        $invalidLockedQuestions = [];
        /** @var ParentalInputNode $lockedQuestion */
        foreach ($lockedQuestions as $lockedQuestion) {
            $unlockersInSameMatrix = $allNodes->getByNodeUids($lockedQuestion->getUnlockers()->getUids());
            if ($unlockersInSameMatrix->isEmpty()) {
                continue;
            }

            /** @var OptionInputNode $unlocker */
            foreach ($unlockersInSameMatrix as $unlocker) {
                /** @var ChoiceInputNode $choiceQuestion */
                $choiceQuestion = $unlocker->getParent();
                $invalidLockedQuestions[$lockedQuestion->getUid()][] = $choiceQuestion->getUid();
            }
        }

        if (0 === count($invalidLockedQuestions)) {
            throw new \InvalidArgumentException('matrix cannot have self unlocking for locked questions: all locked questions are unlocked by questions outside the matrix.');
        }

        $invalidList = '';

        $invalidLockedQuestionsAmount = 0;
        $invalidUnlockingQuestions = [];
        foreach ($invalidLockedQuestions as $lockedQuestionId => $unlockingQuestionIds) {
            ++$invalidLockedQuestionsAmount;
            $invalidUnlockingQuestions = [...$unlockingQuestionIds];
            $invalidList .= '<< question ['.$lockedQuestionId.'] is invalidly unlocked by question(s) ['.implode(' | ', $unlockingQuestionIds).'] >>';
        }

        $invalidUnlockingQuestionsAmountString = (string) count(array_unique($invalidUnlockingQuestions));
        $invalidLockedQuestionsAmountString = (string) $invalidLockedQuestionsAmount;
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': Locked questions in a matrix may not be unlocked by (a) question (unlocker) or questions (chain of unlockers)'
            .' that belong to that same matrix. found '.$invalidLockedQuestionsAmountString.' questions in matrix ['.$matrix->getUid()
            .'] that are unlocked by in total '.$invalidUnlockingQuestionsAmountString.' question(s) within this same matrix:'
            .' those questions that are invalidly unlocked by (a) question(s) in the same matrix are: ['.$invalidList.']."';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%matrix%' => $matrix->getUid(),
                '%invalidLockedQuestionsAmount%' => $invalidLockedQuestionsAmountString,
                '%invalidUnlockingQuestionsAmount%' => $invalidUnlockingQuestionsAmountString,
                '%invalidList%' => $invalidList,
            ]
        );
    }
}
