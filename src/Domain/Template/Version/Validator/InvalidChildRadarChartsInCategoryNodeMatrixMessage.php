<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;

final class InvalidChildRadarChartsInCategoryNodeMatrixMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidChildRadarChartsInCategoryNodeMatrix';

    public static function createMessageForCategoryNodeMatrix(CategoryNode $matrix): self
    {
        $matrixUid = $matrix->getUid();
        $matrixLabel = '[label unknown]';
        if ($matrix->getInfo()->getLocalizedLabel()->hasFallback()) {
            $matrixLabel = ' ['.$matrix->getInfo()->getLocalizedLabel()->getFallback().']';
        }

        $invalidNodesCount = $matrix->getChildren()->getReadOnlyViewNodes()->getRadarChartNodes()->count();

        if (0 === $invalidNodesCount) {
            throw new \Exception(' can not build message '.self::class.": the matrix category [$matrixLabel] with id [$matrixUid] has no radar chart(s) as children!");
        }

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the matrix category [$matrixLabel] with id [$matrixUid] has [$invalidNodesCount] radar chart(s) as children, "
            .'but it can only have category nodes as children.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%matrixId%' => $matrixUid,
                '%matrixLabel%' => $matrixLabel,
                '%invalidNodesCount%' => $invalidNodesCount,
            ]
        );
    }
}
