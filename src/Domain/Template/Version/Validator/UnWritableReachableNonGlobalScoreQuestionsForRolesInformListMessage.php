<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;
use Webmozart\Assert\Assert;

final class UnWritableReachableNonGlobalScoreQuestionsForRolesInformListMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.unWritableReachableNonGlobalScoreQuestionsForRolesInformList';

    /** @param InputNodeCollection<array-key, InputNode> $unWritableQuestions */
    public static function createMessageForQuestionsAndRoles(
        InputNodeCollection $unWritableQuestions,
        RoleCollection $roles,
    ): self {
        $rolesString = implode(' | ', array_map(fn (Role $role) => $role->getName(), $roles->toArray()));

        $questionLabels = implode(' | ', array_map(fn (InputNode $inputNode): string =>
            // an input Node always has a label asserted for at least one locale (i.e. fallback, usually default locale)
            // using nested label (label prepended with all its ancestral labels) will take up more space in error
            // message, but since question(label)s don't have to be unique, this will help to identify questions inside categories
            $inputNode->getNestedLabelForFallBackLocale(), $unWritableQuestions->toArray()));

        $notUnWritableQuestions = [];
        $globalScoreQuestions = [];
        /** @var InputNode $unWritableReachableQuestion */
        foreach ($unWritableQuestions->getUnWritableForNoneOfRoles($roles) as $unWritableReachableQuestion) {
            if ($unWritableReachableQuestion->getFlowPermission()->isWriteAccessGrantedForAtLeastOneRoleInAtLeastOneStep()) {
                $notUnWritableQuestions[] = $unWritableReachableQuestion;
                continue;
            }

            // questions intended as global score nodes can be read-only, without write access for any roles
            if ($unWritableReachableQuestion instanceof ScorableInputNode && !$unWritableReachableQuestion->getAllGlobalScoreCalculationScoreNodeConfigs()->isEmpty()) {
                $globalScoreQuestions[] = $unWritableReachableQuestion;
            }
        }

        Assert::true(
            0 === count($notUnWritableQuestions),
            ' can not build message '.self::class
            .'; at least one of the given questions has write access.'
        );

        Assert::true(
            0 === count($globalScoreQuestions),
            ' can not build message '.self::class
            .'; at least one of the given questions is intended as global score question and is as such allowed to have no write access for any role in any step.'
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': Er is minstens één vraag dat voor geen enkele stap schrijfrechten heeft. De vragen zijn: ['.$questionLabels.']. Zorg ervoor dat er iedere vraag in minstens één stap schrijfrechten heeft.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%questionLabels%' => $questionLabels, '%roles%' => $rolesString]
        );
    }
}
