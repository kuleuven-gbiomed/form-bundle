<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\PrefillableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\StepPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Question\AbstractParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\AbstractReadOnlyViewNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart\ParticipationQuestionEligibility;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\CalculationScoreNodeConfigCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\StepPermissionCollection;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Utility\Collection\CategoryNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\ReadOnlyViewNodeCollection;
use KUL\FormBundle\Domain\Template\Version\PublishedVersion;
use KUL\FormBundle\Domain\Template\Version\Validator\Helper\QuestionWithPermissionsForNonExistingSteps;
use KUL\FormBundle\Domain\Template\Version\Validator\Helper\RadarChartWithPermissionsForNonExistingSteps;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Utility\NumberScaleChecker;

/**
 * Class WorkingVersionFormListValidator.
 *
 * checks if a {@see FormList}
 * in a {@see WorkingVersion}
 * in a {@see TemplateInterface}
 * is valid to be published as part of a {@see PublishedVersion} in that Template
 *
 * requires:
 * - a form list {@see FormList} (no FormList (yet) is possible when admins are just starting on the working version)
 * - at least one reachable (not (in)directly hidden) question in the formList so that there at least one question
 *   to be shown and/or filled in at some point
 * - a workFlow with at least one step so to be able to verify that there are at least questions reachable & accessible
 *   for each step. whether that workFlow has overlap or not is not the responsibility of this validator and thus ignored.
 * - for each of the steps in the workFlow there must be at least one reachable question in the formList that grants
 *   at least read access (either directly granted or indirectly via the higher ranked write access) for at least one of
 *   the given participatable roles (as set by {@see TemplateInterface::getHierarchicalRolesThatCouldParticipate())} so that there is
 *   something to show and/or do in each step by at least one role (1) (2)
 *
 * NOTES
 * (1) based on common sense for publishing an {@see TemplateInterface} with a workflow and formList:
 * if for a step in given {@see WorkFlow}, no questions are available or accessible for any of the given roles
 * as defined by {@see TemplateInterface::getHierarchicalRolesThatCouldParticipate()}, then that step is completely useless and
 * would create a void in time in which no end-users (clients) (including the ones with admin roles in given roles) will have
 * any access. example if we don't check this and allow steps with no available & accessible questions: some end-user (client)
 * submits in one step (that had available & accessible questions). that triggers the next step to open, but that
 * next step has no available & accessible question for any role. which would make the form 'disappear' completely
 * during the period that this next step runs, even for admins. because no one has any access and anything to do in
 * that next step. would also most definitely result in loads more of 'access denied' pages if an end-user (client) had
 * previously some access to a step that had available & accessible question, and know accesses the form e.g. via
 * an url he/she stored, was send by mail or was given by another end-user (client) while it is in a step that has no
 * available & accessible questions. the latter can anyway always occur if that end-user (client) has no more access with
 * one of his/her roles in a step that has available & accessible questions for other roles, but still... we want
 * to avoid as much illogical 'access denied' as possible
 *
 * (2) it would also be preferable to enforce that each reachable question is writable by at least one role in at least
 * one step in the workFlow. common sense dictates that if there is a question that is available in at least one step
 * by at least one role but it never has any write access for any role (only read access by at least one role), then
 * that question can never get any answer! that does not seem to make sense because what is the point of a question
 * that can never get an answer? but the domain decision is made to not enforce this, instead rely on admins having
 * common sense and displaying informal warnings about this in the admin side. the main reason not to enforce this is
 * because a question {@see OperableForScoring::operatesAsGlobalScoreNode()} whose answer is auto calculated is in reality
 * often just read-only for most end-user (client) roles. usually the admins will (should) give themselves write access to this
 * type of question in a later step so they can manually override the auto-calculated value if needed, but - despite
 * comments in (3) - we don't want to enforce admins to give themselves write access to this type or any type of question.
 *
 * the check in (1) on having for each step a reachable question with read access for at least one of the participating
 * roles, does not exclude the possibility that there will be questions that grant no access at all for any of the
 * participating roles and/or in any step: an admin may choose to remove all access from question, so that no roles will
 * have access, in any step. but there must be left at least one question in each step that grants some role at least
 * read access, so that the step will not become dead in time. in practice, having a question permanently without any
 * access will rarely happen, but might happen e.g as a temporary admin edit for whatever reason.
 *
 * based on (1) and (2), it is advisable that admins building questions and assigning
 * permissions based on workflow and roles, should at least grant themselves some access (sensibly write access)
 * in all steps. but if they don't care about being able to change answers or even just see the answers, they can
 * completely deny themselves access and let only other end-user (client) see & do stuff on questions in steps. therefore,
 * the admin side should by default set write access for admin roles on all questions in all steps, from as soon
 * as write access is set for one other end-user (client) role, while give admins the chance to revoke that default write access.
 *
 * {@see     WorkingVersionValidator}
 */
final class WorkingVersionFormListValidator implements WorkingVersionValidatorInterface
{
    /**
     * roles to verify minimal accessibility to questions in each step of the workingVersion's workflow
     * (not all roles have to have access. to have a valid set of questions in a workingVersion's workFlow, for each
     * step in the workFlow there must be at least one of these roles that has at least read access to at least one
     * question.
     */
    private readonly RoleCollection $participatableRoles;

    private InvalidPublishMessageCollection $errorMessages;

    /**
     * @throws MissingNodeException
     */
    private function __construct(private readonly WorkingVersion $workingVersion, RoleCollection $participatableRoles)
    {
        if ($participatableRoles->isEmpty()) {
            throw new \InvalidArgumentException('at least one role [participatableRoles] required to verify accessibility in the workingVersion');
        }
        $this->participatableRoles = $participatableRoles;

        $this->buildErrorMessages();
    }

    /**
     * @throws MissingNodeException
     */
    public static function fromWorkingVersionAndParticipatableRoles(
        WorkingVersion $workingVersion,
        RoleCollection $participatableRoles,
    ): self {
        return new self($workingVersion, $participatableRoles);
    }

    public function allowsPublish(): bool
    {
        return $this->getErrorMessages()->isEmpty();
    }

    public function getErrorMessages(): InvalidPublishMessageCollection
    {
        return $this->errorMessages;
    }

    /**
     * NOTE: we only just check the parental input nodes (= the actual questions and not the input nodes that only
     * operate as dependencies on parental input nodes e.g. the option nodes for a choice question).
     * non-parental nodes like the {@see OptionInputNode} also have access set (copied from) via their
     * parental {@see ChoiceInputNode} and so they could be included in the access (and validity like reachablility)
     * checks in this method as well, but since they depend solely on the parent's access & validity for them to be
     * shown, used, etc. in the first (and last) palace, we can entirely skip checking explicit access & validity
     * for those.
     * the {@see OptionInputNode::getFlowPermission()} to options is copied from their parental choice question upon
     * each update of the choice anyway and the system only checks access to the parental choice for each action.
     *
     * NOTE: we only need to check if the  currently reachable (@throws MissingNodeException
     * {@see InputNode::isReachableInTree()} ) questions have
     * write access at some point:  if an unreachable question without write access becomes reachable again, then
     * that is done by a re-publish, which will again do this whole validation check again.
     *
     * NOTE: we check each step for having at least one readable and one reachable question. this is needed because
     * if a step has no reachable questions, the step would be blank in the sense that if the flow reaches that step,
     * nothing would be shown! we also need to make sure that at least one role has at least read access to at least one
     * question in the step. for exactly the same reason: there might be questions to shown in the step but no one
     * will ever see them because no one would have access to them! in other words: every step must have at least
     * have one question showable to at least one role. we could do one check per step on both reachable and readable
     * questions and put all of this in one error, but because of the importance of it, I decided to show that in
     * two separate error messages, with the reachable building further on the readable questions found in the step.
     * one for the steps with missing readable questions: {@see NoReachableAccessibleQuestionsAndReadOnlyViewNodesForRolesInStepsMessage}
     * and one for the steps with missing reachable questions: {@see NoAccessibleQuestionsAndReadOnlyViewNodesForRolesInStepsMessage}
     * (where the latter only adds steps if at least one readable is found)
     */
    private function buildErrorMessages(): void
    {
        $this->errorMessages = new InvalidPublishMessageCollection();

        // when a child node (question, optionNode or category) or set of child nodes is added to or updated in a categoryNode
        // or ChoiceNode, an assert will make sure that no uuid is used twice by those child nodes. but that assert can
        // only check first children nodes level. it is possible that a child node with an already existing uuid is added
        // on another level up or down the node tree of the parent node to which it is being added. off course, upon each
        // adding of a child, we could go look up and down the tree to see if that child node does not use an uuid that
        // is already used by another node, but that requires that the parent to which it is added is ALWAYS aware of the full tree,
        // and that is not always the case (e.g. during scripting we build and add (parent) nodes in bits and pieces).
        // when an uuid is used twice, it will/could result in both being rendered with one or both being corrupted,
        // unrenderable, unworkable, ... something that will quickly be visually noticed when rendering a new form for
        // the first time, but still: not acceptable and more than enough reason to block any publish of a working version
        $duplicateUids = $this->workingVersion->getFormList()->getFlattenedNodes()->getDuplicateNodeUids();
        if (0 < count($duplicateUids)) {
            $this->errorMessages->add(NonUniqueNodeIdsUsedInFormListMessage::createMessageForNodeIds($duplicateUids));

            return; // unique ids are too important to continue, immediately halt validation instead of validating further
        }

        $workingVersionFormList = $this->workingVersion->getFormList();
        // get all questions (ignoring category nodes and child nodes of questions like options of choice questions)
        // we only need to check the parental nodes, because only those compose the actual questions
        $questions = $workingVersionFormList->getFlattenedParentalInputNodes();

        // at least one question in the formList is needed to make it possible to build a form
        if ($questions->isEmpty()) {
            $this->errorMessages->add(NoQuestionsInFormListMessage::createMessage());

            return; // no point in checking further if no questions at all
        }

        // filter out the questions that not (in)directly hidden = reachable questions
        $reachableQuestions = $questions->getReachableInTree();

        // at least one question in the formList must be reachable to make it possible to build a form
        if ($reachableQuestions->isEmpty()) {
            $this->errorMessages->add(NoReachableQuestionsInFormListMessage::createMessage());

            return; // no point in checking further if no reachable questions at all
        }

        // we need to check the questions against the workflow in the same working versions
        $workFlow = $this->workingVersion->hasWorkFlow() ? $this->workingVersion->getWorkFlow() : null;

        // if no workflow, no question will ever be available. or if no steps in workflow, we can simply not check access
        if (!$workFlow instanceof WorkFlow || $workFlow->getSteps()->isEmpty()) {
            $this->errorMessages->add(MissingWorkFlowForQuestionsReleaseMessage::createMessage());

            return; // no point in checking further if no workFlow
        }

        // if a step is removed from workflow or a step simply does not exist in the workflow, then the admin should also
        // remove the existing permissions for that step on all questions, choice option answer nodes and chart nodes as well.
        $this->errorMessages = $this->errorMessages->getMergedWith($this->getNodesWithPermissionsForNonExistingStepsErrorMessages($workingVersionFormList, $workFlow));

        // every non-hidden (parental) question (including global scores!) must be writable for at least one role in at least
        // one step, so that at least someone / some role can actually answer the question.
        // the only exception are global score questions. those are usually auto-calculated and therefore could be read-only
        // for all roles at all times. however it is still better to grant write access to admin roles at some point,
        // e.g. in the last step or the first step after which the auto-calculation is supposed to have occurred,
        // to make it possible for admins to change that answer if needed, or fill it in manually if it could not be
        // auto-calculated for some reason (e.g. because form went passed deadline without any user have supplied answers
        // for the dependency questions for that global score node and those dependency questions can no longer be filled in) .
        // note: we could do this check before we check the workflow, because no workflow means no steps, means no access
        // even possible, but seems better to first mention the missing workflow or workflow without step before going this deeper
        // note: at this point, the global score nodes dos not have to be valid itself as global score. that check is done
        // later in this service. we just check if the node has dependency questions, which means it is intended as global score node.
        /** @var array<array-key, InputNode> $unWritableReachableQuestions */
        $unWritableReachableQuestions = [];
        foreach ($reachableQuestions->getUnWritableForNoneOfRoles($this->participatableRoles) as $unWritableReachableQuestion) {
            // don't add non-writable question if it is intended as global score node (scorable and having scoring dependencies)
            if ($unWritableReachableQuestion instanceof ScorableInputNode && !$unWritableReachableQuestion->getAllGlobalScoreCalculationScoreNodeConfigs()->isEmpty()) {
                continue;
            }

            $unWritableReachableQuestions[] = $unWritableReachableQuestion;
        }

        // if there are any (non-global score) questions without write access, add error and continue checking further for other invalid states
        if (0 < count($unWritableReachableQuestions)) {
            $this->errorMessages->add(
                UnWritableReachableNonGlobalScoreQuestionsForRolesInformListMessage::createMessageForQuestionsAndRoles(
                    new InputNodeCollection($unWritableReachableQuestions),
                    $this->participatableRoles
                )
            );
        }

        // similar as above for readOnlyView nodes: every readOnlyView node must be reachable not (indirectly) hidden and
        // readable (readOnlyView nodes never grant write access) for at least one role in at least one step, so that at
        // least someone can actually view it at some point. else it's pointless to have in the form.
        $readOnlyViewNodes = $workingVersionFormList->getFlattenedReadOnlyViewNodes();
        $reachableReadOnlyViewNodes = $workingVersionFormList->getFlattenedReachableReadOnlyViewNodes();
        $unReadableReachableReadOnlyViewNodes = $reachableReadOnlyViewNodes->getUnReadableForNoneOfRoles($this->participatableRoles);

        // if there are any readOnlyView node without read access, add error and continue checking further for other invalid states
        if (!$unReadableReachableReadOnlyViewNodes->isEmpty()) {
            $this->errorMessages->add(
                UnReadableReachableReadOnlyViewNodesForRolesInformListMessage::createMessageForReadOnlyViewNodesAndRoles(
                    $unReadableReachableReadOnlyViewNodes,
                    $this->participatableRoles
                )
            );
        }

        // check if questions using minimum & maximum extrema have valid values for those extrema.
        $this->errorMessages = $this->errorMessages->getMergedWith($this->getQuestionExtremaAndScoringScaleErrorMessages($questions));

        // check if nodes marked for global scoring are valid and have valid dependencies
        $this->errorMessages = $this->errorMessages->getMergedWith($this->getGlobalScoringErrorMessages($questions));

        // checking that each step has at least one readable and one reachable question or readOnlyView node. for this,
        // filter the questions & readOnlyView nodes on at least read access for at least one of the participating roles
        // in at least one step. we need all questions & readOnlyView nodes, not just the reachable ones. because we need
        // all of them to be able to check and assert that all steps have at least one reachable question and one readable
        // question so that no step will be blank and/or halt the flow due to being 'empty' (not rendering anything for no one).
        $readableQuestions = $questions->getReadableForAtLeastOneOfRoles($this->participatableRoles);
        $readableReadOnlyViewNodes = $readOnlyViewNodes->getReadableForAtLeastOneOfRoles($this->participatableRoles);

        $steps = $workFlow->getSteps();

        $stepsWithNoAccessibleQuestionsAndReadOnlyViewNodes = [];
        $stepsWithUnreachableAccessibleQuestionsAndReadOnlyViewNodes = [];

        // each step must have at least one question or readOnlyView node with read access for at least one role in that step:
        // achieving this by checking steps for having at least one question or readOnlyView node with at least read access and -
        // if there are such 'accessible' questions and/or readOnlyView nodes - if one of those questions and/or readOnlyView nodes
        // is reachable at all.
        // note: a step with at least one readable, reachable readonlyView node but without any readable, reachable questions is enough
        // to make that step 'workable': it means the step as a whole is readonly, thus not submittable, and users will have to wait for
        // the step's deadline to pass for the next step to open, instead of opening the next step prematurely by an action (i.e.
        // submitting questions). that is similar to a step that has no reachable readOnlyView nodes and only readable (non writable)
        // reachable questions: in this case the step is also read-only as a whole, just showing question answers.
        // (note that premature opening of a step by a submitting action in it's preceding step is an option, not default behaviour,
        // although it is widely used in reality on most steps)
        foreach ($steps as $step) {
            $readableQuestionsInStep = $readableQuestions->getReadableInStep($step);
            $readableReadOnlyViewNodesInStep = $readableReadOnlyViewNodes->getReadableInStep($step);

            if ($readableQuestionsInStep->isEmpty() && $readableReadOnlyViewNodesInStep->isEmpty()) {
                $stepsWithNoAccessibleQuestionsAndReadOnlyViewNodes[] = $step;

                // if there are no questions and readOnlyView nodes with minimal access, continue and skip further check
                // on reachable, because a question without any access in the step is already bad enough.
                continue;
            }

            // if there are readable questions and/or readOnlyView nodes in the step, but none are reachable (directly
            // marked as hidden or indirectly hidden as part of a hidden category or hidden ancestral category)
            if ($readableQuestionsInStep->getReachableInTree()->isEmpty() && $readableReadOnlyViewNodesInStep->getReachableInTree()->isEmpty()) {
                $stepsWithUnreachableAccessibleQuestionsAndReadOnlyViewNodes[] = $step;
            }
        }

        if ([] !== $stepsWithNoAccessibleQuestionsAndReadOnlyViewNodes) {
            $this->errorMessages->add(
                NoAccessibleQuestionsAndReadOnlyViewNodesForRolesInStepsMessage::createMessageForStepsAndRoles(
                    $stepsWithNoAccessibleQuestionsAndReadOnlyViewNodes,
                    $this->participatableRoles
                )
            );
        }

        if ([] !== $stepsWithUnreachableAccessibleQuestionsAndReadOnlyViewNodes) {
            $this->errorMessages->add(
                NoReachableAccessibleQuestionsAndReadOnlyViewNodesForRolesInStepsMessage::createMessageForStepsAndRoles(
                    $stepsWithUnreachableAccessibleQuestionsAndReadOnlyViewNodes,
                    $this->participatableRoles
                )
            );
        }

        $rootNode = $workingVersionFormList->getRootNode();

        // check for issues with dropdown questions.
        // (non-multiple dropdown questions using 'other' answer option, are using no more than one 'other' answer option.)
        $this->errorMessages = $this->errorMessages->getMergedWith($this->getChoiceQuestionsWithOtherOptionErrorMessages($questions->getChoiceInputNodes()));

        // checks for issues of prefillable questions
        $this->errorMessages = $this->errorMessages->getMergedWith($this->getPrefillableQuestionsErrorMessages($questions, $workFlow));

        // checks for invalid prefillable choice questions
        $this->errorMessages = $this->errorMessages->getMergedWith($this->getPrefillableChoiceQuestionsErrorMessages($questions));

        // checks for invalid matrices
        $this->errorMessages = $this->errorMessages->getMergedWith($this->getCategoryMatrixErrorMessages($rootNode->getFlattenedCategoryNodeDescendants()));

        // check if each readOnlyView node is valid (regardless of reachable and/or readable).
        $this->errorMessages = $this->errorMessages->getMergedWith($this->getReadOnlyViewNodesErrorMessages($readOnlyViewNodes));

        // check if text questions are correctly/sanely configured
        $this->errorMessages = $this->errorMessages->getMergedWith($this->getTextNodesErrorMessages($questions->getTextInputNodes()));

        // check if questions are not both single and multi locked
        $this->errorMessages = $this->errorMessages->getMergedWith($this->getMultiLockedErrorMessages($questions->getMultiLocked()));
    }

    /** @param InputNodeCollection<array-key, AbstractParentalInputNode> $questions */
    private function getMultiLockedErrorMessages(InputNodeCollection $questions): InvalidPublishMessageCollection
    {
        $messages = [];

        if (false === $questions->isEmpty()) {
            $singleLockedQuestionIds = array_reduce(
                $this->workingVersion->getFormList()->getFlattenedInputNodes()->toArray(),
                /**
                 * @param string[] $singleLockedIds
                 *
                 * @return string[]
                 */
                function (array $singleLockedIds, InputNode $inputNode): array {
                    if ($inputNode instanceof OptionInputNode && 0 < count($lockedIds = $inputNode->getUnlocksQuestionsIds())) {
                        return array_merge($singleLockedIds, $lockedIds);
                    }

                    return array_unique($singleLockedIds);
                },
                []
            );

            /** @var ParentalInputNode $question */
            foreach ($questions as $question) {
                if (in_array($question->getUid(), $singleLockedQuestionIds, true)) {
                    $messages[] = InvalidSingleAndMultiLockedMessage::createForMultiLockedQuestion($question);
                }
            }
        }

        return new InvalidPublishMessageCollection($messages);
    }

    /** @param ReadOnlyViewNodeCollection<array-key, AbstractReadOnlyViewNode> $readOnlyViewNodeCollection */
    private function getReadOnlyViewNodesErrorMessages(
        ReadOnlyViewNodeCollection $readOnlyViewNodeCollection,
    ): InvalidPublishMessageCollection {
        // only radar charts at time of writing.
        $messages = $this->getRadarChartViewNodesErrorMessages($readOnlyViewNodeCollection->getRadarChartNodes())->toArray();

        return new InvalidPublishMessageCollection($messages);
    }

    /** @param InputNodeCollection<array-key, TextInputNode> $textQuestions */
    private function getTextNodesErrorMessages(
        InputNodeCollection $textQuestions,
    ): InvalidPublishMessageCollection {
        $messages = $this->getTextNodePredefinedErrorMessages($textQuestions)->toArray();

        return new InvalidPublishMessageCollection($messages);
    }

    /**
     * making use of 'predefined answers' is (currently) only implemented for textarea (multiline text) questions.
     * for regular text questions it won't work at end-users's side. it would not break anything, it will just not provide the
     * 'add predefined answers' option when rendered. but since it won't do anything, and we want to make sure 'predefined answers'
     * are not accidentally added to a non-textarea question in a script or form-builder, we block it as a good practice.
     *
     * end-user implementation of predefined answers {@see src/Resources/views/form/formlist/panelview/embed/text_input_node_macros.html.twig:29}.
     *
     * @param InputNodeCollection<array-key, TextInputNode> $textQuestions
     */
    private function getTextNodePredefinedErrorMessages(
        InputNodeCollection $textQuestions,
    ): InvalidPublishMessageCollection {
        $messages = [];

        foreach ($textQuestions as $question) {
            if ($question->getInput()->isMultiLine()) {
                continue;
            }

            if ($question->getInput()->hasPredefinedAnswers()) {
                $messages[] = InvalidUseOfPredefinedAnswersOnTextLikeQuestionInFormList::createMessageForQuestion($question);
            }
        }

        return new InvalidPublishMessageCollection($messages);
    }

    /** @param InputNodeCollection<array-key, InputNode> $questions */
    private function getQuestionExtremaAndScoringScaleErrorMessages(
        InputNodeCollection $questions,
    ): InvalidPublishMessageCollection {
        $messages = [];

        foreach ($questions as $question) {
            if ($question instanceof NumberInputNode) {
                $messages = array_merge(
                    $messages,
                    $this->getNumberQuestionExtremaErrorMessages($question)->toArray()
                );

                continue;
            }

            if ($question instanceof ChoiceInputNode) {
                $messages = array_merge(
                    $messages,
                    $this->getChoiceQuestionExtremaErrorMessages($question)->toArray()
                );
                $messages = array_merge(
                    $messages,
                    $this->getChoiceQuestionAnswerOptionsScoreScaleErrorMessages($question)->toArray()
                );
            }
        }

        return new InvalidPublishMessageCollection($messages);
    }

    private function getNumberQuestionExtremaErrorMessages(
        NumberInputNode $question,
    ): InvalidPublishMessageCollection {
        $messages = [];

        $input = $question->getInput();

        if (NumberScaleChecker::doesNumberExceedMaxAmountOfDecimals($input->getMin(), $input->getScale())) {
            $messages[] = InvalidScaleOnExtremumForQuestionInFormList::createMessageForQuestionAndExtremumType(
                $question,
                InvalidScaleOnExtremumForQuestionInFormList::EXTREMUM_TYPE_MINIMUM
            );
        }

        if (NumberScaleChecker::doesNumberExceedMaxAmountOfDecimals($input->getMax(), $input->getScale())) {
            $messages[] = InvalidScaleOnExtremumForQuestionInFormList::createMessageForQuestionAndExtremumType(
                $question,
                InvalidScaleOnExtremumForQuestionInFormList::EXTREMUM_TYPE_MAXIMUM
            );
        }

        return new InvalidPublishMessageCollection($messages);
    }

    private function getChoiceQuestionExtremaErrorMessages(
        ChoiceInputNode $question,
    ): InvalidPublishMessageCollection {
        $messages = [];

        $scoringParams = $question->getInputScoringParameters();

        // no scale means any amount of decimals is good (if a min or max even exists):
        // note that technically it is possible to have a choice question without a scale but with min & max or without a min
        // and/or max but with scale: it won't be able to operate as a score or globals score, but it might deliberately
        // 'benefit' from the display possibilities (if any) of score questions & its options.
        if (!$scoringParams->hasScale()) {
            return new InvalidPublishMessageCollection($messages);
        }

        $scale = $scoringParams->getScale();

        if ($scoringParams->hasMinScore() && NumberScaleChecker::doesNumberExceedMaxAmountOfDecimals($scoringParams->getMinScore(), $scale)) {
            $messages[] = InvalidScaleOnExtremumForQuestionInFormList::createMessageForQuestionAndExtremumType(
                $question,
                InvalidScaleOnExtremumForQuestionInFormList::EXTREMUM_TYPE_MINIMUM
            );
        }

        if ($scoringParams->hasMaxScore() && NumberScaleChecker::doesNumberExceedMaxAmountOfDecimals($scoringParams->getMaxScore(), $scale)) {
            $messages[] = InvalidScaleOnExtremumForQuestionInFormList::createMessageForQuestionAndExtremumType(
                $question,
                InvalidScaleOnExtremumForQuestionInFormList::EXTREMUM_TYPE_MAXIMUM
            );
        }

        return new InvalidPublishMessageCollection($messages);
    }

    private function getChoiceQuestionAnswerOptionsScoreScaleErrorMessages(
        ChoiceInputNode $question,
    ): InvalidPublishMessageCollection {
        $messages = [];

        $scoringParams = $question->getInputScoringParameters();

        // no scale means any amount of decimals on score values of answer options is good (if such score values even exists):
        // note that technically it is possible to have a choice question without a scale but with score values on its answer
        // options or without score values on its answer options but with scale: it won't be able to operate as a score or
        // global score, but it might deliberately 'benefit' from the display possibilities (if any) of score values on its options.
        if (!$scoringParams->hasScale()) {
            return new InvalidPublishMessageCollection($messages);
        }

        $scale = $scoringParams->getScale();

        // check decimals on score value of each option
        foreach ($question->getChildren()->getHavingScoringValue()->toArray() as $optionNode) {
            if (NumberScaleChecker::doesNumberExceedMaxAmountOfDecimals($optionNode->getScoringValue(), $scale)) {
                $messages[] = InvalidScaleOnScoringValueOfChoiceQuestionAnswerOptionsInFormList::createMessageForChoiceQuestion(
                    $question
                );
                // don't bother looking for more: at least one is enough to build error message
                break;
            }
        }

        return new InvalidPublishMessageCollection($messages);
    }

    /** @param ReadOnlyViewNodeCollection<array-key, RadarChartViewNode> $radarChartCollection */
    private function getRadarChartViewNodesErrorMessages(
        ReadOnlyViewNodeCollection $radarChartCollection,
    ): InvalidPublishMessageCollection {
        $messages = [];

        /** @var RadarChartViewNode $radarChartViewNode */
        foreach ($radarChartCollection as $radarChartViewNode) {
            $messages = array_merge(
                $messages,
                $this->getRadarChartViewNodeErrorMessages($radarChartViewNode)->toArray()
            );
        }

        return new InvalidPublishMessageCollection($messages);
    }

    private function getRadarChartViewNodeErrorMessages(
        RadarChartViewNode $radarChartViewNode,
    ): InvalidPublishMessageCollection {
        $messages = [];

        $failedParticipationEligibilitySet = $radarChartViewNode->getFailedParticipationQuestionEligibilitySet();
        if ($failedParticipationEligibilitySet->isEmpty()) {
            return new InvalidPublishMessageCollection($messages);
        }

        /** @var ParticipationQuestionEligibility $failedParticipationEligibility */
        foreach ($failedParticipationEligibilitySet as $failedParticipationEligibility) {
            if ($failedParticipationEligibility->refersToNonExistingQuestion()) {
                $messages[] = MissingParticipatingQuestionForRadarChartNodeInFormListMessage::createMessageForRadarChartAndParticipationEligibility($radarChartViewNode, $failedParticipationEligibility);
                // no question found, skip further checks
                continue;
            }

            if ($failedParticipationEligibility->refersToNonPlottableQuestion()) {
                $messages[] = NonPlottableParticipatingQuestionForRadarChartNodeInFormListMessage::createMessageForRadarChartAndParticipationEligibility($radarChartViewNode, $failedParticipationEligibility);
                // no plottable question found, skip further checks
                continue;
            }

            if ($failedParticipationEligibility->isMissingMinimumAnswerValue()) {
                $messages[] = MissingRangeRequirementsOnParticipatingQuestionForRadarChartNodeInFormListMessage::createMessageForRadarChartAndParticipationEligibility($radarChartViewNode, $failedParticipationEligibility);
            }

            // only choice questions can have invalid answer options, no need to check question type here.
            if ($failedParticipationEligibility->hasChoiceQuestionAnswerOptionsWithMissingScoreValues()) {
                $messages[] = MissingAnswerOptionScoringValuesOnParticipatingChoiceQuestionForRadarChartNodeInFormListMessage::createMessageForRadarChartAndParticipationEligibility($radarChartViewNode, $failedParticipationEligibility);
            }
        }

        return new InvalidPublishMessageCollection($messages);
    }

    /** @param InputNodeCollection<array-key, InputNode> $questions */
    private function getPrefillableChoiceQuestionsErrorMessages(
        InputNodeCollection $questions,
    ): InvalidPublishMessageCollection {
        $messages = [];

        foreach ($questions->getPrefillableChoiceInputNodesHavingPrefillingQuestions() as $prefillableChoiceInputNode) {
            /** @var ChoiceInputNode $prefillableChoiceInputNode */
            if ($prefillableChoiceInputNode->getInput()->isMultiple()) {
                $messages[] = PrefillableChoiceIsMultipleMessage::createMessageForPrefillableChoice($prefillableChoiceInputNode);
            }

            if (!$prefillableChoiceInputNode->hasValidPrefillingQuestions($questions->getChoiceInputNodes())) {
                $messages[] = ChoicePrefillOptionsDifferMessage::createMessageForPrefillableQuestion($prefillableChoiceInputNode);
            }

            $prefillingQuestionIds = $prefillableChoiceInputNode->getPrefillingQuestionUids();

            foreach ($prefillingQuestionIds as $prefillingQuestionId) {
                if (!$questions->hasOneByNodeUid($prefillingQuestionId)) {
                    $messages[] = ChoicePrefillingQuestionDoesNotExistMessage::createForPrefillableQuestion($prefillableChoiceInputNode, $prefillingQuestionId);
                }
            }

            $prefillingQuestions = $questions->getByNodeUids($prefillingQuestionIds);

            foreach ($prefillingQuestions as $prefillingQuestion) {
                /** @var ChoiceInputNode $prefillingQuestion */
                if ($prefillingQuestion->getInput()->isMultiple()) {
                    $messages[] = PrefillingChoiceIsMultipleMessage::createMessageForPrefillingChoice($prefillingQuestion);
                }
            }
        }

        return new InvalidPublishMessageCollection($messages);
    }

    /** @param InputNodeCollection<array-key,InputNode> $questions */
    private function getPrefillableQuestionsErrorMessages(
        InputNodeCollection $questions,
        WorkFlow $workFlow,
    ): InvalidPublishMessageCollection {
        $messages = [];
        $steps = $workFlow->getSteps()->toArray();

        foreach ($questions->getPrefillableInputNodesHavingPrefillingQuestions() as $prefillableQuestion) {
            foreach ($steps as $step) {
                $messages = array_merge(
                    $messages,
                    $this->getPrefillableQuestionStepPermissionErrorMessage(
                        $prefillableQuestion,
                        $questions,
                        $step
                    )->toArray()
                );
            }
        }

        return new InvalidPublishMessageCollection($messages);
    }

    /**
     * @param InputNodeCollection<array-key,InputNode> $allQuestions
     */
    private function getPrefillableQuestionStepPermissionErrorMessage(
        PrefillableInputNode $prefillableQuestion,
        InputNodeCollection $allQuestions,
        StepInterface $step,
    ): InvalidPublishMessageCollection {
        $flowPermission = $prefillableQuestion->getFlowPermission();
        $stepWriteAccessRoles = $flowPermission->getRolesWithWriteAccessInStepUid($step->getUid());

        // in no roles can fill in (thus also prefill) this question in given step,
        // then a prefill can never even happen in that step -> skip
        if (0 === count($stepWriteAccessRoles)) {
            return new InvalidPublishMessageCollection([]);
        }

        $prefillingQuestions = $allQuestions->getByNodeUids($prefillableQuestion->getPrefillingQuestionUids());
        $messages = [];
        // for each role that can fill in (i.e. thus also prefill) this prefillableQuestion in given step, all the
        // prefilling questions for that prefillableQuestion must grant at least read access in that step for that role.
        // this is a domain decision: you can only benefit from getting a question prefilled in a certain step,
        // if you can see all of its prefilling questions in that step (i.e. you have at least read access on all of them)
        // this is an easy way to avoid users getting a question prefilled with answers from prefilling questions
        // to which they don't have access to (in the step in which the prefilling happens *).
        // * in theory, we don't have to limit checking access for the prefilling questions to the step in which the
        // prefillableQuestion gets prefilled: we could loosen the access check and for each of the prefilling questions
        // look for at least read access in the step in which the prefillableQuestion gets prefilled and if that step
        // does not grant such access, also check for at least read access in at least one of the steps preceding that
        // step. that seems 'fair' also, because the user will then have had access at some point before the prefill,
        // but potentially not anymore in the prefill step itself. but for now, the decision is to look for at least
        // read access in only the step in which the prefillableQuestion gets prefilled.
        foreach ($stepWriteAccessRoles as $roleName) {
            $role = Role::fromName($roleName);
            $invalidPrefillingQuestion = $prefillingQuestions->getUnReadableInStepForRole($step, $role);
            if (0 < $invalidPrefillingQuestion->count()) {
                $messages[] = RoleCanNotAccessSourceQuestionsForPrefillableQuestionInStepMessage::createMessageForPrefillableQuestion(
                    $prefillableQuestion,
                    $invalidPrefillingQuestion,
                    $step,
                    $role
                );
            }
        }

        return new InvalidPublishMessageCollection($messages);
    }

    /**
     * check validity of the nodes that are prepared as global score nodes (i.e. number or choice questions that
     * meet requirements for scoring (minScore, etc.) and rely on other scorable questions to calculate their
     * global score (numeric or choice-option) answer. those other scorable questions must also be valid themselves,
     * on having also scoring requirements, not being locked by other questions, etc. On end-user side, when a
     * global score node does not meet its scoring requirements it is silently skipped from calculating. if one of
     * its depending questions is not valid for scoring or does not exists (anymore) in the tree or even de-activated,
     * that question is skipped and calculation continues with remaining valid questions (to avoid that end-user
     * get exceptions due to 'badly' build forms). this method, called on publish of a template, assures that each
     * scorable question that is configured as a globals score question (simply by having at least one score dependency
     * configured) will be fully valid so that its calculation will done in full, consistently and reliable.
     *
     * @param InputNodeCollection<array-key, InputNode> $questions
     *
     * @throws MissingNodeException
     */
    private function getGlobalScoringErrorMessages(InputNodeCollection $questions): InvalidPublishMessageCollection
    {
        $messages = [];

        /** @var ScorableInputNode $globalScoreNode */
        foreach ($questions->getScorableInputNodes() as $globalScoreNode) {
            $dependencyConfigs = $globalScoreNode->getGlobalScoreCalculationDependencyConfigs();
            // skip the scorable node that is not configured to be global score node = has no dependencies
            if ($dependencyConfigs->isEmpty()) {
                continue;
            }

            $weighingType = $globalScoreNode->getGlobalScoreWeighingType();

            // START check for errors that are not part of the main globalScoreNode->operatesAsGlobalScoreNode()
            // i.e. we check for things that would not make sense or create unworkable/incalculable situations but can not be added
            // to the above-mentioned method for some reason or that might be rewritten to allow it anyway if a use case is found.
            $messages = match ($weighingType) {
                OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE => array_merge(
                    $messages,
                    $this->getGlobalScoreWithPercentageWeighingErrorMessages($globalScoreNode)->toArray()
                ),
                OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE => array_merge(
                    $messages,
                    $this->getGlobalScoreWithAbsoluteWeighingErrorMessages($globalScoreNode)->toArray()
                ),
                default => throw new \InvalidArgumentException('unknown weighing type!'),
            };

            // at time of writing, having 0.0 (zero) as maxScore on a global score question will result in a mathematical
            // division by zero at the end of the actual calculation in end-user side. since none of the implementing apps
            // use zero as maxScore on global score questions, we block this. if ever the need rises to allow this, the
            // calculation algorithm/logic will require rewriting. a start for this rewrite is mentioned in {@see https://jira.gbiomed.kuleuven.be/browse/FORM-310}
            if ($globalScoreNode->getInputScoringParameters()->hasMaxScore() && 0.0 === $globalScoreNode->getInputScoringParameters()->getMaxScore()) {
                $messages[] = InvalidZeroMaxScoreForGlobalScoreInFormListMessage::createMessageForGlobalScoreNode($globalScoreNode);
            }

            // if the global score questions grants write access to at least one role in at least one step, but the option
            // to show the (re)calculation button is disabled AND the option to allow manual input (allowNonAutoCalculatedValue)
            // is also disabled, then the users with write access might end up with a disabled input field for that question
            // that might be blank (if no auto-calculation was able to auto-answer the question before) and thus the user
            // has no way to fill in the field.
            // if the question is required, it will block the possibility to submit the form for that user. if it is optional,
            // it might still be submittable, but then it takes away the option for the user to provide an optional answer.
            // i.e.: it is pointless to configure a global score question as such.
            if ($globalScoreNode->getFlowPermission()->isWriteAccessGrantedForAtLeastOneRoleInAtLeastOneStep()
                && !$globalScoreNode->getInputScoringParameters()->allowNonAutoCalculatedValue()
                && !$globalScoreNode->getInputScoringParameters()->showAutoCalculateBtn()
            ) {
                $messages[] = NotAnswerableWritableGlobalScoreInFormListMessage::createMessageForGlobalScoreNode($globalScoreNode);
            }
            // END check for errors that are not part of the main globalScoreNode->operatesAsGlobalScoreNode()

            // skip if it validates in full , including full validity of all dependency questions.
            // if this fails, the checks below will check each possible individual invalidity on both the global score itself and
            // on all its dependencies. all those checks are based on what caused this operatesAsGlobalScoreNode to return false
            // the global score question must:
            // - be reachable (not marked as hidden or part of a hidden category)
            // - have a minScore, maxScore, scale and roundingMode set
            // - have a valid minScore - maxScore range (maxScore must be greater than minScore))
            // - if it has a custom passScore, it must have that passScore valid in the minScore - maxScore range
            // - if the global score is a choice question (i.e. operates as a valid choice-as-global-score question):
            //      - the choice question does not allow multiple answers
            //      - each answer option of the choice question is available (reachable, not marked as hidden)
            //      - each answer option does not allow for additional text input (i.e. is not an 'other' option)
            //      - each answer option has a scoring value that falls within the min & max score range of the choice-as-score question
            if ($globalScoreNode->operatesAsGlobalScoreNode()) {
                continue;
            }

            // a global score must be visible and not hidden/deactivated. domain decision: an unreachable is never used
            // nor triggers calculations, but when admins start to hide & un-hide global score questions several times,
            // while end-users are already filling in forms, it could lead to inconsistent or no longer calculable globals
            if (!$globalScoreNode->isReachableInTree()) {
                $messages[] = UnReachableGlobalScoreInFormListMessage::createMessageForGlobalScoreNode($globalScoreNode);
            }

            // the basic scoring parameters (min & max Sore, scale, rounding mode, passcore) on the global score question must be fully provided and valid
            if (!$globalScoreNode->getInputScoringParameters()->makesScoringPossible()) {
                $messages = array_merge(
                    $messages,
                    $this->getGlobalScoreScoringParametersErrorMessages($globalScoreNode)->toArray()
                );
            }

            if (!$globalScoreNode->hasValidSumOfDependencyWeighingsForGlobalScoring()) {
                $messages[] = match ($weighingType) {
                    OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE => InvalidSumOfDependencyWeighingsForGlobalScoreWithWeighingTypePercentageInFormListMessage::createMessageForChoiceGlobalScoreNode(
                        $globalScoreNode
                    ),
                    OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE => InvalidSumOfDependencyWeighingsForGlobalScoreWithWeighingTypeAbsoluteInFormListMessage::createMessageForChoiceGlobalScoreNode(
                        $globalScoreNode
                    ),
                    default => throw new \InvalidArgumentException('unknown weighing type!'),
                };
            }

            if ($globalScoreNode instanceof ChoiceInputNode && $globalScoreNode->getInput()->isMultiple()) {
                $messages[] = InvalidMultiSelectForChoiceGlobalScoreInFormListMessage::createMessageForChoiceGlobalScoreNode($globalScoreNode);
            }

            // check if all the answer options (optionInputNodes) of the choice global score question are valid for scoring:
            // - none of the answer options of the global score question may allow for additional text input
            // - the global score question must have a minScore and a maxScore set
            // - all the answer options of the global score question must have a scoring value set
            // - the scoring value of each answer options of the global score questionn must be within the minScore & maxScore range
            if ($globalScoreNode instanceof ChoiceInputNode && !$globalScoreNode->hasAllOptionsValidForScoring()) {
                // - none of the answer options of the global score question may allow for additional text input
                if ($globalScoreNode->usesOptionsWithAdditionalTextInput()) {
                    $messages[] = InvalidOptionsWithTextInputForChoiceGlobalScoreInFormListMessage::createMessageForChoiceGlobalScoreNode($globalScoreNode);
                }

                // - all the answer options of the global score question must have a scoring value set
                if (!$globalScoreNode->getAvailableOptions()->getHavingNoScoringValue()->isEmpty()) {
                    $messages[] = MissingOptionScoreValuesForChoiceGlobalScoreInFormListMessage::createMessageForChoiceGlobalScoreNode($globalScoreNode);
                }

                // - the scoring value of each answer options of the global score question must be within the minScore & maxScore range
                // the presence of maxScore & minScore is already checked above in $globalScoreNode->getInputScoringParameters()->makesScoringPossible()
                if ($globalScoreNode->getInputScoringParameters()->hasScoringExtrema() && !$this->choiceQuestionHasValidScoringValuesForAvailableScoringOptionsWithinScoringExtrema($globalScoreNode)) {
                    $messages[] = InvalidOptionScoreValuesForChoiceGlobalScoreInFormListMessage::createMessageForChoiceGlobalScoreNode($globalScoreNode);
                }
            }

            // get all the fully valid dependencies from the global score (either a number or a choice question):
            // - each dependency is reachable (not marked as hidden or part of a hidden category)
            // - each dependency has a minScore, maxScore, scale and roundingMode set
            // - each dependency has a valid minScore - maxScore range (maxScore must be greater than minScore))
            // - each dependency has a maxScore different of 0 (zero), unless its weighing is also 0 (zero)
            // - each dependency that has a custom passScore, must have that passScore valid in the minScore - maxScore range
            // - if dependency is a choice question (i.e. operates as a valid choice-as-score question):
            //      - the choice question does not allow multiple answers
            //      - each answer option of the choice question is available (reachable, not marked as hidden)
            //      - each answer option does not allow for additional text input (i.e. is not an 'other' option)
            //      - each answer option has a scoring value that falls within the min & max score range of the choice-as-score question
            $validDependencies = $globalScoreNode->getValidGlobalScoreCalculationScoreNodeConfigs();

            foreach ($globalScoreNode->getGlobalScoreCalculationDependencyConfigs() as $dependencyConfig) {
                // each configuration for a dependency must refer to an existing, scorable node and must be valid
                // catch configured references to removed or non-existing questions
                if (!$questions->hasOneByNodeUid($dependencyConfig->getScoreNodeUid())) {
                    $messages[] = MissingGlobalScoreDependencyInFormListMessage::createMessageForGlobalScoreNodeAndDependencyScoreNodeUid(
                        $globalScoreNode,
                        $dependencyConfig->getScoreNodeUid()
                    );
                    continue;
                }

                $dependencyScoreNode = $questions->getOneByNodeUid($dependencyConfig->getScoreNodeUid());

                // dependencies are stored on global node by reference, so just in case, check if it refers to a scorable
                if (!($dependencyScoreNode instanceof ScorableInputNode)) {
                    $messages[] = NotOperableGlobalScoreDependencyInFormListMessage::createMessageForGlobalScoreNodeAndNonOperableNode(
                        $globalScoreNode,
                        $dependencyScoreNode
                    );
                    continue;
                }

                // skip when fully valid for as score dependency for global scoring
                if ($validDependencies->hasForScoreNode($dependencyScoreNode)) {
                    continue;
                }

                $messages = array_merge(
                    $messages,
                    $this->getScoreDependencyErrorMessagesForGlobalScore(
                        $dependencyScoreNode,
                        $globalScoreNode
                    )
                );
            }
        }

        return new InvalidPublishMessageCollection($messages);
    }

    private function getGlobalScoreScoringParametersErrorMessages(
        ScorableInputNode $globalScoreNode,
    ): InvalidPublishMessageCollection {
        // skip if the global score node has no dependencies (i.e. can & does not operate as global score node)
        if ($globalScoreNode->getAllGlobalScoreCalculationScoreNodeConfigs()->getScoreNodes()->isEmpty()) {
            return new InvalidPublishMessageCollection();
        }

        $scoringParameters = $globalScoreNode->getInputScoringParameters();

        // skip if scoring parameters on the global score question are fully provided and valid
        if ($scoringParameters->makesScoringPossible()) {
            return new InvalidPublishMessageCollection();
        }

        $messages = [];

        // at least one of the required scoring params maxScore, minScore, scale and rounding mode are missing
        if (0 < count($scoringParameters->getKeysOfParametersMissingForValidScoring())) {
            $messages[] = MissingScoringParametersForGlobalScoreInFormListMessage::createMessageForGlobalScoreNode($globalScoreNode);
        }

        // if maxScore and minScore are not missing, maxScore must be greater than minScore.
        if ($scoringParameters->hasScoringExtrema() && !$scoringParameters->hasValidScoringExtrema()) {
            $messages[] = InvalidScoringExtremaForGlobalScoreInFormListMessage::createMessageForGlobalScoreNode($globalScoreNode);
        }

        // custom pass score is optional. if set, it must be within the range of the (valid) scoring extrema.
        // hence we can't check it if the extrema are missing or not valid
        if (!$scoringParameters->hasCustomPassScore() || !$scoringParameters->hasValidScoringExtrema()) {
            return new InvalidPublishMessageCollection($messages);
        }

        // custom pass score must be within range of minScore and maxScore (equal to minScore or maxScore is allowed,
        // although it might be weird to choose to set it equal to one of those. still, there might be uses cases)
        if ($scoringParameters->getCustomPassScore() < $scoringParameters->getMinScore() || $scoringParameters->getCustomPassScore() > $scoringParameters->getMaxScore()) {
            $messages[] = InvalidCustomPassScoreForGlobalScoreInFormListMessage::createMessageForGlobalScoreNode($globalScoreNode);
        }

        return new InvalidPublishMessageCollection($messages);
    }

    private function getScoreDependencyScoringParametersErrorMessagesForGlobalScore(
        ScorableInputNode $dependencyScoreNode,
        ScorableInputNode $globalScoreNode,
    ): InvalidPublishMessageCollection {
        $scoringParameters = $dependencyScoreNode->getInputScoringParameters();

        // skip if scoring parameters on the score question are fully provided and valid
        if ($scoringParameters->makesScoringPossible()) {
            return new InvalidPublishMessageCollection();
        }

        $messages = [];

        // at least one of the required scoring params maxScore, minScore, scale and rounding mode are missing
        if (0 < count($scoringParameters->getKeysOfParametersMissingForValidScoring())) {
            $messages[] = MissingScoringParametersForGlobalScoreDependencyInFormListMessage::createMessageForGlobalScoreNodeAndDependencyScoreNode(
                $globalScoreNode,
                $dependencyScoreNode
            );
        }

        // if maxScore and minScore are not missing, maxScore must be greater than minScore.
        if ($scoringParameters->hasScoringExtrema() && !$scoringParameters->hasValidScoringExtrema()) {
            $messages[] = InvalidScoringExtremaForGlobalScoreDependencyInFormListMessage::createMessageForGlobalScoreNodeAndDependencyScoreNode(
                $globalScoreNode,
                $dependencyScoreNode
            );
        }

        // custom pass score is optional. if set, it must be within the range of the (valid) scoring extrema.
        // hence we can't check it if the extrema are missing or not valid
        if (!$scoringParameters->hasCustomPassScore() || !$scoringParameters->hasValidScoringExtrema()) {
            return new InvalidPublishMessageCollection($messages);
        }

        // custom pass score must be within range of minScore and maxScore (equal to minScore or maxScore is allowed,
        // although it might be weird to choose to set it equal to one of those. still, there might be uses cases)
        if ($scoringParameters->getCustomPassScore() < $scoringParameters->getMinScore() || $scoringParameters->getCustomPassScore() > $scoringParameters->getMaxScore()) {
            $messages[] = InvalidCustomPassScoreForGlobalScoreDependencyInFormListMessage::createMessageForGlobalScoreNodeAndDependencyScoreNode(
                $globalScoreNode,
                $dependencyScoreNode
            );
        }

        return new InvalidPublishMessageCollection($messages);
    }

    /**
     * to avoid issues with the total sum of the weights not reaching the required 100% when calculating,
     * we do not allow the use of
     * - optional dependencies: if an optional question as dependency is submitted blank, it is omitted from the
     *   calculation, leaving the total sum of the weights from the participating dependency questions less than 100%
     * - locked dependencies: if a locked (and required) question as dependency is not unlocked, and the global score question
     *   is configured to NOT ignore the not unlocked (required) questions, then that question will block the calculation
     *   to be triggered.
     *   and if the global score question is configured to ignore the not unlocked (required) questions, then that question
     *   is omitted from the calculation (similar to a blank submitted optional question), leaving the total sum of
     *   the weights from the participating dependency questions less than 100%.
     *   hence we do not allow the use of locked questions.
     *   note: we could allow locked required questions as dependencies and then check here if all possible
     *   lock/unlock combinations of all the locked questions will always lead to a sum of 100% weights for the
     *   unlocked questions that remain for each combo. but that is very difficult to determine.
     * if the admin building a form wants to use locked and/or optional questions as dependencies, he/she
     * can then use weighingType 'absolute' instead of 'percentage', which does allow the use of optional and/or
     * locked dependencies. the calculation for both weighingTypes basically work the same way for the weighing,
     * but with 'percentage', the sum of the weights must always be exactly 100%.
     */
    private function getGlobalScoreWithPercentageWeighingErrorMessages(
        ScorableInputNode $globalScoreNode,
    ): InvalidPublishMessageCollection {
        // skip if the global score node has no dependencies (i.e. can & does not operate as global score node)
        $dependencyQuestions = $globalScoreNode->getAllGlobalScoreCalculationScoreNodeConfigs()->getScoreNodes();
        if ($dependencyQuestions->isEmpty()) {
            return new InvalidPublishMessageCollection();
        }

        // skip if the global score node has the wrong weighingType
        if (OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE !== $globalScoreNode->getGlobalScoreWeighingType()) {
            return new InvalidPublishMessageCollection();
        }

        $messages = [];

        // we don't allow use of optional dependencies for calculating global score questions
        if (0 < $dependencyQuestions->getOptional()->count()) {
            $messages[] = InvalidOptionalDependenciesForGlobalScoreWithWeighingTypePercentageInFormListMessage::createMessageForGlobalScoreNode($globalScoreNode);
        }

        // we don't allow use of locked dependencies for calculating global score questions
        if (0 < $dependencyQuestions->getLocked()->count()) {
            $messages[] = InvalidLockedDependenciesForGlobalScoreWithWeighingTypePercentageInFormListMessage::createMessageForGlobalScoreNode($globalScoreNode);
        }

        return new InvalidPublishMessageCollection($messages);
    }

    private function getGlobalScoreWithAbsoluteWeighingErrorMessages(ScorableInputNode $globalScoreNode): InvalidPublishMessageCollection
    {
        // skip if the global score node has no dependencies (i.e. can & does not operate as global score node)
        $dependencyQuestions = $globalScoreNode->getAllGlobalScoreCalculationScoreNodeConfigs()->getScoreNodes();
        if ($dependencyQuestions->isEmpty()) {
            return new InvalidPublishMessageCollection();
        }

        // skip if the global score node has the wrong weighingType
        if (OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE !== $globalScoreNode->getGlobalScoreWeighingType()) {
            return new InvalidPublishMessageCollection();
        }

        $messages = [];

        // if all dependencies are optional, then the global score node itself must be optional itself, to avoid
        // a global score not being submittable with a blank value if all of its dependencies are deliberately filled
        // in as blank .
        // skip if the global score does not grant any write access (can not be filled-in by any role)
        // and thus will never enforce a user to fill it in manually:
        // if the global score question can not be filled in manually by any user, it can only be calculated & 'filled in'
        // automatically during a submit. if there is not enough dependency data present to do that automatic calculation,
        // then it will skip the calculation and not supply and answer for it. thus only if the global score question
        // can be submitted manually, does it need to be optional if all its dependencies are optional.
        if ($globalScoreNode->getFlowPermission()->isWriteAccessGrantedForAtLeastOneRoleInAtLeastOneStep()
            && $globalScoreNode->isRequired()
            && $dependencyQuestions->count() === $dependencyQuestions->getOptional()->count()) {
            $messages[] = InvalidOptionalDependenciesForWritableGlobalScoreWithWeighingTypeAbsoluteInFormListMessage::createMessageForGlobalScoreNode($globalScoreNode);
        }

        return new InvalidPublishMessageCollection($messages);
    }

    private function getScoreDependencyErrorMessagesForGlobalScore(
        ScorableInputNode $dependencyScoreNode,
        ScorableInputNode $globalScoreNode,
    ): array {
        $messages = [];

        if (!$dependencyScoreNode->isReachableInTree()) {
            $messages[] = UnReachableGlobalScorerDependencyInFormListMessage::createMessageForGlobalScoreNodeAndDependencyScoreNode(
                $globalScoreNode,
                $dependencyScoreNode
            );
        }

        $scoringParameters = $dependencyScoreNode->getInputScoringParameters();

        // scoring parameters on the score dependency question must be fully provided and valid
        if (!$scoringParameters->makesScoringPossible()) {
            $messages = array_merge(
                $messages,
                $this->getScoreDependencyScoringParametersErrorMessagesForGlobalScore($globalScoreNode, $dependencyScoreNode)->toArray()
            );
        }

        // max score (if set) may not be zero if weighing is not zero to avoid dividing by zero in calculation formula
        // (i.e. maxScore can only be zero if weighing is also zero)
        if ($scoringParameters->hasMaxScore()) {
            /** {@see CalculationScoreNodeConfigCollection::getValidForCalculation()} (return $maxScore === $configNode->getConfig()->getWeighing()) || !(0.0 === $maxScore); */
            $maxScore = $scoringParameters->getMaxScore();
            $weighing = $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->getOneByNodeUid($dependencyScoreNode->getUid())->getWeighing();
            if (0.0 === $maxScore && !(0.0 === $weighing)) {
                $messages[] = InvalidWeighedZeroMaxScoreForGlobalScoreDependencyInFormList::createMessageForGlobalScoreNodeAndDependencyScoreNode(
                    $globalScoreNode,
                    $dependencyScoreNode
                );
            }
        }

        if (!$dependencyScoreNode instanceof ChoiceInputNode) {
            return $messages;
        }

        // the choice score dependency question may not allow for multiple answers
        if ($dependencyScoreNode->getInput()->isMultiple()) {
            $messages[] = InvalidMultiSelectForGlobalScoreChoiceDependencyInFormListMessage::createMessageForGlobalScoreNodeAndDependencyChoiceScoreNode(
                $globalScoreNode,
                $dependencyScoreNode
            );
        }

        // check if all the answer options (optionInputNodes) of the choice score dependency question are valid for scoring:
        // - none of the answer options of the choice score dependency may allow for additional text input
        // - the choice score dependency question must have a minScore and a maxScore set
        // - all the answer options of the choice score dependency question must have a scoring value set
        // - the scoring value of each answer options of the choice score dependency question must be within the minScore & maxScore range
        // 'choice score dependency question must have a minScore and a maxScore set': already checked in {@see self::getScoreDependencyScoringParametersErrorMessagesForGlobalScore()}
        if ($dependencyScoreNode->hasAllOptionsValidForScoring()) {
            return $messages;
        }

        // - none of the answer options of the choice score dependency may allow for additional text input
        if ($dependencyScoreNode->usesOptionsWithAdditionalTextInput()) {
            $messages[] = InvalidOptionsWithTextInputForGlobalScoreChoiceDependencyInFormListMessage::createMessageForGlobalScoreNodeAndDependencyChoiceScoreNode(
                $globalScoreNode,
                $dependencyScoreNode
            );
        }

        // - all the answer options of the choice score dependency question must have a scoring value set
        if (!$dependencyScoreNode->getAvailableOptions()->getHavingNoScoringValue()->isEmpty()) {
            $messages[] = MissingOptionScoreValuesForGlobalScoreChoiceDependencyInFormListMessage::createMessageForGlobalScoreNodeAndDependencyChoiceScoreNode(
                $globalScoreNode,
                $dependencyScoreNode
            );
        }

        // - the scoring value of answer options of the choice score dependency question are within the minScore & maxScore range
        // the presence of maxScore & minScore is checked above in $dependencyScoreNode->getInputScoringParameters()->makesScoringPossible()
        if ($scoringParameters->hasScoringExtrema() && !$this->choiceQuestionHasValidScoringValuesForAvailableScoringOptionsWithinScoringExtrema($dependencyScoreNode)) {
            $messages[] = InvalidOptionScoreValuesForGlobalScoreChoiceDependencyInFormListMessage::createMessageForGlobalScoreNodeAndDependencyChoiceScoreNode(
                $globalScoreNode,
                $dependencyScoreNode
            );
        }

        return $messages;
    }

    /**
     * For every CategoryNode with displayMode == 'matrix' check if
     * - the node only has (sub)category nodes as children
     * - every subcategory has a label
     * - the subcategories only have input nodes as children
     * - the number of questions in every subcategory is the same
     * - every label for every question in every subcategory is the same (for every language)
     * - every question is of the same type in the same order in every subcategory.
     *
     * admin side note: for UX friendliness, when an admin wants a choice-answer-option to unlocks a question, we will
     * not include individual questions from matrices to the selectable list, but instead add the matrix to which it
     * belongs and let the backend then lock each question in that matrix to that choice-answer-option
     *
     * ------------------------------------------------------------------------------------------------------------------
     *
     * developer note: The following checks/restrictions were in place before but are now disabled and no longer enforced.
     * mixed unlocking & unlocking inside matrix are now allowed (however in some cases this may lead to front-end issues).
     * the code for this disabled restrictions is commented out until we are sure it all works as needed.
     *
     * - (DISABLED) no question in the matrix depends on (is locked by) another question (e.g. a choice-answer-option) in the same matrix.
     * - (DISABLED) when one question in the matrix depends on one or more questions outside the matrix, then all questions in the
     *   matrix must also be locked.
     * - (DISABLED) if all questions in the matrix are locked, then they must all be locked and by the same unlocking question(s).
     */
    private function getCategoryMatrixErrorMessages(CategoryNodeCollection $categories): InvalidPublishMessageCollection
    {
        $messages = [];

        $matrixes = new CategoryNodeCollection();
        foreach ($categories as $category) {
            if (CategoryNode::DISPLAY_MODE_MATRIX === $category->getDisplayMode()) {
                $matrixes->add($category);
            }
        }

        /** @var CategoryNode $matrix */
        foreach ($matrixes as $matrix) {
            // matrix may only have category nodes as children
            $matrixChildren = $matrix->getChildren();
            if (0 < $matrixChildren->getInputNodes()->count()) {
                $messages[] = InvalidChildInputNodesInCategoryNodeMatrixMessage::createMessageForCategoryNodeMatrix($matrix);
            }

            if (0 < $matrixChildren->getReadOnlyViewNodes()->getRadarChartNodes()->count()) {
                $messages[] = InvalidChildRadarChartsInCategoryNodeMatrixMessage::createMessageForCategoryNodeMatrix($matrix);
            }

            // matrix has to have at least one active category node child
            $subcategories = $matrix->getActiveChildren()->getCategoryNodes();
            if ($subcategories->isEmpty()) {
                $messages[] = NoActiveSubcategoriesInCategoryNodeMatrixMessage::createMessageForCategoryNodeMatrix($matrix);
                continue;
            }

            $subcategoryQuestionCounts = [];
            $subcategoryQuestionsLocalizedLabels = [];
            $subcategoryQuestionsTypes = [];
            $totalQuestionsCount = 0;
            $totalLockedQuestionsCount = 0;
            $unlockerChains = [];
            /** @var CategoryNode $subcategory */
            foreach ($subcategories as $subcategory) {
                // the subcategories can only have input nodes as children
                $subCategoryChildren = $subcategory->getChildren();
                $questions = $subCategoryChildren->getInputNodes();

                if (0 < $subCategoryChildren->getCategoryNodes()->count()) {
                    $messages[] = InvalidChildCategoriesForSubcategoryInCategoryNodeMatrixMessage::createMessageForCategoryNodeMatrixAndSubcategory(
                        $matrix,
                        $subcategory
                    );
                }

                if (0 < $subCategoryChildren->getReadOnlyViewNodes()->getRadarChartNodes()->count()) {
                    $messages[] = InvalidChildRadarChartsForSubcategoryInCategoryNodeMatrixMessage::createMessageForCategoryNodeMatrixAndSubcategory(
                        $matrix,
                        $subcategory
                    );
                }

                // All subcategories have a label
                if (!$subcategory->getInfo()->getLocalizedLabel()->hasFallback()) {
                    $messages[] = MissingLabelForSubcategoryInCategoryNodeMatrixMessage::createMessageForCategoryNodeMatrixAndSubcategory(
                        $matrix,
                        $subcategory
                    );
                }

                $questionLocalizedLabels = [];
                $questionTypes = [];
                /** @var InputNode $question */
                foreach ($questions as $question) {
                    ++$totalQuestionsCount;
                    $questionLocalizedLabels[] = $question->getInfo()->getLocalizedLabel();
                    $questionTypes[] = $question::getType();

                    if (!($question instanceof ParentalInputNode)) {
                        continue;
                    }

                    if ($question->isLocked()) {
                        ++$totalLockedQuestionsCount;
                        $unlockerChains[] = implode('|', $question->getUnlockers()->getUids());
                    }
                }
                $subcategoryQuestionCounts[] = $questions->count();
                $subcategoryQuestionsLocalizedLabels[] = $questionLocalizedLabels;
                $subcategoryQuestionsTypes[] = $questionTypes;
            }

            if (count(array_unique($subcategoryQuestionCounts)) > 1) {
                $messages[] = InvalidNumberOfQuestionsInSubcategoriesInCategoryNodeMatrix::createMessageForCategoryNodeMatrixAndQuestionCount(
                    $matrix,
                    $subcategoryQuestionCounts
                );
                continue;
            }

            $uniqueQuestionsLocalizedLabels = array_unique($subcategoryQuestionsLocalizedLabels, \SORT_REGULAR);
            if (count($uniqueQuestionsLocalizedLabels) > 1) {
                $messages[] = LabelsDontMatchOnInputNodesInCategoryNodeMatrixMessage::createMessageForCategoryNodeMatrixAndQuestionLabelsAndCount(
                    $matrix,
                    $uniqueQuestionsLocalizedLabels,
                    $subcategoryQuestionCounts[0]
                );
            }

            // every question should be of the same type in the same order in every subcategory
            $uniqueSubcategoryQuestionsTypes = array_unique($subcategoryQuestionsTypes, \SORT_REGULAR);
            if (count($uniqueSubcategoryQuestionsTypes) > 1) {
                $messages[] = TypesDontMatchOnInputNodesInCategoryNodeMatrixMessage::createMessageForCategoryNodeMatrixAndQuestionTypes(
                    $matrix,
                    $uniqueSubcategoryQuestionsTypes
                );
            }

            /*
             * START LOCKING RESTRICTIONS
             * the following code has been commented out to allow:
             * - locking of questions inside a matrix by unlockers inside that same matrix
             * - unlocking questions in a matrix by different (chains of) unlockers.
             * These restrictions were originally set because in some cases, it results in less UX friendly (but in no
             * way unworkable) matrix rendering. e.g. displaying rows with only empty cells (until unlocked), etc.
             * this code has not yet been removed in case we need to review it or partially reinstate parts of it.
             * note that in (an) implementing app(s) (EQA) matrices are already in use that violate these restrictions.
             */

            //            if (0 === $totalLockedQuestionsCount) {
            //                continue;
            //            }

            // either all questions in matrix are locked or none
            //            if (false === ($totalQuestionsCount === $totalLockedQuestionsCount)) {
            //                $messages[] = InvalidNumberOfLockedQuestionsInCategoryNodeMatrixMessage::createMessageForCategoryNodeMatrix($matrix);
            //            }

            // all locked questions must be locked by the exact same (chain of) unlocker(s)
            //            $uniqueUnlockerChains = array_unique($unlockerChains);
            //            if (1 < count($uniqueUnlockerChains)) {
            //                $messages[] = InvalidMixOfUnlockersForLockedQuestionsInCategoryNodeMatrixMessage::createMessageForCategoryNodeMatrix($matrix);
            //            }

            // questions in the matrix may never be locked by another question in the same matrix.
            // note: OptionInputNode::guardDoNotUnlockOwnQuestion already asserts that no question can unlock itself. so in
            // case all questions in a matrix are locked, and all by the same (chain) of unlocker(s), then it's impossible
            // that a question is locking another inside same matrix. however, we collect all locking-validity errors at once,
            // and since potential future changes on lockability in matrices may occur, it's better to check in all cases
            //            $allMatrixNodes = $matrix->getFlattenedWithAllDescendants();
            //            foreach ($uniqueUnlockerChains as $unlockerChain) {
            //                $unlockingOptionInputNodeIds = explode('|', $unlockerChain);
            //
            //                // if one unlocking option belongs to (a choice question in) the same matrix, then all locking in matrix is invalid
            //                if (false === $allMatrixNodes->getByNodeUids($unlockingOptionInputNodeIds)->isEmpty()) {
            //                    $messages[] = InvalidSelfUnlockingOfLockedQuestionsInCategoryNodeMatrixMessage::createMessageForCategoryNodeMatrix($matrix);
            //                    break;
            //                }
            //            }

            /* END LOCKING RESTRICTIONS */
        }

        return new InvalidPublishMessageCollection($messages);
    }

    /**
     * @param InputNodeCollection<array-key, ChoiceInputNode> $choiceInputNodes
     */
    private function getChoiceQuestionsWithOtherOptionErrorMessages(InputNodeCollection $choiceInputNodes): InvalidPublishMessageCollection
    {
        $messages = [];

        // domain decision: a choice question of any type (dropdown, radio group, checkbox group) and regardless of
        // allowing multiple or single answers, can only allow one (or none) 'other' answer options ({@see OptionWithAdditionalTextInput}),
        // allowing users to add a custom text answer when selecting such 'other' answer option.
        // this is mainly a domain UX decision, especially for dropdown questions. e.g. it is confusing to users if they are
        // shown more than one additional text field below a dropdown if that dropdown allows multiple answer to be chosen,
        // because those additional text fields are never labelled.  in the future , this restriction might be removed
        // for all or selected types of choice questions if needed, but then this will require front end code to make it
        // work. it will for some cases almost certainly require each additional text field to be prefixed with the label
        // of the related 'other' option so the user can differentiate between them (with those labels then being (enforced
        // to be?) unique for dropdown choice question).
        // note: UX wise, we could also enforce that this single 'other' option should always be last in list, but it
        // works also if it's not last and it still UX friendly enough, so not enforcing this
        /** @var ChoiceInputNode $choiceInputNode */
        foreach ($choiceInputNodes as $choiceInputNode) {
            $otherOptions = $choiceInputNode->getChildren()->getUsingAdditionalTextInput();

            if (1 < $otherOptions->count()) {
                $messages[] = InvalidAmountOfOptionsWithAdditionalTextInputForChoiceInFormListMessage::createMessageForDropdownChoiceNode($choiceInputNode);
            }
        }

        return new InvalidPublishMessageCollection($messages);
    }

    public function choiceQuestionHasValidScoringValuesForAvailableScoringOptionsWithinScoringExtrema(
        ChoiceInputNode $choiceInputNode,
    ): bool {
        $scoringParameters = $choiceInputNode->getInputScoringParameters();

        if (!$scoringParameters->hasScoringExtrema()) {
            throw new \Exception('cannot check scoring values of answer options against min-max score range: no minScore and/or maxScore set on the choice question');
        }

        foreach ($choiceInputNode->getAvailableOptions()->getHavingScoringValue() as $availableScoringOption) {
            if ($availableScoringOption->getScoringValue() < $scoringParameters->getMinScore()) {
                return false;
            }

            if ($availableScoringOption->getScoringValue() > $scoringParameters->getMaxScore()) {
                return false;
            }
        }

        return true;
    }

    /**
     * disallow permissions for a non-existing step on questions, choice option answer nodes and chart nodes.
     *
     * the end-user side will not fail on this, as it skips & ignores permissions on questions for non-existing
     * workflow steps, but if we do not catch this on publish, it could be confusing for admins making the form:
     * if e.g. a step is removed and replaced by another - especially with the same name - the admin could forget
     * to remove permissions for that deleted step and/or forget to add permissions for the new step. and if a question
     * only grants write access for that deleted step and no other step, then other checks in this publish validation
     * service will block publish because every question must grant write access (for at least one role) in at least
     * one existing step. but if the question also grants write access permission (for at least one role) for other
     * steps that do exist, then that question will be valid for publish.
     * hence it is better to make sure that the admin is aware of the latter. therefore we check and block if a permission
     * points to a step that does not exist.
     *
     * the answer option nodes of a choice question also have step & role based permissions. but those ara always exactly
     * the same as the permissions of their parental choice question. this is done for consistence between inputNodes,
     * to which both choice question as choice answer options belong. hence we don't check those explicitly: if the
     * invalid permissions of the parental choice question will be updated with valid ones, then those of its option
     * children are also updated with the same permissions.
     */
    private function getNodesWithPermissionsForNonExistingStepsErrorMessages(
        FormList $formList,
        WorkFlow $workFlow,
    ): InvalidPublishMessageCollection {
        $messages = [];

        $existingStepUids = $workFlow->getSteps()->getStepUids();

        /** @var QuestionWithPermissionsForNonExistingSteps[] $questionsWithPermissionsForNonExistingSteps */
        $questionsWithPermissionsForNonExistingSteps = [];
        foreach ($formList->getFlattenedParentalInputNodes() as $question) {
            /** @var StepPermissionCollection $stepPermissionsWithNonExistingSteps */
            $stepPermissionsWithNonExistingSteps = $question->getFlowPermission()->getStepPermissions()->filter(fn (StepPermission $stepPermission) => !in_array($stepPermission->getStepUid(), $existingStepUids, true));
            if (0 === $stepPermissionsWithNonExistingSteps->count()) {
                continue;
            }

            $questionsWithPermissionsForNonExistingSteps[] = new QuestionWithPermissionsForNonExistingSteps(
                $question,
                $stepPermissionsWithNonExistingSteps
            );
        }

        if (0 < count($questionsWithPermissionsForNonExistingSteps)) {
            $messages[] = PermissionsForNonExistingStepsForQuestionsInFormListMessage::createMessageForQuestionsWithPermissionsForNonExistingSteps($questionsWithPermissionsForNonExistingSteps);
        }

        /** @var RadarChartWithPermissionsForNonExistingSteps[] $chartNodesWithPermissionsForNonExistingSteps */
        $chartNodesWithPermissionsForNonExistingSteps = [];
        /** @var RadarChartViewNode $radarChartNode */
        foreach ($formList->getFlattenedReadOnlyViewNodes()->getRadarChartNodes() as $radarChartNode) {
            /** @var StepPermissionCollection $stepPermissionsWithNonExistingSteps */
            $stepPermissionsWithNonExistingSteps = $radarChartNode->getFlowPermission()->getStepPermissions()->filter(fn (StepPermission $stepPermission) => !in_array($stepPermission->getStepUid(), $existingStepUids, true));
            if (0 === $stepPermissionsWithNonExistingSteps->count()) {
                continue;
            }

            $chartNodesWithPermissionsForNonExistingSteps[] = new RadarChartWithPermissionsForNonExistingSteps(
                $radarChartNode,
                $stepPermissionsWithNonExistingSteps
            );
        }

        if (0 < count($chartNodesWithPermissionsForNonExistingSteps)) {
            $messages[] = PermissionsForNonExistingStepsForRadarChartsInFormListMessage::createMessageForRadarChartsWithPermissionsForNonExistingSteps($chartNodesWithPermissionsForNonExistingSteps);
        }

        return new InvalidPublishMessageCollection($messages);
    }
}
