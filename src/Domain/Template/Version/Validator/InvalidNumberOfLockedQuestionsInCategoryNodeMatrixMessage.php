<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;

class InvalidNumberOfLockedQuestionsInCategoryNodeMatrixMessage extends InvalidFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidNumberOfLockedQuestionsInCategoryNodeMatrix';

    public static function createMessageForCategoryNodeMatrix(
        CategoryNode $matrix,
    ): self {
        $matrixUid = $matrix->getUid();

        $allQuestions = $matrix->getFlattenedParentalInputNodeDescendants();
        $allQuestionsAmount = $allQuestions->count();
        if (0 === $allQuestionsAmount) {
            throw new \InvalidArgumentException('matrix cannot have invalid number of locked questions: it has no questions at all.');
        }

        $lockedQuestionsAmount = $allQuestions->getLocked()->count();
        if (0 === $lockedQuestionsAmount) {
            throw new \InvalidArgumentException('matrix cannot have invalid number of locked questions: it has no locked questions.');
        }

        $nonLockedQuestions = $allQuestions->getNonLocked();
        $nonLockedQuestionsAmount = $nonLockedQuestions->count();
        if (0 === $nonLockedQuestionsAmount) {
            throw new \InvalidArgumentException('matrix cannot have invalid number of locked questions: all the questions are locked.');
        }

        $nonLockedQuestionsUidsString = implode(' | ', $nonLockedQuestions->getUids());
        $nonLockedQuestionsAmountString = (string) $nonLockedQuestionsAmount;
        $lockedQuestionsAmountString = (string) $lockedQuestionsAmount;

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': either all or none off the questions in a matrix must be locked: '.$lockedQuestionsAmountString
            .' questions in matrix ['.$matrixUid.'] are locked, but '.$nonLockedQuestionsAmountString.' are not.'
            .' The non locked questions are: ['.$nonLockedQuestionsUidsString.'].';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%matrix%' => $matrixUid,
                '%lockedAmount%' => $lockedQuestionsAmountString,
                '%nonLockedAmount%' => $nonLockedQuestionsAmountString,
                '%nonLockedList%' => $nonLockedQuestionsUidsString,
            ]
        );
    }
}
