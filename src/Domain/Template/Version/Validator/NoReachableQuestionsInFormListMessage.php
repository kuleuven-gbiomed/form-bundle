<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

final class NoReachableQuestionsInFormListMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.noReachableQuestionsInFormList';

    public static function createMessage(): self
    {
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': one or more question are found;'
            .' but none of those questions are made available.'
            .' some or all of these questions are explicitly marked as hidden/inactive'
            .' and/or some or all of of these questions are part of an ancestral category that is marked as hidden/inactive';

        return new self($message, self::TRANSLATION_KEY, self::TRANSLATION_DOMAIN);
    }
}
