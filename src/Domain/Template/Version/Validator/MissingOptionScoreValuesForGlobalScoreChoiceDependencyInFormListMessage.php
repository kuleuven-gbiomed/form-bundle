<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class MissingOptionScoreValuesForGlobalScoreChoiceDependencyInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.missingOptionScoreValuesForGlobalScoreChoiceDependency';

    public static function createMessageForGlobalScoreNodeAndDependencyChoiceScoreNode(
        ScorableInputNode $globalScoreNode,
        ChoiceInputNode $dependencyChoiceScoreNode,
    ): self {
        $locale = $globalScoreNode->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();
        $scoreQuestionLabel = $dependencyChoiceScoreNode->getNestedLabelForFallBackLocale();

        Assert::true(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->hasOneByNodeUid($dependencyChoiceScoreNode->getUid()),
            ' can not build message '.self::class
            .'; the score node ['.$scoreQuestionLabel.']['.$dependencyChoiceScoreNode->getUid()
            .'] is not referenced as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        Assert::false(
            $dependencyChoiceScoreNode->getChildren()->getHavingNoScoringValue()->isEmpty(),
            ' can not build message '.self::class
            .'; the choice score node ['.$scoreQuestionLabel.']['.$dependencyChoiceScoreNode->getUid()
            .'] as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] does not have any child option nodes with missing score value!'
        );

        $invalidOptionLabels = implode(
            ' | ',
            $dependencyChoiceScoreNode->getChildren()->getHavingNoScoringValue()->getLabelsArray($locale)
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the choice question ['.$scoreQuestionLabel.'] is configured as a calculation dependency for the global score question ['.$globalScoreQuestionLabel.']. but it has answer options that have no score value defined. All answer options must have a numeric score value if the choice question is to be used as a dependency for global scoring calculations. Please remove those answer Options or provide a score value for those options. those invalid answer options are: ['.$invalidOptionLabels.'].';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $scoreQuestionLabel,
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%invalidOptionLabels%' => $invalidOptionLabels,
            ]
        );
    }
}
