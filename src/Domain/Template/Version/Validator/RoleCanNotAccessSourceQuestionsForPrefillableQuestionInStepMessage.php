<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\PrefillableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;
use KUL\FormBundle\Domain\Utility\Role;
use Webmozart\Assert\Assert;

final class RoleCanNotAccessSourceQuestionsForPrefillableQuestionInStepMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.roleCanNotAccessSourceQuestionsForPrefillableQuestionInStepMessage';

    /**
     * @param InputNodeCollection<array-key, InputNode> $invalidSourceQuestions
     */
    public static function createMessageForPrefillableQuestion(
        PrefillableInputNode $prefillableQuestion,
        InputNodeCollection $invalidSourceQuestions,
        StepInterface $step,
        Role $role,
    ): self {
        // check if invalid questions are truly invalid (i.e.
        /** @var InputNode $invalidSourceQuestion */
        foreach ($invalidSourceQuestions as $invalidSourceQuestion) {
            Assert::false(
                $invalidSourceQuestion->isStepRoleReadAccessGranted($step, $role),
                ' can not build message '.self::class.'; the invalid source question ['
                .$invalidSourceQuestion->getLabel('en').']['.$invalidSourceQuestion->getUid()
                .'] is not invalid as prefilling question for prefillableQuestion ['
                .$prefillableQuestion->getLabel('en').']['.$prefillableQuestion->getUid()
                .']: this source question DOES grant at least red access in step ['
                .$step->getLabel('en').'] for role ['.$role->getName().']!'
            );
        }
        $label = $prefillableQuestion->getLabel('en');
        $sourceQuestionLabels = implode(' | ', $invalidSourceQuestions->getLabelsArray('en'));

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': you want to prefill the target question with uid '.$prefillableQuestion->getUid()
            .' and label "'.$label.'" with some source questions in step "'.$step->getLabel('en').'"for role "'.$role->getName().'".'
            .' this requires that each of those source questions grant at least read access to that role in that step.'
            .' but this role has no access in that step for the following source questions: ['.$sourceQuestionLabels.']';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $label,
                '%stepLabel%' => $step->getLabel('en'),
                '%role%' => $role->getName(),
                '%invalidSourceQuestionLabels%' => $sourceQuestionLabels,
            ]
        );
    }
}
