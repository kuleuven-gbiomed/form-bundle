<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;

final class ChoicePrefillOptionsDifferMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.OptionsOfPrefillQuestionsDiffer';

    public static function createMessageForPrefillableQuestion(ChoiceInputNode $choiceInputNode): self
    {
        $uid = $choiceInputNode->getUid();
        $label = $choiceInputNode->getLabelForFallBackLocale();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': you want to prefill the question with uid '.$uid
            .' and label "'.$label.'"'
            .', but the questions used to prefill have different options.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%label%' => $label, '%uid%' => $uid]
        );
    }
}
