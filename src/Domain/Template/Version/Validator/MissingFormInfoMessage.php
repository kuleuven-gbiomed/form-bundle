<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

final class MissingFormInfoMessage extends InvalidFormInfoMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.missingFormInfo';

    public static function createMessage(): self
    {
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG.': a form info is missing';

        return new self($message, self::TRANSLATION_KEY, self::TRANSLATION_DOMAIN);
    }
}
