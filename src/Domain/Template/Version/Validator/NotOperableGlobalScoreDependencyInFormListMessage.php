<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class NotOperableGlobalScoreDependencyInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.notOperableGlobalScoreDependency';

    public static function createMessageForGlobalScoreNodeAndNonOperableNode(
        ScorableInputNode $globalScoreNode,
        NodeInterface $nonOperableNode,
    ): self {
        $locale = $globalScoreNode->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();
        $questionLabel = $nonOperableNode->getNestedLabel($locale);

        Assert::true(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->hasOneByNodeUid($nonOperableNode->getUid()),
            ' can not build message '.self::class
            .'; the node ['.$questionLabel.']['.$nonOperableNode->getUid()
            .'] is not referenced as a dependency for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        Assert::false(
            $nonOperableNode instanceof ScorableInputNode,
            ' can not build message '.self::class
            .'; the node ['.$questionLabel.']['.$nonOperableNode->getUid()
            .'] is a scorable question node!'
        );

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the question ['.$questionLabel.'] is configured as a calculation dependency for the global score question ['.$globalScoreQuestionLabel.']. but it is not a type of question that is allowed to be used as a calculation dependency. Please remove this question as a calculation dependency for the global score question';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%questionLabel%' => $questionLabel,
            ]
        );
    }
}
