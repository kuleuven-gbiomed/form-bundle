<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

final class EmptyWorkFlowMessage extends InvalidWorkFlowMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.emptyWorkFlow';

    public static function createMessage(): self
    {
        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG.': WorkFlow is empty: it has no steps at all';

        return new self($message, self::TRANSLATION_KEY, self::TRANSLATION_DOMAIN);
    }
}
