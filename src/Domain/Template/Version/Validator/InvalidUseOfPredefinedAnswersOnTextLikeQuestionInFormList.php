<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;

class InvalidUseOfPredefinedAnswersOnTextLikeQuestionInFormList extends InvalidFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidUseOfPredefinedAnswersOnTextLikeQuestion';

    public static function createMessageForQuestion(
        TextInputNode $question,
    ): self {
        $questionLabel = $question->getNestedLabelForFallBackLocale();

        if (!$question->getInput()->hasPredefinedAnswers()) {
            throw new \BadMethodCallException(' can not build message '.self::class.'; the text question ['.$questionLabel.']['.$question->getUid().'] does NOT has predefined answers!');
        }

        if ($question->getInput()->isMultiLine()) {
            throw new \BadMethodCallException(' can not build message '.self::class.'; the text question ['.$questionLabel.']['.$question->getUid().'] is configured as multiline and thus is allowed to use predefined answers!');
        }

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the text question [{$questionLabel}] has predefined answers and is configured as single line text question. But only multiline text questions (textarea) can make use of predefined answers. Please either make this question multiline (textarea) or remove its predefined answers.";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%questionLabel%' => $questionLabel,
            ]
        );
    }
}
