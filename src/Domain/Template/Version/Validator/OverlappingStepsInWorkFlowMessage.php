<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepCollectionInterface;

final class OverlappingStepsInWorkFlowMessage extends InvalidWorkFlowMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.overlappingStepsInWorkFlow';

    /** @param StepCollectionInterface<array-key, StepInterface> $steps */
    public static function createMessageForSteps(StepCollectionInterface $steps): self
    {
        $stepLabelsString = implode(' | ', array_map(fn (StepInterface $step) => $step->getInfo()->getLocalizedLabel()->getFallback(), $steps->toArray()));

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG.': De workflow heeft overlappende stappen voor ['.$stepLabelsString
            .']. Zorg ervoor dat de begin- en einddatum van iedere stap niet met elkaar conflicteren.';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            ['%stepLabels%' => $stepLabelsString]
        );
    }
}
