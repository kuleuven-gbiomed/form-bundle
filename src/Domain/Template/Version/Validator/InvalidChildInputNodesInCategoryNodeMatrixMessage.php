<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;

final class InvalidChildInputNodesInCategoryNodeMatrixMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidChildInputNodesInCategoryNodeMatrix';

    public static function createMessageForCategoryNodeMatrix(CategoryNode $matrix): self
    {
        $matrixUid = $matrix->getUid();
        $matrixLabel = '[label unknown]';
        if ($matrix->getInfo()->getLocalizedLabel()->hasFallback()) {
            $matrixLabel = ' ['.$matrix->getInfo()->getLocalizedLabel()->getFallback().']';
        }

        $invalidNodesCount = $matrix->getChildren()->getInputNodes()->count();

        if (0 === $invalidNodesCount) {
            throw new \Exception(' can not build message '.self::class.": the matrix category [$matrixLabel] with id [$matrixUid] has no inputNode(s) (questions) as children!");
        }

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": de tabel [$matrixLabel] bevat [$invalidNodesCount] vragen die ingesteld zijn als rijen in plaats van kolommen."
            .'Gelieve eerst een rij (=categorie) toe te voegen aan de tabel en voeg dan de vragen toe als kolommen.exit';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%matrixId%' => $matrixUid,
                '%matrixLabel%' => $matrixLabel,
                '%invalidNodesCount%' => $invalidNodesCount,
            ]
        );
    }
}
