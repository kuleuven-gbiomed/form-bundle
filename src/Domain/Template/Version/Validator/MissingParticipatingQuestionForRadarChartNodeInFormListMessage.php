<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart\ParticipationQuestionEligibility;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;

class MissingParticipatingQuestionForRadarChartNodeInFormListMessage extends InvalidFormListMessage
{
    /** @var string */
    final public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidRadarChartNodeInFormList.missingParticipatingQuestionForRadarChartNode';

    public static function createMessageForRadarChartAndParticipationEligibility(
        RadarChartViewNode $chartViewNode,
        ParticipationQuestionEligibility $questionEligibility,
    ): self {
        $locale = $chartViewNode->getInfo()->getLocalizedLabel()->getFallbackLocale();
        $chartViewNodeLabel = $chartViewNode->getNestedLabel($locale);

        if (!$questionEligibility->refersToNonExistingQuestion()) {
            throw new \InvalidArgumentException(' can not build message '.self::class.' for radarChartNode ['.$chartViewNodeLabel.']: the given ParticipationQuestionEligibility [questionUid: '.$questionEligibility->getReferencedQuestionUid().'] does refer to an existing node/question!');
        }

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .': the list of participating questions for radar chart node ['.$chartViewNodeLabel.'] refers to a question with uid ['.$questionEligibility->getReferencedQuestionUid().' which does not exist in the tree. it might have been removed, in which case you need to remove the use of this question as participating question in radar chart node(s) as well';

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%chartViewNodeLabel%' => $chartViewNodeLabel,
                '%referencedQuestionUid%' => $questionEligibility->getReferencedQuestionUid(),
            ]
        );
    }
}
