<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;

final class MissingLabelForSubcategoryInCategoryNodeMatrixMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.missingLabelForSubcategoryInCategoryNodeMatrix';

    public static function createMessageForCategoryNodeMatrixAndSubcategory(CategoryNode $matrix, CategoryNode $subcategory): self
    {
        $matrixUid = $matrix->getUid();
        $subcategoryUid = $subcategory->getUid();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the subcategory with id [$subcategoryUid] in matrix with id [$matrixUid] needs to have a label but doesn't have one";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%matrixId%' => $matrixUid,
                '%subcategoryId%' => $subcategoryUid,
            ]
        );
    }
}
