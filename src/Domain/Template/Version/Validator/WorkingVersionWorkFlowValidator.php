<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\OverlapAwareStepCollection;
use KUL\FormBundle\Domain\Template\Version\PublishedVersion;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;

/**
 * Class WorkingVersionWorkFlowValidator.
 *
 * checks if a @see WorkFlow
 * in a @see WorkingVersion
 * in a @see TemplateInterface
 * is valid to be published as part of a @see PublishedVersion in that Template
 *
 * requires:
 * - a workflow @see WorkFlow (no workflow (yet) is possible when admins are just starting the working version)
 * - at least one step in workflow so there is at least a flow
 * - no overlapping periods in workflow steps to avoid having two steps open at the same time in a workflow
 *
 * @see     WorkingVersionValidator
 */
final class WorkingVersionWorkFlowValidator implements WorkingVersionValidatorInterface
{
    /** collection of all error message */
    private InvalidPublishMessageCollection $errorMessages;

    private function __construct(private readonly WorkingVersion $workingVersion)
    {
        $this->buildErrorMessages();
    }

    public static function fromWorkingVersion(WorkingVersion $workingVersion): self
    {
        return new self($workingVersion);
    }

    public function allowsPublish(): bool
    {
        return $this->getErrorMessages()->isEmpty();
    }

    public function getErrorMessages(): InvalidPublishMessageCollection
    {
        return $this->errorMessages;
    }

    private function buildErrorMessages(): void
    {
        $this->errorMessages = new InvalidPublishMessageCollection();

        if (!$this->workingVersion->hasWorkFlow()) {
            $this->errorMessages->add(MissingWorkFlowMessage::createMessage());

            return; // can not check further if no workflow set
        }

        $workFlowSteps = $this->workingVersion->getWorkFlow()->getSteps();
        if ($workFlowSteps->isEmpty()) {
            $this->errorMessages->add(EmptyWorkFlowMessage::createMessage());
        }

        // This is nice:
        if ($workFlowSteps instanceof OverlapAwareStepCollection && $workFlowSteps->hasOverlappingSteps()) {
            $this->errorMessages->add(
                OverlappingStepsInWorkFlowMessage::createMessageForSteps($workFlowSteps->getOverlappingSteps())
            );
        }

        // This is best effort; we can't check overlapping steps without reference date:
        if ($workFlowSteps instanceof MixedCalculatedAndFixedStepCollection
            && $workFlowSteps->hasReferenceDateIndependentOverlappingSteps()) {
            $this->errorMessages->add(
                OverlappingStepsInWorkFlowMessage::createMessageForSteps(
                    $workFlowSteps->getReferenceDateIndependentOverlappingSteps()
                )
            );
        }
    }
}
