<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use Webmozart\Assert\Assert;

final class InvalidCustomPassScoreForGlobalScoreInFormListMessage extends InvalidGlobalScoringInFormListMessage
{
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidCustomPassScoreForGlobalScore';

    public static function createMessageForGlobalScoreNode(ScorableInputNode $globalScoreNode): self
    {
        $globalScoreQuestionLabel = $globalScoreNode->getNestedLabelForFallBackLocale();

        $globalScoreUid = $globalScoreNode->getUid();
        Assert::false(
            $globalScoreNode->getGlobalScoreCalculationDependencyConfigs()->isEmpty(),
            ' can not build message '.self::class
            ."; the given node [$globalScoreQuestionLabel][$globalScoreUid] is not configured as a global score node (no dependency configurations found)"
        );

        $scoringParameters = $globalScoreNode->getInputScoringParameters();
        Assert::true(
            $scoringParameters->hasCustomPassScore(),
            ' can not build message '.self::class
            .'; the custom pass score in scoring parameters for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] is not set!'
        );

        Assert::true(
            $scoringParameters->hasValidScoringExtrema(),
            ' can not build message '.self::class
            .'; the extrema scoring parameters needed to check the custom pass score and scoring possible for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().'] are missing or invalid!'
        );

        $customPassScore = $scoringParameters->getCustomPassScore();
        $minScore = $scoringParameters->getMinScore();
        $maxScore = $scoringParameters->getMaxScore();

        Assert::false(
            $customPassScore >= $minScore && $customPassScore <= $maxScore,
            ' can not build message '.self::class
            .'; the custom pass score does fall within the extrema range in the scoring parameters for global score node ['
            .$globalScoreQuestionLabel.']['.$globalScoreNode->getUid().']!'
        );

        $customPassScore = (string) $scoringParameters->getCustomPassScore();
        $minScore = (string) $scoringParameters->getMinScore();
        $maxScore = (string) $scoringParameters->getMaxScore();

        $message = self::BASE_CANNOT_PUBLISH_VERSION_MSG
            .": the question [$globalScoreQuestionLabel] is configured as a global score question and a custom pass score [$customPassScore] was configured on it. but this custom pass score does not fall within the range of the minimum score [$minScore] and maximum score [$maxScore] configured on this global score question: the custom pass score must be less than or equal to the maximum score and greater than or equal to the minimum score. Please make sure the custom pass score is set within this range. alternatively, you can remove the custom pass score to allow pass score to fall back to the half between minimum and maximum score. or demote this global score question to a normal question by removing its dependencies on the questions it depends on for its calculation.";

        return new self(
            $message,
            self::TRANSLATION_KEY,
            self::TRANSLATION_DOMAIN,
            [
                '%globalQuestionLabel%' => $globalScoreQuestionLabel,
                '%customPassScore%' => $customPassScore,
                '%minScore%' => $minScore,
                '%maxScore%' => $maxScore,
            ]
        );
    }
}
