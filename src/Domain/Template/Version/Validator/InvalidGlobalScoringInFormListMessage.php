<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version\Validator;

use KUL\FormBundle\Domain\Template\Element\Node\Scoring\ScoringParameters;

class InvalidGlobalScoringInFormListMessage extends InvalidFormListMessage
{
    /** @var string */
    public const TRANSLATION_KEY = self::TRANSLATION_BASE_KEY.'.invalidGlobalScoreInFormList';

    protected static function getMissingScoringParameterLabels(ScoringParameters $scoringParameters): array
    {
        $scoringParameterLabels = [];

        foreach ($scoringParameters->getKeysOfParametersMissingForValidScoring() as $key) {
            $scoringParameterLabels[] = match ($key) {
                ScoringParameters::KEY_MIN_SCORE => 'minimum score',
                ScoringParameters::KEY_MAX_SCORE => 'maximum score',
                ScoringParameters::KEY_SCALE => 'scale (decimals)',
                ScoringParameters::KEY_ROUNDING_MODE => 'rounding mode',
                default => throw new \InvalidArgumentException('scoring parameter key '.$key.' is unhandled'),
            };
        }

        return $scoringParameterLabels;
    }
}
