<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Version;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Contract\VersionInterface;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;

/**
 * Class AbstractVersion.
 *
 * holds the elements that are versionable in a @see TemplateInterface
 */
abstract class AbstractVersion implements VersionInterface, \JsonSerializable, Deserializable
{
    protected ?FormInfo $info = null;
    protected ?WorkFlow $workFlow = null;
    protected FormList $formList;

    /**
     * @throws \BadMethodCallException
     */
    public function getInfo(): FormInfo
    {
        if (!$this->info instanceof FormInfo) {
            throw new \BadMethodCallException('no info found in version');
        }

        return $this->info;
    }

    public function getWorkFlow(): WorkFlow
    {
        if (!$this->workFlow instanceof WorkFlow) {
            throw new \BadMethodCallException('no workflow found in version');
        }

        return $this->workFlow;
    }

    public function getFormList(): FormList
    {
        return $this->formList;
    }
}
