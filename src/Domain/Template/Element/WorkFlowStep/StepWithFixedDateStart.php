<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\WorkFlowStep;

use KUL\FormBundle\Domain\Template\Contract\StepPeriodInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use Webmozart\Assert\Assert;

/**
 * Class StepWithFixedDateStart.
 *
 * a workflow step of this type has a fixed start & end date.
 *
 * @see     StepFixedPeriod
 *
 * the step can start if the fixed start date is reached or
 * if @see AbstractStep::allowsToStartPrematurelyIfPreviousStepIsFulFilled() is true and the preceding step in the
 * workflow is fulfilled in terms of having all questions answered & submitted.
 * the step will end if the fixed end date is reached (or indirectly closed of in the one-way workflow if the next step
 * has started prematurely).
 *
 * this type is less flexible than @see StepWithCalculatedDateStart as all subject entities that are targeted by a
 * template will have their form 'open' as soon as this start date is reached and end date is not yet reached.
 *
 * NOTE: since steps of this type work with fixed dates, when end-users are to access this step as part of a @see WorkFlow
 * of a @see TemplateInterface ), the date NOW will be used as date to check if this step is open for the end-user.
 * but when admins want to test this step as part of a workflow in the admin-side, they will (need to be able to) use
 * a different date so this specific step can be tested as part of a workflow in a template under construction.
 */
final class StepWithFixedDateStart extends AbstractStep
{
    /** @var string */
    public const TYPE = 'stepWithFixedDateStart';

    protected static function guardStepPeriodClass(StepPeriodInterface $period): void
    {
        Assert::isInstanceOf($period, StepFixedPeriod::class);
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    public function getPeriod(): StepFixedPeriod
    {
        /** @var StepFixedPeriod $period */
        $period = parent::getPeriod();

        return $period;
    }

    /**
     * checks if there is overlap between @see StepFixedPeriod periods for this and given control step.
     *
     * @throws \InvalidArgumentException if controlStep is not of type @see StepWithFixedDateStart
     */
    public function overlapsWithFixedStep(self $controlStep): bool
    {
        return $this->getPeriod()->overlapsWithFixedPeriod($controlStep->getPeriod());
    }

    /**
     * checks if there is step starts before the given control step does.
     *
     * @throws \InvalidArgumentException if controlStep is not of type @see StepWithFixedDateStart
     */
    public function startsBeforeFixedStep(self $controlStep): bool
    {
        return $this->getPeriod()->startsBeforeFixedPeriod(
            $controlStep->getPeriod()
        );
    }

    public function isEndedByDate(\DateTimeInterface $now): bool
    {
        return $this->getPeriod()->isEndedByDate($now);
    }

    /**
     * @throws StepException
     */
    public static function getPeriodFromJsonDecodedArray(array $data): StepFixedPeriod
    {
        return StepFixedPeriod::fromJsonDecodedArray($data);
    }

    public function isOpenByDate(\DateTimeInterface $checkDate): bool
    {
        return $this->getPeriod()->isOpenByDate($checkDate);
    }

    public function isStartedByDate(\DateTimeInterface $checkDate): bool
    {
        return $this->getPeriod()->isStartedByDate($checkDate);
    }
}
