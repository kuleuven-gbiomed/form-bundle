<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\WorkFlowStep;

use KUL\FormBundle\Domain\Template\Contract\StepPeriodInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use Webmozart\Assert\Assert;

/**
 * Class StepWithCalculatedDateStart.
 *
 * a workflow step of this type uses start and end dates that are derived (calculated by adding amounts of days to a
 * significant date) from the objects/entities that are targeted by the Template to which the step belongs.
 *
 * @see     StepCalculatedPeriod
 *
 * the step can start if the date - that is calculated by adding or subtracting days to a given start date - is reached
 * or if @see AbstractStep::allowsToStartPrematurelyIfPreviousStepIsFulFilled() is true and the preceding step in the
 * workflow is fulfilled in terms of having all questions answered & submitted.
 * the step will end if the date - that is calculated by adding or subtracting days to a given end date - is reached
 * (or indirectly closed of in the one-way workflow if the next step has started prematurely).
 *
 * a workflow step of this type does not has a fixed start & end date like @see StepWithFixedDateStart.
 * instead it stores amounts of days to be added or subtracted from a date that is passed in as the start date in the
 * step on the moment the step is called on. note that the amounts of days (add or subtract) are split up in an amount
 * for calculating the start date and an amount for calculating the end date. those 'calculated' dates can then be used
 * as start and end date.
 * this allows to work with variable start dates within one template, depending on a (unique) date of the (instance of)
 * subject entity that the template targets. both start and end date can thus also be derived from a single given date.
 * important remark in that regard is that a domain decision was decided upon to always use the same variable date to
 * calculate both start and end date when using this type of step. hence the amount of days to calculate the start date
 * is asserted to not be greater than amount of days to calculate the end date in @see StepCalculatedPeriod .
 * e.g. when a form is to be rendered/build for a template that targets placements in an internship, the
 * placement-period-startDate is used to determine if this step is 'open' by 'calculating' the step's start date by
 * adding x amount of days to that placement-period-startDate and 'calculating' the step's end date by adding y amount
 * of days to that SAME placement-period-startDate. if the current date-time then is between that calculated start and
 * end date, the step is 'open'. as such only the placement period's start date is needed to determine is step is open.
 */
final class StepWithCalculatedDateStart extends AbstractStep
{
    /** @var string */
    public const TYPE = 'stepWithCalculatedDateStart';

    protected static function guardStepPeriodClass(StepPeriodInterface $period): void
    {
        Assert::isInstanceOf($period, StepCalculatedPeriod::class);
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    public function getPeriod(): StepCalculatedPeriod
    {
        /** @var StepCalculatedPeriod $period */
        $period = parent::getPeriod();

        return $period;
    }

    /**
     * checks if there is overlap between @see StepCalculatedPeriod periods for this and given control step.
     */
    public function overlapsWithCalculatedStep(self $controlStep): bool
    {
        return $this->getPeriod()->overlapsWithCalculatedPeriod($controlStep->getPeriod());
    }

    /**
     * checks if there is step starts before the given control step does,
     * based on an arbitrary dynamically date to calculated start date of each step.
     *
     * @throws \InvalidArgumentException if controlStep is not of type @see StepWithCalculatedDateStart
     */
    public function startsBeforeCalculatedStep(self $controlStep): bool
    {
        return $this->getPeriod()->startsBeforeCalculatedPeriod(
            $controlStep->getPeriod()
        );
    }

    /**
     * @throws StepException
     */
    public static function getPeriodFromJsonDecodedArray(array $data): StepCalculatedPeriod
    {
        return StepCalculatedPeriod::fromJsonDecodedArray($data);
    }

    public function isEndedByDate(\DateTimeInterface $now, \DateTimeInterface $referenceDate): bool
    {
        return $this->getPeriod()->isEndedByDate($now, $referenceDate);
    }

    public function isStartedByDate(\DateTimeInterface $checkDate, \DateTimeInterface $referenceDate): bool
    {
        return $this->getPeriod()->isStartedByDate($checkDate, $referenceDate);
    }

    public function isOpenByDate(\DateTimeInterface $checkDate, \DateTimeInterface $referenceDate): bool
    {
        return $this->getPeriod()->isStartedByDate($checkDate, $referenceDate)
            && !$this->getPeriod()->isEndedByDate($checkDate, $referenceDate);
    }
}
