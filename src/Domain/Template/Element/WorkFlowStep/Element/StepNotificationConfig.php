<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Utility\Collection\RoleNotificationConfigCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Class StepNotificationConfig.
 *
 * bundles configuration for a set of roles used for notifying users with those roles that the user can/should/could
 * participate in a step. e.g. to notify/remind relevant users that a step has started for user-role interaction
 *
 * note: the moment on which both start & remind notification for each role will be sent out, will always be determined
 * based on the same (calculated or fixed) start date of the step to which this notificationConfig belongs, using the
 * offset in days as configured for each (start or/and remind) role-notification-config. it is also allowed to only
 * configure a role to get a remind notification, without that role being configured to be notified of the start.
 * e.g. in case the admin creator of the form-template does not want the system to sent out start notifications because
 * he/she wants to 'manually' sent out custom mails to selected users by using Outlook or something. but the admin still
 * wants the system to remind users that do not have done their part after a certain amount of days after the step's
 * start (= days offset) that they urgently have to do their part (=answers questions in this step).
 * since a reminder is something to be sent out AFTER a start notification, if both start & remind notification
 * configurations is set for a role, then the days offset for the remind notification for that role must be greater
 * than the days offset for the start notification for that role.
 * remark: a step can be configured to start prematurely of the start date, in which case same logic as above applies.
 *
 * assert for each role for which both start & remind notification are to be sent, that the remind notification
 * can only be sent AFTER the start notification (i.e. each notification will be sent based the offset in days
 */
final class StepNotificationConfig implements \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_ROLE_CONFIGS_TO_NOTIFY_OF_START = 'roleConfigsToNotifyOfStart';
    /** @var string */
    public const KEY_ROLE_CONFIGS_TO_REMIND_OF_ONGOING = 'roleConfigsToRemindOfOngoing';
    /** @var string */
    public const KEY_MAIL_BODY_FOR_START = 'mailBodyForStart';
    /** @var string */
    public const KEY_MAIL_BODY_FOR_ONGOING = 'mailBodyForOngoing';
    /** @var string */
    public const KEY_ROLE_MAIL_BODIES_FOR_START = 'roleMailBodiesForStart';
    /** @var string */
    public const KEY_ROLE_MAIL_BODIES_FOR_ONGOING = 'roleMailBodiesForOngoing';

    /**
     * configuration per role to notify of start of step (with +/- days offset relative to (calculated) start date of step).
     */
    private readonly RoleNotificationConfigCollection $roleConfigsToNotifyOfStart;

    /**
     * configuration per role to remind of ongoing step (with +/- days offset relative to (calculated) start date of step).
     */
    private readonly RoleNotificationConfigCollection $roleConfigsToRemindOfOngoing;

    /**
     * @throws StepNotificationException
     */
    private function __construct(
        RoleNotificationConfigCollection $roleConfigsToNotifyOfStart,
        RoleNotificationConfigCollection $roleConfigsToRemindOfOngoing,
        private readonly LocalizedOptionalString $mailBodyForStartNotification, // optional translated text as mail body when sending notify-of-start emails.
        private readonly LocalizedOptionalString $mailBodyForOngoingNotification, // optional translated text as mail body when sending notify-of-ongoing emails.
        private readonly RoleLocalizedOptionalStringCollection $roleMailBodiesForStartNotification, // collection of optional translated role-based texts as mail body when sending notify-of-ongoing emails for specified roles.
        private readonly RoleLocalizedOptionalStringCollection $roleMailBodiesForOngoingNotification, // collection of  optional translated role-based texts as mail body when sending notify-of-start emails for specified roles.
    ) {
        // assert for each role for which both start & remind notification are to be sent, that the remind notification
        // can only be sent AFTER the start notification (i.e. remind offset must be greater than start offset)
        foreach ($roleConfigsToRemindOfOngoing as $roleRemindConfig) {
            $role = $roleRemindConfig->getRole();
            // skip if no start notification is configured for this role
            if (!$roleConfigsToNotifyOfStart->hasOneForRole($role)) {
                continue;
            }

            $roleStartConfig = $roleConfigsToNotifyOfStart->getOneForRole($role);
            // ok if reminder can only result in being sent out after start notification
            if ($roleRemindConfig->getDaysOffset() > $roleStartConfig->getDaysOffset()) {
                continue;
            }

            // fail if reminder could result in being sent out before start notification
            throw new StepNotificationException('invalid days offset configured for ongoing notification for role '.$roleRemindConfig->getRole()->getName().':  days offset for ongoing notification ['.((string) $roleRemindConfig->getDaysOffset()).'] must be greater than days offset for start notification ['.((string) $roleStartConfig->getDaysOffset()).'] to assure that ongoing notification as reminder will not be sent out before a start notification is sent out');
        }

        $this->roleConfigsToNotifyOfStart = $roleConfigsToNotifyOfStart;
        $this->roleConfigsToRemindOfOngoing = $roleConfigsToRemindOfOngoing;
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public static function createEmpty(): self
    {
        return new self(
            RoleNotificationConfigCollection::createEmpty(),
            RoleNotificationConfigCollection::createEmpty(),
            LocalizedOptionalString::createEmpty(),
            LocalizedOptionalString::createEmpty(),
            RoleLocalizedOptionalStringCollection::createEmpty(),
            RoleLocalizedOptionalStringCollection::createEmpty(),
        );
    }

    /**
     * @throws StepNotificationException
     */
    public static function fromRoleNotificationsAndMailBodies(
        RoleNotificationConfigCollection $roleConfigsToNotifyOfStart,
        RoleNotificationConfigCollection $roleConfigsToRemindOfOngoing,
        LocalizedOptionalString $mailBodyForStartNotification,
        LocalizedOptionalString $mailBodyForOngoingNotification,
        ?RoleLocalizedOptionalStringCollection $roleMailBodiesForStartNotification = null,
        ?RoleLocalizedOptionalStringCollection $roleMailBodiesForOngoingNotification = null,
    ): self {
        return new self(
            $roleConfigsToNotifyOfStart,
            $roleConfigsToRemindOfOngoing,
            $mailBodyForStartNotification,
            $mailBodyForOngoingNotification,
            $roleMailBodiesForStartNotification ?? new RoleLocalizedOptionalStringCollection(),
            $roleMailBodiesForOngoingNotification ?? new RoleLocalizedOptionalStringCollection(),
        );
    }

    /**
     * @throws StepNotificationException
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $startData = $data[self::KEY_ROLE_CONFIGS_TO_NOTIFY_OF_START] ?? [];
        $ongoingData = $data[self::KEY_ROLE_CONFIGS_TO_REMIND_OF_ONGOING] ?? [];
        $startMailBodyData = $data[self::KEY_MAIL_BODY_FOR_START] ?? [];
        $ongoingMailBodyData = $data[self::KEY_MAIL_BODY_FOR_ONGOING] ?? [];
        $roleStartMailBodiesData = $data[self::KEY_ROLE_MAIL_BODIES_FOR_START] ?? [];
        $roleOngoingMailBodiesData = $data[self::KEY_ROLE_MAIL_BODIES_FOR_ONGOING] ?? [];

        return new self(
            RoleNotificationConfigCollection::fromJsonDecodedArray($startData),
            RoleNotificationConfigCollection::fromJsonDecodedArray($ongoingData),
            LocalizedOptionalString::fromJsonDecodedArray($startMailBodyData),
            LocalizedOptionalString::fromJsonDecodedArray($ongoingMailBodyData),
            RoleLocalizedOptionalStringCollection::fromJsonDecodedArray($roleStartMailBodiesData),
            RoleLocalizedOptionalStringCollection::fromJsonDecodedArray($roleOngoingMailBodiesData),
        );
    }

    public function getRoleConfigsToNotifyOfStart(): RoleNotificationConfigCollection
    {
        return $this->roleConfigsToNotifyOfStart;
    }

    public function getRoleConfigsToRemindOfOngoing(): RoleNotificationConfigCollection
    {
        return $this->roleConfigsToRemindOfOngoing;
    }

    public function getLocalizedMailBodyForStartNotification(): LocalizedOptionalString
    {
        return $this->mailBodyForStartNotification;
    }

    /**
     * @throws LocalizedStringException if string locale is invalid
     */
    public function hasMailBodyForStartNotification(string $locale): bool
    {
        return $this->getLocalizedMailBodyForStartNotification()->hasForLocaleOrFallback($locale);
    }

    /**
     * @throws LocalizedStringException if string locale is invalid or no translation for locale or fallback found
     */
    public function getMailBodyForStartNotification(string $locale): string
    {
        return $this->getLocalizedMailBodyForStartNotification()->getForLocaleOrFallback($locale);
    }

    public function getLocalizedMailBodyForOngoingNotification(): LocalizedOptionalString
    {
        return $this->mailBodyForOngoingNotification;
    }

    /**
     * @throws LocalizedStringException if string locale is invalid
     */
    public function hasMailBodyForOngoingNotification(string $locale): bool
    {
        return $this->getLocalizedMailBodyForOngoingNotification()->hasForLocaleOrFallback($locale);
    }

    /**
     * @throws LocalizedStringException if string locale is invalid or no translation for locale or fallback found
     */
    public function getMailBodyForOngoingNotification(string $locale): string
    {
        return $this->getLocalizedMailBodyForOngoingNotification()->getForLocaleOrFallback($locale);
    }

    public function getLocalizedRoleMailBodiesForStartNotification(): RoleLocalizedOptionalStringCollection
    {
        return $this->roleMailBodiesForStartNotification;
    }

    public function hasRoleMailBodyForStartNotification(Role $role): bool
    {
        return $this->roleMailBodiesForStartNotification->hasOneForRole($role) && $this->roleMailBodiesForStartNotification->getOneForRole($role)->hasTranslations();
    }

    public function getRoleMailBodyForStartNotification(Role $role, string $locale): string
    {
        return $this->roleMailBodiesForStartNotification->getOneForRole($role)->getLocalizedString()->getForLocaleOrFallback($locale);
    }

    public function getLocalizedRoleMailBodiesForOngoingNotification(): RoleLocalizedOptionalStringCollection
    {
        return $this->roleMailBodiesForOngoingNotification;
    }

    public function hasRoleMailBodyForOngoingNotification(Role $role): bool
    {
        return $this->roleMailBodiesForOngoingNotification->hasOneForRole($role) && $this->roleMailBodiesForStartNotification->getOneForRole($role)->hasTranslations();
    }

    public function getRoleMailBodyForOngoingNotification(Role $role, string $locale): string
    {
        return $this->roleMailBodiesForOngoingNotification->getOneForRole($role)->getLocalizedString()->getForLocaleOrFallback($locale);
    }

    /**
     * @return array{roleConfigsToNotifyOfStart: RoleNotificationConfigCollection, roleConfigsToRemindOfOngoing: RoleNotificationConfigCollection, mailBodyForStart: LocalizedOptionalString, mailBodyForOngoing: LocalizedOptionalString, roleMailBodiesForStart: RoleLocalizedOptionalStringCollection, roleMailBodiesForOngoing: RoleLocalizedOptionalStringCollection}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_ROLE_CONFIGS_TO_NOTIFY_OF_START => $this->getRoleConfigsToNotifyOfStart(),
            self::KEY_ROLE_CONFIGS_TO_REMIND_OF_ONGOING => $this->getRoleConfigsToRemindOfOngoing(),
            self::KEY_MAIL_BODY_FOR_START => $this->getLocalizedMailBodyForStartNotification(),
            self::KEY_MAIL_BODY_FOR_ONGOING => $this->getLocalizedMailBodyForOngoingNotification(),
            self::KEY_ROLE_MAIL_BODIES_FOR_START => $this->getLocalizedRoleMailBodiesForStartNotification(),
            self::KEY_ROLE_MAIL_BODIES_FOR_ONGOING => $this->getLocalizedRoleMailBodiesForOngoingNotification(),
        ];
    }
}
