<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;

/**
 * Class StepSubmitSuccessUserInteraction.
 *
 * bundles all optional messaging, notifying & UX related interaction to prompt on a successful submit by a user
 *
 * developer notice: the main reason for this bundling class is because this remains WIP code: I learned that additional
 * parameters could/will have to be added that neither we nor client has thought of, or that are requested by end-users (clients)
 * or admins. bundling them here allows serialization as one bundle, makes adding (additional parameters) later on easier
 */
final readonly class StepSubmitSuccessUserInteraction implements \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_NOTIFICATION_CONFIG = 'notificationConfig';
    /** @var string */
    public const KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE = 'generalSubmitSuccessMessage';
    /** @var string */
    public const KEY_ROLE_SUBMIT_SUCCESS_MESSAGES = 'roleSubmitSuccessMessages';
    /** @var string */
    public const KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS = 'redirectAwayOnSubmitSuccess';

    /**
     * StepSubmitSuccessUserInteraction constructor.
     */
    private function __construct(
        private StepNotificationConfig $notificationConfig, // optional configuration per role for usage in notification, mailing, etc.
        private LocalizedOptionalString $submitSuccessMessage, // main optional success message to be shown independent of submitting role
        private RoleLocalizedOptionalStringCollection $roleSubmitSuccessMessages,
        private bool $redirectAwayOnSubmitSuccess,
    ) {
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public static function createEmptyWithDefaults(): self
    {
        return new self(
            StepNotificationConfig::createEmpty(),
            LocalizedOptionalString::createEmpty(),
            RoleLocalizedOptionalStringCollection::createEmpty(),
            false
        );
    }

    public static function fromNotificationConfigAndMessages(
        StepNotificationConfig $notificationConfig,
        LocalizedOptionalString $submitSuccessMessage,
        RoleLocalizedOptionalStringCollection $roleSubmitSuccessMessages,
        bool $redirectAwayOnSubmitSuccess = false,
    ): self {
        return new self($notificationConfig, $submitSuccessMessage, $roleSubmitSuccessMessages, $redirectAwayOnSubmitSuccess);
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $notifyData = $data[self::KEY_NOTIFICATION_CONFIG] ?? [];
        $msgData = $data[self::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE] ?? [];
        $roleMsgData = $data[self::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES] ?? [];
        $redirect = $data[self::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS] ?? false;

        return new self(
            StepNotificationConfig::fromJsonDecodedArray($notifyData),
            LocalizedOptionalString::fromJsonDecodedArray($msgData),
            RoleLocalizedOptionalStringCollection::fromJsonDecodedArray($roleMsgData),
            $redirect
        );
    }

    /**
     * config data for each role that requires associated step-participating users to be notified of step status changes.
     */
    public function getNotificationConfig(): StepNotificationConfig
    {
        return $this->notificationConfig;
    }

    /**
     * optional localized message to be shown as response to each successful submit within the step.
     */
    public function getSubmitSuccessMessage(): LocalizedOptionalString
    {
        return $this->submitSuccessMessage;
    }

    /**
     * optional role related localized messages to be shown as response to each successful submit within the step.
     */
    public function getRoleSubmitSuccessMessages(): RoleLocalizedOptionalStringCollection
    {
        return $this->roleSubmitSuccessMessages;
    }

    public function shouldRedirectAwayOnSubmitSuccess(): bool
    {
        return $this->redirectAwayOnSubmitSuccess;
    }

    /**
     * @return array{notificationConfig: StepNotificationConfig, generalSubmitSuccessMessage: LocalizedOptionalString, roleSubmitSuccessMessages: RoleLocalizedOptionalStringCollection, redirectAwayOnSubmitSuccess: bool}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_NOTIFICATION_CONFIG => $this->getNotificationConfig(),
            self::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => $this->getSubmitSuccessMessage(),
            self::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => $this->getRoleSubmitSuccessMessages(),
            self::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => $this->shouldRedirectAwayOnSubmitSuccess(),
        ];
    }
}
