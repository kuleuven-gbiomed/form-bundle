<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Utility\Collection\RoleNotificationConfigCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * contains configuration for a specific role for notifying participating users in a template with that role that
 * they can/should/could interact in a @see StepInterface , based on a certain offset in days in regards
 * to a start/end date of that step. e.g. to notify/remind relevant user with this role that a step has started.
 *
 * TODO more than likely, more (optional) parameters will be added to this role related notification config class
 *
 * @see     RoleNotificationConfigCollection
 * @see     StepNotificationConfig
 */
final readonly class RoleNotificationConfig implements \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_ROLE = 'role';
    /** @var string */
    public const KEY_OFFSET = 'offset';

    /**
     * argument $daysOffset:
     * positive or negative offset for use in determining how many days after or before a (calculated) step date
     * the notification is to be triggered.
     */
    private function __construct(
        private Role $role,
        private int $daysOffset,
    ) {
    }

    public static function fromRoleAndOffset(Role $role, int $daysOffset = 0): self
    {
        return new self($role, $daysOffset);
    }

    /**
     * @throws StepNotificationException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        return new self(Role::fromName($data[self::KEY_ROLE]), $data[self::KEY_OFFSET]);
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    public function getDaysOffset(): int
    {
        return $this->daysOffset;
    }

    /**
     * @return array{role: Role, offset: int}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_ROLE => $this->getRole(),
            self::KEY_OFFSET => $this->getDaysOffset(),
        ];
    }
}
