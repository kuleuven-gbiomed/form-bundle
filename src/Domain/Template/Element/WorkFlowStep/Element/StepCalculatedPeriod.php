<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Factory\DateTimeImmutableFactory;
use KUL\FormBundle\Domain\Template\Contract\StepPeriodInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Exception\StepException;

/**
 * period that opens a step by calculating start & end dates based on a set of amount of days to add or subtract from a
 * custom on-the-fly given variable date, usually associated with the entity that the @see TemplateInterface targets.
 */
final class StepCalculatedPeriod implements StepPeriodInterface, \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_AMOUNT_OF_DAYS_TO_CALCULATE_START_DATE = 'amountOfDaysToCalculateStartDate';
    /** @var string */
    public const KEY_AMOUNT_OF_DAYS_TO_CALCULATE_END_DATE = 'amountOfDaysToCalculateEndDate';

    /** positive or negative amount of days to add or subtract from a given date to obtain a calculated startDate */
    private readonly int $amountOfDaysToCalculateStartDate;
    /** positive or negative amount of days to add or subtract from a given date to obtain a calculated endDate */
    private readonly int $amountOfDaysToCalculateEndDate;

    /**
     * @throws StepException
     */
    private function __construct(int $amountOfDaysToCalculateStartDate, int $amountOfDaysToCalculateEndDate)
    {
        // domain decision: same variable date is always used to calculate both start and end date when checking for
        // open state of a step, hence we can assert that the amount for start must be less than amount for end.
        // both params must be integers, asserting here as well that minimal period is one day (less is pointless)
        if ($amountOfDaysToCalculateStartDate >= $amountOfDaysToCalculateEndDate) {
            throw new StepException("amountOfDaysToCalculateStartDate [$amountOfDaysToCalculateStartDate] must be less than amountOfDaysToCalculateEndDate [$amountOfDaysToCalculateEndDate]");
        }

        $this->amountOfDaysToCalculateStartDate = $amountOfDaysToCalculateStartDate;
        $this->amountOfDaysToCalculateEndDate = $amountOfDaysToCalculateEndDate;
    }

    /**
     * @throws StepException
     */
    public static function fromCalculatingAmounts(
        int $amountOfDaysToCalculateStartDate,
        int $amountOfDaysToCalculateEndDate,
    ): self {
        return new self($amountOfDaysToCalculateStartDate, $amountOfDaysToCalculateEndDate);
    }

    /**
     * @throws StepException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        return new self(
            $data[self::KEY_AMOUNT_OF_DAYS_TO_CALCULATE_START_DATE],
            $data[self::KEY_AMOUNT_OF_DAYS_TO_CALCULATE_END_DATE]
        );
    }

    public function getAmountOfDaysToCalculateStartDate(): int
    {
        return $this->amountOfDaysToCalculateStartDate;
    }

    public function getAmountOfDaysToCalculateEndDate(): int
    {
        return $this->amountOfDaysToCalculateEndDate;
    }

    /**
     * adds or subtracts configured amount of days to the given date and returns a new modified immutable date.
     */
    public function getCalculatedStartDate(\DateTimeInterface $date): \DateTimeImmutable
    {
        return DateTimeImmutableFactory::createFromDateModifiedWithDays(
            $date,
            $this->getAmountOfDaysToCalculateStartDate()
        );
    }

    public function getCalculatedEndDate(\DateTimeInterface $date): \DateTimeImmutable
    {
        return DateTimeImmutableFactory::createFromDateModifiedWithDays(
            $date,
            $this->getAmountOfDaysToCalculateEndDate()
        );
    }

    /**
     * Checks whether the period has been started at $checkDate, given the provided $referenceDate.
     *
     * step period is started when the reference date - plus/minus configured amount of days - is in the past
     * compared to $checkDate.
     */
    public function isStartedByDate(\DateTimeInterface $checkDate, \DateTimeInterface $referenceDate): bool
    {
        return $checkDate >= $this->getCalculatedStartDate($referenceDate);
    }

    /**
     * Checks whether the period has been ended at $checkDate, given the provided $referenceDate.
     *
     * step period has ended when the reference date - plus/minus configured amount of days - is in the past
     * compared to $checkDate.
     */
    public function isEndedByDate(\DateTimeInterface $checkDate, \DateTimeInterface $referenceDate): bool
    {
        return $checkDate >= $this->getCalculatedEndDate($referenceDate);
    }

    /**
     * checks if their is overlap between this period and a given period.
     *
     * If the second period starts at the same moment the first ends, this is not considered overlap.
     */
    public function overlapsWithCalculatedPeriod(self $period): bool
    {
        return $this->getAmountOfDaysToCalculateStartDate() < $period->getAmountOfDaysToCalculateEndDate()
            && $period->getAmountOfDaysToCalculateStartDate() < $this->getAmountOfDaysToCalculateEndDate();
    }

    /**
     * @return array{amountOfDaysToCalculateStartDate: int, amountOfDaysToCalculateEndDate: int}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_AMOUNT_OF_DAYS_TO_CALCULATE_START_DATE => $this->getAmountOfDaysToCalculateStartDate(),
            self::KEY_AMOUNT_OF_DAYS_TO_CALCULATE_END_DATE => $this->getAmountOfDaysToCalculateEndDate(),
        ];
    }

    public function startsBeforeCalculatedPeriod(self $other): bool
    {
        return $this->getAmountOfDaysToCalculateStartDate() < $other->getAmountOfDaysToCalculateStartDate();
    }

    public function isOpenByDate(\DateTimeInterface $checkDate, \DateTimeInterface $referenceDate): bool
    {
        return $this->isStartedByDate($checkDate, $referenceDate) && !$this->isEndedByDate($checkDate, $referenceDate);
    }
}
