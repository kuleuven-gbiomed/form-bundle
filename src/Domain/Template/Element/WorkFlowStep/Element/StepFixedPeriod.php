<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Factory\DateTimeImmutableFactory;
use KUL\FormBundle\Domain\Template\Contract\StepPeriodInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;

/**
 * Class StepFixedPeriod.
 *
 * period that opens a step by fixed start & end dates and checked against a given date (usually the current date time)
 *
 * NOTE: this period is used in step of type @see StepWithFixedDateStart: when end-users are to access such a step as
 * part of a @see WorkFlow of a @see TemplateInterface ), the date NOW will be used as date to check this period
 * via @see StepFixedPeriod::isOpenByDate() . but when admins want to test this step as part of a workflow in the
 * admin-side, they will (need to be able to) use a different date to be able to override/force this period to be open.
 */
final class StepFixedPeriod implements StepPeriodInterface, \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_FIXED_START_DATE = 'fixedStartDate';
    /** @var string */
    public const KEY_FIXED_END_DATE = 'fixedEndDate';

    private readonly \DateTimeImmutable $fixedStartDate;
    private readonly \DateTimeImmutable $fixedEndDate;

    /**
     * @throws StepException
     */
    private function __construct(\DateTimeImmutable $fixedStartDate, \DateTimeImmutable $fixedEndDate)
    {
        // ensure dates are in expected format (without microseconds)
        $startDate = DateTimeImmutableFactory::createFromDate($fixedStartDate);
        $endDate = DateTimeImmutableFactory::createFromDate($fixedEndDate);

        // we could enforce a minimal difference of 1 day between start and end dates since less makes the step a bit
        // pointless. but since only admins will build periods, we will rely on their common sense here
        if ($startDate >= $endDate) {
            throw new StepException('fixedStartDate ['.$startDate->format(self::DATE_FORMAT).'] must be before fixedEndDate ['.$endDate->format(self::DATE_FORMAT).']');
        }

        $this->fixedStartDate = $startDate;
        $this->fixedEndDate = $endDate;
    }

    /**
     * @throws StepException
     */
    public static function fromDates(\DateTimeImmutable $fixedStartDate, \DateTimeImmutable $fixedEndDate): self
    {
        return new self($fixedStartDate, $fixedEndDate);
    }

    /**
     * @throws StepException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        // when coming from json data serialized by this class, date string will be in correct format
        // if not, constructor will strip microseconds anyway (or throw exception if createFromFormat returns false)
        /** @var \DateTimeImmutable $start */
        $start = \DateTimeImmutable::createFromFormat(self::DATE_FORMAT, $data[self::KEY_FIXED_START_DATE]);
        /** @var \DateTimeImmutable $end */
        $end = \DateTimeImmutable::createFromFormat(self::DATE_FORMAT, $data[self::KEY_FIXED_END_DATE]);

        return new self($start, $end);
    }

    public function getFixedStartDate(): \DateTimeImmutable
    {
        return $this->fixedStartDate;
    }

    public function getFixedEndDate(): \DateTimeImmutable
    {
        return $this->fixedEndDate;
    }

    /**
     * when end-users are to access this period attached to a step as part of a workflow, the date NOW will be the
     * given date. when admins access this period for testing a step using this period, the date can be not NOW.
     */
    public function isStartedByDate(\DateTimeInterface $date): bool
    {
        return DateTimeImmutableFactory::createFromDate($date) >= $this->getFixedStartDate();
    }

    /**
     * when end-users are to access this period attached to a step as part of a workflow, the date NOW will be the
     * given date. when admins access this period for testing a step using this period, the date can be not NOW.
     */
    public function isEndedByDate(\DateTimeInterface $date): bool
    {
        return DateTimeImmutableFactory::createFromDate($date) >= $this->getFixedEndDate();
    }

    /**
     * checks if their is overlap between this period and a given period.
     *
     * If the second period starts at the same moment the first ends, this is not considered overlap.
     */
    public function overlapsWithFixedPeriod(self $period): bool
    {
        return $this->getFixedStartDate() < $period->getFixedEndDate()
            && $period->getFixedStartDate() < $this->getFixedEndDate();
    }

    /**
     * @return array{fixedStartDate: string, fixedEndDate: string}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_FIXED_START_DATE => $this->getFixedStartDate()->format(self::DATE_FORMAT),
            self::KEY_FIXED_END_DATE => $this->getFixedEndDate()->format(self::DATE_FORMAT),
        ];
    }

    public function startsBeforeFixedPeriod(self $other): bool
    {
        return $this->getFixedStartDate() < $other->getFixedStartDate();
    }

    public function isOpenByDate(\DateTimeInterface $checkDate): bool
    {
        return $this->isStartedByDate($checkDate) && !$this->isEndedByDate($checkDate);
    }
}
