<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\WorkFlowStep;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Contract\UIdentifiableInterface;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\StepPeriodInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Utility\Collection\AbstractStepCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * bundles all configuration for a step to operate in a template form's workflow.
 *
 * note: in a valid workflow, all steps in the workflow must be of the same type (same implementation of this abstract).
 *
 * @see     AbstractStepCollection
 */
abstract class AbstractStep implements StepInterface, UIdentifiableInterface, \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_CUSTOM_OPTION_SKIP_ADMIN_ROLES_FOR_FULFILMENT_CHECK = 'custom_option_skip_admin_roles_for_fulfilment_check';

    private readonly string $uid;

    abstract public static function getType(): string;

    abstract protected static function guardStepPeriodClass(StepPeriodInterface $period): void;
    //    abstract public function getPeriod(): StepPeriodInterface;
    private readonly StepPeriodInterface $period;

    /**
     * @throws StepException
     *
     * argument $customOptions:
     * this bundle is in constant WIP. this prop allows to add additional options. which by preference should all remain
     * optional and/or should not be of the all-that important kind. this property is added as trade-off for the WIP
     * status to allow to code faster without breaking existing code and tests. values(s) for any custom option should
     * either be a primitive type for the jsonSerialize to work or contain an object implementing @see \JsonSerializable.
     */
    final public function __construct(
        string $uid,
        StepPeriodInterface $period,
        private readonly bool $allowsStartIfPreviousStepIsFulfilled,
        private readonly RoleCollection $rolesLimitingUserToOneSubmitPerRole,
        private readonly LocalizedInfoWithRequiredLabel $info,
        private readonly StepSubmitSuccessUserInteraction $submitSuccessUserInteraction,
        private readonly bool $hideReadOnlyNodesOnLanding,
        private readonly array $customOptions = [],
    ) {
        if ('' === $uid) {
            throw new StepException('parameter uid must be a non-empty string!');
        }

        static::guardStepPeriodClass($period);

        $this->uid = $uid;
        $this->period = $period;
    }

    /**
     * @throws StepException
     */
    public static function fromUidAndDependencies(
        string $uid,
        StepPeriodInterface $period,
        bool $allowsStartIfPreviousStepIsFulfilled,
        RoleCollection $rolesLimitingUserToOneSubmitPerRole,
        LocalizedInfoWithRequiredLabel $info,
        StepSubmitSuccessUserInteraction $userInteraction,
        bool $hideReadOnlyNodesOnLanding,
        array $customOptions = [],
    ): static {
        return new static(
            $uid,
            $period,
            $allowsStartIfPreviousStepIsFulfilled,
            $rolesLimitingUserToOneSubmitPerRole,
            $info,
            $userInteraction,
            $hideReadOnlyNodesOnLanding,
            $customOptions
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public static function fromUidAndMinimalDependencies(
        string $uid,
        StepPeriodInterface $period,
        LocalizedInfoWithRequiredLabel $info,
        bool $allowsStartIfPreviousStepIsFulfilled,
    ): static {
        return new static(
            $uid,
            $period,
            $allowsStartIfPreviousStepIsFulfilled,
            RoleCollection::createEmpty(),
            $info,
            StepSubmitSuccessUserInteraction::createEmptyWithDefaults(),
            false,
            []
        );
    }

    public function getPeriod(): StepPeriodInterface
    {
        return $this->period;
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     * @throws StepException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        // make sure the correct implementation is called
        if (!($data[self::KEY_TYPE] === static::getType())) {
            throw new StepException('expect type '.static::getType().', got '.$data[self::KEY_TYPE]);
        }

        $hideReadOnlyOnLanding = false;
        if (isset($data[self::KEY_HIDE_READ_ONLY_NODES_ON_LANDING])) {
            $hideReadOnlyOnLanding = $data[self::KEY_HIDE_READ_ONLY_NODES_ON_LANDING];
        }

        $period = static::getPeriodFromJsonDecodedArray($data[self::KEY_PERIOD]);

        return static::fromUidAndDependencies(
            $data[self::KEY_UID],
            $period,
            $data[self::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED],
            RoleCollection::fromJsonDecodedArray($data[self::KEY_ROLES_WITH_ONE_SUBMIT]),
            LocalizedInfoWithRequiredLabel::fromJsonDecodedArray($data[self::KEY_INFO]),
            StepSubmitSuccessUserInteraction::fromJsonDecodedArray($data[self::KEY_SUBMIT_SUCCESS_USER_INTERACTION]),
            $hideReadOnlyOnLanding,
            self::getCustomOptionsFromJsonDecodedArray($data)
        );
    }

    private static function getCustomOptionsFromJsonDecodedArray(array $data): array
    {
        return $data[self::KEY_CUSTOM_OPTIONS] ?? [];
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getInfo(): LocalizedInfoWithRequiredLabel
    {
        return $this->info;
    }

    public function getSubmitSuccessUserInteraction(): StepSubmitSuccessUserInteraction
    {
        return $this->submitSuccessUserInteraction;
    }

    public function getRolesLimitingUserToOneSubmitPerRole(): RoleCollection
    {
        return $this->rolesLimitingUserToOneSubmitPerRole;
    }

    public function shouldHideReadOnlyNodesOnLanding(): bool
    {
        return $this->hideReadOnlyNodesOnLanding;
    }

    public function allowsToStartPrematurelyIfPreviousStepIsFulFilled(): bool
    {
        return $this->allowsStartIfPreviousStepIsFulfilled;
    }

    public function getCustomOptions(): array
    {
        return $this->customOptions;
    }

    public function skipAdminRolesForFulFilmentCheck(): bool
    {
        $customOptions = $this->getCustomOptions();

        if (!isset($customOptions[self::KEY_CUSTOM_OPTION_SKIP_ADMIN_ROLES_FOR_FULFILMENT_CHECK])) {
            return false;
        }

        if (!is_bool($customOptions[self::KEY_CUSTOM_OPTION_SKIP_ADMIN_ROLES_FOR_FULFILMENT_CHECK])) {
            return false;
        }

        return $customOptions[self::KEY_CUSTOM_OPTION_SKIP_ADMIN_ROLES_FOR_FULFILMENT_CHECK];
    }

    /**
     * @throws StepException
     */
    public function withPreviousStepFulFilledStartOption(): static
    {
        return new static(
            $this->getUid(),
            $this->getPeriod(),
            true,
            $this->getRolesLimitingUserToOneSubmitPerRole(),
            $this->getInfo(),
            $this->getSubmitSuccessUserInteraction(),
            $this->shouldHideReadOnlyNodesOnLanding(),
            $this->getCustomOptions()
        );
    }

    /**
     * @throws StepException
     */
    public function withoutPreviousStepFulFilledStartOption(): static
    {
        return new static(
            $this->getUid(),
            $this->getPeriod(),
            false,
            $this->getRolesLimitingUserToOneSubmitPerRole(),
            $this->getInfo(),
            $this->getSubmitSuccessUserInteraction(),
            $this->shouldHideReadOnlyNodesOnLanding(),
            $this->getCustomOptions()
        );
    }

    public function limitsEachUniqueUserToOnlyOneSubmitUnderRole(Role $role): bool
    {
        return $this->getRolesLimitingUserToOneSubmitPerRole()->hasRole($role);
    }

    /**
     * checks if the given role is configured to be notified that this step has started.
     *
     * @throws \Exception
     */
    public function shouldNotifyRoleOfStart(Role $role): bool
    {
        return $this->getSubmitSuccessUserInteraction()
            ->getNotificationConfig()
            ->getRoleConfigsToNotifyOfStart()
            ->hasOneForRole($role);
    }

    public function shouldNotifyAtLeastOneRoleOfStart(): bool
    {
        return !$this->getSubmitSuccessUserInteraction()
            ->getNotificationConfig()
            ->getRoleConfigsToNotifyOfStart()
            ->isEmpty();
    }

    public function getRolesThatShouldBeNotifiedOfStart(): RoleCollection
    {
        return $this->getSubmitSuccessUserInteraction()
            ->getNotificationConfig()
            ->getRoleConfigsToNotifyOfStart()
            ->getRoles();
    }

    /**
     * checks if the given role is configured to be reminded that this step has already started some time ago.
     *
     * note: this is to be used with a (DB) check if user has already done his/her part (i.e. submitted his/her
     * questions) for this step.
     *
     * @throws \Exception
     */
    public function shouldRemindRoleOfOngoing(Role $role): bool
    {
        return $this->getSubmitSuccessUserInteraction()
            ->getNotificationConfig()
            ->getRoleConfigsToRemindOfOngoing()
            ->hasOneForRole($role);
    }

    public function shouldRemindAtLeastOneRoleOfOngoing(): bool
    {
        return !$this->getSubmitSuccessUserInteraction()
            ->getNotificationConfig()
            ->getRoleConfigsToRemindOfOngoing()
            ->isEmpty();
    }

    public function getRolesThatShouldBeRemindedOfOngoing(): RoleCollection
    {
        return $this->getSubmitSuccessUserInteraction()
            ->getNotificationConfig()
            ->getRoleConfigsToRemindOfOngoing()
            ->getRoles();
    }

    public function shouldRedirectAwayOnSubmitSuccess(): bool
    {
        return $this->getSubmitSuccessUserInteraction()->shouldRedirectAwayOnSubmitSuccess();
    }

    public function getLabel(string $locale): string
    {
        return $this->getInfo()->getLabel($locale);
    }

    public function hasDescription(string $locale): bool
    {
        return $this->getInfo()->hasDescription($locale);
    }

    public function getDescription(string $locale): string
    {
        return $this->getInfo()->getDescription($locale);
    }

    public function hasRoleDescription(Role $role, string $locale): bool
    {
        return $this->getInfo()->hasRoleDescription($role, $locale);
    }

    public function getRoleDescription(Role $role, string $locale): string
    {
        return $this->getInfo()->getRoleDescription($role, $locale);
    }

    public function hasGeneralSubmitSuccessMessage(string $locale): bool
    {
        return $this->getSubmitSuccessUserInteraction()->getSubmitSuccessMessage()->hasForLocaleOrFallback($locale);
    }

    public function getGeneralSubmitSuccessMessage(string $locale): string
    {
        return $this->getSubmitSuccessUserInteraction()->getSubmitSuccessMessage()->getForLocaleOrFallback($locale);
    }

    public function hasRoleSubmitSuccessMessage(Role $role, string $locale): bool
    {
        if (!$this->getSubmitSuccessUserInteraction()->getRoleSubmitSuccessMessages()->hasOneForRole($role)) {
            return false;
        }

        return $this->getSubmitSuccessUserInteraction()->getRoleSubmitSuccessMessages()
            ->getOneForRole($role)
            ->getLocalizedString()
            ->hasForLocaleOrFallback($locale);
    }

    public function getRoleSubmitSuccessMessage(Role $role, string $locale): string
    {
        return $this->getSubmitSuccessUserInteraction()->getRoleSubmitSuccessMessages()
            ->getOneForRole($role)
            ->getLocalizedString()
            ->getForLocaleOrFallback($locale);
    }

    /**
     * @return array{type: string, uid: string, period: StepPeriodInterface, allowsStartIfPreviousStepIsFulfilled: bool, rolesWithOneSubmit: RoleCollection, info: LocalizedInfoWithRequiredLabel, submitSuccessUserInteraction: StepSubmitSuccessUserInteraction, hideReadOnlyNodesOnLanding: bool, custom_options: mixed[]}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_TYPE => static::getType(),
            self::KEY_UID => $this->getUid(),
            self::KEY_PERIOD => $this->getPeriod(),
            self::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => $this->allowsToStartPrematurelyIfPreviousStepIsFulFilled(),
            self::KEY_ROLES_WITH_ONE_SUBMIT => $this->getRolesLimitingUserToOneSubmitPerRole(),
            self::KEY_INFO => $this->getInfo(),
            self::KEY_SUBMIT_SUCCESS_USER_INTERACTION => $this->getSubmitSuccessUserInteraction(),
            self::KEY_HIDE_READ_ONLY_NODES_ON_LANDING => $this->shouldHideReadOnlyNodesOnLanding(),
            self::KEY_CUSTOM_OPTIONS => $this->getCustomOptions(),
        ];
    }
}
