<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element;

use KUL\FormBundle\Domain\Template\Utility\AbstractLocalizedInfo;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;

/**
 * Class FormInfo.
 *
 * contains localized general information texts for the template consisting of the required label,
 * the optional descriptions and the optional role related descriptions used to help & inform
 * end-users (clients) working on the template-form about what they have can/should/could/... do & know
 */
final class FormInfo extends LocalizedInfoWithRequiredLabel
{
    /**
     * this method is needed for moving the info from the root node to the form info
     * it will only work if there is a label for at least one locale.
     */
    public static function fromAbstractLocalizedInfo(AbstractLocalizedInfo $info): self
    {
        return new self(
            LocalizedRequiredString::fromJsonDecodedArray($info->getLocalizedLabel()->jsonSerialize()),
            $info->getLocalizedDescription(),
            $info->getRoleLocalizedDescriptions(),
        );
    }
}
