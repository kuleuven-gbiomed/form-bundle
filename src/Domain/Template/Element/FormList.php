<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ChildrenAwareInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Factory\NodeFactory;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\AbstractReadOnlyViewNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\ReadOnlyViewNodeCollection;
use KUL\FormBundle\Domain\Template\Version\PublishedVersion;
use KUL\FormBundle\Domain\Template\Version\Validator\WorkingVersionFormListValidator;
use KUL\FormBundle\Domain\Utility\JsonDecoder;

/**
 * contains the list of deeply nested categories and questions that make up a form for a template.
 *
 * NOTE: the nested structure in the single root category node, can be performance heavy for searching, filtering, etc.
 * sometimes, the nested structure is not needed (e.g. when looking for a specific node without interest of its position
 * in the tree structure). doing a @see CategoryNode::getDescendantByUid() to lookup that node in the tree can therefore be
 * heavy due to the recursive lookup in the tree: every branch (node) on every level from root node down needs to be
 * checked for the requested descendants all the way down to the deepest descendants of each branch until the requested.
 * deriving a flattened (single dimension array collection) list of specified sets of nodes from the root
 * category node can also be heavy if it needs to be done repeatedly. but deriving the lists that are frequently
 * used/needed in most operations, only once on first call, and locally caching them on that first call,is a reasonably
 * performance safe operation and a trade-off that increases overall performance a lot for the bulk of usages.
 */
final class FormList implements \JsonSerializable, Deserializable, FormListInterface
{
    /** @var string */
    public const KEY_ROOT = 'root';
    /** @var string identifier of root category node, unique to each formList (name 'form' is not well chosen, but needed for older (twig) code & Db data */
    public const ROOT_UID = 'form';

    private readonly CategoryNode $rootNode;

    /**
     * locally cached 'flattened' (single dimension array collection) representations of all nodes in the tree.
     *
     * @var NodeCollection<array-key, NodeInterface>|null
     */
    private ?NodeCollection $flattenedNodes = null;
    /** @var InputNodeCollection<array-key,InputNode>|null */
    private ?InputNodeCollection $flattenedInputNodes = null;
    /** @var InputNodeCollection<array-key,InputNode>|null */
    private ?InputNodeCollection $flattenedReachableInputNodes = null;
    /** @var InputNodeCollection<array-key,InputNode>|null */
    private ?InputNodeCollection $flattenedUnReachableInputNodes = null;
    /** @var InputNodeCollection<array-key,InputNode>|null */
    private ?InputNodeCollection $flattenedParentalInputNodes = null;
    /** @var InputNodeCollection<array-key,InputNode>|null */
    private ?InputNodeCollection $flattenedReachableParentalInputNodes = null;
    /** @var InputNodeCollection<array-key,InputNode>|null */
    private ?InputNodeCollection $flattenedUnReachableParentalInputNodes = null;
    /** @var ReadOnlyViewNodeCollection<array-key,AbstractReadOnlyViewNode>|null */
    private ?ReadOnlyViewNodeCollection $flattenedReadOnlyViewNodes = null;
    /** @var ReadOnlyViewNodeCollection<array-key,AbstractReadOnlyViewNode>|null */
    private ?ReadOnlyViewNodeCollection $flattenedReachableReadOnlyViewNodes = null;

    private function __construct(CategoryNode $rootNode)
    {
        if (self::ROOT_UID !== $rootNode->getUid()) {
            throw new \InvalidArgumentException('the uid of the rootNode must be ['.self::ROOT_UID.']. Got ['.$rootNode->getUid().']');
        }

        $this->rootNode = $rootNode;
    }

    public function isEmpty(): bool
    {
        return !$this->rootNode->hasChildren();
    }

    /**
     * creates a formList with only the correct named rootNode without any children.
     *
     * NOTE: a formList without children will never pass template publish validation (@see WorkingVersionFormListValidator )
     */
    public static function createWithEmptyRootNode(): self
    {
        return new self(NodeFactory::createEmptyCategoryNode(self::ROOT_UID));
    }

    public static function fromRootNode(CategoryNode $rootNode): self
    {
        return new self($rootNode);
    }

    /**
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        if (!isset($data[self::KEY_ROOT])) {
            throw new \InvalidArgumentException('missing key ['.self::KEY_ROOT.'] in json decoded array data');
        }

        return new self(CategoryNode::fromJsonDecodedArray($data[self::KEY_ROOT]));
    }

    public function getRootNode(): CategoryNode
    {
        return $this->rootNode;
    }

    /**
     * get flattened representation (single dimension array collection) of all Nodes in the tree, regardless of type
     * of node and regardless of the hidden (reachable) status of each node. all nodes in this collection remain aware
     * of their parent and/or their children/descendants, if any.
     *
     * NOTE: any children of an InputNode will be added as well, as well as remain accessible as children within
     * their parent InputNode (@see ParentalInputNode )
     *
     * @return NodeCollection<array-key, NodeInterface>
     */
    public function getFlattenedNodes(): NodeCollection
    {
        //  on first call/need, derive from rootNode once and locally cache it
        if (!$this->flattenedNodes instanceof NodeCollection) {
            $this->flattenedNodes = $this->getRootNode()->getFlattenedWithAllDescendants();
        }

        return $this->flattenedNodes;
    }

    /**
     * get flattened representation (single dimension array collection) of all inputNodes in the tree,
     * including ones that are children of another inputNode (like @see OptionInputNode which is not a question on itself),
     * starting from rootNode down and regardless of hidden (reachable) status.
     * all nodes in this collection remain aware of their parent.
     *
     * WARNING: do not use this to get 'real' questions. for that use methods named with @see ParentalInputNode
     *
     * @return InputNodeCollection<array-key, InputNode>
     */
    public function getFlattenedInputNodes(): InputNodeCollection
    {
        if (!$this->flattenedInputNodes instanceof InputNodeCollection) {
            $this->flattenedInputNodes = $this->getFlattenedNodes()->getInputNodes();
        }

        return $this->flattenedInputNodes;
    }

    /**
     * @return ReadOnlyViewNodeCollection<array-key, AbstractReadOnlyViewNode>
     */
    public function getFlattenedReadOnlyViewNodes(): ReadOnlyViewNodeCollection
    {
        if (!$this->flattenedReadOnlyViewNodes instanceof ReadOnlyViewNodeCollection) {
            $this->flattenedReadOnlyViewNodes = $this->getFlattenedNodes()->getReadOnlyViewNodes();
        }

        return $this->flattenedReadOnlyViewNodes;
    }

    /**
     * get flattened representation (single dimension array collection) of all inputNodes in the tree,
     * including ones that are children of another inputNode (like @see OptionInputNode which is not a question on itself),
     * and that are reachable (NOT directly or indirectly hidden in a parent/ancestor), starting from rootNode down.
     * all nodes in this collection remain aware of their parent.
     *
     * WARNING: do not use this to get 'real' questions. for that use methods named with @see ParentalInputNode
     *
     * @return InputNodeCollection<array-key, InputNode>
     */
    public function getFlattenedReachableInputNodes(): InputNodeCollection
    {
        if (!$this->flattenedReachableInputNodes instanceof InputNodeCollection) {
            $collection = $this->getFlattenedInputNodes()->getReachableInTree();
            $this->flattenedReachableInputNodes = $collection;
        }

        return $this->flattenedReachableInputNodes;
    }

    /**
     * @return ReadOnlyViewNodeCollection<array-key, AbstractReadOnlyViewNode>
     */
    public function getFlattenedReachableReadOnlyViewNodes(): ReadOnlyViewNodeCollection
    {
        if (!$this->flattenedReachableReadOnlyViewNodes instanceof ReadOnlyViewNodeCollection) {
            $this->flattenedReachableReadOnlyViewNodes = $this->getFlattenedReadOnlyViewNodes()->getReachableInTree();
        }

        return $this->flattenedReachableReadOnlyViewNodes;
    }

    /**
     * get flattened representation (single dimension array collection) of all inputNodes in the tree that are not
     * including ones that are children of another inputNode (like @see OptionInputNode which is not a question on itself),
     * and that are NOT reachable (directly and/or indirectly hidden in a parent/ancestor), starting from rootNode down.
     * all nodes in this collection remain aware of their parent.
     *
     * WARNING: do not use this to get 'real' questions. for that use methods named with @see ParentalInputNode
     *
     * @return InputNodeCollection<array-key, InputNode>
     */
    public function getFlattenedUnReachableInputNodes(): InputNodeCollection
    {
        if (!$this->flattenedUnReachableInputNodes instanceof InputNodeCollection) {
            $collection = $this->getFlattenedInputNodes()->getUnReachableInTree();
            $this->flattenedUnReachableInputNodes = $collection;
        }

        return $this->flattenedUnReachableInputNodes;
    }

    /**
     * get flattened representation (single dimension array collection) of all inputNodes in the tree that are not
     * children of another inputNode (@see ParentalInputNode ), starting from rootNode down and
     * regardless of hidden (reachable) status. all nodes in this collection remain aware of their parent.
     *
     * NOTE: any children of an InputNode will not be added, but will be left and accessible as children within
     * their parent InputNode.
     *
     * @return InputNodeCollection<array-key, InputNode>
     */
    public function getFlattenedParentalInputNodes(): InputNodeCollection
    {
        //  on first call/need, filter from flattened nodes once and locally cache it
        if (!$this->flattenedParentalInputNodes instanceof InputNodeCollection) {
            $this->flattenedParentalInputNodes = $this->getFlattenedInputNodes()->getParentalInputNodes();
        }

        return $this->flattenedParentalInputNodes;
    }

    /**
     * get flattened representation (single dimension array collection) of all inputNodes in the tree that are not
     * children of another inputNode (@see ParentalInputNode ) and that are reachable (NOT directly
     * or indirectly hidden in a parent/ancestor), starting from rootNode down. all nodes in this collection remain
     * aware of their parent.
     *
     * NOTE: any children of an InputNode will not be added, but will be left and accessible as children within
     * their parent InputNode.
     *
     * @return InputNodeCollection<array-key, InputNode>
     */
    public function getFlattenedReachableParentalInputNodes(): InputNodeCollection
    {
        //  on first call/need, filter from flattened parental input nodes once and locally cache it
        if (!$this->flattenedReachableParentalInputNodes instanceof InputNodeCollection) {
            $this->flattenedReachableParentalInputNodes = $this->getFlattenedReachableInputNodes()->getParentalInputNodes();
        }

        return $this->flattenedReachableParentalInputNodes;
    }

    /**
     * get flattened representation (single dimension array collection) of all inputNodes in the tree that are not
     * children of another inputNode (@see ParentalInputNode ) and that are NOT reachable (
     * directly and/or indirectly hidden in a parent/ancestor), starting from rootNode down. all nodes in this collection
     * remain aware of their parent.
     *
     * NOTE: any children of an InputNode will not be added, but will be left and accessible as children within
     * their parent InputNode.
     *
     * @return InputNodeCollection<array-key, InputNode>
     */
    public function getFlattenedUnReachableParentalInputNodes(): InputNodeCollection
    {
        //  on first call/need, filter from flattened parental input nodes once and locally cache it
        if (!$this->flattenedUnReachableParentalInputNodes instanceof InputNodeCollection) {
            $this->flattenedUnReachableParentalInputNodes = $this->getFlattenedUnReachableInputNodes()->getParentalInputNodes();
        }

        return $this->flattenedUnReachableParentalInputNodes;
    }

    /**
     * use the locally cached flattened collection of nodes to quickly check if a node for given uid exist.
     */
    public function hasNodeByUid(string $uid): bool
    {
        return $this->getFlattenedNodes()->hasOneByNodeUid($uid);
    }

    /**
     * use the locally cached flattened collection of nodes to quickly check if node exist.
     */
    public function hasNode(NodeInterface $node): bool
    {
        return $this->getFlattenedNodes()->hasNode($node);
    }

    /**
     * use the locally cached flattened collection of nodes to quickly get the node for given uid.
     */
    public function getNodeByUid(string $uid): NodeInterface
    {
        return $this->getFlattenedNodes()->getOneByNodeUid($uid);
    }

    /**
     * validates if the form structure based on the tree structure of categories and questions in this @see FormList
     * will be able to be displayed/ shown in a series of (panel) views with 'next' and 'previous' buttons as a way to
     * reduce the space taken up by the form in the viewport, instead of displaying the form as one single page.
     *
     * because this will use (twitter Bootstrap) panels and/or tabs on client side, assuming that each tab or panel
     * will contain a set of questions grouped in a category, the condition for this to work is that the nodes in the
     * first level directly under the @see FormList::getRootNode() - i.e. the direct children of the root - can only be
     * categories (if any) , but no questions! if there are no children at all under the root node, then it is considered
     * showable as page breaks since no children means it can be shown either way.
     *
     * NOTE: this should never be a breaking issue. display in a single page is always possible.
     * NOTE: in a @see PublishedVersion::getFormList() there will always be at least one child under rootNode
     */
    public function canShowCategoriesAsPageBreaks(): bool
    {
        // ok if root node has no children or only category nodes. not ok if root has (questions) as direct children
        return !$this->getRootNode()->getChildren()->getInputNodes()->isEmpty();
    }

    /**
     * makes a new formList from this instance with given node added as new child node to given parent in the tree.
     *
     * there is a copy of this method below that requires a CategoryNode instead of a ChildrenAwareInterface
     * to keep psalm happy
     *
     * @param NodeInterface                                                                   $node        new node that needs to be added
     * @param ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>> $addToParent
     *
     * @throws \InvalidArgumentException
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public function withNewNodeAddedToParent(NodeInterface $node, ChildrenAwareInterface $addToParent): self
    {
        $baseError = 'cannot add new node ['.$node->getUid().']';

        if ($this->hasNode($node)) {
            throw new \InvalidArgumentException($baseError.': it already exists.');
        }

        if (!$this->hasNode($addToParent)) {
            throw new \InvalidArgumentException($baseError.' to parent ['.$addToParent->getUid().']: parent does not exist.');
        }

        // can not use PHP's clone method for deep nested objects. serialize & deserialize is recommended option
        $rootNodeClone = CategoryNode::fromJsonDecodedArray(JsonDecoder::toAssocArray(json_encode($this->getRootNode(), \JSON_THROW_ON_ERROR)));

        $parentUid = $addToParent->getUid();
        /** @var ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>> $parent */
        $parent = $parentUid === $rootNodeClone->getUid() ? $rootNodeClone : $rootNodeClone->getDescendantByUid($parentUid);
        $parent->attachChild($node);

        return self::fromRootNode($rootNodeClone);
    }

    /**
     * This is a copy of the above method that requires a CategoryNode instead of a ChildrenAwareInterface
     * This is only used because psalm complains if a CategoryNode
     * is given for the second parameter in the method above.
     *
     * @param NodeInterface $node        new node that needs to be added
     * @param CategoryNode  $addToParent parent node in the root's tree to which new node needs to be added
     *
     * @throws \InvalidArgumentException
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public function withNewNodeAddedToParentCategory(NodeInterface $node, CategoryNode $addToParent): self
    {
        $baseError = 'cannot add new node ['.$node->getUid().']';

        if ($this->hasNode($node)) {
            throw new \InvalidArgumentException($baseError.': it already exists.');
        }

        if (!$this->hasNode($addToParent)) {
            throw new \InvalidArgumentException($baseError.' to parent ['.$addToParent->getUid().']: parent does not exist.');
        }

        // can not use PHP's clone method for deep nested objects. serialize & deserialize is recommended option
        $rootNodeClone = CategoryNode::fromJsonDecodedArray(JsonDecoder::toAssocArray(json_encode($this->getRootNode(), \JSON_THROW_ON_ERROR)));

        $parentUid = $addToParent->getUid();
        $parent = $parentUid === $rootNodeClone->getUid() ? $rootNodeClone : $rootNodeClone->getDescendantByUid($parentUid);
        if ($parent instanceof ChildrenAwareInterface) {
            $parent->attachChild($node);
        } else {
            throw new \InvalidArgumentException('parent is not a children aware interface');
        }

        return self::fromRootNode($rootNodeClone);
    }

    /**
     * @param NodeInterface                                                                   $node        new node that needs to be added
     * @param ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>> $addToParent parent node in the root's tree to which new node needs to be added
     *
     * @throws \InvalidArgumentException
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public function withNewNodeAddedToParentAsFirstChild(NodeInterface $node, ChildrenAwareInterface $addToParent): self
    {
        $baseError = 'cannot add new node ['.$node->getUid().']';

        if ($this->hasNode($node)) {
            throw new \InvalidArgumentException($baseError.': it already exists.');
        }

        if (!$this->hasNode($addToParent)) {
            throw new \InvalidArgumentException($baseError.' to parent ['.$addToParent->getUid().']: parent does not exist.');
        }

        // can not use PHP's clone method for deep nested objects. serialize & deserialize is recommended option
        $rootNodeClone = CategoryNode::fromJsonDecodedArray(JsonDecoder::toAssocArray(json_encode($this->getRootNode(), \JSON_THROW_ON_ERROR)));

        $parentUid = $addToParent->getUid();
        /** @var ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>> $parent */
        $parent = $parentUid === $rootNodeClone->getUid() ? $rootNodeClone : $rootNodeClone->getDescendantByUid($parentUid);
        $parent->attachAsFirstChild($node);

        return self::fromRootNode($rootNodeClone);
    }

    /**
     * makes a new formList from this instance with given node added as new child node to given parent in the tree
     * and in the given parent's children on the position after given child.
     *
     * @param NodeInterface                                                                   $node          new node that needs to be added
     * @param ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>> $addToParent   parent node in the root's tree to which new node needs to be added
     * @param NodeInterface                                                                   $addAfterChild child from parent after which new node is to be positioned
     *
     * @throws \InvalidArgumentException
     * @throws LocalizedStringException
     * @throws InputInvalidArgumentException
     */
    public function withNewNodeAddedToParentAfterChild(
        NodeInterface $node,
        ChildrenAwareInterface $addToParent,
        NodeInterface $addAfterChild,
    ): self {
        $baseError = 'cannot add new node ['.$node->getUid().']';
        if ($this->hasNode($node)) {
            throw new \InvalidArgumentException($baseError.': it already exists.');
        }

        if (!$this->hasNode($addToParent)) {
            throw new \InvalidArgumentException($baseError.' to parent ['.$addToParent->getUid().']: parent does not exist.');
        }

        if (!$this->hasNode($addAfterChild)) {
            throw new \InvalidArgumentException($baseError.' after child ['.$addAfterChild->getUid().']: child does not exist');
        }

        // check if child to insert new node after is a child of given parent (checking from the child instead
        // of checking if parent has this child in case the given parent is altered before passed in this method
        $childNode = $this->getNodeByUid($addAfterChild->getUid());
        if (!($childNode->hasParent() && $childNode->getParent()->getUid() === $addToParent->getUid())) {
            throw new \InvalidArgumentException($baseError.' after child ['.$addAfterChild->getUid().']: child is no child of parent ['.$addToParent->getUid().'] to add the new node to.');
        }

        // can not use PHP's clone method for deep nested objects. serialize & deserialize is recommended option
        $rootNodeClone = CategoryNode::fromJsonDecodedArray(JsonDecoder::toAssocArray(json_encode($this->getRootNode(), \JSON_THROW_ON_ERROR)));

        $parentUid = $addToParent->getUid();
        /** @var ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>> $parent */
        $parent = $parentUid === $rootNodeClone->getUid() ? $rootNodeClone : $rootNodeClone->getDescendantByUid($parentUid);
        $parent->attachChildAfter($node, $addAfterChild);

        return self::fromRootNode($rootNodeClone);
    }

    /**
     * makes a new formList from this instance with given node moved in the tree to new given position in parent.
     *
     * NOTE: removes the node first and then adds it as a new node
     *
     * @param ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>> $moveToParent
     *
     * @throws \InvalidArgumentException
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public function withNodeMovedToParent(NodeInterface $node, ChildrenAwareInterface $moveToParent): self
    {
        return $this->withNodeRemoved($node)->withNewNodeAddedToParent($node, $moveToParent);
    }

    /**
     * makes a new formList from this instance with given node moved in the tree to top of the parent.
     *
     * NOTE: removes the node first and then adds it as a new node
     *
     * @param ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>> $moveToParent
     *
     * @throws \InvalidArgumentException
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public function withNodeMovedToTopOfParent(NodeInterface $node, ChildrenAwareInterface $moveToParent): self
    {
        return $this->withNodeRemoved($node)->withNewNodeAddedToParentAsFirstChild($node, $moveToParent);
    }

    /**
     * makes a new formList from this instance with given node moved in the tree to new given position in parent and
     * in the given parent's children on the position after given child.
     *
     * NOTE: removes the node first and then adds it as a new node
     *
     * @param ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>> $moveToParent
     *
     * @throws \InvalidArgumentException
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public function withNodeMovedToParentAfterChild(
        NodeInterface $node,
        ChildrenAwareInterface $moveToParent,
        NodeInterface $moveAfterChild,
    ): self {
        return $this->withNodeRemoved($node)->withNewNodeAddedToParentAfterChild($node, $moveToParent, $moveAfterChild);
    }

    /**
     * makes a new formList with given node removed from the tree.
     *
     * NOTE: domain decision for admin side form construction: a root node is build once upon first construction of form(List)
     *       hence removing the root node is NOT allowed via this method and in fact never allowed at all.
     *       (although all its children can be removed to make the rootNode empty again and by so the formList unusable)
     *
     * @throws \InvalidArgumentException
     * @throws LocalizedStringException
     * @throws InputInvalidArgumentException
     */
    public function withNodeRemoved(NodeInterface $node): self
    {
        if (!$this->hasNode($node)) {
            throw new \InvalidArgumentException('cannot remove node ['.$node->getUid().']: it does not exist.');
        }

        if ($this->getRootNode()->getUid() === $node->getUid()) {
            throw new \InvalidArgumentException('cannot remove node ['.$node->getUid().']: it is the root node.');
        }

        // can not use PHP's clone method for deep nested objects. serialize & deserialize is recommended option
        $rootNodeClone = CategoryNode::fromJsonDecodedArray(JsonDecoder::toAssocArray(json_encode($this->getRootNode(), \JSON_THROW_ON_ERROR)));

        // node is not the root node, so it must have a parent that can have children
        /** @var ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>> $parent */
        $parent = $rootNodeClone->getDescendantByUid($node->getUid())->getParent();
        $parent->removeChild($node);

        return self::fromRootNode($rootNodeClone);
    }

    /**
     * replace (i.e. update) existing node with given node in the formList tree.
     *
     * NOTE: technically, this could be a node of different type than the existing, but - although initially intended
     * for just that based on older code - that is not such a good idea because it implies you re-used the unique uid
     * of the existing node to build a new one of different type. but there might be cases where the node is replaced
     * by one of a type with a class that extends from the class from the existing node
     *
     * use @see withNodeUpdated() instead
     *
     * @throws \Exception
     */
    public function withNodeReplaced(NodeInterface $node): self
    {
        if (!$this->hasNode($node)) {
            throw new \InvalidArgumentException('cannot replace node ['.$node->getUid().']: it does not exist.');
        }

        // if node to replace/update is the root node itself, then use it as the new rootNode
        if ($node instanceof CategoryNode && $node->getUid() === $this->getRootNode()->getUid()) {
            return self::fromRootNode($node);
        }

        // can not use PHP's clone method for deep nested objects. serialize & deserialize is recommended option
        $rootNodeClone = CategoryNode::fromJsonDecodedArray(JsonDecoder::toAssocArray(json_encode($this->getRootNode(), \JSON_THROW_ON_ERROR)));

        // if node is not the rootNode, then is must have a parent that is can have children
        /** @var ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>> $parent */
        $parent = $rootNodeClone->getDescendantByUid($node->getUid())->getParent();
        $parent->replaceChild($node);

        return self::fromRootNode($parent->getTreeRootNode());
    }

    /**
     * update given node in the formList tree. same as @see withNodeReplaced, but the type is asserted here.
     *
     * @throws \Exception
     */
    public function withNodeUpdated(NodeInterface $node): self
    {
        if (!$this->hasNode($node)) {
            throw new \InvalidArgumentException('cannot update node ['.$node->getUid().']: it does not exist.');
        }

        $existingNode = $this->getNodeByUid($node->getUid());
        if ($node::getType() === $existingNode::getType()) {
            return self::withNodeReplaced($node);
        }

        throw new \InvalidArgumentException('cannot update node ['.$node->getUid().']: its type [ '.$node::getType().'] does not match the type ['.$existingNode::getType().'] of the node you want to update.');
    }

    /**
     * @param string[][] $unlockerIds
     *
     * @throws \Exception
     */
    public function withMultiUnlocker(ParentalInputNode $lockedNode, array $unlockerIds): self
    {
        $result = clone ($this);
        $lockedNode->setMultiUnlockingQuestions($unlockerIds);

        $result = $result->withNodeUpdated($lockedNode);

        foreach ($unlockerIds as $choiceNodeId => $optionIds) {
            $unlockingChoiceNode = $this->getNodeByUid($choiceNodeId);
            if ($unlockingChoiceNode instanceof ChoiceInputNode) {
                $unlockingOptions = $unlockingChoiceNode->getAvailableOptionsByUids($optionIds);
                foreach ($unlockingOptions as $unlockingOption) {
                    $unlockingOption->multiUnlocksQuestions($lockedNode);
                    $result = $result->withNodeUpdated($unlockingOption);
                }
            }
        }

        return $result;
    }

    /**
     * @return array{root: CategoryNode}
     */
    public function jsonSerialize(): array
    {
        return [self::KEY_ROOT => $this->getRootNode()];
    }
}
