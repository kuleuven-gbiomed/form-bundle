<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Permission;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\RolePermissionCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\StepPermissionCollection;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;

/**
 * Class StepPermission.
 *
 * configures a set of write and/or read access rights for roles coupled to a step and to be used in a @see FlowPermission
 *
 * Note: in a template, nodes (questions & categories) and workflow are build, serialized & de-serialized separately
 * from each other. de-serializing a step in this object would require full knowledge of the workflow. which is not
 * needed, creates extra dependency and affects performance. so strictly speaking, this object does not even know what a
 * step is. for those reasons this object does/can not use step objects, instead it uses uids referring to @see StepInterface .
 *
 * @see     WorkFlow
 * @see     StepInterface
 * @see     NodeInterface
 * @see     StepPermissionCollection
 */
final class StepPermission implements \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_STEP_UID = 'stepUid';
    /** @var string */
    public const KEY_ROLE_PERMISSIONS = 'rolePermissions';

    private readonly string $stepUid;

    private readonly RolePermissionCollection $rolePermissions;

    /**
     * StepPermission constructor.
     *
     * NOTE: the rolePermissions can not contain multiple permission definitions for the same role.
     *       no role permissions at all for a step is allowed; it can prove useful if an admin wants to (temporary)
     *       deny any role any access to a question in a step (although using @see NodeInterface::isHidden() might
     *       often achieve the same result)
     *
     * @throws \InvalidArgumentException if stepUid is invalid or if more than one rolePermission found for same role
     */
    private function __construct(string $stepUid, RolePermissionCollection $rolePermissions)
    {
        if ('' === $stepUid) {
            throw new \InvalidArgumentException('parameter stepUid must be a non empty string');
        }

        if ($rolePermissions->hasMultipleForAtLeastOneRole()) {
            throw new \InvalidArgumentException('ambigue role permissions: multiple permissions exist for a same role');
        }

        $this->stepUid = $stepUid;
        $this->rolePermissions = $rolePermissions;
    }

    public static function fromStepUidRolePermissions(string $stepUid, RolePermissionCollection $rolePermissions): self
    {
        return new self($stepUid, $rolePermissions);
    }

    /**
     * use the given associative array data (usually JSON decoded) to build the implementing object.
     * all required indices in the array data to build the object are assumed present.
     *
     * @throws \Exception
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        return new self(
            $data[self::KEY_STEP_UID],
            RolePermissionCollection::fromJsonDecodedArray($data[self::KEY_ROLE_PERMISSIONS])
        );
    }

    public function getStepUid(): string
    {
        return $this->stepUid;
    }

    public function getRolePermissions(): RolePermissionCollection
    {
        return $this->rolePermissions;
    }

    public function hasRolePermission(string $role): bool
    {
        return $this->getRolePermissions()->hasOneForRole($role);
    }

    public function getRolePermission(string $role): RolePermission
    {
        return $this->getRolePermissions()->getOneForRole($role);
    }

    public function withRolePermission(RolePermission $rolePermission): self
    {
        // remove any existing permissions for role
        $rolePermissions = $this->withPermissionRevokedForRole($rolePermission->getRole())->getRolePermissions();
        // add new permissions for role
        $rolePermissions->add($rolePermission);

        return new self($this->getStepUid(), $rolePermissions);
    }

    public function withPermissionRevokedForRole(string $role): self
    {
        return new self($this->getStepUid(), $this->getRolePermissions()->withoutRole($role));
    }

    /**
     * removes all write access for each role but keeps read access, unless role had no access to begin with.
     */
    public function withWritePermissionRevokedForAllRoles(): self
    {
        $newRolePermissions = array_map(
            static fn (RolePermission $rolePermission) => $rolePermission->withWriteAccessRevoked(),
            $this->getRolePermissions()->toArray()
        );

        return new self($this->getStepUid(), new RolePermissionCollection($newRolePermissions));
    }

    /**
     * check if write access is explicitly set for given role on this step.
     */
    public function isRoleWriteAccessGranted(string $role): bool
    {
        if (!$this->hasRolePermission($role)) {
            return false;
        }

        return $this->getRolePermission($role)->isWriteAccessGranted();
    }

    /**
     * check if read access is explicitly set for given role on this step,.
     *
     * ignoring write access as indirect read access because that is a matter for the @see FlowPermission to decide
     * also see @see RolePermission::isReadAccessGranted() for more info
     */
    public function isRoleReadAccessGranted(string $role): bool
    {
        if (!$this->hasRolePermission($role)) {
            return false;
        }

        return $this->getRolePermission($role)->isReadAccessGranted();
    }

    /**
     * check if there is at least one role that either read or write access (or both) in this step.
     */
    public function isReadOrWriteAccessGrantedForAtLeastOneRole(): bool
    {
        /** @var RolePermission $rolePermission */
        foreach ($this->getRolePermissions() as $rolePermission) {
            if ($rolePermission->isReadAccessGranted() || $rolePermission->isWriteAccessGranted()) {
                return true;
            }
        }

        return false;
    }

    public function isWriteAccessGrantedForAtLeastOneRole(): bool
    {
        /** @var RolePermission $rolePermission */
        foreach ($this->getRolePermissions() as $rolePermission) {
            if ($rolePermission->isWriteAccessGranted()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array{stepUid: string, rolePermissions: RolePermissionCollection}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_STEP_UID => $this->getStepUid(),
            self::KEY_ROLE_PERMISSIONS => $this->getRolePermissions(),
        ];
    }

    public function duplicate(): self
    {
        return new self($this->getStepUid(), $this->getRolePermissions()->duplicate());
    }
}
