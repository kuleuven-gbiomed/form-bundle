<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Permission;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\RolePermissionCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\StepPermissionCollection;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepCollectionInterface;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;

/**
 * Class FlowPermission.
 *
 * composes & configures a set of step related write and/or read access rights for a number of roles to be used on
 * individual @see InputNode questions to set read and/or write access (or none of the two) to the question for (n)one
 * or more roles in (n)one or more steps, where steps are present and generally configured in a @see WorkFlow
 *
 * IMPORTANT DOMAIN DECISION: unless a method explicitly specifies that 'read-only' access is
 * checked (e.g @see FlowPermission::isStepRoleReadOnlyAccessGranted ), all methods checking for read access for (a)
 * role(s) in steps will check first if explicit read access is granted (@see RolePermission::isReadAccessGranted() )
 * and then - if no explicit read is granted - if 'indirect' read access is granted via
 * write access (@see RolePermission::isWriteAccessGranted() ) for role(s) in step(s).
 * because if a end-user (client) can write in a question (i.e. fill in an answer), then it makes sense that he/she can also
 * read that answer. it is common sense when configuring the permissions for questions in steps in a @see WorkFlow,
 * that if an end-user (client) had write access to a question in one step, he/she should maintain having at least read access
 * in the following steps (although nothing forbids admins to deny any further access in those later steps if the admins
 * wants that, but that would mean a user can fill in an answer and then never see that answer in later steps)
 * hence the use cases where only explicit read access (read-only) is needed or used are few to none.
 * Note that this domain decision is set on this composed object, while the @see StepPermission
 * and @see RolePermission objects that make up this object, do not take this direct vs. indirect read access in account!
 *
 * NOTE: both parameters step(Uid) & role are string (references) here and no objects. this flow permission needs to be
 * serialized & de-serialized directly on questions without having to know what the step or role looks like. especially
 * with steps, it helps to avoid having to maintain the same step object instance dozens or hundreds of times when
 * that step object is changed by an admin in a @see WorkFlow instance. role & step(Uid) are therefore references
 * to their related objects, making it also easier to gracefully fallback to 'no access' if no referenced objects are found.
 *
 * @see     StepPermission
 * @see     RolePermission
 */
final class FlowPermission implements \JsonSerializable, Deserializable
{
    private readonly StepPermissionCollection $stepPermissions;

    /**
     * FlowPermission constructor.
     *
     * NOTE: the stepPermissions can not contain multiple permission definitions for the same stepUid.
     * NOTE: no (step) permissions at all for a question's flow (i.e. empty collection of stepPermissions or all
     *       returning no rights whatsoever for any role in any step) is allowed; mainly because on admin side, when
     *       building a working version of a template, the flow's access rights on questions could be build before the
     *       workflow steps are build (or vice versa). and updates (i.e. deleting or adding steps) on the workflow
     *       (steps) are done before the admin updates the rights for those new/updated steps for each existing
     *       individual question. hence making 'empty' flow permissions necessary. leaving a question without any step
     *       access is allowed & harmless, although useless since no end-user (client) would ever even see that question, but it
     *       could prove 'useful' to already prepare a question without any access to make it available later in a
     *       updated version of a template. however, one should keep in mind that on publish of a working version of
     *       a template, each step in the @see WorkFlow will be required to have at least one question which has
     *       a flow permission that grants at least read access for at least one role in at least one step to make the
     *       step sensible (as in: making sure at least one end-users (clients) has to do something or at least be able to consult
     *       - read - at least one question/answer in that step).
     *
     * @throws \InvalidArgumentException if more than one stepPermission found for same stepUid
     */
    private function __construct(StepPermissionCollection $stepPermissions)
    {
        if ($stepPermissions->hasMultipleForAtLeastOneStepUid()) {
            throw new \InvalidArgumentException('ambigue step permissions: multiple permissions exist for a same step');
        }

        $this->stepPermissions = $stepPermissions;
    }

    public static function createEmpty(): self
    {
        return new self(StepPermissionCollection::createEmpty());
    }

    /**
     * @param StepCollectionInterface<array-key, StepInterface> $steps
     */
    public static function createFromSteps(
        StepCollectionInterface $steps,
    ): self {
        $result = self::createEmpty();

        /** @var StepInterface $step */
        foreach ($steps as $step) {
            $result = $result->withStepPermission(
                StepPermission::fromStepUidRolePermissions(
                    $step->getUid(),
                    new RolePermissionCollection()
                )
            );
        }

        return $result;
    }

    /**
     * @param string[] $stepUids
     */
    public static function createFromStepUids(array $stepUids): self
    {
        $result = self::createEmpty();

        foreach ($stepUids as $stepUid) {
            $result = $result->withStepPermission(
                StepPermission::fromStepUidRolePermissions(
                    $stepUid,
                    new RolePermissionCollection()
                )
            );
        }

        return $result;
    }

    public static function fromStepPermissions(StepPermissionCollection $stepPermissions): self
    {
        return new self($stepPermissions);
    }

    /**
     * @throws \Exception
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        return new self(StepPermissionCollection::fromJsonDecodedArray($data));
    }

    public function withStepPermission(StepPermission $stepPermission): self
    {
        // remove any existing permissions for step
        $stepPermissions = $this->withPermissionsRevokedInStepUidForAllRoles($stepPermission->getStepUid())->getStepPermissions();
        // add new permissions for step
        $stepPermissions->add($stepPermission);

        return new self($stepPermissions);
    }

    public function getStepPermissions(): StepPermissionCollection
    {
        return $this->stepPermissions;
    }

    public function hasStepPermission(string $stepUid): bool
    {
        return $this->getStepPermissions()->hasOneForStepUid($stepUid);
    }

    public function getStepPermission(string $stepUid): StepPermission
    {
        return $this->getStepPermissions()->getOneForStepUid($stepUid);
    }

    /**
     * is given role permitted to write in given stepUid.
     *
     * @see NodeInterface::isStepRoleWriteAccessGranted() for usage and more info
     */
    public function isStepRoleWriteAccessGranted(string $stepUid, string $role): bool
    {
        if (!$this->hasStepPermission($stepUid)) {
            return false;
        }

        return $this->getStepPermission($stepUid)->isRoleWriteAccessGranted($role);
    }

    /**
     * is there at least one role permitted to write in given stepUid.
     */
    public function isStepWriteAccessGrantedForAtLeastOneRole(string $stepUid): bool
    {
        if (!$this->hasStepPermission($stepUid)) {
            return false;
        }

        // don't use collection filter here, return as quickly as possible for performance reasons
        /** @var RolePermission $rolePermission */
        foreach ($this->getStepPermission($stepUid)->getRolePermissions() as $rolePermission) {
            if ($rolePermission->isWriteAccessGranted()) {
                return true;
            }
        }

        return false;
    }

    /**
     * is there at least one step in which given role is permitted to write.
     *
     * NOTE: helps to check if a user (role) will have to (or has shared responsibility with another role to) fill in
     * some question(s) in template at some point in its worklFlow. helps to determine the level of participation of
     * users, e.g. to filter out user to send custom mail with notification that they will have to participate actively
     * at some point in time in the workFlow
     *
     * @throws \Exception
     */
    public function isRoleWriteAccessGrantedForAtLeastOneStep(string $role): bool
    {
        // don't use collection filter here, return as quickly as possible for performance reasons
        /** @var StepPermission $stepPermission */
        foreach ($this->getStepPermissions() as $stepPermission) {
            if ($stepPermission->isRoleWriteAccessGranted($role)) {
                return true;
            }
        }

        return false;
    }

    /**
     * check within given roles if at least one has write access for at least one step.
     *
     * @throws \Exception
     */
    public function isWriteAccessGrantedForAtLeastOneStepAndOneOfRoles(array $roles): bool
    {
        // don't use collection filter here, return as quickly as possible for performance reasons
        foreach ($roles as $role) {
            if ($this->isRoleWriteAccessGrantedForAtLeastOneStep($role)) {
                return true;
            }
        }

        return false;
    }

    public function isWriteAccessGrantedForAtLeastOneRoleInAtLeastOneStep(): bool
    {
        /** @var StepPermission $stepPermission */
        foreach ($this->getStepPermissions() as $stepPermission) {
            if ($stepPermission->isWriteAccessGrantedForAtLeastOneRole()) {
                return true;
            }
        }

        return false;
    }

    /**
     * is given role permitted to to read (or indirectly read via write) in given stepUid.
     *
     * NOTE: read access can be either specifically granted or indirectly by having write access
     *
     * @see NodeInterface::isStepRoleReadAccessGranted() for usage and more info
     */
    public function isStepRoleReadAccessGranted(string $stepUid, string $role): bool
    {
        if (!$this->hasStepPermission($stepUid)) {
            return false;
        }

        $stepPermission = $this->getStepPermission($stepUid);

        return $stepPermission->isRoleReadAccessGranted($role) || $stepPermission->isRoleWriteAccessGranted($role);
    }

    /**
     * is given role permitted to only read (without permission to write) in given stepUid.
     *
     * NOTE: read access must be specifically granted. indirectly granted by having write access is not considered here.
     *
     * @see NodeInterface::isStepRoleReadOnlyAccessGranted() for usage and more info
     */
    public function isStepRoleReadOnlyAccessGranted(string $stepUid, string $role): bool
    {
        if (!$this->hasStepPermission($stepUid)) {
            return false;
        }

        // checking read-only access here. write access giving indirect read access is NOT the goal of this method
        if ($this->getStepPermission($stepUid)->isRoleWriteAccessGranted($role)) {
            return false;
        }

        return $this->getStepPermission($stepUid)->isRoleReadAccessGranted($role);
    }

    /**
     * is there at least one role permitted to read (or indirectly read via write) in given stepUid.
     *
     * NOTE: read access can be either specifically granted or indirectly by having write access
     *
     * @throws \Exception
     */
    public function isStepReadAccessGrantedForAtLeastOneRole(string $stepUid): bool
    {
        if (!$this->hasStepPermission($stepUid)) {
            return false;
        }

        // don't use collection filter here, return as quickly as possible for performance reasons
        /** @var RolePermission $rolePermission */
        foreach ($this->getStepPermission($stepUid)->getRolePermissions() as $rolePermission) {
            if ($rolePermission->isReadAccessGranted() || $rolePermission->isWriteAccessGranted()) {
                return true;
            }
        }

        return false;
    }

    /**
     * is there at least one step in which given role is permitted to read (or indirectly read via write).
     *
     * NOTE: read access can be either specifically granted or indirectly by having write access
     * NOTE: helps to check if a user (role) has the least minimal (i.e. can see something but not necessarily has to
     * fill in some answers) access to template at some point in its worklFlow by looping over questions
     * in @see FormList and finding one that returns true for this method
     *
     * @throws \Exception
     */
    public function isRoleReadAccessGrantedForAtLeastOneStep(string $role): bool
    {
        // don't use collection filter here, return as quickly as possible for performance reasons
        /** @var StepPermission $stepPermission */
        foreach ($this->getStepPermissions() as $stepPermission) {
            if ($stepPermission->isRoleReadAccessGranted($role) || $stepPermission->isRoleWriteAccessGranted($role)) {
                return true;
            }
        }

        return false;
    }

    /**
     * check within given roles if at least one has read access for at least one step.
     *
     * NOTE: like @see FlowPermission::isStepReadAccessGrantedForAtLeastOneRole() this helps to validate working version
     * of a template before publishing it by checking if there is at least one step with at least one question
     * in @see FormList that grants at least some role some minimal access, so that the template is not completely
     * useless (= no questions for anybody to even consult/read, let alone fill in
     *
     * @param array $roles roles of which at least one must have read access
     *
     * @throws \Exception
     */
    public function isReadAccessGrantedForAtLeastOneStepAndOneOfRoles(array $roles): bool
    {
        // don't use collection filter here, return as quickly as possible for performance reasons
        foreach ($roles as $role) {
            if ($this->isRoleReadAccessGrantedForAtLeastOneStep($role)) {
                return true;
            }
        }

        return false;
    }

    /**
     * check if the permissions grant at least one role at least read access (directly or indirectly read via write)
     * in at least one step.
     *
     * NOTE: read access can be either specifically granted or indirectly by having write access
     *
     * NOTE: this is primarily interesting an used to check minimal usable access on questions and subsequently
     * filter out those questions in @see InputNodeCollection::getReadableInAtLeastOneStep via flow
     * permission @see FlowPermission::isReadAccessGrantedForAtLeastOneRoleInAtLeastOneStep() to be able to determine
     * what questions are up for auto assigning write access in @see WorkingVersion::withAutoAssignedTimedWriteAccessForRoles()
     *
     * @throws \Exception
     */
    public function isReadAccessGrantedForAtLeastOneRoleInAtLeastOneStep(): bool
    {
        /** @var StepPermission $stepPermission */
        foreach ($this->getStepPermissions() as $stepPermission) {
            if ($stepPermission->isReadOrWriteAccessGrantedForAtLeastOneRole()) {
                return true;
            }
        }

        return false;
    }

    /**
     * revokes any permission that given role has in any step in this flow permission and returns a new flow permission
     * with all access in all steps removed for given role.
     */
    public function withStepPermissionsRevokedForRole(string $role): self
    {
        $stepPermissions = array_map(
            static fn (StepPermission $stepPermission) => $stepPermission->withPermissionRevokedForRole($role),
            $this->getStepPermissions()->toArray()
        );

        return new self(new StepPermissionCollection($stepPermissions));
    }

    public function withPermissionsRevokedInStepUidForAllRoles(string $stepUid): self
    {
        return new self($this->getStepPermissions()->withoutStepPermissionForStepUid($stepUid));
    }

    public function withPermissionsRevokedForRoleInStepUid(string $role, string $stepUid): self
    {
        // avoid exception on non-existing step permissions; return duplicate instance
        if (!$this->hasStepPermission($stepUid)) {
            return new self($this->getStepPermissions()->duplicate());
        }

        // keep existing stepPermission but remove its permissions for given role
        return $this->withStepPermission($this->getStepPermission($stepUid)->withPermissionRevokedForRole($role));
    }

    public function withFullWritePermissionsForRoleInStepUid(string $role, string $stepUid): self
    {
        $stepPermission = $this->hasStepPermission($stepUid) ?
            $this->getStepPermission($stepUid) :
            StepPermission::fromStepUidRolePermissions($stepUid, new RolePermissionCollection());

        return $this->withStepPermission(
            $stepPermission->withRolePermission(RolePermission::createForRoleWithFullWriteReadAccess($role))
        );
    }

    /**
     * removes in each step all write access for each role but keeps read access, unless role had no access to begin with.
     */
    public function withWritePermissionsRevokedForAllRolesInAllSteps(): self
    {
        $stepPermissions = array_map(
            static fn (StepPermission $stepPermission) => $stepPermission->withWritePermissionRevokedForAllRoles(),
            $this->getStepPermissions()->toArray()
        );

        return new self(new StepPermissionCollection($stepPermissions));
    }

    public function withReadOnlyPermissionsForRoleInStepUid(string $role, string $stepUid): self
    {
        $stepPermission = $this->hasStepPermission($stepUid) ?
            $this->getStepPermission($stepUid) :
            StepPermission::fromStepUidRolePermissions($stepUid, new RolePermissionCollection());

        return $this->withStepPermission(
            $stepPermission->withRolePermission(RolePermission::createForRoleWithReadOnlyAccess($role))
        );
    }

    /**
     * @return string[]
     */
    public function getRolesWithWriteAccessInStepUid(string $stepUid): array
    {
        // no exception if step not found: the step permissions could not be defined for this step,
        // which is the same as no access for any role at all
        if (!$this->hasStepPermission($stepUid)) {
            return [];
        }

        $roles = [];
        /** @var RolePermission $rolePermission */
        foreach ($this->getStepPermission($stepUid)->getRolePermissions() as $rolePermission) {
            if ($rolePermission->isWriteAccessGranted()) {
                $roles[] = $rolePermission->getRole();
            }
        }

        return array_unique($roles);
    }

    /**
     * @return string[]
     */
    public function getRolesWithReadAccessInStepUid(string $stepUid): array
    {
        // no exception if step not found: the step permissions could not be defined for this step,
        // which is the same as no access for any role at all
        if (!$this->hasStepPermission($stepUid)) {
            return [];
        }

        $roles = [];
        /** @var RolePermission $rolePermission */
        foreach ($this->getStepPermission($stepUid)->getRolePermissions() as $rolePermission) {
            if ($rolePermission->isReadAccessGranted() || $rolePermission->isWriteAccessGranted()) {
                $roles[] = $rolePermission->getRole();
            }
        }

        return array_unique($roles);
    }

    /**
     * @return string[]
     */
    public function getRolesWithReadOnlyAccessInStepUid(string $stepUid): array
    {
        // no exception if step not found: the step permissions could not be defined for this step,
        // which is the same as no access for any role at all
        if (!$this->hasStepPermission($stepUid)) {
            return [];
        }

        $roles = [];
        /** @var RolePermission $rolePermission */
        foreach ($this->getStepPermission($stepUid)->getRolePermissions() as $rolePermission) {
            if ($rolePermission->isReadAccessGranted()) {
                $roles[] = $rolePermission->getRole();
            }
        }

        return array_unique($roles);
    }

    /**
     * NOTE: calling @see StepPermissionCollection::jsonSerialize() explicitly here is not needed because
     * contract @see \JsonSerializable on that collection will handle the array type return. it is called explicitly
     * here anyway to satisfy this method's return type and avoid complaints from qa tools. also not
     * calling @see StepPermissionCollection::toArray() here since JsonSerializable contract handling the jsonSerialize.
     */
    public function jsonSerialize(): array
    {
        return $this->getStepPermissions()->jsonSerialize();
    }
}
