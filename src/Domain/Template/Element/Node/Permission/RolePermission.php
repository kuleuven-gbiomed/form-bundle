<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Permission;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\RolePermissionCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Class RolePermission.
 *
 * configures write and/or read access rights for a role to be used in a @see StepPermission
 *
 * Note: strictly speaking does this object not even know what a role object is. for easier serializing and
 * de-serializing, this object does/can not use role objects, instead it uses role names that refer to @see Role .
 *
 * @see     WorkFlow
 * @see     NodeInterface
 * @see     Role
 * @see     RolePermissionCollection
 */
final class RolePermission implements \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_ROLE = 'role';
    /** @var string */
    public const KEY_WRITE = 'write';
    /** @var string */
    public const KEY_READ = 'read';

    private readonly Role $role;

    /**
     * RolePermission constructor.
     *
     * @throws \Exception
     */
    private function __construct(string $roleName, private readonly bool $write, private readonly bool $read)
    {
        if ('' === $roleName) {
            throw new \InvalidArgumentException('parameter roleName must be a non empty string');
        }

        $this->role = Role::fromName($roleName);
    }

    /**
     * grants the given role full access, meaning write access and also explicit read access.
     *
     * use this when you want to give a role only the possibility to 'give' (submit, tempSave) and 'see' answers given
     * to a question in a certain step: @see InputNode::getFlowPermission()
     *
     * NOTE in @see FlowPermission on questions, the read access for a role (in a step) is always determined by checking
     * if that role either has direct read access OR if that role has write access which grants indirect read access (*)
     * so setting the read to true will not make a difference in actual @see FlowPermission checks on questions
     * but it is better to set it explicitly, just in case that might come in handy some day...
     *
     * * because it doesn't make sense to let someone submit an answer for a question in a certain workflow step,
     * but then deny him/her to see that answer after the successful submit of the answer, while the form remains in
     * that same step. instead, it is not uncommon to only grant read access in another, following step or steps. less
     * common is to grant no access at all in another, following step or steps, once you did grant the role write access,
     * but there might be use cases.
     *
     * @throws \Exception
     */
    public static function createForRoleWithFullWriteReadAccess(string $role): self
    {
        return new self($role, true, true);
    }

    /**
     * grants the given role only read access, i.e. no write access.
     *
     * use this when you want to give a role only the possibility to 'see' answers given to a question in a certain
     * step: @see InputNode::getFlowPermission(), witjout being able to 'give' answers
     *
     * @throws \Exception
     */
    public static function createForRoleWithReadOnlyAccess(string $role): self
    {
        return new self($role, false, true);
    }

    /**
     * grants the given role only no access at all.
     *
     * use this when you want to give a role no possibility to 'give' and 'see' answers given to a question in a certain
     * step: @see InputNode::getFlowPermission()
     *
     * NOTE: this is the same as omitting a rolePermission in a @see StepPermission . if the system does not find
     * a role permission for a role, it will consider it as no access granted at all, so instead of using this method
     * to set access, you can also just leave out the rolePermission for a role you don't want  to build access for.
     *
     * @throws \Exception
     */
    public static function createForRoleWithNoAccess(string $role): self
    {
        return new self($role, false, false);
    }

    /**
     * @throws \Exception
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        return new self(
            $data[self::KEY_ROLE],
            $data[self::KEY_WRITE],
            $data[self::KEY_READ]
        );
    }

    /**
     * removes the write access, but keeps read access if it was granted directly (i.e. readOnly access was granted)
     * or indirectly via write access (which automatically grants read access as well).
     * nothing changes if no access was granted to begin with; role will keep having no write nor read access.
     */
    public function withWriteAccessRevoked(): self
    {
        if ($this->isReadAccessGranted()) {
            return self::createForRoleWithReadOnlyAccess($this->getRole());
        }

        return self::createForRoleWithNoAccess($this->getRole());
    }

    public function getRole(): string
    {
        return (string) $this->role;
    }

    public function isWriteAccessGranted(): bool
    {
        return $this->write;
    }

    /**
     * check if explicit read access is granted.
     *
     * NOTE: this only checks is property read is set to true, in contrast to @see FlowPermission that checks and
     * considers read access on explicit read access AND on indirect read access via write access. this is a domain
     * decision: in a workflow, write access automatically gives read access as well, allowing users to see the
     * answers they just gave (submitted by write access) in the same workflow step. flowPermission differentiate
     * between read-only and read access @see FlowPermission::isStepRoleReadOnlyAccessGranted()
     * but on this lowest level, here on role permission as well as on @see StepPermission, the write and read are
     * kept separate for that very reason to allow FlowPermission to decide decides what read access is.
     */
    public function isReadAccessGranted(): bool
    {
        return $this->read;
    }

    /**
     * @return array{role: string, write: bool, read: bool}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_ROLE => $this->getRole(),
            self::KEY_WRITE => $this->isWriteAccessGranted(),
            self::KEY_READ => $this->isReadAccessGranted(),
        ];
    }

    public function duplicate(): self
    {
        if ($this->isWriteAccessGranted()) {
            return self::createForRoleWithFullWriteReadAccess($this->getRole());
        }

        if ($this->isReadAccessGranted()) {
            return self::createForRoleWithReadOnlyAccess($this->getRole());
        }

        return self::createForRoleWithNoAccess($this->getRole());
    }
}
