<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Factory;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\CheckBoxInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\DateInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\AbstractReadOnlyViewNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\BasicViewNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;

/**
 * Class NodeFactory.
 */
class NodeFactory
{
    /**
     * assumes serialized data contains existing and correct value for the key type. if not, either here (non-existing
     * value) or in input node (factory) constructor method(s) (incorrect value), the exception will be thrown.
     *
     * @throws \InvalidArgumentException     if serialized data has wrong node type or wrong/unhandled input type
     * @throws LocalizedStringException      indirectly if any required labels are missing
     * @throws InputInvalidArgumentException
     */
    public static function createInputNodeFromJsonDecodedArray(array $data): InputNode
    {
        if (!array_key_exists(NodeInterface::KEY_TYPE, $data)) {
            throw new \InvalidArgumentException('data for deserializing input node is missing the required type key: '.NodeInterface::KEY_TYPE);
        }

        $inputNodeType = $data[NodeInterface::KEY_TYPE];

        return match ($inputNodeType) {
            CheckBoxInputNode::getType() => CheckBoxInputNode::fromJsonDecodedArray($data),
            ChoiceInputNode::getType() => ChoiceInputNode::fromJsonDecodedArray($data),
            NumberInputNode::getType() => NumberInputNode::fromJsonDecodedArray($data),
            OptionInputNode::getType() => OptionInputNode::fromJsonDecodedArray($data),
            TextInputNode::getType() => TextInputNode::fromJsonDecodedArray($data),
            UploadInputNode::getType() => UploadInputNode::fromJsonDecodedArray($data),
            DateInputNode::getType() => DateInputNode::fromJsonDecodedArray($data),
            default => throw new \InvalidArgumentException('cannot deserialize '.InputNode::class.': unknown/unhandled input type '.$inputNodeType),
        };
    }

    public static function createReadOnlyViewNodeFromJsonDecodedArray(array $data): AbstractReadOnlyViewNode
    {
        if (!array_key_exists(NodeInterface::KEY_TYPE, $data)) {
            throw new \InvalidArgumentException('data for deserializing readOnlyView node is missing the required type key: '.NodeInterface::KEY_TYPE);
        }

        $readOnlyViewNodeType = $data[NodeInterface::KEY_TYPE];

        return match ($readOnlyViewNodeType) {
            BasicViewNode::getType() => BasicViewNode::fromJsonDecodedArray($data),
            RadarChartViewNode::getType() => RadarChartViewNode::fromJsonDecodedArray($data),
            default => throw new \InvalidArgumentException('cannot deserialize '.AbstractReadOnlyViewNode::class.': unknown/unhandled input type '.$readOnlyViewNodeType),
        };
    }

    /**
     * create a category node without any info (the optional label, descriptions) and without any children.
     */
    public static function createEmptyCategoryNode(string $uid): CategoryNode
    {
        return CategoryNode::fromUidAndMinimalDependencies($uid);
    }
}
