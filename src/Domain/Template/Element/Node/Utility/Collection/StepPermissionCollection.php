<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\StepPermission;
use Webmozart\Assert\Assert;

/**
 * @see     StepPermission
 *
 * @extends ArrayCollection<array-key,StepPermission>
 */
final class StepPermissionCollection extends ArrayCollection implements \JsonSerializable, Deserializable
{
    /**
     * creates a flow permission that grants no rights at all to any role in any step.
     */
    public static function createEmpty(): self
    {
        return new self();
    }

    /**
     * @throws \Exception
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $stepPermissions = [];

        foreach ($data as $stepPermissionData) {
            $stepPermissions[] = StepPermission::fromJsonDecodedArray($stepPermissionData);
        }

        return new self($stepPermissions);
    }

    public function getForStepUid(string $stepUid): self
    {
        Assert::stringNotEmpty($stepUid);

        $result = [];

        /** @var StepPermission $stepPermission */
        foreach ($this->getIterator() as $stepPermission) {
            if ($stepPermission->getStepUid() === $stepUid) {
                $result[] = $stepPermission;
            }
        }

        return new self($result);
    }

    public function hasOneForStepUid(string $stepUid): bool
    {
        return 1 === $this->getForStepUid($stepUid)->count();
    }

    /**
     * @param array<array-key, string> $stepUids
     */
    public function getByStepUids(array $stepUids): self
    {
        return $this->filter(fn (StepPermission $stepPermission) => in_array($stepPermission->getStepUid(), $stepUids, true));
    }

    public function getOneForStepUid(string $stepUid): StepPermission
    {
        $stepPermissions = $this->getForStepUid($stepUid);

        $first = $stepPermissions->first();
        if ($first instanceof StepPermission && 1 === $stepPermissions->count()) {
            return $first;
        }

        throw new \BadMethodCallException("expected to find 1 for stepUid {$stepUid}. got {$stepPermissions->count()}");
    }

    /**
     * checks if more than one StepPermission is found for same stepUid.
     */
    public function hasMultipleForAtLeastOneStepUid(): bool
    {
        $foundStepUids = [];

        /** @var StepPermission $stepPermission */
        foreach ($this->getIterator() as $stepPermission) {
            if (in_array($stepPermission->getStepUid(), $foundStepUids, true)) {
                return true;
            }

            $foundStepUids[] = $stepPermission->getStepUid();
        }

        return false;
    }

    public function withoutStepPermissionForStepUid(string $stepUid): self
    {
        return $this->filter(fn (StepPermission $stepPermission) => !($stepPermission->getStepUid() === $stepUid))->duplicate();
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * @return string[]
     */
    public function getStepUids(): array
    {
        return array_map(fn (StepPermission $stepPermission) => $stepPermission->getStepUid(), $this->toArray());
    }

    public function duplicate(): self
    {
        $duplicates = array_map(
            static fn (StepPermission $stepPermission) => $stepPermission->duplicate(),
            $this->toArray()
        );

        return new self($duplicates);
    }
}
