<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\RolePermission;
use Webmozart\Assert\Assert;

/**
 * Class RolePermissionCollection.
 *
 * @see     RolePermission
 *
 * @extends ArrayCollection<array-key, RolePermission>
 */
final class RolePermissionCollection extends ArrayCollection implements \JsonSerializable, Deserializable
{
    /**
     * @throws \Exception
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $rolePermissions = [];

        foreach ($data as $rolePermissionData) {
            $rolePermissions[] = RolePermission::fromJsonDecodedArray($rolePermissionData);
        }

        return new self($rolePermissions);
    }

    public function getForRole(string $role): self
    {
        Assert::stringNotEmpty($role);

        $result = [];

        /** @var RolePermission $rolePermission */
        foreach ($this->getIterator() as $rolePermission) {
            if ($rolePermission->getRole() === $role) {
                $result[] = $rolePermission;
            }
        }

        return new self($result);
    }

    public function hasOneForRole(string $role): bool
    {
        return 1 === $this->getForRole($role)->count();
    }

    /**
     * @throws \BadMethodCallException if none found
     */
    public function getOneForRole(string $role): RolePermission
    {
        $rolePermissions = $this->getForRole($role);

        $first = $rolePermissions->first();
        if ($first instanceof RolePermission && 1 === $rolePermissions->count()) {
            return $first;
        }

        throw new \BadMethodCallException("expected to find 1 for role {$role}. got {$rolePermissions->count()}");
    }

    public function withoutRole(string $role): self
    {
        return $this->filter(fn (RolePermission $rolePermission) => !($rolePermission->getRole() === $role))->duplicate();
    }

    /**
     * checks if more than one rolePermission is found for same role.
     */
    public function hasMultipleForAtLeastOneRole(): bool
    {
        $foundRoles = [];

        /** @var RolePermission $rolePermission */
        foreach ($this->getIterator() as $rolePermission) {
            if (in_array($rolePermission->getRole(), $foundRoles, true)) {
                return true;
            }

            $foundRoles[] = $rolePermission->getRole();
        }

        return false;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function duplicate(): self
    {
        $duplicates = array_map(
            static fn (RolePermission $rolePermission) => $rolePermission->duplicate(),
            $this->toArray()
        );

        return new self($duplicates);
    }
}
