<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\GlobalScoring\CalculationDependencyConfig;
use Webmozart\Assert\Assert;

/**
 * Class CalculationDependencyConfigCollection.
 *
 * @see     CalculationDependencyConfig
 *
 * @extends ArrayCollection<array-key,CalculationDependencyConfig>
 */
final class CalculationDependencyConfigCollection extends ArrayCollection implements \JsonSerializable, Deserializable
{
    /**
     * @param CalculationDependencyConfig[] $elements
     */
    public function __construct(array $elements = [])
    {
        $this->guardSumOfWeighingsIsNotZero(...$elements);

        parent::__construct($elements);
    }

    public static function createEmpty(): self
    {
        return new self();
    }

    public static function fromJsonDecodedArray(array $data): self
    {
        $elements = [];

        foreach ($data as $configData) {
            $elements[] = CalculationDependencyConfig::fromJsonDecodedArray($configData);
        }

        return new self($elements);
    }

    public function hasOneByNodeUid(string $uid): bool
    {
        $configs = $this->filter(fn (CalculationDependencyConfig $config) => $config->getScoreNodeUid() === $uid);

        return 1 === $configs->count();
    }

    public function getOneByNodeUid(string $uid): CalculationDependencyConfig
    {
        $configs = $this->filter(fn (CalculationDependencyConfig $config) => $config->getScoreNodeUid() === $uid);

        $first = $configs->first();
        if ($first instanceof CalculationDependencyConfig && 1 === $configs->count()) {
            return $first;
        }

        throw new \BadMethodCallException("expected to find 1 for uid {$uid}. got {$configs->count()}");
    }

    public function getSumOfWeighings(): float
    {
        $sum = 0.0;

        /** @var CalculationDependencyConfig $calcConfig */
        foreach ($this->getIterator() as $calcConfig) {
            $sum += $calcConfig->getWeighing();
        }

        return $sum;
    }

    private function guardSumOfWeighingsIsNotZero(CalculationDependencyConfig ...$elements): void
    {
        // nothig to guard if no elements set
        if (0 === count($elements)) {
            return;
        }

        $sum = 0.0;
        foreach ($elements as $element) {
            $sum += $element->getWeighing();
        }

        Assert::notEq($sum, 0.0, 'the sum of the weighings may not be 0 (zero) to avoid division by zero in calculations');
    }

    public function add($element): void
    {
        $elements = array_merge($this->toArray(), [$element]);
        // check if adding this element will not make the sum weighings become zero
        $this->guardSumOfWeighingsIsNotZero(...$elements);

        parent::add($element);
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
