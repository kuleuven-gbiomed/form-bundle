<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\GlobalScoring\CalculationDependencyConfig;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\GlobalScoring\CalculationScoreNodeConfig;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;

/**
 * Class CalculationScoreNodeConfigCollection.
 *
 * @see     CalculationScoreNodeConfig
 *
 * @extends ArrayCollection<array-key,CalculationScoreNodeConfig>
 */
class CalculationScoreNodeConfigCollection extends ArrayCollection
{
    /**
     * NOTE: not enforcing that the same node is not added twice to calculated one global score node.
     * because it could be a use case that a global score is calculated using the same node twice (or more).
     * that might be somewhat pointless, because one can use the weighing if the a question has to have 'double' weight
     * in regards to other questions to calculate a global score, but never say never...
     *
     * @return CalculationScoreNodeConfigCollection
     */
    public static function fromRootNodeAndDependencyConfigs(
        CategoryNode $rootNode,
        CalculationDependencyConfigCollection $dependencyConfigs,
    ) {
        // if no configuration set for any nodes, return nada
        if ($dependencyConfigs->isEmpty()) {
            return new self();
        }

        // use flattened list of relevant input nodes from rootNode (instead of searching each descendant in a large trees)
        $parentalInputNodes = $rootNode->getFlattenedParentalInputNodeDescendants();
        $configNodes = [];
        /** @var CalculationDependencyConfig $dependencyConfig */
        foreach ($dependencyConfigs as $dependencyConfig) {
            // if config refers to a node that does not / no longer exists, don't break or throw exception, just skip
            if (!$parentalInputNodes->hasOneByNodeUid($dependencyConfig->getScoreNodeUid())) {
                continue;
            }

            $scoreNode = $parentalInputNodes->getOneByNodeUid($dependencyConfig->getScoreNodeUid());
            // if for some reason the node can't even act as scorable, don't break or throw exception , just skip
            if (!$scoreNode instanceof OperableForScoring) {
                continue;
            }

            $configNodes[] = CalculationScoreNodeConfig::fromScoreNodeAndConfig($scoreNode, $dependencyConfig);
        }

        return new self($configNodes);
    }

    /**
     * get the one that are reachable and that meet requirements for scoring.
     *
     * NOTE: if method used from within a global score node (where the collection on which this filtering is called is
     * build based on the @see CalculationDependencyConfigCollection for that global score node), then we
     * do not need to check if the returned scoring nodes - that meet requirements and are reachable - do actually
     * operate as scoring nodes (@see OperableForScoring::operatesAsScoreNode() for at least one global score node,
     * since we are calling this from a global score node and it's that global score node that has the responsibility
     * for its dependencies to be correctly assigned to himself. so no @see OperableForScoring::operatesAsScoreNode()
     * check here since that is a unnecessary performance heavy method to call, especially if called multiple times.
     * ( if a check on this score nodes being targeted by a specific global score is needed, filtering can
     * be chained on @see OperableForScoring::operatesAsScoreNodeForGlobalScoreNode() )
     *
     * NOTE: in theory, a node can be used twice (or more) to calculate a global score.
     * hence no assert on uniqueness of node is done (see docBlock @see fromRootNodeAndDependencyConfigs )
     */
    public function getValidForCalculation(): self
    {
        /** @var CalculationScoreNodeConfigCollection $result */
        $result = $this->filter(function (CalculationScoreNodeConfig $configNode) {
            $scoreNode = $configNode->getScoreNode();

            // must have valid scoring parameters (e.g. a rounding mode, scale, max score, non multiple in case of choice, etc.)
            if (!$scoreNode->meetsScoringRequirements()) {
                return false;
            }

            // can not be directly or indirectly hidden in the tree. because to un-hide this score later in a new published
            // version of the template would be unwise in case some users already filled in some forms: it would suddenly
            // become a dependency of a global score calculation that might or might not already been automatically
            // calculated and set as answer. especially if no one has write access left to re-calculate the answer.
            if (!$scoreNode->isReachableInTree()) {
                return false;
            }

            // in the formula for weighted calculations, we divide the given (numeric) answer of a dependency
            // question by that question's max score and multiply by that question's weighing. but only if that
            // dependency has actually a weighing different from the max score, otherwise, there is no need for
            // the division/multiplication. but that means the max score can not be zero if it differs from weighing!
            // @see \KUL\FormBundle\Domain\TemplateTarget\Calculation\CalculationDataCollection::getTotalWeightedValuesSumForCommonWeighingType
            $maxScore = $scoreNode->getInputScoringParameters()->getMaxScore();

            return ($maxScore === $configNode->getConfig()->getWeighing())
                || !(0.0 === $maxScore);
        });

        return $result;
    }

    /**
     * get the score nodes.
     *
     * NOTE: a score node can also be a global score itself for other score nodes
     *
     * @return InputNodeCollection<array-key,InputNode>
     *
     * @psalm-suppress InvalidReturnType
     * @psalm-suppress InvalidReturnStatement
     */
    public function getScoreNodes(): InputNodeCollection
    {
        $scoreNodes = [];

        /** @var CalculationScoreNodeConfig $configNode */
        foreach ($this->getIterator() as $configNode) {
            /** @var InputNode $scoreNode */
            $scoreNode = $configNode->getScoreNode();

            $scoreNodes[] = $scoreNode;
        }

        return new InputNodeCollection($scoreNodes);
    }

    public function hasForScoreNode(OperableForScoring $scoreNode): bool
    {
        /** @var CalculationScoreNodeConfig $configNode */
        foreach ($this->getIterator() as $configNode) {
            if ($configNode->getScoreNode()->getUid() === $scoreNode->getUid()) {
                return true;
            }
        }

        return false;
    }

    public function getForScoreNode(OperableForScoring $scoreNode): CalculationScoreNodeConfig
    {
        $nodes = $this->filter(fn (CalculationScoreNodeConfig $config) => $config->getScoreNode()->getUid() === $scoreNode->getUid());
        $first = $nodes->first();

        if ($first instanceof CalculationScoreNodeConfig && 1 === $nodes->count()) {
            return $first;
        }

        throw new \BadMethodCallException("Expected to find exactly one CalculationScoreNodeConfig for {$scoreNode->getUid()}, found: {$nodes->count()}.");
    }
}
