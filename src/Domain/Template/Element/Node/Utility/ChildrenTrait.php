<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Utility;

use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ChildrenAwareInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;

/**
 * Trait ChildrenTrait.
 *
 * TODO attachChild, removeChild, etc used in current admin side to update single children, but it may be possible
 *      to directly replace parent object in tree (using @see replaceChild on parent's parent) with new instance
 *      containing altered version of child collections, based on existing (less mutable)
 *
 * @see     ChildrenAwareInterface
 *
 * @template T of NodeInterface
 * @template TCollection of NodeCollection<array-key, T>
 */
trait ChildrenTrait
{
    /** @return TCollection */
    abstract public function getChildren();

    /** @return TCollection */
    abstract protected static function createChildCollection(array $children = []);

    abstract public function getUid(): string;

    /** @var ?TCollection */
    private $cachedFlattenedNodeDescendants;

    public function hasChildren(): bool
    {
        return !$this->getChildren()->isEmpty();
    }

    /**
     * @param TCollection $children
     */
    protected function updateChildren($children): void
    {
        // important! need to reset the locally cached children
        $this->cachedFlattenedNodeDescendants = null;

        // check for duplicate uids to assert that every uid of any node that is added to any parent is always unique
        $duplicateUids = $children->getDuplicateNodeUids();
        if (0 < count($duplicateUids)) {
            throw new \InvalidArgumentException('the children must be unique (must have unique uids), but found duplicate children for uids ['.implode(' | ', $duplicateUids).'].');
        }

        foreach ($children->toArray() as $child) {
            $child->updateParent($this);
        }

        $this->children = $children;
    }

    public function hasChildByUid(string $uid): bool
    {
        return $this->getChildren()->hasOneByNodeUid($uid);
    }

    public function getChildByUid(string $uid): NodeInterface
    {
        return $this->getChildren()->getOneByNodeUid($uid);
    }

    /**
     * NOTE: could look for descendant by first building a flattened list of all descendants here, like is more
     * or less done in @see CategoryNode::getFlattenedWithAllDescendants() but recursive looping over the children and
     * returning as soon as node is found, will be faster for most nodes to look for. this method is more for internal
     * use and for the building of nodes in admin-side, because for the end-users (clients) side, we work from @see FormList
     * where local caching of the flattened nodes is used to increase performance when looking for specific nodes.
     * hence, as with everything in the nested structure, trick is to decide when to use what to increase performance.
     */
    public function findDescendantByUid(string $uid): ?NodeInterface
    {
        // check direct children first
        if ($this->hasChildByUid($uid)) {
            return $this->getChildByUid($uid);
        }

        // check deeper within each child that can have descendants on its own
        foreach ($this->getChildren() as $child) {
            if (!$child instanceof ChildrenAwareInterface) {
                continue;
            }

            $descendant = $child->findDescendantByUid($uid);
            if ($descendant instanceof NodeInterface) {
                return $descendant;
            }
        }

        return null;
    }

    public function hasDescendantByUid(string $uid): bool
    {
        return $this->findDescendantByUid($uid) instanceof NodeInterface;
    }

    public function getDescendantByUid(string $uid): NodeInterface
    {
        $descendant = $this->findDescendantByUid($uid);

        if (!$descendant instanceof NodeInterface) {
            throw new \BadMethodCallException('no descendant found for uid '.$uid);
        }

        return $descendant;
    }

    public function hasActiveChildren(): bool
    {
        return 0 < $this->getChildren()->getNotMarkedAsHidden()->count();
    }

    /** @return TCollection */
    public function getActiveChildren()
    {
        return $this->getChildren()->getNotMarkedAsHidden();
    }

    public function attachChild(NodeInterface $child): void
    {
        $this->validateAttachingChild($child);

        $this->updateChildren(static::createChildCollection([...$this->getChildren()->toArray(), $child]));
    }

    public function attachChildren(NodeInterface ...$children): self
    {
        foreach ($children as $child) {
            $this->attachChild($child);
        }

        return $this;
    }

    public function attachAsFirstChild(NodeInterface $child): void
    {
        $this->validateAttachingChild($child);

        $this->updateChildren(static::createChildCollection([$child, ...$this->getChildren()->toArray()]));
    }

    public function attachChildAfter(NodeInterface $child, NodeInterface $attachAfterChild): void
    {
        $this->validateAttachingChild($child, $attachAfterChild);

        $children = [];

        foreach ($this->getChildren() as $existingChild) {
            $children[] = $existingChild;

            if ($existingChild->getUid() === $attachAfterChild->getUid()) {
                $children[] = $child;
            }
        }

        $this->updateChildren(static::createChildCollection($children));
    }

    public function removeChild(NodeInterface $child): void
    {
        $this->validateRemovingChild($child);

        $children = static::createChildCollection();
        foreach ($this->getChildren() as $existingChild) {
            if ($existingChild->getUid() === $child->getUid()) {
                continue;
            }

            $children->add($existingChild);
        }

        $this->updateChildren($children);
    }

    public function removeAllChildren(): void
    {
        $this->updateChildren(static::createChildCollection());
    }

    public function replaceChild(NodeInterface $child): void
    {
        $this->validateReplacingChild($child);

        $children = array_map(
            fn (NodeInterface $existingChild) => $existingChild->getUid() === $child->getUid() ? $child : $existingChild,
            $this->getChildren()->toArray()
        );

        $this->updateChildren(static::createChildCollection($children));
    }

    protected function validateAttachingChild(NodeInterface $child, ?NodeInterface $attachAfterChild = null): void
    {
        $baseError = 'cannot attach child ['.$child->getUid().'] to parent ['.$this->getUid().']';

        if ($this->getChildren()->hasNode($child)) {
            throw new \InvalidArgumentException($baseError.': it is already added as child.');
        }

        // don't validate further if not requested to insert on a specified position after a given child
        if (!$attachAfterChild instanceof NodeInterface) {
            return;
        }

        if (!$this->getChildren()->hasNode($attachAfterChild)) {
            throw new \InvalidArgumentException($baseError.' on requested position in existing children: the node with uid ['.$attachAfterChild->getUid().'] to insert after, is not an existing child of the parent.');
        }

        if ($child->getUid() === $attachAfterChild->getUid()) {
            throw new \InvalidArgumentException($baseError.'on requested position in existing children: the existing child node with uid ['.$attachAfterChild->getUid().'] to attach the new child after, refers to the new child itself (invalid position)');
        }
    }

    protected function validateRemovingChild(NodeInterface $child): void
    {
        if (!$this->getChildren()->hasNode($child)) {
            throw new \InvalidArgumentException('cannot remove child ['.$child->getUid().'] from parent ['.$this->getUid().']: it does not exist in the collection of children for this parent.');
        }
    }

    protected function validateReplacingChild(NodeInterface $child): void
    {
        if (!$this->getChildren()->hasNode($child)) {
            throw new \InvalidArgumentException('cannot replace child ['.$child->getUid().'] from parent ['.$this->getUid().']: it does not exist in the collection of children for this parent.');
        }
    }

    /** @return TCollection */
    abstract public function getFlattenedWithAllDescendants();

    /**
     * @return T[]
     */
    public function getFlattenedWithAllDescendantsArray(): array
    {
        $childLists = [];
        foreach ($this->getChildren() as $child) {
            // if child can't have descendants, just add it and go to next child
            if (!$child instanceof ChildrenAwareInterface) {
                $childLists[] = [$child];
                continue;
            }

            // if child has descendants, process recursively so it gets added with all its descendants
            $childDescendants = $child->getFlattenedWithAllDescendantsArray();
            if (0 !== count($childDescendants)) {
                $childLists[] = $childDescendants;
            }
        }

        // add self also! using one time array_merge with variadic args is/should be more performant than calling array_merge
        // in above foreach on each of its elements: reduces total amount of array_merge calls in total recursive result
        return array_merge([$this], ...$childLists);
    }
}
