<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Scoring;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Rounding\RoundingModes;
use Webmozart\Assert\Assert;

/**
 * Class ScoringParameters.
 *
 * parameters needed to work with inputs as part of making the input-related question a @see OperableForScoring
 *
 * NOTE: this object is for internal scoring use, all parameters are optional. scorability is determined based on
 * the presence and values of specified set of parameters in @see makesScoringPossible . admins do not have to provide
 * all parameters at once to make an question scorable or - vice versa - to remove scorability of a question
 */
final class ScoringParameters implements \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_MIN_SCORE = 'minScore';
    /** @var string */
    public const KEY_MAX_SCORE = 'maxScore';
    /** @var string */
    public const KEY_PASS_SCORE = 'passScore';
    /** @var string */
    public const KEY_ROUNDING_MODE = 'roundingMode';
    /** @var string */
    public const KEY_SCALE = 'scale';
    /** @var string */
    public const KEY_SHOW_AUTO_CALCULATE_BTN = 'showAutoCalculateBtn';
    /** @var string */
    public const KEY_ALLOW_NON_AUTO_CALCULATED_VALUE = 'allowNonAutoCalculatedValue';

    /** @var bool */
    public const DEFAULT_SHOW_AUTO_CALCULATE_BTN = true;
    /** @var bool */
    public const DEFAULT_ALLOW_NON_AUTO_CALCULATED_VALUE = true;

    /** holds the minimum score allowed, needed to (re)weigh the submitted/calculated value in calculations */
    private readonly ?float $minScore;
    /** holds the maximum score allowed, needed to (re)weigh the submitted/calculated value in calculations */
    private readonly ?float $maxScore;
    /** holds the value to determine whether submitted/calculated value is enough to 'pass' (cf. passing exams) */
    private readonly ?float $passScore;

    /**
     * holds method on how to round the submitted/calculated value if needed.
     *
     * @phpstan-var ?RoundingModes::ROUND_* $roundingMode
     */
    private readonly ?int $roundingMode;
    /** holds numbers of decimals to use in rounding the submitted/calculated value if needed. */
    private readonly ?int $scale;

    /**
     * @phpstan-param ?RoundingModes::ROUND_* $roundingMode
     */
    private function __construct(
        ?float $minScore,
        ?float $maxScore,
        ?float $passScore,
        ?int $roundingMode,
        ?int $scale,
        /** regardless if a button to retrieve a (re)calculated value via ajax is to be shown if it's a global score */
        private readonly bool $showAutoCalculateBtn,
        /** regardless if to allow manual input (client side and/or server side) if it's a global score */
        private readonly bool $allowNonAutoCalculatedValue,
    ) {
        if (is_int($scale)) {
            Assert::nullOrRange($scale, InputInterface::DEFAULT_MIN_SCALE, InputInterface::DEFAULT_MAX_SCALE);
        }

        $this->minScore = is_float($minScore) ? $minScore : null;
        $this->maxScore = is_float($maxScore) ? $maxScore : null;
        $this->passScore = is_float($passScore) ? $passScore : null;
        $this->roundingMode = is_int($roundingMode) ? RoundingModes::extractAllowedMode($roundingMode) : null;
        $this->scale = is_int($scale) ? $scale : null;
    }

    /**
     * @phpstan-param ?RoundingModes::ROUND_* $roundingMode
     */
    public static function fromParameters(
        ?float $minScore = null,
        ?float $maxScore = null,
        ?float $passScore = null,
        ?int $roundingMode = null,
        ?int $scale = null,
        bool $showAutoCalculateBtn = self::DEFAULT_SHOW_AUTO_CALCULATE_BTN,
        bool $allowNonAutoCalculatedValue = self::DEFAULT_ALLOW_NON_AUTO_CALCULATED_VALUE,
    ): self {
        return new self(
            $minScore,
            $maxScore,
            $passScore,
            $roundingMode,
            $scale,
            $showAutoCalculateBtn,
            $allowNonAutoCalculatedValue
        );
    }

    /**
     * @phpstan-param RoundingModes::ROUND_* $roundingMode
     */
    public static function fromParametersRequiredForValidScoring(
        float $minScore,
        float $maxScore,
        int $roundingMode,
        int $scale,
        bool $showAutoCalculateBtn,
        bool $allowNonAutoCalculatedValue,
        ?float $customPassScore = null,
    ): self {
        Assert::greaterThan($maxScore, $minScore, "invalid maximum score $maxScore. must be greater than minimum score $minScore");
        Assert::range(
            $scale,
            InputInterface::DEFAULT_MIN_SCALE,
            InputInterface::DEFAULT_MAX_SCALE,
            "invalid scale $scale. must be between ".((string) InputInterface::DEFAULT_MIN_SCALE).' and '.((string) InputInterface::DEFAULT_MAX_SCALE)
        );
        Assert::nullOrRange(
            $customPassScore,
            $minScore,
            $maxScore,
            "invalid custom pass score $customPassScore. must be between minimum score ".((string) $minScore).' and maximum score '.((string) $maxScore)
        );

        return new self(
            $minScore,
            $maxScore,
            $customPassScore,
            $roundingMode,
            $scale,
            $showAutoCalculateBtn,
            $allowNonAutoCalculatedValue
        );
    }

    public static function fromJsonDecodedArray(array $data): self
    {
        return new self(
            $data[self::KEY_MIN_SCORE] ?? null,
            $data[self::KEY_MAX_SCORE] ?? null,
            $data[self::KEY_PASS_SCORE] ?? null,
            $data[self::KEY_ROUNDING_MODE] ?? null,
            $data[self::KEY_SCALE] ?? null,
            $data[self::KEY_SHOW_AUTO_CALCULATE_BTN] ?? self::DEFAULT_SHOW_AUTO_CALCULATE_BTN,
            $data[self::KEY_ALLOW_NON_AUTO_CALCULATED_VALUE] ?? self::DEFAULT_ALLOW_NON_AUTO_CALCULATED_VALUE
        );
    }

    public function hasMinScore(): bool
    {
        return is_float($this->minScore);
    }

    public function getMinScore(): float
    {
        if (!is_float($this->minScore)) {
            throw new \BadMethodCallException('no min score set');
        }

        return $this->minScore;
    }

    public function hasMaxScore(): bool
    {
        return is_float($this->maxScore);
    }

    public function getMaxScore(): float
    {
        if (!is_float($this->maxScore)) {
            throw new \BadMethodCallException('no max score set');
        }

        return $this->maxScore;
    }

    public function hasCustomPassScore(): bool
    {
        return is_float($this->passScore);
    }

    public function getCustomPassScore(): float
    {
        if (!is_float($this->passScore)) {
            throw new \BadMethodCallException('no custom pass score set');
        }

        return $this->passScore;
    }

    public function hasDerivablePassScore(): bool
    {
        return $this->hasMinScore() && $this->hasMaxScore() && $this->hasScale() && $this->hasRoundingMode();
    }

    public function getDerivablePassScore(): float
    {
        if (!$this->hasDerivablePassScore()) {
            throw new \BadMethodCallException('not able to derive a pass score');
        }

        $half = $this->getMinScore() + (($this->getMaxScore() - $this->getMinScore()) / 2.0);

        return round($half, $this->getScale(), $this->getRoundingMode());
    }

    public function hasPassScore(): bool
    {
        return $this->hasCustomPassScore() || $this->hasDerivablePassScore();
    }

    public function getPassScore(): float
    {
        if (!$this->hasPassScore()) {
            throw new \BadMethodCallException('no pass score set or able to derive from min & max scores');
        }

        if ($this->hasCustomPassScore()) {
            return $this->getCustomPassScore();
        }

        return $this->getDerivablePassScore();
    }

    public function hasRoundingMode(): bool
    {
        return is_int($this->roundingMode);
    }

    /**
     * @phpstan-return RoundingModes::ROUND_* $roundingMode
     */
    public function getRoundingMode(): int
    {
        if (!is_int($this->roundingMode)) {
            throw new \BadMethodCallException('no rounding mode set');
        }

        return $this->roundingMode;
    }

    public function hasScale(): bool
    {
        return is_int($this->scale);
    }

    public function getScale(): int
    {
        if (!is_int($this->scale)) {
            throw new \BadMethodCallException('no scale set');
        }

        return $this->scale;
    }

    public function showAutoCalculateBtn(): bool
    {
        return $this->showAutoCalculateBtn;
    }

    public function allowNonAutoCalculatedValue(): bool
    {
        return $this->allowNonAutoCalculatedValue;
    }

    public function makesScoringPossible(): bool
    {
        // base conditions
        if (!$this->hasValidScoringExtrema() || !$this->hasScale() || !$this->hasRoundingMode()) {
            return false;
        }

        // no custom pass score, means no check required
        if (!$this->hasCustomPassScore()) {
            return true;
        }

        // if set, custom score must be within the valid extrema range
        return $this->getCustomPassScore() >= $this->getMinScore() && $this->getCustomPassScore() <= $this->getMaxScore();
    }

    public function hasScoringExtrema(): bool
    {
        return $this->hasMinScore() && $this->hasMaxScore();
    }

    public function hasValidScoringExtrema(): bool
    {
        return $this->hasScoringExtrema() && $this->getMaxScore() > $this->getMinScore();
    }

    public function getKeysOfParametersMissingForValidScoring(): array
    {
        $keys = [];

        if (!$this->hasMinScore()) {
            $keys[] = self::KEY_MIN_SCORE;
        }

        if (!$this->hasMaxScore()) {
            $keys[] = self::KEY_MAX_SCORE;
        }

        if (!$this->hasScale()) {
            $keys[] = self::KEY_SCALE;
        }

        if (!$this->hasRoundingMode()) {
            $keys[] = self::KEY_ROUNDING_MODE;
        }

        return $keys;
    }

    /**
     * will given scoring parameters instance calculate the same way as this instance.
     * note: (custom) passScore is not important for calculating, only for displaying purposes
     * note: showing AutoCalculateBtn & allowing NonAutoCalculatedValue only important for global score questions.
     */
    public function matchesCalculatingAbilityWith(
        self $scoringParameters,
        bool $mustMatchOnShowAutoCalculateBtn,
        bool $mustMatchOnAllowNonAutoCalculatedValue,
        bool $mustMatchOnPassScore,
    ): bool {
        $thisMinScore = $this->hasMinScore() ? $this->getMinScore() : null;
        $targetMinScore = $scoringParameters->hasMinScore() ? $scoringParameters->getMinScore() : null;

        if ($thisMinScore !== $targetMinScore) {
            return false;
        }

        $thisMaxScore = $this->hasMaxScore() ? $this->getMaxScore() : null;
        $targetMaxScore = $scoringParameters->hasMaxScore() ? $scoringParameters->getMaxScore() : null;

        if ($thisMaxScore !== $targetMaxScore) {
            return false;
        }

        $thisRoundingMode = $this->hasRoundingMode() ? $this->getRoundingMode() : null;
        $targetRoundingMode = $scoringParameters->hasRoundingMode() ? $scoringParameters->getRoundingMode() : null;

        if ($thisRoundingMode !== $targetRoundingMode) {
            return false;
        }

        $thisScale = $this->hasScale() ? $this->getScale() : null;
        $targetScale = $scoringParameters->hasScale() ? $scoringParameters->getScale() : null;

        if ($thisScale !== $targetScale) {
            return false;
        }

        if ($mustMatchOnShowAutoCalculateBtn && ($this->showAutoCalculateBtn() !== $scoringParameters->showAutoCalculateBtn())) {
            return false;
        }

        if ($mustMatchOnAllowNonAutoCalculatedValue && ($this->allowNonAutoCalculatedValue() !== $scoringParameters->allowNonAutoCalculatedValue())) {
            return false;
        }

        if ($mustMatchOnPassScore) {
            $thisPassScore = $this->hasPassScore() ? $this->getPassScore() : null;
            $targetPassScore = $scoringParameters->hasPassScore() ? $scoringParameters->getPassScore() : null;

            if ($thisPassScore !== $targetPassScore) {
                return false;
            }
        }

        return true;
    }

    public function jsonSerialize(): array
    {
        return [
            self::KEY_MIN_SCORE => $this->hasMinScore() ? $this->getMinScore() : null,
            self::KEY_MAX_SCORE => $this->hasMaxScore() ? $this->getMaxScore() : null,
            self::KEY_PASS_SCORE => $this->hasCustomPassScore() ? $this->getCustomPassScore() : null,
            self::KEY_ROUNDING_MODE => $this->hasRoundingMode() ? $this->getRoundingMode() : null,
            self::KEY_SCALE => $this->hasScale() ? $this->getScale() : null,
            self::KEY_SHOW_AUTO_CALCULATE_BTN => $this->showAutoCalculateBtn(),
            self::KEY_ALLOW_NON_AUTO_CALCULATED_VALUE => $this->allowNonAutoCalculatedValue(),
        ];
    }
}
