<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Scoring\GlobalScoring;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\CalculationDependencyConfigCollection;
use Webmozart\Assert\Assert;

/**
 * Class CalculationDependencyConfig.
 *
 * contains configuration for a referenced (score) dependency (referencing a @see OperableForScoring::operatesAsScoreNode()
 * for usage in weighted calculation of a value for a global score @see OperableForScoring::operatesAsGlobalScoreNode()
 *
 * NOTE: for performance reasons in serializing & de-serializing of the depending nodes needed to calculate
 * global score nodes, where those depending nodes are spread at will over the node tree to which both depending
 * nodes and global score nodes belong, a reference to the depending node is used to serialize & de-serialize
 * so that the heavy operation of looking up the actual depending nodes for a global score node, is done only
 * when needed. this mainly allows to improve performance of building a full tree structure from a rootNode in
 * a top-to-bottom way (one level of children at the time) without having to go back up the tree to find all the
 * - already de-serialized or not yet de-serialized - depending nodes each time a global score node is found
 * during the de-serializing process.
 *
 * NOTE: it is likely that more parameters will be added to this config.
 *
 * @see     CalculationDependencyConfigCollection
 * the reference to a node is used to build a @see CalculationScoreNodeConfig
 */
final class CalculationDependencyConfig implements \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_SCORE_NODE_UID = 'scoreNodeUid';
    /** @var string */
    public const KEY_WEIGHING = 'weighing';

    /** @var string reference to a scoring node */
    private readonly string $scoreNodeUid;
    private readonly float $weighing;

    private function __construct(string $scoreNodeUid, float $weighing)
    {
        Assert::stringNotEmpty($scoreNodeUid);
        Assert::greaterThanEq($weighing, 0.0);

        $this->scoreNodeUid = $scoreNodeUid;
        $this->weighing = $weighing;
    }

    public static function fromScoreNodeUidAndConfigParameters(string $scoreNodeUid, float $weighing): self
    {
        return new static($scoreNodeUid, $weighing);
    }

    public static function fromScoreNodeAndConfigParameters(ScorableInputNode $scoreNode, float $weighing): self
    {
        return new static($scoreNode->getUid(), $weighing);
    }

    public static function fromScoreNodeAndConfigParametersWithMaxScoreAsWeighing(ScorableInputNode $scoreNode): self
    {
        if (!$scoreNode->getInputScoringParameters()->hasMaxScore()) {
            throw new \InvalidArgumentException('no max score found on score node [uiud: '.$scoreNode->getUid().' label: '.$scoreNode->getNestedLabel($scoreNode->getInfo()->getLocalizedLabel()->getFallbackLocale()).'] to use as weighing!');
        }

        return new static($scoreNode->getUid(), $scoreNode->getInputScoringParameters()->getMaxScore());
    }

    public static function fromJsonDecodedArray(array $data): self
    {
        return new self(
            $data[self::KEY_SCORE_NODE_UID],
            $data[self::KEY_WEIGHING]
        );
    }

    public function getScoreNodeUid(): string
    {
        return $this->scoreNodeUid;
    }

    public function getWeighing(): float
    {
        return $this->weighing;
    }

    /**
     * @return array{scoreNodeUid: string, weighing: float}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_SCORE_NODE_UID => $this->getScoreNodeUid(),
            self::KEY_WEIGHING => $this->getWeighing(),
        ];
    }
}
