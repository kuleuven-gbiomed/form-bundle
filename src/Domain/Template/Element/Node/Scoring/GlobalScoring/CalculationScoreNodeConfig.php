<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Scoring\GlobalScoring;

use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\CalculationScoreNodeConfigCollection;

/**
 * Class CalculationScoreNodeConfig.
 *
 * composed object of a question node that can operate as a score node( @see OperableForScoring::meetsScoringRequirements() )
 * and its configuration (@see CalculationDependencyConfig ) where the composed object is used as a weighted dependency
 * to make a calculation of a global score possible for another @see OperableForScoring::operatesAsGlobalScoreNode()
 * somewhere in the same nodes tree
 *
 * the submitted value of the node is used as one required calculation dependency with the submitted values of other
 * such nodes, and weighted in the whole calculation according to the defined configuration to automatically
 * achieve a weighted total of each of those node's values.
 *
 * NOTE: this is a Value Object that - for reasons of performance during node tree de-serializing & building
 * and @see FormList updating purposes - is build on the fly when needed only,
 * based on a @see CalculationDependencyConfig which holds a reference to the node in this value object.
 * for more info on this see NOTE at @see CalculationDependencyConfig
 *
 * @see     OperableForScoring::getValidGlobalScoreCalculationScoreNodeConfigs()
 * @see     CalculationScoreNodeConfigCollection
 */
class CalculationScoreNodeConfig
{
    private readonly OperableForScoring $scoreNode;
    private readonly CalculationDependencyConfig $config;

    /**
     * @throws \InvalidArgumentException
     */
    final private function __construct(OperableForScoring $scoreNode, CalculationDependencyConfig $config)
    {
        if ($scoreNode->getUid() !== $config->getScoreNodeUid()) {
            throw new \InvalidArgumentException('the reference ['.$config->getScoreNodeUid().'] to a score node in the given dependency configuration does not point to the given score node ['.$scoreNode->getUid().']');
        }

        $this->scoreNode = $scoreNode;
        $this->config = $config;
    }

    public static function fromScoreNodeAndConfig(
        OperableForScoring $scoreNode,
        CalculationDependencyConfig $config,
    ): self {
        return new self($scoreNode, $config);
    }

    public function getScoreNode(): OperableForScoring
    {
        return $this->scoreNode;
    }

    public function getConfig(): CalculationDependencyConfig
    {
        return $this->config;
    }
}
