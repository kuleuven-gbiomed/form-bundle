<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Category;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\AbstractNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ChildrenAwareInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Factory\NodeFactory;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\AbstractReadOnlyViewNode;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\ChildrenTrait;
use KUL\FormBundle\Domain\Template\Utility\AbstractLocalizedInfo;
use KUL\FormBundle\Domain\Template\Utility\Collection\CategoryNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\ReadOnlyViewNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Class CategoryNode.
 *
 * configuration of a category allowing to group questions or/and other sub categories
 * with optional (grouping) label, description(s), etc.
 *
 * @implements ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>>
 */
final class CategoryNode extends AbstractNode implements ChildrenAwareInterface
{
    /**
     * @use ChildrenTrait<NodeInterface, NodeCollection<array-key, NodeInterface>>
     */
    use ChildrenTrait {
        updateChildren as traitUpdateChildren;
    }

    /** @var string */
    public const KEY_DEFAULT_FLOW_PERMISSION = 'default_flow_permission';

    /** @var string */
    public const DISPLAY_MODE_DEFAULT = 'default';
    /** @var string */
    public const DISPLAY_MODE_MATRIX = 'matrix';

    /** @var string */
    public const KEY_DISPLAY_MODE = 'displayMode';

    /** @var string */
    public const KEY_FIRST_HEADER_CELL_TITLE = 'firstHeaderCellTitle';

    /** @var NodeCollection<array-key,NodeInterface> */
    private NodeCollection $children;

    private string $displayMode;

    /** @var InputNodeCollection<array-key,InputNode>|null */
    private ?InputNodeCollection $cachedFlattenedParentalInputNodeDescendants = null;
    private ?CategoryNodeCollection $cachedFlattenedCategoryNodeDescendants = null;
    private ?bool $cachedHasReachableParentalInputNodeDescendants = null;
    private ?bool $cachedHasReachableReadOnlyViewNodeDescendants = null;
    /** @var ?ReadOnlyViewNodeCollection<array-key,AbstractReadOnlyViewNode> */
    private ?ReadOnlyViewNodeCollection $cachedFlattenedReadOnlyViewNodeDescendants = null;

    /** @var string */
    public const TYPE = 'category';

    /**
     * AbstractNode constructor.
     *
     * WARNING: we do NOT use flowPermission to update the flowPermission on descendants in this constructor.
     * it is meant for admin-side only, to help admins to have some default flowPermission to set on all descendants
     * before they (optionally) change the flowPermission on the individual descendants (questions and sub categories)
     * if we were to use this to @see CategoryNode::updateAllDescendantsWithDefaultFlowPermission() here in the
     * constructor, then on every de-serializing of this category via @see CategoryNode::fromJsonDecodedArray() in
     * the client (end-user) side, the individual flowPermissions would be overwritten! so it is simply set here
     * and it is up to the admin (via developer providing the option) to use it to update the descendants
     *
     * argument $defaultFlowPermission:
     * FlowPermission a category node has no flowPermission of its own. it relies on children for that. but in the
     * admin-side, when adding a new question to the category (or a new child category), having a default flowPermission
     * comes in hand for admins to pre-fill that's descendants flowPermission with this default.
     *
     * argument $firstHeaderCellTitle:
     * In case the Matrix display mode is selected, this label can optionally be used for the first cell in the header.
     *
     * @param NodeCollection<array-key,NodeInterface> $children
     */
    protected function __construct(
        string $uid,
        LocalizedInfoWithOptionalLabel $info,
        bool $hidden,
        NodeCollection $children,
        private FlowPermission $defaultFlowPermission,
        string $displayMode,
        private readonly LocalizedOptionalString $firstHeaderCellTitle,
    ) {
        parent::__construct($uid, $info, $hidden);

        $this->updateChildren($children);

        $this->updateDisplayMode($displayMode);
    }

    /**
     * @param NodeCollection<array-key,NodeInterface> $children
     *
     * @throws LocalizedStringException
     */
    public static function fromUidAndDependencies(
        string $uid,
        LocalizedInfoWithOptionalLabel $info,
        bool $hidden,
        NodeCollection $children,
        FlowPermission $defaultFlowPermission,
        string $displayMode = self::DISPLAY_MODE_DEFAULT,
        ?LocalizedOptionalString $firstHeaderCellTitle = null,
    ): self {
        $firstHeaderCellTitle = $firstHeaderCellTitle instanceof LocalizedOptionalString ? $firstHeaderCellTitle : LocalizedOptionalString::createEmpty();

        return new self($uid, $info, $hidden, $children, $defaultFlowPermission, $displayMode, $firstHeaderCellTitle);
    }

    /**
     * create a non-hidden category node with just the uid, without any of the optional info and without any children
     * as a way to 'initialize' the node before updating it further.
     *
     * NOTE: updating of the node after using this factory might be required to make it actually 'usable',
     * since only the bare minimum is created to avoid this node to fail upon construct. e.g. without children,
     * this category node will never be shown/rendered.
     *
     * @throws LocalizedStringException
     */
    public static function fromUidAndMinimalDependencies(string $uid): self
    {
        return new self(
            $uid,
            LocalizedInfoWithOptionalLabel::createEmpty(),
            false,
            NodeCollection::createEmpty(),
            FlowPermission::createEmpty(),
            self::DISPLAY_MODE_DEFAULT,
            LocalizedOptionalString::createEmpty()
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws InputInvalidArgumentException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $children = [];
        foreach ($data[self::KEY_CHILDREN] as $childData) {
            $type = $childData[NodeInterface::KEY_TYPE];
            if (self::TYPE === $type) {
                $children[] = self::fromJsonDecodedArray($childData);
                continue;
            }

            if (str_starts_with((string) $type, InputNode::TYPE_PREFIX)) {
                $children[] = NodeFactory::createInputNodeFromJsonDecodedArray($childData);
                continue;
            }

            if (str_starts_with((string) $type, AbstractReadOnlyViewNode::TYPE_PREFIX)) {
                $children[] = NodeFactory::createReadOnlyViewNodeFromJsonDecodedArray($childData);
                continue;
            }

            throw new \InvalidArgumentException('cannot build children for Category: unknown value ['.$type.'] found for "type" in nested jsonDecodedArray data.');
        }

        $flowPermission = FlowPermission::createEmpty();
        if (isset($data[self::KEY_DEFAULT_FLOW_PERMISSION])) {
            $flowPermission = FlowPermission::fromJsonDecodedArray($data[self::KEY_DEFAULT_FLOW_PERMISSION]);
        }

        $displayMode = self::DISPLAY_MODE_DEFAULT;
        if (isset($data[self::KEY_DISPLAY_MODE])) {
            $displayMode = $data[self::KEY_DISPLAY_MODE];
        }

        $firstHeaderCellTitle = $data[self::KEY_FIRST_HEADER_CELL_TITLE] ?? [];

        /** @var NodeCollection<array-key,NodeInterface> $childrenCollection */
        $childrenCollection = new NodeCollection($children);

        return new self(
            $data[self::KEY_UID],
            LocalizedInfoWithOptionalLabel::fromJsonDecodedArray($data[self::KEY_INFO]),
            $data[self::KEY_HIDDEN],
            $childrenCollection, // @phan-suppress-current-line PhanTypeMismatchArgumentProbablyReal
            $flowPermission,
            $displayMode,
            LocalizedOptionalString::fromJsonDecodedArray($firstHeaderCellTitle)
        );
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    /**
     * overwritten for type hinting purposes.
     */
    public function getInfo(): LocalizedInfoWithOptionalLabel
    {
        /** @var LocalizedInfoWithOptionalLabel $result */
        $result = parent::getInfo();

        return $result;
    }

    /**
     * no method overloading, can't type hint argument on implementation and the pass to parent: explicit implementation assert.
     */
    public function updateInfo(AbstractLocalizedInfo $info): void
    {
        if (!($info instanceof LocalizedInfoWithOptionalLabel)) {
            throw new \InvalidArgumentException('expect info to be instance of '.LocalizedInfoWithOptionalLabel::class.' got '.$info::class);
        }

        parent::updateInfo($info);
    }

    /**
     * CategoryNodes do not declare flowPermission themselves
     * to have write access, at least one input node descendant must have write access granted.
     *
     * IMPORTANT: this does not care or take in account if the node is @see NodeInterface::isReachableInTree() or not
     */
    public function isStepRoleWriteAccessGranted(StepInterface $step, Role $role): bool
    {
        return $this->getFlattenedParentalInputNodeDescendants()->hasWritableForRoleInStep($role, $step);
    }

    /**
     * CategoryNodes do not declare flowPermission themselves
     * to have read access, at least one input or radarChart node descendant must have explicit read access granted
     * (input or radarChart node) or indirectly grant read access by granting write access (input node only) .
     *
     * IMPORTANT: this does not care or take in account if the node is @see NodeInterface::isReachableInTree() or not
     */
    public function isStepRoleReadAccessGranted(StepInterface $step, Role $role): bool
    {
        return $this->getFlattenedParentalInputNodeDescendants()->hasReadableForRoleInStep($role, $step)
            || $this->getFlattenedReadOnlyViewNodeDescendants()->hasReadableForRoleInStep($role, $step);
    }

    /**
     * IMPORTANT: this does not care or take in account if the node is @see NodeInterface::isReachableInTree() or not.
     */
    public function isStepRoleReadOnlyAccessGranted(StepInterface $step, Role $role): bool
    {
        return $this->getFlattenedParentalInputNodeDescendants()->hasOnlyReadableForRoleInStep($role, $step)
            || $this->getFlattenedReadOnlyViewNodeDescendants()->hasOnlyReadableForRoleInStep($role, $step);
    }

    /**
     * update the flowPermission access for ALL descendants in this category, meaning all questions
     * in this category and all questions in all it's nested sub categories (if any) get the same flowPermission access.
     *
     * this helps to quickly set same access for all questions. if one questions has to have a different access,
     * then the admin can update the flowPermission directly on specific question (@see InputNode::updateFlowPermission()
     * after having used this method to set/update the flowPermission access on all the category's descending questions.
     *
     * NOTE: a category has no explicit access flowPermission. it's basically just for (UX) grouping of questions.
     * the access to the category is determined via its descending questions (@see ParentalInputNode )
     *
     * note: readOnlyView node descendants like radar chart nodes cannot have write access and this is asserted upon their construct.
     * domain decision is to silently remove write access from given (usually the default) permission so the construct
     * of any readOnlyView node descendants won't fail hard. there is no harm in doing this silently: this bulk updating
     * of all descendants with a (default) flowPermission is used in admin side to automatically (re)set (i.e. overwrite)
     * the flowPermission on each of this category's descendants as a convenient bulk permission updater. After this update,
     * either further fine-tuning of flowPermission on individual descendants might be needed, or it is intentional that all
     * descendants keep same flowPermission. throwing errors because readOnlyView node don't accept write
     * access would not be convenient at all: it would defeat the purpose of bulk (re)setting this category's descendants.
     * skipping updating of readOnlyView nodes would also be confusing. The chance that this silent removing
     * of write access for readOnlyView nodes would result in some roles having no access anymore is nada: permissions on
     * questions are left untouched and write access always also grants read access, so the readonly nodes will still be
     * granting the access the admin has intended, but just as read-only access.
     */
    public function updateAllDescendantsWithFlowPermission(FlowPermission $flowPermission): void
    {
        // get a duplicate without write access (yet keeps any read access), in case we have readOnly view (e.g. radar chart) descendants
        $noWriteFlowPermission = $flowPermission->withWritePermissionsRevokedForAllRolesInAllSteps();

        /** @var NodeInterface $child */
        foreach ($this->getChildren() as $child) {
            // go deep by using this method recursively on the nested sub categories
            if ($child instanceof self) {
                $child->updateAllDescendantsWithFlowPermission($flowPermission);
            }

            // only update actual questions. the choice question will handle the update of its child option nodes
            if ($child instanceof InputNode && $child instanceof ParentalInputNode) {
                $child->updateFlowPermission($flowPermission);
            }

            if ($child instanceof AbstractReadOnlyViewNode) {
                $child->updateFlowPermission($noWriteFlowPermission);
            }
        }
    }

    /**
     * a category node has no flowPermission of its own. for access (flowPermission), it relies on
     * its descending questions (@see InputNode ) to check and derive access.
     * but in the admin-side, when adding a new question to the category (or a new child category),
     * having a default flowPermission comes in hand for admins to pre-fill that's descendants flowPermission
     * with this default.
     *
     * WARNING: do NOT use this to check permission access, especially not in the client (end-users) side! this is to help
     * pre set question access in admin-side only! to check real access @see CategoryNode::isStepRoleWriteAccessGranted()
     * and @see CategoryNode::isStepRoleReadAccessGranted() . exactly the same reason why this default flowPermission
     * is not immediately used in the constructor to update the descendants' flowPermissions
     */
    public function getDefaultFlowPermission(): FlowPermission
    {
        return $this->defaultFlowPermission;
    }

    /**
     * WARNING: this just updates the default flowPermission for usage in admin-side. it does NOT automatically update
     * the flowPermissions of any descendants of this category, so that no individual custom flowPermissions are
     * overwritten.
     *
     * use @see updateAllDescendantsWithDefaultFlowPermission() if you want to updtae descendants with this default
     */
    public function updateDefaultFlowPermission(FlowPermission $flowPermission): void
    {
        $this->defaultFlowPermission = $flowPermission;
    }

    /**
     * update the flowPermission access for ALL descendants in this category with the default flowPermission.
     */
    public function updateAllDescendantsWithDefaultFlowPermission(): void
    {
        $this->updateAllDescendantsWithFlowPermission($this->getDefaultFlowPermission());
    }

    /**
     * returns a collection containing all nested input node descendants, excluding those input nodes that are children
     * from a parental input node and regardless of reachability of each added node.
     *
     * @return InputNodeCollection<array-key,InputNode>
     */
    public function getFlattenedParentalInputNodeDescendants(): InputNodeCollection
    {
        if (null === $this->cachedFlattenedParentalInputNodeDescendants) {
            $this->cachedFlattenedParentalInputNodeDescendants = new InputNodeCollection($this->getFlattenedParentalInputNodeDescendantsArray());
        }

        return $this->cachedFlattenedParentalInputNodeDescendants;
    }

    public function getFlattenedParentalInputNodeDescendantsArray(): array
    {
        $childLists = [];

        foreach ($this->getChildren() as $child) {
            if ($child instanceof InputNode && $child instanceof ParentalInputNode) {
                $childLists[] = [$child];
                continue;
            }

            if (!$child instanceof self) {
                continue;
            }

            $childDescendants = $child->getFlattenedParentalInputNodeDescendantsArray();
            if (0 !== count($childDescendants)) {
                $childLists[] = $childDescendants;
            }
        }

        // using one time array_merge with variadic args is/should be more performant than calling array_merge
        // in above foreach on each of its elements: reduces total amount of array_merge calls in total recursive result
        return array_merge(...$childLists);
    }

    /** @return ReadOnlyViewNodeCollection<array-key, AbstractReadOnlyViewNode> */
    public function getFlattenedReadOnlyViewNodeDescendants(): ReadOnlyViewNodeCollection
    {
        if (null === $this->cachedFlattenedReadOnlyViewNodeDescendants) {
            $this->cachedFlattenedReadOnlyViewNodeDescendants = new ReadOnlyViewNodeCollection($this->getFlattenedReadOnlyViewNodeDescendantsArray());
        }

        return $this->cachedFlattenedReadOnlyViewNodeDescendants;
    }

    public function getFlattenedReadOnlyViewNodeDescendantsArray(): array
    {
        $childLists = [];

        foreach ($this->getChildren() as $child) {
            if ($child instanceof AbstractReadOnlyViewNode) {
                $childLists[] = [$child];
                continue;
            }

            if (!$child instanceof self) {
                continue;
            }

            $childDescendants = $child->getFlattenedReadOnlyViewNodeDescendantsArray();
            if (0 !== count($childDescendants)) {
                $childLists[] = $childDescendants;
            }
        }

        // using one time array_merge with variadic args is/should be more performant than calling array_merge
        // in above foreach on each of its elements: reduces total amount of array_merge calls in total recursive result
        return array_merge(...$childLists);
    }

    /**
     * returns a collection containing all nested category node descendants regardless of reachability of each added node.
     *
     * Starting with PHP 8.1 there is support for unpacking associative arrays,
     * see: https://wiki.php.net/rfc/array_unpacking_string_keys.
     * Until then with have to use an array_values().
     */
    public function getFlattenedCategoryNodeDescendants(): CategoryNodeCollection
    {
        if (null === $this->cachedFlattenedCategoryNodeDescendants) {
            $this->cachedFlattenedCategoryNodeDescendants = new CategoryNodeCollection($this->getFlattenedCategoryNodeDescendantsArray());
        }

        return $this->cachedFlattenedCategoryNodeDescendants;
    }

    public function getFlattenedCategoryNodeDescendantsArray(): array
    {
        $childLists = [];

        foreach ($this->getChildren() as $child) {
            if (!$child instanceof self) {
                continue;
            }

            // add the child category
            $childLists[] = [$child];
            // if child has category descendants, process recursively so it gets added with all its descendants
            $childDescendants = $child->getFlattenedCategoryNodeDescendantsArray();
            if (0 !== count($childDescendants)) {
                $childLists[] = $childDescendants;
            }
        }

        // using one time array_merge with variadic args is/should be more performant than calling array_merge
        // in above foreach on each of its elements: reduces total amount of array_merge calls in total recursive result
        return array_merge(...$childLists);
    }

    /**
     * @param NodeCollection<array-key,NodeInterface> $children
     */
    protected function updateChildren(NodeCollection $children): void
    {
        // important! need to reset all locally cached collections with (filtered) children
        $this->cachedFlattenedCategoryNodeDescendants = null;
        $this->cachedFlattenedParentalInputNodeDescendants = null;
        $this->cachedHasReachableParentalInputNodeDescendants = null;
        $this->cachedHasReachableReadOnlyViewNodeDescendants = null;
        $this->cachedFlattenedReadOnlyViewNodeDescendants = null;

        // mark this parental choice question node as parent on each of the option children
        $this->traitUpdateChildren($children);
    }

    /**
     * @return NodeCollection<array-key,NodeInterface>
     */
    protected static function createChildCollection(array $children = []): NodeCollection
    {
        return new NodeCollection($children);
    }

    /**
     * @throws MissingNodeException
     */
    public function hasAncestorWithDisplayModeMatrix(): bool
    {
        if ($this->isTreeRootNode()) {
            return false;
        }

        /** @var CategoryNode $parentNode */
        $parentNode = $this->getParent();

        return self::DISPLAY_MODE_MATRIX === $parentNode->getDisplayMode();
    }

    public function getFlattenedWithAllDescendants(): NodeCollection
    {
        if (null === $this->cachedFlattenedNodeDescendants) {
            $this->cachedFlattenedNodeDescendants = new NodeCollection($this->getFlattenedWithAllDescendantsArray());
        }

        return $this->cachedFlattenedNodeDescendants;
    }

    /**
     * check CategoryNode on reachable ((in)directly hidden) nested InputNode descendants, while ignoring and skipping
     * the children of any the found inputNodes.
     *
     * NOTE: allows to quickly check if further deeper looping is meaningful when building forms / rendering (partial)
     * views. considers only InputNodes that are direct children of this CategoryNode or nested children from
     * child-CategoryNodes and without checking any children that those nested parental inputNodes might have (children
     * from (parental) InputNodes can be hidden themselves but that is only of concern and responsibility of the
     * parental inputNode.
     * IMPORTANT: using @see getFlattenedParentalInputNodeDescendants to check each item recursively via
     * the @see NodeInterface::isReachableInTree() method (which does an up-the-tree check) is way more a perfomance
     * burden than a custom down-the-tree check with quick return on first hit, mainly on repeated usage of this method.
     */
    public function hasReachableParentalInputNodeDescendants(): bool
    {
        if (null === $this->cachedHasReachableParentalInputNodeDescendants) {
            $this->cachedHasReachableParentalInputNodeDescendants = false;

            // category is marked hidden, then all its descendants are considered unreachable
            if (!$this->isHidden()) {
                // get & check direct children not marked as hidden (skipping those explicitly marked as hidden)
                foreach ($this->getActiveChildren() as $child) {
                    if ($child instanceof ParentalInputNode) {
                        $this->cachedHasReachableParentalInputNodeDescendants = true;
                        // no need to check further, found one non-hidden child,  thus category is not hidden
                        break;
                    }

                    if ($child instanceof self && $child->hasReachableParentalInputNodeDescendants()) {
                        $this->cachedHasReachableParentalInputNodeDescendants = true;
                        // no need to check further, found one non-hidden descendant, thus category is not hidden
                        break;
                    }
                }
            }
        }

        return $this->cachedHasReachableParentalInputNodeDescendants;
    }

    public function hasReachableReadOnlyViewNodeDescendants(): bool
    {
        if (null === $this->cachedHasReachableReadOnlyViewNodeDescendants) {
            $this->cachedHasReachableReadOnlyViewNodeDescendants = false;

            // category is marked hidden, then all its descendants are considered unreachable
            if (!$this->isHidden()) {
                // get & check direct children not marked as hidden (skipping those explicitly marked as hidden)
                foreach ($this->getActiveChildren() as $child) {
                    if ($child instanceof AbstractReadOnlyViewNode) {
                        $this->cachedHasReachableReadOnlyViewNodeDescendants = true;
                        // no need to check further, found one non-hidden child
                        break;
                    }

                    if ($child instanceof self && $child->hasReachableReadOnlyViewNodeDescendants()) {
                        $this->cachedHasReachableReadOnlyViewNodeDescendants = true;
                        // no need to check further, found one non-hidden descendant
                        break;
                    }
                }
            }
        }

        return $this->cachedHasReachableReadOnlyViewNodeDescendants;
    }

    public function hasReachableNonCategoryDescendants(): bool
    {
        return $this->hasReachableParentalInputNodeDescendants() || $this->hasReachableReadOnlyViewNodeDescendants();
    }

    public function hasReadAccessibleNonCategoryDescendantsToRenderInStepForRole(
        StepInterface $step,
        Role $role,
        array $formValues,
    ): bool {
        return $this->getFlattenedParentalInputNodeDescendants()->hasReadableForRoleInStep($role, $step)
            || $this->getFlattenedReadOnlyViewNodeDescendants()->hasAccessibleAndRenderableFor($step, $role, $formValues);
    }

    /** @return NodeCollection<array-key,NodeInterface> */
    public function getChildren(): NodeCollection
    {
        return $this->children;
    }

    public function getDisplayMode(): string
    {
        return $this->displayMode;
    }

    public function updateDisplayMode(string $displayMode): void
    {
        if ('' === $displayMode) {
            throw new \InvalidArgumentException('parameter displayMode must be a non empty string');
        }

        if (!in_array($displayMode, [self::DISPLAY_MODE_DEFAULT, self::DISPLAY_MODE_MATRIX], true)) {
            throw new \InvalidArgumentException('expected parameter displayMode to be one of the following: '.implode(' | ', [self::DISPLAY_MODE_DEFAULT, self::DISPLAY_MODE_MATRIX]).'. got '.$displayMode);
        }

        $this->displayMode = $displayMode;
    }

    public function getLocalizedFirstHeaderCellTitle(): LocalizedOptionalString
    {
        return $this->firstHeaderCellTitle;
    }

    /**
     * @throws LocalizedStringException if no valid locale string given
     */
    public function hasFirstHeaderCellTitle(string $locale): bool
    {
        return $this->getLocalizedFirstHeaderCellTitle()->hasForLocaleOrFallback($locale);
    }

    /**
     * @throws LocalizedStringException
     */
    public function getFirstHeaderCellTitle(string $locale): string
    {
        return $this->firstHeaderCellTitle->getForLocaleOrFallback($locale);
    }

    /**
     * This function is used to find out (on a category with matrix mode) if a column should be shown or not
     * We assume that the matrix is valid.
     */
    public function getVisibleCols(StepInterface $step, Role $role): array
    {
        if (self::DISPLAY_MODE_DEFAULT === $this->getDisplayMode()) {
            throw new \BadMethodCallException("Can't call this method on a node that doesn't have displaymode matrix.");
        }

        $subcategories = $this->getChildren();
        $firstSubcategory = $subcategories->first();
        if (!$firstSubcategory instanceof self) {
            return [];
        }
        $visibleCols = array_fill(0, $firstSubcategory->getChildren()->count(), false);

        foreach ($subcategories as $subcategory) {
            if ($subcategory instanceof self) {
                $questions = $subcategory->getChildren()->toArray();
                /** @var array-key $col */
                foreach (array_keys($visibleCols, false, true) as $col) {
                    if ($questions[$col] instanceof InputNode) {
                        // $questions will only have InputNodes because subcategories of matrixes may ony have InputNodes as children
                        $visibleCols[$col] = $questions[$col]->isStepRoleReadAccessGranted($step, $role);
                    }
                }
            }
        }

        return $visibleCols;
    }

    /**
     * @return array{
     *     uid: string,
     *     type: string,
     *     info: LocalizedInfoWithOptionalLabel,
     *     hidden: bool,
     *     children: NodeCollection<array-key,NodeInterface>,
     *     default_flow_permission: FlowPermission,
     *     displayMode: string,
     *     firstHeaderCellTitle: LocalizedOptionalString
     * }
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_UID => $this->getUid(),
            self::KEY_TYPE => static::getType(),
            self::KEY_INFO => $this->getInfo(),
            self::KEY_HIDDEN => $this->isHidden(),
            self::KEY_CHILDREN => $this->getChildren(),
            self::KEY_DEFAULT_FLOW_PERMISSION => $this->getDefaultFlowPermission(),
            self::KEY_DISPLAY_MODE => $this->getDisplayMode(),
            self::KEY_FIRST_HEADER_CELL_TITLE => $this->getLocalizedFirstHeaderCellTitle(),
        ];
    }

    public function isLocked(): bool
    {
        // a category is locked if all its nested (thus including questions in subcategories) questions are locked
        return $this
            ->getChildren()
            ->filter(fn (NodeInterface $node) => !(($node instanceof ParentalInputNode || $node instanceof self) && $node->isLocked()))
            ->isEmpty();
    }
}
