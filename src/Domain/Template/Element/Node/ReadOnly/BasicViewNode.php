<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly;

use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel;

/**
 * This type of node is pure display. The description field is used to display HTML in the node tree.
 */
class BasicViewNode extends AbstractReadOnlyViewNode
{
    /** @var string */
    public const TYPE = 'basic';

    public function __construct(
        string $uid,
        FlowPermission $flowPermission,
        LocalizedInfoWithOptionalLabel $info,
        string $displayMode,
        bool $hidden,
    ) {
        self::guardDisplayMode($displayMode);
        parent::__construct($uid, $flowPermission, $info, $displayMode, $hidden);
    }

    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        return new self(
            $data[self::KEY_UID],
            FlowPermission::fromJsonDecodedArray($data[self::KEY_FLOW_PERMISSION]),
            LocalizedInfoWithOptionalLabel::fromJsonDecodedArray($data[self::KEY_INFO]),
            $data[self::KEY_DISPLAY_MODE] ?? self::DISPLAY_MODE_REGULAR,
            $data[self::KEY_HIDDEN],
        );
    }

    public static function getType(): string
    {
        return self::TYPE_PREFIX.'_'.self::TYPE;
    }

    public function guardDisplayMode(string $displayMode): void
    {
        $modes = [self::DISPLAY_MODE_REGULAR];

        if (!in_array($displayMode, $modes, true)) {
            throw new \InvalidArgumentException('displayMode for radar chart node must be one of the following: '.implode(' | ', $modes).'. got '.$displayMode);
        }
    }

    public function jsonSerialize(): array
    {
        return [
            ...parent::jsonSerialize(),
            self::KEY_TYPE => self::getType(),
        ];
    }
}
