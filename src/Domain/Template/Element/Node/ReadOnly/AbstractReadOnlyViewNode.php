<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\AbstractNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\Exception\IllegalWriteAccessInReadOnlyViewNodeException;
use KUL\FormBundle\Domain\Template\Utility\AbstractLocalizedInfo;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * a read-only view node displays answers from (a set of) questions (@see InputNode) in some grouping/graphical manner
 * like pie charts, diagrams, etc. A read-only view node is not a question! users cannot change or submit the answers
 * from the questions displayed in it. Its intention is to allow users to view answers from a set of participating
 * questions in a UX friendly synoptic, overview and usually graphical element. any interaction by users on these nodes
 * is purely (client side) UX displayable, like e.g. reordering by a chart scale, axes, ..., never actually altering
 * answers in any way.
 *
 * Write access is useless, thus never granted but the read access is configurable in the same way and to the same extend
 * as it is for questions. read-only view nodes take up spots in a Template's tree-of-nodes in the same way as question &
 * category nodes do, as nestable children of category/categories, and are included in determining user-role-step
 * access (FlowPermission) to a form, in the same way as it is for question nodes.
 *
 * note: a read-only view node will never show temporary saved answers, only submitted values. This follows the logic of
 * the questions (as set by domain decision): in read mode, users never get to see temporary answers given for questions.
 * Only in write mode are temporary saved answers for questions used & shown (in form input fields). Since a read-only
 * view node never grants write access, temporary saved values from its participating questions are never used & shown.
 */
abstract class AbstractReadOnlyViewNode extends AbstractNode
{
    /** @var string */
    final public const TYPE_PREFIX = 'read_only_view';
    /** @var string */
    final public const KEY_FLOW_PERMISSION = 'flowPermission';
    /** @var string */
    final public const KEY_DISPLAY_MODE = 'displayMode';
    /** @var string */
    final public const DISPLAY_MODE_REGULAR = 'regular';

    private FlowPermission $flowPermission;

    protected function __construct(
        string $uid,
        FlowPermission $flowPermission,
        AbstractLocalizedInfo $info,
        private readonly string $displayMode,
        bool $hidden,
    ) {
        self::guardReadOnlyFlowPermission($flowPermission, $uid);

        parent::__construct($uid, $info, $hidden);

        $this->flowPermission = $flowPermission;
    }

    public function getDisplayMode(): string
    {
        return $this->displayMode;
    }

    final public function getFlowPermission(): FlowPermission
    {
        return $this->flowPermission;
    }

    public function updateFlowPermission(FlowPermission $flowPermission): void
    {
        self::guardReadOnlyFlowPermission($flowPermission, $this->getUid());

        $this->flowPermission = $flowPermission;
    }

    public function revokeFlowPermissionForRoleInAllSteps(Role $role): void
    {
        $this->updateFlowPermission($this->getFlowPermission()->withStepPermissionsRevokedForRole($role->getName()));
    }

    public function revokeFlowPermissionForRoleInStep(Role $role, StepInterface $step): void
    {
        $this->updateFlowPermission(
            $this->getFlowPermission()->withPermissionsRevokedForRoleInStepUid(
                $role->getName(),
                $step->getUid()
            )
        );
    }

    public function revokeFlowPermissionForAllRolesInAllSteps(): void
    {
        $this->updateFlowPermission(FlowPermission::createEmpty());
    }

    /**
     * write access is NEVER granted: read-only nodes (i.e.viewing results of questions) means only read access can be granted!
     * this method intentionally cannot be extended (do not remove the 'final' keyword for any reason!). the method arguments
     * are not used, but the method has to comply to the contract it implements.
     */
    final public function isStepRoleWriteAccessGranted(StepInterface $step, Role $role): bool
    {
        return false;
    }

    public function isStepRoleReadAccessGranted(StepInterface $step, Role $role): bool
    {
        // no point in additional checking for indirect read access via write access: write access is forcibly not allowed anyway
        return $this->isStepRoleReadOnlyAccessGranted($step, $role);
    }

    public function isStepRoleReadOnlyAccessGranted(StepInterface $step, Role $role): bool
    {
        return $this->getFlowPermission()->isStepRoleReadOnlyAccessGranted($step->getUid(), $role->getName());
    }

    private static function guardReadOnlyFlowPermission(FlowPermission $flowPermission, ?string $label = null): void
    {
        if ($flowPermission->isWriteAccessGrantedForAtLeastOneRoleInAtLeastOneStep()) {
            throw new IllegalWriteAccessInReadOnlyViewNodeException((is_string($label) ? '['.$label.']: ' : '').'a read only view node can not grant write access to any role in any step, but there is at least one role with write access in at least one step found.');
        }
    }

    public function jsonSerialize(): array
    {
        return [
            self::KEY_UID => $this->getUid(),
            self::KEY_FLOW_PERMISSION => $this->getFlowPermission(),
            self::KEY_INFO => $this->getInfo(),
            self::KEY_HIDDEN => $this->isHidden(),
            self::KEY_DISPLAY_MODE => $this->getDisplayMode(),
        ];
    }

    /**
     * @throws MissingNodeException
     */
    public function hasAncestorWithDisplayModeMatrix(): bool
    {
        return $this->getParent()->hasAncestorWithDisplayModeMatrix();
    }
}
