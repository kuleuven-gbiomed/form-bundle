<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\Exception\NotEnoughRadarChartParticipatingQuestionReferencesException;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart\ChartPlotData;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart\ParticipatingQuestionCollection;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart\ParticipatingQuestionEligibilityCollection;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart\ParticipatingQuestionReferenceCollection;
use KUL\FormBundle\Domain\Template\Utility\AbstractLocalizedInfo;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Role;

final class RadarChartViewNode extends AbstractReadOnlyViewNode
{
    /** @var string */
    public const TYPE = 'radar_chart';
    /** @var string */
    public const KEY_QUESTION_REFERENCES = 'question_references';
    /** @var string */
    public const KEY_IGNORE_INDIVIDUAL_QUESTION_RIGHTS = 'ignore_individual_question_rights';
    /** @var string */
    public const KEY_UNIT_SIGN = 'unit_sign';
    /** @var string */
    public const KEY_HIDE_POINT_VALUES = 'hide_point_values';
    /** @var string */
    public const KEY_HIDE_POINT_UNIT_SUFFIX = 'hide_point_unit_suffix';
    /** @var string */
    public const KEY_HIDE_SCALE_VALUES = 'hide_scale_values';
    /** @var string */
    public const KEY_HIDE_SCALE_UNIT_SUFFIX = 'hide_scale_unit_suffix';
    /** @var int */
    public const CHART_MIN_VALUE = 0;
    /** @var int */
    public const CHART_MAX_VALUE = 100;
    /** @var int */
    public const UNIT_SIGN_MIN_CHARS = 1;
    /** @var int sign usage affects chart overall size & readability -> must be of sensible length */
    public const UNIT_SIGN_MAX_CHARS = 10;

    /** optional sign (e.g. '%') to add as suffix to the axis points and plotted answer (value) points. */
    private readonly ?string $unitSign;

    /** using references instead of questions for deserialization purposes inside node tree */
    private readonly ParticipatingQuestionReferenceCollection $participatingQuestionReferences;

    /**
     * $ignoreIndividualQuestionAccessRights: whether access rights to the individual participating questions is to be
     *   ignored or not when checking for access to this chart node and for plotting all participating questions (ignore)
     *   or only the ones with access (not ignore).
     * $hidePointValues: hide/show values of the plotted answer points
     * $hidePointUnitSuffix: hide/show unit suffix (e.g. %) of the plotted answer points
     * $hideScaleValue: hide/show values of the scale points
     * $hideScaleUnitSuffix: hide/show unit suffix (e.g. %) of the scale points.
     * $participatingQuestionReferences: contains reference-by-uids to participating questions, and optional custom labels.
     */
    protected function __construct(
        string $uid,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        string $displayMode,
        bool $hidden,
        ParticipatingQuestionReferenceCollection $participatingQuestionReferences,
        private readonly bool $ignoreIndividualQuestionAccessRights,
        private readonly bool $hidePointValues,
        private readonly bool $hidePointUnitSuffix,
        private readonly bool $hideScaleValues,
        private readonly bool $hideScaleUnitSuffix,
        ?string $unitSign,
    ) {
        $this->guardDisplayMode($displayMode);

        if (is_string($unitSign)) {
            $this->guardUnitSign($unitSign);
        }

        if (2 > $participatingQuestionReferences->count()) {
            throw new NotEnoughRadarChartParticipatingQuestionReferencesException('at least 2 question references are required for a valid radar chart');
        }

        parent::__construct($uid, $flowPermission, $info, $displayMode, $hidden);

        $this->unitSign = $unitSign;
        // if this node is constructed during JSON deserialization, the rootNode (via parent) in the tree to which this
        // node belongs might not yet be aware of all (referenced) questions in the tree! this means we cannot just use
        // the references here to already get & build the participating questions to which they refer, nor assert that
        // those participating questions are valid. we can only get those questions afterwards, when tree is fully build
        // hence only storing the references to participating questions
        $this->participatingQuestionReferences = $participatingQuestionReferences;
    }

    public static function fromUidAndDependencies(
        string $uid,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        string $displayMode,
        bool $hidden,
        ParticipatingQuestionReferenceCollection $participatingQuestionReferences,
        bool $ignoreIndividualQuestionAccessRights,
        bool $hidePointValues,
        bool $hidePointUnitSuffix,
        bool $hideScaleValues,
        bool $hideScaleUnitSuffix,
        ?string $unitSign,
    ): self {
        return new self(
            $uid,
            $flowPermission,
            $info,
            $displayMode,
            $hidden,
            $participatingQuestionReferences,
            $ignoreIndividualQuestionAccessRights,
            $hidePointValues,
            $hidePointUnitSuffix,
            $hideScaleValues,
            $hideScaleUnitSuffix,
            $unitSign
        );
    }

    public static function fromUidAndMinimalDependencies(
        string $uid,
        LocalizedInfoWithRequiredLabel $info,
    ): self {
        return new self(
            $uid,
            FlowPermission::createEmpty(),
            $info,
            self::DISPLAY_MODE_REGULAR,
            false,
            new ParticipatingQuestionReferenceCollection(),
            false,
            false,
            false,
            false,
            false,
            null
        );
    }

    /**
     * @throws NotEnoughRadarChartParticipatingQuestionReferencesException
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        return new self(
            $data[self::KEY_UID],
            FlowPermission::fromJsonDecodedArray($data[self::KEY_FLOW_PERMISSION]),
            LocalizedInfoWithRequiredLabel::fromJsonDecodedArray($data[self::KEY_INFO]),
            $data[self::KEY_DISPLAY_MODE] ?? self::DISPLAY_MODE_REGULAR,
            $data[self::KEY_HIDDEN],
            ParticipatingQuestionReferenceCollection::fromJsonDecodedArray($data[self::KEY_QUESTION_REFERENCES]),
            $data[self::KEY_IGNORE_INDIVIDUAL_QUESTION_RIGHTS],
            $data[self::KEY_HIDE_POINT_VALUES],
            $data[self::KEY_HIDE_POINT_UNIT_SUFFIX],
            $data[self::KEY_HIDE_SCALE_VALUES],
            $data[self::KEY_HIDE_SCALE_UNIT_SUFFIX],
            $data[self::KEY_UNIT_SIGN],
        );
    }

    public function getNestedLabelForFallBackLocale(string $glue = ' - '): string
    {
        return $this->getNestedLabel($this->getInfo()->getLocalizedLabel()->getFallbackLocale(), $glue);
    }

    public static function getType(): string
    {
        return self::TYPE_PREFIX.'_'.self::TYPE;
    }

    public static function getChartMinValue(): int
    {
        return self::CHART_MIN_VALUE;
    }

    public static function getChartMaxValue(): int
    {
        return self::CHART_MAX_VALUE;
    }

    public function usesUnitSign(): bool
    {
        return is_string($this->unitSign);
    }

    public function getUnitSign(): string
    {
        if (!is_string($this->unitSign)) {
            throw new \BadMethodCallException('no unit sign found');
        }

        return $this->unitSign;
    }

    public function hidePointValues(): bool
    {
        return $this->hidePointValues;
    }

    public function hidePointUnitSuffix(): bool
    {
        return $this->hidePointUnitSuffix;
    }

    public function hideScaleValues(): bool
    {
        return $this->hideScaleValues;
    }

    public function hideScaleUnitSuffix(): bool
    {
        return $this->hideScaleUnitSuffix;
    }

    public function isStepRoleReadAccessGranted(StepInterface $step, Role $role): bool
    {
        // access is first of all granted based on explicit read access for role in step to the chart node itself. if
        // this node is NOT configured to ignore individual participating question access rights, then read access for
        // role must also be granted to at least one participating question (= role is allowed to see only those
        // participating question answers (where available) in the chart). if configured to ignore, access to chart node
        // itself is sufficient (= role is allowed to see all participating question answers (where available) in the chart)
        return 0 < $this->getParticipatingQuestionsInStepForRole($step, $role)->count();
    }

    public function isAccessibleAndRenderableFor(StepInterface $step, Role $role, array $formValues): bool
    {
        // to be able to render: checks if access is granted to this radar chart
        // AND access is granted to at least one of its participating questions (or access to participating questions can be ignored)
        // AND has at least one submitted answer (for its access-granting participating questions) to plot
        return $this->hasPlottableDataFor($step, $role, $formValues);
    }

    public function hasPlottableDataFor(StepInterface $step, Role $role, array $formValues): bool
    {
        // access must be granted to chart node and at least one participating question with a plottable answer in formValues
        return !$this->getParticipatingQuestionsInStepForRole($step, $role)
                ->whereHavingPlottableAnswerInFormValues($formValues)
                ->isEmpty();
    }

    public function getPlottableDataFor(StepInterface $step, Role $role, array $formValues, string $locale): ChartPlotData
    {
        // expects access granted to chart node and at least one participating question with a plottable answer
        // can be derived from given formValues, else it'll fail hard and throw custom exception
        return ChartPlotData::fromParticipatingQuestionsWithAnswersInFormValues(
            $this->getParticipatingQuestionsInStepForRole($step, $role),
            $formValues,
            $locale
        );
    }

    public function jsonSerialize(): array
    {
        return [
            ...parent::jsonSerialize(),
            self::KEY_TYPE => self::getType(),
            self::KEY_QUESTION_REFERENCES => $this->participatingQuestionReferences,
            self::KEY_IGNORE_INDIVIDUAL_QUESTION_RIGHTS => $this->ignoreIndividualQuestionAccessRights,
            self::KEY_HIDE_POINT_VALUES => $this->hidePointValues,
            self::KEY_HIDE_POINT_UNIT_SUFFIX => $this->hidePointUnitSuffix,
            self::KEY_HIDE_SCALE_VALUES => $this->hideScaleValues,
            self::KEY_HIDE_SCALE_UNIT_SUFFIX => $this->hideScaleUnitSuffix,
            self::KEY_UNIT_SIGN => $this->unitSign,
        ];
    }

    /**
     * use the references to participating questions to find the actual questions in the tree rootNode to which
     * this radar chart node belongs, and then builds the participating questions.
     * This fails hard if one or more references point to non-existing or non-plottable questions in the tree, or if one
     * or more plottable questions are invalid as participating question.
     * note that upon publish of the template to which this node(tree) belongs, a check and assert is done for missing
     * and/or invalid participating questions, to avoid errors here. any error thrown here probably means DB tampering.
     */
    private function getParticipatingQuestions(): ParticipatingQuestionCollection
    {
        return ParticipatingQuestionCollection::fromRootNodeForQuestionReferences(
            $this->getTreeRootNode(),
            $this->participatingQuestionReferences
        );
    }

    public function getFailedParticipationQuestionEligibilitySet(): ParticipatingQuestionEligibilityCollection
    {
        return ParticipatingQuestionEligibilityCollection::fromRootNodeByReferences(
            $this->getTreeRootNode(),
            $this->participatingQuestionReferences
        )->getfailed();
    }

    private function getParticipatingQuestionsInStepForRole(StepInterface $step, Role $role): ParticipatingQuestionCollection
    {
        // access to the radar chart node itself must always be granted before checking & filtering participating questions
        if (!parent::isStepRoleReadAccessGranted($step, $role)) {
            return new ParticipatingQuestionCollection([]);
        }

        // if (read) access to the participating questions can be ignored, don't check their access, return all of them.
        if ($this->ignoreIndividualQuestionAccessRights) {
            return $this->getParticipatingQuestions();
        }

        // if (read) access to the participating questions can NOT be ignored, only return those that grant access
        return $this->getParticipatingQuestions()->whereReadAccessGrantedInStepForRole($step, $role);
    }

    private function guardUnitSign(string $unitSign): void
    {
        $chars = strlen($unitSign);
        if (self::UNIT_SIGN_MIN_CHARS > $chars || self::UNIT_SIGN_MAX_CHARS < $chars) {
            throw new \DomainException('unit sign character length must be between '.((string) self::UNIT_SIGN_MIN_CHARS).' and '.((string) self::UNIT_SIGN_MAX_CHARS).' for chart size & readability. got '.((string) $chars));
        }
    }

    public function guardDisplayMode(string $displayMode): void
    {
        $modes = [self::DISPLAY_MODE_REGULAR];

        if (!in_array($displayMode, $modes, true)) {
            throw new \InvalidArgumentException('displayMode for radar chart node must be one of the following: '.implode(' | ', $modes).'. got '.$displayMode);
        }
    }

    /**
     * no method overloading, can't type hint argument on implementation and the pass to parent: explicit implementation assert.
     */
    public function updateInfo(AbstractLocalizedInfo $info): void
    {
        if (!($info instanceof LocalizedInfoWithRequiredLabel)) {
            throw new \InvalidArgumentException('expect info to be instance of '.LocalizedInfoWithRequiredLabel::class.' got '.$info::class);
        }

        parent::updateInfo($info);
    }

    public function getInfo(): LocalizedInfoWithRequiredLabel
    {
        /** @var LocalizedInfoWithRequiredLabel $result */
        $result = parent::getInfo();

        return $result;
    }
}
