<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;

/**
 * reference to a participating question , based on a @see InputNode::$uid
 * with custom data for use in @see RadarChartViewNode.
 *
 * this reference is needed as a go-between object for @see ParticipatingQuestion . it is used to reference those
 * participating questions in a @see RadarChartViewNode so we can use that reference to fetch the participating
 * question from the @see RadarChartViewNode::getTreeRootNode() to which both the participing questions and
 * the  @see RadarChartViewNode belong.  we can not just directly put those participating questions in
 * that @see RadarChartViewNode because of the deserializing process of the node's tree structure from JSON
 * in @see FormList .
 * during deserialization ofa @see RadarChartViewNode, not all of its participating questions might yet be
 * deserialized. only after full deserialization of the tree, is every node question aware of its position in
 * the @see FormList node tree and by such via the root node aware of all other questions.
 * NOTE: it is possible to go looking up and down the serialized tree during deserialization of the tree
 * but that is heavy on performance as it requires multiple runs all over the tree, is less flexible
 * and references are already used for similar logic elsewhere with success.
 *
 * Class ParticipatingQuestionReference
 */
final readonly class ParticipatingQuestionReference implements \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_QUESTION_UID = 'question_uid';
    /** @var string */
    public const KEY_CUSTOM_LABEL = 'custom_labels';

    public function __construct(
        private string $questionUid,
        private LocalizedOptionalString $customLabel,
    ) {
    }

    public function getQuestionUid(): string
    {
        return $this->questionUid;
    }

    public function getCustomLabel(): LocalizedOptionalString
    {
        return $this->customLabel;
    }

    public static function fromJsonDecodedArray(array $data): self
    {
        return new self(
            $data[self::KEY_QUESTION_UID],
            LocalizedOptionalString::fromJsonDecodedArray($data[self::KEY_CUSTOM_LABEL]),
        );
    }

    /**
     * @return array{question_uid: string, custom_labels: LocalizedOptionalString}
     */
    public function jsonSerialize(): array
    {
        return [
            self::KEY_QUESTION_UID => $this->questionUid,
            self::KEY_CUSTOM_LABEL => $this->customLabel,
        ];
    }
}
