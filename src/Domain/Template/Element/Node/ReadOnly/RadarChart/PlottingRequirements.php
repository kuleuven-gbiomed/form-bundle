<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart;

use KUL\FormBundle\Domain\Template\Element\Node\Rounding\RoundingModes;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\ScoringParameters;

class PlottingRequirements
{
    final public const DEFAULT_ROUNDING_MODE = RoundingModes::DEFAULT;
    /**
     * @phpstan-var RoundingModes::ROUND_* $roundingMode
     */
    private readonly int $roundingMode;

    /**
     * @phpstan-param RoundingModes::ROUND_* $roundingMode
     */
    public function __construct(
        private readonly ?float $minimumAnswerValue,
        private readonly ?float $maximumAnswerValue,
        int $roundingMode,
    ) {
        $this->guardMinMaxRange();
        $this->roundingMode = RoundingModes::extractAllowedMode($roundingMode);
    }

    private function guardMinMaxRange(): void
    {
        if ($this->minimumAnswerValue > $this->maximumAnswerValue) {
            throw new \DomainException('the minimumAnswerValue cannot be greater than the maximumAnswerValue');
        }
    }

    public static function fromScoringParameters(ScoringParameters $scoringParameters): self
    {
        return new self(
            $scoringParameters->hasMinScore() ? $scoringParameters->getMinScore() : null,
            $scoringParameters->hasMaxScore() ? $scoringParameters->getMaxScore() : null,
            $scoringParameters->hasRoundingMode() ? $scoringParameters->getRoundingMode() : self::DEFAULT_ROUNDING_MODE
        );
    }

    public function hasMinimumAnswerValue(): bool
    {
        return !(null === $this->minimumAnswerValue);
    }

    public function getMinimumAnswerValue(): float
    {
        if (null === $this->minimumAnswerValue) {
            throw new \Exception('no minimumAnswerValue found');
        }

        return $this->minimumAnswerValue;
    }

    public function hasMaximumAnswerValue(): bool
    {
        return !(null === $this->maximumAnswerValue);
    }

    public function getMaximumAnswerValue(): float
    {
        if (null === $this->maximumAnswerValue) {
            throw new \Exception('no maximumAnswerValue found');
        }

        return $this->maximumAnswerValue;
    }

    /**
     * @phpstan-return RoundingModes::ROUND_*
     */
    public function getRoundingMode(): int
    {
        return $this->roundingMode;
    }
}
