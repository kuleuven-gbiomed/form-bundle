<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * @extends ArrayCollection<array-key,ParticipatingQuestion>
 */
class ParticipatingQuestionCollection extends ArrayCollection
{
    public static function fromRootNodeForQuestionReferences(
        CategoryNode $rootNode,
        ParticipatingQuestionReferenceCollection $questionReferences,
    ): self {
        $result = [];
        /** @var ParticipatingQuestionReference $questionReference */
        foreach ($questionReferences as $questionReference) {
            $result[] = ParticipatingQuestion::fromRootNodeForQuestionReference($rootNode, $questionReference);
        }

        return new self($result);
    }

    public function whereReadAccessGrantedInStepForRole(
        StepInterface $step,
        Role $role,
    ): self {
        return $this->filter(fn (ParticipatingQuestion $participatingQuestion): bool => $participatingQuestion->getQuestion()->isStepRoleReadAccessGranted($step, $role));
    }

    public function whereHavingPlottableAnswerInFormValues(array $formValues): self
    {
        return $this->filter(fn (ParticipatingQuestion $participatingQuestion): bool => $participatingQuestion->hasPlottableAnswerInFormValues($formValues));
    }

    public function toReferences(): ParticipatingQuestionReferenceCollection
    {
        $references = [];

        /** @var ParticipatingQuestion $participatingQuestion */
        foreach ($this->getIterator() as $participatingQuestion) {
            $references[] = $participatingQuestion->toReference();
        }

        return new ParticipatingQuestionReferenceCollection($references);
    }
}
