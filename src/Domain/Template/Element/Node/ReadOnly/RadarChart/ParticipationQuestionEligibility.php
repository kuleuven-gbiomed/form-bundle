<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\PlottableInRadarChart;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;

/**
 * is a question eligible for usage
 * as @see ParticipatingQuestion
 * in a @see RadarChartViewNode.
 */
class ParticipationQuestionEligibility
{
    private function __construct(
        private readonly string $referencedQuestionUid,
        private readonly bool $missingMinimumAnswerValue,
        private readonly bool $missingMaximumAnswerValue,
        private readonly OptionInputNodeCollection $choiceQuestionAnswerOptionsWithMissingScoreValues,
        private readonly ?NodeInterface $question,
    ) {
    }

    public static function fromRootNodeByReference(CategoryNode $rootNode, ParticipatingQuestionReference $questionReference): self
    {
        $missingMinimumAnswerValue = true;
        $missingMaximumAnswerValue = true;
        $choiceQuestionAnswerOptionsWithMissingScoreValues = new OptionInputNodeCollection();
        $question = $rootNode->findDescendantByUid($questionReference->getQuestionUid());

        if ($question instanceof PlottableInRadarChart) {
            $requirements = $question->getPlottingRequirements();
            $missingMinimumAnswerValue = !$requirements->hasMinimumAnswerValue();
            $missingMaximumAnswerValue = !$requirements->hasMaximumAnswerValue();
        }

        if ($question instanceof ChoiceInputNode) {
            $choiceQuestionAnswerOptionsWithMissingScoreValues = self::getInvalidAvailableAnswerOptionsFromChoiceQuestion($question);
        }

        return new self(
            $questionReference->getQuestionUid(),
            $missingMinimumAnswerValue,
            $missingMaximumAnswerValue,
            $choiceQuestionAnswerOptionsWithMissingScoreValues,
            $question
        );
    }

    public static function fromQuestion(PlottableInRadarChart $plottableQuestion): self
    {
        $requirements = $plottableQuestion->getPlottingRequirements();
        $choiceQuestionAnswerOptionsWithMissingScoreValues = new OptionInputNodeCollection();

        if ($plottableQuestion instanceof ChoiceInputNode) {
            $choiceQuestionAnswerOptionsWithMissingScoreValues = self::getInvalidAvailableAnswerOptionsFromChoiceQuestion($plottableQuestion);
        }

        return new self(
            $plottableQuestion->getUid(),
            !$requirements->hasMinimumAnswerValue(),
            !$requirements->hasMaximumAnswerValue(),
            $choiceQuestionAnswerOptionsWithMissingScoreValues,
            $plottableQuestion
        );
    }

    private static function getInvalidAvailableAnswerOptionsFromChoiceQuestion(
        ChoiceInputNode $choiceQuestion,
    ): OptionInputNodeCollection {
        $invalidOptions = [];
        // only available (non-hidden) answer options are taking in account (hidden ones are ignored in all form dealings anyway)
        /** @var OptionInputNode $optionInputNode */
        foreach ($choiceQuestion->getAvailableOptions() as $optionInputNode) {
            if (!$optionInputNode->hasScoringValue()) {
                $invalidOptions[] = $optionInputNode;
            }
        }

        return new OptionInputNodeCollection($invalidOptions);
    }

    public function fails(): bool
    {
        return !$this->refersToNonPlottableQuestion()
            && !$this->isMissingMinimumAnswerValue()
            && !$this->isMissingMaximumAnswerValue()
            && !$this->hasChoiceQuestionAnswerOptionsWithMissingScoreValues();
    }

    public function getReferencedQuestionUid(): string
    {
        return $this->referencedQuestionUid;
    }

    public function refersToNonExistingQuestion(): bool
    {
        return !$this->question instanceof ParentalInputNode;
    }

    public function refersToNonPlottableQuestion(): bool
    {
        return !$this->question instanceof PlottableInRadarChart;
    }

    public function getQuestion(): PlottableInRadarChart
    {
        if ($this->question instanceof PlottableInRadarChart) {
            return $this->question;
        }

        throw new \Exception('no PlottableInRadarChart question found');
    }

    public function isMissingMinimumAnswerValue(): bool
    {
        return $this->missingMinimumAnswerValue;
    }

    public function isMissingMaximumAnswerValue(): bool
    {
        return $this->missingMaximumAnswerValue;
    }

    public function hasChoiceQuestionAnswerOptionsWithMissingScoreValues(): bool
    {
        return !$this->choiceQuestionAnswerOptionsWithMissingScoreValues->isEmpty();
    }

    public function getChoiceQuestionAnswerOptionsWithMissingScoreValues(): OptionInputNodeCollection
    {
        return $this->choiceQuestionAnswerOptionsWithMissingScoreValues;
    }
}
