<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;

/**
 * @extends ArrayCollection<array-key, ParticipationQuestionEligibility>
 */
class ParticipatingQuestionEligibilityCollection extends ArrayCollection
{
    public static function fromRootNodeByReferences(
        CategoryNode $rootNode,
        ParticipatingQuestionReferenceCollection $questionReferences,
    ): self {
        $result = [];

        /** @var ParticipatingQuestionReference $questionReference */
        foreach ($questionReferences as $questionReference) {
            $result[] = ParticipationQuestionEligibility::fromRootNodeByReference($rootNode, $questionReference);
        }

        return new self($result);
    }

    public function getfailed(): self
    {
        return $this->filter(fn (ParticipationQuestionEligibility $eligibility) => !$eligibility->fails());
    }
}
