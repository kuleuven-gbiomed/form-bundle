<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Contract\Deserializable;

/**
 * @extends ArrayCollection<array-key, ParticipatingQuestionReference>
 */
final class ParticipatingQuestionReferenceCollection extends ArrayCollection implements \JsonSerializable, Deserializable
{
    public static function fromJsonDecodedArray(array $data): self
    {
        $elements = [];

        foreach ($data as $participatingQuestionReferenceData) {
            $elements[] = ParticipatingQuestionReference::fromJsonDecodedArray($participatingQuestionReferenceData);
        }

        return new self($elements);
    }

    public function jsonSerialize(): array
    {
        $data = [];

        /** @var ParticipatingQuestionReference $participatingQuestionReference */
        foreach ($this->getIterator() as $participatingQuestionReference) {
            $data[] = $participatingQuestionReference;
        }

        return $data;
    }
}
