<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart;

use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\PlottableInRadarChart;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\Exception\InvalidRadarChartParticipatingQuestionException;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\Exception\UnPlottableRadarChartDataException;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;

class ParticipatingQuestion
{
    private readonly PlottableInRadarChart $question;

    private function __construct(PlottableInRadarChart $question, private readonly LocalizedOptionalString $customLabel)
    {
        self::guardQuestion($question);

        $this->question = $question;
    }

    public static function fromRootNodeForQuestionReference(
        CategoryNode $rootNode,
        ParticipatingQuestionReference $questionReference,
    ): self {
        if (!(FormList::ROOT_UID === $rootNode->getUid())) {
            throw new InvalidRadarChartParticipatingQuestionException('node with uid '.$rootNode->getUid().' to get participation node from is no root node');
        }

        $participationEligibility = ParticipationQuestionEligibility::fromRootNodeByReference($rootNode, $questionReference);

        if ($participationEligibility->refersToNonExistingQuestion()) {
            throw new InvalidRadarChartParticipatingQuestionException('no question for referenced uid '.$questionReference->getQuestionUid().' is found in root');
        }

        if ($participationEligibility->refersToNonPlottableQuestion()) {
            throw new InvalidRadarChartParticipatingQuestionException('node with uid '.$questionReference->getQuestionUid().' is no PlottableInRadarChart node');
        }

        return new self($participationEligibility->getQuestion(), $questionReference->getCustomLabel());
    }

    public static function fromQuestionAndCustomLabel(
        PlottableInRadarChart $question,
        LocalizedOptionalString $customLabel,
    ): self {
        return new self($question, $customLabel);
    }

    public function getQuestion(): PlottableInRadarChart
    {
        return $this->question;
    }

    private function findCustomLabel(string $locale): ?string
    {
        if ($this->customLabel->hasForLocaleOrFallback($locale)) {
            return $this->customLabel->getForLocaleOrFallback($locale);
        }

        return null;
    }

    public function getLabel(string $locale): string
    {
        // custom label has priority, even if there is none for given locale but one for other locale (fallback domain decision)
        $customLabel = $this->findCustomLabel($locale);

        return $customLabel ?? $this->getQuestion()->getLabel($locale);
    }

    public function hasPlottableAnswerInFormValues(array $formValues): bool
    {
        return is_int($this->findPlottableAnswerInFormValues($formValues));
    }

    public function getPlottableAnswerInFormValues(array $formValues): int
    {
        $answer = $this->findPlottableAnswerInFormValues($formValues);
        if (!is_int($answer)) {
            throw new \InvalidArgumentException('no plottable answer found in given formValues');
        }

        return $answer;
    }

    private function findPlottableAnswerInFormValues(array $formValues): ?int
    {
        $question = $this->getQuestion();
        $info = self::getInputNodeIdentifyingInfo($this->getQuestion());

        // if question is never submitted, no value will, nor can exist in the (submitted) form values
        // (don't use isset/empty here, could have been submitted validly null/blank or 0 (zero)!)
        if (!array_key_exists($this->getQuestion()->getUid(), $formValues)) {
            return null;
        }

        if ($question instanceof NumberInputNode) {
            // answer could validly be null if the question is not required and submitted blank! for consistency (older
            // submitted forms in db having 'stringy/integery' float): check with is_numeric and make sure we have a float
            $answer = is_numeric($formValues[$question->getUid()]) ? (float) $formValues[$question->getUid()] : null;

            return is_float($answer) ? $this->makePlottableAnswer($answer) : null;
        }

        if ($question instanceof ChoiceInputNode) {
            // the chosen (submitted) answer option(InputNode) for a Choice question is found in formValues with its uid
            // as key (associated value is not important here). we know the choice question was submitted (see above: its
            // uid was found in formValues) and if the question is required, one answer option will have been chosen &
            // submitted by a user (option uid found in formValues) . (multiple answer options is not allowed nor possible
            // for choice-as-score questions). we're only checking options with scoring value (without it's unplottable),
            // but all options should have one as it asserted upon class construct.
            /** @var OptionInputNode $choiceScoringOption */
            foreach ($question->getAvailableOptions()->getHavingScoringValue() as $choiceScoringOption) {
                if (!array_key_exists($choiceScoringOption->getUid(), $formValues)) {
                    continue;
                }

                return $this->makePlottableAnswer($choiceScoringOption->getScoringValue());
            }

            // if choice question is NOT required then it's validly possible no answer option is found = user submitted blank.
            if (!$question->isRequired()) {
                return null;
            }

            // Choice question is submitted and required, but no answer option found = something is wrong (tampering with
            // DB data, question options (scoringValues) updated afterwards but not it's use al participating question, etc.
            // note: charts are not that important in context of the whole form logic, so if ever this exception needs avoiding
            // for some reason, we could safely return null here which would mean this answer will be omitted in the chart.
            // but it's better to catch any (unintentional) choice question broken for chart participation.
            throw new \DomainException('choice question '.$info.' was submitted & is required but could not find the submitted answer option! DB data might be corrupted or tampered with and/or you used invalid/incomplete formValues (e.g. temporary saved formValues instead of submitted formValues).');
        }

        // if ever a new question type is added and it is a scorable (has min & max config) question -> catch here
        throw new \DomainException('unhandled question type for finding plottable answer for question '.$info);
    }

    /**
     * for uniform & correct scaled plotting of answer on the 0 to 100 integer chart scale, need to adjust
     * answer & max if the min is not 0 (zero), before doing the conversion, rounding (without decimals) and finally
     * casting to the 0 to 100 integer chart scale. (the max adjusting also guarantees no division by 0 (zero) in
     * calculation. the rounding with zero precision guarantees subsequent safe casting to integer without loss.
     *
     * examples:
     * min = o -> answer 7 on 0 to 20 scale => plotted in chart as 7 * 100 / 20 = 35 on 0 to 100 chart scale
     * min > o -> answer 7 on 5 to 20 scale => plotted in chart as (7-5) * 100 / (20-5) = 13 (rounded) on 0 to 100 chart scale
     * min < o -> answer 7 on -5 to 20 scale => plotted as (7+5) * 100 / (20+5) = 48 on 0 to 100 chart scale
     *         -> answer -4 on -5 to 20 scale => plotted as (-4+5) * 100 / (20+5) = 4 on 0 to 100 chart scale
     *         -> answer 19 on -5 to 20 scale => plotted as (19+5) * 100 / (20+5) = 96 on 0 to 100 chart scale
     *         -> answer -4 on -5 to -1 scale => plotted as (-4+5) * 100 / (-1+5) = 25 on 0 to 100 chart scale
     */
    private function makePlottableAnswer(float $answer): int
    {
        $plottingRequirements = $this->getQuestion()->getPlottingRequirements();

        $min = $plottingRequirements->getMinimumAnswerValue();
        $max = $plottingRequirements->getMaximumAnswerValue();

        // can't harm to assert that given answer fits the question. (e.g. a temporary saved answer doesn't have to fit the
        // min-max range constraint (yet) but then again, radar charts don't use temporary saved answers (domain decision))
        if ($answer < $min || $answer > $max) {
            $info = self::getInputNodeIdentifyingInfo($this->getQuestion());
            throw new UnPlottableRadarChartDataException('answer does not fit the min-max range from the question '.$info);
        }

        if ($min > 0) {
            $max -= $min;
            $answer -= $min;
        } elseif ($min < 0) {
            $max += abs($min);
            $answer += abs($min);
        }

        $chartMaxValue = (float) RadarChartViewNode::getChartMaxValue();

        return (int) round(
            $answer * $chartMaxValue / $max,
            0,
            $plottingRequirements->getRoundingMode()
        );
    }

    /**
     * i.e. at time of writing only @see ScorableInputNode can be used as valid participating questions:
     * to plot a question's answer in a radar chart, we need a min & max allowed input (answer) value configured
     * on the question, against which we will be able to calculate and set the answered question's value in the
     * chart. this min & max settings means de facto that the only questions that can be participating questions are:
     * - number questions (always have (default) min & max and always get answered with numeric answer)
     * - choice questions that have set:
     *      - a min score value
     *      - a max score value
     *      - a score value for each of its answer options
     *   which means choice questions that (can) operate as choice-as-score or choice-as-global question,
     *   although they do not necessarily have to actually operate as choice-as-(global)score questions!
     */
    private static function guardQuestion(PlottableInRadarChart $question): void
    {
        $participationEligibility = ParticipationQuestionEligibility::fromQuestion($question);

        if ($participationEligibility->isMissingMinimumAnswerValue()) {
            throw new InvalidRadarChartParticipatingQuestionException('question '.self::getInputNodeIdentifyingInfo($question).' has no minimumAnswerValue value configured.');
        }

        if ($participationEligibility->isMissingMaximumAnswerValue()) {
            throw new InvalidRadarChartParticipatingQuestionException('question '.self::getInputNodeIdentifyingInfo($question).' has no maximumAnswerValue value configured.');
        }

        // for a choice-as-score question to be able to be plotted in a chart,
        // all available options must have a scoring value (regardless if question is required or not)
        if ($participationEligibility->hasChoiceQuestionAnswerOptionsWithMissingScoreValues()) {
            $invalidOptions = $participationEligibility->getChoiceQuestionAnswerOptionsWithMissingScoreValues();
            throw new InvalidRadarChartParticipatingQuestionException('question '.self::getInputNodeIdentifyingInfo($question).' has one or more answer options with missing scoring values: '.implode(' | ', $invalidOptions->getLabelsArray('en')));
        }
    }

    private static function getInputNodeIdentifyingInfo(NodeInterface $node): string
    {
        $label = 'unknown label';
        $type = 'unknown type';

        if ($node instanceof InputNode) {
            $label = $node->getLabel('en');
            $type = $node->getInput()::getType();
        }

        return '[uid: '.$node->getUid().', label: '.$label.', type: '.$type.']';
    }

    public function toReference(): ParticipatingQuestionReference
    {
        return new ParticipatingQuestionReference($this->question->getUid(), $this->customLabel);
    }
}
