<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart;

use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\Exception\UnPlottableRadarChartDataException;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;
use Webmozart\Assert\Assert;

class ChartPlotData implements \JsonSerializable
{
    /** @var string[] */
    private readonly array $labels;
    /** @var int[] */
    private readonly array $answers;

    private function __construct(array $labels, array $answers)
    {
        if (!(count($labels) === count($answers))) {
            throw new UnPlottableRadarChartDataException('amount of labels to plot must equal amount of answers to plot');
        }

        if (0 === count($answers)) { // 0 answers also means 0 labels ;)
            throw new UnPlottableRadarChartDataException('there must be at least one answer and associated label to plot');
        }

        foreach ($labels as $label) {
            // need a label to show for each
            if (!is_string($label) || ('' === trim($label, ' '))) {
                throw new UnPlottableRadarChartDataException('plottable labels must be non-empty strings');
            }
        }

        $minPoint = RadarChartViewNode::getChartMinValue();
        $maxPoint = RadarChartViewNode::getChartMaxValue();
        foreach ($answers as $answer) {
            // chart only works with integers, so any floats should have been cast to int. let's assert this
            if (!is_int($answer) || !($minPoint <= $answer && $maxPoint >= $answer)) {
                throw new UnPlottableRadarChartDataException('plottable answers must be integers between '.((string) $minPoint).' and '.((string) $maxPoint));
            }
        }

        $this->labels = $labels;
        $this->answers = $answers;
    }

    public static function fromParticipatingQuestionsWithAnswersInFormValues(
        ParticipatingQuestionCollection $participatingQuestions,
        array $formValues,
        string $locale,
    ): self {
        Assert::notEmpty($locale);

        $participatingQuestionsWithAnswers = $participatingQuestions->whereHavingPlottableAnswerInFormValues($formValues);
        if ($participatingQuestionsWithAnswers->isEmpty()) {
            throw new UnPlottableRadarChartDataException('need a least one participating question with a plottable answer');
        }

        $labels = [];
        $answers = [];

        /** @var ParticipatingQuestion $participatingQuestion */
        foreach ($participatingQuestionsWithAnswers as $participatingQuestion) {
            $labels[] = $participatingQuestion->getLabel($locale);
            $answers[] = $participatingQuestion->getPlottableAnswerInFormValues($formValues);
        }

        return new self($labels, $answers);
    }

    public function jsonSerialize(): array
    {
        return [
            'labels' => $this->labels,
            'data' => $this->answers,
        ];
    }
}
