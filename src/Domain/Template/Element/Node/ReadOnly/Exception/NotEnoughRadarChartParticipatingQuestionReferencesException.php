<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\Exception;

class NotEnoughRadarChartParticipatingQuestionReferencesException extends \Exception
{
}
