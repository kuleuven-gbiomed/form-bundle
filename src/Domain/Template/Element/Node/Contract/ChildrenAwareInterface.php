<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Contract;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;

/**
 * Interface ChildrenAwareInterface.
 *
 * categories and choice type questions can have other nodes as children
 *
 * @see     CategoryNode can have other category nodes as well as all type of input nodes (except for option nodes) as children
 * @see     ChoiceInputNode can have option nodes as children (should need at least two children to be valid usable as choice question)
 *
 * @template T of NodeInterface
 * @template TCollection of NodeCollection<array-key, T>
 */
interface ChildrenAwareInterface extends NodeInterface
{
    /** @var string */
    public const KEY_CHILDREN = 'children';

    public function hasChildren(): bool;

    /**
     * @return TCollection
     */
    public function getChildren();

    public function hasChildByUid(string $uid): bool;

    public function getChildByUid(string $uid): NodeInterface;

    /**
     * find a node for given uid which is found down the tree of this node or return NULL.
     */
    public function findDescendantByUid(string $uid): ?NodeInterface;

    /**
     * checks down the tree, starting at this node, if there is a node for given uid.
     */
    public function hasDescendantByUid(string $uid): bool;

    /**
     * gets the node for given uid which is found down the tree of this node.
     *
     * NOTE: a category node can have any number of descendants on any level down its tree.
     *       a parental input node @see ChoiceInputNode will only have children one level down as descendants
     */
    public function getDescendantByUid(string $uid): NodeInterface;

    /**
     * check for at least one child node that is not marked @see NodeInterface::isHidden().
     */
    public function hasActiveChildren(): bool;

    /**
     * get child nodes that is not marked @see NodeInterface::isHidden().
     *
     * @return TCollection
     */
    public function getActiveChildren();

    /**
     * adds given node to the existing children (if any) as last child.
     */
    public function attachChild(NodeInterface $child): void;

    /**
     * adds given node to the existing children (if any) as first child.
     */
    public function attachAsFirstChild(NodeInterface $child): void;

    /**
     * adds given node on the position in the existing children right after the given child node.
     */
    public function attachChildAfter(NodeInterface $child, NodeInterface $attachAfterChild): void;

    /**
     * remove the child from existing children.
     */
    public function removeChild(NodeInterface $child): void;

    /**
     * remove all children.
     */
    public function removeAllChildren(): void;

    /**
     * finds the node with same uid as given node and replaces it with given node with same uid.
     *
     * NOTE: used to update a child, where the $child then is a new or altered version based on the old child.
     *       based on existing code
     */
    public function replaceChild(NodeInterface $child): void;

    /**
     * returns a flat (all on one single level) collection containing this node and ALL its nested descendants
     * (regardless of node type, reachability and parental status).
     *
     * NOTE: all means children of parental inputNodes (@see ParentalInputNode ) are added as well. i.e. if this node
     *       or any descendant is a @see ChoiceInputNode, then that choice node is added with all it
     *       option @see OptionInputNodeCollection as well (a choice node has only direct children, no deeeper descendants)
     * NOTE: useful to extract a flattened list of all nodes in the rootNode when creating/updating a @see FormList.
     *       allows - instead of searching via the deep nested tree - to search, get & list nodes faster via a
     *       single dimension collection when tree structure is not needed (i.e. no view/form rendering), whilst all
     *       nodes in resulting collection are each individually still fully aware of the tree and their position in it.
     * NOTE: keeps some sensible order: depth first (= first add self, then add each direct child directly followed by
     *       that child's children (and recursively from those children down) before going to next direct child and so on.)
     *
     * @return NodeCollection<array-key,T>
     */
    public function getFlattenedWithAllDescendants(): NodeCollection;

    public function getFlattenedWithAllDescendantsArray(): array;
}
