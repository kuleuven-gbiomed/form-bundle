<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Contract;

use KUL\FormBundle\Domain\Template\Element\Node\Scoring\ScoringParameters;

/**
 * Interface ScorableInputInterface.
 */
interface ScorableInputInterface extends InputInterface
{
    public function getScoringParameters(): ScoringParameters;
}
