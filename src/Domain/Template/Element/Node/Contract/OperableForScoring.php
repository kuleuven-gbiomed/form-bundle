<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Contract;

use KUL\FormBundle\Domain\Template\Element\Node\Scoring\GlobalScoring\CalculationDependencyConfig;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\ScoringParameters;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\CalculationDependencyConfigCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\CalculationScoreNodeConfigCollection;

/**
 * Interface OperableForScoring.
 *
 * a question can operate as a score, a global score ar as both. those questions are no different than any other
 * questions except that apart from their 'normal' individual answering behaviour, they also additionally affect or
 * sum up to the answer of another question if they operate as a score
 *
 * questions operating as score or global score have a few requirements in common such as a min score, max score
 * and pass score. this contract bundles this common requirements.
 * HOW IT WORKS: when a question A operates as a score question A for a global score question B, then the answer of
 * that score question A, together with the answers of other score questions C to F, can be used to automatically
 * calculate the answer (weighted sum) for the global score question B.
 * the question A is answered individually and independently from global score question B. if on a submit by a user, the
 * system discovers that the global score question B has never yet been answered and all depending score questions
 * (A and C to F) do now have an answer (with C to F either answered in same submit by that user or by other users before)
 * then the global score question B is automatically calculated based on questions A & C to F and answered by the system.
 * this happens only once! if globals score question B was manually submitted by the user or the system notices that
 * it already has an answer (answered before by another user manually or auto calculated), then no calculation is done!
 * when a user has write access to global score question B, it is rendered as a submittable input field and thus user
 * can manually submit a value with no calculation done on server side. next to that input field will always be a
 * button that allows to make a AJAX server call to (re)calculate and return that global score value, IF answers to
 * all depending score questions are available (either from submitted before or user is filling them in at that point) .
 * this allows that user to choose to either manually submit a value for that global score question B, ignoring any
 * calculation. or it allows him/her to alter some values for score questions A & C to F (if he/she has write access to
 * those score questions and thus sees them rendered as input fields of course)
 *
 * IMPORTANT: a question can be a score question dependency for more than one global score question. and a global score
 * question can be a score question for another global score question!
 */
interface OperableForScoring extends ParentalInputNode
{
    /** @var string */
    public const KEY_GLOBAL_SCORE_CALCULATION_DEPENDENCY_CONFIGS = 'globalScoreCalculationDependencyConfigs';
    /** @var string */
    public const KEY_GLOBAL_SCORE_WEIGHING_TYPE = 'weighingType';

    /**
     * definition of weighing type where submitted score values are added 'as is' to calculated the global score value.
     *
     * add submitted value of score question directly to other score questions depending on same global score question,
     * to achieve total score. with achieved total score, recalculate against global score question's maxScore.
     * the weighing of each score question is usually set by admins to be equal to the max score of the score question,
     * because usually the intention is to just add all answer values of the score question together, with their
     * respective weighing determined by their respective max score. so that e.g. an score question with max score 20
     * weighs heavier that an score question with max score 10. however, one (or more) score question(s) could be
     * configured to weigh even heavier (or lower) in calculations than their max score, in which case the answer
     * value of it has to be re-calculated against that weighing. basically same way of calculating as with
     * WEIGHING_TYPE_PERCENTAGE, but the difference is that the sum of the score question weighings does not have to
     * be 100.
     *
     * usually, for each item, the weighing will be the max score of the item. but it could differ from the max score,
     * in which case the weighing for that specific item(s) is increased (or decreased). basically, this type of
     * 'absolute' weighing in calculations is the same as with percentage weighing, but the difference lies in the fact
     * that contrary to percentage calculations, the total sum of the item weighings does not have to 100 and
     *
     *
     * Dutch: 'T.o.v. totaal aantal mogelijk te behalen punten.'
     *
     * example:
     *                  UID    submitted-value   maxScore = weighing     in field displayed to end user as
     * score questions: uid_1        5             25                           _ / 25
     *                  uid_2        23            50                           _ / 50
     *                  uid_3        9             25                           _ / 25
     *                  uid_4        18            25                           _ / 25
     * global score question:  maxScore = 30
     * calculate: total_allowed_score = uid_1_maxScore + uid_2_maxScore + uid_3_maxScore + uid_4_maxScore = 125
     *            added_total         = 5 + 23 + 9 + 18
     *            global_score        = added_total/total_allowed_score * maxScore = added_total/125 * 30
     *                                = 13.2 on 30 (here the rounding process will kick in)
     *
     *  example with a weighing different from max score:
     *
     *                  UID    submitted-value   maxScore      weighing   in field displayed to end user as
     * score questions: uid_1        5             25             100           _ / 25
     *                  uid_2        23            50             50            _ / 50
     *                  uid_3        9             25             25            _ / 25
     *                  uid_4        18            25             25            _ / 25
     * global score question:  maxScore = 30
     * calculate: total_allowed_score = (uid_1_maxScore * 4) + uid_2_maxScore + uid_3_maxScore + uid_4_maxScore = 200
     *            added_total         = (5 * 4) + 23 + 9 + 18
     *            global_score        = added_total/total_allowed_score * maxScore = added_total/125 * 30
     *                                = 10.5 on 30 (here the rounding process will kick in)
     *
     * @var string
     */
    public const GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE = 'absolute';

    /**
     * definition of weighing type where submitted score values are added by percentage to calculated the global score value.
     *
     * maxScore of each score question counts as percentage of sum of percentages of all score questions depending on
     * same global score question (usually & logically adds up 100% but that's not required).each of the submitted
     * values of those score questions is calculated as value-over-maxScore times percentage and added to each other to
     * the added total. with achieved total score, recalculate over total-percentage times global-score-field-maxScore
     * the total sum of the item weighings has to be 100 %
     *
     * Dutch: 'Voor een opgegeven percentage.'
     *
     * example:
     *                  UID    submitted-value  maxScore       percentage  in field displayed to end user as
     * score questions: uid_1        5             20              25%           _ / 20
     *                  uid_2        23            40              25%           _ / 40
     *                  uid_3        9             20              25%           _ / 20
     *                  uid_4        18            100             25%           _ / 100
     * global score question:  globalMaxScore = 30
     * calculate: total_percentage = uid_1_% + uid_2_% + uid_3_% + uid_4_% = 100
     *            added_total      = (5/20 * 25) + (23/40 * 25) + (9/20 * 25) + (18/100 * 25)
     *            global_score     = added_total/total_percentage * globalMaxScore = added_total/100 * 30 = 50.75/100 *30
     *                             = 10.91 on 30 (here the rounding process will kick in)
     *
     * remark: compared to @see self::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE this example uses same submitted-values but different maxScores
     *         if exact same maxScores would be used, then result will be same. hence no real difference except that
     *         percentage type separates the maxScore from the weighing while add type uses the maxScore as weighing
     *
     * @var string
     */
    public const GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE = 'percentage';

    /**
     * @var string
     */
    public const GLOBAL_SCORE_DEFAULT_WEIGHING_TYPE = self::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE;

    /**
     * get the parameters, as determined by the question's input configuration, that are needed to determine if
     * scoring is possible and (if so) for usage in the calculations of (global) scoring.
     */
    public function getInputScoringParameters(): ScoringParameters;

    /**
     * check if the question has everything needed to function as a score and/or global score question.
     *
     * WARNING: this does not mean it actually is operating as a score question dependency for a global score question
     *          and/or as a global score question depending on other score questions. it could be fully functional for
     *          scoring but it might not have been assigned as a dependency to one or more global score questions and/or
     *          not have other score questions assigned as dependencies to make it operate as global score question.
     *          until then it's just some other node that could be 'upgraded' to a score / global score question.
     *          in that sense, the bode could intentionally be in some sort of sleeping mode. An admin constructing a question
     *          in a template can add these requirements before assigning the question as a score dependency to a global
     *          score node or making it in a global score question by adding dependencies to it. Or the admin can add
     *          this node with all scoring requirements for later use or even never upgrade it as a (global) score
     *          question and leave it as a 'stand alone scorable question' without usage in global score calculation to
     *          benefit from the scoring question's display effects (e.g. value is colored red in export if value is below
     *          pass score if pass score is set or derivable in scoring parameters).
     *          basically, this follows a 'require stuff but don't break if not all requirements are set' principle
     *          so that the admin working on nodes is not presented with a complex, bulky and multiform operation to make
     *          some node valid as e.g. a global score question, just because that admins hides or removes a score question
     *          that was a dependency for that global score question. i.e. gracefully falling back as long as bigger
     *          picture (admin- and endUser site) is still workable.
     *
     * needs:
     * for all type of questions: minScore, maxScore & passScore (passScore useful for display but not really for calculation)
     * for choice type questions: additionally requires that all child option nodes must have a value set as fixed score value
     */
    public function meetsScoringRequirements(): bool;

    /**
     * is this node actively operating as a score question with all that that implies:
     * it meets the score requirements and at least one valid reachable global score node exists in the rootNode's
     * descendants that refers to and uses this node as global score dependency for its global score calculation.
     *
     * NOTE: this check needs to verify if there is at least one reachable global score in the tree, which makes it
     * more performance heavy. its usage is only useful if one needs to check if some random node not only meets
     * the score requirements, but is also used as score node; e.g. to give some special mark-up to all score nodes
     * in an export or e.g. to list all nodes used as score nodes in admin side. if a node is only to be checked as
     * valid as score node for some global score node, then the @see meetsScoringRequirements will be more efficient.
     */
    public function operatesAsScoreNode(): bool;

    /**
     * checks if node operates as score node for specific global score node.
     */
    public function operatesAsScoreNodeForGlobalScoreNode(self $globalScoreNode): bool;

    /**
     * is this node actively operating as a global score question with all that that implies:
     * it meets the global score requirements and it has at least one valid reachable global score dependency
     * for its calculation to be able to succeed.
     */
    public function operatesAsGlobalScoreNode(): bool;

    /**
     * contains a set of configurations, each with a reference to a 'scoring' node that exists somewhere in
     * this node's @see NodeInterface::getTreeRootNode() , that can use that 'scoring' node's submitted value
     * in conjunction with the configuration to calculate a total score as the automatically calculated
     * value for this node as a @see OperableForScoring::operatesAsGlobalScoreNode().
     */
    public function getGlobalScoreCalculationDependencyConfigs(): CalculationDependencyConfigCollection;

    /**
     * uses the set of configurations (@see CalculationDependencyConfig ), each with a reference to a 'scoring' node
     * that exists somewhere in the @see NodeInterface::getTreeRootNode() of this node, to try to build a valid set of
     * those 'scoring' nodes with their configuration - based on the 'scoring' nodes being reachable and containing
     * all required data usable for calculations - so that the submitted values available for each of those 'scoring'
     * nodes - each in conjunction with their weighing configuration set for that 'scoring' node - can be used to
     * (automatically) calculate a total score as a valid & submittable value for this node.
     *
     * NOTE: a domain decision is made to make such a calculation as minimal breakable as possible, meaning that if
     *       some of those 'scoring' nodes are missing, or are not @see NodeInterface::isReachableInTree() or don't
     *       have valid data, that then should not break the process of form submitting, workflow transition, etc..
     *       in more detail: any score node dependency that is directly or indirectly hidden, is silently omitted
     *       from any calculations and its weighing in the calculation is taken over by - or thus distributed over -
     *       the other score node dependencies. because hiding nodes is not a common practise or sometimes a node that
     *       was taken part in a calculation before, can be decided by admins to become - temporarily or permanently -
     *       obsolete (or a mistake) in both the whole of the template as in the calculation.
     *       same for nodes that are missing some data to make the calculation even possible.
     *       because of admins altering the templates in bits and pieces, it can also be that the reference (uid) to a
     *       node in a @see CalculationDependencyConfig no longer points to an existing node. although admin side will/should clean
     *       this up, at least upon publish, this is not enough cause to throw an exception, but is skipped silently.
     *       it thus can be possible that no valid ScoreNodeDependencies are left, in which case no calculation
     *       will be possible and (automatic) calculation will be skipped.
     *       it has to be noted that admins will be warned about which global scores are calculable, both during
     *       the whole template building/altering process as well as upon publish (with confirmation request) of one
     */
    public function getValidGlobalScoreCalculationScoreNodeConfigs(): CalculationScoreNodeConfigCollection;

    /**
     * this is needed to know how the globl score is to handle the @see CalculationDependencyConfig::getWeighing()
     * when calculating the total score. should default to @see OperableForScoring::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE.
     *
     * @return string
     */
    public function getGlobalScoreWeighingType();
}
