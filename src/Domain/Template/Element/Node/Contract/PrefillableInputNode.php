<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Contract;

interface PrefillableInputNode extends ParentalInputNode
{
    public function hasPrefillingQuestions(): bool;

    /**
     * @return string[]
     */
    public function getPrefillingQuestionUids(): array;
}
