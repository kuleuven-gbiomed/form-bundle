<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Contract;

use KUL\FormBundle\Domain\Contract\UIdentifiableInterface;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Utility\AbstractLocalizedInfo;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Interface NodeInterface.
 *
 * DEVELOP NOTE: nodes are build in tree structure, making it very difficult (impossible) to work with collections instead
 * of arrays for children / deep descendants. same reason why the nodes are also not (full) Value Objects because for
 * (re)ordering and deep nested building, adding, updating & deleting of descendants, the objects are far easier to be
 * altered rather than return new instances. this code is based on extensive codebase (which has proven its worth) which
 * has also to be taken in account with regards to the here mentioned remarks. also, the naming of properties and
 * certainly methods surely might be questionable in a few cases (e.g. 'hidden') but naming comes from existing
 * codebase and is kept to avoid too much refactoring (especially in twigs)
 */
interface NodeInterface extends UIdentifiableInterface
{
    /**
     * @var string
     *
     * @psalm-suppress AmbiguousConstantInheritance this is a bug in psalm https://github.com/vimeo/psalm/issues/7818
     */
    public const KEY_TYPE = 'type';
    /**
     * @var string
     *
     * @psalm-suppress AmbiguousConstantInheritance this is a bug in psalm https://github.com/vimeo/psalm/issues/7818
     */
    public const KEY_INFO = 'info';
    /**
     * @var string
     *
     * @psalm-suppress AmbiguousConstantInheritance this is a bug in psalm https://github.com/vimeo/psalm/issues/7818
     */
    public const KEY_HIDDEN = 'hidden';

    /**
     * defines what kind of node it is. e.g. a category node or an input (question) node of some kind. its primary use
     * is for helping in the (de)serializing process, to determine what kind of node is to be constructed from given
     * serialized data by adding this type to the serialized dat during serialization.
     */
    public static function getType(): string;

    public function getInfo(): AbstractLocalizedInfo;

    /**
     * check if node has a label for given locale or - if not for given locale - for any other (fallback).
     *
     * for categories the label is optional via @see LocalizedInfoWithOptionalLabel
     * inputNodes (questions) will always have a label in @see LocalizedInfoWithRequiredLabel which is asserted
     * upon inputNode construction.
     *
     * @throws LocalizedStringException
     */
    public function hasLabel(string $locale): bool;

    /**
     * get node a label for given locale or - if not for given locale - for fallback locale.
     */
    public function getLabel(string $locale): string;

    public function hasDescription(string $locale): bool;

    public function getDescription(string $locale): string;

    public function hasRoleDescription(Role $role, string $locale): bool;

    public function getRoleDescription(Role $role, string $locale): string;

    public function hasFullDescription(string $locale, ?Role $role): bool;

    public function getFullDescription(string $locale, ?Role $role): ?string;

    /**
     * all input nodes and all category nodes except for the root categoryNode, always have a parent.
     */
    public function hasParent(): bool;

    /**
     * NOTE: if called on the root categoryNode, an exception should be thrown. rootNode has no parent in a valid tree.
     *
     * @psalm-return ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, ChildrenAwareInterface as NodeInterface>>
     *
     * @phpstan-return ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>>
     *
     * @throws \BadMethodCallException if no parent found
     */
    public function getParent(): ChildrenAwareInterface;

    /**
     * parent is updated when node is attached as a child or moved to given parent node.
     *
     * NOTE: on each node, the parent must be explicitly set. because a parent is not needed if one goes top-down in the
     * tree to find some node, but to get any ancestor / ancestral info of a random node that is on an unknown (random)
     * position in the tree, the parent must be known to the node (cfr. a bit like in (mysql) self referencing where
     * parent reference is set explicitly). parent is not set on construction of node. instead when a child is attached
     * (newly added, moved, replaced, etc..) to this node, the parent of the child is updated with this node as parent
     * (making de serializing of a deep nested tree a lot more efficient, especially performance wise).
     * remark: of course, there might be other of ways to get a parent of a random node, even on the fly. since to find
     * a random node, one has to go down from the root of the tree, which means that for each node one encounters,
     * one knows the direct parent, so e.g. one could use value objects to build a new version of each node encountered
     * with the parent (+ ancestors) attached. but in the end, it all just about convenience, performance and pragmatism:
     * I set (update) the parent once on the child upon attaching a child to that parent, and that way both child and parent
     * stay aware of each other until the parent/child gets detached/removed/moved... which means that all nodes are
     * no value objects but instead normal objects with changing parental state.
     */
    public function updateParent(self $parent): self;

    /**
     * update the main parameters of the node.
     *
     * NOTE: like any update methods in nodes, this does NOT return a new instance. for reasons of update actions
     * in admin side of single nodes in an existing node tree structure where the tree with all its instances is kept
     * and (can) not - at least not immediately - (be) rebuild. rebuilding the full tree itself can be done
     * via @see FormList::withNodeUpdated() and other methods in that class.
     * nodes are no immutable Value Objects. read note on @see updateParent
     */
    public function updateInfo(AbstractLocalizedInfo $info): void;

    /**
     * update the main parameters of the node.
     *
     * NOTE: like any update methods in nodes, this does NOT return a new instance. for reasons of update actions
     * in admin side of single nodes in an existing node tree structure where the tree with all its instances is kept
     * and (can) not - at least not immediately - (be) rebuild. rebuilding the full tree itself can be done
     * via @see FormList::withNodeUpdated() and other methods in that class.
     * nodes are no immutable Value Objects. read note on @see updateParent
     */
    public function updateHidden(bool $hidden): void;

    /**
     * recursively gets a flattened one-dimensional collection of the ancestral nodes for the node.
     * keeping a strict order with the category rootNode always being the first in the collection, going deeper level
     * by level from there while each time adding the next level ancestor (if any) to the preceding one-level-up one,
     * all the way down to the direct parent of the node, which is added as last to the collection.
     * (unless called on rootNode of course, which does not have any ancestors and returns empty collection).
     *
     * NOTE: kinda obvious but each node can have one parent only, meaning its ancestral tree chain is a single chain:
     *       one ancestor per level in the tree until & including the single root is hit, making this collection an
     *       ordered set of nested parents with one level down per collection item from root to direct parent
     *
     * @return NodeCollection<array-key, NodeInterface>
     */
    public function getAncestors(): NodeCollection;

    public function getAncestorsArray(): array;

    /**
     * returns array with non-empty, locale based labels from ancestors,
     * appended by own label, keeping the tree structure order in resulting array.
     *
     * @return string[]
     */
    public function getAncestorsLabels(string $locale): array;

    /**
     * check if this node is the category node that is the top parent in the tree.
     *
     * NOTE: if this node is used outside a tree (standalone) then there could be no parent or parent could be no category
     * in that case, since we use use @see getTreeRootNode here and compare uid's, it is possible an exception will be thrown.
     * this is intended (domain decision) since this method and @see getTreeRootNode are to be used (solely) in trees and root
     * is then always a category! that means that these method are better only called in admin-side on a node after
     * it is added to a rootNode in a formList (@see FormList::getTreeRootNode()) (or if added to another categoryNode
     * that is itself NOT added to @see FormList::getRootNode() which technically will then operate as rootNode)
     *
     * @throws MissingNodeException should fail if there is no rootNode (used outside a tree)
     */
    public function isTreeRootNode(): bool;

    /**
     * get the root level category node for this node or the node itself if it's the root node in the tree.
     *
     * NOTE: a set of nodes (questions & categories) always forms a tree structure with one single categoryNode on the top level
     * any node of any type - except for the rootNode itself - has an ancestral category node, either as a direct
     * parent or up the tree via its direct parent (like @see OptionInputNode ). the ancestral category node on the highest
     * level found is the rootNode.
     *
     * @throws MissingNodeException (used outside a tree)
     */
    public function getTreeRootNode(): CategoryNode;

    /**
     * instead of deleting, admins can (e.g. temporarily) hide a node from end-users (clients) and any end-user (client) operation.
     *
     * marks a node as hidden from end-users (clients) in a sense that it is deactivated/excluded from any operation concerning
     * anything other than operations in the admin side of a templates's questions. this includes everything from
     * displaying, exporting, checking for access, calculation of scores, dependency constraints with other nodes, etc.
     * this can have a profound impact: if this is a category node, than all direct and nested children in that node
     * will become indirectly hidden (@see NodeInterface::isReachableInTree() ) as well. a template also can only be
     * published if each of its workflow step has at least one question (@see InputNode ) that is not hidden and not
     * part of a hidden ancestor. exclusion of any end-user (client) operations goes always all the way: e.g. in calculations
     * of globals scores (@see OperableForScoring ), a hidden question operating as a score for this calculation, will
     * not be taken in account and the other score dependencies for that calculation will be 're-weighted' to make the
     * calculation work.
     * develop notice: name hidden might be open for discussion but is kept as such because of existing code
     */
    public function isHidden(): bool;

    /**
     * a node is reachable if
     *  - it is not explicitly marked as @see NodeInterface::isHidden() itself
     *  - and none of its ancestors up the tree are marked as @see NodeInterface::isHidden().
     *
     * NOTE: logic is based on fact that any node - except for rootNode - has exactly one direct parent
     */
    public function isReachableInTree(): bool;

    /**
     * get number responding to the tree depth from rootNode (1) down.
     */
    public function getDepth(): int;

    /**
     * Get the ordered number of the node in the sequence.
     */
    public function getSequence(): int;

    /**
     * Returns an array with the nodes at the same level.
     *
     * @return NodeCollection<array-key, NodeInterface>
     */
    public function getSiblings(): NodeCollection;

    /**
     * Returns an array with the at each key sequence of the node and its parents in reverse order.
     *
     * @return list<int>
     */
    public function getPosition(): array;

    public function hasNestedLabel(string $locale): bool;

    /**
     * gets the label (if any) prefixed with the glue string (dash) separated ancestral labels (if any).
     */
    public function getNestedLabel(string $locale, string $glue = ' - '): string;

    /**
     * check for write access to this node for given step uid and given role.
     *
     * IMPORTANT: this does not care or take in account if the node is @see NodeInterface::isReachableInTree() or not
     */
    public function isStepRoleWriteAccessGranted(StepInterface $step, Role $role): bool;

    /**
     * check for read access - explicitly or indirectly via write access - to this node for given step and role.
     *
     * IMPORTANT DOMAIN DECISION: write access is higher ranked than read access and grants read access if no explicit
     *            read access is granted. basically common sense: e.g. if a user (role) can answer a question (write
     *            access) in a step, it's kind of expected that that user can see that answer in the same step in
     *            export, detail views, copy views, overview, etc. or if a step only grants each user (role) only one
     *            submit (@see StepInterface::limitsEachUniqueUserToOnlyOneSubmitUnderRole() ) then that user
     *            still has to be able to see that answer, even if he/she can not alter (write) that answer anymore.
     *            in 99,9% of the case, whether read access is explicitly granted or indirectly given by write access
     *            doesn't matter. if it does matter use @see NodeInterface::isStepRoleReadOnlyAccessGranted()
     * IMPORTANT: this does not care or take in account if the node is @see NodeInterface::isReachableInTree() or not
     */
    public function isStepRoleReadAccessGranted(StepInterface $step, Role $role): bool;

    /**
     * check for explicit read only access to this node for given step and role.
     *
     * IMPORTANT: write access is higher ranked than read access and grants read access if no explicit read access
     *            is granted (@see NodeInterface::isStepRoleReadAccessGranted() ). this method ignores that
     *            write access and only checks if explicit read access is given
     * IMPORTANT: this does not care or take in account if the node is @see NodeInterface::isReachableInTree() or not
     */
    public function isStepRoleReadOnlyAccessGranted(StepInterface $step, Role $role): bool;

    /**
     * check if node is a descendant of a category node with display mode matrix.
     */
    public function hasAncestorWithDisplayModeMatrix(): bool;
}
