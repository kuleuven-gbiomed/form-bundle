<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Contract;

use KUL\FormBundle\Domain\Template\Element\Node\Input\ChoiceInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Rounding\RoundingModes;
use Symfony\Component\Form\AbstractType;

/**
 * Interface InputInterface.
 */
interface InputInterface
{
    /** @var string */
    public const KEY_TYPE = 'type';
    /** @var string */
    public const KEY_REQUIRED = 'required';
    /** @var string */
    public const KEY_DISPLAY_MODE = 'displayMode';

    /** @var string */
    public const DISPLAY_MODE_REGULAR = 'regular'; // shows the input in the regular HTML way (the default for any input)

    /** @var int */
    public const DEFAULT_SCALE = 0;
    /** @var int */
    public const DEFAULT_MIN_SCALE = 0;
    /** @var int using sensible & save PHP floating point numbers maximum precision (64 bit IEEE format) for scale */
    public const DEFAULT_MAX_SCALE = 14;
    /** @var int */
    public const DEFAULT_ROUNDING_MODE = RoundingModes::DEFAULT;

    public static function getType(): string;

    public function isRequired(): bool;

    /**
     * different modes in which the input can be rendered on client side as HTML (form field) element.
     *
     * one of the possibilities is always the default regular @see InputInterface::DISPLAY_MODE_REGULAR mode:
     * which will render the input in a HTML form in its regular HTML field (tag) mode: e.g. for a @see TextInput this
     * will be a HTML input tag with value 'text' for the HTML type attribute. for a @see NumberInput this will be a
     * HTML input tag with value 'number' for the HTML type attribute. for a @see ChoiceInput this will be a HTML select
     * tag. etc.
     *
     * @return string[]
     *
     * @psalm-return list<string>
     */
    public static function getAvailableDisplayModes(): array;

    /**
     * how is the input to be rendered on client side as HTML (form field) element.
     *
     * @see InputInterface::getAvailableDisplayModes()
     *
     * NOTE: this serves strictly a client-side code purpose and is/should never cause throwing exceptions or returning
     * no mode at all: the method (should) always defaults to the regular mode @see InputInterface::DISPLAY_MODE_REGULAR
     * NOTE: using this display mode instead of dedicated properties & methods like 'renderAsModeXXX' on each
     * input type allows to add more modes to certain input types in the future, without breaking the structure
     * etc. of each input type and/or having to add more properties and catch those missing properties in (de)serializing.
     */
    public function getDisplayMode(): string;

    /**
     * options to add to the @see AbstractType that is to be used to build, render & validate the input as a submittable field.
     *
     * @param string|null $locale optional. some input types have options that can contain translatable data (e.g. placeholders)
     */
    public function getFieldOptions(?string $locale): array;
}
