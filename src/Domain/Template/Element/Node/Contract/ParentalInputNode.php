<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Contract;

use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;

/**
 * Interface ParentalInputNode.
 *
 * an @see InputNode that can never be a child of another inputNode, is called a parental inputNode.
 * parental does not mean that this inputNode effectively has children or even can have children. it only
 * distinguishes the inputNode as being an inputNode that can/should not be used as a child of another inputNode.
 *
 * NOTE: in a node tree structure in a valid @see FormList::getRootNode(), every parental inputNode will be a child
 * of a @see CategoryNode and will be treated (rendered, submitted, processed, etc.) as an individual question inside
 * that tree . whilst if an inputNode is not a parental inputNode but a child of another - thus parental - inputNode,
 * it will not be treated as a question on itself but merely as a dependency of that other - parental - inputNode
 * all Input Nodes are by default ParentalInputNodes. (at time of writing) only @see OptionInputNode is NOT a
 * parentalInputNode and has only meaning (and is only used) as child (dependency) within a @see ChoiceInputNode
 * which uses optionNodes as its submittable answering options and other inner workings.(1)
 * in a simplified way, one could say that a parental inputNode is (usable as) an individual question, whilst a
 * non-parental inputNode is - theoretically - not (usable as) a question but rather a part of a question.
 *
 * NOTE: in formBuilding, form processing, checking access to questions, etc., we work in the first place with parental
 * inputNodes. the non-parental input nodes are only called inside a parental input node.
 * this distinction is mainly used to build filtered (and flattened to a single dimensional) collections of
 * the inputNodes as relevant questions from a node tree structure. those collections are then uses extensively for
 * further filtering for user-role access in a workflow, displaying questions in table overviews and exports, finding
 * questions, etc. it is also used in formBuilding to guarantee no non-parental inputNodes are accidentally build as
 * questions.
 *
 * (1) REMARK: it is - theoretically - possible that an @see OptionInputNode could be used as a question on itself but
 * this is not foreseen and is to be avoided.
 * it is however foreseen that an option inputNode as answering option of a @see ChoiceInputNode question could allow
 * for an additional custom (string) answer: see comment on @see ChoiceInputNode
 *
 * NOTE: naming is copied from existing code. it is not ideal as name but it helps to identify & distinguish methods
 * working with these parental inputNodes.  e.g. @see FormList::getFlattenedReachableParentalInputNodes()
 */
interface ParentalInputNode extends NodeInterface
{
    /**
     * TODO FIXME:
     *       1. the historically grown name 'parentalInputNode' should be renamed to 'Question' as that is exactly what
     *       it represents. all @see InputNode are parentalInputNodes except @see OptionInputNode (=no question on itself).
     */
    public function isLocked(): bool;

    public function getUnlockers(): OptionInputNodeCollection;

    public function isRequired(): bool;

    public function getFlowPermission(): FlowPermission;

    /**
     * @param array<string, array<string>> $unlockingQuestions
     */
    public function setMultiUnlockingQuestions(array $unlockingQuestions): void;

    /**
     * @return string[][]
     */
    public function getMultiUnlockingQuestions(): array;

    /** @return string[] */
    public function getAllMultiUnlockingOptions(): array;

    public function isMultiLockable(): bool;
}
