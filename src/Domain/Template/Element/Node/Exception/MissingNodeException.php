<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Exception;

use KUL\FormBundle\Domain\Template\Element\Node\AbstractNode;

/**
 * Class MissingNodeException.
 *
 * exceptions related to @see AbstractNode
 */
class MissingNodeException extends \Exception
{
}
