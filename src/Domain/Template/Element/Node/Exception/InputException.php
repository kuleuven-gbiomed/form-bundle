<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Exception;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;

/**
 * Class InputException.
 *
 * exception on @see InputInterface
 */
class InputException extends \Exception
{
}
