<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Exception;

/**
 * Class InputInvalidArgumentException.
 */
class InputInvalidArgumentException extends InputException
{
}
