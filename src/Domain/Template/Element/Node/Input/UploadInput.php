<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Input;

use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Validator\Constraints\SubmittedFilesAmount;

/**
 * TODO MAJOR define a list of predefined mimeTypes to allow from which admins can choose from when creating a upload
 * input 'cause now any type is allowed via @see UploadInput::allowsAllMimeTypes(). this is not as easy at it seems
 * and it's probably a better idea too define this is some app related config, instead of here (list is really, really long).
 *
 * Class UploadInput
 *
 * build, render, validate & process input for a question that offer end-users (clients) the possibility to upload files as answers
 *
 * @see     UploadInputNode
 */
final class UploadInput extends AbstractInput
{
    /** @var string */
    public const TYPE = 'upload';

    /** @var string */
    public const KEY_MIN_AMOUNT = 'minAmount';
    /** @var string */
    public const KEY_MAX_AMOUNT = 'maxAmount';
    /** @var string */
    public const KEY_MAX_FILE_SIZE = 'maxFileSize';
    /** @var string */
    public const KEY_MIME_TYPES = 'mimeTypes';
    /** @var string */
    public const KEY_EXTENSIONS = 'extensions';

    /** @var int default  filesize in bytes per file that can be uploaded */
    public const DEFAULT_MAX_FILE_SIZE = 25_000_000;
    /** @var int sensible max filesize (according to PHP) in bytes per file that can be uploaded */
    public const MAX_FILE_SIZE = 32_000_000;
    /** @var int minimal filesize in bytes per file that can be uploaded */
    public const MIN_FILE_SIZE = 1;
    /** @var string delimiter to concatenate file Uids when storing file uuids as submitted form value (a.k.a. answer) */
    public const FORM_VALUE_DELIMITER = '|';

    /**
     * minimum amount of files that must be uploaded (submitted). relates to the @see UploadInput::isRequired().
     * if input is required, then min must be greater or equal to 1. if not required, then it must be 0, since input is
     * then optional but if user chooses to submit, he/she can submit as many as up to @see UploadInput::$maxAmount.
     */
    private int $minAmount;

    /** maximum amount of files that is allowed to be uploaded (submitted) */
    private readonly int $maxAmount;

    /** maximum filesize in bytes per file that must/can be uploaded (submitted). */
    private readonly int $maxFileSize;

    /**
     * UploadInput constructor.
     *
     * @param string[] $mimeTypes
     *
     * @throws InputInvalidArgumentException
     */
    public function __construct(
        bool $required,
        string $displayMode,
        int $minAmount,
        int $maxAmount,
        int $maxFileSize,
        private readonly array $mimeTypes,
        /** @var string[] file extensions allowed. domain decision: if array empty, allow all extensions */
        private readonly array $extensions,
    ) {
        parent::__construct($required, $displayMode);

        $this->validateParameters($minAmount, $maxAmount, $maxFileSize);

        // min & max amount are validated, now adjust the min amount based on required if needed, without breaking
        // the construct, because the min & max amount are to be used in a custom constraint to validate submitted amount
        $this->minAmount = $minAmount;

        if ($required && 0 === $this->minAmount) {
            // if question is required, then min amount should be at least 1 so that user can at least comply & upload a file
            $this->minAmount = 1;
        }

        if (false === $required && $this->minAmount > 0) {
            // if question is not required, then min amount should be 0 so that user can choose to submit it blank (upload nada)
            $this->minAmount = 0;
        }

        $this->maxAmount = $maxAmount;
        $this->maxFileSize = $maxFileSize;
    }

    /**
     * @param string[] $mimeTypes
     *
     * @throws InputInvalidArgumentException
     */
    public static function fromParameters(
        bool $required = false,
        string $displayMode = self::DISPLAY_MODE_REGULAR,
        int $minAmount = 0,
        int $maxAmount = 1,
        int $maxFileSize = self::DEFAULT_MAX_FILE_SIZE,
        array $mimeTypes = [],
        array $extensions = [],
    ): self {
        return new self($required, $displayMode, $minAmount, $maxAmount, $maxFileSize, $mimeTypes, $extensions);
    }

    /**
     * @throws InputInvalidArgumentException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        return new self(
            $data[self::KEY_REQUIRED] ?? false,
            $data[self::KEY_DISPLAY_MODE] ?? self::DISPLAY_MODE_REGULAR,
            $data[self::KEY_MIN_AMOUNT] ?? 0,
            $data[self::KEY_MAX_AMOUNT] ?? 1,
            $data[self::KEY_MAX_FILE_SIZE] ?? self::DEFAULT_MAX_FILE_SIZE,
            $data[self::KEY_MIME_TYPES] ?? [],
            $data[self::KEY_EXTENSIONS] ?? []
        );
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    public function getMinAmount(): int
    {
        return $this->minAmount;
    }

    public function getMaxAmount(): int
    {
        return $this->maxAmount;
    }

    /**
     * minimum filesize is used to assure no zero byte files is uploaded.
     *
     * NOTE: min file size is not set per instance of upload questions, since there are too many use cases where
     * a min size in conjunction with mimetype and compression would frustrate end users. this method is too make
     * sure we can check and catch if zero byte files which causes issues with file handling
     */
    public function getMinFileSize(): int
    {
        return self::MIN_FILE_SIZE;
    }

    public function getMaxFileSize(): int
    {
        return $this->maxFileSize;
    }

    public function allowsMimeTypes(string $mimeType): bool
    {
        return $this->allowsAllMimeTypes() || in_array($mimeType, $this->getMimeTypes(), true);
    }

    /**
     * domain decision: if no set of mimeTypes defined, allow all.
     */
    public function allowsAllMimeTypes(): bool
    {
        return 0 === count($this->mimeTypes);
    }

    /**
     * @return string[]
     */
    public function getMimeTypes(): array
    {
        return $this->mimeTypes;
    }

    public function allowsExtensions(string $extensions): bool
    {
        return $this->allowsAllExtensions() || in_array($extensions, $this->getExtensions(), true);
    }

    /**
     * domain decision: if no set of extensions defined, allow all.
     */
    public function allowsAllExtensions(): bool
    {
        return 0 === count($this->extensions);
    }

    /**
     * @return string[]
     */
    public function getExtensions(): array
    {
        return $this->extensions;
    }

    public static function getFormValueDelimiter(): string
    {
        return static::FORM_VALUE_DELIMITER;
    }

    /**
     * dedicated helper method to extract the submitted file uids from an answer (= stored value). where that stored
     * (submitted or temporary saved) value is assumed to be under the form of a concatenated string of SavedFile
     * uids separated by @see UploadInput::FORM_VALUE_DELIMITER (or null if upload question is not required and was
     * submitted as 'blank').
     *
     * storedValue contains null or concatenated string of submitted SubmittedFile uids
     *
     * @return string[]
     */
    public static function extractSavedFileUidsFromFormValue(?string $storedValue): array
    {
        if (null === $storedValue) {
            return [];
        }

        return explode(self::FORM_VALUE_DELIMITER, $storedValue);
    }

    /**
     * dedicated helper method to make a concatenated string of (SubmittedFile) uids from an array of given
     * strings that are assumed to be uids from files that are part of an answer (= stored value) for an upload question.
     *
     * @param string[] $uids set of (SubmittedFile) uids
     *
     * returns concatenated string of given uids
     */
    public static function makeFormValueFromSavedFileUids(array $uids): string
    {
        return implode(static::FORM_VALUE_DELIMITER, $uids);
    }

    /**
     * @return array{required: bool, constraints: SubmittedFilesAmount}
     */
    public function getFieldOptions(?string $locale): array
    {
        return [
            'required' => $this->isRequired(),
            'constraints' => new SubmittedFilesAmount([
                'minAmount' => $this->getMinAmount(),
                'maxAmount' => $this->getMaxAmount(),
            ]),
        ];
    }

    public function jsonSerialize(): array
    {
        return [
            ...parent::jsonSerialize(),
            self::KEY_MIN_AMOUNT => $this->getMinAmount(),
            self::KEY_MAX_AMOUNT => $this->getMaxAmount(),
            self::KEY_MAX_FILE_SIZE => $this->getMaxFileSize(),
            self::KEY_MIME_TYPES => $this->getMimeTypes(),
            self::KEY_EXTENSIONS => $this->extensions,
        ];
    }

    /**
     * @throws InputInvalidArgumentException
     */
    private function validateParameters(int $minAmount, int $maxAmount, int $maxFileSize): void
    {
        if ($minAmount < 0) {
            // zero files allowed to upload is the absolute minimum (duh)
            throw new InputInvalidArgumentException('parameter [minAmount] must be greater than or equal to zero');
        }

        if ($maxAmount < 1) {
            // an upload question allowing no files to upload at all is pointless
            throw new InputInvalidArgumentException('parameter [maxAmount] must be greater than or equal to 1');
        }

        if ($maxAmount < $minAmount) {
            // a max equal to min is allowed, i.e. asking for exactly one file
            throw new InputInvalidArgumentException('parameter [maxAmount] must be greater than or equal to parameter [minAmount]');
        }

        if ($maxFileSize > self::MAX_FILE_SIZE) {
            throw new InputInvalidArgumentException('parameter [maxFileSize] must be less than or equal to '.((string) self::MAX_FILE_SIZE).' bytes');
        }

        if ($maxFileSize < self::MIN_FILE_SIZE) {
            throw new InputInvalidArgumentException('parameter [maxFileSize] must be greater than or equal to '.((string) self::MIN_FILE_SIZE).' bytes');
        }
    }
}
