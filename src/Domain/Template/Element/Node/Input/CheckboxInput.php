<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Input;

use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\CheckBoxInputNode;
use KUL\FormBundle\Validator\Constraints\IsChecked;

/**
 * Class CheckboxInput.
 *
 * build, render, validate & process input for a question that offer end-users (clients) the possibility to check/uncheck
 * a HTML checkbox as an answer
 *
 * @see     CheckBoxInputNode
 */
final class CheckboxInput extends AbstractInput
{
    /** @var string */
    public const TYPE = 'checkbox';

    /** @var string */
    public const KEY_DEFAULT_CHECKED = 'defaultChecked';

    /**
     * @throws InputInvalidArgumentException
     */
    protected function __construct(
        bool $required,
        string $displayMode,  // check the box (client-side ) if it was never submitted before (submitted value has priority)
        private readonly bool $defaultChecked,
    ) {
        parent::__construct($required, $displayMode);
    }

    /**
     * @throws InputInvalidArgumentException
     */
    public static function fromParameters(
        bool $required = false,
        string $displayMode = self::DISPLAY_MODE_REGULAR,
        bool $defaultChecked = false,
    ): self {
        return new static($required, $displayMode, $defaultChecked);
    }

    /**
     * @throws InputInvalidArgumentException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        return new self(
            $data[self::KEY_REQUIRED] ?? false,
            $data[self::KEY_DISPLAY_MODE] ?? self::DISPLAY_MODE_REGULAR,
            $data[self::KEY_DEFAULT_CHECKED] ?? false
        );
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    public function isDefaultChecked(): bool
    {
        return $this->defaultChecked;
    }

    /**
     * @return array{required: bool, constraints: (never[]|IsChecked[])}
     */
    public function getFieldOptions(?string $locale): array
    {
        $constraints = [];
        // if multiple is true, then constructor has asserted min & max select are available
        if ($this->isRequired()) {
            $constraints[] = new IsChecked();
        }

        return [
            'required' => $this->isRequired(),
            'constraints' => $constraints,
        ];
    }

    public function jsonSerialize(): array
    {
        return [
            ...parent::jsonSerialize(),
            self::KEY_DEFAULT_CHECKED => $this->isDefaultChecked(),
        ];
    }
}
