<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Input;

use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;

/**
 * Class OptionWithAdditionalTextInput.
 *
 * @see     OptionInputNode
 *
 * NOTE: when a user selects an option with this input (@see OptionInputNode::getInput()) as an answer for a choice question,
 * the user will be presented with an additional HTML text input field alongside this option (which on itself could be
 * either a HTML radio, HTML checkbox or HTML select option element). The user can use this text input to additionally
 * provides some text to go along with the selected option as his/her answer. this additional text input field could be
 * optional or required and is also limited with min & max length of amount of characters. the syntax in which this
 * option is then stored as answer for the choice question is no different from an 'normal' option: the uid of the
 * optionNode is used as key in the key-value pair that is added to the answer array for the choice question. but the
 * value in the pair is then not the boolean TRUE as with a 'normal' option, but instead a string (provided by user)
 * or NULL if the additional text input is not required and not given by user.
 * see docBlock on @see ChoiceInputNode for more info
 */
final class OptionWithAdditionalTextInput extends OptionInput
{
    /** @var string */
    public const TYPE = 'optionWithAdditionalText';

    /** @var string */
    public const KEY_ADDITIONAL_TEXT_MIN_LENGTH = 'additionalTextMinLength';
    /** @var string */
    public const KEY_ADDITIONAL_TEXT_MAX_LENGTH = 'additionalTextMaxLength';
    /** @var string */
    public const KEY_ADDITIONAL_TEXT_REQUIRED = 'additionalTextRequired';

    /** @var int */
    public const DEFAULT_ADDITIONAL_TEXT_MIN_LENGTH = 1;
    /** @var int */
    public const DEFAULT_ADDITIONAL_TEXT_MAX_LENGTH = 255;

    private bool $additionalTextRequired = false;
    private int $additionalTextMinLength = self::DEFAULT_ADDITIONAL_TEXT_MIN_LENGTH;
    private int $additionalTextMaxLength = self::DEFAULT_ADDITIONAL_TEXT_MAX_LENGTH;

    /**
     * @throws InputInvalidArgumentException
     */
    public static function fromParameters(
        bool $required = false,
        string $displayMode = self::DISPLAY_MODE_REGULAR,
        bool $defaultSelected = false,
        ?float $score = null,
        bool $hideLabelInRelevantChoiceDisplayMode = false,
        bool $additionalTextRequired = false,
        int $additionalTextMinLength = self::DEFAULT_ADDITIONAL_TEXT_MIN_LENGTH,
        int $additionalTextMaxLength = self::DEFAULT_ADDITIONAL_TEXT_MAX_LENGTH,
    ): self {
        self::validateParameters($additionalTextMinLength, $additionalTextMaxLength);

        $input = new self(
            $required,
            $displayMode,
            $defaultSelected,
            $score,
            $hideLabelInRelevantChoiceDisplayMode
        );

        $input->additionalTextRequired = $additionalTextRequired;
        $input->additionalTextMinLength = $additionalTextMinLength;
        $input->additionalTextMaxLength = $additionalTextMaxLength;

        return $input;
    }

    /**
     * @throws InputInvalidArgumentException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $additionalTextRequired = false;
        if (isset($data[self::KEY_ADDITIONAL_TEXT_REQUIRED])) {
            $additionalTextRequired = $data[self::KEY_ADDITIONAL_TEXT_REQUIRED];
        }

        $additionalTextMinLength = self::DEFAULT_ADDITIONAL_TEXT_MIN_LENGTH;
        if (isset($data[self::KEY_ADDITIONAL_TEXT_MIN_LENGTH])) {
            $additionalTextMinLength = $data[self::KEY_ADDITIONAL_TEXT_MIN_LENGTH];
        }

        $additionalTextMaxLength = self::DEFAULT_ADDITIONAL_TEXT_MAX_LENGTH;
        if (isset($data[self::KEY_ADDITIONAL_TEXT_MAX_LENGTH])) {
            $additionalTextMaxLength = $data[self::KEY_ADDITIONAL_TEXT_MAX_LENGTH];
        }

        $hideLabelInRelevantChoiceDisplayMode = false;
        if (isset($data[self::KEY_HIDE_LABEL_IN_RELEVANT_CHOICE_DISPLAY_MODE])) {
            $hideLabelInRelevantChoiceDisplayMode = $data[self::KEY_HIDE_LABEL_IN_RELEVANT_CHOICE_DISPLAY_MODE];
        }

        return self::fromParameters(
            $data[self::KEY_REQUIRED] ?? false,
            $data[self::KEY_DISPLAY_MODE] ?? self::DISPLAY_MODE_REGULAR,
            $data[self::KEY_DEFAULT_SELECTED] ?? false,
            $data[self::KEY_SCORE] ?? null,
            $hideLabelInRelevantChoiceDisplayMode,
            $additionalTextRequired,
            $additionalTextMinLength,
            $additionalTextMaxLength
        );
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    public function isAdditionalTextRequired(): bool
    {
        return $this->additionalTextRequired;
    }

    public function getAdditionalTextMinLength(): int
    {
        return $this->additionalTextMinLength;
    }

    public function getAdditionalTextMaxLength(): int
    {
        return $this->additionalTextMaxLength;
    }

    public function jsonSerialize(): array
    {
        return [
            ...parent::jsonSerialize(),
            self::KEY_ADDITIONAL_TEXT_REQUIRED => $this->isAdditionalTextRequired(),
            self::KEY_ADDITIONAL_TEXT_MIN_LENGTH => $this->getAdditionalTextMinLength(),
            self::KEY_ADDITIONAL_TEXT_MAX_LENGTH => $this->getAdditionalTextMaxLength(),
        ];
    }

    /**
     * @throws InputInvalidArgumentException
     */
    private static function validateParameters(int $additionalTextMinLength, int $additionalTextMaxLength): void
    {
        if ($additionalTextMinLength < 0) {
            throw new InputInvalidArgumentException('parameter [additionalTextMinLength] must be greater than or equal to zero');
        }

        if ($additionalTextMaxLength < 1) {
            // allowing not at least one character is pointless
            throw new InputInvalidArgumentException('parameter [additionalTextMaxLength] must be greater than or equal to 1');
        }

        if ($additionalTextMaxLength < $additionalTextMinLength) {
            // a max equal to min is allowed, i.e. asking for a one letter answer
            throw new InputInvalidArgumentException('parameter [additionalTextMaxLength] must be greater than or equal to parameter [additionalTextMinLength]');
        }
    }
}
