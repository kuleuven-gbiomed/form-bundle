<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Input;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ScorableInputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\ScoringParameters;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ChoiceInput.
 *
 * build, render, validate & process input for a question that offer end-user (client) a choice of answers via a HTML
 * select or set of radio's or checkboxes
 *
 * @see ChoiceInputNode
 */
final class ChoiceInput extends AbstractInput implements ScorableInputInterface
{
    /** @var string */
    public const TYPE = 'choice';

    /**
     * option to render choice input on client side as (JavaScript driven) slider: horizontal bar with marks, each
     * representing an option answer, and a handle to allow user to choose answer option by moving the handle over the bar
     * between marks. options ordered from left to right by the order of the options in @see ChoiceInputNode::getAvailableOptions().
     *
     * @var string
     */
    public const DISPLAY_MODE_SLIDER = 'slider';
    /**
     * option to render choice input on client side in a HTML table with one row with each cell containing one answer option.
     *
     * @var string
     */
    public const DISPLAY_MODE_SINGLE_TABLE_ROW = 'singleTableRow';

    /** @var string */
    public const KEY_PLACEHOLDER = 'placeholder';
    /** @var string */
    public const KEY_MULTIPLE = 'multiple';
    /** @var string */
    public const KEY_EXPANDED = 'expanded';
    /** @var string */
    public const KEY_MIN_SELECT = 'minSelect';
    /** @var string */
    public const KEY_MAX_SELECT = 'maxSelect';
    /** @var string */
    public const KEY_SORT_OPTIONS_ALPHABETICALLY = 'sortOptionsAlphabetically';
    /** @var string */
    public const KEY_SCORING_PARAMETERS = 'scoringParameters';
    /** @var bool true if user can select multiple options as answer */
    private readonly bool $multiple;
    /** @var int|null if multiple, this is a required integer */
    private ?int $minSelect = null;
    /** @var int|null if multiple, this is a required integer */
    private ?int $maxSelect = null;

    /**
     * argument $scoringParameters:
     * parameters needed for scoring purposes. for a choice question/input this is a complete separate & optional object,
     * unlike with @see NumberInput where most of the scoring parameters are derivable from the required number parameters.
     *
     * var $sortOptionsAlphabetically: ignore options order & sort alphabetical (note: some JS select plugins will sort alphabetically by default).
     * var $expanded: true if field has to be rendered as set of radios (multiple=false) or checkboxes (multiple=true).
     *
     * @throws InputInvalidArgumentException
     */
    protected function __construct(
        bool $required,
        string $displayMode,
        private readonly LocalizedOptionalString $placeholder, // UX placeholder (usually) backdrop in the HTML form field (if possible & relevant)
        bool $multiple,
        private readonly bool $expanded,
        ?int $minSelect,
        ?int $maxSelect,
        private readonly bool $sortOptionsAlphabetically,
        private readonly ScoringParameters $scoringParameters,
    ) {
        parent::__construct($required, $displayMode);

        $this->validateParameters($multiple, $displayMode, $minSelect, $maxSelect);
        $this->multiple = $multiple;
        // set min & max select to relevant value based on multiple state
        $this->minSelect = $multiple && is_numeric($minSelect) ? $minSelect : null;
        $this->maxSelect = $multiple && is_numeric($maxSelect) ? $maxSelect : null;
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    /**
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public static function fromParameters(
        bool $required = false,
        string $displayMode = self::DISPLAY_MODE_REGULAR,
        ?LocalizedOptionalString $placeholder = null,
        bool $multiple = false,
        bool $expanded = false,
        ?int $minSelect = null,
        ?int $maxSelect = null,
        bool $sortOptionsAlphabetically = false,
        ?ScoringParameters $scoringParameters = null,
    ): self {
        return new self(
            $required,
            $displayMode,
            $placeholder instanceof LocalizedOptionalString ? $placeholder : LocalizedOptionalString::fromLocaleTranslations([]),
            $multiple,
            $expanded,
            is_numeric($minSelect) ? $minSelect : null,
            is_numeric($maxSelect) ? $maxSelect : null,
            $sortOptionsAlphabetically,
            $scoringParameters instanceof ScoringParameters ? $scoringParameters : ScoringParameters::fromParameters()
        );
    }

    /**
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $placeholderData = $data[self::KEY_PLACEHOLDER] ?? [];
        $scoringParametersData = $data[self::KEY_SCORING_PARAMETERS] ?? [];

        return new self(
            $data[self::KEY_REQUIRED] ?? false,
            $data[self::KEY_DISPLAY_MODE] ?? self::DISPLAY_MODE_REGULAR,
            LocalizedOptionalString::fromJsonDecodedArray($placeholderData),
            $data[self::KEY_MULTIPLE] ?? false,
            $data[self::KEY_EXPANDED] ?? false,
            $data[self::KEY_MIN_SELECT] ?? null,
            $data[self::KEY_MAX_SELECT] ?? null,
            $data[self::KEY_SORT_OPTIONS_ALPHABETICALLY] ?? false,
            ScoringParameters::fromJsonDecodedArray($scoringParametersData)
        );
    }

    public function getLocalizedPlaceholder(): LocalizedOptionalString
    {
        return $this->placeholder;
    }

    /**
     * placeholder is a preserved keyword for choiceType with multiple-false (dropdown). It will use placeholder value
     * as placeholder attribute. Symfony allows use of placeholder expanded-true as well,
     * but this is ONLY desired for OPTIONAL radiobuttons (expanded-true multiple-false)
     * not for checkboxes (expanded-true multiple-true).
     */
    public function usePlaceholder(): bool
    {
        return !($this->isExpanded() && ($this->isMultiple() || $this->isRequired()))
            && self::DISPLAY_MODE_REGULAR === $this->getDisplayMode()
            && $this->getLocalizedPlaceholder()->hasFallback();
    }

    public function isMultiple(): bool
    {
        return $this->multiple;
    }

    public function isExpanded(): bool
    {
        return $this->expanded;
    }

    public function hasMaxSelect(): bool
    {
        return is_int($this->maxSelect);
    }

    public function getMaxSelect(): ?int
    {
        return $this->maxSelect;
    }

    public function hasMinSelect(): bool
    {
        return is_int($this->minSelect);
    }

    public function getMinSelect(): ?int
    {
        return $this->minSelect;
    }

    /**
     * @param string|null $locale optional locale for placeholder. not asserted on string, graceful fallback in use
     *
     * @throws LocalizedStringException
     */
    public function getFieldOptions(?string $locale): array
    {
        $attributes = $constraints = [];

        if ($this->usePlaceholder()) {
            $pLoc = $this->getLocalizedPlaceholder();
            $attributes['placeholder'] = (is_string($locale) && '' !== $locale) ? $pLoc->getForLocaleOrFallback($locale) : $pLoc->getFallback();
        }

        if ($this->isMultiple()) {
            // if multiple is true, then constructor has asserted that min & max select are available
            $constraints[] = new Count([
                'min' => $this->getMinSelect(),
                'max' => $this->getMaxSelect(),
            ]);
        } elseif ($this->isRequired()) {
            // if not multiple but required, use standard not blank
            $constraints[] = new NotBlank();
        }

        return [
            'required' => $this->isRequired(),
            'multiple' => $this->isMultiple(),
            'expanded' => $this->isExpanded(),
            'constraints' => $constraints,
            'attr' => $attributes,
        ];
    }

    public function sortOptionsAlphabetically(): bool
    {
        return $this->sortOptionsAlphabetically;
    }

    /**
     * a choice input can be rendered in the regular (Symfony & HTML) way as a HTML select,a set of HTML radios or
     * a set of HTML checkboxes, depending on and derived on-the-fly by the configuration of properties 'multiple'
     * and 'expanded' and their relation towards each other (determined and set in @see ChoiceInput::getFieldOptions() )
     * on top of this - indirectly determined - way of rendering, an additional client side rendering layer can
     * be used to render the choice input as either a @see ChoiceInput::DISPLAY_MODE_SLIDER
     * or as a @see ChoiceInput::DISPLAY_MODE_SINGLE_TABLE_ROW, both modes which are explicitly configured on this object.
     */
    public static function getAvailableDisplayModes(): array
    {
        return [
            ...parent::getAvailableDisplayModes(),
            self::DISPLAY_MODE_SLIDER,
            self::DISPLAY_MODE_SINGLE_TABLE_ROW,
        ];
    }

    public function getScoringParameters(): ScoringParameters
    {
        return $this->scoringParameters;
    }

    /**
     * @throws InputInvalidArgumentException
     */
    public function withScoringParameters(ScoringParameters $scoringParameters): self
    {
        return new self(
            $this->isRequired(),
            $this->getDisplayMode(),
            $this->getLocalizedPlaceholder(),
            $this->isMultiple(),
            $this->isExpanded(),
            $this->getMinSelect(),
            $this->getMaxSelect(),
            $this->sortOptionsAlphabetically(),
            $scoringParameters
        );
    }

    public function jsonSerialize(): array
    {
        return [
            ...parent::jsonSerialize(),
            self::KEY_PLACEHOLDER => $this->getLocalizedPlaceholder(),
            self::KEY_MULTIPLE => $this->isMultiple(),
            self::KEY_EXPANDED => $this->isExpanded(),
            self::KEY_MIN_SELECT => $this->hasMinSelect() ? $this->getMinSelect() : null,
            self::KEY_MAX_SELECT => $this->hasMaxSelect() ? $this->getMaxSelect() : null,
            self::KEY_SORT_OPTIONS_ALPHABETICALLY => $this->sortOptionsAlphabetically(),
            self::KEY_SCORING_PARAMETERS => $this->getScoringParameters(),
        ];
    }

    public function useSingleRowView(): bool
    {
        return self::DISPLAY_MODE_SINGLE_TABLE_ROW === $this->getDisplayMode();
    }

    /**
     * @param int|null $maxSelect
     *
     * NOTE: if displayMode is requested to show choice question as {@see ChoiceInput::DISPLAY_MODE_SLIDER} and multiple
     * is set to true, then slider mode can not be allowed: a slider is a non-multiple select tool, it is not possible
     * to use a Javascript driven slider that allows for multiple options to be selected on a slider bar
     *
     * @throws InputInvalidArgumentException
     */
    private function validateParameters(
        bool $multiple,
        string $displayMode,
        ?int $minSelect,
        ?int $maxSelect,
    ): void {
        if ($multiple && null === $minSelect) {
            throw new InputInvalidArgumentException('parameter [minSelect] must be a numeric (integer) value if choice allows multiple select');
        }

        if ($multiple && null === $maxSelect) {
            throw new InputInvalidArgumentException('parameter [maxSelect] must be a numeric (integer) value if choice allows multiple select');
        }

        if ($multiple && ($minSelect > $maxSelect)) {
            throw new InputInvalidArgumentException('parameter [minSelect] for choiceInput must be less than or equal to [maxSelect] if choice allows multiple select');
        }

        // don't allow slider mode if input is configured as multiple
        if ($multiple && self::DISPLAY_MODE_SLIDER === $displayMode) {
            throw new InputInvalidArgumentException('parameter [displayMode] for choiceInput can not be '.self::DISPLAY_MODE_SLIDER.' if parameter [multiple] is set as TRUE: a choice question allowing multiple answers cannot be rendered as slider on client side');
        }
    }
}
