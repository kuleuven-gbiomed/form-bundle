<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Input;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Validator\Constraints\MultiLineTextLength;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class TextInput.
 *
 * build, render, validate & process input for a question that offer end-users (clients) the possibility to give
 * a string value as an answer
 *
 * @see     TextInputNode
 */
final class TextInput extends AbstractInput
{
    /** @var string */
    public const TYPE = 'text';

    /** @var string */
    public const KEY_PLACEHOLDER = 'placeholder';
    /** @var string */
    public const KEY_MULTI_LINE = 'multiLine';
    /** @var string */
    public const KEY_MIN_LENGTH = 'minLength';
    /** @var string */
    public const KEY_MAX_LENGTH = 'maxLength';
    /** @var string */
    public const KEY_PREDEFINED_ANSWERS = 'predefinedAnswers';

    /** @var string option to render text(area) input on client side in a wysiwyg/text editor. */
    public const DISPLAY_MODE_WYSIWYG = 'wysiwyg';

    /** @var int */
    public const DEFAULT_LENGTH_MIN = 1;
    /** @var int */
    public const DEFAULT_LENGTH_MAX_TEXT = 255;
    /** @var int */
    public const DEFAULT_LENGTH_MAX_TEXTAREA = 12000;
    /** @var int sensible max amount of chars allowed to configure on a text question (65000 = 120+ pages) */
    public const ALLOWED_LENGTH_MAX_TEXTAREA = 65000;

    /** min amount of characters allowed */
    private readonly int $minLength;
    /** max amount of characters allowed */
    private readonly int $maxLength;

    /**
     * @param string[] $predefinedAnswers
     *
     * @psalm-param list<string> $predefinedAnswers
     *
     * var multiline: allow big(ger) text to be typed in the form field; determines indirectly if HTML text vs. textarea is needed.
     *
     * @throws InputInvalidArgumentException
     */
    public function __construct(
        bool $required,
        string $displayMode,
        private readonly LocalizedOptionalString $placeholder,
        private readonly bool $multiLine,
        int $minLength,
        int $maxLength,
        private readonly array $predefinedAnswers,
    ) {
        parent::__construct($required, $displayMode);

        $this->validateParameters($minLength, $maxLength);
        $this->minLength = $minLength;
        $this->maxLength = $maxLength;
    }

    /**
     * @param list<string> $predefinedAnswers
     *
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public static function fromParameters(
        bool $required = false,
        string $displayMode = self::DISPLAY_MODE_REGULAR,
        ?LocalizedOptionalString $placeholder = null,
        bool $multiLine = false,
        int $minLength = self::DEFAULT_LENGTH_MIN,
        int $maxLength = self::DEFAULT_LENGTH_MAX_TEXT,
        array $predefinedAnswers = [],
    ): self {
        $placeholder = $placeholder instanceof LocalizedOptionalString ? $placeholder : LocalizedOptionalString::fromLocaleTranslations([]);

        return new self($required, $displayMode, $placeholder, $multiLine, $minLength, $maxLength, $predefinedAnswers);
    }

    /**
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $placeholderData = $data[self::KEY_PLACEHOLDER] ?? [];

        return new self(
            $data[self::KEY_REQUIRED] ?? false,
            $data[self::KEY_DISPLAY_MODE] ?? self::DISPLAY_MODE_REGULAR,
            LocalizedOptionalString::fromJsonDecodedArray($placeholderData),
            $data[self::KEY_MULTI_LINE] ?? false,
            $data[self::KEY_MIN_LENGTH] ?? self::DEFAULT_LENGTH_MIN,
            $data[self::KEY_MAX_LENGTH] ?? self::DEFAULT_LENGTH_MAX_TEXT,
            $data[self::KEY_PREDEFINED_ANSWERS] ?? []
        );
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    public function getLocalizedPlaceholder(): LocalizedOptionalString
    {
        return $this->placeholder;
    }

    public function usePlaceholder(): bool
    {
        return $this->getLocalizedPlaceholder()->hasFallback();
    }

    /**
     * does dis question expect big(ger) text to be typed in the form field by user so that at a bigger input field
     * (i.e. a HTML textarea instead of norma HTML 'text' input) will be needed.
     */
    public function isMultiLine(): bool
    {
        return $this->multiLine;
    }

    public function getMinLength(): int
    {
        return $this->minLength;
    }

    public function getMaxLength(): int
    {
        return $this->maxLength;
    }

    public function getFieldOptions(?string $locale): array
    {
        $attributes = $constraints = [];

        if ($this->usePlaceholder()) {
            $pLoc = $this->getLocalizedPlaceholder();
            $attributes['placeholder'] = (is_string($locale) && '' !== $locale) ? $pLoc->getForLocaleOrFallback($locale) : $pLoc->getFallback();
        }

        if ($this->isRequired()) {
            $constraints[] = new NotBlank();
        }

        if ($this->isMultiLine()) {
            // validate the submitted string value without HTML tags and/or line(breaks) from textarea and wysiwyg
            $constraints[] = new MultiLineTextLength(['min' => $this->getMinLength(), 'max' => $this->getMaxLength()]);

            // avoid malicious submitted string values overloaded with thousands of intentionally useless HTML tags.
            // assuming a (big) string/text with HTML will never be 3 times as big as its HTML purified version, add
            // an additional standard Length Constraint with adjusted max:
            // if max is smaller than 1/3 of default (+/- 20000 chars) or set max === default max, the default max is used.
            // if set max is between 1/3 of default max and default max or bigger than default max, 3 times set max is used
            $defaultMax = self::DEFAULT_LENGTH_MAX_TEXTAREA;
            $overloadMax = $this->getMaxLength() > ($defaultMax / 3) ? $this->getMaxLength() * 3 : $defaultMax;
            // min on additional after-purifying constraint needs to be 0 to avoid showing the 'too short' validation
            // message twice on the field if only 1 or very few characters are submitted
            $constraints[] = new Length(['min' => 0, 'max' => $overloadMax]);
        } else {
            $constraints[] = new Length(['min' => $this->getMinLength(), 'max' => $this->getMaxLength()]);
        }

        return [
            'required' => $this->isRequired(),
            'constraints' => $constraints,
            'attr' => $attributes,
        ];
    }

    public static function getAvailableDisplayModes(): array
    {
        return [...parent::getAvailableDisplayModes(), ...[
            self::DISPLAY_MODE_WYSIWYG,
        ]];
    }

    /**
     * @return string[]
     *
     * @psalm-return list<string>
     */
    public function getPredefinedAnswers(): array
    {
        return $this->predefinedAnswers;
    }

    public function hasPredefinedAnswers(): bool
    {
        return 0 !== count($this->getPredefinedAnswers());
    }

    /**
     * dedicated helper method to check if display in a wysiwyg/text editor is required.
     *
     * NOTE: it's kind of logical that a wysiwyg is relevant if the text question allows for bigger text (i.e.
     * if its to be rendered as textarea via @see TextInput::isMultiLine() ). using a wysiwyg if not a lot of
     * text is allowed is kind of illogical, but it's not forbidden. admin choice ;)
     */
    public function useWysiwyg(): bool
    {
        return self::DISPLAY_MODE_WYSIWYG === $this->getDisplayMode();
    }

    public function jsonSerialize(): array
    {
        return [
            ...parent::jsonSerialize(),
            self::KEY_PLACEHOLDER => $this->getLocalizedPlaceholder(),
            self::KEY_MULTI_LINE => $this->isMultiLine(),
            self::KEY_MIN_LENGTH => $this->getMinLength(),
            self::KEY_MAX_LENGTH => $this->getMaxLength(),
            self::KEY_PREDEFINED_ANSWERS => $this->getPredefinedAnswers(),
        ];
    }

    /**
     * @throws InputInvalidArgumentException
     */
    private function validateParameters(int $minLength, int $maxLength): void
    {
        if ($minLength < 0) {
            throw new InputInvalidArgumentException('parameter [minLength] must be greater than or equal to zero');
        }

        if ($maxLength < 1) {
            // allowing not at least one character is pointless
            throw new InputInvalidArgumentException('parameter [maxLength] must be greater than or equal to 1');
        }

        if ($maxLength > self::ALLOWED_LENGTH_MAX_TEXTAREA) {
            throw new InputInvalidArgumentException('parameter [maxLength] is too big, it must be less than or equal to '.((string) self::ALLOWED_LENGTH_MAX_TEXTAREA));
        }

        if ($maxLength < $minLength) {
            // a max equal to min is allowed, i.e. asking for a one letter answer
            throw new InputInvalidArgumentException('parameter [maxLength] must be greater than or equal to parameter [minLength]');
        }
    }
}
