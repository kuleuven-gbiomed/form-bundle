<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Input;

use KUL\FormBundle\Client\Manage\IsoDateConverter;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class DateInput.
 *
 * build, render, validate & process input for a question that offer end-users (clients) the possibility to give
 * a date value as an answer
 *
 * @see     DateInputNodeInputNode
 */
final class DateInput extends AbstractInput
{
    /* -------------------------------------------------------------------------------------------------------- const */
    /** @var string */
    public const TYPE = 'date';

    /** @var string */
    public const KEY_PLACEHOLDER = 'placeholder';

    /** @var string */
    public const KEY_FORMATMASK = 'formatMask';

    /* ----------------------------------------------------------------------------------------------------- defaults */
    /** @var string */
    public const DEFAULT_FORMAT_MASK = 'dd/mm/yy';

    /* -------------------------------------------------------------------------------------------------- constructor */

    /**
     * @throws InputInvalidArgumentException
     */
    public function __construct(
        bool $required,
        string $displayMode,
        /** @var LocalizedOptionalString optional placeholder as backdrop in the HTML text/textarea input field */
        protected LocalizedOptionalString $placeholder,
        /** @var string the formatting mask to render the raw datestring to the user */
        protected string $formatMask,
    ) {
        parent::__construct($required, $displayMode);
    }

    /* --------------------------------------------------------------------------------------------- static factories */
    /**
     * @throws InputInvalidArgumentException
     * @throws LocalizedStringException
     */
    public static function fromParameters(
        bool $required = false,
        string $displayMode = self::DISPLAY_MODE_REGULAR,
        ?LocalizedOptionalString $placeholder = null,
        string $formatMask = self::DEFAULT_FORMAT_MASK,
    ): self {
        $placeholder = $placeholder instanceof LocalizedOptionalString ? $placeholder : LocalizedOptionalString::fromLocaleTranslations([]);

        return new self($required, $displayMode, $placeholder, $formatMask);
    }

    /* ------------------------------------------------------------------------------------------------------ getters */

    public static function getType(): string
    {
        return self::TYPE;
    }

    public function getFormatMask(): string
    {
        return $this->formatMask;
    }

    public function getLocalizedPlaceholder(): LocalizedOptionalString
    {
        return $this->placeholder;
    }

    public function usePlaceholder(): bool
    {
        return $this->getLocalizedPlaceholder()->hasFallback();
    }

    /* ---------------------------------------------------------------------------------------- get the field options */
    public function getFieldOptions(?string $locale = null): array
    {
        $attributes = [];
        $constraints = [];

        if ($this->usePlaceholder()) {
            $pLoc = $this->getLocalizedPlaceholder();
            $attributes['placeholder'] = (is_string($locale) && '' !== $locale) ? $pLoc->getForLocaleOrFallback($locale) : $pLoc->getFallback();
        }

        if ($this->isRequired()) {
            $constraints[] = new NotBlank();
        }

        $constraints[] = new DateTime(['format' => IsoDateConverter::convertToPhpFormatMask($this->getFormatMask())]);
        $constraints[] = new Length(['min' => mb_strlen($this->getFormatMask()) + 2, 'max' => (mb_strlen($this->getFormatMask()) + 2)]);

        return [
            'required' => $this->isRequired(),
            'constraints' => $constraints,
            'attr' => $attributes,
        ];
    }

    /* ------------------------------------------------------------------------------------------ json input & output */

    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $placeholderData = $data[self::KEY_PLACEHOLDER] ?? [];

        return new self(
            $data[self::KEY_REQUIRED] ?? false,
            $data[self::KEY_DISPLAY_MODE] ?? self::DISPLAY_MODE_REGULAR,
            LocalizedOptionalString::fromJsonDecodedArray($placeholderData),
            $data[self::KEY_FORMATMASK] ?? self::DEFAULT_FORMAT_MASK
        );
    }

    public function jsonSerialize(): array
    {
        return [
            ...parent::jsonSerialize(),
            self::KEY_PLACEHOLDER => $this->getLocalizedPlaceholder(),
            self::KEY_FORMATMASK => $this->getFormatMask(),
        ];
    }
}
