<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Input;

use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;

/**
 * Class OptionInput.
 *
 * @see     OptionInputNode
 * @see     ChoiceInputNode
 *
 * An @see OptionInputNode is used as answering option for @see ChoiceInputNode questions. this class provides
 * the functionality on how such an option is to be rendered, validated and processed as answer to a choice question.
 *
 * NOTE: options are rendered as either HTML select option, HTML radio or HTML checkbox, depending on how the choice
 * question they depend on  is configured. when a user selects an option (or more than one if the choice
 * allows multiple) as answer for the choice question, that option is added as answer for the choice (i.e. to the answer
 * array for the choice).
 * see docBlock on @see ChoiceInputNode for more info on storing syntax, examples, etc.
 *
 * NOTE: this option is basically only selectable as answer for a choice question.
 * there is a special option configuration @see OptionWithAdditionalTextInput extending this option configuration
 * that works in the same way but also provides the possibility for end-users to add additional text input alongside
 * the option when selected as answer for a choice question.
 */
class OptionInput extends AbstractInput
{
    /** @var string */
    public const TYPE = 'option';

    /** @var string */
    public const KEY_DEFAULT_SELECTED = 'defaultSelected';
    /** @var string */
    public const KEY_SCORE = 'score';
    /** @var string */
    public const KEY_HIDE_LABEL_IN_RELEVANT_CHOICE_DISPLAY_MODE = 'hideLabelInRelevantChoiceDisplayMode';

    /**
     * OptionInput constructor.
     *
     * argument $defaultSelected: mark option as selected in parental choice question if it was never submitted before (submitted value has priority)
     * argument $displayMode: determined by the @see ChoiceInputNode::getInput() on which this depends
     * argument $hideLabelInRelevantChoiceDisplayMode:
     * hide label if option rendered as part of a choiceQuestion in @see ChoiceInput::DISPLAY_MODE_SLIDER
     * argument $hideLabelInRelevantChoiceDisplayMode:
     * low priority, non-breaking option to hide the label in some specific rendering modes of the parent choice
     * question that uses this option. e.g. as part of a choiceQuestion in @see ChoiceInput::DISPLAY_MODE_SLIDER.
     *
     * score: optional value for usage in the parental choice question as a scoring question
     * note: if you pass an int into a method argument that is typed as float (as score here), that int will be silently cast to a float
     *
     * @see OptionInput::shouldHideLabel()
     *
     * @throws InputInvalidArgumentException
     */
    final protected function __construct(
        bool $required,
        string $displayMode,
        private readonly bool $defaultSelected,
        private readonly ?float $score,
        private readonly bool $hideLabelInRelevantChoiceDisplayMode,
    ) {
        parent::__construct($required, $displayMode);
    }

    /**
     * @throws InputInvalidArgumentException
     */
    public static function fromParameters(
        bool $required = false,
        string $displayMode = self::DISPLAY_MODE_REGULAR,
        bool $defaultSelected = false,
        ?float $score = null,
        bool $hideLabelInRelevantChoiceDisplayMode = false,
    ): self {
        return new self($required, $displayMode, $defaultSelected, $score, $hideLabelInRelevantChoiceDisplayMode);
    }

    /**
     * @throws InputInvalidArgumentException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $hideLabelInRelevantChoiceDisplayMode = false;
        if (isset($data[self::KEY_HIDE_LABEL_IN_RELEVANT_CHOICE_DISPLAY_MODE])) {
            $hideLabelInRelevantChoiceDisplayMode = $data[self::KEY_HIDE_LABEL_IN_RELEVANT_CHOICE_DISPLAY_MODE];
        }

        return new static(
            $data[self::KEY_REQUIRED] ?? false,
            $data[self::KEY_DISPLAY_MODE] ?? self::DISPLAY_MODE_REGULAR,
            $data[self::KEY_DEFAULT_SELECTED] ?? false,
            $data[self::KEY_SCORE] ?? null,
            $hideLabelInRelevantChoiceDisplayMode
        );
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    public function isDefaultSelected(): bool
    {
        return $this->defaultSelected;
    }

    public function hasScore(): bool
    {
        return is_float($this->score);
    }

    /**
     * @throws \BadMethodCallException
     */
    public function getScore(): float
    {
        if (is_float($this->score)) {
            return $this->score;
        }

        throw new \BadMethodCallException('no score set');
    }

    /**
     * a label is required on any option @see OptionInputNode::getLabel(), but in rare cases, admins want to be able to
     * hide the label. at time of writing the only edge case for this is if the option is part of a set of options
     * that are rendered as part of a choiceQuestion which is displayed as a 'slider' (@see ChoiceInput::DISPLAY_MODE_SLIDER).
     *
     * NOTE: it does not make sense to have an option without label. it would mean that if such an option is used as
     * answer for a choice question and the answer must be displayed, exported, etc., then nothing would be displayed
     * and cause confusion as it will seem as if no answer was given. having an option without label is a very rare edge
     * case (actually a request by one admin user for a one time usage), so to avoid breaking consistency on nodes
     * and having to do heavy checks just to allow this weird edge case, this option to hide the label is a compromise:
     * admins making this option will have to supply a label but it will be hidden in slider mode, yet can be shown in
     * exports, answer display, etc.
     */
    public function shouldHideLabel(): bool
    {
        return $this->hideLabelInRelevantChoiceDisplayMode;
    }

    public function jsonSerialize(): array
    {
        return [
            ...parent::jsonSerialize(),
            self::KEY_DEFAULT_SELECTED => $this->isDefaultSelected(),
            self::KEY_SCORE => $this->hasScore() ? $this->getScore() : null,
            self::KEY_HIDE_LABEL_IN_RELEVANT_CHOICE_DISPLAY_MODE => $this->shouldHideLabel(),
        ];
    }
}
