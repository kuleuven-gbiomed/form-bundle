<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Input;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ScorableInputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Rounding\RoundingModes;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\ScoringParameters;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Validator\Constraints\NumberScale;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

/**
 * Class NumberInput.
 *
 * build, render, validate & process input for a question that offer end-users (clients) the possibility to give
 * a numeric value as an answer
 *
 * @see     NumberInputNode
 */
final class NumberInput extends AbstractInput implements ScorableInputInterface
{
    /** @var string */
    public const TYPE = 'number';

    /** @var string */
    public const KEY_PLACEHOLDER = 'placeholder';
    /** @var string */
    public const KEY_MIN = 'min';
    /** @var string */
    public const KEY_MAX = 'max';
    /** @var string */
    public const KEY_ROUNDING_MODE = 'roundingMode';
    /** @var string */
    public const KEY_SCALE = 'scale';
    /** @var string */
    public const KEY_STEP_INTERVAL = 'stepInterval';
    /** @var string */
    public const KEY_SHOW_MAX_AS_INPUT_ADDON = 'showMaxAsInputAddon';
    /** @var string */
    public const KEY_SHOW_MIN_AS_INPUT_ADDON = 'showMinAsInputAddon';
    /** @var string */
    public const DATA_TYPE_INTEGER = 'integer';

    /**
     * option to render choice input on client side as (JavaScript driven) slider: horizontal bar and a handle to allow
     * user to choose a number value by moving the handle over the bar between the @see NumberInput::getMin() on the left
     * and the @see NumberInput::getMax() on the right, with the handle moving from (unmarked) positions each corresponding
     * to a specific number value that is determined automatically  by the @see NumberInput::getStepInterval().
     *
     * @var string
     */
    public const DISPLAY_MODE_SLIDER = 'slider';

    // I think these are strange values for DEFAULT_MIN_FLOAT and DEFAULT_MAX_FLOAT.
    /** @var float using sensible & (windows) save PHP_INT_MAX (-1) as default minimum */
    public const DEFAULT_MIN_FLOAT = -2_147_483_646.0;
    public const DEFAULT_MIN = -2_147_483_646;
    /** @var float using sensible & (windows) save PHP_INT_MAX (-1) as default maximum */
    public const DEFAULT_MAX_FLOAT = 2_147_483_646.0;
    public const DEFAULT_MAX = 2_147_483_646;
    /** @var float 0 = 'any' in HTML attribute step. otherwise any number > 0 will be required */
    public const DEFAULT_STEP_INTERVAL = 0.0;

    /** minimum value allowed to submit */
    private readonly float $min;
    /** maximum value allowed to submit */
    private readonly float $max;
    /** allowed amount of decimals */
    private readonly int $scale;
    /** limits values to step-wise intervals between min & max (cfr. HTML <input step="number|any"> */
    private readonly float $stepInterval;
    /** parameters needed for scoring purposes. most are derivable from the other number parameters */
    private readonly ScoringParameters $scoringParameters;
    /**
     * how the submitted value is to be rounded, in conjunction with scale.
     *
     * @phpstan-var RoundingModes::ROUND_* $roundingMode
     */
    private readonly int $roundingMode;

    /**
     * @phpstan-param RoundingModes::ROUND_* $roundingMode
     */
    protected function __construct(
        bool $required,
        string $displayMode,
        private readonly LocalizedOptionalString $placeholder,  // UX placeholder backdrop in the HTML input number field
        float $min,
        float $max,
        int $roundingMode,
        int $scale,
        float $stepInterval,
        private readonly bool $showMaxAsInputAddon, // whether to display the max inside the HTML input field as an extra (right side) addon label
        private readonly bool $showMinAsInputAddon, // whether to display the min inside the HTML input field as an extra (left side) addon label
        ?float $passScore,
        bool $showAutoCalculateBtn,
        bool $allowNonAutoCalculatedValue,
    ) {
        parent::__construct($required, $displayMode);

        $this->roundingMode = RoundingModes::extractAllowedMode($roundingMode);

        $this->validateParameters($min, $max, $scale, $stepInterval);
        $this->min = $min;
        $this->max = $max;
        $this->scale = $scale;
        $this->stepInterval = $stepInterval;

        $this->scoringParameters = ScoringParameters::fromParameters(
            $this->min,
            $this->max,
            $passScore,
            $this->roundingMode,
            $this->scale,
            $showAutoCalculateBtn,
            $allowNonAutoCalculatedValue
        );
    }

    /**
     * @phpstan-param RoundingModes::ROUND_* $roundingMode
     */
    public static function fromParameters(
        bool $required = false,
        string $displayMode = self::DISPLAY_MODE_REGULAR,
        ?LocalizedOptionalString $placeholder = null,
        float $min = self::DEFAULT_MIN_FLOAT,
        float $max = self::DEFAULT_MAX_FLOAT,
        int $roundingMode = self::DEFAULT_ROUNDING_MODE,
        int $scale = self::DEFAULT_SCALE,
        float $stepInterval = self::DEFAULT_STEP_INTERVAL,
        bool $showMaxAsInputAddon = false,
        bool $showMinAsInputAddon = false,
        ?float $passScore = null,
        bool $showAutoCalculateBtn = ScoringParameters::DEFAULT_SHOW_AUTO_CALCULATE_BTN,
        bool $allowNonAutoCalculatedValue = ScoringParameters::DEFAULT_ALLOW_NON_AUTO_CALCULATED_VALUE,
    ): self {
        return new self(
            $required,
            $displayMode,
            $placeholder instanceof LocalizedOptionalString ? $placeholder : LocalizedOptionalString::fromLocaleTranslations([]),
            $min,
            $max,
            $roundingMode,
            $scale,
            $stepInterval,
            $showMaxAsInputAddon,
            $showMinAsInputAddon,
            $passScore,
            $showAutoCalculateBtn,
            $allowNonAutoCalculatedValue
        );
    }

    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $placeholderData = $data[self::KEY_PLACEHOLDER] ?? [];

        $showAutoCalBtn = ScoringParameters::DEFAULT_SHOW_AUTO_CALCULATE_BTN;
        if (isset($data[ScoringParameters::KEY_SHOW_AUTO_CALCULATE_BTN])) {
            $showAutoCalBtn = $data[ScoringParameters::KEY_SHOW_AUTO_CALCULATE_BTN];
        }

        $allowNonCalValue = ScoringParameters::DEFAULT_ALLOW_NON_AUTO_CALCULATED_VALUE;
        if (isset($data[ScoringParameters::KEY_ALLOW_NON_AUTO_CALCULATED_VALUE])) {
            $allowNonCalValue = $data[ScoringParameters::KEY_ALLOW_NON_AUTO_CALCULATED_VALUE];
        }

        return new self(
            $data[self::KEY_REQUIRED] ?? false,
            $data[self::KEY_DISPLAY_MODE] ?? self::DISPLAY_MODE_REGULAR,
            LocalizedOptionalString::fromJsonDecodedArray($placeholderData),
            $data[self::KEY_MIN] ?? self::DEFAULT_MIN,
            $data[self::KEY_MAX] ?? self::DEFAULT_MAX,
            $data[self::KEY_ROUNDING_MODE] ?? self::DEFAULT_ROUNDING_MODE,
            $data[self::KEY_SCALE] ?? self::DEFAULT_SCALE,
            $data[self::KEY_STEP_INTERVAL] ?? self::DEFAULT_STEP_INTERVAL,
            $data[self::KEY_SHOW_MAX_AS_INPUT_ADDON] ?? false,
            $data[self::KEY_SHOW_MIN_AS_INPUT_ADDON] ?? false,
            $data[ScoringParameters::KEY_PASS_SCORE] ?? null,
            $showAutoCalBtn,
            $allowNonCalValue
        );
    }

    public static function getType(): string
    {
        return self::TYPE;
    }

    public function getLocalizedPlaceholder(): LocalizedOptionalString
    {
        return $this->placeholder;
    }

    public function usePlaceholder(): bool
    {
        return self::DISPLAY_MODE_REGULAR === $this->getDisplayMode() && $this->getLocalizedPlaceholder()->hasFallback();
    }

    public function getMin(): float
    {
        return $this->min;
    }

    public function getMax(): float
    {
        return $this->max;
    }

    /**
     * @phpstan-return RoundingModes::ROUND_*
     */
    public function getRoundingMode(): int
    {
        return $this->roundingMode;
    }

    public function getScale(): int
    {
        return $this->scale;
    }

    public function showMaxAsInputAddon(): bool
    {
        return $this->showMaxAsInputAddon;
    }

    public function showMinAsInputAddon(): bool
    {
        return $this->showMinAsInputAddon;
    }

    public function getStepInterval(): float
    {
        return $this->stepInterval;
    }

    public function getDataType(): string
    {
        return $this->getScale() > 0 ? 'number' : 'integer';
    }

    public function getScoringParameters(): ScoringParameters
    {
        return $this->scoringParameters;
    }

    public function withScoringParameters(ScoringParameters $scoringParameters): self
    {
        return new self(
            $this->isRequired(),
            $this->getDisplayMode(),
            $this->getLocalizedPlaceholder(),
            $scoringParameters->hasMinScore() ? $scoringParameters->getMinScore() : $this->getMin(),
            $scoringParameters->hasMaxScore() ? $scoringParameters->getMaxScore() : $this->getMax(),
            $scoringParameters->hasRoundingMode() ? $scoringParameters->getRoundingMode() : $this->getRoundingMode(),
            $scoringParameters->hasScale() ? $scoringParameters->getScale() : $this->getScale(),
            $this->getStepInterval(),
            $this->showMaxAsInputAddon(),
            $this->showMinAsInputAddon(),
            $scoringParameters->hasPassScore() ? $scoringParameters->getPassScore() : null,
            $scoringParameters->showAutoCalculateBtn(),
            $scoringParameters->allowNonAutoCalculatedValue()
        );
    }

    /**
     * @return array{required: bool, invalid_message: string, constraints: Constraint[], attr: array{min: float, max: float, step: string, use_max_addon?: true, use_min_addon?: true, placeholder?: string}}
     *
     * @throws LocalizedStringException
     */
    public function getFieldOptions(?string $locale): array
    {
        $attributes = [
            'min' => $this->getMin(),
            'max' => $this->getMax(),
            'step' => $this->getHtmlAttributeStepValue(),
            // TODO 'type' => $this->getDisplayMode() === self::DISPLAY_MODE_SLIDER ? 'range' : 'number' ....
        ];

        if ($this->showMaxAsInputAddon()) {
            $attributes['use_max_addon'] = true;
        }

        if ($this->showMinAsInputAddon()) {
            $attributes['use_min_addon'] = true;
        }

        if ($this->usePlaceholder()) {
            $pLoc = $this->getLocalizedPlaceholder();
            $attributes['placeholder'] = (is_string($locale) && '' !== $locale) ? $pLoc->getForLocaleOrFallback($locale) : $pLoc->getFallback();
        }

        $constraints = [
            new Range(['min' => $this->getMin(), 'max' => $this->getMax()]),
            new NumberScale(['maxScale' => $this->getScale()]),
            // TODO use a constraint for stepInterval
        ];

        if ($this->isRequired()) {
            $constraints[] = new NotBlank();
        }

        return [
            'required' => $this->isRequired(),
            'invalid_message' => 'invalidNumber',
            'constraints' => $constraints,
            'attr' => $attributes,
        ];
    }

    public static function getAvailableDisplayModes(): array
    {
        return [...parent::getAvailableDisplayModes(), ...[
            self::DISPLAY_MODE_SLIDER,
        ]];
    }

    public function jsonSerialize(): array
    {
        $scoreParams = $this->getScoringParameters();

        return [
            ...parent::jsonSerialize(),
            self::KEY_PLACEHOLDER => $this->getLocalizedPlaceholder(),
            self::KEY_MIN => $this->getMin(),
            self::KEY_MAX => $this->getMax(),
            self::KEY_ROUNDING_MODE => $this->getRoundingMode(),
            self::KEY_SCALE => $this->getScale(),
            self::KEY_STEP_INTERVAL => $this->getStepInterval(),
            self::KEY_SHOW_MAX_AS_INPUT_ADDON => $this->showMaxAsInputAddon(),
            self::KEY_SHOW_MIN_AS_INPUT_ADDON => $this->showMinAsInputAddon(),
            ScoringParameters::KEY_PASS_SCORE => $scoreParams->hasCustomPassScore() ? $scoreParams->getCustomPassScore() : null,
            ScoringParameters::KEY_SHOW_AUTO_CALCULATE_BTN => $scoreParams->showAutoCalculateBtn(),
            ScoringParameters::KEY_ALLOW_NON_AUTO_CALCULATED_VALUE => $scoreParams->allowNonAutoCalculatedValue(),
        ];
    }

    /**
     * @throws InputInvalidArgumentException
     */
    private function validateParameters(
        float $min,
        float $max,
        int $scale,
        float $stepInterval,
    ): void {
        if ($max < $min) {
            // a max equal to min is allowed, e.g. to create a required (human) check question like '1 + 1 = ?' ;)
            throw new InputInvalidArgumentException('parameter [max] must be greater than or equal to parameter [min]');
        }

        if ($scale < self::DEFAULT_MIN_SCALE || $scale > self::DEFAULT_MAX_SCALE) {
            throw new InputInvalidArgumentException('parameter [scale] for NumberInput must range between '.((string) self::DEFAULT_MIN_SCALE).' and '.((string) self::DEFAULT_MAX_SCALE).'. Got ['.((string) $scale).']');
        }

        if ($stepInterval < 0) {
            throw new InputInvalidArgumentException('parameter [stepInterval] must be a greater than or equal to zero');
        }

        $numericDiff = abs($max - $min);
        if ($stepInterval > $numericDiff) {
            throw new InputInvalidArgumentException('parameter [stepInterval] must be a less than or equal to ['.((string) $numericDiff).'] which is the absolute difference between min and max.');
        }
    }

    /**
     * build the numeric string values for the HTML input attributes 'min', 'max' and 'step': this requires some fine
     * tuning based on data type and @see getScale(), especially if data type is a number. in case no explicit
     * stepInterval is set (i.e. its 0.0) and the data type is number (i.e. decimals allowed), we can return 'any' as
     * a valid value for 'step'. however, that would allow browsers to let any amount of decimals pass, thus ignoring
     * the scale set on this input. which means the end-user (client) will have to submit and learn about decimal limitation
     * via server side validation instead of the much faster build-in client side validation in some browsers.
     * hence building the minimal step interval manually, based and limited by scale is better.
     *
     * NOTE: on construct, the step interval is asserted to be no greater than the absolute difference between max & min
     */
    public function getHtmlAttributeStepValue(): string
    {
        // if integer, no point in using floating number interval; check for custom interval or use the minimum of 1
        if ('integer' === $this->getDataType()) {
            $stepInterval = (int) $this->getStepInterval();

            return $stepInterval > 0 ? (string) $stepInterval : '1';
        }

        $stepInterval = $this->getStepInterval();

        if (0.0 === $stepInterval) {
            // if no explicit interval set, assert browsers will only allow the smallest floating number step interval
            // with no more than the amount of decimals as set by the scale
            return '0.'.str_repeat('0', $this->getScale() - 1).'1';
        }

        return (string) $stepInterval;
    }
}
