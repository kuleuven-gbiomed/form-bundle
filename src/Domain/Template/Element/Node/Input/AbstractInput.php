<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Input;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;

/**
 * Class AbstractInput.
 */
abstract class AbstractInput implements InputInterface, \JsonSerializable, Deserializable
{
    /** how the input is to be rendered on client side */
    private readonly string $displayMode;

    /**
     * AbstractInput constructor.
     *
     * @throws InputInvalidArgumentException
     */
    protected function __construct(/** makes the input field a required field */
        private readonly bool $required,
        string $displayMode,
    ) {
        if (!in_array($displayMode, static::getAvailableDisplayModes(), true)) {
            throw new InputInvalidArgumentException('parameter [displayMode] must be one of the following: '.implode('|', static::getAvailableDisplayModes()).'. got '.$displayMode);
        }
        $this->displayMode = $displayMode;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public static function getAvailableDisplayModes(): array
    {
        return [self::DISPLAY_MODE_REGULAR];
    }

    public function getDisplayMode(): string
    {
        return $this->displayMode;
    }

    public function getFieldOptions(?string $locale): array
    {
        return [];
    }

    public function jsonSerialize(): array
    {
        return [
            self::KEY_TYPE => static::getType(),
            self::KEY_REQUIRED => $this->isRequired(),
            self::KEY_DISPLAY_MODE => $this->getDisplayMode(),
        ];
    }

    /**
     * check if the associative array coming from JSON decoded data has the correct type referenced
     * to create the requested input.
     */
    protected static function validateJsonDecodeArrayData(array $data): void
    {
        if (!array_key_exists(self::KEY_TYPE, $data)) {
            throw new \InvalidArgumentException('missing key ['.self::KEY_TYPE.'] in json decoded input array data');
        }

        if ($data[self::KEY_TYPE] === static::getType()) {
            return;
        }

        throw new \InvalidArgumentException('expected value for key ['.self::KEY_TYPE.'] in json decoded input array data to be ['.static::getType().']. got ['.$data[self::KEY_TYPE]);
    }
}
