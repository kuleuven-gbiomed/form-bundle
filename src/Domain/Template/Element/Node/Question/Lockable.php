<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;

trait Lockable
{
    protected ?OptionInputNodeCollection $cachedUnlockers = null;
    protected ?OptionInputNodeCollection $cachedPartialUnlockers = null;

    abstract public function getTreeRootNode(): CategoryNode;

    abstract public function getUid(): string;

    public function isLocked(): bool
    {
        return false === ($this->getUnlockers()->isEmpty() && $this->getPartialUnlockers()->isEmpty());
    }

    public function getUnlockers(): OptionInputNodeCollection
    {
        // finding unlockers up & down the tree is performance hell. let's do it once, and only when first asked for
        // (thus not in the constructor!) and then 'cache' it in a property (similar to 'local caches' on @see FormList).
        if (null === $this->cachedUnlockers) {
            $this->cachedUnlockers = $this->getTreeRootNode()
                ->getFlattenedWithAllDescendants()
                ->getInputNodes()
                ->getOptionInputNodes()
                ->filter(fn (OptionInputNode $node) => in_array($this->getUid(), $node->getUnlocksQuestionsIds(), true));
        }

        return $this->cachedUnlockers;
    }

    public function getPartialUnlockers(): OptionInputNodeCollection
    {
        // finding unlockers up & down the tree is performance hell. let's do it once, and only when first asked for
        // (thus not in the constructor!) and then 'cache' it in a property (similar to 'local caches' on @see FormList).
        if (null === $this->cachedPartialUnlockers) {
            $this->cachedPartialUnlockers = $this->getTreeRootNode()
                ->getFlattenedWithAllDescendants()
                ->getInputNodes()
                ->getOptionInputNodes()
                ->filter(fn (OptionInputNode $node) => in_array($this->getUid(), $node->getMultiUnlocksQuestionIds(), true));
        }

        return $this->cachedPartialUnlockers;
    }
}
