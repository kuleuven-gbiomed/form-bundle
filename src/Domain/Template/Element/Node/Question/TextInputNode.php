<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\PrefillableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use Webmozart\Assert\Assert;

/**
 * Class TextInputNode.
 *
 * creates a question to allow end-user to submit a string value within configured boundaries
 *
 * NOTE: when a user answers this type of question, the answer value will be either a string value or NULL
 * all answers to any questions are stored in a single array 'formValues' that is then serialized in the DB table
 * related to the @see StoredTemplateTargetInterface. for text questions, the answer will be stored with the uid of
 * the question as key in that array and the value will a string value or NULL (if question is not required and was answered blank)
 * e.g. $formValues = [
 *                  'uid-of-text-question' =>  'some text, possibly with <p>HTML</p>',
 *                  'uid-of-other-text-question-that-is-not-required-and-answered-blank' => NULL,
 *                  'uid-of-some-other-question-of-different-type' => 'answer-from-that-other-question',
 *                  ...
 *               ]
 */
final class TextInputNode extends AbstractParentalInputNode implements ParentalInputNode, PrefillableInputNode
{
    use Lockable;

    /**
     * @var string[]
     */
    private array $prefillWithIds = [];

    public function prefillWith(self ...$textInputNodes): void
    {
        $ids = (new NodeCollection($textInputNodes))->getUnique()->getUids();

        if (in_array($id = $this->getUid(), $ids, true)) {
            throw new \Exception("Question with ID $id can not be used to prefill itself.");
        }

        $this->prefillWithIds = $ids;
    }

    public function getPrefillingQuestionUids(): array
    {
        return $this->prefillWithIds;
    }

    public function hasPrefillingQuestions(): bool
    {
        return count($this->getPrefillingQuestionUids()) > 0;
    }

    public static function getType(): string
    {
        return self::TYPE_PREFIX.'_'.TextInput::getType();
    }

    /**
     * @param LocalizedInfoWithRequiredLabel $info with label required & asserted (label is the actual textual 'question')
     */
    public static function fromUidAndDependencies(
        string $uid,
        TextInput $input,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        bool $hidden,
        string $descriptionDisplayMode,
    ): self {
        return new self(
            $uid,
            $input,
            $flowPermission,
            $info,
            $hidden,
            $descriptionDisplayMode,
        );
    }

    /**
     * creates a text node with the bare minimum as a way to 'initialize' the node before updating it further.
     *
     * NOTE: updating of the node after using this factory might be required to make it actually 'usable',
     * since only the bare minimum is created to avoid this node to fail upon construct.
     * e.g. without flowPermission, this node will not be accessible for any user and thus will never be shown
     * and worse, the template will not allow to be published because every inputNode must have at least some access,
     * otherwise it's useless and only dead weight.
     */
    public static function fromUidAndMinimalDependencies(
        string $uid,
        TextInput $input,
        LocalizedInfoWithRequiredLabel $info,
    ): self {
        return new self(
            $uid,
            $input,
            FlowPermission::createEmpty(),
            $info,
            false,
            self::DESCRIPTION_DISPLAY_MODE_ICON,
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws InputInvalidArgumentException
     * @throws \Exception
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $descriptionDisplayMode = self::DESCRIPTION_DISPLAY_MODE_ICON;
        if (isset($data[self::KEY_DESCRIPTION_DISPLAY_MODE])) {
            $descriptionDisplayMode = $data[self::KEY_DESCRIPTION_DISPLAY_MODE];
        }

        $node = new self(
            $data[self::KEY_UID],
            TextInput::fromJsonDecodedArray($data[self::KEY_INPUT]),
            FlowPermission::fromJsonDecodedArray($data[self::KEY_FLOW_PERMISSION]),
            LocalizedInfoWithRequiredLabel::fromJsonDecodedArray($data[self::KEY_INFO]),
            $data[self::KEY_HIDDEN],
            $descriptionDisplayMode,
        );

        if (isset($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS])) {
            $node->setMultiUnlockingQuestions($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS]);
        }

        $node->prefillWithIds = array_key_exists('prefillWithIds', $data) ? $data['prefillWithIds'] : [];

        return $node;
    }

    /**
     * overwritten for type hinting purposes.
     */
    public function getInput(): TextInput
    {
        /** @var TextInput $result */
        $result = parent::getInput();

        return $result;
    }

    public function updateInput(InputInterface $input): void
    {
        Assert::isInstanceOf($input, TextInput::class);

        $this->input = $input;
    }

    public function jsonSerialize(): array
    {
        return array_merge(
            parent::jsonSerialize(),
            0 === count($this->prefillWithIds) ? [] : [
                'prefillWithIds' => $this->prefillWithIds,
            ],
        );
    }
}
