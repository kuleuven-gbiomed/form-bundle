<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionWithAdditionalTextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use Webmozart\Assert\Assert;

/**
 * Class OptionInputNode.
 *
 * creates an option to allow end-users (clients) to select it as answer of a choice question @see ChoiceInputNode
 *
 * NOTE: used solely as answering options for a @see ChoiceInputNode. therefore, this is (currently) the only inputNode
 * that is NOT contracted as @see ParentalInputNode
 * these nodes are only relevant as option for the parental inputNode @see ChoiceInputNode . when one or more
 * optionNodes are selected by a user as answer on submit of the choiceNode, these selected OptionNodes are added to
 * the answer array associated with the choiceNode inside the stored formValues that holds all answers to for any
 * questions. as such, a selected option as answer is stored as key-value pair in the answer array for the choice
 * question with the uid of the optionInputNode as the key in the pair. the value in the pair can be three different
 * possible values, depending on how the optionNode is configured as @see OptionInput
 * or as @see OptionWithAdditionalTextInput :
 * - boolean TRUE if no additional text is optionally allowed or required (default and most common usage).
 * - a string if additional text is required.
 * - NULL if additional text is allowed but not required and users opted to not make use of it (= submit blank text).
 * also see class docBlock on @see ChoiceInputNode for more info on the storing syntax
 *
 * IMPORTANT: since an optionNode is only relevant to a parental choice question node ( @see ChoiceInputNode ), the
 * access to the optionNode is determined by the access (permissions) defined on that parental choice question. But
 * each inputNode requires a @see FlowPermission . since an optionNode - at time of writing - is never used outside that
 * parental choice question, the flowPermission of the optionNode could be empty or contain anything it wants as it is
 * always ignored. so I could explicitly set the optionNodes's flowPermission to empty on construct or assert it to be
 * empty or ignore it and allow anything. but I prefer to use the flowPermission from the parental choice question and
 * pass that in to the option node when it gets added to the parental choice question (via @see ChoiceInputNode::updateChildren() ).
 * simply because that makes more sense. the flowPermission of the optionNode is never called, but one never knows that
 * that could change in the future; e.g. might prove useful if a @see InputNodeCollection is filtered on flowPermission.
 */
final class OptionInputNode extends InputNode
{
    /**
     * @var string[]
     */
    private array $unlocksQuestionsIds = [];

    private array $multiUnlocksQuestionIds = [];

    /** @var string */
    public const KEY_UNLOCKS_QUESTION_IDS = 'unlocksQuestionIds';
    public const KEY_MULTI_UNLOCKS_QUESTION_IDS = 'multiUnlocksQuestionIds';

    public function unlocksQuestions(ParentalInputNode ...$questions): void
    {
        $this->guardDoNotUnlockOwnQuestion(...$questions);

        $this->unlocksQuestionsIds = array_unique(array_merge(
            $this->unlocksQuestionsIds,
            (new NodeCollection($questions))->getUids()
        ));
    }

    public function multiUnlocksQuestions(ParentalInputNode ...$questions): void
    {
        $this->guardDoNotUnlockOwnQuestion(...$questions);

        $this->multiUnlocksQuestionIds = array_unique(array_merge(
            $this->multiUnlocksQuestionIds,
            (new NodeCollection($questions))->getUids(),
        ));
    }

    /**
     * @return string[]
     */
    public function getMultiUnlocksQuestionIds(): array
    {
        return $this->multiUnlocksQuestionIds;
    }

    public function unlocksAllNestedQuestionsInCategory(CategoryNode $category): void
    {
        /** @var ParentalInputNode[] $questions */
        $questions = $category->getFlattenedParentalInputNodeDescendants()->toArray();

        $this->unlocksQuestions(...$questions);
    }

    /**
     * @return string[]
     */
    public function getUnlocksQuestionsIds(): array
    {
        return $this->unlocksQuestionsIds;
    }

    public static function getType(): string
    {
        return self::TYPE_PREFIX.'_'.OptionInput::getType();
    }

    public static function fromUidAndDependencies(
        string $uid,
        OptionInput|OptionWithAdditionalTextInput $input,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        bool $hidden,
        string $descriptionDisplayMode,
    ): self {
        return new self($uid, $input, $flowPermission, $info, $hidden, $descriptionDisplayMode);
    }

    /**
     * creates an option node with the bare minimum as a way to 'initialize' the node before updating it further.
     *
     * NOTE: updating of the node after using this factory might be required to make it actually 'usable',
     * since only the bare minimum is created to avoid this node to fail upon construct.
     * e.g. in theory, without flowPermission, this node would not be accessible for any user and thus will never be shown
     * and worse, the template will not allow to be published because every inputNode must have at least some access
     * but this kind of node is used only as child of @see ChoiceInputNode and its usability/renderability depends
     * solely on that choice parent. upon attaching this node to such a choice node, the flowPermission of that
     * choice node is copied, just as way . if that choice node has no flowPermission (yet),
     * then neither will this option node be shown or used. upon updating the flowPermission of the choice node, the
     * flow permission of this and other option nodes that are children of the same choice node, are updated as well.
     */
    public static function fromUidAndMinimalDependencies(
        string $uid,
        OptionInput|OptionWithAdditionalTextInput $input,
        LocalizedInfoWithRequiredLabel $info,
    ): self {
        return new self(
            $uid,
            $input,
            FlowPermission::createEmpty(),
            $info,
            false,
            self::DESCRIPTION_DISPLAY_MODE_ICON
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws \Exception
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $descriptionDisplayMode = self::DESCRIPTION_DISPLAY_MODE_ICON;
        if (isset($data[self::KEY_DESCRIPTION_DISPLAY_MODE])) {
            $descriptionDisplayMode = $data[self::KEY_DESCRIPTION_DISPLAY_MODE];
        }

        $optionInputData = $data[self::KEY_INPUT];
        if ($optionInputData[InputInterface::KEY_TYPE] === OptionWithAdditionalTextInput::getType()) {
            $optionInput = OptionWithAdditionalTextInput::fromJsonDecodedArray($optionInputData);
        } else {
            $optionInput = OptionInput::fromJsonDecodedArray($optionInputData);
        }

        $node = new self(
            $data[self::KEY_UID],
            $optionInput,
            FlowPermission::fromJsonDecodedArray($data[self::KEY_FLOW_PERMISSION]),
            LocalizedInfoWithRequiredLabel::fromJsonDecodedArray($data[self::KEY_INFO]),
            $data[self::KEY_HIDDEN],
            $descriptionDisplayMode
        );

        $node->unlocksQuestionsIds = array_key_exists(self::KEY_UNLOCKS_QUESTION_IDS, $data) ? $data[self::KEY_UNLOCKS_QUESTION_IDS] : [];
        $node->multiUnlocksQuestionIds = array_key_exists(self::KEY_MULTI_UNLOCKS_QUESTION_IDS, $data) ? $data[self::KEY_MULTI_UNLOCKS_QUESTION_IDS] : [];

        return $node;
    }

    /**
     * overwritten for type hinting purposes.
     */
    public function getInput(): OptionInput|OptionWithAdditionalTextInput
    {
        /** @var OptionInput|OptionWithAdditionalTextInput $result */
        $result = parent::getInput();

        return $result;
    }

    public function usesAdditionalTextInput(): bool
    {
        return $this->getInput() instanceof OptionWithAdditionalTextInput;
    }

    public function updateInput(InputInterface $input): void
    {
        Assert::isInstanceOf($input, OptionInput::class);

        $this->input = $input;
    }

    public function hasScoringValue(): bool
    {
        return $this->getInput()->hasScore();
    }

    /**
     * @throws \BadMethodCallException indirectly if no score set on the input
     */
    public function getScoringValue(): float
    {
        return $this->getInput()->getScore();
    }

    private function guardDoNotUnlockOwnQuestion(ParentalInputNode ...$questions): void
    {
        try {
            $parent = $this->getParent();
        } catch (MissingNodeException) {
            throw new \Exception(sprintf("Answer with ID '%s' Can not unlock other questions without having its own question.", $this->getUid()));
        }

        foreach ($questions as $question) {
            if ($parent->getUid() === $question->getUid()) {
                throw new \Exception(sprintf("Answer option with ID '%s' can not unlock its own parent question with ID '%s'.", $this->getUid(), $parent->getUid()));
            }
        }
    }

    public function jsonSerialize(): array
    {
        return array_merge(
            parent::jsonSerialize(),
            0 === count($this->unlocksQuestionsIds) ? [] : [
                self::KEY_UNLOCKS_QUESTION_IDS => $this->unlocksQuestionsIds,
            ],
            0 === count($this->multiUnlocksQuestionIds) ? [] : [
                self::KEY_MULTI_UNLOCKS_QUESTION_IDS => $this->multiUnlocksQuestionIds,
            ],
        );
    }

    public function isSameOptionAs(self $otherOptionInputNode): bool
    {
        if ($this->hasScoringValue() !== $otherOptionInputNode->hasScoringValue()) {
            return false;
        }

        if ($this->hasScoringValue() && $this->getScoringValue() !== $otherOptionInputNode->getScoringValue()) {
            return false;
        }

        $thisLocaleStrings = $this->getInfo()->getLocalizedLabel()->getLocaleStrings();
        $otherLocaleStrings = $otherOptionInputNode->getInfo()->getLocalizedLabel()->getLocaleStrings();

        foreach ($thisLocaleStrings as $locale => $string) {
            if (!array_key_exists($locale, $otherLocaleStrings)) {
                return false;
            }

            if ($string !== $otherLocaleStrings[$locale]) {
                return false;
            }
        }

        return true;
    }
}
