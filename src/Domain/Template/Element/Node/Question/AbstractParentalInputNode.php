<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;

abstract class AbstractParentalInputNode extends InputNode implements ParentalInputNode
{
    public const KEY_MULTI_UNLOCKING_QUESTION_UIDS = 'multiUnlockingQuestionUids';

    /** @var array<string, array<string>> */
    private array $unlockingQuestions = [];

    /**
     * @return array<string, array<string>>
     */
    public function getMultiUnlockingQuestions(): array
    {
        return $this->unlockingQuestions;
    }

    /**
     * @return string[]
     */
    public function getAllMultiUnlockingOptions(): array
    {
        $result = [];
        $unlockConfig = $this->getMultiUnlockingQuestions();
        array_walk_recursive($unlockConfig, function (string $value) use (&$result): void {
            $result[] = $value;
        });

        return $result;
    }

    public function isMultiLockable(): bool
    {
        $unlockConditions = $this->getMultiUnlockingQuestions();

        $unlockingQuestions = array_keys($unlockConditions);
        if (0 === count($unlockingQuestions)) {
            return false;
        }

        foreach ($unlockConditions as $unlockingOptionIds) {
            if (0 === count($unlockingOptionIds)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array<string, array<string>> $unlockingQuestions
     */
    public function setMultiUnlockingQuestions(array $unlockingQuestions): void
    {
        $this->unlockingQuestions = $unlockingQuestions;
    }

    public function jsonSerialize(): array
    {
        return array_merge(
            parent::jsonSerialize(),
            [
                self::KEY_MULTI_UNLOCKING_QUESTION_UIDS => $this->getMultiUnlockingQuestions(),
            ]
        );
    }
}
