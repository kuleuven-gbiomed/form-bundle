<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart\PlottingRequirements;

interface PlottableInRadarChart extends NodeInterface
{
    public function getPlottingRequirements(): PlottingRequirements;
}
