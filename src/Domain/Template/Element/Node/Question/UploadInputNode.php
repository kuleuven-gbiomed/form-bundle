<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use Webmozart\Assert\Assert;

/**
 * Class UploadInputNode.
 *
 * NOTE: when a user uploads a file as an answer, that file is uploaded via ajax, validated on serverside, given a Uuid
 * as unique identifier, moved to a dir with uid as filename, and then uid is stored in a standalone DB table. that uid
 * is then returned to client side where it is hold in a hidden input until user decides to submit the form to which
 * this question belongs. only when user explicitly submits it, is the file considered as answer.
 * all answers to any questions are stored in a single array 'formValues' that is then serialized in the DB table related
 * to the @see StoredTemplateTargetInterface . for an upload question,  the question's uid is used as key in the formValues
 * and the associated value stored for an uploadInput is thus the (string) uid of that uploaded file. if multiple
 * files are allowed and/or uploaded as answers, the uids of those files are concatenated
 * with @see UploadInput::FORM_VALUE_DELIMITER as separator in to a single string of file uids. if question is not
 * required and was answered blank, the value will be NULL. the reason why a concatenated string is used over an array
 * is because of the way the client-side code works (with a simple hidden text input that is easier to process)
 * e.g. $formValues = [
 *                  'uid-of-upload-question' => 'uid-of-file-one|uid-of-file-two',
 *                  'uid-of-other-upload-question-with-no-multiple-files-allowed-or-submitted-as-answers' => 'uid-of-file-tree',
 *                  'uid-of-other-upload-question-that-is-not-required-and-no-files-uploaded' => NULL,
 *                  'uid-of-some-other-question-of-different-type' => 'answer-from-that-other-question',
 *                  ...
 *               ]
 */
final class UploadInputNode extends AbstractParentalInputNode implements ParentalInputNode
{
    use Lockable;

    /**
     * @param LocalizedInfoWithRequiredLabel $info with label required & asserted (label is the actual textual 'question')
     */
    public static function fromUidAndDependencies(
        string $uid,
        UploadInput $input,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        bool $hidden,
        string $descriptionDisplayMode,
    ): self {
        return new self(
            $uid,
            $input,
            $flowPermission,
            $info,
            $hidden,
            $descriptionDisplayMode
        );
    }

    /**
     * creates an upload node with the bare minimum as a way to 'initialize' the node before updating it further.
     *
     * NOTE: updating of the node after using this factory might be required to make it actually 'usable',
     * since only the bare minimum is created to avoid this node to fail upon construct.
     * e.g. without flowPermission, this node will not be accessible for any user and thus will never be shown
     * and worse, the template will not allow to be published because every inputNode must have at least some access,
     * otherwise it's useless and only dead weight.
     */
    public static function fromUidAndMinimalDependencies(
        string $uid,
        UploadInput $input,
        LocalizedInfoWithRequiredLabel $info,
    ): self {
        return new self(
            $uid,
            $input,
            FlowPermission::createEmpty(),
            $info,
            false,
            self::DESCRIPTION_DISPLAY_MODE_ICON
        );
    }

    public static function getType(): string
    {
        return self::TYPE_PREFIX.'_'.UploadInput::getType();
    }

    /**
     * @throws LocalizedStringException
     * @throws InputInvalidArgumentException
     * @throws \Exception
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $descriptionDisplayMode = self::DESCRIPTION_DISPLAY_MODE_ICON;
        if (isset($data[self::KEY_DESCRIPTION_DISPLAY_MODE])) {
            $descriptionDisplayMode = $data[self::KEY_DESCRIPTION_DISPLAY_MODE];
        }

        $node = new self(
            $data[self::KEY_UID],
            UploadInput::fromJsonDecodedArray($data[self::KEY_INPUT]),
            FlowPermission::fromJsonDecodedArray($data[self::KEY_FLOW_PERMISSION]),
            LocalizedInfoWithRequiredLabel::fromJsonDecodedArray($data[self::KEY_INFO]),
            $data[self::KEY_HIDDEN],
            $descriptionDisplayMode
        );

        if (isset($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS])) {
            $node->setMultiUnlockingQuestions($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS]);
        }

        return $node;
    }

    /**
     * overwritten for type hinting purposes.
     */
    public function getInput(): UploadInput
    {
        /** @var UploadInput $result */
        $result = parent::getInput();

        return $result;
    }

    public function updateInput(InputInterface $input): void
    {
        Assert::isInstanceOf($input, UploadInput::class);

        $this->input = $input;
    }

    /**
     * To make sure the label can be used as a subdirectory path in a Zip file that is extractable in all OS's, we
     * need to sanitize this label, which can contain HTML, special (illegal for paths) characters and trailing spaces, etc.:
     * remove HTML, convert to lower case and replace ALL special characters by a hyphen (except for underscore, hyphen
     * and point). trim start & end from space, period, hyphen, underscore.
     * also make sure the result is unique when used as a Zip subdir path (question labels do not have to be unique
     * amongst the other questions in the FormList to which it belongs).
     * it is also better to limit the result to a reasonable length (31 is advised for all OS).
     */
    public function getCompressSafeLabel(string $locale, ?int $limitLength): string
    {
        $uidPartLength = 5;
        if (is_int($limitLength) && $limitLength < ($uidPartLength + 2)) {
            throw new \InvalidArgumentException('limitLength can not be lower than '.((string) ($uidPartLength + 2)).' because '.((string) $uidPartLength).' characters of the question uid, plus a hyphen, are reserved in the final length to suffix the result to make it unique.');
        }

        // remove all HTML and convert to lower case, to make sure the label as path is safe for all OS
        $label = mb_strtolower(strip_tags($this->getLabel($locale)));
        // replace ALL special characters by a hyphen (except for underscore, hyphen and point).
        $label = preg_replace('/[^a-z0-9\\._-]/', '-', $label) ?? $this->getUid();
        // trim start & end from space, period, hyphen, underscore.
        $label = trim($label, " .-_\t\n\r\0\x0B");

        if (is_int($limitLength)) {
            $label = mb_substr($label, 0, $limitLength - $uidPartLength - 1);
        }

        return $label.'-'.mb_substr($this->getUid(), 0, $uidPartLength);
    }
}
