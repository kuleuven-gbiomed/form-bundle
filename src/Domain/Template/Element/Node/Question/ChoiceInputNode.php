<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ChildrenAwareInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\PrefillableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\ChoiceInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionWithAdditionalTextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\ChildrenTrait;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\CalculationDependencyConfigCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\InputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;
use Webmozart\Assert\Assert;

/**
 * Class ChoiceInputNode.
 *
 * creates a question to allow end-users (clients) to select an answer option from a set of options attached to this question
 *
 * with option to use question as a score question dependency is global scoring or as a global scoring question itself
 *
 * NOTE: associates to a HTML <select>, set of <radio> or <checkbox>, depending on the configuration options.
 *
 * NOTE: @see ChoiceInputNode::MIN_AMOUNT_OF_NON_HIDDEN_OPTIONS_REQUIRED: a choice question that has not at
 * least two options available (= non-hidden) to build its answer options with, is useless. hence on creation or update
 * of the choice question with its children (via @see ChoiceInputNode::updateChildren, it is asserted that at least
 * two non-hidden option nodes are present. one could argue that one available option is sufficient but a choice question
 * is all about given a the end-user (client) a choice of a of predefined answers, while one option isn't a choice anymore. if an
 * admin wants to make a choice question with only one option - or hide and/or remove all but one options - then admin
 * should use another type of question (e.g. a @see CheckBoxInputNode ). also, marking all option nodes as hidden could
 * be considered a way of disabling the parent choice question, but if an admin wants to disable the choice question,
 * he/she can mark the choice question itself as hidden. enforcing two available options on creation & update also
 * ensures having to check for at least two options in every usage (which might be forgotten in new features).
 *
 * NOTE: all answers to any questions are stored in a single dimension array 'formValues' that is then serialized in
 * the DB table related to the @see StoredTemplateTargetInterface. for choice questions, this will result in the uid
 * of the question being set as a key in the formValues and with value TRUE, to indicate the choice question was saved
 * or submitted, regardless of which choice-options were chosen as answers (and regardless of the choice question being
 * required or not). and each of the choice-options chosen as answers are added in the same way to the same formValues
 * array: uid of choice-option as array key and as array value: either TRUE (to indicate it is a choice-option
 * that can be chosen as answer, nothing more (@see OptionInputNode ) or either a string/NULL (to indicate it is a
 * choice-option that can be chosen as answer with an required or optional added user inputted
 * string (@see OptionWithAdditionalTextInput )
 * examples:
 * [
 *    'uid-of-choice-question-with-no-multiple-answers-allowed-and-required' => true,
 *    'uid-of-choice-option-selected-as-answer-x' => true,
 * ]
 *
 * [
 *    'uid-of-choice-question-with-no-multiple-answers-allowed-and-optional-and-user-chose-no-answer' => true,
 * ]
 *
 * [
 *    'uid-of-choice-question-with-no-multiple-answers-allowed-and-optional-and-user-chose-an-answer' => true,
 *    'uid-of-choice-option-selected-as-answer-x' => true,
 * ]
 *
 * [
 *    'uid-of-choice-question-with-multiple-answers-allowed-and-required' => true,
 *    'uid-of-choice-option-selected-as-answer-1' => true,
 *    'uid-of-choice-option-selected-as-answer-2' => true,
 * ]
 *
 * [
 *    'uid-of-choice-question-with-multiple-answers-allowed-and-optional-and-user-chose-no-answer' => true,
 * ]
 *
 * [
 *    'uid-of-choice-question-with-multiple-answers-allowed-and-optional-and-user-chose-some-answers' => true,
 *    'uid-of-choice-option-selected-as-answer-1' => true,
 *    'uid-of-choice-option-selected-as-answer-2' => true,
 * ]
 *
 * [
 *    'uid-of-choice-question-with-multiple-allowed' => true,
 *    'uid-of-choice-option-selected-as-answer-1' => true,
 *    'uid-of-choice-option-with-required-text-selected-as-answer-1' => 'some text answer',
 *    'uid-of-choice-option-with-optional-text-selected-as-answer-2-and-saved-with-blank-text' => null,
 * ]
 *
 * @implements ChildrenAwareInterface<OptionInputNode, OptionInputNodeCollection>
 */
final class ChoiceInputNode extends ScorableInputNode implements ChildrenAwareInterface, PrefillableInputNode
{
    /**
     * @use ChildrenTrait<OptionInputNode, OptionInputNodeCollection>
     */
    use ChildrenTrait {
        validateReplacingChild as traitValidateReplacingChild;
        validateAttachingChild as traitValidateAttachingChild;
        updateChildren as traitUpdateChildren;
    }

    use Lockable;

    /** @var string */
    public const KEY_ROLES_ALLOWED_TO_SEE_OPTION_SCORE_VALUES = 'rolesAllowedToSeeOptionScoreValues';

    /** @var int minimum amount of child options required to allow the choice question to be build */
    public const MIN_AMOUNT_OF_NON_HIDDEN_OPTIONS_REQUIRED = 2;

    // suffix to add to the field when the 'other'-option is used
    public const OTHER_VALUE_FIELD_SUFFIX = '_otherValue';

    /** @var string */
    public const KEY_PREFILLING_QUESTION_UIDS = 'prefillingQuestionUids';
    /** roles allowed to see score values attached to the choice options */
    private RoleCollection $rolesAllowedToSeeOptionScoreValues;

    /**
     * @var string[]
     */
    private array $prefillingQuestionUids = [];

    /**
     * ChoiceInputNode constructor.
     *
     * NOTE: requires at least @see ChoiceInputNode::MIN_AMOUNT_OF_NON_HIDDEN_OPTIONS_REQUIRED options that are not
     * marked as hidden, or fails otherwise. asserted via @see considersAsPotentiallyAvailableOptions()
     * this means that a choice node can only be created AND updated if at least that amount (2 at time of writing)
     * of options ar provided, also in admin-side. the reason is twofolded:
     *
     * NOTE: issue with setting flowPermissions on children via an indirect method:
     * background: the flowPermission of the children (@see OptionInputNodeCollection) of this choice node are updated
     * with the flowPermission from this (parental) choice node upon each call of @see updateFlowPermission() .
     * (strictly speaking an @see OptionInputNode deisn't need an flowPermission since it depends on it parental choice node
     * for every access, but for consistence between all inputNodes, it is set anyway with permission from this parental choice node).
     * now this could cause some circular issues in this constructor: the @see updateChildren() will need the flowPermission
     * to be a valid instance while the @see updateFlowPermission() will need the children to be a valid instance.
     * we can not just first call @see updateFlowPermission() without setting the children first because that will
     * throw error on the children being NULL at this point. and we can not just first call @see updateChildren()
     * without setting flowPermission first because that will throw error on flowPermission being NULL at this point.
     * also, the @see updateChildren() requires at least x children or it will fail, so we can not just initialize
     * the children with an empty collection before calling the @see updateFlowPermission().
     * solution: initialize the children with the given options before calling @see updateFlowPermission() and although
     * at that point the children will already have a (different) flow permission, the @see updateFlowPermission()
     * will update the children with the correct flowPermission of the parent via @see updateChildren().
     *
     * @param LocalizedInfoWithRequiredLabel $info     with label required & asserted (label is the actual textual 'question')
     * @param OptionInputNodeCollection      $children contains the options for the choice question as OptionInputNode children
     *
     * @throws \Exception
     */
    protected function __construct(
        string $uid,
        ChoiceInput $input,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        bool $hidden,
        string $descriptionDisplayMode,
        protected OptionInputNodeCollection $children,
        CalculationDependencyConfigCollection $globalScoreCalculationDependencyConfigs,
        string $globalScoreWeighingType,
        RoleCollection $rolesAllowedToSeeOptionScoreValues,
        array $customOptions = [],
    ) {
        // children are pre set with given collection $children (instead of calling updateChildren) so that the updateFlowPermission
        // will have the correct set of children and set (override) the flowPermission on each of those with the correct given flowPermission

        parent::__construct(
            $uid,
            $input,
            $flowPermission,
            $info,
            $hidden,
            $descriptionDisplayMode,
            $globalScoreCalculationDependencyConfigs,
            $globalScoreWeighingType,
            $customOptions
        );

        $this->updateRolesAllowedToSeeOptionScoreValues($rolesAllowedToSeeOptionScoreValues);
    }

    protected static function createChildCollection(array $children = []): OptionInputNodeCollection
    {
        return new OptionInputNodeCollection($children);
    }

    public static function getType(): string
    {
        return self::TYPE_PREFIX.'_'.ChoiceInput::getType();
    }

    /**
     * @param OptionInputNodeCollection $optionNodes contains the options for the choice question as OptionInputNode children
     */
    public static function fromUidAndDependencies(
        string $uid,
        ChoiceInput $input,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        bool $hidden,
        string $descriptionDisplayMode,
        OptionInputNodeCollection $optionNodes,
        CalculationDependencyConfigCollection $globalScoreCalculationDependencyConfigs,
        string $globalScoreWeighingType,
        RoleCollection $rolesAllowedToSeeOptionScoreValues,
        array $customOptions = [],
    ): self {
        return new self(
            $uid,
            $input,
            $flowPermission,
            $info,
            $hidden,
            $descriptionDisplayMode,
            $optionNodes,
            $globalScoreCalculationDependencyConfigs,
            $globalScoreWeighingType,
            $rolesAllowedToSeeOptionScoreValues,
            $customOptions
        );
    }

    /**
     * creates a choice node with the bare minimum as a way to 'initialize' the node before updating it further.
     *
     * NOTE: updating of the node after using this factory might be required to make it actually 'usable',
     * since only the bare minimum is created to avoid this node to fail upon construct.
     * e.g. without flowPermission, this node will not be accessible for any user and thus will never be shown
     * and worse, the template will not allow to be published because every inputNode must have at least some access,
     * otherwise it's useless and only dead baggage.
     * e.g. without option children, this choice node is useless and will be skipped or ignored.
     */
    public static function fromUidAndMinimalDependencies(
        string $uid,
        ChoiceInput $input,
        LocalizedInfoWithRequiredLabel $info,
        OptionInputNodeCollection $optionNodes,
    ): self {
        return new self(
            $uid,
            $input,
            FlowPermission::createEmpty(),
            $info,
            false,
            self::DESCRIPTION_DISPLAY_MODE_ICON,
            $optionNodes,
            CalculationDependencyConfigCollection::createEmpty(),
            self::GLOBAL_SCORE_DEFAULT_WEIGHING_TYPE,
            RoleCollection::createEmpty(),
            []
        );
    }

    public function withOptionNodes(OptionInputNodeCollection $optionNodes): self
    {
        return new self(
            $this->getUid(),
            $this->getInput(),
            $this->getFlowPermission(),
            $this->getInfo(),
            $this->isHidden(),
            $this->getDescriptionDisplayMode(),
            $optionNodes,
            $this->getGlobalScoreCalculationDependencyConfigs(),
            $this->getGlobalScoreWeighingType(),
            $this->getRolesAllowedToSeeOptionScoreValues(),
            $this->getCustomOptions()
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws InputInvalidArgumentException
     * @throws \Exception
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $descriptionDisplayMode = self::DESCRIPTION_DISPLAY_MODE_ICON;
        if (isset($data[self::KEY_DESCRIPTION_DISPLAY_MODE])) {
            $descriptionDisplayMode = $data[self::KEY_DESCRIPTION_DISPLAY_MODE];
        }

        $optionNodes = [];
        foreach ($data[self::KEY_CHILDREN] as $childData) {
            $optionNodes[] = OptionInputNode::fromJsonDecodedArray($childData);
        }

        $globalScoreCalculationDependencyConfigsData = [];
        if (isset($data[self::KEY_GLOBAL_SCORE_CALCULATION_DEPENDENCY_CONFIGS])) {
            $globalScoreCalculationDependencyConfigsData = $data[self::KEY_GLOBAL_SCORE_CALCULATION_DEPENDENCY_CONFIGS];
        }

        $rolesOptionScoresData = [];
        if (isset($data[self::KEY_ROLES_ALLOWED_TO_SEE_OPTION_SCORE_VALUES])) {
            $rolesOptionScoresData = $data[self::KEY_ROLES_ALLOWED_TO_SEE_OPTION_SCORE_VALUES];
        }

        $globalScoreWeighingType = static::GLOBAL_SCORE_DEFAULT_WEIGHING_TYPE;
        if (isset($data[self::KEY_GLOBAL_SCORE_WEIGHING_TYPE])) {
            $globalScoreWeighingType = $data[self::KEY_GLOBAL_SCORE_WEIGHING_TYPE];
        }

        $node = new self(
            $data[self::KEY_UID],
            ChoiceInput::fromJsonDecodedArray($data[self::KEY_INPUT]),
            FlowPermission::fromJsonDecodedArray($data[self::KEY_FLOW_PERMISSION]),
            LocalizedInfoWithRequiredLabel::fromJsonDecodedArray($data[self::KEY_INFO]),
            $data[self::KEY_HIDDEN],
            $descriptionDisplayMode,
            new OptionInputNodeCollection($optionNodes),
            CalculationDependencyConfigCollection::fromJsonDecodedArray($globalScoreCalculationDependencyConfigsData),
            $globalScoreWeighingType,
            RoleCollection::fromJsonDecodedArray($rolesOptionScoresData),
            parent::getCustomOptionsFromJsonDecodedArray($data)
        );

        if (isset($data[self::KEY_PREFILLING_QUESTION_UIDS])
            && 0 !== (
                is_countable($data[self::KEY_PREFILLING_QUESTION_UIDS])
                ? count($data[self::KEY_PREFILLING_QUESTION_UIDS])
                : 0
            )
        ) {
            $node->prefillWithIds($data[self::KEY_PREFILLING_QUESTION_UIDS]);
        }

        if (isset($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS])) {
            $node->setMultiUnlockingQuestions($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS]);
        }

        return $node;
    }

    public function getChildren(): OptionInputNodeCollection
    {
        return $this->children;
    }

    /**
     * children must meet following conditions:
     *  - must be a set of {@see OptionInputNode} children
     *  - at least {@see ChoiceInputNode::MIN_AMOUNT_OF_NON_HIDDEN_OPTIONS_REQUIRED} children must be marked as non-hidden
     *  - if choice question has min and/or max score set via its {@see ChoiceInputNode::getInputScoringParameters()},
     *    then the scoring value of each child option with {@see OptionInputNode::hasScoringValue()}, must be valid
     *    against the min if set, the max if set or both if set.
     *
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    protected function updateChildren(OptionInputNodeCollection $children): void
    {
        self::validatePotentiallyAvailableOptions($children);

        // make sure each of the option children have the same permission access as this parental choice question node
        /** @var OptionInputNode $optionInputNode */
        foreach ($children as $optionInputNode) {
            $optionInputNode->updateFlowPermission($this->getFlowPermission());
        }

        // mark this parental choice question node as parent on each of the option children
        $this->traitUpdateChildren($children);
    }

    /**
     * overwritten to assure that the flow permissions of all option children are updated with new flow permission as well.
     *
     * NOTE: all nodes of any type are required to have at least some access set to make them usable, so for convenience
     * & catching any dev changes in (permissions) update for the children,
     * a full update of the children is ran, using the current set of children, to update the permissions on each child.
     */
    public function updateFlowPermission(FlowPermission $flowPermission): void
    {
        parent::updateFlowPermission($flowPermission);

        // run a full update on the children as well to assure everything is still validated
        // (especially making sure the flowPermission on each child is the same as this parent choice
        $this->updateChildren($this->getChildren());
    }

    /**
     * overwritten for type hinting purposes.
     */
    public function getInput(): ChoiceInput
    {
        /** @var ChoiceInput $result */
        $result = parent::getInput();

        return $result;
    }

    public function updateInput(InputInterface $input): void
    {
        Assert::isInstanceOf($input, ChoiceInput::class);

        $this->input = $input;
    }

    public function getRolesAllowedToSeeOptionScoreValues(): RoleCollection
    {
        return $this->rolesAllowedToSeeOptionScoreValues;
    }

    public function updateRolesAllowedToSeeOptionScoreValues(RoleCollection $rolesAllowedToSeeOptionScoreValues): void
    {
        $this->rolesAllowedToSeeOptionScoreValues = $rolesAllowedToSeeOptionScoreValues;
    }

    public function allowsRoleToSeeOptionScoreValues(Role $role): bool
    {
        return $this->getRolesAllowedToSeeOptionScoreValues()->hasRole($role);
    }

    /**
     * conditions to show the minimum & maximum score values for a choice are:
     * - there is a minimum and/or maximum extremum value to be shown (duh)
     * - role must be allowed to see the scoring values of the choice's answer options
     * - there is at least one (non-hidden) answer option that has a scoring values (otherwise, it's pointless to show
     *   min/max, since min/max is linked solely to those answer option scoring values and vice versa).
     *
     * note that a choice question can use the minimum and/or the maximum and/or the answer options scoring values without
     * actually operating as a choice-as-score or choice-as-global-score question (which requires all scoring parameters to be
     * set @see ChoiceInputNode::meetsScoringRequirements and be part of a scoring calaculation): the choice question can
     * intentionally use the minimum & maximum (& answer options scoring) values for displaying purposes only.
     */
    public function allowsRoleToSeeExtremaScoreValues(Role $role): bool
    {
        $scoringParams = $this->getInputScoringParameters();
        if (!$scoringParams->hasMinScore() && !$scoringParams->hasMaxScore()) {
            return false;
        }

        if (!$this->allowsRoleToSeeOptionScoreValues($role)) {
            return false;
        }

        return 0 < $this->getAvailableOptions()->getHavingScoringValue()->count();
    }

    /**
     * get child option nodes that are valid for usage in building (and rendering, processing, etc.) the choice question.
     */
    public function getAvailableOptions(): OptionInputNodeCollection
    {
        /** @var OptionInputNodeCollection $result */
        $result = $this->getChildren()->getNotMarkedAsHidden();

        return $result;
    }

    public function getUnlocksQuestionIdsPerAnswer(): array
    {
        return array_reduce(
            $this->getAvailableOptions()->toArray(),
            function (array $lockedQuestions, OptionInputNode $answer): array {
                $lockedQuestions[$answer->getUid()] = $answer->getUnlocksQuestionsIds();

                return $lockedQuestions;
            },
            []
        );
    }

    public function usesOptionsWithAdditionalTextInput(): bool
    {
        /** @var OptionInputNode $optionNode */
        foreach ($this->getChildren() as $optionNode) {
            if ($optionNode->usesAdditionalTextInput()) {
                return true;
            }
        }

        return false;
    }

    /**
     * get child option nodes that are valid for usage in building (and rendering, processing, etc.) the choice question
     * based on given set of option uids.
     */
    public function getAvailableOptionsByUids(array $uids): OptionInputNodeCollection
    {
        return $this->getAvailableOptions()->getByNodeUids($uids);
    }

    public function getAvailableOptionsByLocalizedLabel(string $label, string $locale): OptionInputNodeCollection
    {
        return $this->getAvailableOptions()->getByLocalizedLabel($label, $locale);
    }

    public function getPrefillingQuestionUids(): array
    {
        return $this->prefillingQuestionUids;
    }

    public function prefillWith(self ...$prefillQuestions): void
    {
        $this->prefillingQuestionUids = (new InputNodeCollection($prefillQuestions))->getUids();
    }

    /** @param string[] $prefillingQuestionIds*/
    public function prefillWithIds(array $prefillingQuestionIds): void
    {
        $this->prefillingQuestionUids = $prefillingQuestionIds;
    }

    public function hasPrefillingQuestions(): bool
    {
        return count($this->getPrefillingQuestionUids()) > 0;
    }

    /**
     * @phpstan-param InputNodeCollection<array-key, covariant InputNode> $formChoiceQuestions
     *
     * @psalm-param InputNodeCollection<array-key, ChoiceInputNode> $formChoiceQuestions
     */
    public function hasValidPrefillingQuestions(InputNodeCollection $formChoiceQuestions): bool
    {
        if (!$this->hasPrefillingQuestions()) {
            return false;
        }

        $prefillingQuestions = $formChoiceQuestions->getByNodeUids($this->getPrefillingQuestionUids());

        foreach ($prefillingQuestions as $prefillingQuestion) {
            if (!$prefillingQuestion instanceof self) {
                return false;
            }

            if (!$this->hasChildrenWithSameOptionsAs($prefillingQuestion)) {
                return false;
            }
        }

        return true;
    }

    public function getFlattenedWithAllDescendants(): OptionInputNodeCollection
    {
        if (null === $this->cachedFlattenedNodeDescendants) {
            $this->cachedFlattenedNodeDescendants = new OptionInputNodeCollection($this->getFlattenedWithAllDescendantsArray());
        }

        return $this->cachedFlattenedNodeDescendants;
    }

    public function hasChildrenWithSameOptionsAs(self $otherChoice): bool
    {
        return $this->getChildren()->hasSameOptionsAs($otherChoice->getChildren());
    }

    /**
     * check if given set of options has the minimum amount of non-hidden options required for a choice question
     * to be able to be created or updated with this given set of options.
     *
     * helper method to verify if given set op options will allow to create or update the choice question with these
     * options. this only considers if the given options will suffice as available amount, there could be other stuff
     * wrong with these children. @see ChoiceInputNode::updateChildren() for more conditions!
     */
    public static function considersAsPotentiallyAvailableOptions(OptionInputNodeCollection $optionInputNodes): bool
    {
        return $optionInputNodes->getNotMarkedAsHidden()->count() >= self::MIN_AMOUNT_OF_NON_HIDDEN_OPTIONS_REQUIRED;
    }

    /**
     * @throws \InvalidArgumentException
     */
    private static function validatePotentiallyAvailableOptions(OptionInputNodeCollection $optionInputNodes): void
    {
        if (!self::considersAsPotentiallyAvailableOptions($optionInputNodes)) {
            throw new \InvalidArgumentException('at least ['.((string) self::MIN_AMOUNT_OF_NON_HIDDEN_OPTIONS_REQUIRED).'] options (that are NOT marked as hidden) are required for a valid choice question. got ['.((string) $optionInputNodes->getNotMarkedAsHidden()->count()).'] options (that are NOT marked as hidden) in a total of ['.((string) $optionInputNodes->count()).'] options given.');
        }
    }

    /**
     * choice question could be used a (global) score node if all scoring parameters set & all optionNodes have a score value.
     */
    public function meetsScoringRequirements(): bool
    {
        // validate that this parental choice has the required min & max and scale & rounding mode, as is required
        // for all scorable questions
        if (!parent::meetsScoringRequirements()) {
            return false;
        }

        // scoring only works if choice question allows only one answer (whether that one answer is required or not does
        // not matter: if not required and user submits choice question without answer, calculation will not take this
        // choice question in account and redistribute weighted calculation amongst the other (filled in) score questions)
        if ($this->getInput()->isMultiple()) {
            return false;
        }

        return $this->hasAllOptionsValidForScoring();
    }

    public function hasAllOptionsValidForScoring(): bool
    {
        $scoringParameters = $this->getInputScoringParameters();

        // options can only be validated if a min & max score extrema are set on the choice question
        if (!$scoringParameters->hasScoringExtrema()) {
            return false;
        }

        // options that allow for an additional text input to be rendered, are not allowed for scoring
        if ($this->usesOptionsWithAdditionalTextInput()) {
            return false;
        }

        $availableOptions = $this->getAvailableOptions();
        $availableScoringOptions = $this->getAvailableOptions()->getHavingScoringValue();

        // a choice question is asserted upon construct & update to have at least two children. if none of children
        // are available or none of the available children have scoring values at all, then scoring is not even possible.
        if ($availableScoringOptions->isEmpty()) {
            return false;
        }

        // all available options must have scoring values set, otherwise calculations would become way too complex & shaky
        if ($availableOptions->count() !== $availableScoringOptions->count()) {
            return false;
        }

        foreach ($this->getAvailableOptions()->getHavingScoringValue() as $availableScoringOption) {
            if ($availableScoringOption->getScoringValue() < $scoringParameters->getMinScore()) {
                return false;
            }

            if ($availableScoringOption->getScoringValue() > $scoringParameters->getMaxScore()) {
                return false;
            }
        }

        return true;
    }

    public function jsonSerialize(): array
    {
        return array_merge(
            parent::jsonSerialize(),
            [
                self::KEY_ROLES_ALLOWED_TO_SEE_OPTION_SCORE_VALUES => $this->getRolesAllowedToSeeOptionScoreValues(),
                self::KEY_PREFILLING_QUESTION_UIDS => $this->getPrefillingQuestionUids(),
            ]
        );
    }

    /**
     * attach an option child to the choice question.
     *
     * @param NodeInterface|null $attachAfterChild optional existing choice optionNode to position given child optionNode after
     */
    protected function validateAttachingChild(NodeInterface $child, ?NodeInterface $attachAfterChild = null): void
    {
        if (!$child instanceof OptionInputNode) {
            throw new \InvalidArgumentException(' cannot attach child ['.$child->getUid().'] on parent choice node ['.$this->getUid().']. Expected child to be instance of '.OptionInputNode::class.', got: '.$child::class);
        }

        $this->traitValidateAttachingChild($child, $attachAfterChild);
    }

    /**
     * replace an option child in the choice question.
     *
     * @throws \InvalidArgumentException if child is not a valid option child
     */
    protected function validateReplacingChild(NodeInterface $child): void
    {
        if (!$child instanceof OptionInputNode) {
            throw new \InvalidArgumentException(' cannot replace child on parent choice node ['.$this->getUid().']. Expected the replacement child ['.$child->getUid().'] to be instance of '.OptionInputNode::class.', got: '.$child::class);
        }

        $this->traitValidateReplacingChild($child);
    }
}
