<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Input\CheckboxInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use Webmozart\Assert\Assert;

/**
 * Class CheckBoxInputNode.
 *
 * creates a question to allow end-users (clients) to check/uncheck a checkbox as answer.
 *
 * NOTE: when a user answers this type of question, the answer value will be either TRUE or FALSE
 * all answers to any questions are stored in a single dimension array 'formValues' that is then serialized in the DB table
 * related to the @see StoredTemplateTargetInterface. for checkbox questions, the answer value will be either TRUE or
 * FALSE and it will be stored with the uid of the question as key and a boolean as answer
 * e.g. $formValues = [
 *                  'uid-of-checkbox-question' => true,
 *                  'uid-of-other-checkbox-question' => false,
 *                  'uid-of-some-other-question-of-different-type' => 'answer-from-that-other-question',
 *                  ...
 *               ]
 */
final class CheckBoxInputNode extends AbstractParentalInputNode implements ParentalInputNode
{
    use Lockable;

    public static function getType(): string
    {
        return self::TYPE_PREFIX.'_'.CheckboxInput::getType();
    }

    public static function fromUidAndDependencies(
        string $uid,
        CheckboxInput $input,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        bool $hidden,
        string $descriptionDisplayMode,
    ): self {
        return new self($uid, $input, $flowPermission, $info, $hidden, $descriptionDisplayMode);
    }

    /**
     * creates a checkbox node with the bare minimum as a way to 'initialize' the node before updating it further.
     *
     * NOTE: updating of the node after using this factory might be required to make it actually 'usable',
     * since only the bare minimum is created to avoid this node to fail upon construct.
     * e.g. without flowPermission, this node will not be accessible for any user and thus will never be shown
     * and worse, the template will not allow to be published because every inputNode must have at least some access,
     * otherwise it's useless and only dead baggage.
     */
    public static function fromUidAndMinimalDependencies(
        string $uid,
        CheckboxInput $input,
        LocalizedInfoWithRequiredLabel $info,
    ): self {
        return new self(
            $uid,
            $input,
            FlowPermission::createEmpty(),
            $info,
            false,
            self::DESCRIPTION_DISPLAY_MODE_ICON
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws \Exception
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $descriptionDisplayMode = self::DESCRIPTION_DISPLAY_MODE_ICON;
        if (isset($data[self::KEY_DESCRIPTION_DISPLAY_MODE])) {
            $descriptionDisplayMode = $data[self::KEY_DESCRIPTION_DISPLAY_MODE];
        }

        $node = new self(
            $data[self::KEY_UID],
            CheckboxInput::fromJsonDecodedArray($data[self::KEY_INPUT]),
            FlowPermission::fromJsonDecodedArray($data[self::KEY_FLOW_PERMISSION]),
            LocalizedInfoWithRequiredLabel::fromJsonDecodedArray($data[self::KEY_INFO]),
            $data[self::KEY_HIDDEN],
            $descriptionDisplayMode
        );

        if (isset($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS])) {
            $node->setMultiUnlockingQuestions($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS]);
        }

        return $node;
    }

    /**
     * overwritten for type hinting purposes.
     */
    public function getInput(): CheckboxInput
    {
        /** @var CheckboxInput $result */
        $result = parent::getInput();

        return $result;
    }

    public function updateInput(InputInterface $input): void
    {
        Assert::isInstanceOf($input, CheckboxInput::class);

        $this->input = $input;
    }
}
