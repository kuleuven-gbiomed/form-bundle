<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\NumberInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\CalculationDependencyConfigCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;

/**
 * Class NumberInputNode.
 *
 * creates a question to allow end-users (clients) to submit a numeric value within configured boundaries & rounding mode.
 *
 * with option to use question as a score question dependency is global scoring or as a global scoring question itself
 *
 * NOTE: when a user answers this type of question, the answer value will be either a numeric value or NULL
 * all answers to any questions are stored in a single array 'formValues' that is then serialized in the DB table
 * related to the @see StoredTemplateTargetInterface. for number questions, the answer will be stored with the uid of
 * the question as key in that array and the value will a numeric value or NULL (if question is not required and was answered blank)
 * e.g. $formValues = [
 *                  'uid-of-number-question' => 15.5,
 *                  'uid-of-other-number-question-that-is-not-required-and-answered-blank' => NULL,
 *                  'uid-of-some-other-question-of-different-type' => 'answer-from-that-other-question',
 *                  ...
 *               ]
 */
final class NumberInputNode extends ScorableInputNode
{
    use Lockable;

    public static function getType(): string
    {
        return self::TYPE_PREFIX.'_'.NumberInput::getType();
    }

    /**
     * @param LocalizedInfoWithRequiredLabel $info with label required & asserted (label is the actual textual 'question')
     *
     * @throws \Exception
     */
    public static function fromUidAndDependencies(
        string $uid,
        NumberInput $input,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        bool $hidden,
        string $descriptionDisplayMode,
        CalculationDependencyConfigCollection $globalScoreCalculationDependencyConfigs,
        string $globalScoreWeighingType,
        array $customOptions = [],
    ): self {
        return new self(
            $uid,
            $input,
            $flowPermission,
            $info,
            $hidden,
            $descriptionDisplayMode,
            $globalScoreCalculationDependencyConfigs,
            $globalScoreWeighingType,
            $customOptions
        );
    }

    /**
     * creates a number node with the bare minimum as a way to 'initialize' the node before updating it further.
     *
     * NOTE: updating of the node after using this factory might be required to make it actually 'usable',
     * since only the bare minimum is created to avoid this node to fail upon construct.
     * e.g. without flowPermission, this node will not be accessible for any user and thus will never be shown
     * and worse, the template will not allow to be published because every inputNode must have at least some access,
     * otherwise it's useless and only dead baggage.
     *
     * @throws \Exception
     */
    public static function fromUidAndMinimalDependencies(
        string $uid,
        NumberInput $input,
        LocalizedInfoWithRequiredLabel $info,
    ): self {
        return new self(
            $uid,
            $input,
            FlowPermission::createEmpty(),
            $info,
            false,
            self::DESCRIPTION_DISPLAY_MODE_ICON,
            CalculationDependencyConfigCollection::createEmpty(),
            self::GLOBAL_SCORE_DEFAULT_WEIGHING_TYPE,
            []
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws InputInvalidArgumentException
     * @throws \Exception
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $descriptionDisplayMode = self::DESCRIPTION_DISPLAY_MODE_ICON;
        if (isset($data[self::KEY_DESCRIPTION_DISPLAY_MODE])) {
            $descriptionDisplayMode = $data[self::KEY_DESCRIPTION_DISPLAY_MODE];
        }

        $globalScoreCalculationDependencyConfigsData = [];
        if (isset($data[self::KEY_GLOBAL_SCORE_CALCULATION_DEPENDENCY_CONFIGS])) {
            $globalScoreCalculationDependencyConfigsData = $data[self::KEY_GLOBAL_SCORE_CALCULATION_DEPENDENCY_CONFIGS];
        }

        $globalScoreWeighingType = static::GLOBAL_SCORE_DEFAULT_WEIGHING_TYPE;
        if (isset($data[self::KEY_GLOBAL_SCORE_WEIGHING_TYPE])) {
            $globalScoreWeighingType = $data[self::KEY_GLOBAL_SCORE_WEIGHING_TYPE];
        }

        $node = new self(
            $data[self::KEY_UID],
            NumberInput::fromJsonDecodedArray($data[self::KEY_INPUT]),
            FlowPermission::fromJsonDecodedArray($data[self::KEY_FLOW_PERMISSION]),
            LocalizedInfoWithRequiredLabel::fromJsonDecodedArray($data[self::KEY_INFO]),
            $data[self::KEY_HIDDEN],
            $descriptionDisplayMode,
            CalculationDependencyConfigCollection::fromJsonDecodedArray($globalScoreCalculationDependencyConfigsData),
            $globalScoreWeighingType,
            parent::getCustomOptionsFromJsonDecodedArray($data)
        );

        if (isset($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS])) {
            $node->setMultiUnlockingQuestions($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS]);
        }

        return $node;
    }

    /**
     * overwritten for type hinting purposes.
     */
    public function getInput(): NumberInput
    {
        /** @var NumberInput $result */
        $result = parent::getInput();

        return $result;
    }

    public function updateInput(InputInterface $input): void
    {
        // Assert::isInstanceOf($input, NumberInput::class);

        $this->input = $input;
    }
}
