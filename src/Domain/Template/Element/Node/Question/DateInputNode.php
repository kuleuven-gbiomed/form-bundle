<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Input\DateInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use Webmozart\Assert\Assert;

/**
 * Class DateInputNode.
 *
 * Creates a question to allow end-user to submit a string as formatted as a date-string.
 * Also in the front-end a helper date-selector (aka date-picker) will be rendered to help the end user
 * to select a valid date. Some extra validation is also performed to be sure that the inputted information is
 * formatted as a date string.
 *
 * The answer value is stored as key value pair in the database, with the value as an iso formatted date string.
 *
 * e.g. $formValue = ['uid-of-date-question' => '2021-10-01T00:00:00+0200']
 */
final class DateInputNode extends AbstractParentalInputNode implements ParentalInputNode
{
    use Lockable;

    /* --------------------------------------------------------------------------------------------- static factories */

    public static function fromUidAndDependencies(
        string $uid,
        DateInput $input,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        bool $hidden,
        string $descriptionDisplayMode,
    ): self {
        return new self(
            $uid,
            $input,
            $flowPermission,
            $info,
            $hidden,
            $descriptionDisplayMode
        );
    }

    public static function fromUidAndMinimalDependencies(
        string $uid,
        DateInput $input,
        LocalizedInfoWithRequiredLabel $info,
    ): self {
        return new self(
            $uid,
            $input,
            FlowPermission::createEmpty(),
            $info,
            false,
            self::DESCRIPTION_DISPLAY_MODE_ICON
        );
    }

    /* ------------------------------------------------------------------------------------------------------ getters */

    public static function getType(): string
    {
        return self::TYPE_PREFIX.'_'.DateInput::getType();
    }

    /**
     * Overwritten for type hinting purposes.
     */
    public function getInput(): DateInput
    {
        /** @var DateInput $result */
        $result = parent::getInput();

        return $result;
    }

    public function updateInput(InputInterface $input): void
    {
        Assert::isInstanceOf($input, DateInput::class);
        $this->input = $input;
    }

    /* ------------------------------------------------------------------------------------------ json input & output */
    /**
     * @throws LocalizedStringException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        self::validateJsonDecodeArrayData($data);

        $descriptionDisplayMode = self::DESCRIPTION_DISPLAY_MODE_ICON;
        if (isset($data[self::KEY_DESCRIPTION_DISPLAY_MODE])) {
            $descriptionDisplayMode = $data[self::KEY_DESCRIPTION_DISPLAY_MODE];
        }

        $node = new self(
            $data[self::KEY_UID],
            DateInput::fromJsonDecodedArray($data[self::KEY_INPUT]),
            FlowPermission::fromJsonDecodedArray($data[self::KEY_FLOW_PERMISSION]),
            LocalizedInfoWithRequiredLabel::fromJsonDecodedArray($data[self::KEY_INFO]),
            $data[self::KEY_HIDDEN],
            $descriptionDisplayMode
        );

        if (isset($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS])) {
            $node->setMultiUnlockingQuestions($data[self::KEY_MULTI_UNLOCKING_QUESTION_UIDS]);
        }

        return $node;
    }
}
