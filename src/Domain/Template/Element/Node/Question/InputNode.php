<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\AbstractNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ChildrenAwareInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\StepPermissionCollection;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Utility\AbstractLocalizedInfo;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Role;
use Symfony\Component\Form\AbstractType;

/**
 * Class InputNode.
 *
 * configuration of a node as question in a @see FormList
 * with
 *   - a required label that operates as the actual textual question (in @see NodeInterface::getInfo())
 *   - optional description(s) (in @see NodeInterface::getInfo()) and other optional display options
 *   - an @see InputInterface object containing configuration on how to render the question, if & what constraints to
 *     apply on of question submit, and other stuff/optionals. the input objects with all their properties follow closely
 *     the (customized) Symfony form types (@see AbstractType ) for which they are eventually used to build an input
 *     field for the question and handling (validating) (submitted) values coming through that field.
 *   - a set of access rights (@see FlowPermission )
 *     per step known to the @see WorkFlow and
 *     per role known & participating in the @see TemplateInterface::getParticipatingRolesHierarchy()
 *     note: when admins are building and/or altering a @see TemplateInterface , they do not build the whole of it in one giant
 *     page. it's built/altered in regulated bits and pieces. so the questions could be build/altered before or after
 *     the workflow is built/altered. which means that the questions could have no access rights (as in an
 *     empty @see StepPermissionCollection for the flowPermission) or obsolete access rights according to the
 *     templates @see WorkFlow (e.g. a step has been deleted). e.g. resulting in a workflow step that has no
 *     questions whatsoever that allow anybody any access, which make the step pointless as noone will ever get to
 *     access anything in it. (although a question on itself that grants no access to anybody, is allowed, either because
 *     questions are made while no workflow steps yet exist or even because admin wants it as a prepared question for later)
 *     the admin side is aimed to try to help/warn admins & automate sync between question-workflow as much as possible
 *     but it is still on publish of a new/altered working version of a template that both questions & workflow are
 *     validated, both apart and as inter-depending elements of a template
 *
 * JV: Maybe InputNode should be an interface instead of an abstract class?
 */
abstract class InputNode extends AbstractNode
{
    /** @var string */
    public const KEY_FLOW_PERMISSION = 'flowPermission';
    /** @var string */
    public const KEY_INPUT = 'input';
    /** @var string */
    public const KEY_DESCRIPTION_DISPLAY_MODE = 'descriptionDisplayMode';

    // TODO SCONE MIGRATING DATA: corresponds to const TYPE = 'input'. ALL children use same. use $data[InputNode::KEY_INPUT][InputInterface::KEY_TYPE] to check;
    /** @var string */
    public const TYPE_PREFIX = 'question';
    /** @var string */
    public const DESCRIPTION_DISPLAY_MODE_ICON = 'icon';
    /** @var string */
    public const DESCRIPTION_DISPLAY_MODE_BOX = 'box';

    /**
     * @psalm-suppress PropertyNotSetInConstructor handled by extending children via an implemented updateInput method
     *
     * configuration of the question is to be treated as actual form input (rendering options, submit value constraints,..).
     */
    protected InputInterface $input;

    /**
     * @psalm-suppress PropertyNotSetInConstructor handled by extending children via an implemented updateFlowPermission method
     *
     * configuration of access rights to the question per step and role.
     */
    private FlowPermission $flowPermission;

    /**
     * @psalm-suppress PropertyNotSetInConstructor handled by extending children via an implemented updateDescriptionDisplayMode method
     *
     * how prominently visible the description(s) (if any) are to be displayed in views (UX).
     */
    private string $descriptionDisplayMode;

    /**
     * @param LocalizedInfoWithRequiredLabel $info with optional description(s) & required label (= the actual 'question')
     */
    protected function __construct(
        string $uid,
        InputInterface $input,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        bool $hidden,
        string $descriptionDisplayMode,
    ) {
        parent::__construct($uid, $info, $hidden);

        $this->updateInput($input);
        $this->updateFlowPermission($flowPermission);
        $this->updateDescriptionDisplayMode($descriptionDisplayMode);
    }

    /**
     * overwritten for type hinting purposes.
     */
    public function getInfo(): LocalizedInfoWithRequiredLabel
    {
        /** @var LocalizedInfoWithRequiredLabel $result */
        $result = parent::getInfo();

        return $result;
    }

    /**
     * no method overloading, can't type hint argument on implementation and the pass to parent: explicit implementation assert.
     */
    public function updateInfo(AbstractLocalizedInfo $info): void
    {
        if (!($info instanceof LocalizedInfoWithRequiredLabel)) {
            throw new \InvalidArgumentException('expect info to be instance of '.LocalizedInfoWithRequiredLabel::class.' got '.$info::class);
        }

        parent::updateInfo($info);
    }

    public function getFlowPermission(): FlowPermission
    {
        return $this->flowPermission;
    }

    public function updateFlowPermission(FlowPermission $flowPermission): void
    {
        $this->flowPermission = $flowPermission;
    }

    /**
     * revoke all access for given role (= no access for given role in any step).
     */
    public function revokeFlowPermissionForRoleInAllSteps(Role $role): void
    {
        $this->updateFlowPermission($this->getFlowPermission()->withStepPermissionsRevokedForRole($role->getName()));
    }

    public function revokeFlowPermissionForRoleInStep(Role $role, StepInterface $step): void
    {
        $this->updateFlowPermission(
            $this->getFlowPermission()->withPermissionsRevokedForRoleInStepUid(
                $role->getName(),
                $step->getUid()
            )
        );
    }

    public function revokeFlowPermissionForAllRolesInStep(StepInterface $step): void
    {
        $this->updateFlowPermission(
            $this->getFlowPermission()->withPermissionsRevokedInStepUidForAllRoles(
                $step->getUid()
            )
        );
    }

    /**
     * revoke all access: reset flow permission access to zero: no access for any role in any step.
     */
    public function revokeFlowPermissionForAllRolesInAllSteps(): void
    {
        $this->updateFlowPermission(FlowPermission::createEmpty());
    }

    /**
     * IMPORTANT: this does not care or take in account if the node is @see NodeInterface::isReachableInTree() or not.
     */
    public function isStepRoleWriteAccessGranted(StepInterface $step, Role $role): bool
    {
        return $this->getFlowPermission()->isStepRoleWriteAccessGranted($step->getUid(), $role->getName());
    }

    /**
     * IMPORTANT: this does not care or take in account if the node is @see NodeInterface::isReachableInTree() or not.
     */
    public function isStepRoleReadAccessGranted(StepInterface $step, Role $role): bool
    {
        return $this->getFlowPermission()->isStepRoleReadAccessGranted($step->getUid(), $role->getName());
    }

    /**
     * IMPORTANT: this does not care or take in account if the node is @see NodeInterface::isReachableInTree() or not.
     */
    public function isStepRoleReadOnlyAccessGranted(StepInterface $step, Role $role): bool
    {
        return $this->getFlowPermission()->isStepRoleReadOnlyAccessGranted($step->getUid(), $role->getName());
    }

    public function getInput(): InputInterface
    {
        return $this->input;
    }

    public function isRequired(): bool
    {
        return $this->getInput()->isRequired();
    }

    abstract public function updateInput(InputInterface $input): void;

    public function isUploadNode(): bool
    {
        return $this instanceof UploadInputNode;
    }

    public function isNumberNode(): bool
    {
        return $this instanceof NumberInputNode;
    }

    public function isCheckboxNode(): bool
    {
        return $this instanceof CheckBoxInputNode;
    }

    public function isOptionNode(): bool
    {
        return $this instanceof OptionInputNode;
    }

    public function isTextNode(): bool
    {
        return $this instanceof TextInputNode;
    }

    public function isChoiceNode(): bool
    {
        return $this instanceof ChoiceInputNode;
    }

    public function getDescriptionDisplayMode(): string
    {
        return $this->descriptionDisplayMode;
    }

    public function updateDescriptionDisplayMode(string $descriptionDisplayMode): void
    {
        if ('' === $descriptionDisplayMode) {
            throw new \InvalidArgumentException('parameter descriptionDisplayMode must be a non empty string');
        }

        if (!in_array($descriptionDisplayMode, [self::DESCRIPTION_DISPLAY_MODE_ICON, self::DESCRIPTION_DISPLAY_MODE_BOX], true)) {
            throw new \InvalidArgumentException('expected parameter descriptionDisplayMode to be one of the following: '.implode(' | ', [self::DESCRIPTION_DISPLAY_MODE_ICON, self::DESCRIPTION_DISPLAY_MODE_BOX]).'. got '.$descriptionDisplayMode);
        }

        $this->descriptionDisplayMode = $descriptionDisplayMode;
    }

    /**
     * @throws MissingNodeException
     */
    public function hasAncestorWithDisplayModeMatrix(): bool
    {
        return $this->getParent()->hasAncestorWithDisplayModeMatrix();
    }

    public function getNestedLabelForFallBackLocale(string $glue = ' - '): string
    {
        return $this->getNestedLabel($this->getInfo()->getLocalizedLabel()->getFallbackLocale(), $glue);
    }

    public function getLabelForFallBackLocale(): string
    {
        return $this->getLabel($this->getInfo()->getLocalizedLabel()->getFallbackLocale());
    }

    public function jsonSerialize(): array
    {
        $array = [
            self::KEY_UID => $this->getUid(),
            self::KEY_TYPE => static::getType(),
            self::KEY_INPUT => $this->getInput(),
            self::KEY_FLOW_PERMISSION => $this->getFlowPermission(),
            self::KEY_INFO => $this->getInfo(),
            self::KEY_HIDDEN => $this->isHidden(),
            self::KEY_DESCRIPTION_DISPLAY_MODE => $this->getDescriptionDisplayMode(),
        ];

        if ($this instanceof ChildrenAwareInterface) {
            $array[ChildrenAwareInterface::KEY_CHILDREN] = $this->getChildren();
        }

        return $array;
    }
}
