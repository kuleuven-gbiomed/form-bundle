<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ScorableInputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChart\PlottingRequirements;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\ScoringParameters;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\CalculationDependencyConfigCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\CalculationScoreNodeConfigCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepCollectionInterface;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\TemplateTarget\Update\AbstractUpdated;
use Webmozart\Assert\Assert;

/**
 * Class ScorableInputNode.
 *
 * bundles logic for node implementing contract @see OperableForScoring
 */
abstract class ScorableInputNode extends AbstractParentalInputNode implements PlottableInRadarChart, OperableForScoring
{
    /** @var string */
    final public const KEY_CUSTOM_OPTIONS = 'custom_options';
    /** @var string */
    final public const KEY_RECALCULATE_ON_EACH_SUBMIT_IN_STEPS = 'recalculate_on_each_submit_in_steps';
    /** @var string */
    final public const KEY_IGNORE_LOCKED_QUESTIONS_IN_CALCULATION = 'ignore_locked_questions_in_calculation';

    final public const ALLOWED_GLOBAL_SCORE_WEIGHING_TYPES = [self::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE, self::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE];

    /**
     * configs with score node references (i.e. dependencies ) for global score calculation.
     */
    private CalculationDependencyConfigCollection $globalScoreCalculationDependencyConfigs;

    /**
     * performance enhancing locally cached set of configured score nodes, filtered from the set of.
     *
     * @see getGlobalScoreCalculationDependencyConfigs on being fully valid for global score calculation
     *
     * NOTE: building the score nodes with configuartion requires multiple recursive searching through the rootNode.
     * since this set of score nodes are only needed op specific operations, prominently when calculation is required,
     * this set is not build upon construction of node to gain performance but build and cached once on first need/call
     */
    private ?CalculationScoreNodeConfigCollection $cacheValidGlobalScoreCalculationScoreNodeConfigs = null;

    private string $globalScoreWeighingType;

    /**
     * ChoiceInputNode constructor.
     *
     * argument $info: with label required & asserted (label is the actual textual 'question')
     *
     * argument $customOptions:
     * this bundle is in constant WIP. this prop allows to add additional options. which by preference should all remain
     * optional and/or should not be of the all-that important kind. this property is added as trade-off for the WIP
     * status to allow to code faster without breaking existing code and tests. values(s) for any custom option should
     * either be a primitive type for the jsonSerialize to work or contain an object implementing @throws \Exception
     *
     *@see \JsonSerializable.
     */
    protected function __construct(
        string $uid,
        InputInterface $input,
        FlowPermission $flowPermission,
        LocalizedInfoWithRequiredLabel $info,
        bool $hidden,
        string $descriptionDisplayMode,
        CalculationDependencyConfigCollection $globalScoreCalculationDependencyConfigs,
        string $globalScoreWeighingType,
        private array $customOptions = [],
    ) {
        self::guardGlobalScoreCalculationDependencyConfigsForSelfUid($globalScoreCalculationDependencyConfigs, $uid);
        self::guardGlobalScoreWeighingType($globalScoreWeighingType);

        parent::__construct($uid, $input, $flowPermission, $info, $hidden, $descriptionDisplayMode);

        $this->globalScoreCalculationDependencyConfigs = $globalScoreCalculationDependencyConfigs;
        $this->globalScoreWeighingType = $globalScoreWeighingType;
    }

    public function getInputScoringParameters(): ScoringParameters
    {
        $input = $this->getInput();

        if (!$input instanceof ScorableInputInterface) {
            throw new \DomainException('can not retrieve scoring parameters from input of class ['.$input::class.']: it is not contracted to '.ScorableInputInterface::class);
        }

        return $input->getScoringParameters();
    }

    public function getPlottingRequirements(): PlottingRequirements
    {
        return PlottingRequirements::fromScoringParameters($this->getInputScoringParameters());
    }

    public function getGlobalScoreCalculationDependencyConfigs(): CalculationDependencyConfigCollection
    {
        return $this->globalScoreCalculationDependencyConfigs;
    }

    public function updateGlobalScoreCalculationDependencyConfigs(
        CalculationDependencyConfigCollection $globalScoreCalculationDependencyConfigs,
    ): void {
        self::guardGlobalScoreCalculationDependencyConfigsForSelfUid($globalScoreCalculationDependencyConfigs, $this->getUid());

        $this->globalScoreCalculationDependencyConfigs = $globalScoreCalculationDependencyConfigs;
    }

    private static function guardGlobalScoreCalculationDependencyConfigsForSelfUid(
        CalculationDependencyConfigCollection $configs,
        string $selfUid,
    ): void {
        $usedScoreUids = [];

        foreach ($configs->getIterator() as $config) {
            if (in_array($config->getScoreNodeUid(), $usedScoreUids, true)) {
                throw new \Exception(sprintf("A calculation dependency configuration can not refer to the same score node twice: referencing with ID '%s' ", $config->getScoreNodeUid()));
            }

            if ($config->getScoreNodeUid() === $selfUid) {
                throw new \Exception(sprintf("A calculation dependency configuration can not refer to itself as a dependency: referencing with ID '%s' ", $config->getScoreNodeUid()));
            }

            $usedScoreUids[] = $config->getScoreNodeUid();
        }
    }

    public function getGlobalScoreWeighingType(): string
    {
        return $this->globalScoreWeighingType;
    }

    public function hasValidSumOfDependencyWeighingsForGlobalScoring(): bool
    {
        // only when calculation as global score has to be done using weighing of type percentage must the sum of the
        // weighings of the configured dependencies add up to exactly 100%
        if (self::GLOBAL_SCORE_WEIGHING_TYPE_PERCENTAGE === $this->getGlobalScoreWeighingType()) {
            return 100.0 === $this->getGlobalScoreCalculationDependencyConfigs()->getSumOfWeighings();
        }

        // sum of weights from the dependencies can not be zero when working with weighing type absolute
        // note: at time of writing, zero as sum of weights will result in a mathematical division by zero in calculations.
        // we can remove this check and allow zero if we deal with division by zero , when a use case requires it
        // only relevant for weighingType absolute. for percentage, it must be exactly 100 (%)
        if (self::GLOBAL_SCORE_WEIGHING_TYPE_ABSOLUTE === $this->getGlobalScoreWeighingType()) {
            return 0.0 !== $this->getGlobalScoreCalculationDependencyConfigs()->getSumOfWeighings();
        }

        return true;
    }

    public function updateGlobalScoreWeighingType(string $globalScoreWeighingType): void
    {
        self::guardGlobalScoreWeighingType($globalScoreWeighingType);

        $this->globalScoreWeighingType = $globalScoreWeighingType;
    }

    private static function guardGlobalScoreWeighingType(string $globalScoreWeighingType): void
    {
        if (!in_array($globalScoreWeighingType, self::ALLOWED_GLOBAL_SCORE_WEIGHING_TYPES, true)) {
            throw new \InvalidArgumentException('globalScoreWeighingType '.$globalScoreWeighingType.' is not an allowed global Score Weighing Type');
        }
    }

    /**
     * a question could be used a (global) score node if at least all scoring parameters are set.
     */
    public function meetsScoringRequirements(): bool
    {
        return $this->getInputScoringParameters()->makesScoringPossible();
    }

    /**
     * TODO somehow will have to make this faster/performant (heavy due to random position(s) of the globals in tree).
     *
     * a question operates as a score node if it meets score requirements and it is used as valid score
     * node dependency by at least one global score node somewhere in the tree (from rootNode down)
     *
     * @throws MissingNodeException
     */
    public function operatesAsScoreNode(): bool
    {
        if (!$this->meetsScoringRequirements()) {
            return false;
        }

        $operableAsGlobalScoreNodes = $this->getTreeRootNode()
            ->getFlattenedParentalInputNodeDescendants()
            ->getOperatingAsGlobalScoreNodes();

        // don't use collection filter here, return as quickly as possible for performance reasons
        /** @var OperableForScoring $operableAsGlobalScoreNode */
        foreach ($operableAsGlobalScoreNodes as $operableAsGlobalScoreNode) {
            if ($operableAsGlobalScoreNode->getValidGlobalScoreCalculationScoreNodeConfigs()->hasForScoreNode($this)) {
                return true;
            }
        }

        return false;
    }

    /**
     * a question operates as a score node for given global score node if it meets score requirements and it is
     * used as valid score node dependency for the given global score node.
     *
     * NOTE: for performance reasons, we do NOT call @see operatesAsScoreNode here. instead we repeat its logic
     * by calling the @see meetsScoringRequirements explicitly and checking if the presence of this node in the
     * dependencies of the given global score node. which is a lot faster than going through the whole tree first.
     */
    public function operatesAsScoreNodeForGlobalScoreNode(OperableForScoring $globalScoreNode): bool
    {
        if (!$this->meetsScoringRequirements()) {
            return false;
        }

        return $globalScoreNode->getValidGlobalScoreCalculationScoreNodeConfigs()->hasForScoreNode($this);
    }

    /**
     * a question operates as a global score node if it meets score requirements and all its score node dependency
     * are also meet scoring requirements (both global score and dependencies must all validly be configured for scoring).
     *
     * @throws MissingNodeException
     */
    public function operatesAsGlobalScoreNode(): bool
    {
        if (!$this->meetsScoringRequirements()) {
            return false;
        }

        if (!$this->isReachableInTree()) {
            return false;
        }

        // there should be at least one valid calculation dependency configured.
        $validScoreNodeConfigs = $this->getValidGlobalScoreCalculationScoreNodeConfigs();
        if ($validScoreNodeConfigs->isEmpty()) {
            return false;
        }

        // each configuration should point to an associated score node that is fully valid for scoring on itself
        // and the sum of the weighings of each dependency should be valid, depending on the weighing type.
        return ($validScoreNodeConfigs->count() === $this->getGlobalScoreCalculationDependencyConfigs()->count())
            && $this->hasValidSumOfDependencyWeighingsForGlobalScoring();
    }

    /**
     * use the configs, each with a reference to a score node uid, to build actual scoreNodes that are usable for
     * calculation, skipping references to non-existing score nodes.
     *
     * NOTE: the use of bot a @see CalculationDependencyConfigCollection
     * and a @see CalculationScoreNodeConfigCollection is done both for serializing & de-serializing purposes,
     * but as well to ease the work of admins: when they update/adjust a template, they might remove questions.
     * if a question is removed (or updated as such that it's not anymore usable for calculation), we don't
     * want to break the re-publish because of that. for a removed question, that really helps admins, but
     * the down-side is that that if an admins updates a question that now is no longer usable for calculation,
     * they might or might not be aware that it will no longer be used in the calculation. that's only one
     * reason why the admin-side will have to do some soft, non publish-blocking - validation on (global) score
     * questions to give admin a chance to alter (or not alter) the dependencies and other related stuff. there
     * is just too much that is needed for calculation to simply break-off any publish attempt by admins due
     * to 'invalid' (global) score nodes..
     *
     * NOTE: in theory, a node can be used twice (or more) to calculate a global score.
     * hence no assert on uniqueness of node is done when building the scoreNodes
     *
     * @throws MissingNodeException
     */
    public function getValidGlobalScoreCalculationScoreNodeConfigs(): CalculationScoreNodeConfigCollection
    {
        if (!$this->cacheValidGlobalScoreCalculationScoreNodeConfigs instanceof CalculationScoreNodeConfigCollection) {
            $this->cacheValidGlobalScoreCalculationScoreNodeConfigs = $this->getAllGlobalScoreCalculationScoreNodeConfigs()->getValidForCalculation();
        }

        return $this->cacheValidGlobalScoreCalculationScoreNodeConfigs;
    }

    public function getAllGlobalScoreCalculationScoreNodeConfigs(): CalculationScoreNodeConfigCollection
    {
        return CalculationScoreNodeConfigCollection::fromRootNodeAndDependencyConfigs(
            $this->getTreeRootNode(),
            $this->getGlobalScoreCalculationDependencyConfigs()
        );
    }

    public function getCustomOptions(): array
    {
        return $this->customOptions;
    }

    /**
     * check if should allow to override the standard one-time-only server side calculation of this global score and
     * allow to (re)calculate again upon each submit action, even if it was already calculated before, as long as
     * the form in is given step.
     *
     * the standard use case for server side calculation of a global score based on its score dependencies is that it
     * is done only once when, upon a successful submit action by some user, the system checks and concludes
     *  1 that there is not yet a value submitted and stored by a previous successful submit (either manually or calculated)
     *  2 that global score is not manually submitted in this submit action (i.e. the submitting user has write access
     *    on this global score question and he/she has manually supplied answer)
     *  3 that all score dependency questions for a certain global score are available at that moment (i.e. submitted
     *    either during current or previous successful submit action(s)) and all have a valid calculable value
     * only then is the calculation executed. the value is then calculated and it is stored as answer for that global
     * score question. after that, standard behaviour is that upon each next successful submit by same or other user,
     * this 1-2-3 check is executed again and the system will skip calculation because of 1: there is already a value
     * submitted and stored!
     *
     * @see AbstractUpdated::processGlobalScoreInputNode()
     *
     * this method ignores 1 and enforces the (re)calculation of the global score over and over again for given step,
     * as long as the submit action is in that step. But conditions 2 and 3 still apply!
     *
     * this step related enforcement of recalculation has the benefit that as long as the step is open and submitting
     * user has write access to edit the score dependencies over and over, the global score value will silently be
     * updated with a (possibly) new calculated value upon each successful submit action, based on changes to the
     * score dependencies.
     *
     * NOTE for 2: a calculation is never executed if the submitting user has write access to that question at
     * the moment of submit: this allows user to manually suplly an answer. this also means that user can actually
     * see the value of that global score since he/she can fill in an answer. while server side calculation upon
     * submit means that user had no write (and usually but not exclusively also no read access) access to the global
     * score and the user is thus unaware of calculation triggered by his/her successful submit.
     */
    public function isRecalculatingOnEachSubmitInStep(StepInterface $step): bool
    {
        $customOptions = $this->getCustomOptions();

        if (!isset($customOptions[self::KEY_RECALCULATE_ON_EACH_SUBMIT_IN_STEPS])) {
            return false;
        }

        $steps = $customOptions[self::KEY_RECALCULATE_ON_EACH_SUBMIT_IN_STEPS];

        return is_array($steps) && in_array($step->getUid(), $steps, true);
    }

    /**
     * @param StepCollectionInterface<array-key, StepInterface> $steps
     */
    public function updateRecalculateOnEachSubmitInSteps(StepCollectionInterface $steps): void
    {
        $this->customOptions[self::KEY_RECALCULATE_ON_EACH_SUBMIT_IN_STEPS] = $steps->getStepUids();
    }

    /**
     * if this question operates as a global score question (@see ScorableInputNode::operatesAsGlobalScoreNode()),
     * then this setting determines if its dependency questions that are configured as locked questions but are not unlocked
     * when its calculation is triggered, should be ignored in that calculation so that the calculation is continued
     * with all the dependencies that are unlocked (and the dependencies that are not lockable to start with).
     * if it is configured to not ignore those not unlocked questions, and at least one locked dependecy question is
     * not unlocked, then the calculation will not be executed.
     * in both cases, the condition - as with all calculations - remains that all dependencies that should be taken in
     * account (the locked ones depending on this setting )have valid answer at the time the calculation is triggered.
     *
     * IMPORTANT: the default for this is TRUE, which means that if this key is not provided in the 'custom options',
     * then the locked questions that are not unlocked, will be ignored in the calculations
     *
     * If this function returns true, and we are dealing with a global score question, this means that if one or more of
     * the source questions that are configured as locked questions and are NOT unlocked, those not unlocked questions
     * are ignored for the score calculation. So only the scores (and weights) of the 'source questions' that are
     * not configured as locked or are configured as locked but are unlocked are taken into account for the calculation:
     * the not unlocked questions are thus ignored when calculating.
     * If this function returns false, then any locked dependency questions must be answered, thus be unlocked.
     *
     * Note that if this returns false (do not ignore not unlocked questions) it could make a calculated score impossible
     * to calculate, because if at least one of those locked questions is not unlocked, the calculation will not be executed.
     *
     * note in case the calculation uses the percentage 'weighing type' and this setting returns TRUE (ignore not unlocked questions),
     * this would be a big problem, because percentage always requires the sum of the source question weights being equal
     * to 100 and if the not unlocked questions were to be ignored, the sum of the weights of the remaining unlocked question
     * and/or questions that were not lockable, would not be 100% and calculation will fail hard (with Exception).
     * but this is caught when publishing a template: using percentage in combo with locked questions is not allowed or
     * only allowed with this setting set to FALSE.
     *
     * A typical use case (with this setting returning TRUE: ignore not unlocked questions) is a calculated score
     * with calculation type 'absolute' where the source questions contain 2 subsets of questions where either the first
     * set or the second subset is unlocked, depending on the 'unlocking' answer of one main question.
     * regardless of which of the two subsets is unlocked, the calculation will be able to be executed,
     * ignoring the locked subset of questions and using the unlocked subset.
     *
     * A combination of locked (not unlocked) and 'normal' questions can also be used, where the
     * locked questions then can only weigh on the calculation if they are unlocked.
     *
     * this setting is only used on global (total/subtotal) score questions. on any other questions, this setting has no effect at all.
     */
    public function ignoreAllNotUnlockedDependencyQuestionsInGlobalScoreCalculation(): bool
    {
        $customOptions = $this->getCustomOptions();

        // default behaviour is to ignore not unlocked questions in calculation
        if (!isset($customOptions[self::KEY_IGNORE_LOCKED_QUESTIONS_IN_CALCULATION])) {
            return true;
        }

        return true === $customOptions[self::KEY_IGNORE_LOCKED_QUESTIONS_IN_CALCULATION];
    }

    public function setGlobalScoreCalculationToIgnoreAllNotUnlockedDependencyQuestions(): void
    {
        $this->customOptions[self::KEY_IGNORE_LOCKED_QUESTIONS_IN_CALCULATION] = true;
    }

    public function unsetGlobalScoreCalculationToIgnoreAllNotUnlockedDependencyQuestions(): void
    {
        $this->customOptions[self::KEY_IGNORE_LOCKED_QUESTIONS_IN_CALCULATION] = false;
    }

    /**
     * @deprecated use setGlobalScoreCalculationToIgnoreAllNotUnlockedDependencyQuestions and unsetGlobalScoreCalculationToIgnoreAllNotUnlockedDependencyQuestions instead
     */
    public function updateIgnoreLockedQuestionsInCalculation(bool $ignoreLockedQuestions): void
    {
        $ignoreLockedQuestions ?
            $this->setGlobalScoreCalculationToIgnoreAllNotUnlockedDependencyQuestions() :
            $this->unsetGlobalScoreCalculationToIgnoreAllNotUnlockedDependencyQuestions();
    }

    protected static function getCustomOptionsFromJsonDecodedArray(array $data): array
    {
        return $data[self::KEY_CUSTOM_OPTIONS] ?? [];
    }

    public function jsonSerialize(): array
    {
        return array_merge(
            parent::jsonSerialize(),
            [
                self::KEY_GLOBAL_SCORE_CALCULATION_DEPENDENCY_CONFIGS => $this->getGlobalScoreCalculationDependencyConfigs(),
                self::KEY_GLOBAL_SCORE_WEIGHING_TYPE => $this->getGlobalScoreWeighingType(),
                self::KEY_CUSTOM_OPTIONS => $this->getCustomOptions(),
            ]
        );
    }
}
