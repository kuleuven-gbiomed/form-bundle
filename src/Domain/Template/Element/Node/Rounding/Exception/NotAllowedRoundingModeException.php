<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Rounding\Exception;

class NotAllowedRoundingModeException extends \Exception
{
}
