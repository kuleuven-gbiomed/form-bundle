<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node\Rounding;

use KUL\FormBundle\Domain\Template\Element\Node\Rounding\Exception\NotAllowedRoundingModeException;
use KUL\FormBundle\Utility\LocalizedNumberFormatter;

final class RoundingModes
{
    public const ROUND_HALF_UP = \PHP_ROUND_HALF_UP;  // e.g.: 1.4 => 1, 1.5 => 2, 1.6 => 2
    public const ROUND_HALF_DOWN = \PHP_ROUND_HALF_DOWN;
    public const DEFAULT = self::ROUND_HALF_UP;

    /**
     * @var array<int, int>
     */
    private const TO_NUMBER_FORMATTER_MAPPING = [
        self::ROUND_HALF_UP => \NumberFormatter::ROUND_HALFUP,
        self::ROUND_HALF_DOWN => \NumberFormatter::ROUND_HALFDOWN,
    ];

    /**
     * @return array<array-key, self::ROUND_HALF_UP|self::ROUND_HALF_DOWN>
     */
    public static function getAllowedModes(): array
    {
        return [
            self::ROUND_HALF_UP,
            self::ROUND_HALF_DOWN,
        ];
    }

    /**
     * Developer notice:
     * due to a long-running previous bug in this bundle in which @see \NumberFormatter::ROUND_HALFUP (integer 6)
     * was used instead of @see \PHP_ROUND_HALF_UP (integer 1), a lot of existing forms in DB of implementing apps have
     * integer 6 instead of integer 1 as rounding mode stored on questions using rounding.
     * this bundle uses PHP's round() function for rounding. this PHP function only accepts integers 1,2,3 and 4 as values
     * for its third (rounding) parameter but falls back to integer 1 if any other integer is passed in. This fallback
     * was a giant stroke of luck since it meant the rounding has always correctly fallen back to \PHP_ROUND_HALF_UP when
     * integer 6 was passed in during the time (years) this bug was in the code.
     * as more rounding modes will be allowed and the need for uniform usage & consistency of allowed rounding modes,
     * we need to assert only certain PHP rounding modes get through when question rounding modes are build/created - hence
     * this class - but we also need to make sure the old integer 6 gets through and is converted on-the-fly to integer 1
     * to make sure the existing forms in DB still work.
     *
     * note: at time of writing, only HALF_UP (ROUND_HALFUP/PHP_ROUND_HALF_UP) is used in this bundle and in all implementing apps
     * hence we only catch this specifically and ignore any others, including @see \NumberFormatter::ROUND_HALFDOWN (int 5)
     *
     * this previous bug was fixed here:
     *
     * @see https://jira.gbiomed.kuleuven.be/browse/FORM-275
     * @see https://gitlab.com/kuleuven-gbiomed/form-bundle/-/commits/master?search=FORM-275
     *
     * @phpstan-return self::ROUND_*
     */
    public static function extractAllowedMode(int $roundingMode): int
    {
        if (in_array($roundingMode, self::getAllowedModes(), true)) {
            return $roundingMode;
        }

        // catch older values in db using NumberFormatter::ROUND_HALFUP (6) instead of PHP_ROUND_HALF_UP (1)
        if (\NumberFormatter::ROUND_HALFUP === $roundingMode) {
            return self::ROUND_HALF_UP;
        }

        throw new NotAllowedRoundingModeException("rounding mode {$roundingMode} is not an allowed rounding mode nor associated with an allowed rounding mode (allowed modes: ".implode(',', self::getAllowedModes()).').');
    }

    /**
     * for usage in localized formatted number display in e.g. @see LocalizedNumberFormatter
     * note that the @see \NumberFormatter ROUND_ constants uses different integers than PHP' ROUND_ constants
     * fails if no mapping found.
     *
     * @phpstan-param RoundingModes::ROUND_* $allowedRoundingMode
     *
     * @phpstan-return \NumberFormatter::ROUND_*
     */
    public static function mapAllowedModeToNumberFormatterMode(int $allowedRoundingMode): int
    {
        return self::TO_NUMBER_FORMATTER_MAPPING[$allowedRoundingMode];
    }
}
