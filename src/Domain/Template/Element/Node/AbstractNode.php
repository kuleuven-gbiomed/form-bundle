<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element\Node;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ChildrenAwareInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\MissingNodeException;
use KUL\FormBundle\Domain\Template\Utility\AbstractLocalizedInfo;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Class AbstractNode.
 *
 * bundles some methods that all implementations of @see NodeInterface use
 */
abstract class AbstractNode implements NodeInterface, \JsonSerializable, Deserializable
{
    private readonly string $uid;
    /** @psalm-suppress PropertyNotSetInConstructor handled by extending children via an implementing updateInfo method*/
    private AbstractLocalizedInfo $info;
    private ?NodeInterface $parent = null;

    /** @var NodeCollection<array-key, NodeInterface>|null */
    private ?NodeCollection $cachedAncestors = null;

    protected function __construct(string $uid, AbstractLocalizedInfo $info, private bool $hidden)
    {
        if ('' === $uid) {
            throw new \InvalidArgumentException('uid must be a non empty string');
        }

        $this->uid = $uid;
        $this->updateInfo($info);
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getInfo(): AbstractLocalizedInfo
    {
        return $this->info;
    }

    public function updateInfo(AbstractLocalizedInfo $info): void
    {
        $this->info = $info;
    }

    public function updateHidden(bool $hidden): void
    {
        $this->hidden = $hidden;
    }

    /**
     * @throws LocalizedStringException
     */
    public function hasLabel(string $locale): bool
    {
        $info = $this->getInfo();

        if ($info instanceof LocalizedInfoWithOptionalLabel) {
            return $info->hasLabel($locale);
        }

        return $info instanceof LocalizedInfoWithRequiredLabel;
    }

    /**
     * @throws LocalizedStringException if node is a category (optional label) with no label for locale or fallback found
     */
    public function getLabel(string $locale): string
    {
        return $this->getInfo()->getLabel($locale);
    }

    /**
     * @throws LocalizedStringException
     */
    public function hasDescription(string $locale): bool
    {
        return $this->getInfo()->hasDescription($locale);
    }

    /**
     * @throws LocalizedStringException if no description found
     */
    public function getDescription(string $locale): string
    {
        return $this->getInfo()->getDescription($locale);
    }

    /**
     * @throws LocalizedStringException
     */
    public function hasRoleDescription(Role $role, string $locale): bool
    {
        return $this->getInfo()->hasRoleDescription($role, $locale);
    }

    /**
     * @throws LocalizedStringException
     */
    public function getRoleDescription(Role $role, string $locale): string
    {
        return $this->getInfo()->getRoleDescription($role, $locale);
    }

    public function hasFullDescription(string $locale, ?Role $role): bool
    {
        if ($this->hasDescription($locale)) {
            return true;
        }

        return $role instanceof Role && $this->hasRoleDescription($role, $locale);
    }

    public function getFullDescription(string $locale, ?Role $role): ?string
    {
        if (!($role instanceof Role)) {
            return $this->hasDescription($locale) ? $this->getDescription($locale) : null;
        }

        if ($this->hasDescription($locale) && $this->hasRoleDescription($role, $locale)) {
            return $this->getDescription($locale).'<br/>'.$this->getRoleDescription($role, $locale);
        }

        if ($this->hasDescription($locale)) {
            return $this->getDescription($locale);
        }

        if ($this->hasRoleDescription($role, $locale)) {
            return $this->getRoleDescription($role, $locale);
        }

        return null;
    }

    public function hasParent(): bool
    {
        return $this->parent instanceof NodeInterface;
    }

    /**
     * @psalm-return ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, ChildrenAwareInterface as NodeInterface>>
     *
     * @phpstan-return ChildrenAwareInterface<NodeInterface, NodeCollection<array-key, NodeInterface>>
     *
     * @throws MissingNodeException if no parent found
     */
    public function getParent(): ChildrenAwareInterface
    {
        if (!$this->parent instanceof ChildrenAwareInterface) {
            throw new MissingNodeException("no parent node found for node [{$this->getUid()}]");
        }

        return $this->parent;
    }

    /**
     * @throws \InvalidArgumentException if trying to add itself as parent
     */
    public function updateParent(NodeInterface $parent): self
    {
        if ($parent->getUid() === $this->getUid()) {
            throw new \InvalidArgumentException('cannot make node ['.$this->getUid().'] the parent of itself');
        }

        $this->parent = $parent;

        return $this;
    }

    /**
     * @return NodeCollection<array-key, NodeInterface>
     */
    public function getAncestors(): NodeCollection
    {
        // finding ancestors up the tree is performance hell, especially when done repeatedly. let's do it once, and only
        // when first asked for (thus not in the constructor!) and then 'cache' it in a property (similar to 'local caches' on @see FormList).
        if (null === $this->cachedAncestors) {
            $this->cachedAncestors = new NodeCollection($this->getAncestorsArray());
        }

        return $this->cachedAncestors;
    }

    public function getAncestorsArray(): array
    {
        $ancestors = [];
        if (!$this->hasParent()) {
            return $ancestors;
        }

        // keep order! first add ancestors (if any), then add direct parent as last
        $parent = $this->getParent();
        $parentAncestors = $parent->getAncestorsArray();
        if (0 !== count($parentAncestors)) {
            $ancestors = array_merge($ancestors, $parentAncestors);
        }

        $ancestors[] = $parent;

        return $ancestors;
    }

    /**
     * @return string[]
     */
    public function getAncestorsLabels(string $locale): array
    {
        $labels = [];

        foreach ($this->getAncestors() as $ancestor) {
            // skip unlabeled if label is optional and ancestor node has none (categories with optional label)
            if (!$ancestor->hasLabel($locale)) {
                continue;
            }

            $labels[] = $ancestor->getLabel($locale);
        }

        return $labels;
    }

    /**
     * is this the root category node in the tree or fail if there is none.
     *
     * @throws MissingNodeException
     */
    public function isTreeRootNode(): bool
    {
        return $this->getUid() === $this->getTreeRootNode()->getUid();
    }

    /**
     * get the root category node in the tree if there is none.
     *
     * @throws MissingNodeException
     */
    public function getTreeRootNode(): CategoryNode
    {
        // in a node tree, the only node without a parent is the root category node
        if (!$this->hasParent() && $this instanceof CategoryNode) {
            return $this;
        }

        // flattened collection of ancestors is build with rootNode being first in collection
        $firstAncestor = $this->getAncestors()->first();
        if ($firstAncestor instanceof CategoryNode) {
            return $firstAncestor;
        }

        throw new MissingNodeException("no category rootNode found for node [{$this->getUid()}]");
    }

    public function isHidden(): bool
    {
        return $this->hidden;
    }

    public function isReachableInTree(): bool
    {
        // if node is explicitly set as hidden, than obviously it's not reachable
        if ($this->isHidden()) {
            return false;
        }

        // if node has at least one ancestor in tree that is marked as hidden, then all descendants of that ancestor -
        // including this node - are considered unreachable.
        foreach ($this->getAncestors() as $ancestor) {
            if ($ancestor->isHidden()) {
                return false;
            }
        }

        return true;
    }

    public function getDepth(): int
    {
        $depth = 1;

        if ($this->hasParent()) {
            $depth += $this->getParent()->getDepth();
        }

        return $depth;
    }

    /**
     * check if node has ancestors with label and/or own label. only having own label is also a nested label.
     *
     * @throws LocalizedStringException
     */
    public function hasNestedLabel(string $locale): bool
    {
        $labels = $this->getAncestorsLabels($locale);

        if ($this->hasLabel($locale)) {
            $labels[] = $this->getLabel($locale);
        }

        return 0 !== count($labels);
    }

    /**
     * returns a concatenated - with optional delimiter - string of all labels of ancestors (if any) of this node
     * in top-down order of the tree's structure and own label (if any) added as last.
     *
     * this is used in error messages, export headers, etc. to show labels of inputNodes as single strings because
     * labels of inputNodes (i.e. the question string itself) do not have to be unique in a form since same question can
     * be repeated (but uid of each question's inputNode is & stays always unique). showing a question string prefixed
     * with it's ancestral category labels (if any) helps the users to identify those recurring questions by their
     * ancestral category structure.
     * only if this node is a category node without label and all its ancestors are categories without labels themselves
     * will there be no nested label and will an exception be thrown. only having own label is also a nested label
     *
     * @throws LocalizedStringException
     */
    public function getNestedLabel(string $locale, string $glue = ' - '): string
    {
        // get ancestral labels, in order of ancestry
        $labels = $this->getAncestorsLabels($locale);
        // add own label, if any, as last
        if ($this->hasLabel($locale)) {
            $labels[] = $this->getLabel($locale);
        }

        if (0 === count($labels)) {
            throw new \BadMethodCallException('no nested label found for node '.$this->getUid().' of type '.static::getType());
        }

        return implode($glue, $labels);
    }

    /**
     * check if the associative array coming from JSON decoded data has the correct type referenced
     * to create the requested node.
     */
    protected static function validateJsonDecodeArrayData(array $data): void
    {
        if (!array_key_exists(self::KEY_TYPE, $data)) {
            throw new \InvalidArgumentException('missing key ['.self::KEY_TYPE.'] in json decoded node array data');
        }

        if ($data[self::KEY_TYPE] === static::getType()) {
            return;
        }

        throw new \InvalidArgumentException('expected value for key ['.self::KEY_TYPE.'] in json decoded node array data to be ['.static::getType().']. got ['.$data[self::KEY_TYPE]);
    }

    public function getSequence(): int
    {
        $sequence = 0;
        foreach ($this->getSiblings() as $sibling) {
            if ($sibling->getUid() === $this->getUid()) {
                break;
            }
            ++$sequence;
        }

        return $sequence;
    }

    /**
     * zero based.
     *
     * @return list<int>
     */
    public function getPosition(): array
    {
        $position = [];
        $node = $this;
        while ($node->hasParent()) {
            array_unshift($position, $node->getSequence());
            $node = $node->getParent();
        }

        return $position;
    }

    /** 1 based */
    public function getPositionString(): string
    {
        return implode('.', array_map(fn ($index) => $index + 1, $this->getPosition()));
    }

    /**
     * @return NodeCollection<array-key, NodeInterface>
     */
    public function getSiblings(): NodeCollection
    {
        try {
            return $this->getParent()->getChildren();
        } catch (MissingNodeException) {
            return new NodeCollection();
        }
    }
}
