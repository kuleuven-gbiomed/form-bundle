<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;

interface FormListInterface
{
    public function hasNodeByUid(string $uid): bool;

    public function getNodeByUid(string $uid): NodeInterface;
}
