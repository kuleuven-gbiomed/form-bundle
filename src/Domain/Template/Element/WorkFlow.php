<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Element;

use KUL\FormBundle\Domain\Contract\Deserializable;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepCollectionInterface;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection;

/**
 * Class WorkFlow.
 *
 * contains all the steps that make up a workflow. a valid publishable workflow must have at least one step.
 * only one step at the time can be 'open' in terms of start & end date.
 *
 * NOTE: a workflow does not have to be fully valid (containing steps end having no overlap) as long as it is a
 * working version of a workflow. Only when used as part of @see TemplateInterface::publishNewCurrentVersion() to create
 * a new version of a template withh all its elements, does the workflow have to be valid for publish.
 * IN DETAIL: a step can exist on its own and even be invalid towards other steps in a workflow as long as the workflow
 * is not validated for publish. (e.g. like having overlapping steps periods: a start date for step 2 that comes before
 * end date of step 1). but once a step is part of a valid publishable workflow, then all steps must be valid in
 * comparison to each other. meaning that steps can not overlap each other because a workflow can only have exactly one
 * step 'open' at any time or no step open (e.g. workflow is then in between two steps, making the @see TemplateInterface
 * inaccessible for everybody). templates are build in bits and pieces by admins, hence a workflow does not has to have
 * all its step valid until the template using this workflow is published as part of a new template version.
 * (however, admin-side should  - non-breakingly - warn about invalid steps that admins are in the process of creating
 * inside a working version of workflow)
 */
final class WorkFlow implements \JsonSerializable, Deserializable
{
    /** @var string */
    public const KEY_STEPS_TYPE = 'stepsType';
    /** @var string */
    public const KEY_STEPS = 'steps';

    /** @var StepCollectionInterface<array-key, StepInterface> */
    private readonly StepCollectionInterface $steps;

    /**
     * @param StepCollectionInterface<array-key, StepInterface> $steps
     */
    private function __construct(StepCollectionInterface $steps)
    {
        // check for duplicate uids to assert that every uid of any step that is added is unique
        if (!$steps->hasUniqueSteps()) {
            throw new \InvalidArgumentException('the steps must be unique (must have unique uids), but found duplicate steps for uids ['.implode(' | ', $steps->getDuplicateStepUids()).'].');
        }

        $this->steps = $steps->getSortedByAscendingFlow();
    }

    /**
     * create a workflow with no steps that will allow to work with mixed steps.
     *
     * NOTE: could be useful in admin-side: right before admin wants to create the new workflow, we could ask admin
     * what type of steps he/she intends to use. if he/she intends to use both types (with fixed dates and with
     * calculated dates), then we use this method to pre-create it. that will hep to assert some stuff and advise
     * the admin, most importantly if admin starts to add steps: in a mixed set of steps, we can not organize
     * the steps by their start and end dates if a mix is truly used. we have to rely on order in which steps get added.
     * if we know beforehand that a mix will be sued, we can do more checks and help more on every steps that gets
     * added by admin: advise about the ordering and/or even asking for ordering by comparing new-to-add step against
     * the already present steps of the same kind.
     */
    public static function createEmptyForMixedCalculatedAndFixedDatesSteps(): self
    {
        return new static(MixedCalculatedAndFixedStepCollection::createEmpty());
    }

    /**
     * create a workflow with no steps that will allow to work with only steps of the @see StepWithCalculatedDateStart type.
     *
     * NOTE: could be useful in admin-side: right before admin wants to create the new workflow, we could ask admin
     * what type of steps he/she intends to use. if he/she intends to use only one type, namely the one with calculated
     * dates, then we have less work to do to check ordering and thus also less help to offer to the admin in comparison
     * to @see createEmptyForMixedCalculatedAndFixedDatesSteps
     */
    public static function createEmptyForCalculatedDatesSteps(): self
    {
        return new self(StepWithCalculatedDateStartCollection::createEmpty());
    }

    /**
     * create a workflow with no steps that will allow to work with only steps of the @see StepWithFixedDateStart type.
     *
     * NOTE: could be useful in admin-side: right before admin wants to create the new workflow, we could ask admin
     * what type of steps he/she intends to use. if he/she intends to use only one type, namely the one with fixed
     * dates, then we have less work to do to check ordering and thus also less help to offer to the admin in comparison
     * to @see createEmptyForMixedCalculatedAndFixedDatesSteps
     */
    public static function createEmptyForFixedDatesSteps(): self
    {
        return new self(StepWithFixedDateStartCollection::createEmpty());
    }

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public static function fromJsonDecodedArray(array $data): self
    {
        $type = $data[self::KEY_STEPS_TYPE];
        $stepsData = $data[self::KEY_STEPS];

        if ($type === StepWithCalculatedDateStartCollection::getType()) {
            return new self(StepWithCalculatedDateStartCollection::fromJsonDecodedArray($stepsData));
        }

        if ($type === StepWithFixedDateStartCollection::getType()) {
            return new self(StepWithFixedDateStartCollection::fromJsonDecodedArray($stepsData));
        }

        if ($type === MixedCalculatedAndFixedStepCollection::getType()) {
            return new self(MixedCalculatedAndFixedStepCollection::fromJsonDecodedArray($stepsData));
        }

        throw new \DomainException('unhandled steps type ['.$type.']');
    }

    /**
     * creating and updating the workflow is nothing more than setting a set of steps in the workflow.
     *
     * @param StepCollectionInterface<array-key, StepInterface> $steps
     */
    public static function fromSteps(StepCollectionInterface $steps): self
    {
        return new self($steps);
    }

    /**
     * @return StepCollectionInterface<array-key, StepInterface>
     */
    public function getSteps(): StepCollectionInterface
    {
        return $this->steps;
    }

    public function hasOneStepByUid(string $uid): bool
    {
        return $this->getSteps()->hasOneStepByUid($uid);
    }

    public function getOneStepByUid(string $uid): StepInterface
    {
        return $this->getSteps()->getOneStepByUid($uid);
    }

    public function hasOpenStep(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $calculatedStepReferenceDate,
        ?\DateTimeInterface $secondaryCalculatedStepReferenceDate,
    ): bool {
        $steps = $this->getSteps();

        // if only fixed ones used, don't break, just use the given checkDate to check for one fixed open step
        if ($steps instanceof StepWithFixedDateStartCollection) {
            return $steps->hasOpenStep($checkDate);
        }

        $generalReferenceDate = $secondaryCalculatedStepReferenceDate instanceof \DateTimeInterface
            ? $secondaryCalculatedStepReferenceDate : $calculatedStepReferenceDate;

        // if only calculated ones used, don't break, just use the given calculatedStepReferenceDate(s) to check for one calculated open step
        if ($steps instanceof StepWithCalculatedDateStartCollection) {
            return $steps->hasOpenStep($checkDate, $calculatedStepReferenceDate, $generalReferenceDate);
        }

        /** @var MixedCalculatedAndFixedStepCollection $mixedSteps */
        $mixedSteps = $steps;

        // if a mix is (possibly) used, use the both (or all three) given dates
        return $mixedSteps->hasOpenStep(
            $checkDate,
            $calculatedStepReferenceDate,
            $generalReferenceDate
        );
    }

    public function getOpenStep(
        \DateTimeInterface $checkDate,
        \DateTimeInterface $referenceDateForFirstStepStart,
        ?\DateTimeInterface $generalReferenceDate,
    ): StepInterface {
        $steps = $this->getSteps();

        if ($steps instanceof StepWithFixedDateStartCollection) {
            return $steps->getOpenStep($checkDate);
        }

        $generalReferenceDate = $generalReferenceDate instanceof \DateTimeInterface
            ? $generalReferenceDate : $referenceDateForFirstStepStart;

        if ($steps instanceof StepWithCalculatedDateStartCollection) {
            return $steps->getOpenStep($checkDate, $referenceDateForFirstStepStart, $generalReferenceDate);
        }

        /** @var MixedCalculatedAndFixedStepCollection $mixedSteps */
        $mixedSteps = $steps;

        return $mixedSteps->getOpenStep(
            $checkDate,
            $referenceDateForFirstStepStart,
            $generalReferenceDate
        );
    }

    public function hasSteps(): bool
    {
        return $this->steps->hasSteps();
    }

    /**
     * it's important to know if the workflow uses only steps of the same (period) type or steps of different (period) types.
     *
     * if it uses steps of the same period type, then there can and will always be only one or no step open at any time.
     * if it uses steps of different period types, then overlap between steps can be never be fully avoided and if two
     * steps are open at the same time, then it will be exactly one step of each type. one step will use fixed start and
     * end date while the other dynamic on-the-fly start and end date, derived from the entity that is targeted
     * by the @see TemplateInterface that uses this workflow.in such a case the step with fixed dates has priority over
     * the step with calculated dates and will be returned as only open step, ignoring the other step with the calculated
     * dates (@see MixedCalculatedAndFixedStepCollection::findOneStepThatIsOpenByDate() ). so it is important in some
     * cases to know that there might be steps of different types. e;g. when checking & concluding that a step with
     * calculated dates is not yet ended but a step with fixed dates is already started!
     */
    public function hasStepsOfDifferentPeriodTypes(): bool
    {
        return $this->hasStepsOfCalculatedPeriodType() && $this->hasStepsOfFixedPeriodType();
    }

    /**
     * checks if at least one step uses calculated dates to start.
     */
    public function hasStepsOfCalculatedPeriodType(): bool
    {
        $steps = $this->getSteps();

        if ($steps instanceof StepWithFixedDateStartCollection) {
            return false;
        }

        if ($steps instanceof StepWithCalculatedDateStartCollection) {
            return $this->hasSteps();
        }

        return $steps instanceof MixedCalculatedAndFixedStepCollection && $steps->hasStepsWithCalculatedDateStart();
    }

    /**
     * checks if at least one step uses fixed dates to start.
     */
    public function hasStepsOfFixedPeriodType(): bool
    {
        $steps = $this->getSteps();

        if ($steps instanceof StepWithCalculatedDateStartCollection) {
            return false;
        }

        if ($steps instanceof StepWithFixedDateStartCollection) {
            return $this->hasSteps();
        }

        return $steps instanceof MixedCalculatedAndFixedStepCollection && $steps->hasStepsWithFixedDateStart();
    }

    /**
     * @return array{stepsType: string, steps: StepCollectionInterface<array-key, StepInterface>}
     */
    public function jsonSerialize(): array
    {
        /** @var StepCollectionInterface<array-key, StepInterface> $steps */
        $steps = $this->getSteps();

        return [
            self::KEY_STEPS_TYPE => $steps::getType(),
            self::KEY_STEPS => $steps,
        ];
    }
}
