<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Exception;

/**
 * Class WorkFlowException.
 */
class WorkFlowException extends \Exception
{
}
