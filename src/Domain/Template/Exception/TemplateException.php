<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Exception;

/**
 * Class TemplateException.
 */
class TemplateException extends \Exception
{
}
