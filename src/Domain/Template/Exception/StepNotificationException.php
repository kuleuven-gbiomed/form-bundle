<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Exception;

/**
 * Class StepNotificationException.
 */
class StepNotificationException extends \Exception
{
}
