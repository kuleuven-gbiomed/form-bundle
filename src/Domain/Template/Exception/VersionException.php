<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Exception;

/**
 * Class VersionException.
 */
class VersionException extends \Exception
{
}
