<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Exception;

/**
 * Class MissingVersionException.
 */
class MissingVersionException extends VersionException
{
}
