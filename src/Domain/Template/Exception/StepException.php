<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Exception;

/**
 * Class StepException.
 */
class StepException extends \Exception
{
}
