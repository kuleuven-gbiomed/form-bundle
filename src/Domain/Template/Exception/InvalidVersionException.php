<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Exception;

/**
 * Classs InvalidVersionException.
 */
class InvalidVersionException extends VersionException
{
}
