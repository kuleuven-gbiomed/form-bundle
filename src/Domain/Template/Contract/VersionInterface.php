<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Contract;

use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Utility\Collection\PublishedVersionCollection;

/**
 * Interface VersionInterface.
 *
 * allow to make versions of the template's elements needed to make it usable for end-users (clients)
 * a version holds all elements needed to make a template usable & workable as a form.
 *
 * in essence it holds general informative data as well as a form question list and configuration of a workflow
 * to define what user-roles have access to read and/or write answers to questions in what steps in the workflow,
 * under what conditions and from when to when. and more...
 *
 * @see     PublishedVersionCollection
 */
interface VersionInterface
{
    /** @var string */
    public const KEY_INFO = 'info';
    /** @var string */
    public const KEY_WORKFLOW = 'workFlow';
    /** @var string */
    public const KEY_FORM_LIST = 'formList';

    public function getInfo(): FormInfo;

    public function getWorkFlow(): WorkFlow;

    public function getFormList(): FormList;
}
