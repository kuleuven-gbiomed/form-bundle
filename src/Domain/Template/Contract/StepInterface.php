<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Contract;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction;
use KUL\FormBundle\Domain\Template\Utility\Collection\AbstractStepCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Interface StepInterface.
 *
 * full configuration of a step in a @see WorkFlow
 *
 * the start of a step in a workflow depends on a calculated or fixed date or if it allows itself to prematurely start
 * based on the (directly preceding) previous step in the workflow being fulfilled (i.e. all questions in previous step
 * have been submitted)
 * the end of a step in a workflow depends on a calculated or fixed date (if the next step has started prematurely,
 * then this step is closed rather than ended, because in theory, it is possible to return to this step as long as the
 * end date is not reached).
 *
 * @see     StepInterface::isOpenByDate
 * @see     StepInterface::allowsToStartPrematurelyIfPreviousStepIsFulFilled()
 *
 * developer notice: a step must be valid on itself. but as part of a workflow that is build in pieces by admins, it
 * also needs to be valid within that workflow (i.e. towards other steps) at the very least at the time that workflow
 * is published as part of a new template version! basically, this means that a balance needs to be found between
 * allowing admins to puzzle with steps without overloading them with requirements to make the workflow valid as a whole
 * on every move, delete, add of a step, while on the other hand try to maintain the workflow as a whole as valid as
 * possible to minimize the 'validation error messages' upon publish of the (workflow in the) template. notably
 * important in terms of overlap: a workflow can have only one (or none) step open at the time, but in a working
 * version of workflow, overlap in 'open' between two or more steps could be allowed until publish, which then will
 * fail and/or ask the template maker to fix the @see AbstractStepCollection::hasOverlap()
 */
interface StepInterface
{
    /** @var string */
    public const KEY_TYPE = 'type';
    /** @var string */
    public const KEY_PERIOD = 'period';
    /** @var string */
    public const KEY_INFO = 'info';
    /** @var string */
    public const KEY_ROLES_WITH_ONE_SUBMIT = 'rolesWithOneSubmit';
    /** @var string */
    public const KEY_SUBMIT_SUCCESS_USER_INTERACTION = 'submitSuccessUserInteraction';
    /** @var string */
    public const KEY_HIDE_READ_ONLY_NODES_ON_LANDING = 'hideReadOnlyNodesOnLanding';
    /** @var string */
    public const KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED = 'allowsStartIfPreviousStepIsFulfilled';
    /** @var string */
    public const KEY_CUSTOM_OPTIONS = 'custom_options';

    /**
     * helps to serialize & de-serialize to correct type of step via unique string.
     */
    public static function getType(): string;

    public function getUid(): string;

    public function getInfo(): LocalizedInfoWithRequiredLabel;

    /**
     * may this step start earlier than it's (fixed or calculated) start date if all submittable questions (i.e. the
     * ones with write access for at least one role) for the direct preceding step (if any) are submitted
     * (regardless of the answers and ignoring temporary saved answers).
     *
     * this allows to ignore the start date of this step and the end date of the previous step. it helps and is widely
     * used to set broad periods for both this and the preceding step to allow users lots of time while also triggering
     * this step to start (and 'finish' preceding step) if all questions that needed an answer are answered and there
     * is no point in waiting any longer for this step to start. it helps a lot to speed up the flow for fast submitting
     * participating users whilst still giving slower user time to do their part.
     *
     * note: if questions in preceding form are divided over multiple roles with some requiring to be filled in only by
     * role X and others only by role Y, and (a user with) role X has submitted answers for his/her questions , then
     * this start option will not be triggered until also (a user with) role Y has submitted answers for his/her questions
     *
     * note: reality has proven that the start of a step is more important & significant for coding & UX than its end (
     * although end as deadline is equally important & final). hence setting this alternative start option on a step,
     * rather than adding a alternative finish option on a step, is more meaningful. especially since this makes it
     * easier to update the status of a submitted form for a template (status = step open on last submit, being this
     * step or the next one if that submit has proven fulfilled)
     *
     * note: in principle, the first step in a valid workflow should not have this option. but if it does is marked as
     *       such, it does not do any harm as there never will be a preceding step and no sensible code will check
     *       for preceding steps if it is the first step.
     *
     * note: experience in real life use cases learns that the standard is setting this to true, but that is not to
     * be considered as the default, it just seems advisory because that allows quicker running through the workflow
     * steps. but the template workflow maker (admin) needs to decide if a step needs to start as quickly as the
     * previous is fullfilled (= all questions with at least one ROLE having write access are submitted) while
     * sometimes it might seems better to let the flow run it course.
     */
    public function allowsToStartPrematurelyIfPreviousStepIsFulFilled(): bool;

    public function getCustomOptions(): array;

    /**
     * when another step following thisStep (@see AbstractStepCollection::hasOneStepThatDirectlyFollowsStep(thisStep)
     * is configured to start prematurely (@see allowsToStartPrematurelyIfPreviousStepIsFulFilled )
     * then upon each successful submit within thisStep, thisStep will be check if it is fulfilled and if so,
     * the start date of the nextStep will be ignored and open up anyway.
     *
     * fulfilment is nothing more that checking if all question that grant write access to at least one role in thisStep
     * are submitted (not temporary saved!), regardless of what answer might be given (so an optional question
     * must be submitted as well, with at least an empty answer, so that we know someone at looked at (i.e. considered
     * (not) answering) that question. it also does not matter if that question was submitted in thisStep or
     * already submitted before in a previousStep, because the only requirement is that is is submitted at this point
     * in the workflow. so it is quite possible that a question was already submitted in a previous step and the
     * submitting user decided to not even update it's answer, even though he/she has write access in thisStep.
     * also see @see InputNodeCollection::getWritableInStep()
     *
     * when admins build templates with questions and workflow steps, it is common sense that they grant themselves
     * write access to a question as soon as (= from the first step in which ) they grant write access to another
     * role (that is not an admin role. that way, admins will always have the possibility to change answers during
     * the course of the workflow, in case they need to if they don't grant any write access in later steps.
     *
     * but in some cases this could have unwanted results:
     *
     * suppose a simple template has two questions. suppose that template's workflow has 3 steps. steps 2 and 3 are
     * configure to start prematurely, based on their preceding step being fulfilled.
     * suppose we are allowing 3 roles to participate in some manner: NON_ADMIN_ROLE, SECOND_NON_ADMIN_ROLE and ADMIN_ROLE
     * the most common way to grant access to the two questions is a follows:
     *  - question 1:
     *      in step 1: write access for NON_ADMIN_ROLE. but also write access for the ADMIN_ROLE
     *                 maybe read access for SECOND_NON_ADMIN_ROLE (but for example purposes no write access for that role)
     *      in step 2: only read access for NON_ADMIN_ROLE. but still write access for the ADMIN_ROLE
     *                 maybe read access for SECOND_NON_ADMIN_ROLE (but for example purposes no write access for that role)
     *      in step 3: only read access for NON_ADMIN_ROLE. but still write access for the ADMIN_ROLE
     *                 maybe read access for SECOND_NON_ADMIN_ROLE (but for example purposes no write access for that role)
     *  - question 2:
     *      in step 1: no access for any role. (this is a very very simple example with 1 question with write access
     *                 for non-admins per step, simulating some logical questionnaire)
     *      in step 2: write access for SECOND_NON_ADMIN_ROLE. but also write access for the ADMIN_ROLE
     *                 maybe read access for the other NON_ADMIN_ROLE (but for example purposes no write access for that role)
     *      in step 3: only read access for SECOND_NON_ADMIN_ROLE. but still write access for the ADMIN_ROLE
     *                 maybe read access for NON_ADMIN_ROLE (but for example purposes no write access for that role)
     *
     * now, suppose the from opens up in first step. and no-one submits anything in that first step: neither that
     * NON_ADMIN_ROLE and neither the ADMIN_ROLE. the first step runs it course until its deadline date, after which
     * it is indirectly closed (for any roles) and the second step opens up, based on that second steps start date (for
     * example purposes, we assume no 'dead' period with no access for anyone between steps). so the first step
     * has never been fulfilled and the second step could not start prematurely.
     * in the second step. the SECOND_NON_ADMIN_ROLE does submit successfully (i.e. submits an answer for question 2,
     * the question he can even see and has write access to).
     *
     * the third step is configured to start prematurely if its preceding (i.e. the second) step is fulfilled.
     * now, the second step is NOT fulfilled because there are two questions in it that grant write access to at least
     * one role. question 2 grants access to SECOND_NON_ADMIN_ROLE and that question IS submitted. but question 1
     * is not (never) submitted but it grants write access for the ADMIN_ROLE. the system does it jobs and determines
     * that not all questions are submitted, so it does not trigger step 3 to start.
     *
     * now in 99% of the cases, this is probably what you want: the/a (user with the) NON_ADMIN_ROLE has failed
     * to do his job in step 1. the whole reason for the workflow is that it follows some logical order of
     * questions, possibly (actually most surely) with questions reating to each other and building further on
     * answers of other questions that were answered in previous steps in the workflow. in other words: the fact
     * that question 1 remained un-answered should probably be of importance to admins and they might have
     * to take actions (e.g. like answering the question themselves in step 2 after some user with SECOND_NON_ADMIN_ROLE
     * that answered question 2 in step 2 has notified admin that the from did not trigger the next (third) step
     * and made him pissed because he wants to move on. or something like that)
     *
     * so this option allows you to tell the template that when checking if thisStep is fulfilled (and nextStep
     * wants to start prematurely), it has to ignore the questions to which ONLY the NON_ADMIN_ROLE has write access.
     * allowing to start the next step anyway if all questions with write access for at least one non-admin role are
     * submitted. note that if both an admin-role and a non-admin-role have write access, that question will not be
     * skipped in the check, since there is a non-admin role that can submit it.
     */
    public function skipAdminRolesForFulFilmentCheck(): bool;

    public function withPreviousStepFulFilledStartOption(): static;

    public function withoutPreviousStepFulFilledStartOption(): static;

    /**
     * check if given role can only submit one time in this step. if a unique user - having this role - has
     * already submitted once under this role in this step, he/she no longer has any write access to any question
     * in this step. instead the questions with write access are reduced to questions with read-only access as long
     * as the form is in this step.
     *
     * note: refers to a successfully processed submit in step; a submit can return with validation errors, which
     * doesn't count (duh). only refers to submitting, the amount of temporary saving is never limited as such.
     */
    public function limitsEachUniqueUserToOnlyOneSubmitUnderRole(Role $role): bool;

    /**
     * get the translated label for the given locale or return fallback if none exist for given locale.
     *
     * @throws LocalizedStringException if no translation for locale and no fallback found
     */
    public function getLabel(string $locale): string;

    /**
     * should this step notify the participating users with given role - which gives that user (write or read) access
     * in this step - that this step has started (or will start soon).
     *
     * @throws \Exception
     */
    public function shouldNotifyRoleOfStart(Role $role): bool;

    /**
     * check if there are roles that are configured to be notified that this step has started (or will start soon).
     *
     * @throws \Exception
     */
    public function shouldNotifyAtLeastOneRoleOfStart(): bool;

    /**
     * get all roles that are configured to be notified that this step has started (or will start soon).
     *
     * @throws \Exception
     */
    public function getRolesThatShouldBeNotifiedOfStart(): RoleCollection;

    /**
     * should this step remind the participating users with given role - which gives that user (write or read) access
     * in this step - that the step was started and has been open for a while if user has not yet done his/her part.
     *
     * NOTE: up to your app to decide if you notify the user even if he/she has done his/her part.
     *
     * @throws \Exception
     */
    public function shouldRemindRoleOfOngoing(Role $role): bool;

    /**
     * check if there are roles that are configured to be notified that this step was started and has been open for a while.
     *
     * @throws \Exception
     */
    public function shouldRemindAtLeastOneRoleOfOngoing(): bool;

    /**
     * get all roles that are configured to be notified that this step was started and has been open for a while.
     *
     * @throws \Exception
     */
    public function getRolesThatShouldBeRemindedOfOngoing(): RoleCollection;

    /**
     * sometimes it can be useful to forcefully move user away from the form page after user has successfully submitted,
     * e.g. to make sure the user realizes that his/her writing part is over in this step.
     */
    public function shouldRedirectAwayOnSubmitSuccess(): bool;

    /**
     * forms can have loads of nodes (questions & categories). Often a user-role sees most nodes with read access only
     * and only a few nodes with write access. to avoid long pages filled with read-only answer, views have a toggle
     * to hide/show the read-only nodes in a step. this property is used to set this toggle to hide upon page landing.
     */
    public function shouldHideReadOnlyNodesOnLanding(): bool;

    public function getRolesLimitingUserToOneSubmitPerRole(): RoleCollection;

    public function getSubmitSuccessUserInteraction(): StepSubmitSuccessUserInteraction;

    /**
     * check if there is successfully submitted message available to flash/show - regardless of roles - for the given
     * locale or fallback locale.
     *
     * @throws LocalizedStringException if a locale translation is invalid
     */
    public function hasGeneralSubmitSuccessMessage(string $locale): bool;

    /**
     * get the successfully submitted message to flash/show to the submitting user - regardless of roles - for the
     * given locale or fallback locale.
     *
     * @throws LocalizedStringException if no translation for locale and no fallback found
     */
    public function getGeneralSubmitSuccessMessage(string $locale): string;

    /**
     * check if there is successfully submitted message available to flash/show for given role, for the given locale
     * or fallback locale.
     *
     * @throws LocalizedStringException if a locale translation is invalid
     */
    public function hasRoleSubmitSuccessMessage(Role $role, string $locale): bool;

    /**
     * get the successfully submitted message to flash/show to the submitting user that has submitted under that role,
     * for the given locale or fallback locale.
     *
     * @throws \BadMethodCallException  if no message for given role found
     * @throws LocalizedStringException if no translation for locale and no fallback found
     */
    public function getRoleSubmitSuccessMessage(Role $role, string $locale): string;

    public function getPeriod(): StepPeriodInterface;

    /**
     * this helper method is added here as a public method instead of in the @see AbstractStep class because of PHP
     * strict standard that does not allow to declare (protected) static methods as abstract.
     */
    public static function getPeriodFromJsonDecodedArray(array $data): StepPeriodInterface;
}
