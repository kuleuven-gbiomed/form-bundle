<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Contract;

use KUL\FormBundle\Client\Slugifier\RoleSlugifier;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\Template\Exception\TemplateException;
use KUL\FormBundle\Domain\Template\Utility\Collection\PublishedVersionCollection;
use KUL\FormBundle\Domain\Template\Version\PublishedVersion;
use KUL\FormBundle\Domain\Template\Version\Validator\WorkingVersionValidator;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * A template is the specification that will be used to build a Symfony form with.
 *
 * Please read carefully, this is THE root domain object of this domain repo!
 * (also important: the secondary root object is @see StoredTemplateTargetInterface that acts as link between Template,
 * targeted entities/objects and 'end-users' (end-users are referred to in code as 'clients'), in a sense that it stores
 * submitted/tempSaved answers for questions rendered based on template-workflow-step-role-read-write-access and
 * given by a client for a target entity)
 *
 * a template is responsible for building a submittable/tempSave-able form and/or consulting-answers view
 * consisting of a combination of read-only and writable - possibly inter-depending - questions ordered in (nested)
 * categories and within a workflow that grants certain roles in certain steps of that workflow either read, write,
 * both or no access for clients that have access to the object/entity that this template is designed to target as
 * subject for the questionnaire. and with a heap of (overridable) limitations on access and added functionality.
 *
 * the contract for the elements configuration that is to be used to build a usable form for a target subject/object
 * (@see StoredTemplateTargetInterface ). a template contains all necessary elements needed to allow clients
 * to participate with one or more participatable user-roles for viewing (read) and/or storing (write; submitting and
 * temporary saving) answers to questions within a dynamic workflow via a custom build form & form result view for a
 * subject/object that the templates targets (ref. 'templateTarget'). it contains:
 *      - the questions in the form and their intertwined dependencies
 *      - the workflow in which the form is run through by clients and that holds steps that are opened & closed by
 *        calculated dates based on the templateTarget and/or opened & closed by one or more clients fulfilling
 *        (submitting) the step's questions & requirements
 *      - the rights for user-roles to each question that allow clients - related to a templateTarget - with those
 *        role(s) to read and/or write answers for those questions in the step(s) in the workflow that the question
 *        is configured to allow read/write (or no) access.
 *      - a bunch of custom information about the form that can help clients to understand what that particular
 *        form is about and what purpose it serves, etc..
 *
 * a template has one working version containing working versions of all elements (questions, workflow, info,
 * label, etc...). A template has zero to infinity published versions. a working version is accessible for admins only
 * and is used as a base to make new published versions. published versions are numbered and only one published version
 * can be the current version (i.e. version with highest version number). the current version is the version that is
 * used by clients. all previously published versions are kept and never deleted or altered.
 *
 * a template being build or updated by an admin, is always done by building on the single working version. when an admin
 * decides to make the template available, he/she publishes that working version in to a full grown version (nothing
 * more than making an exact copy of it). once a working version is published, it remains available & dormant. when an
 * admin wants to make a change to the current published version, he/she essentially always departs from the working
 * version, which at that point is exactly the same as the current version. as soon as an admin makes a change - however
 * small it may be - that change is applied to the working version which then becomes dissimilar to the current version.
 * in meanwhile, clients keep working on the current version until the admin decides to publish a new version based on
 * the altered working version. at that point (i.e. on new publish) the current version is being demoted to a previous
 * version, kept in storage and a new current version is published based on the altered working version (i.e. new copy).
 *
 * a template's working version is build in pieces, meaning that the questions, info, steps in the workflow, individual
 * role rights on questions, etc.. are added & changed one by one, often going back and forth between adding/changing/
 * deleting steps in the workflow, adding/changing/deleting questions, rights, etc. admins must be allowed to change,
 * add, delete parts of the working version without requiring them to make parts and the whole template fully valid
 * (for publish) right away. some parts (like the dependencies for a 'automatic calculable global score' question) can
 * be asserted to be valid right away. but others can not or not completely be asserted to be valid right awy. e.g.
 * an admin can make a workflow with some steps without having any questions yet, or deleting a step that makes a
 * question unaccessible because it was only accessible for read/write in that step, etc. this de facto means that -
 * unlike a published version - the working version does NOT need to be a valid version containing all required elements.
 * But when a working version is published by an admin, the version undergoes a thorough validation check and only when
 * valid will a new version be published!
 *
 * when the working version is published, the template is immediately activated. it then is possible for an admin to
 * deactivate the template (for whatever reason) which then makes all access for clients to forms related to this
 * template impossible. re-activating a template is then only possible if the template has a current version to activate.
 * publishing a new version when previous versions exist and the template is deactivated will also result in a re-
 * activation alongside the publication of a new version. this is especially handy if admin needs to make crucial
 * changes to a template that is already in use and he/she wants to halt all further activity on forms related to
 * the template until he/she finishes the alterations and publishes a new version.
 *
 * developer notice: in theory the current version (and any other previous published version) is NEVER touched with
 * intention to change it! as soon as a working version was validated and published in to a new current version, that
 * current version is assumed to remain as it was on the moment it was published. even a small spelling error should
 * be 'fixed' by fixing it in the working version and then lead to publication of a new version, effectively replacing
 * the current version by the one based on a copy from the working version with the spelling fix. Why is this important?
 * Because doing a full validation check on the current version, each time it is called by an end-user (client), would be too much
 * of a performance issue, especially if everything in the version is to be checked thoroughly! hence, the template
 * does not AND should not have any public methods that can touch the current version with the intention to change it.
 * only retrieval of the current version is public! changing a (current) version in DB is completely at own risk
 * (but not impossible or always fatal like in the case of a spelling error)!
 * however in practice, it could be considered,
 * case by case that some minor non-breaking  changes - like the spelling fix - could be done directly on the current
 * version, bypassing the  publish of a new version, but that should be avoided as it would mean the working version
 * would no longer be 'ahead' of the current version. but all warnings aside, the domain decision is made that it will
 * be allowed for admins to make direct changes to certain elements of the current version without having to publish
 * a new current version (with validation check on the altered current version). but only limited, non logic stuff like
 * the label, info texts, category,etc. can be changed directly on current version and never stuff that is used for
 * logic, question dependencies, access-rights, etc. which always has to go via publish.
 * most elements of a template's (current/working) version are stored (serialized) in separate table columns instead of
 * stuffing everything as one bulky serialized object in a single DB table column. because some elements have to be
 * queried directly without serializing like the @see CategoryInterface . while storing elements seperately helps for performance
 * linked to JSON (de)serialization: not always are all elements needed and separation allows to only deserialize what
 * is needed. when a new current version is published, the old current version is serialized as a bulky serialized object
 * containing all elements (including serialized Category) and stored in a separate designated column, because the need
 * to retrieve & deserialize a previous version is rare and if it is needed (e.g. return to previous version), the
 * deserialization performance can be neglected. this is also why it would be nice if current version could be retrieved
 * from a collection of published versions, but that is not a good idea here, as performance is to be kept in mind.
 * hence the current version is stored in separate (serialized) elements in DB table.
 *
 * TODO the logic for making direct SMALL and NON-CRUCIAL changes on current version is not yet implemented:
 * in the logic (method, VO) to be made for this, we can use the @see deactivate()
 * and certainly the @see reactivateCurrentVersion() to handle this. this making direct SMALL and NON-CRUCIAL changes
 * on current version is needed if there are small NON_CRUCIAL (like spelling) mistakes that an admin wants to correct
 * in the current version, without having to do make the changes on the working version which then require the admin
 * to publish a new current version. other such small changes directly on current version could be: admin wants to
 * add/update optional descriptions on a step or question, update a label of a step or question or do other NON_CRUCIAL
 * stuff (e.g. move a question to another position in the tree structure without changing anything of its access).
 * it was always intended (and should remain the main way to go) that ANY change on a version should follows the
 * provided flow of updating the working version @see updateWorkingVersion() and then doing a publish which
 * will validate the working version and copy it to a new current version. because most of the changes on the elements in
 * a version (workflow, formList) might have impact on access.
 * then why allow admins to make small changes that DO NOT AFFECT crucial stuff like access, without going
 * through working version and directly execute them on current version? because a publish of a current version
 * makes and stores a new @see TemplateInterface::getCurrentVersionNumber() to track versions and because the steps
 * in a @see WorkFlow can be configured to limit all unique users to submit only once per unique role they have.
 * but at time of writing this @see StepInterface::limitsEachUniqueUserToOnlyOneSubmitUnderRole()
 * submit limit is also VERSION DEPENDENT: if a user-role has submitted in a certain step in one published version,
 * he can no longer submit in that step IN THAT VERSION. but he will get a second submit chance if a new version is
 * published (this is domain decision (TODO change this?))
 */
interface TemplateInterface
{
    /** @var int */
    public const VERSION_INITIAL_NUMBER = 1;

    public function getId(): string;

    /**
     * distinguishes templates for usage within application.
     *
     * in you app, every targeting type will have it's own set of routes and controllers (actions) that handle the
     * preliminary access on the targeted entity/object that the template is called on, before passing it forward in this
     * bundle. the string returned by this method should be meaningful enough. it should mention the entity/object class/code
     * the templates is targeting as well as the intention or purpose for which the template is to be used on those targets.
     * this helps with querying correct template as well as identifying controllers used by the template
     *
     * e.g. suppose a template is meant & configured to build forms for evaluations of students in an internship:
     * suppose the targeted entity/object is something like 'App/Entity/InternshipStudent', then the string returned
     * by the method could be something like 'internship-student-evaluations'.
     * a specific set of controller actions & routes will then handle requests based on specific internshipStudent and
     * template(s) of this targeting type. (in this case your app probably will have different internships and you
     * probably want to evaluate students in a particular internship, so you will most likely have
     * an 'App/Entity/Internship' and you will have to link the template to internships somewhere in your app,
     * e.g. in an intermediate relation table in DB).
     * this template with type 'internship-student-evaluations' could handle different sorts of evaluations, like
     * mid-term evaluations, final evaluations, etc. if this is just a matter of names the use the @see CategoryInterface.
     * one template will then have @see CategoryInterface 'Mid Term Evaluation', while another will have category
     * 'Final Evaluation' but both have the same targeting type 'internship-student-evaluations' and apart from category
     * and template id, nothing changes. So if everything is the same and the flow & preliminary access to
     * internshipStudent stays exactly the same for any of these targetingTypes, regardless of name/category
     * then the same  set of controllers can handle incoming requests for student evaluations in an internship.
     * in your app, you could also have evaluation for students, but not related to specific internships.
     * the targeting type could then be something like 'student-evaluations'. And in that case the template will not
     * be linked to an internship in your app and a different set of controllers & routes would/could/should be used
     *
     * note that this targeting type is important for your app only. for the bundle, this is just a string that is
     * - at most - used for querying if.
     */
    public function getTargetingType(): string;

    /**
     * limiting hierarchy of roles used to determine which roles might have the opportunity to participate in the template.
     *
     * ANY ROLE YOU WANT TO CONSIDER TO ALLOW TO PARTICIPATE IN THIS TEMPLATE AT ANY TIME, MUST BE ADDED HERE. THAT
     * ALSO INCLUDES ANY ADMIN ROLE(S). THERE IS NO EXCEPTION HERE: AN ADMIN ROLE IS TO THIS TEMPLATE NO DIFFERENT
     * FROM ANY OTHER ROLE! EVERYTHING IN TEMPLATE (ACCESS) IS ROLE BASED AND IT DOES NOT KNOW WHAT AN ADMIN USER IS!
     * this set of roles is also used in the client (end-user front) side to check if a role even could access a
     * template. that means that any role that tries to access the template will - in all cases - first have to
     * pass this set of roles before any real access (to questions) will even be checked). in an admin-side, admins
     * may get the chance (developer's choice) to set/select this set of roles, instead of it being hardcoded, with
     * selection based on predefined set of roles that your app knows about and is required to be configured in config
     * of this bundle. and if admins will be able to select/set, then it is likely and very much convenient that the
     * developer making this admin-side will exclude the admin role(s) from the dropdown presented to the admin.
     * and/or maybe even provide a secondary dropdown with only admin roles that the admin can choose from to consider
     * for participation. simply because the developer knows that the admin role will always be hae to be considered
     * for participation and/or that the developer will handle access for admin role(s) automatically when building
     * the role access to questions. either way, the admin roles MUST be in this set of roles also if you want
     * that admin-roles to even be considered for access! because to the template and all it's access related matters,
     * an admin role is the same as an other role; if you fail to add it here, it will/should not even be available when
     * setting the access to questions and even if you would add this admin role(s) to the access of questions (by
     * not building your access matrix based on this set of roles), then still not a single user with that admin role
     * will ever get any access, if that role is not in this set!
     *
     * reduces the amount of roles to check on and build access-rights for. helps to quickly check if a user has roles
     * that COULD allow him/her to participate in the Template before doing any further access checks.
     * having a role that COULD participate does not mean there will actually be questions with any access for that role
     * this is mainly important to avoid having to do the (performance heavy) de-serializing & deeper checks, if the
     * result of that will be that no accessible questions will be returned anyway. because this set of roles is used
     * in admin side building of template to present a limited matrix of roles to set question access for.
     * the hierarchy is fully optional and can be used e.g. to determine what to show first in an overview page for
     * which of the two roles that a user has. or which set of role-related questions have to be shown if a user has
     * two roles that allow him/her to participate and for which both roles have access.
     * please note that if a role is not found in this set of roles, no further access will ever be checked
     * usually, the possibility to set these roles on admin side when building/changing template could/should not be given
     * to admins, instead the set of roles should be hardcoded in the app, per @see getTargetingType and based on the
     * entities that the implementing template targets, and then passed as such to the template upon first construct to
     * avoid having too many roles that need have rights set and checked every time template is used.
     * this is not mandatory and this set of roles could just be all roles known to the app, but it helps if
     * you app has many roles and you know that only a few of those roles could ever participate in certain targetingType
     * of templates
     * since only these roles are the ones that could uberhaupt have access to questions, any other set of roles
     * in this template (e.g. roles allowed to export, etc.) must be within this roles that could participate.
     * basically this is a fast way to check in overviews if a deserialize and further deeper checking for access is needed
     * based on the user having at least one of this roles. it also helps in admin side to build the access rights for
     * each role with (custom) default rights for each question, when the admin has not yet supplied any question
     * specific rights. basically it helps to limit & prefil the questions-steps-access-role-rights matrix for admins
     * in which they set the read and/or write access rights for each role on each question in each step.
     * NOTE:to check if user will have any real access at some point in time
     * use @see TemplateInterface::getRolesThatParticipateInCurrentVersion() , which is heavier on performance
     * and checks first if role has participatable access and then deeper if at least read access is given
     * to at least one question in at least one (past, current or future) workflow step
     *
     * usage in routes: this hierarchy is used for users with multiple roles to switch between access: a user
     * can only access a form with one role at the time. if a user lands on a form page, and that user has multiple
     * roles that all grant some access, then these roles are hold against this hierarchy and the highest ranked
     * will be used as primary role and the form-view will rendered for that primary role access. on the page, the
     * user will then get a switch-roles dropdown. user can than switch to another role and see the same form-page
     * but with different access (i.e. different questions, different read/ write on questions,etc.). for this reason
     * the form routes have an optional 'role' parameter in the route. if that role parameter is not given or it is
     * given but it contains an invalid role or a role that does not grant user access (some users pass urls to
     * other user via mail, facebook, etc.), then the first role in hierarchy that user has and that grants user
     * access is used as primary access. any more roles are added to the switcher.
     *
     * also @see getRolesThatShouldBeUsedForBuildingAutomaticWriteAccess
     */
    public function getHierarchicalRolesThatCouldParticipate(): RoleCollection;

    public function getParticipatableRoleSlugifier(): RoleSlugifier;

    /**
     * this method is for developer use! it does not really 'do' something, let alone set access on questions, not even
     * at run time. all it does is 'notify' the developer that he/she should consider setting write access on questions
     * for the roles in this set AUTOMATICALLY; i.e. take the burden away from the admin making the template
     * hence the weird naming!
     *
     * in an admin-side, in the 'matrix' part where access rights are set on questions, based on roles,
     * the admin building the template will & should almost always want to grant write access for themselves
     * on each question as soon (from the first step) as they grant write access for another non-admin role on
     * that question. making it possible for admins to update (or give) answers to questions as soon as one of the
     * other non-admin user (roles) can. e.g. very useful to correct answers on questions that were submitted by
     * some role that had write access, but has no write access anymore.
     * this method is just something that could be used in an admin side to hide the checkboxes/functionality
     * that are used to set access to questions: the roles in this set could not be shown in the matrix that
     * sets this access on questions (based on roles and workflow). instead of showing that functionality
     * for these roles, the developer can take over and set write access on any question in the background.
     *
     * usually, this will only contain admin roles. a
     *
     * NOTE: any role in this set must also be a role in @see getHierarchicalRolesThatCouldParticipate
     * if a role in this set is NOT found in that other set, it is silmenty filtered out (since this is
     * just a helping method for developers and should NEVER break anything)
     *
     * NOTE: this method in no way sets access or grants access at run time to these roles. never!
     *
     * NOTE: please do note that setting automatic write access to questions for these roles is NOT to be done blindly!
     * if this contains admin roles, then you do not want to set write access on a question before any other non-admin
     * role does! if you (accidentally) add write access for an admin role in a step in the workflow, while
     * any other non-admin role only gets write access in a later, following step, then that will not break stuff,
     * but it will make those questions required to be filled in by those admins (since they are the only ones you
     * gave write access to in that step) and can have effect on the workflow to (prematurely) pass to the next step.
     * in other words, and again, an admin role is no different from any other role.
     * if the admin making a template intentionally wants to give write access to a question in a step, before
     * intentionally setting write access to other roles in later step(s), then that admin's  admin role should
     * probably not be in this set but in @see getRolesThatShouldBeUsedForBuildingAutomaticWriteAccess
     * because this set here should be used to only set access as soon as any other role has and that without the admin
     * making the template being aware that that is done!
     *
     * TODO START:
     * I think it is possible, yet probably difficult and somewhat risky, to automate setting this write access
     * via a method in this template, that takes the working version and this set of roles. and then loops
     * over workflow in working version and updates the flowPermissions for each question with write access for
     * each of this roles, in the first step found that grants another role write access. needs a lot of checks on
     * working version being far enough valid and will need to decide what to do with global score (in SCONE,
     * sometimes no other role has ever write access, but admins get it the step right after the global score is
     * (supposed to) be calculated. but it could be handy to allow the developer to call this method on this template
     * after and/or before every matrix change?
     * there is also the possibility to use that new method upon each @see updateWorkingVersion()
     * to set it completely automated. can NOT do this on @see publishNewCurrentVersion()
     * because that method must run on workingVersion first otherwise the publish might fail and it also means
     * that the @see canPublishNewCurrentVersion() would already apply it on working version. and anyway, this
     * is not something to do on publish: the working version must be already been made valid beforehand
     * TODO END
     */
    public function getRolesForAutomaticQuestionWriteAccessBuildingInAdminSide(): RoleCollection;

    /**
     * check whether given role COULD participate.
     */
    public function couldAllowRoleToParticipate(Role $role): bool;

    /**
     * check whether at least one of given roles COULD participate.
     */
    public function couldAllowAtLeastOneOfRolesToParticipate(RoleCollection $roles): bool;

    /**
     * check whether given client (person with roles that have access to a @see TemplateTargetableInterface
     * targeted by this template) has at least one role with access to target that COULD allow client to participate.
     */
    public function couldAllowClientWithTargetAccessToParticipate(
        ClientWithTargetAccessInterface $clientWithTargetAccess,
    ): bool;

    /**
     * filter roles for given client (person with roles that have access to a @see TemplateTargetableInterface
     * targeted by this template) that could COULD allow client to participate with those roles.
     */
    public function getRolesThatCouldAllowClientWithTargetAccessToParticipate(
        ClientWithTargetAccessInterface $clientWithTargetAccess,
    ): RoleCollection;

    /**
     * takes the roles that COULD participate and filters out the roles that (will) have PARTICIPATION access
     * (i.e read and/or write access) at some point in the workflow in the current published version (if any)
     * (roles with at least read access on at least one @see InputNode::isReachableInTree() question in at least one workflow step).
     *
     * NOTE: this is more heavy on performance than @see TemplateInterface::getHierarchicalRolesThatCouldParticipate()
     * since it will have to deserialize and build the current version and go through the @see FormList
     * and @see WorkFlow of the current version.
     * NOTE: this only checks if role has at least read access on at least one question @see InputNode::isReachableInTree()
     * in at least one step of the current workflow.
     */
    public function getRolesThatParticipateInCurrentVersion(): RoleCollection;

    /**
     * check whether given role will have some access at some point in the workFlow in the current published version (if any)
     * (role with at least read access on at least one @see InputNode::isReachableInTree() question in at least one workflow step).
     */
    public function allowsRoleToParticipateInCurrentVersion(Role $role): bool;

    public function allowsRoleToParticipateInCurrentVersionStep(Role $role, StepInterface $step): bool;

    /**
     * check whether at least one of given roles will have some access at some point in the workFlow in the current published version (if any)
     * (role with at least read access on at least one @see InputNode::isReachableInTree() question in at least one workflow step).
     *
     * @throws MissingVersionException
     */
    public function allowsAtLeastOneOfRolesToParticipateInCurrentVersion(RoleCollection $roles): bool;

    /**
     * check whether given client (person with roles that have access to a @see TemplateTargetableInterface
     * targeted by this template) has at least one role (with access to the target) that WILL allow client to
     * participate at some point in the workFlow in the current published version (if any).
     */
    public function allowsClientWithTargetAccessToParticipateInCurrentVersion(
        ClientWithTargetAccessInterface $clientWithTargetAccess,
    ): bool;

    /**
     * for developers or admins impersonating end-user (client), it is handy to disable auto temporary saving. e.g.
     * when developer/ impersonating-admin want to consult a templateTarget form because of some issue, they don't
     * want to have to worry about an autoSave to be triggered every x minutes, changing answers because of some
     * client side coed testing or a tempSave record poping up in submit/tempSave logs or as warning on an answer.
     *
     * in practice, the possibility to set these roles on admin side when building/changing template will usually
     * not even be given to admins, instead it will usually be hardcoded on this interfaces's implementation or via
     * some other way passed in on the template's construction. but if the app owner desires it, it could be an option
     * for admins to add/remove roles if the developer provides this functionality on the admin side for specific templates
     *
     * roles are/should be filtered silently on being also a role  in a @see TemplateInterface::getHierarchicalRolesThatCouldParticipate():
     * if role in this set is not a role that could participate, then that is not important enough to throw error.
     * e.g. if you would have a role that is not a role that could participate, then there nothing to disable anyway
     * or even get you access to the template, hence the silent filtering (here and on set/update)
     */
    public function getRolesWithDisabledAutoTemporarySaving(): RoleCollection;

    public function shouldDisableAutoTemporarySavingForRole(Role $role): bool;

    /**
     * whether or not the templates allows clients with these role(s) to export submitted answers (on questions
     * that the role has at least read access to at current point in the templateTarget's workflow).
     *
     * roles are/should be filtered silently on being also a role  in a @see TemplateInterface::getHierarchicalRolesThatCouldParticipate():
     * if role in this set is not a role that could participate, then that is not important enough to throw error.
     * e.g. if you would have a role that is not a role that could participate, then there nothing to export anyway
     * or even get you access to the template, hence the silent filtering (here and on set/update)
     */
    public function getRolesAllowedToExport(): RoleCollection;

    /**
     * check whether given role is allowed to export submitted answers.
     */
    public function allowsRoleToExport(Role $role): bool;

    /**
     * gets the version on which admin is working. this is the version that is used to add/alter/delete stuff to
     * regarding workflow, questions, etc. this version exists apart from published versions and either is ahead of
     * current version or is same as current version if no changes were made to working version since last publish.
     *
     * important: the working version doesn't need to be valid until it is used to publish as a new current version!
     *
     * @throws MissingVersionException if there is no working version
     */
    public function getWorkingVersion(): WorkingVersion;

    /**
     * stores changes to the working version's elements.
     */
    public function updateWorkingVersion(WorkingVersion $workingVersion): void;

    /**
     * checks if there is a current published version.
     */
    public function hasCurrentVersion(): bool;

    /**
     * quick check to see if template is fully published and activated so that clients can use it.
     *
     * NOTE: template that is active will be published as well (have a current published version). but not the
     * necessary other way around: it could have a current published version but it could be (temporary) de-activaed
     * by an admin fro whatever reason, blocking all usage for clients (including admins who are clients as well)
     * and only be accessible in the admin-side
     */
    public function isValidForClientUse(): bool;

    /**
     * get the current published version, which contains all template
     * elements currently in use (current workFlow, current info, current questions, etc.).
     *
     * IMPORTANT: on publish of a new current version via @see TemplateInterface::publishNewCurrentVersion(), the
     * working version used to make the new current version, is validated & asserted on several publish condition
     * using the @see WorkingVersionValidator . only if valid is that working version set as the new current version on the
     * template. this validation is heavy on performance, so on retrieval of the current version via this method, no
     * new validation is done to avoid the associated performance leaks! hence the current
     * version is assumed valid if it exists and retrieved here and - unless tampered with in the DB - can only be
     * altered in any way by using the dedicated @see TemplateInterface::publishNewCurrentVersion()
     *
     * @throws MissingVersionException if there is no current published version
     */
    public function getCurrentVersion(): PublishedVersion;

    /**
     * get the version number referring to the current published version in use, if there is a current version in use.
     *
     * note: this number is altered (incremented) upon each publish of a new current version
     * NOTE: this number is taken from the @see TemplateInterface::getCurrentVersion() and stored redundantly
     * on @see TemplateInterface::publishNewCurrentVersion() of a new version for more performance efficient checks
     * and quick querying purposes
     *
     * @throws \BadMethodCallException if there is no number found
     * @throws MissingVersionException if there is no current version
     */
    public function getCurrentVersionNumber(): int;

    /**
     * check if a new version can be published based on working version.
     *
     * @see WorkingVersionValidator for publish condition
     */
    public function canPublishNewCurrentVersion(): bool;

    /**
     * check if the working version can be used to republish as existing current published version.
     */
    public function canRePublishAsExistingCurrentVersion(): bool;

    /**
     * takes workingVersion, validates it for publish, makes an exact copy of the template's working version elements,
     * adds those copied elements as new current version's elements, increases incremented versionNumber by one, backs-up
     * the previous current version (if any), activates the template and as such gives clients a new current version
     * with immediate impact. fails if working version is not valid for publish.
     *
     * @see WorkingVersionValidator for publish condition
     */
    public function publishNewCurrentVersion(): void;

    /**
     * takes workingVersion, validates it for publish, makes an exact copy of the template's working version elements,
     * adds those copied elements as new current version's elements, overwrites the previous current version without
     * backing it up, activates the template and as such gives clients an updated current version with immediate impact.
     * fails if working version is not valid for publish.
     */
    public function rePublishAsExistingCurrentVersion(): void;

    /**
     * when a new current version is published, based on the working version at that time, and there was
     * already a current version in use, then that old current version becomes a previous published version and
     * stored here with it's version number.
     */
    public function getPreviouslyPublishedVersions(): PublishedVersionCollection;

    /**
     * a template must be active for any activity by end users. activation is automatically done when publishing a
     * new current version or by re-activation after a (temporary) deactivation of a template that has a current version.
     */
    public function isActive(): bool;

    /**
     * allows to halt all end-user (client) usage & access on a template. regardless if that template was already
     * published (or reactivated) with a current version. helps to block all access in case of an urgent change or
     * for whatever reason.
     *
     * @see reactivateCurrentVersion for more info
     */
    public function deactivate(): void;

    /**
     * if template has a current published version, but it not active, then it can only mean that the template
     * was de-activated by an admin (for whatever reason, temporarily or permanently).
     *
     * NOTE: when a template is successfully published @see publishNewCurrentVersion() , the template is
     * ALWAYS activated by default. admins can de-activate the current version at any time after it was published
     */
    public function isDeactivated(): bool;

    public function canReactivateCurrentVersion(): bool;

    /**
     * allows to reactivate the currentVersion of a template for end-user (client) usage that was (temporary) deactivated.
     *
     * with @see deactivate this allows an admin to de-activate to temporary block all usage by client end-users of this
     * active template e.g. because they want to make changes to the working version and don't want users to continue
     * submitting in current version until they @see publishNewCurrentVersion() that updated working version. or simply
     * because something external happens and they want to halt all usage of the template for whatever reason
     *
     * use @see canReactivateCurrentVersion to see if re-activation is allowed
     *
     * @throws MissingVersionException if there is no current version to reactivateCurrentVersion
     * @throws TemplateException       if there is a current version that is already active
     */
    public function reactivateCurrentVersion(): void;

    /**
     * admins can opt at any time to let the form - based on the current version of the @see FormList in this template -
     * to be shown in a series of (tab) views with 'next' and 'previous' buttons as a way to reduce the space taken
     * up by the form in the viewport, instead of displaying the form as one single page.
     *
     * IMPORTANT: this is a non-breaking method: admins can enable/disable this option at any time, but if the
     * conditions (@see FormList::canShowCategoriesAsPageBreaks() ) are not met, this method will simply ignore the option and
     * return false, instead of throwing an exception, because this is only for rendering purposes!
     *
     * NOTE: this is mainly a client side display solution. the only server side interaction is that every hit on
     * 'next' or 'previous' by a user (for which auto saving is not disabled) will trigger the auto temporary save
     * which in no way differs from any other auto temporary save. default is false in which case the form is shown
     * in a single page.
     */
    public function usesPageBreaksInCurrentVersion(): bool;

    public function isReadOnlyChoiceAnswerOptionsTogglingEnabled(): bool;

    public function isAutoTemporarySavingEnabled(): bool;

    public function isShowPositionNumbers(): bool;
}
