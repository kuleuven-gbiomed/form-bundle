<?php

declare(strict_types=1);

namespace KUL\FormBundle\Domain\Template\Contract;

/**
 * Interface StepPeriodInterface.
 *
 * period for a step that uses a given date tot determine if the step is started or ended
 */
interface StepPeriodInterface
{
    /** @var string */
    public const DATE_FORMAT = 'Y-m-d H:i:s';
}
