// use these styles or alternatively the ones src/Resources/public/js/formBundleStyles.js
// (better for webpack)

// npm dependencies
import 'selectize/dist/css/selectize.css';
import 'blueimp-file-upload/css/jquery.fileupload.css';
import 'bootstrap3/dist/css/bootstrap.min.css';
import 'jquery-ui/dist/themes/base/jquery-ui.min.css';

// local dependencies
import '../stylesheet/form-field.css'
import '../stylesheet/form-layout.css'
import '../stylesheet/summary-panels.css'