import 'selectize';
import 'blueimp-file-upload';
import 'bootstrap3';
import bootbox from 'bootbox';
import 'jquery-ui/dist/jquery-ui';
import Chart from 'chart.js/auto';

import 'form-bundle';

window.bootbox = bootbox
window.Chart = Chart
