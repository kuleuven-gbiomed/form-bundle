// Note: We expect JQuery to be loaded.
export default function historyCollapser() {
    'use strict';

    $("#workflow-history").on("show.bs.collapse", function () {
        $(this).parent().find(".panel-status-indicator").addClass('active');
    });

    $("#workflow-history").on('hide.bs.collapse', function () {
        $(this).parent().find(".panel-status-indicator").removeClass('active');
    });

    $('.step-submissions-collapser').on('click', function (event) {
        event.preventDefault();
        $('.' + $(this).data('target-class')).toggle();
    });

}