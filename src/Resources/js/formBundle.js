// Form bundle scripts
import preventSubmitOnEnter from './submitPrevention';
import otherOptionForSelectQuestions from './otherOption';
import questionUnlocker from "./question-unlocker";
import prefiller from "./prefiller";
import historyCollapser from "./historyCollapser";
import selectizedRoleSwitcher from "./selectizedRoleSwitcher";
import './mainFormInfo'
import './ckeditor'

// I don't think jquery-steps css is needed. It is in collected-embeds-stylesheets.html.twig though...
import '../public/stylesheet/form-field.css'
import '../public/stylesheet/form-layout.css'
import '../public/stylesheet/summary-panels.css'
import '../public/stylesheet/icons/iconfont/fonts/iconfont.woff'
import '../public/stylesheet/icons/iconfont/fonts/iconfont.ttf'

export { questionUnlocker, otherOptionForSelectQuestions, preventSubmitOnEnter, prefiller, historyCollapser, selectizedRoleSwitcher };