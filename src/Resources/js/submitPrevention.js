export default function preventSubmitOnEnter() {
// prevent submit buttons from triggering when user presses enter
    $(':text').on("keydown", function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
        }
    });
}
