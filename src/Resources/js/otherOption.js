// Note: We expect JQuery to be loaded.
export default function otherOptionForSelectQuestions() {
    $('.other-value-field').each(function (index) {
        var targetId = this.id.replace('other-value-field-', '');
        var otherFor = $(this).attr('kfb-other-for');

        // other-for dropdown (select(ize)) choice question
        let dropdownSelect = $('#id-' + otherFor);
        if (dropdownSelect.length) {
            dropdownSelect.change(
                function () {

                    // I don't understand javascript variable scopes 😟
                    let source = document.querySelector('#id-' + otherFor);
                    let target = document.querySelector('#other-value-field-' + targetId);
                    let selectedOtherOptionInMultiSelect = $(".item[data-value='" + targetId + "']");

                    const selectedIndex = source.selectedIndex;
                    const isSelected = (typeof source[selectedIndex] !== 'undefined' && source[selectedIndex].value === targetId)
                        || selectedOtherOptionInMultiSelect.length === 1;

                    target.classList[isSelected ? "add" : "remove"]("showme");
                }
            ).change();
        }

        // other-for radio group choice question
        let radioGroup = $('input[name="' + otherFor + '"]');
        if (radioGroup.length) {
            radioGroup.change(function() {
                let target = document.querySelector('#other-value-field-' + targetId);
                let isSelected = this.value === targetId && this.checked;
                target.classList[isSelected ? "add" : "remove"]("showme");

            }).change();
        }

        // other-for checkbox group choice question
        let checkboxGroup = $('input[name="' + otherFor + '[]"]');
        if (checkboxGroup.length) {
            checkboxGroup.change(function() {
                let target = document.querySelector('#other-value-field-' + targetId);
                if (this.value !== targetId) {
                    // this does a (weird) additional looping over the other checkboxes, including the 'other' field.
                    // need this loop to not listen to checked state of this checkbox for 'other' field if that checkbox was checked
                    // already, but is not the one checked in this change event now => hence return (not an issue for radio's,
                    // since for radio's, only one radio can be selected at once).
                    return;
                }

                target.classList[this.checked ? "add" : "remove"]("showme");

            }).change();
        }
    });
}
