// pass the selectize instances via the $select variable
let questionUnlocker = function ($, logger, $select) {

  let answersThatUnlockQuestions = $('[kfb-unlocks-questions]');

  $('#base_save').attr('formnovalidate', 'formnovalidate');

  let unlocker = {
    unlockedQuestions: new Map(),
    questionLockingAnswers: new Map(),

    /////////////////// code for AND
    selectedOptions: new Map(), // Stores currently selected options for dropdowns
    multiUnlockRules: {}, // Stores which options of which choice nodes unlock which questions

    determineUnlockRules: function () {
      let self = this;
      $('[kfb-multi-unlocking-question-ids]').each(function () {
        let id = $(this).attr('kfb-id');
        let multiUnlockingIds = $(this).attr('kfb-multi-unlocking-question-ids');

        // Parse unlockingIds safely
        try {
          multiUnlockingIds = multiUnlockingIds ? JSON.parse(multiUnlockingIds) : {};
        } catch (e) {
          console.error(`Invalid JSON in kfb-multi-unlocking-question-ids for id ${id}:`, e);
          multiUnlockingIds = {};
        }

        if (!$.isEmptyObject(multiUnlockingIds)) {
          self.multiUnlockRules[id] = multiUnlockingIds;
        }
      });

      // Apply the "locked" class to all questions that should be initially locked
      Object.keys(self.multiUnlockRules).forEach(questionId => {
        $(`[kfb-id="${questionId}"]`).addClass('locked');
      });
    },
    addSelectedOption: function (optionId) {
      if (!this.selectedOptions.has(optionId)) {
        this.selectedOptions.set(optionId);
      }
    },
    removeSelectedOption: function (optionId) {
      if (this.selectedOptions.has(optionId)) {
        this.selectedOptions.delete(optionId);
      }
    },
    // Checks if all required dropdowns have correct options selected
    checkMultiUnlockConditions: function () {
      let self = this;
      let selectedOptions = Array.from(this.selectedOptions.keys());

      return Object.entries(this.multiUnlockRules).map(([lockedQuestionId, unlockingConditions]) => {
        // Check if each unlocking question has at least one matching option
        let allConditionsMet = Object.entries(unlockingConditions).every(([unlockingQuestionId, requiredOptions]) => {
          return requiredOptions.some(optionId => this.selectedOptions.has(optionId));
        });
        let locked = true;
        if (allConditionsMet) {
          $(`[kfb-id="${lockedQuestionId}"]`).removeClass('locked');
          locked = false;
        } else {
          $(`[kfb-id="${lockedQuestionId}"]`).addClass('locked');
          locked = true;
        }
        if (this.questionLockToggler) {
          this.questionLockToggler(lockedQuestionId, locked);
        }
      });
    },
    getQuestionsIdsThatUnlockOtherQuestionsWithAnd: function () {
      const secondLevelKeys = [];
      const obj = this.multiUnlockRules;
      for (const topLevelKey in obj) {
        if (typeof obj[topLevelKey] === 'object' && !Array.isArray(obj[topLevelKey])) {
          for (const secondLevelKey in obj[topLevelKey]) {
            secondLevelKeys.push(secondLevelKey);
          }
        }
      }

      return new Set(secondLevelKeys);
    },

    ////////////////////// end code for AND

    lockQuestionByAnswer: function (questionId, answerId) {
      let unlockers = this.unlockedQuestions.get(questionId);

      if (undefined === unlockers) {
        return;
      }

      unlockers.delete(answerId);

      if (0 === unlockers.size) {
        this.unlockedQuestions.delete(questionId);
        this.questionLocked(questionId);
      } else {
        this.unlockedQuestions.set(questionId, unlockers);
      }
    },
    unlockQuestionByAnswer: function (questionId, answerId) {
      let unlockers = this.unlockedQuestions.get(questionId) || new Set();

      if (unlockers.has(answerId)) {
        return;
      }

      unlockers.add(answerId);

      this.unlockedQuestions.set(questionId, unlockers);

      if (1 === unlockers.size) {
        this.questionUnlocked(questionId);
      }
    },
    questionLocked: function (questionId) {
      logger.info('Question locked', {'question': questionId});
      let unlocker = this;
      this.questionLockingAnswers.forEach(function (object, answerId) {
        if (object.question === questionId) {
          unlocker.toggleAnswer(answerId, false);
        }
      });

      if (this.questionLockToggler) {
        this.questionLockToggler(questionId, true);
      }
    },
    questionUnlocked: function (questionId) {
      logger.info('Question unlocked', {'question': questionId});
      if (this.questionLockToggler) {
        this.questionLockToggler(questionId, false);
      }
    },
    setQuestionLockingAnswer: function (answerId, unlocksQuestionIds, questionId) {
      this.questionLockingAnswers.set(
        answerId,
        {
          unlocks: unlocksQuestionIds,
          question: questionId
        }
      );
    },
    setQuestionLockToggler: function (callback) {
      this.questionLockToggler = callback;
    },
    toggleAnswer: function (answerId, state) {
      let questionIds = this.questionLockingAnswers.get(answerId);

      if (undefined === questionIds) {
        return;
      }

      questionIds.unlocks.forEach(function (questionId) {
        state
          ? unlocker.unlockQuestionByAnswer(questionId, answerId)
          : unlocker.lockQuestionByAnswer(questionId, answerId)
        ;
      });
    },
    getQuestionsIdsThatUnlockOtherQuestions: function () {
      return new Set(Array.from(this.questionLockingAnswers.values()).map(function (questionLockingAnswer) {
        return questionLockingAnswer.question
      }));
    },
    getSelectizeInstance: function (questionId) {
      var instance = null;
      for (var i = 0; i < $select.length; ++i) {
        instance = $select[i];
        if (instance.id === 'id-' + questionId) {
          break;
        }
      }

      return instance;
    },
  };

  unlocker.determineUnlockRules();

  unlocker.setQuestionLockToggler(function (questionId, locked) {
    // let question = $(`[kfb-id='${questionId}']`).toggleClass('locked', locked);
    let question = $("[kfb-id='" + questionId + "']").toggleClass('locked', locked);

    if (false === locked) {
      // let questionTarget = $(`[name='${questionId}']`);
      let questionTarget = $("[name='" + questionId + "']");
      if (questionTarget.is('div.select-metadata')) {
        unlocker.getSelectizeInstance(questionId).selectize.trigger('change');
      } else {
        questionTarget.change();
      }
    }

    let ancestors = question.parents('.kfb-category');
    ancestors.each(function (index, element) {
      var category = $(element);
      category.toggleClass(
        'locked',
        category.find('.kfb-question').not('.locked').length === 0
      );

    });
  });

  answersThatUnlockQuestions.each(function () {
    var answer = $(this);
    var answerId = answer.attr('value');
    //                           radio/checkbox || select
    var questionId = (answer.attr('name') || answer.parent().attr('name'));
    var unlocksQuestionIds = answer.attr('kfb-unlocks-questions').split(' ');

    unlocker.setQuestionLockingAnswer(answerId, new Set(unlocksQuestionIds), questionId);
  });

  unlocker.getQuestionsIdsThatUnlockOtherQuestions().forEach(function (questionId) {
    setEventListeners(questionId);
  });

  ///// for AND
  unlocker.getQuestionsIdsThatUnlockOtherQuestionsWithAnd().forEach(function (questionId) {
    setEventListeners(questionId);
  });

  ////////

  function setEventListeners(questionId) {
    // var questionTarget = $(`[name="${questionId}"]`);
    var questionTarget = $("[name=\"" + questionId + "\"]");
    if (questionTarget.is('div.select-metadata')) {
      // For FORM-174, I added the information about which questions are unlocked by which answers,
      // which is in the kfb-unlocks-question attribute, to a hidden div.
      // Initially (before selectize), this information was in select- and option-tags,
      // but selectize would remove the options, making that information unavailable.
      // So if we find a kfb-unlocks-question attribute on a div, we know that we are handling a
      // drop-down with selectize.
      var answers = $('.answer-metadata[name="' + questionId + '"]');

      // The $selectize variable contains all instances of selectize. We need the one with the
      // identifier that matches questionId. I thought I could do
      /// $select.find(inst => inst.id === questionId), but that didn't seem to work?
      // FIXME: There must be a better way to find the correct instance 🤯
      var instance = unlocker.getSelectizeInstance(questionId);

      // Set a change event handler on the instance, so that it behaves correctly when the
      // user select an answer from the options.
      instance.selectize.on('change', function () {
        answers.each(function (index, element) {
          var option = $(element);
          if (option.attr('value') !== '') {
            const value = option.attr('value');
            if (value === instance.selectize.getValue()) {
              unlocker.addSelectedOption(value);
            } else {
              unlocker.removeSelectedOption(value);
            }
          }
          unlocker.checkMultiUnlockConditions();

          unlocker.toggleAnswer(
            option.attr('value'),
            // It looks strange to use instance.selectize inside this function.
            // But it seems to work. 🤔
            option.attr('value') === instance.selectize.getValue()
          );
        });
      });
      instance.selectize.trigger('change');

      return;
    }

    // In this case, the question is not a selectize drop-down, so we can use Brams existing code
    // to set up the change handler.
    var answers = questionTarget.is('select') ? questionTarget.children() : questionTarget;


    questionTarget.change(function () {
      answers.each(function (index, element) {
        var option = $(element);

        // add selected option to array when checked, otherwise remove it
        if (option.is(':checked' || option.is(':selected'))) {
          unlocker.addSelectedOption(option.attr('value'));
        } else {
          unlocker.removeSelectedOption(option.attr('value'));
        }

        unlocker.toggleAnswer(
          option.attr('value'),
          option.is(':checked') || option.is(':selected')
        );
      });

      // check unlockconditions for AND unlockers
      unlocker.checkMultiUnlockConditions();
    }).change();
  }

  return unlocker;
};

export default questionUnlocker;
