// assumes selectize plugin is present and loaded
let selectizedRoleSwitcher = function() {
    'use strict';

    let select = $('select#kfb-roleSwitcher');

    let renderSafeHtmlOptionText = function (item, escape) {
        let role = escape(item.roleLabel);
        return '<div class="option">' + escape(item.text).replace(role, '<strong>'+ role + '</strong>') + '</div>';
    };

    select.selectize({
        render: {
            item: function(item, escape) { return renderSafeHtmlOptionText(item, escape); },
            option: function(item, escape) { return renderSafeHtmlOptionText(item, escape); }
        },
        onChange: function (value) {
            if (value === '') {
                return false;
            }
            let data = this.options[value]; // pulls data-data from the original option
            window.location.replace(data.url); // reload page as other role
        }
    });
};

export default selectizedRoleSwitcher;