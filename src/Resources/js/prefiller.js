const prefiller = function () {
    'use strict';

    const prefillButtons = $(".prefill-button");
    const selectorFromId = function (id) {
        return '#'+id;
    };
    const unique = function(x, i, a) {
        return i === a.indexOf(x);
    };


    prefillButtons.each(function () {
        this.onclick = function () {
            let questionId = $(this).attr('data-node-id');
            let inputToPrefill = $(questionId);

            if (typeof inputToPrefill.attr('kfb-prefill-with') !== 'undefined' && inputToPrefill.attr('kfb-prefill-with') != '') {

                let sourceIds = inputToPrefill.attr('kfb-prefill-with').split(' ');
                let sourceSelector = sourceIds.map(selectorFromId).join(', ');
                let sourceElements = $(sourceSelector);

                let concatenatedValues = getConcatenatedValues(
                    sourceElements,
                    inputToPrefill.is('textarea') ? '\n\n' : ', '
                );


                inputToPrefill.val(concatenatedValues);
            }

            return false;
        };
    });

    function getConcatenatedValues(elements, delimiter) {
        return elements
            .map(function () {
                let val = $(this).val();
                // if question is in read-only mode, then there obviously is no input value.
                if ('' === val) {
                    // use html instead of .text() because the answer could come from question that allows html (textarea or wysiwyg)
                    // also, the.val() also takes html so let's keep it consistent
                    val = decodeHtml($(this).html());
                }

                return val;
            })
            .get()
            .filter(String)
            .filter(unique)
            .join(delimiter);
    }

    // https://stackoverflow.com/questions/7394748/whats-the-right-way-to-decode-a-string-that-has-special-html-entities-in-it?lq=1
    function decodeHtml(html) {
        let txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;
    }
};

export default prefiller;
