import 'ckeditor5/ckeditor5.css';
import { ClassicEditor, AutoLink, Autosave, Bold, Essentials, Italic, Link, List, Paragraph } from 'ckeditor5';
import coreTranslations from 'ckeditor5/translations/nl.js';

const editorConfig = {
  hideMaximizeButton: false,
  toolbar: {
    items: ['bold', 'italic', '|', 'bulletedList', 'numberedList', '|', 'link', '|', 'undo', 'redo'],
    shouldNotGroupWhenFull: false
  },
  plugins: [AutoLink, Autosave, Bold, Essentials, Italic, Link, List, Paragraph],
  licenseKey: 'GPL',
  link: {
    addTargetToExternalLinks: true,
    defaultProtocol: 'https://',
    decorators: {
      toggleDownloadable: {
        mode: 'manual',
        label: 'Downloadable',
        attributes: {
          download: 'file'
        }
      }
    }
  },
  translations: [ coreTranslations ],
  ui: {
    poweredBy: {
      position: 'inside',
      side: 'right',
      label: ''
    }
  },
  placeholder: 'Type or paste your content here!'
};

let wysiwygEditors = document.getElementsByClassName('wysiwygCKEditor5');
window.cKEditors = []; // set ckeditor instances to window to use in acceptance tests
for(let wysiwygEditor of wysiwygEditors) {
  ClassicEditor.create(wysiwygEditor, editorConfig)
    .then((cKEditor) => {
      window.cKEditors[wysiwygEditor.name] = cKEditor;
    });
}


