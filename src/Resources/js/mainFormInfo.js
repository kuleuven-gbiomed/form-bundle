document.addEventListener('DOMContentLoaded', function () {

  $("#kfb-main-form-information").on("show.bs.collapse", function () {
    $(this).parent().find(".panel-status-indicator").addClass('active');
  });

  $("#kfb-main-form-information").on('hide.bs.collapse', function () {
    $(this).parent().find(".panel-status-indicator").removeClass('active');
  });

});