<?php

declare(strict_types=1);

namespace KUL\FormBundle\Service;

use KUL\FormBundle\Entity\StoredTemplateTargetRepository;

class SubmittedTemplateService
{
    public function __construct(
        private readonly StoredTemplateTargetRepository $storedTemplateTargetRepository,
    ) {
    }

    public function hasSubmissionsForTemplate(string $templateId): bool
    {
        // for now we check if there are ANY submissions, even temporary or autosaves
        // if necessary we can extend this logic later
        return $this->storedTemplateTargetRepository->hasForTemplate($templateId);
    }
}
