<?php

declare(strict_types=1);

namespace KUL\FormBundle\Validator\Constraints;

use KUL\FormBundle\Utility\NumberScaleChecker;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * @see     NumberScale
 */
class NumberScaleValidator extends AbstractConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof NumberScale) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\NumberScale');
        }

        // this custom constraint/validator does not take care of empty values (see NotBlank, NotNull, etc. for that)
        if (null === $value || '' === $value) {
            return;
        }

        $maxScale = $constraint->getMaxScale();

        // skip if valid integer or valid float with decimals less or equal to max amount of decimals allowed
        if (!NumberScaleChecker::doesNumberExceedMaxAmountOfDecimals($value, $maxScale)) {
            return;
        }

        // custom error messages if only integer allowed but value contains decimals
        if (0 === $maxScale) {
            $this->context->buildViolation($constraint->getMessage())->addViolation();

            return;
        }

        // custom error message if float with optional decimals but value contains too many decimals
        $this->context->buildViolation($constraint->getMessage())->setParameter('%scale%', (string) $maxScale)->addViolation();
    }
}
