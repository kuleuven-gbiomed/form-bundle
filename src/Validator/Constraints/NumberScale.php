<?php

declare(strict_types=1);

namespace KUL\FormBundle\Validator\Constraints;

use KUL\FormBundle\Client\Manage\FormType\NumberType;
use KUL\FormBundle\Domain\Template\Element\Node\Input\NumberInput;

/**
 * used to validate amount of decimals allowed by the associated @see NumberInput::getScale() when building the
 * FormType @see NumberType . This FormType has an option 'scale' (via Symfony NumberType) but that is used to silently
 * remove the amount of decimals that exceed the max allowed amount (and subsequently round the submitted value), but
 * it does not validate the amount of decimals. hence this 'scale' option is disabled (null) when building number
 * questions. an out-of-the-box Symfony scale constraint does not exist. this contraint gives users the chance to
 * change/reduce decimals instead of cutting the amount of decimals.
 *
 * @see     NumberScaleValidator
 *
 * IMPORTANT: why the properties are protected and not private? see IMPORTANT on @see AbstractConstraint
 */
class NumberScale extends AbstractConstraint
{
    protected string $message = 'numberScale';

    /**
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected int $maxScale;

    /**
     * @psalm-suppress PropertyNotSetInConstructor (overwritten to keep psalm happy)
     *
     * @var string[]
     */
    public $groups;

    public function getRequiredOptions(): array
    {
        return ['maxScale'];
    }

    public function getMaxScale(): int
    {
        return $this->maxScale;
    }

    public function setMaxScale(mixed $maxScale): void
    {
        $this->maxScale = (int) $maxScale;
    }

    public function getMessage(): string
    {
        return $this->message.'.'.(0 === $this->getMaxScale() ? 'integer' : 'number');
    }
}
