<?php

declare(strict_types=1);

namespace KUL\FormBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class AbstractConstraint.
 *
 * NOTE: for the messages, use the key provided in the message property/properties from the extending classes
 * and use that key to provide a translation in your apps validators.locale.yml file(s)
 * the default translations are provided in kuleuvenform/form-domain/src/Resources/translations
 *
 * IMPORTANT: @see Constraint makes public properties of the passed-in options by default. setting as private with enforcing
 *        setters is impossible. setting with visibility protected with public getters is as good as it gets, unfortunately
 */
abstract class AbstractConstraint extends Constraint
{
    public function validatedBy(): string
    {
        return static::class.'Validator';
    }
}
