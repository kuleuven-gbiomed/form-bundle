<?php

declare(strict_types=1);

namespace KUL\FormBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class IsCheckedValidator.
 *
 * @see     IsChecked
 */
class IsCheckedValidator extends AbstractConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof IsChecked) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\IsChecked');
        }

        // this custom constraint/validator does not take care of empty values (see NotBlank, NotNull, etc. for that)
        if (null === $value || '' === $value) {
            return;
        }

        if (true === $value || 1 === $value || '1' === $value) {
            return;
        }

        $this->context->buildViolation($constraint->getMessage())
            ->setParameter('%value%', $this->formatValue($value))
            ->addViolation();
    }
}
