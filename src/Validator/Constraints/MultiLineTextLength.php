<?php

declare(strict_types=1);

namespace KUL\FormBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraints\Length;

/**
 * Class MultiLineTextLength.
 *
 * constraint used to validate the string value coming from textarea's fields and wysiwyg editor fields, where the
 * value can contain HTML and/or (line)breaks that on the one hands needs to be stored but also needs to be stripped
 * first so that the 'real' content can be validated on length, without counting the HTML tags and (line)breaks as
 * part of the content to store.
 *
 * @see     MultiLineTextLengthValidator
 *
 * IMPORTANT: why the properties are protected and not private? see IMPORTANT on @see AbstractConstraint
 */
class MultiLineTextLength extends Length
{
    /**
     * @psalm-suppress PropertyNotSetInConstructor (overwritten to keep psalm happy)
     *
     * @var string[]
     */
    public $groups;

    public function validatedBy(): string
    {
        return static::class.'Validator';
    }
}
