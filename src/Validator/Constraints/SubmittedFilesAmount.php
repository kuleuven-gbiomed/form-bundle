<?php

declare(strict_types=1);

namespace KUL\FormBundle\Validator\Constraints;

use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;

/**
 * Class SubmittedFilesAmount.
 *
 * allows to validate if uploaded files for a formType associated with a @see UploadInput is within the
 * min/max amount as set by @see UploadInput . the value to validate is NOT a set of files, but a concatenated string
 * coming from a hidden text input whose value is a concatenated string with the uids of those independently uploaded
 * files. Valid existence of a uploaded file for each uid in this value (the concatenated string) is not checked here
 * (the existence is checked elsewhere in the formType & beyond).
 * this constraint just assumes that each uid in the concatenated string is associated with an existing file that was
 * uploaded seperately and it uses the amount of uids from that (exploded) string to check if the amount of uploaded
 * files is within the min-max range to allow the associated set of uploaded files to be considered as a valid
 * 'submitted' value for the associated upload question.
 *
 * IMPORTANT: why the properties are protected and not private? see IMPORTANT on @see AbstractConstraint
 *
 * @see     SubmittedFilesAmountValidator
 */
class SubmittedFilesAmount extends AbstractConstraint
{
    protected string $message = 'submittedFilesAmount';
    /**
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected int $minAmount;
    /**
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected int $maxAmount;

    /**
     * @psalm-suppress PropertyNotSetInConstructor (overwritten to keep psalm happy)
     *
     * @var string[]
     */
    public $groups;

    public function getRequiredOptions(): array
    {
        return ['minAmount', 'maxAmount'];
    }

    public function getMinAmount(): int
    {
        return $this->minAmount;
    }

    public function getMaxAmount(): int
    {
        return $this->maxAmount;
    }

    public function getMessage(): string
    {
        if (1 === $this->getMinAmount() && $this->getMinAmount() === $this->getMaxAmount()) {
            return $this->message.'.mustContainExactlyOneFile';
        }

        return $this->message.'.mustContainFilesInRange';
    }

    public function getAddFilesToComplyMessage(): string
    {
        return $this->getMessage().'.addFilesToComply';
    }

    public function getRemoveFilesToComplyMessage(): string
    {
        return $this->getMessage().'.removeFilesToComply';
    }
}
