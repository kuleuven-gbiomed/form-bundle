<?php

declare(strict_types=1);

namespace KUL\FormBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class AbstractConstraintValidator.
 */
abstract class AbstractConstraintValidator extends ConstraintValidator
{
    /**
     * overwritten for correct type hinting,  making it possible to use @see ExecutionContextInterface::buildViolation()
     * without needless check & consequent if-else usage on context contract.
     *
     * between Symfony version 2.7 to 3.0, this property in parent see ConstraintValidator::$context
     * is still type hinted on the deprecated @see \Symfony\Component\Validator\ExecutionContextInterface
     * yet the new context interface @see ExecutionContextInterface (extending the old in 2.7) is always used.
     *
     * @psalm-suppress MissingConstructor
     * @psalm-suppress PropertyNotSetInConstructor
     *
     * @var ExecutionContextInterface
     */
    protected $context;
}
