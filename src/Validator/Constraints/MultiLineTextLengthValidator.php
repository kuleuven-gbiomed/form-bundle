<?php

declare(strict_types=1);

namespace KUL\FormBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LengthValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class MultiLineTextLengthValidator.
 *
 * IMPORTANT: this will strip the submitted value from all HTML tags and line(breaks) so that the 'real'
 * submitted content can be validated on char length. but this does NOT validate if the original value on length.
 * a malicious user could pump thousands of HTML tags into a textarea/wysiwyg field with only one single character as
 * 'real' content. in that case, this validator will pass, but you might end up with a huge string, assuming - as is
 * intended - that the HTML is not stripped from the final value that passed this validator. So it's wise to use an
 * additional standard Symfony @see Length constraint alongs this, e.g. with min & max set to 3 times the min & max
 * used in the @see MultiLineTextLength used in this validator
 *
 * @see     MultiLineTextLength
 */
class MultiLineTextLengthValidator extends LengthValidator
{
    /**
     * @psalm-suppress PropertyNotSetInConstructor
     *
     * @var ExecutionContextInterface
     */
    protected $context;

    public function __construct(private readonly string $cacheSerializerPath)
    {
    }

    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof MultiLineTextLength) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\WysiwygLength');
        }

        // this custom constraint/validator does not take care of empty values (see NotBlank, NotNull, etc. for that)
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_scalar($value) && !(is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedTypeException($value, 'string');
        }

        // strip all HTML tags from the submitted value so we can validate the length solely based on the 'real' content
        // FIXME: Maybe we can use the HTMLPurifierBundle which has built in Symfony integration?
        // https://github.com/Exercise/HTMLPurifierBundle
        $stringValue = (string) $value;
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('AutoFormat.RemoveEmpty', true);
        // we set the HTML.Allowed to none. because we want to remove ALL HTML tags around the textual content here,
        // so we can check only the actual text content.
        // however, malicious HTMl, like script tags, are by default removed WITH their actual text content by the purifier
        // e.g. suppose value is '<p>blabla</p>'. this will be purified to 'blabla', which means it has a char length of 6.
        //      suppose value is '<p></p>'. this will be purified to '', which means it has a char length of 0.
        //      suppose value is '<script>blabla</script>'. this will be purified to '' with 'blabla' removed, which means it has a char length of 0!
        $config->set('HTML.Allowed', '');
        $config->set('Cache.SerializerPath', $this->cacheSerializerPath);
        $purifier = new \HTMLPurifier($config);
        $stringValue = $purifier->purify($stringValue);
        // make sure that (line) breaks are gone (coming from textarea's or copy pastes in wysiwyg or ...)
        $stringValue = preg_replace("/\r|\n|\t/", '', $stringValue);

        // delegate with 'real' value to standard parent Length validator
        parent::validate($stringValue, $constraint);
    }
}
