<?php

declare(strict_types=1);

namespace KUL\FormBundle\Validator\Constraints;

/**
 * Class IsChecked.
 *
 * @see IsCheckedValidator
 *
 * IMPORTANT: why the properties are protected and not private? see IMPORTANT on @see AbstractConstraint
 */
class IsChecked extends AbstractConstraint
{
    protected string $message = 'isChecked.mustBeChecked';

    /**
     * @psalm-suppress PropertyNotSetInConstructor (overwritten to keep psalm happy)
     *
     * @var string[]
     */
    public $groups;

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
}
