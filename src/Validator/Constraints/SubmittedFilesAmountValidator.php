<?php

declare(strict_types=1);

namespace KUL\FormBundle\Validator\Constraints;

use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class SubmittedFilesAmountValidator.
 *
 * @see     SubmittedFilesAmount
 *
 * NOTE: a min & max amount is always set on @see UploadInput and Constraint SubmittedFilesAmount is always added
 *       to the associated FormType , based on min - max amount. meaning that if input is configured as
 *       non-required, the min will be available with value 0 (zero)
 */
class SubmittedFilesAmountValidator extends AbstractConstraintValidator
{
    /**
     * NOTE: no check on NULL or empty value here with default return, as is custom in other constraints so that the
     * NotBlank or NotNull can handle that! this constraint(Validator) replaces usage of NotNull and NotBlank and
     * converts the value - being a string, empty string or NULL - to an array and holds that array against the
     * min-max range as set by @see UploadInput.
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof SubmittedFilesAmount) {
            throw new UnexpectedTypeException($constraint, SubmittedFilesAmount::class);
        }

        $minAmount = $constraint->getMinAmount();
        $maxAmount = $constraint->getMaxAmount();
        $amountInSubmit = count(UploadInput::extractSavedFileUidsFromFormValue($value));

        // if amount of uids associated with uploaded files is within range, validate a ok. if value was NULL or empty
        // string then amountInSubmit will be 0, and if minAmount is > 0, then this will not validate and continue as intended
        if ($amountInSubmit >= $minAmount && $amountInSubmit <= $maxAmount) {
            return;
        }

        if ($amountInSubmit < $minAmount) {
            $amountToAdd = $minAmount - $amountInSubmit;

            $this->context->buildViolation($constraint->getAddFilesToComplyMessage())
                ->setParameter('%count%', (string) $amountToAdd)
                ->setParameter('%min%', (string) $minAmount)
                ->setParameter('%max%', (string) $maxAmount)
                ->addViolation();

            return;
        }

        if ($amountInSubmit > $maxAmount) {
            $amountToRemove = $amountInSubmit - $maxAmount;

            $this->context->buildViolation($constraint->getRemoveFilesToComplyMessage())
                ->setParameter('%count%', (string) $amountToRemove)
                ->setParameter('%min%', (string) $minAmount)
                ->setParameter('%max%', (string) $maxAmount)
                ->addViolation();
        }
    }
}
