<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Repository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflowStep;

/**
 * @extends EntityRepository<FormWorkflowStep>
 */
class FormWorkflowStepRepository extends EntityRepository
{
    final public const NO_ENTITY_FOUND = 'FormBuilder: No Form Workflow step found.';

    // -----------------------------------------------------------------------------------------------------------------
    // Single entity functions
    // -----------------------------------------------------------------------------------------------------------------

    public function save(FormWorkflowStep $model): FormWorkflowStep
    {
        $em = $this->getEntityManager();
        $em->persist($model);
        $em->flush();

        return $model;
    }

    public function getById(int $id): FormWorkflowStep
    {
        $model = $this->find($id);
        if (!$model instanceof FormWorkflowStep) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function getBySequenceAndWorkflowId(int $sequence, int $workflowId): FormWorkflowStep
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.sequence = :sequence')
            ->setParameter('sequence', $sequence)
            ->andWhere('t.formWorkflow = :formWorkflowId')
            ->setParameter('formWorkflowId', $workflowId)
            ->getQuery()
            ->getOneOrNullResult();
        if (null === $model) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function getByUidAndWorkflowId(string $uid, int $workflowId): FormWorkflowStep
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.uid = :uid')
            ->setParameter('uid', $uid)
            ->andWhere('t.formWorkflow = :formWorkflowId')
            ->setParameter('formWorkflowId', $workflowId)
            ->getQuery()
            ->getOneOrNullResult();
        if (null === $model) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function findByWorkflowAndSequence(FormWorkflow $formWorkflow, int $sequence): ?FormWorkflowStep
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.formWorkflow = :formWorkflowId')
            ->setParameter('formWorkflowId', $formWorkflow->getId())
            ->andWhere('t.sequence = :sequence')
            ->setParameter('sequence', $sequence)
            ->getQuery()
            ->getOneOrNullResult();

        return $model;
    }

    public function delete(FormWorkflowStep $model): bool
    {
        $em = $this->getEntityManager();
        $em->remove($model);
        $em->flush();

        return true;
    }
}
