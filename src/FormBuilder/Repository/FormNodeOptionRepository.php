<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Repository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;

/**
 * @extends EntityRepository<FormNodeOption>
 */
class FormNodeOptionRepository extends EntityRepository
{
    final public const NO_ENTITY_FOUND = 'FormBuilder: No Form Node Option found.';

    // -----------------------------------------------------------------------------------------------------------------
    // Single entity functions
    // -----------------------------------------------------------------------------------------------------------------

    public function save(FormNodeOption $model): FormNodeOption
    {
        $em = $this->getEntityManager();
        $em->persist($model);
        $em->flush();

        return $model;
    }

    public function delete(FormNodeOption $model): void
    {
        $em = $this->getEntityManager();
        $em->remove($model);
        $em->flush();
    }

    public function findById(int $id): ?FormNodeOption
    {
        $model = $this->find($id);
        if ($model instanceof FormNodeOption) {
            return $model;
        }

        return null;
    }

    public function getById(int $id): FormNodeOption
    {
        $model = $this->find($id);
        if (!$model instanceof FormNodeOption) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function findByUidAndNodeId(string $uid, int $nodeId): ?FormNodeOption
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.uid = :uid')
            ->setParameter('uid', $uid)
            ->andWhere('t.formNode = :formNodeId')
            ->setParameter('formNodeId', $nodeId)
            ->getQuery()
            ->getOneOrNullResult();

        return $model;
    }

    public function getByUidAndNodeId(string $uid, int $nodeId): FormNodeOption
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.uid = :uid')
            ->setParameter('uid', $uid)
            ->andWhere('t.formNode = :formNodeId')
            ->setParameter('formNodeId', $nodeId)
            ->getQuery()
            ->getOneOrNullResult();
        if (null === $model) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function findByFormNodeAndSequence(FormNode $formNode, int $sequence): ?FormNodeOption
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.formNode = :formNodeId')
            ->setParameter('formNodeId', $formNode->getId())
            ->andWhere('t.sequence = :sequence')
            ->setParameter('sequence', $sequence)
            ->getQuery()
            ->getOneOrNullResult();

        return $model;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Multiple entity functions
    // -----------------------------------------------------------------------------------------------------------------

    /** @return list<FormNodeOption> */
    public function getOptionsByUnlocksQuestionIds(string $targetUid): array
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.unlocksQuestionIds LIKE :targetUid')
            ->setParameter('targetUid', '%'.$targetUid.'%')
            ->orderBy('t.id', 'ASC');

        return $query->getQuery()->getResult();
    }
}
