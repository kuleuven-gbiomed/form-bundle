<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Repository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormInfo;

/**
 * @extends EntityRepository<FormInfo>
 */
class FormInfoRepository extends EntityRepository
{
    final public const NO_ENTITY_FOUND = 'FormBuilder: No Form Info found.';

    // -----------------------------------------------------------------------------------------------------------------
    // Single entity functions
    // -----------------------------------------------------------------------------------------------------------------

    public function save(FormInfo $model): FormInfo
    {
        $em = $this->getEntityManager();
        $em->persist($model);
        $em->flush();

        return $model;
    }

    public function getById(int $id): FormInfo
    {
        $model = $this->find($id);
        if (!$model instanceof FormInfo) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function findByForm(Form $form): ?FormInfo
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->getQuery()
            ->getOneOrNullResult();

        return $model;
    }

    public function getByForm(Form $form): FormInfo
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->getQuery()
            ->getOneOrNullResult();
        if (null === $model) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function getByUid(string $uid): FormInfo
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.uid = :uid')
            ->setParameter('uid', $uid)
            ->getQuery()
            ->getOneOrNullResult();
        if (null === $model) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }
}
