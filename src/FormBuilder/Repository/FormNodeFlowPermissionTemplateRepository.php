<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Repository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermissionTemplate;

/**
 * @extends EntityRepository<FormNodeFlowPermissionTemplate>
 */
class FormNodeFlowPermissionTemplateRepository extends EntityRepository
{
    final public const NO_ENTITY_FOUND = 'FormBuilder: No Form Flow Permission template found.';

    public function save(FormNodeFlowPermissionTemplate $model): FormNodeFlowPermissionTemplate
    {
        $em = $this->getEntityManager();
        $em->persist($model);
        $em->flush();

        return $model;
    }

    public function delete(FormNodeFlowPermissionTemplate $template): bool
    {
        $em = $this->getEntityManager();
        $em->remove($template);
        $em->flush();

        return true;
    }

    public function getById(int $id): FormNodeFlowPermissionTemplate
    {
        $model = $this->find($id);
        if (!$model instanceof FormNodeFlowPermissionTemplate) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function getTemplatesByForm(Form $form): array
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId());

        return $query->getQuery()->getResult();
    }
}
