<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Repository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;

/**
 * @extends EntityRepository<FormNode>
 */
class FormNodeRepository extends EntityRepository
{
    final public const NO_ENTITY_FOUND = 'FormBuilder: No Form List Node found.';

    // -----------------------------------------------------------------------------------------------------------------
    // Single entity functions
    // -----------------------------------------------------------------------------------------------------------------

    public function save(FormNode $model): FormNode
    {
        $em = $this->getEntityManager();
        $em->persist($model);
        $em->flush();

        return $model;
    }

    public function delete(FormNode $model): bool
    {
        $em = $this->getEntityManager();
        $em->remove($model);
        $em->flush();

        return true;
    }

    public function findById(int $id): ?FormNode
    {
        $model = $this->find($id);
        if ($model instanceof FormNode) {
            return $model;
        }

        return null;
    }

    public function getById(int $id): FormNode
    {
        $model = $this->find($id);
        if (!$model instanceof FormNode) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function findByUidAndFormId(string $uid, int $formId): ?FormNode
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.uid = :uid')
            ->setParameter('uid', $uid)
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $formId)
            ->getQuery()
            ->getOneOrNullResult();

        return $model;
    }

    public function getByUidAndFormId(string $uid, int $formId): FormNode
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.uid = :uid')
            ->setParameter('uid', $uid)
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $formId)
            ->getQuery()
            ->getOneOrNullResult();
        if (null === $model) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function findByFormLevelSequenceTypeSubTypeUid(
        Form $form,
        int $level,
        int $sequence,
        string $type,
        string $subType,
        string $uid,
    ): ?FormNode {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.uid = :uid')
            ->setParameter('uid', $uid)
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->andWhere('t.level = :level')
            ->setParameter('level', $level)
            ->andWhere('t.sequence = :sequence')
            ->setParameter('sequence', $sequence)
            ->andWhere('t.type = :type')
            ->setParameter('type', $type)
            ->andWhere('t.subType = :subType')
            ->setParameter('subType', $subType)
            ->getQuery()
            ->getOneOrNullResult();

        return $model;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Multiple entity functions
    // -----------------------------------------------------------------------------------------------------------------

    public function getRootNodeForForm(Form $form): FormNode
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->andWhere('t.parent is NULL')
            ->andWhere('t.level = :level')
            ->setParameter('level', 0)
            ->orderBy('t.sequence', 'ASC')
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $model) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    /** @return list<FormNode> */
    public function getNodesByFormAndSubType(Form $form, string $subType): array
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->andWhere('t.subType = :subType')
            ->setParameter('subType', $subType)
            ->andWhere('t.level != 0')
            ->orderBy('t.parent', 'ASC');

        return $query->getQuery()->getResult();
    }

    /** @return list<FormNode> */
    public function getNodesByFormAndType(Form $form, string $type): array
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->andWhere('t.type = :type')
            ->setParameter('type', $type)
            ->andWhere('t.level != 0')
            ->orderBy('t.parent', 'ASC');

        return $query->getQuery()->getResult();
    }

    /** @return list<FormNode> */
    public function getQuestionsByFormAndListUid(Form $form, array $listUid): array
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->andWhere('t.uid IN (:listUid)')
            ->setParameter('listUid', $listUid)
            ->andWhere('t.type = :type')
            ->setParameter('type', FormNode::TYPE_QUESTION)
            ->orderBy('t.id', 'ASC');

        return $query->getQuery()->getResult();
    }

    /** @return list<FormNode> */
    public function getQuestionsByFormAndListId(Form $form, array $listId): array
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->andWhere('t.id IN (:listId)')
            ->setParameter('listId', $listId)
            ->andWhere('t.type = :type')
            ->setParameter('type', FormNode::TYPE_QUESTION)
            ->orderBy('t.id', 'ASC');

        return $query->getQuery()->getResult();
    }

    /** @return list<FormNode> */
    public function getNodesByOptionUnlockIds(Form $form, string $targetUid): array
    {
        $query = $this->createQueryBuilder('t')
            ->leftJoin('t.options', 'o')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->andWhere('o.unlocksQuestionIds LIKE :targetUid')
            ->setParameter('targetUid', '%'.$targetUid.'%')
            ->orderBy('t.parent', 'ASC');

        return $query->getQuery()->getResult();
    }

    /** @return list<FormNode> */
    public function getNodesByCalculationParticipation(Form $form, string $scoreUid): array
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->andWhere('t.scoringConfig LIKE :sourceUid')
            ->setParameter('sourceUid', '%"scoreNodeUid": "'.$scoreUid.'%')
            ->orderBy('t.id', 'ASC');

        return $query->getQuery()->getResult();
    }

    /** @return list<FormNode> */
    public function getNodesByPrefilParticipation(Form $form, string $prefillUid): array
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->andWhere('t.prefillConfig LIKE :prefillUid')
            ->setParameter('prefillUid', '%'.$prefillUid.'%')
            ->orderBy('t.id', 'ASC');

        return $query->getQuery()->getResult();
    }

    /** @return list<FormNode> */
    public function getNodesByChartParticipation(Form $form, string $participantUid): array
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->andWhere('t.propsConfig LIKE :sourceUid')
            ->setParameter('sourceUid', '%"question_uid": "'.$participantUid.'%')
            ->orderBy('t.id', 'ASC');

        return $query->getQuery()->getResult();
    }
}
