<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Repository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use KUL\FormBundle\FormBuilder\Entity\Form;

/**
 * @extends EntityRepository<Form>
 */
class FormRepository extends EntityRepository
{
    final public const NO_ENTITY_FOUND = 'FormBuilder: No Form found.';

    // -----------------------------------------------------------------------------------------------------------------
    // Single entity functions
    // -----------------------------------------------------------------------------------------------------------------

    public function save(Form $model): Form
    {
        $em = $this->getEntityManager();
        $em->persist($model);
        $em->flush();

        return $model;
    }

    public function getById(int $id): Form
    {
        $model = $this->find($id);
        if (!$model instanceof Form) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function findByUid(string $uid): ?Form
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.uid = :uid')
            ->setParameter('uid', $uid)
            ->getQuery()
            ->getOneOrNullResult();

        return $model;
    }

    public function getByUid(string $uid): Form
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.uid = :uid')
            ->setParameter('uid', $uid)
            ->getQuery()
            ->getOneOrNullResult();
        if (null === $model) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function deleteForm(Form $form): void
    {
        $em = $this->getEntityManager();
        $em->remove($form);
        $em->flush();
    }
}
