<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Repository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;

/**
 * @extends EntityRepository<FormNodeFlowPermission>
 */
class FormNodeFlowPermissionRepository extends EntityRepository
{
    final public const NO_ENTITY_FOUND = 'FormBuilder: No Form Flow Permission found.';

    // -----------------------------------------------------------------------------------------------------------------
    // Single entity functions
    // -----------------------------------------------------------------------------------------------------------------

    public function save(FormNodeFlowPermission $model): FormNodeFlowPermission
    {
        $em = $this->getEntityManager();
        $em->persist($model);
        $em->flush();

        return $model;
    }

    public function delete(FormNodeFlowPermission $model): bool
    {
        $em = $this->getEntityManager();
        $em->remove($model);
        $em->flush();

        return true;
    }

    public function getById(int $id): FormNodeFlowPermission
    {
        $model = $this->find($id);
        if (!$model instanceof FormNodeFlowPermission) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function findByFormNode(FormNode $formNode): ?FormNodeFlowPermission
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.formNode = :formNodeId')
            ->setParameter('formNodeId', $formNode->getId())
            ->getQuery()
            ->getOneOrNullResult();

        return $model;
    }

    public function getByFormNode(FormNode $formNode): FormNodeFlowPermission
    {
        $model = $this->findByFormNode($formNode);

        if (null === $model) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function findByFormNodeOption(FormNodeOption $formNodeOption): ?FormNodeFlowPermission
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.formNodeOption = :formNodeOptionId')
            ->setParameter('formNodeOptionId', $formNodeOption->getId())
            ->getQuery()
            ->getOneOrNullResult();

        return $model;
    }

    public function getByFormNodeOption(FormNodeOption $formNodeOption): FormNodeFlowPermission
    {
        $model = $this->findByFormNodeOption($formNodeOption);

        if (null === $model) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }
}
