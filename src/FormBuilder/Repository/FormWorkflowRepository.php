<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Repository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;

/**
 * @extends EntityRepository<FormWorkflow>
 */
class FormWorkflowRepository extends EntityRepository
{
    final public const NO_ENTITY_FOUND = 'FormBuilder: No Form Workflow found.';

    // -----------------------------------------------------------------------------------------------------------------
    // Single entity functions
    // -----------------------------------------------------------------------------------------------------------------

    public function save(FormWorkflow $model): FormWorkflow
    {
        $em = $this->getEntityManager();
        $em->persist($model);
        $em->flush();

        return $model;
    }

    public function getById(int $id): FormWorkflow
    {
        $model = $this->find($id);
        if (!$model instanceof FormWorkflow) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function findByForm(Form $form): ?FormWorkflow
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->getQuery()
            ->getOneOrNullResult();

        return $model;
    }

    public function getByForm(Form $form): FormWorkflow
    {
        $model = $this->createQueryBuilder('t')
            ->andWhere('t.form = :formId')
            ->setParameter('formId', $form->getId())
            ->getQuery()
            ->getOneOrNullResult();
        if (null === $model) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function findByUid(string $uid): ?FormWorkflow
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.uid = :uid')
            ->setParameter('uid', $uid)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
