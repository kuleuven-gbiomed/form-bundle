<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Repository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use KUL\FormBundle\FormBuilder\Entity\CommandHistoryEntry;

/**
 * @extends EntityRepository<CommandHistoryEntry>
 */
class CommandHistoryEntryRepository extends EntityRepository
{
    final public const NO_ENTITY_FOUND = 'FormBuilder: No History Entry found.';

    public function save(CommandHistoryEntry $model): CommandHistoryEntry
    {
        $em = $this->getEntityManager();
        $em->persist($model);
        $em->flush();

        return $model;
    }

    public function getById(int $id): CommandHistoryEntry
    {
        $model = $this->find($id);
        if (!$model instanceof CommandHistoryEntry) {
            throw new EntityNotFoundException(self::NO_ENTITY_FOUND);
        }

        return $model;
    }

    public function getTail(int $number = 10): array
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT c
            FROM KUL\FormBundle\FormBuilder\Entity\CommandHistoryEntry c
            ORDER BY c.id DESC'
        )->setMaxResults($number);

        return $query->getResult();
    }

    public function getByCommandAndSubjectId(string $commandType, int $subjectId): array
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.type = :commandType')
            ->setParameter('commandType', $commandType)
            ->andWhere('t.subjectId = :subjectId')
            ->setParameter('subjectId', $subjectId)
            ->addOrderBy('t.id', 'DESC');

        return $query->getQuery()->getResult();
    }

    /** return CommandHistoryEntry[] */
    public function getByFormUid(string $formUid): array
    {
        $query = $this->createQueryBuilder('t')
            ->andWhere('t.formUid = :formUid')
            ->setParameter('formUid', $formUid)
            ->addOrderBy('t.id', 'DESC');

        return $query->getQuery()->getResult();
    }

    public function deleteEntriesByFormUid(string $formUid): bool
    {
        $qb = $this->createQueryBuilder('c');
        $qb->delete()
            ->where('c.formUid = :uid')
            ->setParameter('uid', $formUid);
        $query = $qb->getQuery();
        $query->execute();

        return true;
    }
}
