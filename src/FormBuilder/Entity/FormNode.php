<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Table(name="kuleuven_form_fb_form_node")
 *
 * @ORM\Entity(repositoryClass="KUL\FormBundle\FormBuilder\Repository\FormNodeRepository")
 */
class FormNode
{
    // ---------------------------------------------------------------------------------------------------------- traits
    use Hashable;
    use Identifiable;
    use Timestampable;

    // ------------------------------------------------------------------------------------------------------- constants
    final public const TYPE_CATEGORY = 'category';
    final public const TYPE_QUESTION = 'question';

    final public const SUB_TYPE_CATEGORY = 'category';
    final public const SUB_TYPE_CHOICE = 'question_choice';
    final public const SUB_TYPE_NUMBER = 'question_number';
    final public const SUB_TYPE_TEXT = 'question_text';
    final public const SUB_TYPE_UPLOAD = 'question_upload';
    final public const SUB_TYPE_DATE = 'question_date';
    final public const SUB_TYPE_RADAR_CHART = 'read_only_view_radar_chart';
    final public const SUB_TYPE_READ_ONLY_VIEW_BASIC = 'read_only_view_basic';

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": true})
     */
    private bool $required = false;
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $info = [];
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": true})
     */
    private bool $hidden = false;
    /**
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private string $displayMode = '';
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $firstHeaderCellTitle = [];
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $infoConfig = [];
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $propsConfig = [];
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $scoringConfig = [];
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $prefillConfig = [];
    /**
     * array of choice ids as keys and array of option ids as values.
     *
     * @param string[][] $multiUnlockedBy
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private array $multiUnlockConfig = [];

    /**
     * @ORM\OneToMany(targetEntity="FormNode", mappedBy="parent", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     *
     * @ORM\OrderBy({"sequence": "ASC"})
     *
     * @var Collection<array-key, self>
     */
    private Collection $children;
    /**
     * @ORM\OneToMany(targetEntity="FormNodeOption", mappedBy="formNode", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     *
     * @ORM\OrderBy({"sequence": "ASC"})
     *
     * @var Collection<array-key, FormNodeOption>
     */
    private Collection $options;

    // -----------------------------------------------------------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        string $uid,
        /**
         * @ORM\ManyToOne(targetEntity="FormNode", inversedBy="children", fetch="EXTRA_LAZY", cascade={"persist"})
         *
         * @ORM\JoinColumn(name="parent_id", nullable=true, onDelete="CASCADE")
         */
        private ?self $parent,
        /**
         * @ORM\Column(type="integer", nullable=false, options={"default" : 0})
         */
        private int $level,
        // ------------------------------------------------------------------------------------------------------ properties
        /**
         * @ORM\Column(type="integer", nullable=false, options={"default" : 0})
         */
        private int $sequence,
        /**
         * @ORM\Column(type="string", length=191, nullable=true)
         */
        private string $type,
        /**
         * @ORM\Column(type="string", length=191, nullable=true)
         */
        private string $subType,
        // ------------------------------------------------------------------------------------------------------- relations
        /**
         * @ORM\ManyToOne(targetEntity="KUL\FormBundle\FormBuilder\Entity\Form", inversedBy="nodes", fetch="EXTRA_LAZY")
         *
         * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
         */
        private Form $form,
    ) {
        $this->uid = $uid;
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        // make sure Doctrine relations are set with empty, default Collection (i.e. ArrayCollection as used when hydrated)
        // this avoids errors 'Typed property xxx must not be accessed before initialization' in e.g. Hashable::makeContentHash
        $this->children = new ArrayCollection();
        $this->options = new ArrayCollection();
        $this->multiUnlockConfig = [];
    }

    public function importFields(
        bool $required,
        array $info,
        bool $hidden,
        string $displayMode,
        array $firstHeaderCellTitle = [],
        array $infoConfig = [],
        array $propsConfig = [],
        array $scoringConfig = [],
        array $prefillConfig = [],
        array $multiUnlockConfig = [],
    ): void {
        $this->updatedAt = new \DateTimeImmutable();
        $this->required = $required;
        $this->info = $info;
        $this->hidden = $hidden;
        $this->displayMode = $displayMode;
        $this->firstHeaderCellTitle = $firstHeaderCellTitle;
        $this->infoConfig = $infoConfig;
        $this->propsConfig = $propsConfig;
        $this->scoringConfig = $scoringConfig;
        $this->prefillConfig = $prefillConfig;
        $this->multiUnlockConfig = $multiUnlockConfig;
    }

    public function __clone()
    {
        // If the entity has an identity, proceed as normal. (From doctrine documentation)
        if ($this->id > 0) {
            $this->children = $this->children->map(function (self $child) {
                $clonedChild = clone $child;
                $clonedChild->setParent($this);

                return $clonedChild;
            });
            $this->options = $this->options->map(function (FormNodeOption $option) {
                $clonedOption = clone $option;
                $clonedOption->initFormNodeForCopy($this);

                return $clonedOption;
            });
            $this->uid = Uuid::uuid4()->toString();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Setters
    // -----------------------------------------------------------------------------------------------------------------

    public function touch(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function setRequired(bool $required): void
    {
        $this->touch();
        $this->required = $required;
    }

    public function setInfo(array $info): void
    {
        $this->touch();
        $this->info = $info;
    }

    public function setInfoConfig(array $infoConfig): void
    {
        $this->touch();
        $this->infoConfig = $infoConfig;
    }

    public function setHidden(bool $hidden): void
    {
        $this->touch();
        $this->hidden = $hidden;
    }

    public function setDisplayMode(string $displayMode): void
    {
        $this->touch();
        $this->displayMode = $displayMode;
    }

    public function setPropsConfig(array $propsConfig): void
    {
        $this->touch();
        $this->propsConfig = $propsConfig;
    }

    public function setScoringConfig(array $scoringConfig): void
    {
        $this->touch();
        $this->scoringConfig = $scoringConfig;
    }

    public function setFirstHeaderCellTitle(array $firstHeaderCellTitle): void
    {
        $this->touch();
        $this->firstHeaderCellTitle = $firstHeaderCellTitle;
    }

    public function setPrefillConfig(array $prefillConfig): void
    {
        $this->touch();
        $this->prefillConfig = $prefillConfig;
    }

    public function setMultiUnlockConfig(array $multiUnlockConfig): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->multiUnlockConfig = $multiUnlockConfig;
    }

    /** @param string[] $optionUids */
    public function updateMultiUnlockIdsForChoice(string $choiceUid, array $optionUids): void
    {
        $this->updatedAt = new \DateTimeImmutable();

        $config = $this->multiUnlockConfig['multiUnlockingQuestionUids'];

        if (array_key_exists($choiceUid, $config)) {
            if (0 === count($optionUids)) {
                unset($config[$choiceUid]);
            } else {
                $config[$choiceUid] = $optionUids;
            }
        } else {
            $config[$choiceUid] = $optionUids;
        }

        $this->multiUnlockConfig['multiUnlockingQuestionUids'] = $config;
    }

    public function removeMultiUnlockId(string $optionIdToRemove): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $config = $this->multiUnlockConfig['multiUnlockingQuestionUids'];
        foreach ($config as $key => $subarray) {
            if (($index = array_search($optionIdToRemove, $subarray, true)) !== false) {
                unset($config[$key][$index]);
                // Reindex the subarray to maintain sequential keys
                $config[$key] = array_values($config[$key]);
                if (0 === count($config[$key])) {
                    unset($config[$key]);
                }
            }
        }
        $this->multiUnlockConfig['multiUnlockingQuestionUids'] = $config;
    }

    public function setParent(self $parent): void
    {
        $this->touch();
        $this->parent = $parent;
    }

    public function setSequence(int $sequence): void
    {
        $this->touch();
        $this->sequence = $sequence;
    }

    public function setLevel(int $level): void
    {
        $this->touch();
        $this->level = $level;
    }

    public function setPrefillingQuestionUids(array $prefillingQuestionUids): void
    {
        $this->touch();
        $this->prefillConfig[ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS] = $prefillingQuestionUids;
    }

    public function addNodeAsPrefillingNode(self $prefillingNode): void
    {
        $this->touch();
        $this->prefillConfig[ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS][] = $prefillingNode->getUid();
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function getSequence(): int
    {
        return $this->sequence;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isCategory(): bool
    {
        return self::TYPE_CATEGORY === $this->type;
    }

    public function getSubType(): string
    {
        return $this->subType;
    }

    public function getRequired(): bool
    {
        return $this->required;
    }

    public function getInfo(): array
    {
        return $this->info;
    }

    public function getHidden(): bool
    {
        return $this->hidden;
    }

    public function getDisplayMode(): string
    {
        return $this->displayMode;
    }

    public function getFirstHeaderCellTitle(): array
    {
        return $this->firstHeaderCellTitle;
    }

    public function getInfoConfig(): array
    {
        return $this->infoConfig;
    }

    public function getPropsConfig(): array
    {
        return $this->propsConfig;
    }

    public function getScoringConfig(): array
    {
        return $this->scoringConfig;
    }

    public function getPrefillConfig(): array
    {
        return $this->prefillConfig;
    }

    public function getMultiUnlockConfig(): array
    {
        return $this->multiUnlockConfig;
    }

    public function getPrefillingQuestionUids(): array
    {
        return $this->prefillConfig[ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS];
    }

    public function getForm(): Form
    {
        return $this->form;
    }

    /** @return Collection<array-key, FormNode> */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /** @return Collection<array-key, FormNodeOption> */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    /** @return list<FormNode> */
    public function getFlattenedChildren(): array
    {
        $children = [];
        foreach ($this->children as $child) {
            $nextChildren = $child->getFlattenedChildren();
            foreach ($nextChildren as $nextChild) {
                $children[] = $nextChild;
            }
            $children[] = $child;
        }

        return $children;
    }

    public function getAllChildrenIds(): array
    {
        $childrenIds = [];
        foreach ($this->children as $child) {
            $nextChildrenIds = $child->getAllChildrenIds();
            foreach ($nextChildrenIds as $nextChildId) {
                $childrenIds[] = $nextChildId;
            }
            $childrenIds[] = $child->getId();
        }

        return $childrenIds;
    }

    public function getAllParentIds(): array
    {
        $parentIds = [];
        if ($this->parent instanceof self) {
            $parentIds[] = $this->parent->getId();
            foreach ($this->parent->getAllParentIds() as $parentId) {
                $parentIds[] = $parentId;
            }
        }

        return $parentIds;
    }

    /**
     * Returns an array with the at each key sequence of the node and its parents in reverse order.
     *
     * @return list<int>
     */
    public function getPosition(): array
    {
        $position = [];
        $node = $this;
        while (null !== $node) {
            array_unshift($position, $node->getSequence());
            $node = $node->getParent();
        }

        return $position;
    }

    public function isBelowNode(self $node): bool
    {
        $thisPosition = $this->getPosition();
        $comparePosition = $node->getPosition();

        foreach ($thisPosition as $key => $value) {
            if (!array_key_exists($key, $comparePosition)) {
                return true;
            }
            if ($value !== $comparePosition[$key]) {
                return $value > $comparePosition[$key];
            }
        }

        return false;
    }

    public function isCalculationQuestion(): bool
    {
        return
            isset($this->getScoringConfig()['globalScoreCalculationDependencyConfigs'])
            && [] !== $this->getScoringConfig()['globalScoreCalculationDependencyConfigs'];
    }

    public function isPrefilledQuestion(): bool
    {
        return
            array_key_exists('prefillingQuestionUids', $this->getPrefillConfig())
            && [] !== $this->getPrefillConfig()['prefillingQuestionUids'];
    }
}
