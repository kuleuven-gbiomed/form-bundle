<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="kuleuven_form_fb_form")
 *
 * @ORM\Entity(repositoryClass="KUL\FormBundle\FormBuilder\Repository\FormRepository")
 */
class Form
{
    use Hashable;
    use Identifiable;
    use Timestampable;

    /**
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private string $targetingType = '';

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private int $currentVersionNumber = 0;

    /**
     * The user who deconstructed the form.
     *
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private string $user = '';

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $rolesThatCouldParticipate = [];

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": true})
     */
    private bool $active = false;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     */
    private bool $showPositionNumbers = false;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     */
    private bool $enableReadOnlyChoiceAnswerOptionsToggling = false;

    /**
     * @ORM\OneToMany(targetEntity="FormNode", mappedBy="form", fetch="EXTRA_LAZY")
     *
     * @ORM\OrderBy({"sequence": "ASC"})
     *
     * @var Collection<array-key, FormNode>
     */
    private Collection $nodes;

    /**
     * @ORM\OneToMany(targetEntity="FormNodeFlowPermissionTemplate", mappedBy="form", fetch="EXTRA_LAZY")
     *
     * @var Collection<array-key, FormNodeFlowPermissionTemplate>
     */
    private Collection $permissionTemplates;

    /**
     * @ORM\OneToOne(targetEntity="KUL\FormBundle\FormBuilder\Entity\FormInfo", mappedBy="form", cascade={"persist", "remove"})
     */
    private ?FormInfo $formInfo = null;

    // -----------------------------------------------------------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(string $uid)
    {
        $this->uid = $uid;
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        // make sure Doctrine relations are set with empty, default Collection (i.e. ArrayCollection as used when hydrated)
        // this avoids errors 'Typed property xxx must not be accessed before initialization' in e.g. Hashable::makeContentHash
        $this->nodes = new ArrayCollection();
        $this->permissionTemplates = new ArrayCollection();
    }

    public function importFields(
        string $targetingType,
        int $currentVersionNumber,
        array $rolesThatCouldParticipate,
        bool $active,
        bool $showPositionNumbers,
        bool $enableReadOnlyChoiceAnswerOptionsToggling,
    ): void {
        $this->updatedAt = new \DateTimeImmutable();
        $this->targetingType = trim($targetingType);
        $this->currentVersionNumber = $currentVersionNumber;
        $this->rolesThatCouldParticipate = $rolesThatCouldParticipate;
        $this->active = $active;
        $this->showPositionNumbers = $showPositionNumbers;
        $this->enableReadOnlyChoiceAnswerOptionsToggling = $enableReadOnlyChoiceAnswerOptionsToggling;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Setters
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * For use by the DeconstructionManager
     * this method doesn't set a new updatedAt time.
     */
    public function deconstructedBy(string $user): void
    {
        $this->user = trim($user);
    }

    public function touch(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Getters
    // -----------------------------------------------------------------------------------------------------------------
    public function getFormInfo(): ?FormInfo
    {
        return $this->formInfo;
    }

    public function getTargetingType(): string
    {
        return $this->targetingType;
    }

    public function getCurrentVersionNumber(): int
    {
        return $this->currentVersionNumber;
    }

    public function isShowPositionNumbers(): bool
    {
        return $this->showPositionNumbers;
    }

    public function setShowPositionNumbers(bool $showPositionNumbers): void
    {
        $this->showPositionNumbers = $showPositionNumbers;
    }

    public function isEnableReadOnlyChoiceAnswerOptionsToggling(): bool
    {
        return $this->enableReadOnlyChoiceAnswerOptionsToggling;
    }

    public function setEnableReadOnlyChoiceAnswerOptionsToggling(bool $enableReadOnlyChoiceAnswerOptionsToggling): void
    {
        $this->enableReadOnlyChoiceAnswerOptionsToggling = $enableReadOnlyChoiceAnswerOptionsToggling;
    }

    /**
     * Technically individual nodes can have any locale,
     * but we limit the available locales for editing in the FormBuilder
     * to the locales available for the title of the form.
     *
     * In other words: To be able to edit a form in a language you have
     * to add a title in that locale in @see FormInfo
     */
    public function getLanguages(): array
    {
        return $this->formInfo?->getLanguages() ?? [];
    }

    public function getRolesThatCouldParticipate(): array
    {
        return $this->rolesThatCouldParticipate;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function getUser(): string
    {
        return $this->user;
    }

    /** @return Collection<array-key, FormNode> */
    public function getNodes(): Collection
    {
        return $this->nodes;
    }
}
