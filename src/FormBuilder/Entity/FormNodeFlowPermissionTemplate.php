<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use KUL\FormBundle\FormBuilder\Commands\UpdatePermissions\Model\FlowPermission;

/**
 * @ORM\Table(name="kuleuven_form_fb_form_node_flow_permission_template")
 *
 * @ORM\Entity(repositoryClass="KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionTemplateRepository")
 */
class FormNodeFlowPermissionTemplate
{
    // ---------------------------------------------------------------------------------------------------------- traits
    use Hashable;
    use Identifiable;
    use Timestampable;

    /**
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private string $name = '';
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $flowPermission = [];

    // ------------------------------------------------------------------------------------------------------- relations
    /**
     * @ORM\OneToMany(targetEntity="FormNodeFlowPermission", mappedBy="template", fetch="EXTRA_LAZY")
     *
     * @var Collection<array-key, FormNodeFlowPermission>
     */
    private Collection $flowPermissions;
    /**
     * @ORM\ManyToOne(targetEntity="Form", inversedBy="permissionTemplates", fetch="EXTRA_LAZY")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private ?Form $form = null;

    // -----------------------------------------------------------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(string $uid)
    {
        $this->uid = $uid;
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        // make sure Doctrine relations are set with empty, default Collection (i.e. ArrayCollection as used when hydrated)
        // this avoids errors 'Typed property xxx must not be accessed before initialization' in e.g. Hashable::makeContentHash
        $this->flowPermissions = new ArrayCollection();
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Setters
    // -----------------------------------------------------------------------------------------------------------------

    public function setFormNameAndFlowPermission(Form $form, string $name, FlowPermission $flowPermission): void
    {
        $this->form = $form;
        $this->name = $name;
        $this->flowPermission = $flowPermission->getSteps();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function setNameAndFlowPermission(string $name, FlowPermission $flowPermission): void
    {
        $this->name = $name;
        $this->flowPermission = $flowPermission->getSteps();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function setFlowPermission(FlowPermission $flowPermission): void
    {
        $this->flowPermission = $flowPermission->getSteps();
        $this->updatedAt = new \DateTimeImmutable();
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getName(): string
    {
        return $this->name;
    }

    public function getFlowPermission(): array
    {
        return $this->flowPermission;
    }

    public function getForm(): Form
    {
        if (!$this->form instanceof Form) {
            throw new \Exception('no form found');
        }

        return $this->form;
    }
}
