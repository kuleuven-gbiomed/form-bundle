<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Table(name="kuleuven_form_fb_form_node_option")
 *
 * @ORM\Entity(repositoryClass="KUL\FormBundle\FormBuilder\Repository\FormNodeOptionRepository")
 */
class FormNodeOption
{
    // ---------------------------------------------------------------------------------------------------------- traits
    use Hashable;
    use Identifiable;
    use Timestampable;

    // ------------------------------------------------------------------------------------------------------- constants

    final public const TYPE_QUESTION_OPTION = 'question_option';

    final public const INPUT_TYPE_OPTION = 'option';
    final public const INPUT_TYPE_OPTION_WITH_ADDITIONAL_TEXT = 'optionWithAdditionalText';
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": true})
     */
    private bool $required = false;
    /**
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private string $displayMode = 'regular';
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": true})
     */
    private bool $defaultSelected = false;
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $score = null;
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $info = [];
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": true})
     */
    private bool $hideLabelInRelevantChoiceDisplayMode = false;
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": true})
     */
    private bool $hidden = false;
    /**
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private string $descriptionDisplayMode = 'icon';
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $unlocksQuestionIds = [];
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $multiUnlocksQuestionIds = [];
    /**
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private string $inputType = self::INPUT_TYPE_OPTION;
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $propsConfig = [];

    // -----------------------------------------------------------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        string $uid,
        // ------------------------------------------------------------------------------------------------------ properties
        /**
         * @ORM\Column(type="integer", nullable=false, options={"default" : 0})
         */
        private int $sequence,
        /**
         * @ORM\Column(type="string", length=191, nullable=true)
         */
        private string $type,
        // ------------------------------------------------------------------------------------------------------- relations
        /**
         * @ORM\ManyToOne(targetEntity="FormNode", inversedBy="options", fetch="EXTRA_LAZY")
         *
         * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
         */
        private FormNode $formNode,
    ) {
        // -- uuid
        $this->uid = $uid;

        // -- timestamps
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->propsConfig['type'] = 'option';
        $this->multiUnlocksQuestionIds = [];
    }

    public function importFields(
        bool $required,
        string $displayMode,
        bool $defaultSelected,
        array $info,
        bool $hideLabelInRelevantChoiceDisplayMode,
        bool $hidden,
        string $descriptionDisplayMode,
        array $unlocksQuestionIds,
        array $multiUnlocksQuestionIds,
        string $inputType,
        array $propsConfig,
        ?float $score,
    ): void {
        $this->updatedAt = new \DateTimeImmutable();
        $this->required = $required;
        $this->displayMode = $displayMode;
        $this->defaultSelected = $defaultSelected;
        $this->score = $score;
        $this->info = $info;
        $this->hideLabelInRelevantChoiceDisplayMode = $hideLabelInRelevantChoiceDisplayMode;
        $this->hidden = $hidden;
        $this->descriptionDisplayMode = $descriptionDisplayMode;
        $this->unlocksQuestionIds = $unlocksQuestionIds;
        $this->multiUnlocksQuestionIds = $multiUnlocksQuestionIds;
        $this->inputType = $inputType;
        $this->propsConfig = $propsConfig;
    }

    public function __clone()
    {
        // If the entity has an identity, proceed as normal. (From doctrine documentation)
        if ($this->id > 0) {
            $this->uid = Uuid::uuid4()->toString();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Setters
    // -----------------------------------------------------------------------------------------------------------------

    public function setInfo(array $info): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->info = $info;
    }

    public function setDefaultSelected(bool $isDefaultSelected): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->defaultSelected = $isDefaultSelected;
    }

    public function setScore(?float $score): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->score = $score;
    }

    public function setSequence(int $sequence): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->sequence = $sequence;
    }

    public function setInputType(string $inputType): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->inputType = $inputType;
    }

    public function setPropsConfig(array $propsConfig): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->propsConfig = $propsConfig;
    }

    public function setUnlockIds(array $unlocksQuestionIds): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->unlocksQuestionIds = $unlocksQuestionIds;
    }

    public function addUnlockId(string $uid): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->unlocksQuestionIds[] = $uid;
    }

    public function setMultiUnlockIds(array $unlocksQuestionIds): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->multiUnlocksQuestionIds = $unlocksQuestionIds;
    }

    public function addMultiUnlockId(string $uid): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->multiUnlocksQuestionIds[] = $uid;
    }

    public function initFormNodeForCopy(FormNode $formNode): void
    {
        $this->formNode = $formNode;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getSequence(): int
    {
        return $this->sequence;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function getDisplayMode(): string
    {
        return $this->displayMode;
    }

    public function isDefaultSelected(): bool
    {
        return $this->defaultSelected;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function getInfo(): array
    {
        return $this->info;
    }

    public function getHideLabelInRelevantChoiceDisplayMode(): bool
    {
        return $this->hideLabelInRelevantChoiceDisplayMode;
    }

    public function getHidden(): bool
    {
        return $this->hidden;
    }

    public function getDescriptionDisplayMode(): string
    {
        return $this->descriptionDisplayMode;
    }

    public function getUnlocksQuestionIds(): array
    {
        return $this->unlocksQuestionIds;
    }

    public function getMultiUnlocksQuestionIds(): array
    {
        return $this->multiUnlocksQuestionIds;
    }

    public function getInputType(): string
    {
        return $this->inputType;
    }

    public function getPropsConfig(): array
    {
        return $this->propsConfig;
    }

    public function getFormNode(): FormNode
    {
        return $this->formNode;
    }
}
