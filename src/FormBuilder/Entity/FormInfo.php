<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="kuleuven_form_fb_form_info")
 *
 * @ORM\Entity(repositoryClass="KUL\FormBundle\FormBuilder\Repository\FormInfoRepository")
 */
class FormInfo
{
    use Hashable;
    use Identifiable;
    use Timestampable;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $label = [];
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $description = [];
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $roleDescriptions = [];

    // -----------------------------------------------------------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        string $uid,
        /**
         * @ORM\OneToOne(targetEntity="KUL\FormBundle\FormBuilder\Entity\Form", fetch="EXTRA_LAZY", inversedBy="formInfo")
         *
         * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
         */
        private Form $form,
    ) {
        $this->uid = $uid;
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function importFields(
        array $label,
        array $description,
        array $roleDescriptions,
    ): void {
        $this->label = $label;
        $this->description = $description;
        $this->roleDescriptions = $roleDescriptions;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Setters
    // -----------------------------------------------------------------------------------------------------------------

    public function touch(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function setLabel(array $label): void
    {
        $this->touch();
        $this->label = $label;
    }

    public function setDescription(array $description): void
    {
        $this->touch();
        $this->description = $description;
    }

    public function setRoleDescriptions(array $roleDescriptions): void
    {
        $this->touch();
        $this->roleDescriptions = $roleDescriptions;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getLabel(): array
    {
        return $this->label;
    }

    public function getDescription(): array
    {
        return $this->description;
    }

    public function getRoleDescriptions(): array
    {
        return $this->roleDescriptions;
    }

    public function getForm(): Form
    {
        return $this->form;
    }

    public function getLanguages(): array
    {
        return array_keys($this->label);
    }
}
