<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Entity;

use Doctrine\ORM\Mapping as ORM;
use KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRoleOffsetConfigs\ConfigSubject;
use KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepText\DescriptionSubject;

/**
 * @ORM\Table(name="kuleuven_form_fb_form_workflow_step")
 *
 * @ORM\Entity(repositoryClass="KUL\FormBundle\FormBuilder\Repository\FormWorkflowStepRepository")
 */
class FormWorkflowStep
{
    use Hashable;
    use Identifiable;
    use Timestampable;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private int $x = 0;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private int $y = 0;

    // ---------------------------------------------------------------------------
    // Constructor
    // ---------------------------------------------------------------------------

    public function __construct(
        string $uid,
        /**
         * @ORM\Column(type="integer", nullable=false, options={"default" : 0})
         */
        private int $sequence,
        /**
         * @ORM\ManyToOne(targetEntity="KUL\FormBundle\FormBuilder\Entity\FormWorkflow", inversedBy="steps", fetch="EXTRA_LAZY")
         *
         * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
         */
        private FormWorkflow $formWorkflow,
        /**
         * @ORM\Column(type="string", length=191, nullable=true)
         */
        private string $type,
        /**
         * @ORM\Column(type="json", nullable=true)
         */
        private array $period,
        /**
         * @ORM\Column(type="boolean", nullable=false, options={"default":true})
         */
        private bool $allowsStartIfPreviousStepIsFulfilled,
        /**
         * @ORM\Column(type="json", nullable=true)
         */
        private array $rolesWithOneSubmit,
        /**
         * @ORM\Column(type="json", nullable=true)
         */
        private array $info,
        /**
         * @ORM\Column(type="json", nullable=true)
         */
        private array $submitSuccessUserInteraction,
        /**
         * @ORM\Column(type="boolean", nullable=false, options={"default":true})
         */
        private bool $hideReadOnlyNodesOnLanding,
        /**
         * @ORM\Column(type="json", nullable=true)
         */
        private array $customOptions,
    ) {
        $this->uid = $uid;
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    // ---------------------------------------------------------------------------
    // Setters
    // ---------------------------------------------------------------------------

    public function setSchemaCoordinates(int $x, int $y): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->x = $x;
        $this->y = $y;
    }

    public function setSequence(int $sequence): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->sequence = $sequence;
    }

    public function setNameAndTiming(array $info, array $period, bool $allowsStartIfPreviousStepIsFulfilled): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->info = $info;
        $this->period = $period;
        $this->allowsStartIfPreviousStepIsFulfilled = $allowsStartIfPreviousStepIsFulfilled;
    }

    public function setRolesWithOneSubmit(array $roles): void
    {
        $this->rolesWithOneSubmit = $roles;
    }

    public function setHideReadOnlyNodesOnLanding(bool $hideReadOnlyNodesOnLanding): void
    {
        $this->hideReadOnlyNodesOnLanding = $hideReadOnlyNodesOnLanding;
    }

    public function setRedirectAwayOnSubmitSuccess(bool $redirectAwayOnSubmitSuccess): void
    {
        // {"notificationConfig": {
        //      "mailBodyForStart": [],
        //      "mailBodyForOngoing": [],
        //      "roleConfigsToNotifyOfStart": [],
        //      "roleConfigsToRemindOfOngoing": []
        //      "roleMailBodiesForStart": []
        //      "roleMailBodiesForOngoing": []
        //  },
        // "roleSubmitSuccessMessages": [],
        // "generalSubmitSuccessMessage": [],
        // "redirectAwayOnSubmitSuccess": false}

        $this->submitSuccessUserInteraction['redirectAwayOnSubmitSuccess'] = $redirectAwayOnSubmitSuccess;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Update some descriptions
    // -----------------------------------------------------------------------------------------------------------------

    public function updateSingleText(DescriptionSubject $subject, string $language, string $description): void
    {
        switch ($subject) {
            case DescriptionSubject::generalSubmitSuccessMessage:
                if (0 === (is_countable($this->submitSuccessUserInteraction['generalSubmitSuccessMessage'])
                        ? count($this->submitSuccessUserInteraction['generalSubmitSuccessMessage']) : 0)) {
                    if (mb_strlen(trim($description)) > 0) {
                        $this->submitSuccessUserInteraction['generalSubmitSuccessMessage'] = [];
                        $this->submitSuccessUserInteraction['generalSubmitSuccessMessage'][$language] = $description;
                    }
                } else {
                    $this->submitSuccessUserInteraction['generalSubmitSuccessMessage'][$language] = $description;
                }
                break;
            case DescriptionSubject::mailBodyForStart:
                if (0 === (is_countable($this->submitSuccessUserInteraction['notificationConfig']['mailBodyForStart'])
                        ? count($this->submitSuccessUserInteraction['notificationConfig']['mailBodyForStart']) : 0)) {
                    if (mb_strlen(trim($description)) > 0) {
                        $this->submitSuccessUserInteraction['notificationConfig']['mailBodyForStart'] = [];
                        $this->submitSuccessUserInteraction['notificationConfig']['mailBodyForStart'][$language] = $description;
                    }
                } else {
                    $this->submitSuccessUserInteraction['notificationConfig']['mailBodyForStart'][$language] = $description;
                }
                break;
            case DescriptionSubject::mailBodyForOngoing:
                if (0 === (is_countable($this->submitSuccessUserInteraction['notificationConfig']['mailBodyForOngoing'])
                        ? count($this->submitSuccessUserInteraction['notificationConfig']['mailBodyForOngoing']) : 0)) {
                    if (mb_strlen(trim($description)) > 0) {
                        $this->submitSuccessUserInteraction['notificationConfig']['mailBodyForOngoing'] = [];
                        $this->submitSuccessUserInteraction['notificationConfig']['mailBodyForOngoing'][$language] = $description;
                    }
                } else {
                    $this->submitSuccessUserInteraction['notificationConfig']['mailBodyForOngoing'][$language] = $description;
                }
                break;
        }
    }

    public function setRoleSubmitSuccessMessages(array $roleSubmitSuccessMessages): void
    {
        $this->submitSuccessUserInteraction['roleSubmitSuccessMessages'] = $roleSubmitSuccessMessages;
    }

    public function setRoleMailBodiesForStart(array $roleDescriptions): void
    {
        $this->submitSuccessUserInteraction['notificationConfig']['roleMailBodiesForStart'] = $roleDescriptions;
    }

    public function setRoleMailBodiesForOngoing(array $roleDescriptions): void
    {
        $this->submitSuccessUserInteraction['notificationConfig']['roleMailBodiesForOngoing'] = $roleDescriptions;
    }

    public function updateOffsetConfig(ConfigSubject $subject, array $roleConfiguration): void
    {
        switch ($subject) {
            case ConfigSubject::roleConfigsToNotifyOfStart:
                $this->submitSuccessUserInteraction['notificationConfig']['roleConfigsToNotifyOfStart'] = $roleConfiguration;
                break;
            case ConfigSubject::roleConfigsToRemindOfOngoing:
                $this->submitSuccessUserInteraction['notificationConfig']['roleConfigsToRemindOfOngoing'] = $roleConfiguration;
                break;
        }
    }

    // ---------------------------------------------------------------------------
    // Getters
    // ---------------------------------------------------------------------------

    public function getSequence(): int
    {
        return $this->sequence;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getPeriod(): array
    {
        return $this->period;
    }

    public function getAllowsStartIfPreviousStepIsFulfilled(): bool
    {
        return $this->allowsStartIfPreviousStepIsFulfilled;
    }

    public function getRolesWithOneSubmit(): array
    {
        return $this->rolesWithOneSubmit;
    }

    public function getInfo(): array
    {
        return $this->info;
    }

    public function getSubmitSuccessUserInteraction(): array
    {
        return $this->submitSuccessUserInteraction;
    }

    public function getHideReadOnlyNodesOnLanding(): bool
    {
        return $this->hideReadOnlyNodesOnLanding;
    }

    public function getCustomOptions(): array
    {
        return $this->customOptions;
    }

    public function getFormWorkflow(): FormWorkflow
    {
        return $this->formWorkflow;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }
}
