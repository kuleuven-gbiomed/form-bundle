<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="kuleuven_form_fb_form_node_flow_permission")
 *
 * @ORM\Entity(repositoryClass="KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionRepository")
 */
class FormNodeFlowPermission
{
    // ---------------------------------------------------------------------------------------------------------- traits
    use Hashable;
    use Identifiable;
    use Timestampable;

    // ------------------------------------------------------------------------------------------------------- constants
    final public const DEFAULT_CATEGORY = 'default-category';
    final public const QUESTION = 'question';
    final public const OPTION = 'option';
    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private array $flowPermission = [];
    /**
     * @ORM\ManyToOne(targetEntity="FormNodeFlowPermissionTemplate", inversedBy="flowPermissions", fetch="EXTRA_LAZY")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private ?FormNodeFlowPermissionTemplate $template = null;

    // -----------------------------------------------------------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        // ------------------------------------------------------------------------------------------------------ properties
        /**
         * @ORM\Column(type="string", length=191, nullable=true)
         */
        private string $type,
        // ------------------------------------------------------------------------------------------------------- relations
        /**
         * @ORM\OneToOne(targetEntity="FormNode", fetch="EXTRA_LAZY")
         *
         * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
         */
        private ?FormNode $formNode = null,
        /**
         * @ORM\OneToOne(targetEntity="FormNodeOption", fetch="EXTRA_LAZY")
         *
         * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
         */
        private ?FormNodeOption $formNodeOption = null,
    ) {
        $this->uid = '';
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function importFields(array $flowPermission): void
    {
        $this->flowPermission = $flowPermission;
    }

    public function setFlowPermission(array $flowPermission): void
    {
        $this->flowPermission = $flowPermission;
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function setTemplate(FormNodeFlowPermissionTemplate $template): void
    {
        $this->template = $template;
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function removeTemplate(): void
    {
        $this->template = null;
        $this->updatedAt = new \DateTimeImmutable();
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getType(): string
    {
        return $this->type;
    }

    public function getFlowPermission(): array
    {
        return $this->flowPermission;
    }

    public function getFormNode(): ?FormNode
    {
        return $this->formNode;
    }

    public function getFormNodeOption(): ?FormNodeOption
    {
        return $this->formNodeOption;
    }

    public function getTemplate(): ?FormNodeFlowPermissionTemplate
    {
        return $this->template;
    }
}
