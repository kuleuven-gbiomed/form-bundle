<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Hashable
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $contentHash = '';

    // -----------------------------------------------------------------------------------------------------------------
    // Getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getContentHash(): string
    {
        return $this->contentHash;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Hash the content
    // -----------------------------------------------------------------------------------------------------------------

    public function makeContentHash(): void
    {
        $excludedProperties = ['contentHash', 'id'];
        $propertiesArray = array_filter(
            get_object_vars($this),
            fn ($key) => !in_array($key, $excludedProperties, true),
            \ARRAY_FILTER_USE_KEY
        );

        $serializedProperties = serialize($propertiesArray);
        $this->contentHash = hash('md5', $serializedProperties);
    }
}
