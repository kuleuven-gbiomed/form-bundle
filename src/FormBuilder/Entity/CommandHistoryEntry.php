<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="kuleuven_form_fb_command_history")
 *
 * @ORM\Entity(repositoryClass="KUL\FormBundle\FormBuilder\Repository\CommandHistoryEntryRepository")
 */
class CommandHistoryEntry implements \JsonSerializable
{
    /**
     * psalm issue: property id is set & handled by Doctrine.
     *
     * @psalm-suppress PropertyNotSetInConstructor
     *
     * @ORM\Id
     *
     * @ORM\GeneratedValue
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        /**
         * @ORM\Column(type="integer", nullable=false, options={"default" : 0})
         */
        private int $currentVersionNumber,
        /**
         * @ORM\Column(type="string", nullable=false)
         */
        private string $formUid,
        /**
         * @ORM\Column(type="datetime_immutable", nullable=false)
         */
        private \DateTimeImmutable $timestamp,
        /**
         * @ORM\Column(type="string", nullable=false)
         */
        private string $user,
        /**
         * @ORM\Column(type="integer", nullable=false)
         */
        private int $subjectId,
        /**
         * @ORM\Column(type="string", nullable=false)
         */
        private string $subject,
        /**
         * @ORM\Column(type="string", nullable=false)
         */
        private string $type,
        /**
         * @ORM\Column(type="text", nullable=false)
         */
        private string $commandJSON,
    ) {
    }

    // -----------------------------------------------------------------------------------------------------------------
    // what to render as json
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * @return array{id: int, currentVersionNumber: int, form: string, timestamp: \DateTimeImmutable, user: string, commandType: string, subjectId: int, changeFrom: string, changeTo: string, add: string, delete: string}
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'currentVersionNumber' => $this->currentVersionNumber,
            'form' => $this->formUid,
            'timestamp' => $this->timestamp,
            'user' => $this->user,
            'commandType' => $this->type,
            'subjectId' => $this->subjectId,
            'changeFrom' => $this->getChangeFrom(),
            'changeTo' => $this->getChangeTo(),
            'add' => '',
            'delete' => '',
        ];
    }

    public function getChangeFrom(): string
    {
        $result = '';
        if (str_contains(strtoupper($this->type), 'UPDATE')) {
            $commandContent = json_decode($this->commandJSON, null, 512, \JSON_THROW_ON_ERROR);
            $result = json_encode($commandContent->changeFrom, \JSON_THROW_ON_ERROR);
        }

        return $result;
    }

    public function getChangeTo(): string
    {
        $result = '';
        if (str_contains(strtoupper($this->type), 'UPDATE')) {
            $commandContent = json_decode($this->commandJSON, null, 512, \JSON_THROW_ON_ERROR);
            $result = json_encode($commandContent->changeTo, \JSON_THROW_ON_ERROR);
        }

        return $result;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getId(): int
    {
        return $this->id;
    }

    public function getCurrentVersionNumber(): int
    {
        return $this->currentVersionNumber;
    }

    public function getTimestamp(): \DateTimeImmutable
    {
        return $this->timestamp;
    }

    public function getUser(): string
    {
        return $this->user;
    }

    public function getCommandJSON(): string
    {
        return $this->commandJSON;
    }

    public function getFormUid(): string
    {
        return $this->formUid;
    }

    public function getSubjectId(): int
    {
        return $this->subjectId;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
