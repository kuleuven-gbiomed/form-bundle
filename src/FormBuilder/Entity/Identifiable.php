<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Identifiable
{
    /**
     * psalm issue: property id is set & handled by Doctrine.
     *
     * @psalm-suppress PropertyNotSetInConstructor
     *
     * @ORM\Id()
     *
     * @ORM\GeneratedValue()
     *
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=191, nullable=false)
     */
    private string $uid;

    // -----------------------------------------------------------------------------------------------------------------
    // Getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getId(): int
    {
        return $this->id;
    }

    public function getUid(): string
    {
        return $this->uid;
    }
}
