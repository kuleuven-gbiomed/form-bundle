<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="kuleuven_form_fb_form_workflow")
 *
 * @ORM\Entity(repositoryClass="KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository")
 */
class FormWorkflow
{
    use Hashable;
    use Identifiable;
    use Timestampable;

    /**
     * @ORM\Column(type="string", length=191, nullable=true)
     */
    private string $stepsType = '';
    /**
     * @ORM\OneToMany(targetEntity="KUL\FormBundle\FormBuilder\Entity\FormWorkflowStep", mappedBy="formWorkflow", fetch="EXTRA_LAZY")
     *
     * @var Collection<array-key, FormWorkflowStep>
     *
     * @ORM\OrderBy({"sequence": "ASC"})
     */
    private Collection $steps;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private int $startX = 0;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private int $startY = 0;

    // -----------------------------------------------------------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        string $uid,
        /**
         * @ORM\OneToOne(targetEntity="KUL\FormBundle\FormBuilder\Entity\Form", fetch="EXTRA_LAZY")
         *
         * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
         */
        private Form $form,
    ) {
        $this->uid = $uid;
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        // make sure Doctrine relations are set with empty, default Collection (i.e. ArrayCollection as used when hydrated)
        // this avoids errors 'Typed property xxx must not be accessed before initialization' in e.g. Hashable::makeContentHash
        $this->steps = new ArrayCollection();
    }

    public function importFields(string $stepsType): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->stepsType = $stepsType;
    }

    public function setStartButtonCoordinates(int $x, int $y): void
    {
        $this->updatedAt = new \DateTimeImmutable();
        $this->startX = $x;
        $this->startY = $y;
    }

    public function setType(string $type): void
    {
        $this->stepsType = $type;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Getters

    public function getStepsType(): string
    {
        return $this->stepsType;
    }

    public function getForm(): Form
    {
        return $this->form;
    }

    /** @return Collection<array-key, FormWorkflowStep> */
    public function getSteps(): Collection
    {
        return $this->steps;
    }

    public function getStartX(): int
    {
        return $this->startX;
    }

    public function getStartY(): int
    {
        return $this->startY;
    }
}
