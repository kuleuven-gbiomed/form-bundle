<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder;

enum Permission: string
{
    case NONE = 'NONE';
    case READ = 'READ';
    case WRITE = 'WRITE';

    public function ranking(): int
    {
        // phpcs:disable
        // fixme als deze bug opgelost is kan dit weg
        // PHPCompatibility geeft een error hier Echter dit is een bug in PHPCompatibility
        // Zie: https://github.com/PHPCompatibility/PHPCompatibility/issues/1343
        // We wachten op release 10.0.0
        return match ($this) {
            Permission::NONE => 0,
            Permission::READ => 1,
            Permission::WRITE => 2,
        };
        // phpcs:enable
    }

    /** This returns the Permission which is larger. */
    public function add(Permission $permission): Permission
    {
        // phpcs:disable
        // fixme als deze bug opgelost is kan dit weg
        // PHPCompatibility geeft een error hier Echter dit is een bug in PHPCompatibility
        // Zie: https://github.com/PHPCompatibility/PHPCompatibility/issues/1343
        // We wachten op release 10.0.0
        return $permission->ranking() > $this->ranking() ? $permission : $this;
        // phpcs:enable
    }
}
