<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateNumberProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\NumberPropertiesPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractUpdateCommand<NumberPropertiesPayload> */
class UpdateNumberPropertiesCommand extends AbstractUpdateCommand
{
    /** @var NumberPropertiesPayload */
    protected PayloadInterface $changeFrom;
    /** @var NumberPropertiesPayload */
    protected PayloadInterface $changeTo;
}
