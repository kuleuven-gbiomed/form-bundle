<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateNumberProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class UpdateNumberPropertiesCommandHandler extends CommandHandler
{
    public function __invoke(UpdateNumberPropertiesCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        // -- execute the command
        $changeTo = $command->getChangeTo();

        // -- update props config
        $propsConfig = $formNode->getPropsConfig();
        $propsConfig['min'] = $changeTo->getMin();
        $propsConfig['max'] = $changeTo->getMax();
        $propsConfig['scale'] = $changeTo->getScale();
        $propsConfig['stepInterval'] = $changeTo->getStepInterval();
        $propsConfig['showMaxAsInputAddon'] = $changeTo->isShowMaxAsInputAddon();
        $propsConfig['showMinAsInputAddon'] = $changeTo->isShowMinAsInputAddon();

        // -- update content hash and save
        $formNode->setPropsConfig($propsConfig);
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
