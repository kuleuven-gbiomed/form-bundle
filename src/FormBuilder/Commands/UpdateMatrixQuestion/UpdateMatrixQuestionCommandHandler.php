<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateMatrixQuestion;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Entity\FormNode;

final class UpdateMatrixQuestionCommandHandler extends CommandHandler
{
    public function __invoke(UpdateMatrixQuestionCommand $command): void
    {
        $matrixNode = $this->getFormNodeForCommand($command);
        $questionLabel = $this->htmlSanitizer->sanitize($command->getChangeTo()->question);

        foreach ($matrixNode->getChildren() as $row) {
            $formNode = $row->getChildren()->get($command->colIndex);
            if (!$formNode instanceof FormNode) {
                throw new \UnexpectedValueException('Expected $formNode to be a FormNode');
            }
            $info = $formNode->getInfo();

            if (0 === (is_countable($info['label']) ? count($info['label']) : 0)) {
                if (mb_strlen($questionLabel) > 0) {
                    $info['label'] = [];
                    $info['label'][$command->getLanguage()] = $questionLabel;
                }
            } else {
                $info['label'][$command->getLanguage()] = $questionLabel;
            }
            $formNode->setInfo($info);
            $formNode->makeContentHash();
        }
        $this->entityManager->flush();
    }
}
