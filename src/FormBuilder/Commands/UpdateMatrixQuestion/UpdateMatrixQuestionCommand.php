<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateMatrixQuestion;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\MatrixQuestionPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractUpdateCommand<MatrixQuestionPayload> */
class UpdateMatrixQuestionCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var MatrixQuestionPayload */
    protected PayloadInterface $changeFrom;
    /** @var MatrixQuestionPayload */
    protected PayloadInterface $changeTo;

    public function __construct(
        int $id,
        string $type,
        string $contentHash,
        string $subject,
        bool $force,
        MatrixQuestionPayload $changeFrom,
        MatrixQuestionPayload $changeTo,
        public readonly int $colIndex,
    ) {
        parent::__construct($id, $type, $contentHash, $subject, $force, $changeFrom, $changeTo);
    }
}
