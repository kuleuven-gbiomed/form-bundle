<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateCalculationProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Payloads\CalculationPropertiesPayload;
use KUL\FormBundle\FormBuilder\Entity\FormNode;

final class UpdateCalculationPropertiesCommandHandler extends CommandHandler
{
    public function __invoke(UpdateCalculationPropertiesCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $changeTo = $command->getChangeTo();
        if (!$this->isCommandValid($changeTo, $formNode->getSubType())) {
            throw new \Exception(AbstractCommand::COMMAND_NOT_VALID);
        }
        $configScoring = $formNode->getScoringConfig();

        if (FormNode::SUB_TYPE_CHOICE === $formNode->getSubType()) {
            // -- config scoring
            $configScoring['isCalculation'] = $changeTo->isCalculation();
            $configScoring['weighingType'] = $changeTo->getWeighingType();
            $configScoring['scoringParameters']['scale'] = $changeTo->getScale();
            $configScoring['scoringParameters']['maxScore'] = $changeTo->getMaxScore();
            $configScoring['scoringParameters']['minScore'] = $changeTo->getMinScore();
            $configScoring['scoringParameters']['passScore'] = $changeTo->getPassScore();
            $configScoring['scoringParameters']['roundingMode'] = $changeTo->getRoundingMode();
            $configScoring['scoringParameters']['showAutoCalculateBtn'] = $changeTo->isShowAutoCalculateBtn();
            $configScoring['scoringParameters']['allowNonAutoCalculatedValue'] = $changeTo->isAllowNonAutoCalculatedValue();
            if (!$configScoring['isCalculation']) {
                $configScoring['globalScoreCalculationDependencyConfigs'] = [];
            }
            $formNode->setScoringConfig($configScoring);
        } else {
            // -- config props
            $configProps = $formNode->getPropsConfig();
            $configProps['scale'] = $changeTo->getScale();
            $configProps['roundingMode'] = $changeTo->getRoundingMode();
            $formNode->setPropsConfig($configProps);
            // -- config scoring
            $configScoring['isCalculation'] = $changeTo->isCalculation();
            $configScoring['weighingType'] = $changeTo->getWeighingType();
            $configScoring['passScore'] = $changeTo->getPassScore();
            $configScoring['showAutoCalculateBtn'] = $changeTo->isShowAutoCalculateBtn();
            $configScoring['allowNonAutoCalculatedValue'] = $changeTo->isAllowNonAutoCalculatedValue();
            if (!$configScoring['isCalculation']) {
                $configScoring['globalScoreCalculationDependencyConfigs'] = [];
            }
            $formNode->setScoringConfig($configScoring);
        }

        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }

    private function isCommandValid(CalculationPropertiesPayload $changeTo, string $nodeType): bool
    {
        if ($changeTo->isCalculation()) {
            // weighing type is absolute or percentage
            if (!('absolute' === $changeTo->getWeighingType() || 'percentage' === $changeTo->getWeighingType())) {
                return false;
            }
            // rounding mode is 5 or 6
            if (!(1 === $changeTo->getRoundingMode() || 2 === $changeTo->getRoundingMode())) {
                return false;
            }
            // scale is 0,1,2 or 3
            if (!(0 === $changeTo->getScale()
                || 1 === $changeTo->getScale()
                || 2 === $changeTo->getScale()
                || 3 === $changeTo->getScale())) {
                return false;
            }
            // for choice types min and max score are defined and max is bigger or equal then min. score.
            if (FormNode::SUB_TYPE_CHOICE === $nodeType) {
                if (null === $changeTo->getMinScore()) {
                    return false;
                }
                if (null === $changeTo->getMaxScore()) {
                    return false;
                }
                if ($changeTo->getMinScore() > $changeTo->getMaxScore()) {
                    return false;
                }
                if ($changeTo->getPassScore() > $changeTo->getMaxScore()) {
                    return false;
                }
            }
        }

        return true;
    }
}
