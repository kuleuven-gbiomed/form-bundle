<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateCalculationProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\CalculationPropertiesPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractUpdateCommand<CalculationPropertiesPayload> */
class UpdateCalculationPropertiesCommand extends AbstractUpdateCommand
{
    /** @var CalculationPropertiesPayload */
    protected PayloadInterface $changeFrom;
    /** @var CalculationPropertiesPayload */
    protected PayloadInterface $changeTo;
}
