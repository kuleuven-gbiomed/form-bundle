<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddScore;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ScorePayload;

/** @extends AbstractAddCommand<ScorePayload> */
class AddScoreCommand extends AbstractAddCommand
{
    /** @var ScorePayload */
    protected PayloadInterface $add;
}
