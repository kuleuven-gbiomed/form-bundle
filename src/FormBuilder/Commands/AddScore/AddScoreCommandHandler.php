<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddScore;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Util\ScoreUtil;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;

final class AddScoreCommandHandler extends CommandHandler
{
    public function __invoke(AddScoreCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $add = $command->getAdd();
        $scoreNode = $this->formNodeRepository->getById($add->getScoreQuestionId());
        $calculationNode = $this->formNodeRepository->getById($add->getScoreCalculationId());

        $scoringConfig = $calculationNode->getScoringConfig();
        $scoringList = $scoringConfig['globalScoreCalculationDependencyConfigs'];
        $scoreNodeIndex = ScoreUtil::findInScoringConfig($scoreNode->getUid(), $scoringList);

        if (-1 === $scoreNodeIndex) {
            $scoringItem = [];
            $scoringItem['weighing'] = $add->getWeight();
            $scoringItem['scoreNodeUid'] = $scoreNode->getUid();
            $scoringList[] = $scoringItem;
        } else {
            $scoringList[$scoreNodeIndex]['weighing'] = $add->getWeight();
        }

        $scoringConfig['globalScoreCalculationDependencyConfigs'] = $scoringList;

        // -- touch the score node ---------------------------------------------------
        if (FormNode::SUB_TYPE_CHOICE === $scoreNode->getSubType()) {
            $scoreScoringConfig = $scoreNode->getScoringConfig();

            if (!isset($scoreScoringConfig['scoringParameters'])) {
                $scoreScoringConfig['scoringParameters'] = [];
            }
            // -- set default values if no value exists --------------------------------
            if (!isset($scoreScoringConfig['scoringParameters']['scale'])) {
                $scoreScoringConfig['scoringParameters']['scale'] = $this->getScaleFromOptions($scoreNode->getOptions()->getValues());
            }
            if (!isset($scoreScoringConfig['scoringParameters']['roundingMode'])) {
                $scoreScoringConfig['scoringParameters']['roundingMode'] = 6;
            }
            if (!isset($scoreScoringConfig['scoringParameters']['minScore'])) {
                $scoreScoringConfig['scoringParameters']['minScore'] = $this->getMinScoreFromOptions($scoreNode->getOptions()->getValues());
            }
            if (!isset($scoreScoringConfig['scoringParameters']['maxScore'])) {
                $scoreScoringConfig['scoringParameters']['maxScore'] = $this->getMaxScoreFromOptions($scoreNode->getOptions()->getValues());
            }
            $scoreNode->setScoringConfig($scoreScoringConfig);
        }
        $scoreNode->touch();
        $scoreNode->makeContentHash();
        $this->formNodeRepository->save($scoreNode);

        // -- update the calculation node with the new scoring config ----------------
        $calculationNode->setScoringConfig($scoringConfig);
        $calculationNode->makeContentHash();
        $this->formNodeRepository->save($calculationNode);
    }

    /**
     * @param FormNodeOption[] $options
     */
    private function getMaxScoreFromOptions(array $options): int
    {
        $result = 0;

        // todo
        return $result;
    }

    /**
     * @param FormNodeOption[] $options
     */
    private function getMinScoreFromOptions(array $options): int
    {
        $result = 0;

        // todo
        return $result;
    }

    /**
     * @param FormNodeOption[] $options
     */
    private function getScaleFromOptions(array $options): float
    {
        $result = 0;

        // todo
        return $result;
    }
}
