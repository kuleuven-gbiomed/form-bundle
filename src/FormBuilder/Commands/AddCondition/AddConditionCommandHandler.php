<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddCondition;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;

final class AddConditionCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws \JsonException
     * @throws EntityNotFoundException
     */
    public function __invoke(AddConditionCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $sourceNode = $this->formNodeRepository->getById($command->getAdd()->getUnlockerId());
        $targetNode = $this->formNodeRepository->getById($command->getAdd()->getLockedId());

        // -- get the option
        $option = $this->formNodeOptionRepository->getById($command->getAdd()->getOptionId());
        $unlockIds = $option->getUnlocksQuestionIds();
        $unlockIds[] = $targetNode->getUid();
        $option->setUnlockIds(array_unique($unlockIds));
        $option->makeContentHash();
        $this->formNodeOptionRepository->save($option);

        // -- touch the source node
        $sourceNode->touch();
        $sourceNode->makeContentHash();
        $this->formNodeRepository->save($sourceNode);

        // -- touch the target node
        $targetNode->touch();
        $targetNode->makeContentHash();
        $this->formNodeRepository->save($targetNode);
    }
}
