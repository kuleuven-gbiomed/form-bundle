<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddCondition;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ConditionPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractAddCommand<ConditionPayload> */
class AddConditionCommand extends AbstractAddCommand
{
    /** @var ConditionPayload */
    protected PayloadInterface $add;
}
