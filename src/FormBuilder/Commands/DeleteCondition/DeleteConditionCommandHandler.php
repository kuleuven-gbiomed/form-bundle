<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteCondition;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class DeleteConditionCommandHandler extends CommandHandler
{
    public function __invoke(DeleteConditionCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $sourceNode = $this->formNodeRepository->getById($command->getDelete()->getUnlockerId());
        $targetNode = $this->formNodeRepository->getById($command->getDelete()->getLockedId());

        // -- get the option
        $option = $this->formNodeOptionRepository->getById($command->getDelete()->getOptionId());
        $unlockIds = $option->getUnlocksQuestionIds();
        $foundIndex = 0;
        $index = 0;
        foreach ($unlockIds as $uid) {
            if ($targetNode->getUid() === $uid) {
                $foundIndex = $index;
            }
            ++$index;
        }
        // -- remove at the found index
        array_splice($unlockIds, $foundIndex, 1);
        // -- check the length of the unlockIds
        if (0 === count($unlockIds)) {
            $unlockIds = [];
        }
        $option->setUnlockIds($unlockIds);
        $option->makeContentHash();
        $this->formNodeOptionRepository->save($option);

        // -- touch the source node
        $sourceNode->touch();
        $sourceNode->makeContentHash();
        $this->formNodeRepository->save($sourceNode);

        // -- touch the target node
        $targetNode->touch();
        $targetNode->makeContentHash();
        $this->formNodeRepository->save($targetNode);
    }
}
