<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteCondition;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class DeleteMultiConditionCommandHandler extends CommandHandler
{
    public function __invoke(DeleteMultiConditionCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $sourceNode = $this->formNodeRepository->getById($command->getDelete()->getUnlockerId());
        $targetNode = $this->formNodeRepository->getById($command->getDelete()->getLockedId());

        // -- get the option
        $option = $this->formNodeOptionRepository->getById($command->getDelete()->getOptionId());
        $unlockIds = $option->getMultiUnlocksQuestionIds();
        $foundIndex = 0;
        $index = 0;
        foreach ($unlockIds as $uid) {
            if ($targetNode->getUid() === $uid) {
                $foundIndex = $index;
            }
            ++$index;
        }
        // -- remove at the found index
        array_splice($unlockIds, $foundIndex, 1);
        // -- check the length of the unlockIds
        if (0 === count($unlockIds)) {
            $unlockIds = [];
        }
        $option->setMultiUnlockIds($unlockIds);
        $option->makeContentHash();
        $this->formNodeOptionRepository->save($option);

        // -- touch the source node
        $sourceNode->touch();
        $sourceNode->makeContentHash();
        $this->formNodeRepository->save($sourceNode);

        // -- remove option uid from the multi unlock config
        $targetNode->removeMultiUnlockId($option->getUid());
        $targetNode->makeContentHash();
        $this->formNodeRepository->save($targetNode);
    }
}
