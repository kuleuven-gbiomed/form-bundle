<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteCondition;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractDeleteCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ConditionPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractDeleteCommand<ConditionPayload> */
class DeleteConditionCommand extends AbstractDeleteCommand
{
    /** @var ConditionPayload */
    protected PayloadInterface $delete;
}
