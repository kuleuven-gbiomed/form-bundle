<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddOption;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\AddOptionPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractAddCommand<AddOptionPayload> */
class AddOptionCommand extends AbstractAddCommand
{
    use LanguageTrait;

    /** @var AddOptionPayload */
    protected PayloadInterface $add;
}
