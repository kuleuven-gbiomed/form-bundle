<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddOption;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;
use Ramsey\Uuid\Uuid;

final class AddOptionCommandHandler extends CommandHandler
{
    public function __invoke(AddOptionCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        // -- determine the max sequence and find additional text option
        $maxSequence = 0;
        $additionalTextOption = null;
        foreach ($formNode->getOptions() as $option) {
            if (FormNodeOption::INPUT_TYPE_OPTION_WITH_ADDITIONAL_TEXT === $option->getInputType()) {
                /** @var FormNodeOption $additionalTextOption */
                $additionalTextOption = $option;
            }
            $maxSequence = $option->getSequence();
        }

        // -- make sure the additional text option stays the last
        $insertSequence = $maxSequence + 1;
        if ($additionalTextOption instanceof FormNodeOption) {
            $insertSequence = $maxSequence;
            $additionalTextOption->setSequence($maxSequence + 1);
            $additionalTextOption->makeContentHash();
            $this->formNodeOptionRepository->save($additionalTextOption);
        }

        // -- make a new option
        $option = new FormNodeOption(Uuid::uuid4()->toString(), $insertSequence, FormNodeOption::TYPE_QUESTION_OPTION, $formNode);

        // -- set the info and the properties
        $info = [];
        $info['label'] = [];
        $info['description'] = [];
        $info['roleDescriptions'] = [];
        $info['label'][$command->getLanguage()] = $this->htmlSanitizer->sanitize($command->getAdd()->getOption());
        $option->setInfo($info);
        $option->setScore($command->getAdd()->getScore());

        // -- save the new option
        $option->makeContentHash();
        $this->formNodeOptionRepository->save($option);

        // -- touch the parent form node
        $formNode->touch();
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
