<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteOption;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractDeleteCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\DeleteOptionPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractDeleteCommand<DeleteOptionPayload> */
class DeleteOptionCommand extends AbstractDeleteCommand
{
    /** @var DeleteOptionPayload */
    protected PayloadInterface $delete;
}
