<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteOption;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class DeleteOptionCommandHandler extends CommandHandler
{
    public function __invoke(DeleteOptionCommand $command): void
    {
        $formNodeOption = $this->getFormNodeOptionForCommand($command);

        // -- touch the parent node
        $formNode = $formNodeOption->getFormNode();
        $formNode->touch();
        $formNode->makeContentHash();

        // -- remove the option
        $this->formNodeOptionRepository->delete($formNodeOption);

        // -- renumber the sequence
        $options = $formNodeOption->getFormNode()->getOptions();
        $sequence = 0;
        foreach ($options as $option) {
            $option->setSequence($sequence);
            $option->makeContentHash();
            ++$sequence;
        }
        $this->entityManager->flush();
    }
}
