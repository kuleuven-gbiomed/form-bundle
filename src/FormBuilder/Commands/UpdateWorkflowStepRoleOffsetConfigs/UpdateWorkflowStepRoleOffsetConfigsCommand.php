<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRoleOffsetConfigs;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\UpdateWorkflowStepRoleOffsetConfigsPayload;

/** @extends AbstractUpdateCommand<UpdateWorkflowStepRoleOffsetConfigsPayload> */
class UpdateWorkflowStepRoleOffsetConfigsCommand extends AbstractUpdateCommand
{
    /** @var UpdateWorkflowStepRoleOffsetConfigsPayload */
    protected PayloadInterface $changeFrom;
    /** @var UpdateWorkflowStepRoleOffsetConfigsPayload */
    protected PayloadInterface $changeTo;
}
