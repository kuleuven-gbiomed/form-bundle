<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRoleOffsetConfigs;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\Common\Types\RoleOffsetConfig;

class UpdateWorkflowStepRoleOffsetConfigsCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws EntityNotFoundException
     */
    public function __invoke(UpdateWorkflowStepRoleOffsetConfigsCommand $command): void
    {
        $workflowStep = $this->getFormWorkflowStep($command);
        $changeTo = $command->getChangeTo();

        // -- hydrate the raw array into typed objects and convert them into form-bundle structure
        // = separating all descriptions for all the different roles
        $roleSubmitSuccessConfigs = [];
        foreach ($changeTo->getConfigs() as $configArray) {
            foreach (RoleOffsetConfig::hydrateFromCommand($configArray)->convertToFormBundleStructure() as $resultConfig) {
                $roleSubmitSuccessConfigs[] = $resultConfig;
            }
        }

        $workflowStep->updateOffsetConfig(ConfigSubject::from($changeTo->getSubject()), $roleSubmitSuccessConfigs);
        $workflowStep->makeContentHash();
        $this->formWorkflowStepRepository->save($workflowStep);
    }
}
