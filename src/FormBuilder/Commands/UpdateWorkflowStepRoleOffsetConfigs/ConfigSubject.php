<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRoleOffsetConfigs;

enum ConfigSubject: string
{
    case roleConfigsToNotifyOfStart = 'roleConfigsToNotifyOfStart';
    case roleConfigsToRemindOfOngoing = 'roleConfigsToRemindOfOngoing';
}
