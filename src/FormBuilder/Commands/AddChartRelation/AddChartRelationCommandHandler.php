<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddChartRelation;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\Util\ChartUtil;

class AddChartRelationCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws EntityNotFoundException
     * @throws \JsonException
     */
    public function __invoke(AddChartRelationCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        $add = $command->getAdd();
        $participantNode = $this->formNodeRepository->getById($add->getParticipantId());
        $chartNode = $this->formNodeRepository->getById($add->getChartId());

        $propsConfig = $chartNode->getPropsConfig();
        $questionReferences = $propsConfig['question_references'];
        $participantNodeIndex = ChartUtil::findInQuestionReferences($participantNode->getUid(), $questionReferences);

        if (-1 === $participantNodeIndex) {
            $newReference = [];
            $newReference['question_uid'] = $participantNode->getUid();
            $newReference['custom_labels'] = [];
            if ('' !== $add->getCustomLabel()) {
                $newReference['custom_labels'][$command->getLanguage()] = $add->getCustomLabel();
            }
            $questionReferences[] = $newReference;
        } else {
            $reference = $questionReferences[$participantNodeIndex];
            if (!is_array($reference['custom_labels'])) {
                $reference['custom_labels'] = [];
            }
            if ('' === $add->getCustomLabel()) {
                unset($reference['custom_labels'][$command->getLanguage()]);
            } else {
                $reference['custom_labels'][$command->getLanguage()] = $add->getCustomLabel();
            }
            $questionReferences[$participantNodeIndex] = $reference;
        }

        $propsConfig['question_references'] = $questionReferences;

        $chartNode->setPropsConfig($propsConfig);
        $chartNode->makeContentHash();
        $this->formNodeRepository->save($chartNode);

        $participantNode->touch();
        $participantNode->makeContentHash();
        $this->formNodeRepository->save($participantNode);
    }
}
