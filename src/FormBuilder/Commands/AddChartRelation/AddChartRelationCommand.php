<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddChartRelation;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ChartRelationPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractAddCommand<ChartRelationPayload> */
class AddChartRelationCommand extends AbstractAddCommand
{
    use LanguageTrait;

    /** @var ChartRelationPayload */
    protected PayloadInterface $add;
}
