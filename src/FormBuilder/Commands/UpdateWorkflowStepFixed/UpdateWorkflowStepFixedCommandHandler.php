<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepFixed;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;

class UpdateWorkflowStepFixedCommandHandler extends CommandHandler
{
    /**
     * @throws OutOfSyncException
     * @throws SubmissionsExistException
     * @throws EntityNotFoundException
     */
    public function __invoke(UpdateWorkflowStepFixedCommand $command): void
    {
        $workflowStep = $this->getFormWorkflowStep($command);
        $changeTo = $command->getChangeTo();

        // -- set the info (label) ---------------------------------------------
        $info = $workflowStep->getInfo();
        $label = $this->htmlSanitizer->sanitize($changeTo->getLabel());
        if (0 === (is_countable($info['label']) ? count($info['label']) : 0)) {
            if (mb_strlen($label) > 0) {
                $info['label'] = [];
                $info['label'][$command->getLanguage()] = $label;
            }
        } else {
            $info['label'][$command->getLanguage()] = $label;
        }

        // -- make a period -----------------------------------------------------
        $period = [];
        $start = new \DateTime($changeTo->getFixedStartDate(), new \DateTimeZone('UTC'));
        $end = new \DateTime($changeTo->getFixedEndDate(), new \DateTimeZone('UTC'));
        $start->setTimezone(new \DateTimeZone('Europe/Brussels'));
        $end->setTimezone(new \DateTimeZone('Europe/Brussels'));
        $period['fixedStartDate'] = $start->format('Y-m-d H:i:\0\0');
        $period['fixedEndDate'] = $end->format('Y-m-d H:i:\0\0');

        // -- update the properties and save ---------------------------------
        $workflowStep->setNameAndTiming(
            $info,
            $period,
            $changeTo->isAllowsStartIfPreviousStepIsFulfilled()
        );
        $workflowStep->makeContentHash();
        $this->formWorkflowStepRepository->save($workflowStep);
    }
}
