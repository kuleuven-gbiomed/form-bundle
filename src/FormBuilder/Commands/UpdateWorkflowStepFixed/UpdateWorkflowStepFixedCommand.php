<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepFixed;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\StepNameAndTimingFixedPayload;

/** @extends  AbstractUpdateCommand<StepNameAndTimingFixedPayload> */
class UpdateWorkflowStepFixedCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var StepNameAndTimingFixedPayload */
    protected PayloadInterface $changeFrom;
    /** @var StepNameAndTimingFixedPayload */
    protected PayloadInterface $changeTo;
}
