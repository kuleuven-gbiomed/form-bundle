<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateRadarChartProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\RadarChartPropertiesPayload;

/**
 * @extends AbstractUpdateCommand<RadarChartPropertiesPayload>
 *
 * @template-extends AbstractUpdateCommand<RadarChartPropertiesPayload>
 */
class UpdateRadarChartPropertiesCommand extends AbstractUpdateCommand
{
    /** @var RadarChartPropertiesPayload */
    protected PayloadInterface $changeFrom;
    /** @var RadarChartPropertiesPayload */
    protected PayloadInterface $changeTo;
}
