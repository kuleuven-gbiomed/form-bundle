<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateRadarChartProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

class UpdateRadarChartPropertiesCommandHandler extends CommandHandler
{
    public function __invoke(UpdateRadarChartPropertiesCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $changeTo = $command->getChangeTo();

        $propsConfig = $formNode->getPropsConfig();
        $propsConfig['unit_sign'] = $changeTo->getUnitSign();
        $propsConfig['hide_point_values'] = $changeTo->isHidePointValues();
        $propsConfig['hide_scale_values'] = $changeTo->isHideScaleValues();
        $propsConfig['hide_point_unit_suffix'] = $changeTo->isHidePointUnitSuffix();
        $propsConfig['hide_scale_unit_suffix'] = $changeTo->isHideScaleUnitSuffix();

        $formNode->setPropsConfig($propsConfig);
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
