<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\OrderChartRelation;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Util\ChartUtil;

class OrderChartRelationCommandHandler extends CommandHandler
{
    public function __invoke(OrderChartRelationCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        $changeTo = $command->getChangeTo();
        $propsConfig = $formNode->getPropsConfig();
        $questionReferences = $propsConfig['question_references'];
        $newQuestionReferences = [];

        foreach ($changeTo->getOrder() as $orderUid) {
            $item = [];
            $item['question_uid'] = $orderUid;
            $item['custom_labels'] = ChartUtil::getCustomLabels($orderUid, $questionReferences);
            $newQuestionReferences[] = $item;
        }
        $propsConfig['question_references'] = $newQuestionReferences;

        $formNode->setPropsConfig($propsConfig);
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
