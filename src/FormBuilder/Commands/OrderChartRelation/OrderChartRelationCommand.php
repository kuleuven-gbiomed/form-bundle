<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\OrderChartRelation;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ChartRelationOrderPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/**
 * @extends AbstractUpdateCommand<ChartRelationOrderPayload>
 *
 * @template-extends AbstractUpdateCommand<ChartRelationOrderPayload>
 */
class OrderChartRelationCommand extends AbstractUpdateCommand
{
    /** @var ChartRelationOrderPayload */
    protected PayloadInterface $changeFrom;
    /** @var ChartRelationOrderPayload */
    protected PayloadInterface $changeTo;
}
