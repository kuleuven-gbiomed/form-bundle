<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateStepOrder;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

class UpdateStepOrderCommandHandler extends CommandHandler
{
    public function __invoke(UpdateStepOrderCommand $command): void
    {
        // update the new sequence of the workflow steps

        $newOrderIds = $command->getChangeTo()->getOrder();
        $sequence = 0;

        foreach ($newOrderIds as $newOrderId) {
            $workflowStep = $this->formWorkflowStepRepository->getById($newOrderId);
            $workflowStep->setSequence($sequence);
            $workflowStep->makeContentHash();
            $this->formWorkflowStepRepository->save($workflowStep);
            ++$sequence;
        }
    }
}
