<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateStepOrder;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\StepOrderPayload;

/** @extends AbstractUpdateCommand<StepOrderPayload> */
class UpdateStepOrderCommand extends AbstractUpdateCommand
{
    /** @var StepOrderPayload */
    protected PayloadInterface $changeFrom;
    /** @var StepOrderPayload */
    protected PayloadInterface $changeTo;
}
