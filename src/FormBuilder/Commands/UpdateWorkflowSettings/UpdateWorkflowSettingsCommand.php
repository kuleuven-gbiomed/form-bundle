<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowSettings;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\WorkflowSettingsPayload;

/** @extends AbstractUpdateCommand<WorkflowSettingsPayload> */
class UpdateWorkflowSettingsCommand extends AbstractUpdateCommand
{
    /** @var WorkflowSettingsPayload */
    protected PayloadInterface $changeFrom;
    /** @var WorkflowSettingsPayload */
    protected PayloadInterface $changeTo;
}
