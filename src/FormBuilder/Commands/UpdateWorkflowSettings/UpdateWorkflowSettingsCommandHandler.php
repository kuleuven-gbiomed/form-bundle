<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowSettings;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;

class UpdateWorkflowSettingsCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws \JsonException
     * @throws EntityNotFoundException
     */
    public function __invoke(UpdateWorkflowSettingsCommand $command): void
    {
        $workflowStep = $this->getFormWorkflowStep($command);
        $changeTo = $command->getChangeTo();

        $workflowStep->setRolesWithOneSubmit($changeTo->getRolesWithOneSubmit());
        $workflowStep->setHideReadOnlyNodesOnLanding($changeTo->isHideReadOnlyNodesOnLanding());
        $workflowStep->setRedirectAwayOnSubmitSuccess($changeTo->isRedirectAwayOnSubmitSuccess());
        $workflowStep->makeContentHash();

        $this->formWorkflowStepRepository->save($workflowStep);
    }
}
