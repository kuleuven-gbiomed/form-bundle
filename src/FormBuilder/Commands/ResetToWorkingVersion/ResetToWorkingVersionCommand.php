<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\ResetToWorkingVersion;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractCommand;

class ResetToWorkingVersionCommand extends AbstractCommand
{
    public function __construct(
        protected readonly int $id,
        protected readonly string $type,
        protected readonly string $contentHash,
        protected readonly string $subject,
        protected readonly bool $force,
    ) {
    }
}
