<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\ResetToWorkingVersion;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Manager\DeconstructionManager;

final class ResetToWorkingVersionCommandHandler extends CommandHandler
{
    public function __invoke(ResetToWorkingVersionCommand $command): void
    {
        $form = $this->getFormForCommand($command);

        $this->entityManager->beginTransaction();
        try {
            $dm = new DeconstructionManager($this->templateRepository, $this->entityManager);
            $dm->resetForm($form);
            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->getConnection()->rollBack();
            throw $e;
        }
    }
}
