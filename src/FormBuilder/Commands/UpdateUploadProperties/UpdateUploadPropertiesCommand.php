<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateUploadProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\UploadPropertiesPayload;

/** @extends AbstractUpdateCommand<UploadPropertiesPayload> */
class UpdateUploadPropertiesCommand extends AbstractUpdateCommand
{
    /** @var UploadPropertiesPayload */
    protected PayloadInterface $changeFrom;
    /** @var UploadPropertiesPayload */
    protected PayloadInterface $changeTo;
}
