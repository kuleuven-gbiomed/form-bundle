<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateUploadProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class UpdateUploadPropertiesCommandHandler extends CommandHandler
{
    public function __invoke(UpdateUploadPropertiesCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $changeTo = $command->getChangeTo();
        $configProps = $formNode->getPropsConfig();
        $configProps['minAmount'] = $changeTo->getMinAmount();
        $configProps['maxAmount'] = $changeTo->getMaxAmount();
        $configProps['maxFileSize'] = $changeTo->getMaxFileSize();
        $configProps['mimeTypes'] = $changeTo->getMimeTypes();
        $configProps['extensions'] = $changeTo->getExtensions();
        $formNode->setPropsConfig($configProps);
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
