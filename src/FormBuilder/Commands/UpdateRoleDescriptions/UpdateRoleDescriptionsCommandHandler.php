<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateRoleDescriptions;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\Common\Types\RoleDescription;

class UpdateRoleDescriptionsCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws \JsonException
     * @throws EntityNotFoundException
     */
    public function __invoke(UpdateRoleDescriptionsCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $changeTo = $command->getChangeTo();
        $info = $formNode->getInfo();
        $roleDescriptions = [];
        foreach ($changeTo->getDescriptions() as $descriptionArray) {
            foreach (RoleDescription::hydrateFromCommand($descriptionArray)->sanitizeAndConvertToFormBundleStructure($this->htmlSanitizer) as $resultDescription) {
                $roleDescriptions[] = $resultDescription;
            }
        }
        $info['roleDescriptions'] = $roleDescriptions;
        $formNode->setInfo($info);
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
