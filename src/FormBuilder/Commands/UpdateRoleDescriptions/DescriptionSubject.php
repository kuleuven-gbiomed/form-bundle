<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateRoleDescriptions;

enum DescriptionSubject: string
{
    case roleDescriptions = 'roleDescriptions';
}
