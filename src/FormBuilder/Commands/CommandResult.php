<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands;

class CommandResult implements \JsonSerializable
{
    private function __construct(
        private readonly bool $result,
        private readonly string $title,
        private readonly string $message,
    ) {
    }

    public static function make(bool $result, string $title, string $message): self
    {
        return new self($result, $title, $message);
    }

    // ---------------------------------------------------------------------------
    // what to render as json
    // ---------------------------------------------------------------------------

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->result = $this->result;
        $json->title = $this->title;
        $json->message = $this->message;

        return $json;
    }

    public function getResult(): bool
    {
        return $this->result;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
