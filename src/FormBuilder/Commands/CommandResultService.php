<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandInterface;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\CommandFailedException;
use KUL\FormBundle\FormBuilder\Utility\FormBuilderUser;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Contracts\Translation\TranslatorInterface;

class CommandResultService implements CommandResultServiceInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly TranslatorInterface $translator,
        private readonly CommandDispatcherService $commandDispatcherService,
    ) {
    }

    private function getCommandResult(?CommandInterface $command, FormBuilderUser $user): CommandResult
    {
        if (!$command instanceof CommandInterface) {
            return CommandResult::make(false, 'invalidCommand.title', 'invalidCommand.message');
        }

        try {
            $command->setUser($user);
            $this->commandDispatcherService->dispatch($command);
            $commandType = $command->getType();
            $result = CommandResult::make(true, "{$commandType}.success.title", "{$commandType}.success.message");
        } catch (HandlerFailedException $handlerFailedException) {
            $exception = $handlerFailedException->getPrevious();
            if ($exception instanceof CommandFailedException) {
                $this->logger->critical(
                    $exception::class.': '.$exception->getMessage()
                );
                $result = CommandResult::make(false, $exception->getTitleForUser(), $exception->getMessageForUser());
            } else {
                $this->logger->critical(
                    HandlerFailedException::class.': error thrown without previous exception'
                );
                $result = CommandResult::make(false, 'generalError.title', 'generalError.message');
            }
        }

        return $result;
    }

    public function getTranslatedCommandResult(?CommandInterface $command, FormBuilderUser $user): CommandResult
    {
        $result = $this->getCommandResult($command, $user);

        return CommandResult::make(
            $result->getResult(),
            $this->translate($result->getTitle()),
            $this->translate($result->getMessage()),
        );
    }

    private function translate(string $text): string
    {
        return $this->translator->trans($text, domain: 'commandResult');
    }
}
