<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateDisplayCategory;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class UpdateDisplayCategoryCommandHandler extends CommandHandler
{
    public function __invoke(UpdateDisplayCategoryCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        // -------------------------------------------------------------------- update the hidden flag for this category
        $formNode->setHidden($command->getChangeTo()->isHidden());
        $formNode->makeContentHash();
        $formNode = $this->formNodeRepository->save($formNode);

        // -------------------------------------------------------------------- set this hidden flag to all the children
        foreach ($formNode->getAllChildrenIds() as $childId) {
            $formNodeChild = $this->formNodeRepository->getById($childId);
            $formNodeChild->setHidden($command->getChangeTo()->isHidden());
            $formNodeChild->makeContentHash();
            $this->formNodeRepository->save($formNodeChild);
        }
    }
}
