<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateDisplayCategory;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\DisplayPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractUpdateCommand<DisplayPayload> */
class UpdateDisplayCategoryCommand extends AbstractUpdateCommand
{
    /** @var DisplayPayload */
    protected PayloadInterface $changeFrom;
    /** @var DisplayPayload */
    protected PayloadInterface $changeTo;
}
