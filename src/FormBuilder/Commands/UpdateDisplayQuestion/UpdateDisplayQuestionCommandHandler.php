<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateDisplayQuestion;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class UpdateDisplayQuestionCommandHandler extends CommandHandler
{
    public function __invoke(UpdateDisplayQuestionCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        $formNode->setHidden($command->getChangeTo()->isHidden());
        $formNode->makeContentHash();
        $formNode = $this->formNodeRepository->save($formNode);
        if (!$command->getChangeTo()->isHidden()) {
            foreach ($formNode->getAllParentIds() as $parentId) {
                $parentFormNode = $this->formNodeRepository->getById($parentId);
                $parentFormNode->setHidden(false);
                $parentFormNode->makeContentHash();
                $this->formNodeRepository->save($parentFormNode);
            }
        }
    }
}
