<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepText;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;

class UpdateWorkflowStepTextCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws EntityNotFoundException
     * @throws \JsonException
     */
    public function __invoke(UpdateWorkflowStepTextCommand $command): void
    {
        $workflowStep = $this->getFormWorkflowStep($command);
        $changeTo = $command->getChangeTo();
        $workflowStep->updateSingleText(
            DescriptionSubject::from($changeTo->getSubject()),
            strtolower($command->getLanguage()),
            $this->htmlSanitizer->sanitize($changeTo->getDescription())
        );
        $workflowStep->makeContentHash();
        $this->formWorkflowStepRepository->save($workflowStep);
    }
}
