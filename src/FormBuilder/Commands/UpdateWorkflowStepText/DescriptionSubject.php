<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepText;

enum DescriptionSubject: string
{
    // -- single editor texts
    case generalSubmitSuccessMessage = 'generalSubmitSuccessMessage';
    case mailBodyForStart = 'mailBodyForStart';
    case mailBodyForOngoing = 'mailBodyForOngoing';
}
