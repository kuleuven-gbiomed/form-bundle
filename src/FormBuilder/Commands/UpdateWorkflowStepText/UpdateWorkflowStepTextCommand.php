<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepText;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\UpdateWorkflowStepTextPayload;

/** @extends AbstractUpdateCommand<UpdateWorkflowStepTextPayload> */
class UpdateWorkflowStepTextCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var UpdateWorkflowStepTextPayload */
    protected PayloadInterface $changeFrom;
    /** @var UpdateWorkflowStepTextPayload */
    protected PayloadInterface $changeTo;
}
