<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common;

class CommandTypes
{
    public const SKIPPED_SUBMISSIONS_CHECK_COMMANDS = [
        'updateFormInfo',
        'updateCategoryInfo',
        'updateQuestionInfo',
        self::UPDATE_OPTION,
        'updateRoleDescriptions',
        'updateMatrixQuestion',
        'updateDisplayOptions',
    ];
    public const PUBLISH_FORM = 'publishForm';
    public const UPDATE_OPTION = 'updateOption';
    public const RESET_TO_WORKING_VERSION = 'resetToWorkingVersion';
}
