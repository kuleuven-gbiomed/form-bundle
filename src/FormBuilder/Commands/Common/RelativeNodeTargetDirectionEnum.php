<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common;

enum RelativeNodeTargetDirectionEnum: string
{
    case IN = 'in';
    case ABOVE = 'above';
    case BELOW = 'below';
}
