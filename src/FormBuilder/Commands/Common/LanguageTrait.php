<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common;

trait LanguageTrait
{
    private string $language = '';

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }
}
