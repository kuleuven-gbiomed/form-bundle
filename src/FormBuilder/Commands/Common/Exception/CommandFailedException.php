<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common\Exception;

interface CommandFailedException
{
    public function getTitleForUser(): string;

    public function getMessageForUser(): string;
}
