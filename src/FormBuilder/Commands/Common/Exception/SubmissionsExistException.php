<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common\Exception;

class SubmissionsExistException extends \Exception implements CommandFailedException
{
    public function getTitleForUser(): string
    {
        return 'submissionsExist.title';
    }

    public function getMessageForUser(): string
    {
        return 'submissionsExist.message';
    }
}
