<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common\Exception;

use KUL\FormBundle\FormBuilder\Commands\Common\OutOfSync;

class OutOfSyncException extends \Exception implements CommandFailedException
{
    public function getTitleForUser(): string
    {
        return OutOfSync::TITLE;
    }

    public function getMessageForUser(): string
    {
        return OutOfSync::MESSAGE;
    }
}
