<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common\Exception;

class CannotDeleteLastEditableNodeException extends \Exception implements CommandFailedException
{
    public function getTitleForUser(): string
    {
        return 'Delete failed';
    }

    public function getMessageForUser(): string
    {
        return 'Cannot delete last editable node.';
    }
}
