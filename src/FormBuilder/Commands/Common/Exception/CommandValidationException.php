<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common\Exception;

class CommandValidationException extends \Exception implements CommandFailedException
{
    public function getTitleForUser(): string
    {
        return 'validationFailed.title';
    }

    public function getMessageForUser(): string
    {
        return '' === $this->message ? 'validationFailed.defaultMessage' : 'validationFailed.'.$this->message;
    }
}
