<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common;

use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/**
 * @template T of PayloadInterface
 */
abstract class AbstractDeleteCommand extends AbstractCommand
{
    /**
     * child classes overwrite this property and specify implemented Payload class
     * in docblock for the Symfony Serializer.
     *
     * @psalm-var T
     */
    protected PayloadInterface $delete;

    /**
     * @psalm-param T $delete
     */
    public function __construct(
        protected readonly int $id,
        protected readonly string $type,
        protected readonly string $contentHash,
        protected readonly string $subject,
        protected readonly bool $force,
        PayloadInterface $delete,
    ) {
        $this->delete = $delete; // psalm complains if you use constructor promotion here
    }

    /** @return T */
    public function getDelete(): PayloadInterface
    {
        return $this->delete;
    }
}
