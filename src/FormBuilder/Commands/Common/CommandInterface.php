<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common;

use KUL\FormBundle\FormBuilder\Utility\FormBuilderUser;

interface CommandInterface
{
    public function getId(): int;

    public function getType(): string;

    public function getContentHash(): string;

    public function isContentHashValid(string $sourceHash): bool;

    public function getSubject(): string;

    public function setUser(FormBuilderUser $user): void;

    public function getUser(): FormBuilderUser;

    public function isForce(): bool;
}
