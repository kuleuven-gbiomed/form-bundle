<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common;

use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/**
 * @template T of PayloadInterface
 */
abstract class AbstractUpdateCommand extends AbstractCommand
{
    /**
     * child classes overwrite this property and specify implemented Payload class
     * in docblock for the Symfony Serializer.
     *
     * @psalm-var T
     */
    protected PayloadInterface $changeFrom;

    /**
     * child classes overwrite this property and specify implemented Payload class
     * in docblock for the Symfony Serializer.
     *
     * @psalm-var T
     */
    protected PayloadInterface $changeTo;

    /**
     * @psalm-param T $changeFrom
     * @psalm-param T $changeTo
     */
    public function __construct(
        protected readonly int $id,
        protected readonly string $type,
        protected readonly string $contentHash,
        protected readonly string $subject,
        protected readonly bool $force,
        PayloadInterface $changeFrom,
        PayloadInterface $changeTo,
    ) {
        $this->changeFrom = $changeFrom;    // psalm complains if you use constructor promotion here
        $this->changeTo = $changeTo;        // psalm complains if you use constructor promotion here
    }

    /** @return T */
    public function getChangeFrom(): PayloadInterface
    {
        return $this->changeFrom;
    }

    /** @return T */
    public function getChangeTo(): PayloadInterface
    {
        return $this->changeTo;
    }
}
