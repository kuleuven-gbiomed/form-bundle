<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common;

class CommandSubject
{
    final public const NODE = 'node';
    final public const OPTION = 'option';
    final public const FORM = 'form';
    final public const FORM_INFO = 'formInfo';
    final public const PERMISSION_TEMPLATE = 'permissionTemplate';
    final public const WORKFLOW = 'workflow';
    final public const WORKFLOW_STEP = 'step';
}
