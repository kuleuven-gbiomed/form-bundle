<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common;

use KUL\FormBundle\FormBuilder\Commands\AddChartRelation\AddChartRelationCommand;
use KUL\FormBundle\FormBuilder\Commands\AddCondition\AddConditionCommand;
use KUL\FormBundle\FormBuilder\Commands\AddMatrix\AddMatrixCommand;
use KUL\FormBundle\FormBuilder\Commands\AddMatrixQuestion\AddMatrixQuestionCommand;
use KUL\FormBundle\FormBuilder\Commands\AddNode\AddNodeCommand;
use KUL\FormBundle\FormBuilder\Commands\AddNodeCopy\AddNodeCopyCommand;
use KUL\FormBundle\FormBuilder\Commands\AddNodeMove\AddNodeMoveCommand;
use KUL\FormBundle\FormBuilder\Commands\AddOption\AddOptionCommand;
use KUL\FormBundle\FormBuilder\Commands\AddPermissionsTemplate\AddPermissionsTemplateCommand;
use KUL\FormBundle\FormBuilder\Commands\AddPrefillSource\AddPrefillSourceCommand;
use KUL\FormBundle\FormBuilder\Commands\AddScore\AddScoreCommand;
use KUL\FormBundle\FormBuilder\Commands\AddWorkflowStep\AddWorkflowStepCommand;
use KUL\FormBundle\FormBuilder\Commands\ApplyPermissionsToCategory\ApplyPermissionsToCategoryCommand;
use KUL\FormBundle\FormBuilder\Commands\DeleteChartRelation\DeleteChartRelationCommand;
use KUL\FormBundle\FormBuilder\Commands\DeleteCondition\DeleteConditionCommand;
use KUL\FormBundle\FormBuilder\Commands\DeleteCondition\DeleteMultiConditionCommand;
use KUL\FormBundle\FormBuilder\Commands\DeleteMatrixQuestion\DeleteMatrixQuestionCommand;
use KUL\FormBundle\FormBuilder\Commands\DeleteNode\DeleteNodeCommand;
use KUL\FormBundle\FormBuilder\Commands\DeleteOption\DeleteOptionCommand;
use KUL\FormBundle\FormBuilder\Commands\DeletePermissionsTemplate\DeletePermissionsTemplateCommand;
use KUL\FormBundle\FormBuilder\Commands\DeletePrefillSource\DeletePrefillSourceCommand;
use KUL\FormBundle\FormBuilder\Commands\DeleteScore\DeleteScoreCommand;
use KUL\FormBundle\FormBuilder\Commands\DeleteWorkflowStep\DeleteWorkflowStepCommand;
use KUL\FormBundle\FormBuilder\Commands\ImportOptions\ImportOptionsCommand;
use KUL\FormBundle\FormBuilder\Commands\OrderChartRelation\OrderChartRelationCommand;
use KUL\FormBundle\FormBuilder\Commands\PublishForm\PublishFormCommand;
use KUL\FormBundle\FormBuilder\Commands\ResetToWorkingVersion\ResetToWorkingVersionCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateCalculationProperties\UpdateCalculationPropertiesCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateCategoryInfo\UpdateCategoryInfoCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateChartRelation\UpdateChartRelationCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateChoiceProperties\UpdateChoicePropertiesCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateDateProperties\UpdateDatePropertiesCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateDisplayCategory\UpdateDisplayCategoryCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateDisplayOptions\UpdateDisplayOptionsCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateDisplayQuestion\UpdateDisplayQuestionCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateFormInfo\UpdateFormInfoCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateInfoRoleDescriptions\UpdateInfoRoleDescriptionsCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateMatrixColOrder\UpdateMatrixColOrderCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateMatrixQuestion\UpdateMatrixQuestionCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateMatrixRowOrder\UpdateMatrixRowOrderCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateNumberProperties\UpdateNumberPropertiesCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateOption\UpdateOptionCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateOptionSort\UpdateOptionSortCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdatePermissions\UpdatePermissionsCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdatePermissionsTemplate\UpdatePermissionsTemplateCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateQuestionInfo\UpdateQuestionInfoCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateQuestionRequired\UpdateQuestionRequiredCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateRadarChartProperties\UpdateRadarChartPropertiesCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateRoleDescriptions\UpdateRoleDescriptionsCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateScore\UpdateScoreCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateScoreProperties\UpdateScorePropertiesCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateStepOrder\UpdateStepOrderCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateTextProperties\UpdateTextPropertiesCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateUnlockingOptions\UpdateMultiUnlockingOptionsCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateUnlockingOptions\UpdateUnlockingOptionsCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateUploadProperties\UpdateUploadPropertiesCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowSchema\UpdateWorkflowSchemaCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowSettings\UpdateWorkflowSettingsCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepFixed\UpdateWorkflowStepFixedCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRelative\UpdateWorkflowStepRelativeCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRoleOffsetConfigs\UpdateWorkflowStepRoleOffsetConfigsCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRoleTexts\UpdateWorkflowStepRoleTextsCommand;
use KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepText\UpdateWorkflowStepTextCommand;
use KUL\FormBundle\FormBuilder\Utility\FormBuilderUser;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;

#[DiscriminatorMap(
    typeProperty: 'type',
    mapping: [
        'updateDisplayQuestion' => UpdateDisplayQuestionCommand::class,
        'updateQuestionRequired' => UpdateQuestionRequiredCommand::class,
        'updateCategoryInfo' => UpdateCategoryInfoCommand::class,
        'updateQuestionInfo' => UpdateQuestionInfoCommand::class,
        'updateDisplayCategory' => UpdateDisplayCategoryCommand::class,
        'updatePermissions' => UpdatePermissionsCommand::class,
        'updatePermissionsTemplate' => UpdatePermissionsTemplateCommand::class,
        'addPermissionsTemplate' => AddPermissionsTemplateCommand::class,
        'deletePermissionsTemplate' => DeletePermissionsTemplateCommand::class,
        'applyPermissionsToCategory' => ApplyPermissionsToCategoryCommand::class,
        'updateOption' => UpdateOptionCommand::class,
        'addOption' => AddOptionCommand::class,
        'deleteOption' => DeleteOptionCommand::class,
        'updateOptionSort' => UpdateOptionSortCommand::class,
        'addCondition' => AddConditionCommand::class,
        'deleteCondition' => DeleteConditionCommand::class,
        'deleteMultiCondition' => DeleteMultiConditionCommand::class,
        'updateChoiceProperties' => UpdateChoicePropertiesCommand::class,
        'updateTextProperties' => UpdateTextPropertiesCommand::class,
        'updateNumberProperties' => UpdateNumberPropertiesCommand::class,
        'updateFormInfo' => UpdateFormInfoCommand::class,
        'updateCalculationProperties' => UpdateCalculationPropertiesCommand::class,
        'addScore' => AddScoreCommand::class,
        'updateScore' => UpdateScoreCommand::class,
        'deleteScore' => DeleteScoreCommand::class,
        'updateScoreProperties' => UpdateScorePropertiesCommand::class,
        'addChartRelation' => AddChartRelationCommand::class,
        'deleteChartRelation' => DeleteChartRelationCommand::class,
        'updateChartRelation' => UpdateChartRelationCommand::class,
        'updateRadarChartProperties' => UpdateRadarChartPropertiesCommand::class,
        'orderChartRelation' => OrderChartRelationCommand::class,
        'updateUploadProperties' => UpdateUploadPropertiesCommand::class,
        'updateDateProperties' => UpdateDatePropertiesCommand::class,
        'addPrefillSource' => AddPrefillSourceCommand::class,
        'deletePrefillSource' => DeletePrefillSourceCommand::class,
        'addNode' => AddNodeCommand::class,
        'addNodeMove' => AddNodeMoveCommand::class,
        'addNodeCopy' => AddNodeCopyCommand::class,
        'addCategoryCopy' => AddNodeCopyCommand::class,
        'deleteNode' => DeleteNodeCommand::class,
        'publishForm' => PublishFormCommand::class,
        'importOptions' => ImportOptionsCommand::class,
        'resetToWorkingVersion' => ResetToWorkingVersionCommand::class,
        'updateWorkflowSchema' => UpdateWorkflowSchemaCommand::class,
        'addWorkflowStep' => AddWorkflowStepCommand::class,
        'updateWorkflowStepNameAndTimingRelative' => UpdateWorkflowStepRelativeCommand::class,
        'updateWorkflowStepNameAndTimingFixed' => UpdateWorkflowStepFixedCommand::class,
        'updateStepOrder' => UpdateStepOrderCommand::class,
        'deleteWorkFlowStep' => DeleteWorkflowStepCommand::class,
        'updateWorkflowSettings' => UpdateWorkflowSettingsCommand::class,
        'updateUnlockingOptions' => UpdateUnlockingOptionsCommand::class,
        'updateMultiUnlockingOptions' => UpdateMultiUnlockingOptionsCommand::class,
        'updateMatrixQuestion' => UpdateMatrixQuestionCommand::class,
        'deleteMatrixQuestion' => DeleteMatrixQuestionCommand::class,
        'updateWorkflowStepText' => UpdateWorkflowStepTextCommand::class,
        'updateWorkflowStepRoleTexts' => UpdateWorkflowStepRoleTextsCommand::class,
        'updateWorkflowStepRoleOffsetConfigs' => UpdateWorkflowStepRoleOffsetConfigsCommand::class,
        'updateRoleDescriptions' => UpdateRoleDescriptionsCommand::class,
        'updateMatrixRowOrder' => UpdateMatrixRowOrderCommand::class,
        'updateMatrixColOrder' => UpdateMatrixColOrderCommand::class,
        'updateInfoRoleDescriptions' => UpdateInfoRoleDescriptionsCommand::class,
        'addMatrixQuestion' => AddMatrixQuestionCommand::class,
        'addMatrix' => AddMatrixCommand::class,
        'updateDisplayOptions' => UpdateDisplayOptionsCommand::class,
    ]
)]
abstract class AbstractCommand implements CommandInterface
{
    final public const COMMAND_NOT_VALID = 'Command is not valid';

    /* @phpstan-ignore-next-line */
    protected readonly int $id;
    /* @phpstan-ignore-next-line */
    protected readonly string $type;
    /* @phpstan-ignore-next-line */
    protected readonly string $contentHash;
    /* @phpstan-ignore-next-line */
    protected readonly string $subject;
    /* @phpstan-ignore-next-line */
    protected readonly bool $force;

    /** This is set in @see CommandResultService */
    protected ?FormBuilderUser $user = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getContentHash(): string
    {
        return $this->contentHash;
    }

    public function isContentHashValid(string $sourceHash): bool
    {
        return $this->contentHash === $sourceHash;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getUser(): FormBuilderUser
    {
        if (null === $this->user) {
            throw new \Exception('FormBuilderUser has to be set before accessing.');
        }

        return $this->user;
    }

    public function setUser(FormBuilderUser $user): void
    {
        $this->user = $user;
    }

    public function isForce(): bool
    {
        return $this->force;
    }
}
