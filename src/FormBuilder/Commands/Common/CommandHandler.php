<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\Entity\TemplateRepository;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\PublishForm\PublishFormCommand;
use KUL\FormBundle\FormBuilder\Entity\CommandHistoryEntry;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormInfo;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermissionTemplate;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflowStep;
use KUL\FormBundle\FormBuilder\Permission;
use KUL\FormBundle\FormBuilder\Repository\CommandHistoryEntryRepository;
use KUL\FormBundle\FormBuilder\Repository\FormInfoRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionTemplateRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeOptionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowStepRepository;
use KUL\FormBundle\Service\SubmittedTemplateService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HtmlSanitizer\HtmlSanitizer;
use Symfony\Component\HtmlSanitizer\HtmlSanitizerConfig;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
abstract class CommandHandler
{
    protected FormNodeRepository $formNodeRepository;
    protected FormRepository $formRepository;
    protected FormNodeOptionRepository $formNodeOptionRepository;
    protected FormInfoRepository $formInfoRepository;
    protected FormWorkflowRepository $formWorkflowRepository;
    protected FormWorkflowStepRepository $formWorkflowStepRepository;
    protected FormNodeFlowPermissionRepository $formNodeFlowPermissionRepository;
    protected FormNodeFlowPermissionTemplateRepository $permissionTemplateRepo;
    protected CommandHistoryEntryRepository $historyRepo;
    protected HtmlSanitizer $htmlSanitizer;

    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected SubmittedTemplateService $submittedTemplateService,
        protected TemplateRepository $templateRepository,
        protected ParameterBagInterface $params,
    ) {
        $this->formNodeRepository = $this->entityManager->getRepository(FormNode::class);
        $this->formRepository = $this->entityManager->getRepository(Form::class);
        $this->formNodeOptionRepository = $this->entityManager->getRepository(FormNodeOption::class);
        $this->formInfoRepository = $this->entityManager->getRepository(FormInfo::class);
        $this->formWorkflowRepository = $this->entityManager->getRepository(FormWorkflow::class);
        $this->formWorkflowStepRepository = $this->entityManager->getRepository(FormWorkflowStep::class);
        $this->formNodeFlowPermissionRepository = $this->entityManager->getRepository(FormNodeFlowPermission::class);
        $this->permissionTemplateRepo = $this->entityManager->getRepository(FormNodeFlowPermissionTemplate::class);
        $this->historyRepo = $entityManager->getRepository(CommandHistoryEntry::class);

        $this->htmlSanitizer = new HtmlSanitizer(
            (new HtmlSanitizerConfig())->allowSafeElements()
        );
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Check if command is allowed
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     */
    protected function checkCommandAllowed(Form $form, CommandInterface $command, string $contentHash): void
    {
        // check if the form has already submissions (skipped for certain label commands, see below)
        if ($this->isSubmissionsCheck($form, $command) && $this->formHasSubmissions($form)) {
            throw new SubmissionsExistException();
        }
        // check if the content hash matches with the hash in the command, to check synchronisation
        if (!$command->isContentHashValid($contentHash)) {
            throw new OutOfSyncException();
        }
    }

    protected function isSubmissionsCheck(Form $form, CommandInterface $command): bool
    {
        // for certain commands we don't check if the form already contains submissions
        if (in_array($command->getType(), CommandTypes::SKIPPED_SUBMISSIONS_CHECK_COMMANDS, true)) {
            if (CommandTypes::UPDATE_OPTION === $command->getType()) {
                // for the update option command, check if only the labels are changed,
                // or check if the other field haven't changed
                /**
                 * @psalm-suppress UndefinedInterfaceMethod
                 *
                 * @phpstan-ignore-next-line
                 */
                $changeFrom = $command->getChangeFrom();
                /**
                 * @psalm-suppress UndefinedInterfaceMethod
                 *
                 * @phpstan-ignore-next-line
                 */
                $changeTo = $command->getChangeTo();

                if ($changeFrom->getScore() === $changeTo->getScore()
                    && $changeFrom->isDefaultSelected() === $changeTo->isDefaultSelected()
                    && $changeFrom->isAdditionalTextRequired() === $changeTo->isAdditionalTextRequired()
                    && $changeFrom->getAdditionalTextMinLength() === $changeTo->getAdditionalTextMinLength()
                    && $changeFrom->getAdditionalTextMaxLength() === $changeTo->getAdditionalTextMaxLength()
                ) {
                    return false;
                }
            } else {
                return false;
            }
        }

        if ($command instanceof PublishFormCommand) {
            // when the history of commands only contains label updates when publishing a form,
            // we can skip the submissions check when publishing a form
            if ($this->historyLogsOnlyContainsLabelChanges($form)) {
                return false;
            }
        }

        // when given permission and force is set on a command we don't check for submissions
        // and the changes can be published under the same version number
        // please only use carefully and give this permission only to Devs/Admins
        // SCONE-7138
        if ($command->isForce() && Permission::WRITE === $command->getUser()->getPermissions()['allowForcePublishOnFormWithSubmissions']) {
            return false;
        }

        return true;
    }

    protected function formHasSubmissions(Form $form): bool
    {
        return $this->submittedTemplateService->hasSubmissionsForTemplate($form->getUid());
    }

    protected function historyLogsOnlyContainsLabelChanges(Form $form): bool
    {
        $history = $this->historyRepo->getByFormUid($form->getUid());
        foreach ($history as $historyListItem) {
            if (CommandTypes::PUBLISH_FORM === $historyListItem->getType()
                || CommandTypes::RESET_TO_WORKING_VERSION === $historyListItem->getType()) {
                break;
            }
            if (false === in_array($historyListItem->getType(), CommandTypes::SKIPPED_SUBMISSIONS_CHECK_COMMANDS, true)) {
                return false;
            }
        }

        return true;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Functions that retrieve data and check if the command is allowed (submissions & contentHash check)
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * @throws OutOfSyncException
     * @throws SubmissionsExistException
     * @throws EntityNotFoundException
     */
    protected function getFormNodeForCommand(CommandInterface $command): FormNode
    {
        $formNode = $this->formNodeRepository->getById($command->getId());
        $this->checkCommandAllowed($formNode->getForm(), $command, $formNode->getContentHash());

        return $formNode;
    }

    /**
     * @throws OutOfSyncException
     * @throws SubmissionsExistException
     * @throws EntityNotFoundException
     */
    protected function getFormNodeOptionForCommand(CommandInterface $command): FormNodeOption
    {
        $formNodeOption = $this->formNodeOptionRepository->getById($command->getId());
        $this->checkCommandAllowed($formNodeOption->getFormNode()->getForm(), $command, $formNodeOption->getContentHash());

        return $formNodeOption;
    }

    /**
     * @throws OutOfSyncException
     * @throws SubmissionsExistException
     * @throws EntityNotFoundException
     */
    protected function getFormForCommand(CommandInterface $command): Form
    {
        $form = $this->formRepository->getById($command->getId());
        $this->checkCommandAllowed($form, $command, $form->getContentHash());

        return $form;
    }

    /**
     * @throws OutOfSyncException
     * @throws SubmissionsExistException
     * @throws EntityNotFoundException
     */
    protected function getFormInfoForCommand(CommandInterface $command): FormInfo
    {
        $form = $this->formRepository->getById($command->getId());
        $formInfo = $this->formInfoRepository->getByForm($form);
        $this->checkCommandAllowed($form, $command, $formInfo->getContentHash());

        return $formInfo;
    }

    /**
     * @throws OutOfSyncException
     * @throws SubmissionsExistException
     * @throws EntityNotFoundException
     */
    protected function getFormWorkflowStep(CommandInterface $command): FormWorkflowStep
    {
        $workflowStep = $this->formWorkflowStepRepository->getById($command->getId());
        $form = $workflowStep->getFormWorkflow()->getForm();
        $this->checkCommandAllowed($form, $command, $workflowStep->getContentHash());

        return $workflowStep;
    }
}
