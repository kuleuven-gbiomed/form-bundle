<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common;

use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/**
 * @template T of PayloadInterface
 */
abstract class AbstractPayloadCommand extends AbstractCommand
{
    /**
     * child classes overwrite this property and specify implemented Payload class in docblock for the Symfony Serializer.
     *
     * @psalm-var T
     */
    protected PayloadInterface $payload;

    /**
     * @psalm-param T $payload
     */
    public function __construct(
        protected readonly int $id,
        protected readonly string $type,
        protected readonly string $contentHash,
        protected readonly string $subject,
        protected readonly bool $force,
        PayloadInterface $payload,
    ) {
        $this->payload = $payload; // psalm complains if you use constructor promotion here
    }

    /** @return T */
    public function getPayload(): PayloadInterface
    {
        return $this->payload;
    }
}
