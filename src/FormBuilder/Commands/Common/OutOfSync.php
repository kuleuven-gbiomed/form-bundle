<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common;

class OutOfSync
{
    final public const TITLE = 'outOfSync.title';
    final public const MESSAGE = 'outOfSync.message';
}
