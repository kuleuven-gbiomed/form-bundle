<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common\Types;

class RoleOffsetConfig
{
    // ——————————————————————————————————————————————————————————————————————————
    // Constructor
    // ——————————————————————————————————————————————————————————————————————————

    private function __construct(
        protected array $roles,
        protected int $offset,
    ) {
    }

    // ——————————————————————————————————————————————————————————————————————————
    // Hydration
    // ——————————————————————————————————————————————————————————————————————————

    public static function hydrateFromCommand(array $configArray): self
    {
        $typedRoles = [];
        foreach ($configArray['roles'] as $roleArray) {
            $typedRoles[] = new Role($roleArray['role'], $roleArray['label']);
        }

        return new self($typedRoles, (int) $configArray['offset']);
    }

    // ——————————————————————————————————————————————————————————————————————————
    // Conversion
    // ——————————————————————————————————————————————————————————————————————————

    public function convertToFormBundleStructure(): array
    {
        $result = [];
        /** @var Role $role */
        foreach ($this->roles as $role) {
            $config = [];
            $config['role'] = $role->getRole();
            $config['offset'] = $this->offset;
            $result[] = $config;
        }

        return $result;
    }

    // ——————————————————————————————————————————————————————————————————————————
    // Getters
    // ——————————————————————————————————————————————————————————————————————————

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }
}
