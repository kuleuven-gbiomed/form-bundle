<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common\Types;

class RoleDescriptionTranslation
{
    public function __construct(
        protected string $language,
        protected string $description,
    ) {
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
