<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common\Types;

class Role
{
    public function __construct(
        protected string $role,
        protected string $label,
    ) {
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getLabel(): string
    {
        return $this->label;
    }
}
