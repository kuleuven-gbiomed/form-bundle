<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common\Types;

class RoleMessageTranslation
{
    public function __construct(
        protected string $language,
        protected string $message,
    ) {
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
