<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common\Types;

use Symfony\Component\HtmlSanitizer\HtmlSanitizer;

class RoleDescription
{
    private function __construct(
        protected array $roles,
        protected array $translations,
    ) {
    }

    public static function hydrateFromCommand(array $descriptionArray): self
    {
        $typedRoles = [];
        $typedTranslations = [];

        foreach ($descriptionArray['roles'] as $roleArray) {
            $typedRoles[] = new Role($roleArray['role'], $roleArray['label']);
        }
        foreach ($descriptionArray['translations'] as $translation) {
            $typedTranslations[] = new RoleDescriptionTranslation($translation['language'], $translation['description']);
        }

        return new self($typedRoles, $typedTranslations);
    }

    public function sanitizeAndConvertToFormBundleStructure(HtmlSanitizer $htmlSanitizer): array
    {
        $result = [];
        /** @var Role $role */
        foreach ($this->roles as $role) {
            $description = [];
            $description['role'] = $role->getRole();
            $resultTranslation = [];
            /** @var RoleDescriptionTranslation $translation */
            foreach ($this->translations as $translation) {
                $resultTranslation[$translation->getLanguage()] = $htmlSanitizer->sanitize($translation->getDescription());
            }
            $description['translations'] = $resultTranslation;
            $result[] = $description;
        }

        return $result;
    }
}
