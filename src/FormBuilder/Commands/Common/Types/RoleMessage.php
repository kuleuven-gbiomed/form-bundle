<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Common\Types;

use Symfony\Component\HtmlSanitizer\HtmlSanitizer;

class RoleMessage
{
    private function __construct(
        protected array $roles,
        protected array $translations,
    ) {
    }

    public static function hydrateFromCommand(array $messageArray): self
    {
        $typedRoles = [];
        $typedTranslations = [];

        foreach ($messageArray['roles'] as $roleArray) {
            $typedRoles[] = new Role($roleArray['role'], $roleArray['label']);
        }
        foreach ($messageArray['translations'] as $translation) {
            $typedTranslations[] = new RoleMessageTranslation($translation['language'], $translation['message']);
        }

        return new self($typedRoles, $typedTranslations);
    }

    public function convertToFormBundleStructure(HtmlSanitizer $htmlSanitizer): array
    {
        $result = [];
        /** @var Role $role */
        foreach ($this->roles as $role) {
            $message = [];
            $message['role'] = $role->getRole();
            $resultTranslation = [];
            /** @var RoleMessageTranslation $translation */
            foreach ($this->translations as $translation) {
                $resultTranslation[$translation->getLanguage()] = $htmlSanitizer->sanitize($translation->getMessage());
            }
            $message['translations'] = $resultTranslation;
            $result[] = $message;
        }

        return $result;
    }
}
