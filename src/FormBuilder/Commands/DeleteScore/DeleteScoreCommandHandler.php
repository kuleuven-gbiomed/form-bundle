<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteScore;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Util\ScoreUtil;
use KUL\FormBundle\FormBuilder\Entity\FormNode;

final class DeleteScoreCommandHandler extends CommandHandler
{
    public function __invoke(DeleteScoreCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $delete = $command->getDelete();
        $scoreNode = $this->formNodeRepository->getById($delete->getScoreQuestionId());
        $calculationNode = $this->formNodeRepository->getById($delete->getScoreCalculationId());

        $scoringConfig = $calculationNode->getScoringConfig();
        $scoringList = $scoringConfig['globalScoreCalculationDependencyConfigs'];
        $scoreNodeIndex = ScoreUtil::findInScoringConfig($scoreNode->getUid(), $scoringList);
        if (!(-1 === $scoreNodeIndex)) {
            array_splice($scoringList, $scoreNodeIndex, 1);
            $scoringConfig['globalScoreCalculationDependencyConfigs'] = $scoringList;
        }

        // -- reset and touch the score node -------------------------------------------
        if (FormNode::SUB_TYPE_CHOICE === $scoreNode->getSubType()) {
            $scoreScoringConfig = $scoreNode->getScoringConfig();
            $propsConfig = $scoreNode->getPropsConfig();
            $customOptions = $propsConfig['custom_options'];

            // reset scoring config if scores are not shown in options -----------------
            if (!(isset($customOptions['show_scores_in_options'])
                    && true === $customOptions['show_scores_in_options'])
            ) {
                $scoreScoringConfig['scoringParameters']['scale'] = null;
                $scoreScoringConfig['scoringParameters']['roundingMode'] = null;
                $scoreScoringConfig['scoringParameters']['minScore'] = null;
                $scoreScoringConfig['scoringParameters']['maxScore'] = null;
                $scoreNode->setScoringConfig($scoreScoringConfig);
            }
        }
        $scoreNode->touch();
        $scoreNode->makeContentHash();
        $this->formNodeRepository->save($scoreNode);

        // -- update the calculation node with the new scoring config
        $calculationNode->setScoringConfig($scoringConfig);
        $calculationNode->makeContentHash();
        $this->formNodeRepository->save($calculationNode);
    }
}
