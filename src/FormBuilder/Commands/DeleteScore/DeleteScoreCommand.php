<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteScore;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractDeleteCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ScorePayload;

/** @extends AbstractDeleteCommand<ScorePayload> */
class DeleteScoreCommand extends AbstractDeleteCommand
{
    /** @var ScorePayload */
    protected PayloadInterface $delete;
}
