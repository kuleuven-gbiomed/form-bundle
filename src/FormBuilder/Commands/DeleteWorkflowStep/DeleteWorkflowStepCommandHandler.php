<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteWorkflowStep;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\UpdatePermissions\Model\FlowPermission;
use KUL\FormBundle\FormBuilder\Commands\Util\WorkflowTypeDetector;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermissionTemplate;

class DeleteWorkflowStepCommandHandler extends CommandHandler
{
    public function __invoke(DeleteWorkflowStepCommand $command): void
    {
        // check if step and form are beloning to the same form
        $form = $this->formRepository->getById($command->getDelete()->getFormId());
        $formWorkFlow = $this->formWorkflowRepository->getByForm($form);
        $workflowStep = $this->formWorkflowStepRepository->getById($command->getDelete()->getStepId());
        if ($workflowStep->getFormWorkflow()->getForm()->getId() !== $form->getId()) {
            throw new \Exception('Form Id is not matching with the command information.');
        }

        $stepToDeleteUuid = $workflowStep->getUid();

        // remove the steps in the "kuleuven_form_fb_form_node_flow_permission" table for all the questions
        $formNodes = $this->formNodeRepository->getNodesByFormAndType($form, FormNode::TYPE_QUESTION);
        /** @var FormNode $formNode */
        foreach ($formNodes as $formNode) {
            $nodeFlowPermission = $this->formNodeFlowPermissionRepository->getByFormNode($formNode);
            $nodeFlowPermissionList = $nodeFlowPermission->getFlowPermission();
            $newNodeFlowPermissionList = [];
            // loop over the steps and add only the ones with not matching the deleted uuid
            foreach ($nodeFlowPermissionList as $nodeFlowPermissionElement) {
                if ($nodeFlowPermissionElement['stepUid'] !== $stepToDeleteUuid) {
                    $newNodeFlowPermissionList[] = $nodeFlowPermissionElement;
                }
            }
            $nodeFlowPermission->setFlowPermission($newNodeFlowPermissionList);
            $nodeFlowPermission->makeContentHash();
            $this->formNodeFlowPermissionRepository->save($nodeFlowPermission);
        }

        // remove the step for the permission templates connected to this form ----
        $permissionTemplates = $this->permissionTemplateRepo->getTemplatesByForm($form);
        /** @var FormNodeFlowPermissionTemplate $permissionTemplate */
        foreach ($permissionTemplates as $permissionTemplate) {
            $newNodeFlowPermissionList = [];
            $nodeFlowPermissionList = $permissionTemplate->getFlowPermission();
            foreach ($nodeFlowPermissionList as $nodeFlowPermissionElement) {
                if ($nodeFlowPermissionElement['stepUid'] !== $stepToDeleteUuid) {
                    $newNodeFlowPermissionList[] = $nodeFlowPermissionElement;
                }
            }
            $typedFlowPermission = FlowPermission::convertFromArray($newNodeFlowPermissionList);
            $permissionTemplate->setFlowPermission($typedFlowPermission);
            $permissionTemplate->makeContentHash();
            $this->permissionTemplateRepo->save($permissionTemplate);
        }

        // actual delete the workflow step in the database ---------------------------
        $this->formWorkflowStepRepository->delete($workflowStep);

        // renumber the sequence
        $workflowSteps = $formWorkFlow->getSteps();
        $sequence = 0;
        foreach ($workflowSteps as $step) {
            $step->setSequence($sequence);
            $step->makeContentHash();
            ++$sequence;
        }

        // determine the general type of the workflow --------------------------------
        WorkflowTypeDetector::setWorkflowType($this->formWorkflowRepository, $formWorkFlow->getId());
    }
}
