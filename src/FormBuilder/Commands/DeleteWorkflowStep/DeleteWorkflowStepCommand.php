<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteWorkflowStep;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractDeleteCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\DeleteStepPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractDeleteCommand<DeleteStepPayload> */
class DeleteWorkflowStepCommand extends AbstractDeleteCommand
{
    /** @var DeleteStepPayload */
    protected PayloadInterface $delete;
}
