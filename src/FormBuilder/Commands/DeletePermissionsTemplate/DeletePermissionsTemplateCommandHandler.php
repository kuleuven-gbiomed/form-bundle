<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeletePermissionsTemplate;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class DeletePermissionsTemplateCommandHandler extends CommandHandler
{
    public function __invoke(DeletePermissionsTemplateCommand $command): void
    {
        $form = $this->getFormForCommand($command);

        $template = $this->permissionTemplateRepo->getById($command->getDelete()->getId());
        $this->permissionTemplateRepo->delete($template);

        $form->makeContentHash();
        $this->formRepository->save($form);
    }
}
