<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeletePermissionsTemplate;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractDeleteCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\IdPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractDeleteCommand<IdPayload> */
class DeletePermissionsTemplateCommand extends AbstractDeleteCommand
{
    /** @var IdPayload */
    protected PayloadInterface $delete;
}
