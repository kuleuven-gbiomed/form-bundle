<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteMatrixQuestion;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractCommand;

class DeleteMatrixQuestionCommand extends AbstractCommand
{
    public function __construct(
        protected readonly int $id,
        protected readonly string $type,
        protected readonly string $contentHash,
        protected readonly string $subject,
        protected readonly bool $force,
        public readonly int $colIndex,
    ) {
    }
}
