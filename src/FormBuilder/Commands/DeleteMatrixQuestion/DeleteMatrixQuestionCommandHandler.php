<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteMatrixQuestion;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Manager\NodeManager;

final class DeleteMatrixQuestionCommandHandler extends CommandHandler
{
    public function __invoke(DeleteMatrixQuestionCommand $command): void
    {
        $matrixNode = $this->getFormNodeForCommand($command);
        $nodeManager = new NodeManager($this->entityManager);

        foreach ($matrixNode->getChildren() as $row) {
            $formNode = $row->getChildren()->get($command->colIndex);
            if (null !== $formNode) {
                $nodeManager->deleteNodeRecursively($formNode);
            }
        }
        $this->entityManager->flush();
    }
}
