<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\ImportOptions;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;
use Ramsey\Uuid\Uuid;

class ImportOptionsCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function __invoke(ImportOptionsCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        // -- compile the options to add ---------------------------------------
        $options = [];
        $importString = $command->getAdd()->getImportString();
        $importLines = explode("\n", $importString);
        foreach ($importLines as $importLine) {
            if ('' !== trim($importLine)) {
                $optionAr = explode('|', $importLine);
                $option = new \stdClass();
                $option->name = $this->htmlSanitizer->sanitize(trim($optionAr[0]));
                $option->language = $command->getLanguage();
                $option->score = null;
                if (count($optionAr) >= 2) {
                    $option->score = (float) $optionAr[1];
                }
                $options[] = $option;
            }
        }
        // -- determine the start sequence -----------------------------------
        $startSequence = 0;
        if ($command->getAdd()->isAppend()) {
            foreach ($formNode->getOptions() as $formNodeOption) {
                $startSequence = $formNodeOption->getSequence() + 1;
            }
        }
        // -- not append = replace -- delete options -------------------------
        if (!$command->getAdd()->isAppend()) {
            foreach ($formNode->getOptions() as $formNodeOption) {
                $this->formNodeOptionRepository->delete($formNodeOption);
            }
        }

        // -- add the options -----------------------------------------------
        $sequence = $startSequence;
        foreach ($options as $option) {
            $optionEntity = new FormNodeOption(
                Uuid::uuid4()->toString(),
                $sequence,
                FormNodeOption::TYPE_QUESTION_OPTION,
                $formNode
            );
            $info = [];
            $info['label'][$option->language] = $this->htmlSanitizer->sanitize($option->name);
            $info['description'] = [];
            $info['roleDescriptions'] = [];
            $propsConfig = [];
            $propsConfig['type'] = FormNodeOption::INPUT_TYPE_OPTION;
            $optionEntity->importFields(
                false,
                'regular',
                false,
                $info,
                false,
                false,
                'icon',
                [],
                [],
                FormNodeOption::INPUT_TYPE_OPTION,
                $propsConfig,
                $option->score
            );
            $optionEntity->makeContentHash();
            $this->formNodeOptionRepository->save($optionEntity);
            ++$sequence;
        }

        // -- touch the form-node
        $formNode->touch();
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
