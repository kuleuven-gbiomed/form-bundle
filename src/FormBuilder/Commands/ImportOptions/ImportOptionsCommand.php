<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\ImportOptions;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ImportOptionsPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractAddCommand<ImportOptionsPayload> */
class ImportOptionsCommand extends AbstractAddCommand
{
    use LanguageTrait;

    /** @var ImportOptionsPayload */
    protected PayloadInterface $add;
}
