<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\SerializerInterface;

final readonly class CommandParamConverter implements ParamConverterInterface
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $commandJSON = $request->request->get('command');
        try {
            $command = $this->serializer->deserialize($commandJSON, AbstractCommand::class, 'json');
        } catch (NotNormalizableValueException) {
            $command = null;
        }

        $request->attributes->set('command', $command);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return CommandInterface::class === $configuration->getClass();
    }
}
