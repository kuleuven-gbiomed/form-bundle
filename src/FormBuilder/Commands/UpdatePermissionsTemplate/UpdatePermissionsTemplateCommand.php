<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdatePermissionsTemplate;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PermissionTemplatePayload;

/** @extends AbstractUpdateCommand<PermissionTemplatePayload> */
class UpdatePermissionsTemplateCommand extends AbstractUpdateCommand
{
    /** @var PermissionTemplatePayload */
    protected PayloadInterface $changeFrom;
    /** @var PermissionTemplatePayload */
    protected PayloadInterface $changeTo;
}
