<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdatePermissionsTemplate;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Util\FlowPermissionConverter;

final class UpdatePermissionsTemplateCommandHandler extends CommandHandler
{
    public function __invoke(UpdatePermissionsTemplateCommand $command): void
    {
        $template = $this->permissionTemplateRepo->getById($command->getId());

        // ---------------------------------------------------------------------------------------- content hash checker
        if (!$command->isContentHashValid($template->getContentHash())) {
            throw new OutOfSyncException();
        }

        // ------------------------------------------------------------------- set the command information in the entity
        $flowPermission = FlowPermissionConverter::convertToFlowPermission($command->getChangeTo()->getFlowPermission());
        $template->setNameAndFlowPermission($command->getChangeTo()->getName(), $flowPermission);

        // ------------------------------------------------------------------------------------------- save the template
        $template->makeContentHash();
        $this->permissionTemplateRepo->save($template);
    }
}
