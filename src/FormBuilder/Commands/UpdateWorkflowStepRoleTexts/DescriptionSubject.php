<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRoleTexts;

enum DescriptionSubject: string
{
    case roleSubmitSuccessMessages = 'roleSubmitSuccessMessages';
    case roleMailBodiesForStart = 'roleMailBodiesForStart';
    case roleMailBodiesForOngoing = 'roleMailBodiesForOngoing';
}
