<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRoleTexts;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\UpdateWorkflowStepRolesTextsPayload;

/** @extends AbstractUpdateCommand<UpdateWorkflowStepRolesTextsPayload> */
class UpdateWorkflowStepRoleTextsCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var UpdateWorkflowStepRolesTextsPayload */
    protected PayloadInterface $changeFrom;
    /** @var UpdateWorkflowStepRolesTextsPayload */
    protected PayloadInterface $changeTo;
}
