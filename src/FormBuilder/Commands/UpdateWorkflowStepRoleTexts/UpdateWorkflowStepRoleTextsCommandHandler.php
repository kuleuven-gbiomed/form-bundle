<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRoleTexts;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\Common\Types\RoleMessage;

class UpdateWorkflowStepRoleTextsCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws \JsonException
     * @throws EntityNotFoundException
     */
    public function __invoke(UpdateWorkflowStepRoleTextsCommand $command): void
    {
        $workflowStep = $this->getFormWorkflowStep($command);
        $changeTo = $command->getChangeTo();

        // -- hydrate the raw array into typed objects and convert them into form-bundle structure
        // = separating all descriptions for all the different roles
        $roleSubmitSuccessMessages = [];
        foreach ($changeTo->getMessages() as $messageArray) {
            foreach (RoleMessage::hydrateFromCommand($messageArray)->convertToFormBundleStructure($this->htmlSanitizer) as $resultMessage) {
                $roleSubmitSuccessMessages[] = $resultMessage;
            }
        }

        switch ($command->getChangeTo()->getSubject()) {
            case DescriptionSubject::roleSubmitSuccessMessages->value:
                $workflowStep->setRoleSubmitSuccessMessages($roleSubmitSuccessMessages);
                break;
            case DescriptionSubject::roleMailBodiesForOngoing->value:
                $workflowStep->setRoleMailBodiesForOngoing($roleSubmitSuccessMessages);
                break;
            case DescriptionSubject::roleMailBodiesForStart->value:
                $workflowStep->setRoleMailBodiesForStart($roleSubmitSuccessMessages);
                break;
        }

        $workflowStep->makeContentHash();
        $this->formWorkflowStepRepository->save($workflowStep);
    }
}
