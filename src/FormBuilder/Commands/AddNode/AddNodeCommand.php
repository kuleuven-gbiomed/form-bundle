<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddNode;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\AddNodePayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractAddCommand<AddNodePayload> */
class AddNodeCommand extends AbstractAddCommand
{
    use LanguageTrait;

    /** @var AddNodePayload */
    protected PayloadInterface $add;
}
