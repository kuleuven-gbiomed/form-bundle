<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddNode;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\RelativeNodeTargetDirectionEnum;
use KUL\FormBundle\FormBuilder\Manager\NodeCreationManager;

class AddNodeCommandHandler extends CommandHandler
{
    public function __invoke(AddNodeCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        // -- prepare some variables --------------------------------------

        $targetNode = $formNode;
        $form = $targetNode->getForm();
        if (RelativeNodeTargetDirectionEnum::IN->value === $command->getAdd()->getTargetDirection()) {
            $parent = $formNode;
            $insertSequence = 0;
        } else {
            $parent = $formNode->getParent();
            if (RelativeNodeTargetDirectionEnum::ABOVE->value === $command->getAdd()->getTargetDirection()) {
                $insertSequence = $targetNode->getSequence();
            } else {
                $insertSequence = $targetNode->getSequence() + 1;
            }
        }

        $nodeType = $command->getAdd()->getNodeType();
        $label = $command->getAdd()->getLabel();
        $permissionTemplateId = $command->getAdd()->getPermissionTemplateId();
        $language = $command->getLanguage();

        $nodeCreationManager = new NodeCreationManager($this->entityManager);
        $nodeCreationManager->createDefaultNodeWithPermissions(
            $nodeType,
            $parent,
            $insertSequence,
            $form,
            $label,
            $language,
            $permissionTemplateId
        );
    }
}
