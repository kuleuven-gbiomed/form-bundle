<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateMatrixRowOrder;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\RowOrderPayload;

/** @extends AbstractUpdateCommand<RowOrderPayload> */
class UpdateMatrixRowOrderCommand extends AbstractUpdateCommand
{
    /** @var RowOrderPayload */
    protected PayloadInterface $changeFrom;
    /** @var RowOrderPayload */
    protected PayloadInterface $changeTo;
}
