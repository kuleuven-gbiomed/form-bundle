<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateMatrixRowOrder;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

class UpdateMatrixRowOrderCommandHandler extends CommandHandler
{
    public function __invoke(UpdateMatrixRowOrderCommand $command): void
    {
        $matrixFormNode = $this->getFormNodeForCommand($command);

        foreach ($command->getChangeTo()->rowOrder as $i => $rowId) {
            $row = $this->formNodeRepository->getById($rowId);
            $row->setSequence($i);
            $row->makeContentHash();
        }

        $matrixFormNode->touch();
        $matrixFormNode->makeContentHash();
        $this->entityManager->flush();
    }
}
