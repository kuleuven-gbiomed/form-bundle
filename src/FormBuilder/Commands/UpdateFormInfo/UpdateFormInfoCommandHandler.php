<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateFormInfo;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\CommandValidationException;

final class UpdateFormInfoCommandHandler extends CommandHandler
{
    public function __invoke(UpdateFormInfoCommand $command): void
    {
        $formInfo = $this->getFormInfoForCommand($command);
        $changeTo = $command->getChangeTo();

        if (2 !== strlen($command->getLanguage())) {
            throw new CommandValidationException('Language must be a two letter code');
        }

        if (!in_array(
            $command->getLanguage(),
            (array) $this->params->get('kul_form.translation.available_locales'),
            true
        )
        ) {
            throw new CommandValidationException('languageNotAvailable');
        }

        // ------------------------------------------------------------------------------------------------ update title
        $label = $formInfo->getLabel();
        $label[$command->getLanguage()] = $this->htmlSanitizer->sanitize($changeTo->getTitle());
        $formInfo->setLabel($label);

        // ------------------------------------------------------------------------------------------ update description
        $description = $formInfo->getDescription();
        $description[$command->getLanguage()] = $this->htmlSanitizer->sanitize($changeTo->getDescription());
        $formInfo->setDescription($description);

        $formInfo->makeContentHash();
        $this->formInfoRepository->save($formInfo);
    }
}
