<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateFormInfo;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\FormInfoPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractUpdateCommand<FormInfoPayload> */
class UpdateFormInfoCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var FormInfoPayload */
    protected PayloadInterface $changeFrom;
    /** @var FormInfoPayload */
    protected PayloadInterface $changeTo;
}
