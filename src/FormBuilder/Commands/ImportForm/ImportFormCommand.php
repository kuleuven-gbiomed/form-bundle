<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\ImportForm;

class ImportFormCommand
{
    // ----------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------

    private function __construct(
        protected readonly string $id,
        protected readonly array $workflow,
        protected readonly array $formList,
    ) {
    }

    // ----------------------------------------------------------------
    // Hydration from JSON
    // ----------------------------------------------------------------

    public static function hydrateFromJson(\stdClass $json): self
    {
        // workflow can be empty
        if ('' === trim((string) $json->workflow)) {
            $workflow = '[]';
        } else {
            $workflow = $json->workflow;
        }

        // formList can be empty
        if ('' === trim((string) $json->formList)) {
            $formList = '[]';
        } else {
            $formList = $json->formList;
        }

        return new self(
            trim((string) $json->id),
            json_decode((string) $workflow, true, 512, \JSON_THROW_ON_ERROR),
            json_decode((string) $formList, true, 512, \JSON_THROW_ON_ERROR)
        );
    }

    // ----------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------

    public function getId(): string
    {
        return $this->id;
    }

    public function getWorkflow(): array
    {
        return $this->workflow;
    }

    public function getFormList(): array
    {
        return $this->formList;
    }
}
