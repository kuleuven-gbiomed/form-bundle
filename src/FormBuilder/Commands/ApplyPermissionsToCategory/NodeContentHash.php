<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\ApplyPermissionsToCategory;

class NodeContentHash
{
    public function __construct(
        private readonly int $id,
        private readonly string $contentHash,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getContentHash(): string
    {
        return $this->contentHash;
    }
}
