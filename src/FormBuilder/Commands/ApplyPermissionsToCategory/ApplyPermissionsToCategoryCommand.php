<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\ApplyPermissionsToCategory;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractPayloadCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends  AbstractPayloadCommand<CategoryPermissionPayload> */
class ApplyPermissionsToCategoryCommand extends AbstractPayloadCommand
{
    /** @var CategoryPermissionPayload */
    protected PayloadInterface $payload;
}
