<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\ApplyPermissionsToCategory;

use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

class CategoryPermissionPayload implements PayloadInterface
{
    public function __construct(
        private readonly array $flowPermission,
        private readonly int $templateId,
        /** @var list<NodeContentHash> */
        private readonly array $nodeContentHashes,
    ) {
    }

    public function getFlowPermission(): array
    {
        return $this->flowPermission;
    }

    public function getTemplateId(): int
    {
        return $this->templateId;
    }

    public function getNodeContentHashes(): array
    {
        return $this->nodeContentHashes;
    }

    public function getContentHashesMappedToFormNodeId(): array
    {
        $result = [];
        foreach ($this->nodeContentHashes as $nodeContentHash) {
            $result[$nodeContentHash->getId()] = $nodeContentHash->getContentHash();
        }

        return $result;
    }
}
