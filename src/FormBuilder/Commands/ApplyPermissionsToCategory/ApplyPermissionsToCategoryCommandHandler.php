<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\ApplyPermissionsToCategory;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\Entity\TemplateRepository;
use KUL\FormBundle\FormBuilder\Commands\CommandDispatcherService;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PermissionPayload;
use KUL\FormBundle\FormBuilder\Commands\UpdatePermissions\UpdatePermissionsCommand;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionRepository;
use KUL\FormBundle\Service\SubmittedTemplateService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final class ApplyPermissionsToCategoryCommandHandler extends CommandHandler
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected SubmittedTemplateService $submittedTemplateService,
        protected TemplateRepository $templateRepository,
        protected CommandDispatcherService $commandDispatcher,
        protected ParameterBagInterface $params,
    ) {
        parent::__construct(
            $this->entityManager,
            $this->submittedTemplateService,
            $this->templateRepository,
            $this->params,
        );
    }

    public function __invoke(ApplyPermissionsToCategoryCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        /** @psalm-var FormNodeFlowPermissionRepository $permissionRepo */
        $permissionRepo = $this->entityManager->getRepository(FormNodeFlowPermission::class);

        $childQuestions = array_filter($formNode->getFlattenedChildren(), fn ($child) => 'question' === $child->getType());
        try {
            $this->entityManager->beginTransaction();
            foreach ($childQuestions as $child) {
                if ($child->getContentHash() !== $command->getPayload()->getContentHashesMappedToFormNodeId()[$child->getId()]) {
                    throw new OutOfSyncException();
                }
                $flowPermission = $permissionRepo->getByFormNode($child);
                $subCommand = new UpdatePermissionsCommand(
                    $child->getId(),
                    'updatePermissions',
                    $child->getContentHash(),
                    'node',
                    $command->isForce(),
                    new PermissionPayload($flowPermission->getFlowPermission(), $flowPermission->getTemplate()?->getId() ?? 0),
                    new PermissionPayload($command->getPayload()->getFlowPermission(), $command->getPayload()->getTemplateId()),
                );
                $subCommand->setUser($command->getUser());
                $this->commandDispatcher->dispatch($subCommand);
            }
            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }
        // touch the connected form node for an updated content hash
        $formNode->touch();
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
