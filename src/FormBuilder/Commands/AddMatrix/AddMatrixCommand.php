<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddMatrix;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\AddMatrixPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractAddCommand<AddMatrixPayload> */
class AddMatrixCommand extends AbstractAddCommand
{
    use LanguageTrait;

    /** @var AddMatrixPayload */
    protected PayloadInterface $add;
}
