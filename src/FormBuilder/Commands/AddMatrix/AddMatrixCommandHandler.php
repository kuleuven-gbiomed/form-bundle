<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddMatrix;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\Common\RelativeNodeTargetDirectionEnum;
use KUL\FormBundle\FormBuilder\Manager\NodeCreationManager;
use KUL\FormBundle\FormBuilder\Model\FormNode;

class AddMatrixCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws \JsonException
     * @throws EntityNotFoundException
     */
    public function __invoke(AddMatrixCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        $targetNode = $formNode;
        $form = $targetNode->getForm();
        if (RelativeNodeTargetDirectionEnum::IN->value === $command->getAdd()->targetDirection) {
            $parent = $formNode;
            $insertSequence = 0;
        } else {
            $parent = $formNode->getParent();
            if (RelativeNodeTargetDirectionEnum::ABOVE->value === $command->getAdd()->targetDirection) {
                $insertSequence = $targetNode->getSequence();
            } else {
                $insertSequence = $targetNode->getSequence() + 1;
            }
        }

        $firstQuestionNodeType = $command->getAdd()->firstQuestionType;
        $label = $command->getAdd()->label;
        $permissionTemplateId = $command->getAdd()->permissionTemplateId;
        $language = $command->getLanguage();

        $nodeCreationManager = new NodeCreationManager($this->entityManager);

        $matrixCategory = $nodeCreationManager->createDefaultNodeWithPermissions(
            FormNode::SUB_TYPE_CATEGORY,
            $parent,
            $insertSequence,
            $form,
            $label,
            $language,
            $permissionTemplateId
        );

        $matrixCategory->setDisplayMode(CategoryNode::DISPLAY_MODE_MATRIX);

        $matrixRow = $nodeCreationManager->createDefaultNodeWithPermissions(
            FormNode::SUB_TYPE_CATEGORY,
            $matrixCategory,
            0,
            $form,
            $command->getAdd()->firstRowLabel,
            $language,
            $permissionTemplateId
        );

        $nodeCreationManager->createDefaultNodeWithPermissions(
            $firstQuestionNodeType,
            $matrixRow,
            0,
            $form,
            $command->getAdd()->firstQuestionLabel,
            $language,
            $permissionTemplateId
        );
    }
}
