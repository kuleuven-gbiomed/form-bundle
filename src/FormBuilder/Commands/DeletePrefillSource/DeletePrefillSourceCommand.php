<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeletePrefillSource;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractDeleteCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PrefillSourceQuestionPayload;

/** @extends AbstractDeleteCommand<PrefillSourceQuestionPayload> */
class DeletePrefillSourceCommand extends AbstractDeleteCommand
{
    /** @var PrefillSourceQuestionPayload */
    protected PayloadInterface $delete;
}
