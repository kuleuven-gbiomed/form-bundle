<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeletePrefillSource;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

class DeletePrefillSourceCommandHandler extends CommandHandler
{
    public function __invoke(DeletePrefillSourceCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $sourceNode = $this->formNodeRepository->getById($command->getDelete()->getSourceId());

        $prefillConfig = $formNode->getPrefillConfig();

        $prefillUids = $prefillConfig[ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS];
        $indexToDelete = -1;
        $index = 0;
        foreach ($prefillUids as $prefillUid) {
            if ($prefillUid === $sourceNode->getUid()) {
                $indexToDelete = $index;
            }
            ++$index;
        }
        if (-1 !== $indexToDelete) {
            array_splice($prefillUids, $indexToDelete, 1);
        }

        $prefillConfig[ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS] = $prefillUids;
        $formNode->setPrefillConfig($prefillConfig);
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);

        $sourceNode->touch();
        $sourceNode->makeContentHash();
        $this->formNodeRepository->save($sourceNode);
    }
}
