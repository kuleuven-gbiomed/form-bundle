<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateMatrixColOrder;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractUpdateCommand<MatrixColOrderPayload> */
class UpdateMatrixColOrderCommand extends AbstractUpdateCommand
{
    /** @var MatrixColOrderPayload */
    protected PayloadInterface $changeFrom;
    /** @var MatrixColOrderPayload */
    protected PayloadInterface $changeTo;
}
