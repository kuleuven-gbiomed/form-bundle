<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateMatrixColOrder;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Entity\FormNode;

class UpdateMatrixColOrderCommandHandler extends CommandHandler
{
    public function __invoke(UpdateMatrixColOrderCommand $command): void
    {
        $matrixFormNode = $this->getFormNodeForCommand($command);

        foreach ($command->getChangeTo()->colOrder as $newIndex => $colInfo) {
            foreach ($matrixFormNode->getChildren() as $row) {
                $formNode = $row->getChildren()->get($colInfo->currentColIndex);
                if (!$formNode instanceof FormNode) {
                    throw new \UnexpectedValueException("FormNode with id:$colInfo->currentColIndex not found as child to Matrix FormNode with id:{$matrixFormNode->getId()}");
                }
                $formNode->setSequence($newIndex);
                $formNode->makeContentHash();
            }
        }

        $matrixFormNode->touch();
        $matrixFormNode->makeContentHash();
        $this->entityManager->flush();
    }
}
