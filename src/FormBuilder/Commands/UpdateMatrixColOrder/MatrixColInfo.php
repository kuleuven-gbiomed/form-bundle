<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateMatrixColOrder;

readonly class MatrixColInfo
{
    public function __construct(
        public int $currentColIndex,
        public string $label,
    ) {
    }
}
