<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateMatrixColOrder;

use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

readonly class MatrixColOrderPayload implements PayloadInterface
{
    public function __construct(
        /** @var list<MatrixColInfo> */
        public array $colOrder,
    ) {
    }
}
