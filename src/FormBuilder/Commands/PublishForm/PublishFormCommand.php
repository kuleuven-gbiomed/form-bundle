<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\PublishForm;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractCommand;

class PublishFormCommand extends AbstractCommand
{
    public function __construct(
        protected readonly int $id,
        protected readonly string $type,
        protected readonly string $subject,
        protected readonly string $contentHash,
        protected readonly bool $force,
        protected readonly bool $useNewVersionNumber,
    ) {
    }

    public function isUseNewVersionNumber(): bool
    {
        return $this->useNewVersionNumber;
    }
}
