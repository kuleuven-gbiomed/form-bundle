<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\PublishForm;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Manager\ConstructionManager;
use KUL\FormBundle\FormBuilder\Manager\DeconstructionManager;

final class PublishFormCommandHandler extends CommandHandler
{
    public function __invoke(PublishFormCommand $command): void
    {
        $form = $this->getFormForCommand($command);

        $manager = new ConstructionManager($this->templateRepository, $this->entityManager);
        $validationMessages = $manager->validateForm($form->getId());

        if (0 === count($validationMessages)) {
            $result = $manager->publishForm($form->getId(), $command->isUseNewVersionNumber());
            if ($result) {
                $deconstructionManager = new DeconstructionManager($this->templateRepository, $this->entityManager);
                // -- update the deconstructed form with a correct version number
                $deconstructionManager->updateFormAfterPublish($form->getUid());
                $this->entityManager->flush();
            }
        } else {
            throw new \Exception('Form is not valid to publish.');
        }
    }
}
