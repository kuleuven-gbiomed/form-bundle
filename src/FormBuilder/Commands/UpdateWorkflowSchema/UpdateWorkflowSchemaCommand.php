<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowSchema;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\WorkflowSchemaPayload;

/** @extends AbstractUpdateCommand<WorkflowSchemaPayload> */
class UpdateWorkflowSchemaCommand extends AbstractUpdateCommand
{
    /** @var WorkflowSchemaPayload */
    protected PayloadInterface $changeFrom;
    /** @var WorkflowSchemaPayload */
    protected PayloadInterface $changeTo;
}
