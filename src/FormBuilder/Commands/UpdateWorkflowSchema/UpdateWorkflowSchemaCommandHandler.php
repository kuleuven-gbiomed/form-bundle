<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowSchema;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

class UpdateWorkflowSchemaCommandHandler extends CommandHandler
{
    public function __invoke(UpdateWorkflowSchemaCommand $command): void
    {
        $config = $command->getChangeTo()->getConfig();
        foreach ($config as $configItem) {
            if ('add' === $configItem['id']) {
                $workflow = $this->formWorkflowRepository->getById($command->getId());
                $workflow->setStartButtonCoordinates(
                    (int) $configItem['x'],
                    (int) $configItem['y']
                );
                $workflow->makeContentHash();
                $this->formWorkflowRepository->save($workflow);
            } else {
                $workflowStepId = (int) $configItem['id'];
                $workflowStep = $this->formWorkflowStepRepository->getById($workflowStepId);
                $workflowStep->setSchemaCoordinates(
                    (int) $configItem['x'],
                    (int) $configItem['y']
                );
                $workflowStep->makeContentHash();
                $this->formWorkflowStepRepository->save($workflowStep);
            }
        }
    }
}
