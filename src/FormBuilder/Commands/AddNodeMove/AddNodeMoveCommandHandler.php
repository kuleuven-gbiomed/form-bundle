<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddNodeMove;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\Util\NodeTargetUtil;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Manager\NodeManager;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;

final class AddNodeMoveCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function __invoke(AddNodeMoveCommand $command): void
    {
        $targetNode = $this->getFormNodeForCommand($command);

        $sourceNodeInfo = $command->getAdd();
        /** @psalm-var FormNodeRepository $formNodeRepo */
        $formNodeRepo = $this->entityManager->getRepository(FormNode::class);
        $sourceNode = $formNodeRepo->getById($sourceNodeInfo->getSourceNodeId());
        if ($sourceNode->getContentHash() !== $sourceNodeInfo->getSourceNodeContentHash()) {
            throw new OutOfSyncException();
        }

        $target = NodeTargetUtil::calculateTarget($targetNode, $sourceNodeInfo->getTargetDirection());
        $nodeManager = new NodeManager($this->entityManager);
        $nodeManager->moveNodeTo($sourceNode, $target['parent'], $target['sequence']);
    }
}
