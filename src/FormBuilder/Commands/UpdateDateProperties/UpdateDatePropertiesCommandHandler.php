<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateDateProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

class UpdateDatePropertiesCommandHandler extends CommandHandler
{
    public function __invoke(UpdateDatePropertiesCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        // -- execute the command
        $changeTo = $command->getChangeTo();

        // -- update props config
        $propsConfig = $formNode->getPropsConfig();
        $propsConfig['formatMask'] = $changeTo->getFormatMask();
        $propsConfig['prefillWithToday'] = $changeTo->isPrefillWithToday();

        // -- update content hash and save
        $formNode->setPropsConfig($propsConfig);
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
