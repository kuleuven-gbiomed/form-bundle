<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateDateProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\DatePropertiesPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractUpdateCommand<DatePropertiesPayload> */
class UpdateDatePropertiesCommand extends AbstractUpdateCommand
{
    /** @var DatePropertiesPayload */
    protected PayloadInterface $changeFrom;
    /** @var DatePropertiesPayload */
    protected PayloadInterface $changeTo;
}
