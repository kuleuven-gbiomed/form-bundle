<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateChartRelation;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Util\ChartUtil;

class UpdateChartRelationCommandHandler extends CommandHandler
{
    public function __invoke(UpdateChartRelationCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        $changeTo = $command->getChangeTo();
        $participantNode = $this->formNodeRepository->getById($changeTo->getParticipantId());
        $chartNode = $this->formNodeRepository->getById($changeTo->getChartId());

        $propsConfig = $chartNode->getPropsConfig();
        $questionReferences = $propsConfig['question_references'];
        $participantNodeIndex = ChartUtil::findInQuestionReferences($participantNode->getUid(), $questionReferences);

        if (!(-1 === $participantNodeIndex)) {
            $reference = $questionReferences[$participantNodeIndex];
            if (!is_array($reference['custom_labels'])) {
                $reference['custom_labels'] = [];
            }
            if ('' === $changeTo->getCustomLabel()) {
            } else {
                $reference['custom_labels'][$command->getLanguage()] = $changeTo->getCustomLabel();
            }
            $questionReferences[$participantNodeIndex] = $reference;
        }

        $propsConfig['question_references'] = $questionReferences;

        $chartNode->setPropsConfig($propsConfig);
        $chartNode->makeContentHash();
        $this->formNodeRepository->save($chartNode);

        $participantNode->touch();
        $participantNode->makeContentHash();
        $this->formNodeRepository->save($participantNode);
    }
}
