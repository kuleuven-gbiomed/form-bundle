<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateChartRelation;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ChartRelationPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/**
 * @extends AbstractUpdateCommand<ChartRelationPayload>
 *
 * @template-extends AbstractUpdateCommand<ChartRelationPayload>
 */
class UpdateChartRelationCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var ChartRelationPayload */
    protected PayloadInterface $changeFrom;
    /** @var ChartRelationPayload */
    protected PayloadInterface $changeTo;
}
