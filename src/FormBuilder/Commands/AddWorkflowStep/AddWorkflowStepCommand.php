<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddWorkflowStep;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\AddStepInfoPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractAddCommand<AddStepInfoPayload> */
class AddWorkflowStepCommand extends AbstractAddCommand
{
    use LanguageTrait;

    /** @var AddStepInfoPayload */
    protected PayloadInterface $add;
}
