<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddWorkflowStep;

use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\UpdatePermissions\Model\FlowPermission;
use KUL\FormBundle\FormBuilder\Commands\Util\WorkflowTypeDetector;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermissionTemplate;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflowStep;
use Ramsey\Uuid\Uuid;

class AddWorkflowStepCommandHandler extends CommandHandler
{
    public function __invoke(AddWorkflowStepCommand $command): void
    {
        $form = $this->getFormForCommand($command);
        $formWorkFlow = $this->formWorkflowRepository->getByForm($form);
        $add = $command->getAdd();

        // -- make type and a default period ---------------------------------------
        $period = [];
        switch ($add->getType()) {
            case 'relative':
                $type = StepWithCalculatedDateStart::TYPE;
                $period['amountOfDaysToCalculateStartDate'] = 0;
                $period['amountOfDaysToCalculateEndDate'] = 60;
                break;
            default:
                $type = StepWithFixedDateStart::TYPE;
                $now = new \DateTime();
                $end = new \DateTime();
                $endDate = $end->add(new \DateInterval('P1M'));
                $period['fixedStartDate'] = $now->format('Y-m-d H:i:s');
                $period['fixedEndDate'] = $endDate->format('Y-m-d H:i:s');
                break;
        }

        // -- roles with one submit ----------------------------------------------
        $rolesWithOneSubmit = [];

        // -- make info object ---------------------------------------------------
        $info = [];
        $info['label'] = [];
        $info['label'][$command->getLanguage()] = $this->htmlSanitizer->sanitize($add->getLabel());
        $info['description'] = [];
        $info['roleDescriptions'] = [];

        // -- user interaction object --------------------------------------------
        $jsonString = '{"notificationConfig": {"mailBodyForStart": [], "mailBodyForOngoing": [], "roleConfigsToNotifyOfStart": [], "roleConfigsToRemindOfOngoing": []}, "roleSubmitSuccessMessages": [], "generalSubmitSuccessMessage": [], "redirectAwayOnSubmitSuccess": false}';
        $submitSuccessUserInteraction = json_decode($jsonString, true);

        // -- custom options -----------------------------------------------------
        $customOptions = [];
        $sequence = $command->getAdd()->getStep() - 1;

        // -- make the step and save it ------------------------------------------
        $workflowStep = new FormWorkflowStep(
            Uuid::uuid4()->toString(),
            $sequence,
            $formWorkFlow,
            $type,
            $period,
            false,
            $rolesWithOneSubmit,
            $info,
            $submitSuccessUserInteraction,
            false,
            $customOptions
        );
        $workflowStep->setSchemaCoordinates(
            $command->getAdd()->getX(),
            $command->getAdd()->getY(),
        );
        $workflowStep->makeContentHash();
        $this->formWorkflowStepRepository->save($workflowStep);

        // -- update the coordinates of the add button ----------------------------
        $formWorkFlow->setStartButtonCoordinates($command->getAdd()->getX(), $command->getAdd()->getY() + 250);
        $formWorkFlow->makeContentHash();
        $this->formWorkflowRepository->save($formWorkFlow);

        // -- add an empty step for all nodes of this form template --------------
        $stepUuid = $workflowStep->getUid();
        $emptyStep = [];
        $emptyStep['stepUid'] = $stepUuid;
        $emptyStep['rolePermissions'] = [];

        $formNodes = $this->formNodeRepository->getNodesByFormAndType($form, FormNode::TYPE_QUESTION);
        /** @var FormNode $formNode */
        foreach ($formNodes as $formNode) {
            $nodeFlowPermission = $this->formNodeFlowPermissionRepository->getByFormNode($formNode);
            $nodeFlowPermissionList = $nodeFlowPermission->getFlowPermission();
            $nodeFlowPermissionList[] = $emptyStep;
            $nodeFlowPermission->setFlowPermission($nodeFlowPermissionList);
            $nodeFlowPermission->makeContentHash();
            $this->formNodeFlowPermissionRepository->save($nodeFlowPermission);
        }

        // -- add an empty step for all templates for this form template -------------
        $permissionTemplates = $this->permissionTemplateRepo->getTemplatesByForm($form);
        /** @var FormNodeFlowPermissionTemplate $permissionTemplate */
        foreach ($permissionTemplates as $permissionTemplate) {
            $nodeFlowPermissionList = $permissionTemplate->getFlowPermission();
            $nodeFlowPermissionList[] = $emptyStep;
            $typedFlowPermission = FlowPermission::convertFromArray($nodeFlowPermissionList);
            $permissionTemplate->setFlowPermission($typedFlowPermission);
            $permissionTemplate->makeContentHash();
            $this->permissionTemplateRepo->save($permissionTemplate);
        }

        // -- determine the general type of the workflow ------------------------------
        WorkflowTypeDetector::setWorkflowType($this->formWorkflowRepository, $formWorkFlow->getId());
    }
}
