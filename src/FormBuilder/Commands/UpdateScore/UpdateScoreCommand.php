<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateScore;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ScorePayload;

/** @extends  AbstractUpdateCommand<ScorePayload> */
class UpdateScoreCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var ScorePayload */
    protected PayloadInterface $changeFrom;
    /** @var ScorePayload */
    protected PayloadInterface $changeTo;
}
