<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateScore;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Util\ScoreUtil;

final class UpdateScoreCommandHandler extends CommandHandler
{
    public function __invoke(UpdateScoreCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $changeTo = $command->getChangeTo();
        $scoreNode = $this->formNodeRepository->getById($changeTo->getScoreQuestionId());
        $calculationNode = $this->formNodeRepository->getById($changeTo->getScoreCalculationId());

        $scoringConfig = $calculationNode->getScoringConfig();
        $scoringList = $scoringConfig['globalScoreCalculationDependencyConfigs'];
        $scoreNodeIndex = ScoreUtil::findInScoringConfig($scoreNode->getUid(), $scoringList);

        if (!(-1 === $scoreNodeIndex)) {
            $scoringList[$scoreNodeIndex]['weighing'] = $changeTo->getWeight();
            $scoringConfig['globalScoreCalculationDependencyConfigs'] = $scoringList;
        }

        // -- touch the score
        $scoreNode->touch();
        $scoreNode->makeContentHash();
        $this->formNodeRepository->save($scoreNode);
        // -- update the calculation node with the new scoring config
        $calculationNode->setScoringConfig($scoringConfig);
        $calculationNode->makeContentHash();
        $this->formNodeRepository->save($calculationNode);
    }
}
