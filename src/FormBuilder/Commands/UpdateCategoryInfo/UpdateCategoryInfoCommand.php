<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateCategoryInfo;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\CategoryInfoPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/**
 * @extends AbstractUpdateCommand<CategoryInfoPayload>
 *
 * @template-extends AbstractUpdateCommand<CategoryInfoPayload>
 */
class UpdateCategoryInfoCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var CategoryInfoPayload */
    protected PayloadInterface $changeFrom;
    /** @var CategoryInfoPayload */
    protected PayloadInterface $changeTo;
}
