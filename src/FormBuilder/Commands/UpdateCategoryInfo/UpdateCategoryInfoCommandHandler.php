<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateCategoryInfo;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class UpdateCategoryInfoCommandHandler extends CommandHandler
{
    public function __invoke(UpdateCategoryInfoCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        // ----------------------------------------------------------------------------------------- execute the command
        $info = $formNode->getInfo();

        $changeTo = $command->getChangeTo();
        // ------------------------------------------------------------------------------------------------ update title
        if (0 === (is_countable($info['label']) ? count($info['label']) : 0)) {
            if (mb_strlen($changeTo->getTitle()) > 0) {
                $info['label'] = [];
                $info['label'][$command->getLanguage()] = $this->htmlSanitizer->sanitize($changeTo->getTitle());
            }
        } else {
            $info['label'][$command->getLanguage()] = $this->htmlSanitizer->sanitize($changeTo->getTitle());
        }

        // ------------------------------------------------------------------------------------------ update description
        if (0 === (is_countable($info['description']) ? count($info['description']) : 0)) {
            if (mb_strlen($changeTo->getDescription()) > 0) {
                $info['description'] = [];
                $info['description'][$command->getLanguage()] = $this->htmlSanitizer->sanitize($changeTo->getDescription());
            }
        } else {
            $info['description'][$command->getLanguage()] = $this->htmlSanitizer->sanitize($changeTo->getDescription());
        }
        $formNode->setInfo($info);

        // -- update the display mode: default or matrix
        $formNode->setDisplayMode($changeTo->getDisplayMode());
        if ('matrix' == $changeTo->getDisplayMode()) {
            $firstHeaderCellTitle = $formNode->getFirstHeaderCellTitle();
            $firstHeaderCellTitle[$command->getLanguage()] = $this->htmlSanitizer->sanitize($changeTo->getFirstHeaderCellTitle());
            $formNode->setFirstHeaderCellTitle($firstHeaderCellTitle);
        } else {
            $formNode->setFirstHeaderCellTitle([]);
        }

        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
