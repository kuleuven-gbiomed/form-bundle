<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateChoiceProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class UpdateChoicePropertiesCommandHandler extends CommandHandler
{
    public function __invoke(UpdateChoicePropertiesCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        // -- execute the command -----------------------------------------
        $changeTo = $command->getChangeTo();

        // -- update props config
        $propsConfig = $formNode->getPropsConfig();
        $propsConfig['expanded'] = $changeTo->isExpanded();
        $propsConfig['sortOptionsAlphabetically'] = $changeTo->isSortOptionsAlphabetically();
        $propsConfig['multiple'] = $changeTo->isMultiple();
        if ($changeTo->isMultiple()) {
            $propsConfig['minSelect'] = $changeTo->getMinSelect();
            $propsConfig['maxSelect'] = $changeTo->getMaxSelect();
        } else {
            $propsConfig['minSelect'] = null;
            $propsConfig['maxSelect'] = null;
        }
        $propsConfig['custom_options'] = $changeTo->getCustomOptions();

        // -- update required status

        if ($changeTo->getMinSelect() > 0) {
            $formNode->setRequired(true);
        }
        if ($command->getChangeFrom()->getMinSelect() > 0 && 0 === $changeTo->getMinSelect()) {
            $formNode->setRequired(false);
        }

        // -- update display mode -----------------------------------------
        $formNode->setDisplayMode($changeTo->getDisplayMode());

        // -- update content hash and save --------------------------------
        $formNode->setPropsConfig($propsConfig);
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
