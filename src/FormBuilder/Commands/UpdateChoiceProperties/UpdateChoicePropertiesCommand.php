<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateChoiceProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ChoicePropertiesPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractUpdateCommand<ChoicePropertiesPayload> */
class UpdateChoicePropertiesCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var ChoicePropertiesPayload */
    protected PayloadInterface $changeFrom;
    /** @var ChoicePropertiesPayload */
    protected PayloadInterface $changeTo;
}
