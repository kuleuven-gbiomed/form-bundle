<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddNodeCopy;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\SourceNodeInfoPayload;

/** @extends AbstractAddCommand<SourceNodeInfoPayload> */
class AddNodeCopyCommand extends AbstractAddCommand
{
    use LanguageTrait;

    /** @var SourceNodeInfoPayload */
    protected PayloadInterface $add;
}
