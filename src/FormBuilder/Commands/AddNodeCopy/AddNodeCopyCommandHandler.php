<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddNodeCopy;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\Util\NodeTargetUtil;
use KUL\FormBundle\FormBuilder\Manager\NodeManager;

final class AddNodeCopyCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function __invoke(AddNodeCopyCommand $command): void
    {
        $targetNode = $this->getFormNodeForCommand($command);

        $sourceNodeInfo = $command->getAdd();
        $sourceNode = $this->formNodeRepository->getById($sourceNodeInfo->getSourceNodeId());
        if ($sourceNode->getContentHash() !== $sourceNodeInfo->getSourceNodeContentHash()) {
            throw new OutOfSyncException();
        }
        $target = NodeTargetUtil::calculateTarget($targetNode, $sourceNodeInfo->getTargetDirection());
        $nodeManager = new NodeManager($this->entityManager);

        $nodeManager->copyNodeTo(
            $sourceNode,
            $target['parent'],
            $target['sequence'],
            $this->htmlSanitizer->sanitize($sourceNodeInfo->getLabel()),
            $command->getLanguage(),
            $sourceNodeInfo->getPermissionTemplateId()
        );
    }
}
