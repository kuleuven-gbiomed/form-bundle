<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddPermissionsTemplate;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Util\FlowPermissionConverter;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermissionTemplate;
use Ramsey\Uuid\Uuid;

final class AddPermissionsTemplateCommandHandler extends CommandHandler
{
    public function __invoke(AddPermissionsTemplateCommand $command): void
    {
        $form = $this->getFormForCommand($command);

        // ------------------------------------------------------------------- set the command information in the entity
        $flowPermission = FlowPermissionConverter::convertToFlowPermission($command->getAdd()->getFlowPermission());
        $template = new FormNodeFlowPermissionTemplate(Uuid::uuid4()->toString());
        $template->setFormNameAndFlowPermission($form, $command->getAdd()->getName(), $flowPermission);

        // -------------------------------------------------------------------------------------------- add the template
        $template->makeContentHash();
        $this->permissionTemplateRepo->save($template);
    }
}
