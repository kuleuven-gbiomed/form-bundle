<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddPermissionsTemplate;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PermissionTemplatePayload;

/** @extends AbstractAddCommand<PermissionTemplatePayload> */
class AddPermissionsTemplateCommand extends AbstractAddCommand
{
    /** @var PermissionTemplatePayload */
    protected PayloadInterface $add;
}
