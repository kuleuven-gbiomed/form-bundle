<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandInterface;
use KUL\FormBundle\FormBuilder\Utility\FormBuilderUser;

interface CommandResultServiceInterface
{
    public function getTranslatedCommandResult(?CommandInterface $command, FormBuilderUser $user): CommandResult;
}
