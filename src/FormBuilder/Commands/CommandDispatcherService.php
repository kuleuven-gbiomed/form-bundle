<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandInterface;
use KUL\FormBundle\FormBuilder\Service\CommandHistory;
use Symfony\Component\Messenger\MessageBusInterface;

class CommandDispatcherService
{
    public function __construct(
        private readonly CommandHistory $commandHistory,
        private readonly MessageBusInterface $formBundleCommandBus,
    ) {
    }

    public function dispatch(CommandInterface $command): void
    {
        $this->formBundleCommandBus->dispatch($command);
        $this->commandHistory->append($command);
    }
}
