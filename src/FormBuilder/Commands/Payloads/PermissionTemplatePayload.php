<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class PermissionTemplatePayload implements PayloadInterface
{
    public function __construct(
        private readonly string $name,
        private readonly array $flowPermission,
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFlowPermission(): array
    {
        return $this->flowPermission;
    }
}
