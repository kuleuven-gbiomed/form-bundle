<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class StepNameAndTimingFixedPayload implements PayloadInterface
{
    public function __construct(
        private readonly string $label,
        private readonly bool $allowsStartIfPreviousStepIsFulfilled,
        private readonly string $fixedStartDate,
        private readonly string $fixedEndDate,
    ) {
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function isAllowsStartIfPreviousStepIsFulfilled(): bool
    {
        return $this->allowsStartIfPreviousStepIsFulfilled;
    }

    public function getFixedStartDate(): string
    {
        return $this->fixedStartDate;
    }

    public function getFixedEndDate(): string
    {
        return $this->fixedEndDate;
    }
}
