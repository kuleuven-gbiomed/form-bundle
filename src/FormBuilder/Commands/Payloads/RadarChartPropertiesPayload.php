<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class RadarChartPropertiesPayload implements PayloadInterface
{
    public function __construct(
        private readonly bool $hidePointValues,
        private readonly bool $hideScaleValues,
        private readonly bool $hideScaleUnitSuffix,
        private readonly bool $hidePointUnitSuffix,
        private readonly ?string $unitSign,
    ) {
    }

    public function isHidePointValues(): bool
    {
        return $this->hidePointValues;
    }

    public function isHideScaleValues(): bool
    {
        return $this->hideScaleValues;
    }

    public function isHideScaleUnitSuffix(): bool
    {
        return $this->hideScaleUnitSuffix;
    }

    public function isHidePointUnitSuffix(): bool
    {
        return $this->hidePointUnitSuffix;
    }

    public function getUnitSign(): ?string
    {
        return $this->unitSign;
    }
}
