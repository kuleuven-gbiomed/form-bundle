<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class TextPropertiesPayload implements PayloadInterface
{
    public function __construct(
        private readonly bool $multiLine,
        private readonly int $minLength,
        private readonly int $maxLength,
        private readonly string $displayMode,
    ) {
    }

    public function isMultiLine(): bool
    {
        return $this->multiLine;
    }

    public function getMinLength(): int
    {
        return $this->minLength;
    }

    public function getMaxLength(): int
    {
        return $this->maxLength;
    }

    public function getDisplayMode(): string
    {
        return $this->displayMode;
    }
}
