<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

readonly class RowOrderPayload implements PayloadInterface
{
    /** @param list<int> $rowOrder */
    public function __construct(public array $rowOrder)
    {
    }
}
