<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class UploadPropertiesPayload implements PayloadInterface
{
    public function __construct(
        private readonly int $minAmount,
        private readonly int $maxAmount,
        private readonly int $maxFileSize,
        private readonly array $mimeTypes,
        private readonly array $extensions,
    ) {
    }

    public function getMinAmount(): int
    {
        return $this->minAmount;
    }

    public function getMaxAmount(): int
    {
        return $this->maxAmount;
    }

    public function getMaxFileSize(): int
    {
        return $this->maxFileSize;
    }

    public function getMimeTypes(): array
    {
        return $this->mimeTypes;
    }

    public function getExtensions(): array
    {
        return $this->extensions;
    }
}
