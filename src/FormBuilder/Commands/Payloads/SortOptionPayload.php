<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class SortOptionPayload implements PayloadInterface
{
    public function __construct(private readonly array $options)
    {
    }

    public function getOptions(): array
    {
        return $this->options;
    }
}
