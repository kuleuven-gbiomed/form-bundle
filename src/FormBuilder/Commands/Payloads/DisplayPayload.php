<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class DisplayPayload implements PayloadInterface
{
    public function __construct(private readonly bool $hidden)
    {
    }

    public function isHidden(): bool
    {
        return $this->hidden;
    }
}
