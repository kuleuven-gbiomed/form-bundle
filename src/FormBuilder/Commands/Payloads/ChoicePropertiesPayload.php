<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class ChoicePropertiesPayload implements PayloadInterface
{
    public function __construct(
        private readonly bool $multiple,
        private readonly bool $expanded,
        private readonly ?int $minSelect,
        private readonly ?int $maxSelect,
        private readonly bool $sortOptionsAlphabetically,
        private readonly string $displayMode,
        private readonly array $custom_options,
    ) {
    }

    public function isMultiple(): bool
    {
        return $this->multiple;
    }

    public function isExpanded(): bool
    {
        return $this->expanded;
    }

    public function getMinSelect(): ?int
    {
        return $this->minSelect;
    }

    public function getMaxSelect(): ?int
    {
        return $this->maxSelect;
    }

    public function isSortOptionsAlphabetically(): bool
    {
        return $this->sortOptionsAlphabetically;
    }

    public function getDisplayMode(): string
    {
        return $this->displayMode;
    }

    public function getCustomOptions(): array
    {
        return $this->custom_options;
    }
}
