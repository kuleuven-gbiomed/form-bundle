<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class DeleteStepPayload implements PayloadInterface
{
    public function __construct(
        private readonly int $stepId,
        private readonly int $formId,
    ) {
    }

    public function getStepId(): int
    {
        return $this->stepId;
    }

    public function getFormId(): int
    {
        return $this->formId;
    }
}
