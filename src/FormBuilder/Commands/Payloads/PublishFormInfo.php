<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class PublishFormInfo implements PayloadInterface
{
    public function __construct(
        private readonly bool $useNewVersionNumber,
    ) {
    }

    public function useNewVersionNumber(): bool
    {
        return $this->useNewVersionNumber;
    }
}
