<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class QuestionRequiredPayload implements PayloadInterface
{
    public function __construct(private readonly bool $required)
    {
    }

    public function isRequired(): bool
    {
        return $this->required;
    }
}
