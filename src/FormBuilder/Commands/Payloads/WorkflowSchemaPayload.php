<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class WorkflowSchemaPayload implements PayloadInterface
{
    /*
     * -> id / number or 'add'
     * -> x
     * -> y
     */

    public function __construct(
        private readonly array $config,
    ) {
    }

    public function getConfig(): array
    {
        return $this->config;
    }
}
