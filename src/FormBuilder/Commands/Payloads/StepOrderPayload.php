<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class StepOrderPayload implements PayloadInterface
{
    public function __construct(
        private readonly array $order,
    ) {
    }

    public function getOrder(): array
    {
        return $this->order;
    }
}
