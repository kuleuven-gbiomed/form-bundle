<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class ScorePayload implements PayloadInterface
{
    public function __construct(private readonly int $scoreQuestionId, private readonly int $scoreCalculationId, private readonly float $weight)
    {
    }

    public function getScoreQuestionId(): int
    {
        return $this->scoreQuestionId;
    }

    public function getScoreCalculationId(): int
    {
        return $this->scoreCalculationId;
    }

    public function getWeight(): float
    {
        return $this->weight;
    }
}
