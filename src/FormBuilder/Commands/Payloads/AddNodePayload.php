<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class AddNodePayload implements PayloadInterface
{
    public function __construct(
        private readonly string $targetDirection,
        private readonly string $label,
        private readonly int $permissionTemplateId,
        private readonly string $nodeType,
    ) {
    }

    public function getTargetDirection(): string
    {
        return $this->targetDirection;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getPermissionTemplateId(): int
    {
        return $this->permissionTemplateId;
    }

    public function getNodeType(): string
    {
        return $this->nodeType;
    }
}
