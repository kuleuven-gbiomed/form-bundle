<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class SourceNodeInfoPayload implements PayloadInterface
{
    public function __construct(
        private readonly string $targetDirection,
        private readonly string $sourceNodeContentHash,
        private readonly int $sourceNodeId,
        private readonly int $permissionTemplateId,
        private readonly string $label,
    ) {
    }

    public function getTargetDirection(): string
    {
        return $this->targetDirection;
    }

    public function getSourceNodeContentHash(): string
    {
        return $this->sourceNodeContentHash;
    }

    public function getSourceNodeId(): int
    {
        return $this->sourceNodeId;
    }

    public function getPermissionTemplateId(): int
    {
        return $this->permissionTemplateId;
    }

    public function getLabel(): string
    {
        return $this->label;
    }
}
