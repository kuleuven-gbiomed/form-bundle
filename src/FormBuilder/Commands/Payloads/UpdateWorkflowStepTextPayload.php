<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class UpdateWorkflowStepTextPayload implements PayloadInterface
{
    public function __construct(
        protected string $subject,
        protected string $description,
    ) {
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
