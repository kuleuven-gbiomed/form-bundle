<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class PrefillSourceQuestionPayload implements PayloadInterface
{
    public function __construct(
        private readonly int $sourceId,
    ) {
    }

    public function getSourceId(): int
    {
        return $this->sourceId;
    }
}
