<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class CalculationPropertiesPayload implements PayloadInterface
{
    public function __construct(
        private readonly bool $isCalculation,
        private readonly string $weighingType,
        private readonly ?float $passScore,
        private readonly bool $showAutoCalculateBtn,
        private readonly bool $allowNonAutoCalculatedValue,
        private readonly ?int $roundingMode,
        private readonly ?int $scale,
        private readonly ?float $minScore,
        private readonly ?float $maxScore,
    ) {
    }

    public function isCalculation(): bool
    {
        return $this->isCalculation;
    }

    public function getWeighingType(): string
    {
        return $this->weighingType;
    }

    public function getPassScore(): ?float
    {
        return $this->passScore;
    }

    public function isShowAutoCalculateBtn(): bool
    {
        return $this->showAutoCalculateBtn;
    }

    public function isAllowNonAutoCalculatedValue(): bool
    {
        return $this->allowNonAutoCalculatedValue;
    }

    public function getRoundingMode(): ?int
    {
        return $this->roundingMode;
    }

    public function getScale(): ?int
    {
        return $this->scale;
    }

    public function getMinScore(): ?float
    {
        return $this->minScore;
    }

    public function getMaxScore(): ?float
    {
        return $this->maxScore;
    }
}
