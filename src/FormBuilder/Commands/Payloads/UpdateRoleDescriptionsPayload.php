<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class UpdateRoleDescriptionsPayload implements PayloadInterface
{
    public function __construct(
        protected string $subject,
        protected array $descriptions,
    ) {
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getDescriptions(): array
    {
        return $this->descriptions;
    }
}
