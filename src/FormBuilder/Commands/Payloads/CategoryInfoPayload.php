<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class CategoryInfoPayload implements PayloadInterface
{
    public function __construct(
        private readonly string $title,
        private readonly string $description,
        private readonly string $displayMode,
        private readonly string $firstHeaderCellTitle,
    ) {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getDisplayMode(): string
    {
        return $this->displayMode;
    }

    public function getFirstHeaderCellTitle(): string
    {
        return $this->firstHeaderCellTitle;
    }
}
