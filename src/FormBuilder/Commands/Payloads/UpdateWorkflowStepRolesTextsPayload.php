<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class UpdateWorkflowStepRolesTextsPayload implements PayloadInterface
{
    public function __construct(
        protected string $subject,
        protected array $messages,
    ) {
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }
}
