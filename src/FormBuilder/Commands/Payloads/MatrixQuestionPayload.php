<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

readonly class MatrixQuestionPayload implements PayloadInterface
{
    public function __construct(
        public string $question,
    ) {
    }
}
