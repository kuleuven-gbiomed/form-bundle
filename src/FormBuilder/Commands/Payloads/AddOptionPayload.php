<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class AddOptionPayload implements PayloadInterface
{
    public function __construct(private readonly string $option, private readonly ?float $score)
    {
    }

    public function getOption(): string
    {
        return $this->option;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }
}
