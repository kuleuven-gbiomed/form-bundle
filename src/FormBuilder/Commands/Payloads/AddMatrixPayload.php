<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

readonly class AddMatrixPayload implements PayloadInterface
{
    public function __construct(
        public string $targetDirection,
        public string $label,
        public int $permissionTemplateId,
        public string $nodeType,
        public string $firstRowLabel,
        public string $firstQuestionLabel,
        public string $firstQuestionType,
    ) {
    }
}
