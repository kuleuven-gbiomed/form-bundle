<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class AddStepInfoPayload implements PayloadInterface
{
    public function __construct(
        private readonly string $label,
        private readonly int $step,
        private readonly string $type,
        private readonly int $x,
        private readonly int $y,
    ) {
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getStep(): int
    {
        return $this->step;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }
}
