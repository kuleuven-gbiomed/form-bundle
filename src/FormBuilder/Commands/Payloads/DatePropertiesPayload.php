<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class DatePropertiesPayload implements PayloadInterface
{
    public function __construct(
        private readonly string $formatMask,
        private readonly bool $prefillWithToday,
        private readonly string $minDate,
        private readonly string $maxDate,
    ) {
    }

    public function getFormatMask(): string
    {
        return $this->formatMask;
    }

    public function isPrefillWithToday(): bool
    {
        return $this->prefillWithToday;
    }

    public function getMinDate(): string
    {
        return $this->minDate;
    }

    public function getMaxDate(): string
    {
        return $this->maxDate;
    }
}
