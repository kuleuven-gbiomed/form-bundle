<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class PermissionPayload implements PayloadInterface
{
    public function __construct(private readonly array $flowPermission, private readonly int $templateId)
    {
    }

    public function getFlowPermission(): array
    {
        return $this->flowPermission;
    }

    public function getTemplateId(): int
    {
        return $this->templateId;
    }
}
