<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class PrefillSourceConfigurationPayload implements PayloadInterface
{
    public function __construct(
        private readonly int $prefillSourceQuestionId,
        private readonly int $prefilledQuestionId,
    ) {
    }

    public function getPrefillSourceQuestionId(): int
    {
        return $this->prefillSourceQuestionId;
    }

    public function getPrefilledQuestionId(): int
    {
        return $this->prefilledQuestionId;
    }
}
