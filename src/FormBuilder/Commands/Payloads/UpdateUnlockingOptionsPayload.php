<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

final readonly class UpdateUnlockingOptionsPayload implements PayloadInterface
{
    /** @param list<int> $optionIds */
    public function __construct(public array $optionIds)
    {
    }

    /** @return list<int> */
    public function getOptionIds(): array
    {
        return $this->optionIds;
    }
}
