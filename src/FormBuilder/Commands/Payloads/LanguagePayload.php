<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class LanguagePayload implements PayloadInterface
{
    public function __construct(private readonly string $language)
    {
    }

    public function getLanguage(): string
    {
        return $this->language;
    }
}
