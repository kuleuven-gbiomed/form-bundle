<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class ScorePropertiesPayload implements PayloadInterface
{
    public function __construct(
        private readonly int $roundingMode,
        private readonly int $scale,
        private readonly float $minScore,
        private readonly float $maxScore,
    ) {
    }

    public function getRoundingMode(): int
    {
        return $this->roundingMode;
    }

    public function getScale(): int
    {
        return $this->scale;
    }

    public function getMinScore(): float
    {
        return $this->minScore;
    }

    public function getMaxScore(): float
    {
        return $this->maxScore;
    }
}
