<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class DeleteNodePayload implements PayloadInterface
{
    public function __construct(
        private readonly int $nodeId,
        private readonly int $formId,
    ) {
    }

    public function getNodeId(): int
    {
        return $this->nodeId;
    }

    public function getFormId(): int
    {
        return $this->formId;
    }
}
