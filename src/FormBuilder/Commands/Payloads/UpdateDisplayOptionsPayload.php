<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class UpdateDisplayOptionsPayload implements PayloadInterface
{
    public function __construct(
        public readonly bool $showPositionNumbers,
        public readonly bool $enableReadOnlyChoiceAnswerOptionsToggling,
    ) {
    }
}
