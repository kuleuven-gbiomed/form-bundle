<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class ConditionPayload implements PayloadInterface
{
    public function __construct(
        private readonly int $lockedId,
        private readonly int $unlockerId,
        private readonly int $optionId,
    ) {
    }

    public function getLockedId(): int
    {
        return $this->lockedId;
    }

    public function getUnlockerId(): int
    {
        return $this->unlockerId;
    }

    public function getOptionId(): int
    {
        return $this->optionId;
    }
}
