<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class StepNameAndTimingRelativePayload implements PayloadInterface
{
    public function __construct(
        private readonly string $label,
        private readonly bool $allowsStartIfPreviousStepIsFulfilled,
        private readonly string $amountOfDaysToCalculateStartDate,
        private readonly string $amountOfDaysToCalculateEndDate,
    ) {
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function isAllowsStartIfPreviousStepIsFulfilled(): bool
    {
        return $this->allowsStartIfPreviousStepIsFulfilled;
    }

    public function getAmountOfDaysToCalculateStartDate(): string
    {
        return $this->amountOfDaysToCalculateStartDate;
    }

    public function getAmountOfDaysToCalculateEndDate(): string
    {
        return $this->amountOfDaysToCalculateEndDate;
    }
}
