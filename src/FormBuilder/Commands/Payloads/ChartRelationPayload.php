<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class ChartRelationPayload implements PayloadInterface
{
    public function __construct(
        private readonly int $participantId,
        private readonly int $chartId,
        private readonly string $customLabel,
    ) {
    }

    public function getParticipantId(): int
    {
        return $this->participantId;
    }

    public function getChartId(): int
    {
        return $this->chartId;
    }

    public function getCustomLabel(): string
    {
        return $this->customLabel;
    }
}
