<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class UpdateWorkflowStepRoleOffsetConfigsPayload implements PayloadInterface
{
    public function __construct(
        protected string $subject,
        protected array $configs,
    ) {
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getConfigs(): array
    {
        return $this->configs;
    }
}
