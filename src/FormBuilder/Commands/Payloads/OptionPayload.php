<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class OptionPayload implements PayloadInterface
{
    public function __construct(
        private readonly string $option,
        private readonly ?float $score,
        private readonly bool $defaultSelected,
        private readonly string $inputType,
        private readonly ?int $additionalTextMinLength,
        private readonly ?int $additionalTextMaxLength,
        private readonly bool $additionalTextRequired = false,
    ) {
    }

    public function getOption(): string
    {
        return $this->option;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function isDefaultSelected(): bool
    {
        return $this->defaultSelected;
    }

    public function getInputType(): string
    {
        return $this->inputType;
    }

    public function isAdditionalTextRequired(): bool
    {
        return $this->additionalTextRequired;
    }

    public function getAdditionalTextMinLength(): ?int
    {
        return $this->additionalTextMinLength;
    }

    public function getAdditionalTextMaxLength(): ?int
    {
        return $this->additionalTextMaxLength;
    }
}
