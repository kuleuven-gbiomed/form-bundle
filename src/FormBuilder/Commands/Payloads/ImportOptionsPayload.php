<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class ImportOptionsPayload implements PayloadInterface
{
    public function __construct(
        private readonly string $importString,
        private readonly bool $isAppend,
    ) {
    }

    public function getImportString(): string
    {
        return $this->importString;
    }

    public function isAppend(): bool
    {
        return $this->isAppend;
    }
}
