<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class NumberPropertiesPayload implements PayloadInterface
{
    public function __construct(
        private readonly int $min,
        private readonly int $max,
        private readonly int $scale,
        private readonly int $stepInterval,
        private readonly bool $showMaxAsInputAddon,
        private readonly bool $showMinAsInputAddon,
    ) {
    }

    public function getMin(): int
    {
        return $this->min;
    }

    public function getMax(): int
    {
        return $this->max;
    }

    public function getScale(): int
    {
        return $this->scale;
    }

    public function getStepInterval(): int
    {
        return $this->stepInterval;
    }

    public function isShowMaxAsInputAddon(): bool
    {
        return $this->showMaxAsInputAddon;
    }

    public function isShowMinAsInputAddon(): bool
    {
        return $this->showMinAsInputAddon;
    }
}
