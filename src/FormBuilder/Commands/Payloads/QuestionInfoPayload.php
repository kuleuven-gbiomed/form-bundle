<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class QuestionInfoPayload implements PayloadInterface
{
    public function __construct(
        private readonly string $question,
        private readonly string $description,
        private readonly string $descriptionDisplayMode,
        private readonly string $placeholder,
    ) {
    }

    public function getQuestion(): string
    {
        return $this->question;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getDescriptionDisplayMode(): string
    {
        return $this->descriptionDisplayMode;
    }

    public function getPlaceholder(): string
    {
        return $this->placeholder;
    }
}
