<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class WorkflowSettingsPayload implements PayloadInterface
{
    public function __construct(
        private readonly array $rolesWithOneSubmit,
        private readonly bool $hideReadOnlyNodesOnLanding,
        private readonly bool $redirectAwayOnSubmitSuccess,
    ) {
    }

    public function getRolesWithOneSubmit(): array
    {
        return $this->rolesWithOneSubmit;
    }

    public function isHideReadOnlyNodesOnLanding(): bool
    {
        return $this->hideReadOnlyNodesOnLanding;
    }

    public function isRedirectAwayOnSubmitSuccess(): bool
    {
        return $this->redirectAwayOnSubmitSuccess;
    }
}
