<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Payloads;

class DeleteOptionPayload implements PayloadInterface
{
    public function __construct(
        private readonly int $optionId,
        private readonly int $formId,
    ) {
    }

    public function getOptionId(): int
    {
        return $this->optionId;
    }

    public function getFormId(): int
    {
        return $this->formId;
    }
}
