<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddForm;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\FormBuilder\Manager\TemplateManager;
use KUL\FormBundle\Service\SubmittedTemplateService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
readonly class AddFormCommandHandler
{
    // @phpstan-ignore-next-line fixme $entityManager and $sumbittedTemplateService are not used but injected in config
    public function __construct(
        private TemplateManager $templateManager,
        EntityManagerInterface $entityManager,
        SubmittedTemplateService $submittedTemplateService,
    ) {
    }

    public function __invoke(AddFormCommand $command): void
    {
        if ([] === $command->workflow && [] === $command->formList) {
            $this->templateManager->addNewTemplate(
                $command->id,
                $command->name,
                $command->language,
                $command->targetingType,
                $command->roles,
            );
        } else {
            $this->templateManager->importTemplate(
                $command->id,
                $command->name,
                $command->language,
                $command->targetingType,
                $command->roles,
                FormList::fromJsonDecodedArray($command->formList),
                WorkFlow::fromJsonDecodedArray($command->workflow),
            );
        }
    }
}
