<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddForm;

readonly class AddFormCommand
{
    // ----------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------

    public function __construct(
        public string $id,
        public string $name,
        public string $language,
        public string $targetingType,
        public array $roles,
        public array $workflow,
        public array $formList,
    ) {
    }

    // ----------------------------------------------------------------
    // Hydration from JSON
    // ----------------------------------------------------------------

    public static function hydrateFromJson(\stdClass $json): self
    {
        if ('' === trim((string) $json->formList)) {
            $formList = '[]';
        } else {
            $formList = $json->formList;
        }

        if ('' === trim((string) $json->workflow)) {
            $workflow = '[]';
        } else {
            $workflow = $json->workflow;
        }

        return new self(
            trim((string) $json->id),
            trim((string) $json->name),
            trim((string) $json->language),
            trim((string) $json->targetingType),
            json_decode((string) $json->roles, true, 512, \JSON_THROW_ON_ERROR),
            json_decode((string) $workflow, true, 512, \JSON_THROW_ON_ERROR),
            json_decode((string) $formList, true, 512, \JSON_THROW_ON_ERROR)
        );
    }
}
