<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddPrefillSource;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

class AddPrefillSourceCommandHandler extends CommandHandler
{
    public function __invoke(AddPrefillSourceCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        $targetNode = $this->formNodeRepository->getById($command->getAdd()->getPrefilledQuestionId());
        $sourceNode = $this->formNodeRepository->getById($command->getAdd()->getPrefillSourceQuestionId());

        $prefillConfig = $targetNode->getPrefillConfig();
        $prefillUids = $prefillConfig[ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS];
        $prefillUids[] = $sourceNode->getUid();
        $prefillUids = array_unique($prefillUids);
        $prefillConfig[ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS] = $prefillUids;

        $targetNode->setPrefillConfig($prefillConfig);
        $targetNode->makeContentHash();
        $this->formNodeRepository->save($targetNode);

        $sourceNode->touch();
        $sourceNode->makeContentHash();
        $this->formNodeRepository->save($sourceNode);
    }
}
