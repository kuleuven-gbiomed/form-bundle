<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddPrefillSource;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PrefillSourceConfigurationPayload;

/** @extends AbstractAddCommand<PrefillSourceConfigurationPayload> */
class AddPrefillSourceCommand extends AbstractAddCommand
{
    /** @var PrefillSourceConfigurationPayload */
    protected PayloadInterface $add;
}
