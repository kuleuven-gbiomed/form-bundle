<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateUnlockingOptions;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

class UpdateUnlockingOptionsCommandHandler extends CommandHandler
{
    public function __invoke(UpdateUnlockingOptionsCommand $command): void
    {
        $unlockingQuestion = $this->getFormNodeForCommand($command);
        $options = $unlockingQuestion->getOptions();
        $lockedQuestion = $this->formNodeRepository->getById($command->getLockedQuestionId());
        $lockedQuestionUid = $lockedQuestion->getUid();
        $unlockingOptions = $command->getChangeTo()->getOptionIds();

        foreach ($options as $option) {
            $unlockUids = $option->getUnlocksQuestionIds();
            if (in_array($option->getId(), $unlockingOptions, true)) {
                $unlockUids[] = $lockedQuestionUid;
            } else {
                $unlockUids = array_filter($unlockUids, fn ($uid) => $uid !== $lockedQuestionUid);
            }
            $option->setUnlockIds(array_unique($unlockUids));
            $option->makeContentHash();
            $this->formNodeOptionRepository->save($option);
        }

        $unlockingQuestion->touch();
        $unlockingQuestion->makeContentHash();
        $this->formNodeRepository->save($unlockingQuestion);

        $lockedQuestion->touch();
        $lockedQuestion->makeContentHash();
        $this->formNodeRepository->save($lockedQuestion);
    }
}
