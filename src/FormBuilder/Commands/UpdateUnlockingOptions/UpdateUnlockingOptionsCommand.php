<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateUnlockingOptions;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\UpdateUnlockingOptionsPayload;

/** @extends AbstractUpdateCommand<UpdateUnlockingOptionsPayload> */
class UpdateUnlockingOptionsCommand extends AbstractUpdateCommand
{
    /** @var UpdateUnlockingOptionsPayload */
    protected PayloadInterface $changeFrom;
    /** @var UpdateUnlockingOptionsPayload */
    protected PayloadInterface $changeTo;

    /**
     * @param UpdateUnlockingOptionsPayload $changeFrom
     * @param UpdateUnlockingOptionsPayload $changeTo
     */
    public function __construct(
        int $id,
        string $type,
        string $contentHash,
        string $subject,
        bool $force,
        PayloadInterface $changeFrom,
        PayloadInterface $changeTo,
        protected int $lockedQuestionId,
    ) {
        parent::__construct(
            $id,
            $type,
            $contentHash,
            $subject,
            $force,
            $changeFrom,
            $changeTo
        );
    }

    public function getLockedQuestionId(): int
    {
        return $this->lockedQuestionId;
    }
}
