<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateUnlockingOptions;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

class UpdateMultiUnlockingOptionsCommandHandler extends CommandHandler
{
    public function __invoke(UpdateMultiUnlockingOptionsCommand $command): void
    {
        $unlockingQuestion = $this->getFormNodeForCommand($command);
        $options = $unlockingQuestion->getOptions();
        $lockedQuestion = $this->formNodeRepository->getById($command->getLockedQuestionId());
        $lockedQuestionUid = $lockedQuestion->getUid();
        $unlockingOptions = $command->getChangeTo()->getOptionIds();

        $unlockingOptionUids = [];
        foreach ($options as $option) {
            $unlockUids = $option->getMultiUnlocksQuestionIds();
            if (in_array($option->getId(), $unlockingOptions, true)) {
                $unlockUids[] = $lockedQuestionUid;
                $unlockingOptionUids[] = $option->getUid();
            } else {
                $unlockUids = array_filter($unlockUids, fn ($uid) => $uid !== $lockedQuestionUid);
            }
            $option->setMultiUnlockIds(array_unique($unlockUids));
            $option->makeContentHash();
            $this->formNodeOptionRepository->save($option);
        }

        $unlockingQuestion->touch();
        $unlockingQuestion->makeContentHash();
        $this->formNodeRepository->save($unlockingQuestion);

        $lockedQuestion->updateMultiUnlockIdsForChoice($unlockingQuestion->getUid(), $unlockingOptionUids);
        $lockedQuestion->makeContentHash();
        $this->formNodeRepository->save($lockedQuestion);
    }
}
