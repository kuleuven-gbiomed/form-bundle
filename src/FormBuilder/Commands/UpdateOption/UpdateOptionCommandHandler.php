<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateOption;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;

final class UpdateOptionCommandHandler extends CommandHandler
{
    public function __invoke(UpdateOptionCommand $command): void
    {
        $formNodeOption = $this->getFormNodeOptionForCommand($command);
        $formNode = $formNodeOption->getFormNode();
        $changeTo = $command->getChangeTo();

        // -------------------------------------------------------------------------------------------------------- info
        $info = $formNodeOption->getInfo();

        // -- update option label

        if (mb_strlen($changeTo->getOption()) > 0) {
            if (0 === (is_countable($info['label']) ? count($info['label']) : 0)) {
                $info['label'] = [];
            }
            $info['label'][$command->getLanguage()] = $this->htmlSanitizer->sanitize($changeTo->getOption());
        }

        $formNodeOption->setInfo($info);

        // -- update default selected property

        $formNodeOption->setDefaultSelected($changeTo->isDefaultSelected());
        // uncheck all other options for non multiple choice questions
        if ($changeTo->isDefaultSelected() && false === $formNode->getPropsConfig()['multiple']) {
            foreach ($formNode->getOptions() as $option) {
                if (!($formNodeOption->getId() === $option->getId())) {
                    $option->setDefaultSelected(false);
                    $option->makeContentHash();
                    $this->formNodeOptionRepository->save($option);
                }
            }
        }

        // -- set the inputType and props config

        $inputType = $changeTo->getInputType();
        $formNodeOption->setInputType($inputType);

        if (FormNodeOption::INPUT_TYPE_OPTION === $inputType) {
            $propsConfig = [];
            $propsConfig['type'] = FormNodeOption::INPUT_TYPE_OPTION;
            $propsConfig['additionalTextRequired'] = false;
            $propsConfig['additionalTextMinLength'] = null;
            $propsConfig['additionalTextMaxLength'] = null;
            $formNodeOption->setPropsConfig($propsConfig);
        } elseif (FormNodeOption::INPUT_TYPE_OPTION_WITH_ADDITIONAL_TEXT === $inputType) {
            $propsConfig = [];
            $propsConfig['type'] = FormNodeOption::INPUT_TYPE_OPTION_WITH_ADDITIONAL_TEXT;
            $propsConfig['additionalTextRequired'] = $changeTo->isAdditionalTextRequired();
            $propsConfig['additionalTextMinLength'] = $changeTo->getAdditionalTextMinLength();
            $propsConfig['additionalTextMaxLength'] = $changeTo->getAdditionalTextMaxLength();
            $formNodeOption->setPropsConfig($propsConfig);
        }

        // -- set the score

        $formNodeOption->setScore($changeTo->getScore());

        // -- save the form node option with update content hash

        $formNodeOption->makeContentHash();
        $this->formNodeOptionRepository->save($formNodeOption);
    }
}
