<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateOption;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\OptionPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractUpdateCommand<OptionPayload> */
class UpdateOptionCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var OptionPayload */
    protected PayloadInterface $changeFrom;
    /** @var OptionPayload */
    protected PayloadInterface $changeTo;
}
