<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateInfoRoleDescriptions;

enum DescriptionSubject: string
{
    case roleInfoDescriptions = 'roleInfoDescriptions';
}
