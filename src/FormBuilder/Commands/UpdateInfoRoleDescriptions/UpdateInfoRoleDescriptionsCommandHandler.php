<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateInfoRoleDescriptions;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\Common\Types\RoleDescription;

class UpdateInfoRoleDescriptionsCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws \JsonException
     * @throws EntityNotFoundException
     */
    public function __invoke(UpdateInfoRoleDescriptionsCommand $command): void
    {
        $form = $this->getFormForCommand($command);
        $info = $this->formInfoRepository->getByForm($form);
        $changeTo = $command->getChangeTo();

        $roleDescriptions = [];
        foreach ($changeTo->getDescriptions() as $descriptionArray) {
            foreach (RoleDescription::hydrateFromCommand($descriptionArray)->sanitizeAndConvertToFormBundleStructure($this->htmlSanitizer) as $resultDescription) {
                $roleDescriptions[] = $resultDescription;
            }
        }

        $info->setRoleDescriptions($roleDescriptions);
        $info->makeContentHash();
        $this->formInfoRepository->save($info);
    }
}
