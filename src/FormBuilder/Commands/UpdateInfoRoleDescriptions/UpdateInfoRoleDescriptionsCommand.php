<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateInfoRoleDescriptions;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\UpdateRoleDescriptionsPayload;

/** @extends AbstractUpdateCommand<UpdateRoleDescriptionsPayload> */
class UpdateInfoRoleDescriptionsCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var UpdateRoleDescriptionsPayload */
    protected PayloadInterface $changeFrom;
    /** @var UpdateRoleDescriptionsPayload */
    protected PayloadInterface $changeTo;
}
