<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Util;

class ScoreUtil
{
    public static function findInScoringConfig(string $uid, array $scoringList): int
    {
        $_foundIndex = -1;
        $_index = 0;
        foreach ($scoringList as $item) {
            if (trim($uid) === trim((string) $item['scoreNodeUid'])) {
                $_foundIndex = $_index;
            }
            ++$_index;
        }

        return $_foundIndex;
    }
}
