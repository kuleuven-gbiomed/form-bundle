<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Util;

class ChartUtil
{
    public static function findInQuestionReferences(string $uid, array $questionReferences): int
    {
        $_foundIndex = -1;
        $_index = 0;
        foreach ($questionReferences as $item) {
            // todo test on existence of this key...
            if (trim($uid) === trim((string) $item['question_uid'])) {
                $_foundIndex = $_index;
            }
            ++$_index;
        }

        return $_foundIndex;
    }

    public static function getCustomLabels(string $uid, array $questionReferences): array
    {
        foreach ($questionReferences as $item) {
            if (trim($uid) === trim((string) $item['question_uid'])) {
                return $item['custom_labels'];
            }
        }

        return [];
    }
}
