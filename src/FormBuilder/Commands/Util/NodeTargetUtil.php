<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Util;

use KUL\FormBundle\FormBuilder\Commands\Common\RelativeNodeTargetDirectionEnum;
use KUL\FormBundle\FormBuilder\Entity\FormNode;

class NodeTargetUtil
{
    /**
     * @return array{'parent':FormNode, 'sequence':int}
     *
     * @throws \Exception
     */
    public static function calculateTarget(FormNode $targetNode, string $direction): array
    {
        $target = [];
        $targetParent = $targetNode->getParent();
        switch ($direction) {
            case RelativeNodeTargetDirectionEnum::IN->value:
                $target['parent'] = $targetNode;
                $target['sequence'] = 0;
                break;
            case RelativeNodeTargetDirectionEnum::ABOVE->value:
                if (null === $targetParent) {
                    throw new \Exception("Can't add a node above the root node");
                }
                $target['parent'] = $targetParent;
                $target['sequence'] = $targetNode->getSequence();
                break;
            default: // RelativeNodeTargetDirectionEnum::BELOW
                if (null === $targetParent) {
                    throw new \Exception("Can't add a node below the root node");
                }
                $target['parent'] = $targetParent;
                $target['sequence'] = $targetNode->getSequence() + 1;
                break;
        }

        return $target;
    }
}
