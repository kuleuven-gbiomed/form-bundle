<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Util;

use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflowStep;
use KUL\FormBundle\FormBuilder\Model\FormWorkflowType;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository;

class WorkflowTypeDetector
{
    public static function setWorkflowType(FormWorkflowRepository $formWorkflowRepository, int $workflowId): void
    {
        $formWorkflow = $formWorkflowRepository->getById($workflowId);
        $currentType = $formWorkflow->getStepsType();
        $resultingType = $formWorkflow->getStepsType();

        $prevStepType = '';

        /** @var FormWorkflowStep $step */
        foreach ($formWorkflow->getSteps() as $step) {
            $stepType = $step->getType();
            if ('' === $prevStepType) {
                switch ($stepType) {
                    case StepWithCalculatedDateStart::TYPE:
                        $resultingType = FormWorkflowType::STEPSWITHCALCULATEDDATESTART->value;
                        break;
                    case StepWithFixedDateStart::TYPE:
                        $resultingType = FormWorkflowType::STEPSWITHFIXEDDATESTART->value;
                        break;
                }
            } elseif ($prevStepType !== $stepType) {
                $resultingType = FormWorkflowType::STEPSWITHCALCULATEDDATESTARTORFIXEDDATESTART->value;
            }
            $prevStepType = $stepType;
        }

        // -- save the calculated type of the workflow -----------------------------------------
        if ($currentType !== $resultingType) {
            $formWorkflow->setType($resultingType);
            $formWorkflow->makeContentHash();
            $formWorkflowRepository->save($formWorkflow);
        }
    }
}
