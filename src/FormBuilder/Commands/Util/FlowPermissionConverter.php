<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\Util;

use KUL\FormBundle\FormBuilder\Commands\UpdatePermissions\Model\FlowPermission;
use KUL\FormBundle\FormBuilder\Commands\UpdatePermissions\Model\RolePermission;
use KUL\FormBundle\FormBuilder\Commands\UpdatePermissions\Model\StepFlowPermission;

class FlowPermissionConverter
{
    public static function convertToFlowPermission(array $srcFlowPermission): FlowPermission
    {
        // construct the json object for flow permissions
        $flowPermissionData = new FlowPermission();
        foreach ($srcFlowPermission as $stepUid => $value) {
            $step = new StepFlowPermission($stepUid);
            foreach ($value as $role => $permissions) {
                $rolePermission = new RolePermission($permissions['read'], $permissions['write'], $role);
                // a false/false combination does not have to be recorded in the database
                if ($rolePermission->isRead() || $rolePermission->isWrite()) {
                    $step->addRolePermission($rolePermission);
                }
            }
            $flowPermissionData->addStep($step);
        }

        return $flowPermissionData;
    }
}
