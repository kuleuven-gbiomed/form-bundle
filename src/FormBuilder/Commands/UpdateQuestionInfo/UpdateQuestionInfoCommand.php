<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateQuestionInfo;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\QuestionInfoPayload;

/** @extends AbstractUpdateCommand<QuestionInfoPayload> */
class UpdateQuestionInfoCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var QuestionInfoPayload */
    protected PayloadInterface $changeFrom;
    /** @var QuestionInfoPayload */
    protected PayloadInterface $changeTo;
}
