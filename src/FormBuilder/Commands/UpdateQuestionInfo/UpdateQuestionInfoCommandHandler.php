<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateQuestionInfo;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class UpdateQuestionInfoCommandHandler extends CommandHandler
{
    public function __invoke(UpdateQuestionInfoCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $changeTo = $command->getChangeTo();

        // -------------------------------------------------------------------------------------------------------- info
        $info = $formNode->getInfo();

        // -- update question
        $question = $this->htmlSanitizer->sanitize($changeTo->getQuestion());
        if (0 === (is_countable($info['label']) ? count($info['label']) : 0)) {
            if (mb_strlen($question) > 0) {
                $info['label'] = [];
                $info['label'][$command->getLanguage()] = $question;
            }
        } else {
            $info['label'][$command->getLanguage()] = $question;
        }

        // -- update description
        $description = $this->htmlSanitizer->sanitize($changeTo->getDescription());
        if (0 === (is_countable($info['description']) ? count($info['description']) : 0)) {
            if (mb_strlen($description) > 0) {
                $info['description'] = [];
                $info['description'][$command->getLanguage()] = $description;
            }
        } else {
            $info['description'][$command->getLanguage()] = $description;
        }

        // ------------------------------------------------------------------------------------------------- info config
        $infoConfig = $formNode->getInfoConfig();

        // -- update descriptionDisplayMode
        $infoConfig['descriptionDisplayMode'] = $changeTo->getDescriptionDisplayMode();

        // -- update placeholder info
        $placeholder = $this->htmlSanitizer->sanitize($changeTo->getPlaceholder());
        if (0 === (is_array($infoConfig['placeholder']) ? count($infoConfig['placeholder']) : 0)) {
            if (mb_strlen($placeholder) > 0) {
                $infoConfig['placeholder'] = [];
                $infoConfig['placeholder'][$command->getLanguage()] = $placeholder;
            }
        } else {
            if ('' === $placeholder) {
                unset($infoConfig['placeholder'][$command->getLanguage()]);
            } else {
                $infoConfig['placeholder'][$command->getLanguage()] = $placeholder;
            }
        }

        $formNode->setInfo($info);
        $formNode->setInfoConfig($infoConfig);
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
