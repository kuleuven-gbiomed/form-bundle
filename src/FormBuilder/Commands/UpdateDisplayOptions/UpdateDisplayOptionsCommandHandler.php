<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateDisplayOptions;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class UpdateDisplayOptionsCommandHandler extends CommandHandler
{
    public function __invoke(UpdateDisplayOptionsCommand $command): void
    {
        $form = $this->getFormForCommand($command);
        $form->setShowPositionNumbers($command->getChangeTo()->showPositionNumbers);
        $form->setEnableReadOnlyChoiceAnswerOptionsToggling($command->getChangeTo()->enableReadOnlyChoiceAnswerOptionsToggling);
        $form->makeContentHash();
        $this->formRepository->save($form);
    }
}
