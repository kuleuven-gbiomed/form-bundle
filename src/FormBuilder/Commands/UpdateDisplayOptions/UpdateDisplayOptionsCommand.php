<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateDisplayOptions;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\UpdateDisplayOptionsPayload;

/** @extends  AbstractUpdateCommand<UpdateDisplayOptionsPayload> */
class UpdateDisplayOptionsCommand extends AbstractUpdateCommand
{
    /** @var UpdateDisplayOptionsPayload */
    protected PayloadInterface $changeFrom;
    /** @var UpdateDisplayOptionsPayload */
    protected PayloadInterface $changeTo;
}
