<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DuplicateForm;

class DuplicateFormCommand
{
    // ----------------------------------------------------------------
    // Constructor
    // ----------------------------------------------------------------

    private function __construct(
        protected readonly string $sourceUid,
        protected readonly string $targetUid,
    ) {
    }

    // ----------------------------------------------------------------
    // Hydration from JSON
    // ----------------------------------------------------------------

    public static function hydrateFromJson(\stdClass $json): self
    {
        return new self(
            json_decode((string) $json->sourceUid, true, 512, \JSON_THROW_ON_ERROR),
            json_decode((string) $json->targetUid, true, 512, \JSON_THROW_ON_ERROR)
        );
    }

    // ----------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------

    public function getSourceUid(): string
    {
        return $this->sourceUid;
    }

    public function getTargetUid(): string
    {
        return $this->targetUid;
    }
}
