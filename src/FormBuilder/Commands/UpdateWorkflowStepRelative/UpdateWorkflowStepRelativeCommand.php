<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRelative;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\StepNameAndTimingRelativePayload;

/** @extends  AbstractUpdateCommand<StepNameAndTimingRelativePayload> */
class UpdateWorkflowStepRelativeCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var StepNameAndTimingRelativePayload */
    protected PayloadInterface $changeFrom;
    /** @var StepNameAndTimingRelativePayload */
    protected PayloadInterface $changeTo;
}
