<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateWorkflowStepRelative;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

class UpdateWorkflowStepRelativeCommandHandler extends CommandHandler
{
    public function __invoke(UpdateWorkflowStepRelativeCommand $command): void
    {
        $workflowStep = $this->getFormWorkflowStep($command);
        $changeTo = $command->getChangeTo();

        // -- set the info (label) ---------------------------------------------
        $info = $workflowStep->getInfo();
        $label = $this->htmlSanitizer->sanitize($changeTo->getLabel());
        if (0 === (is_countable($info['label']) ? count($info['label']) : 0)) {
            if (mb_strlen($label) > 0) {
                $info['label'] = [];
                $info['label'][$command->getLanguage()] = $label;
            }
        } else {
            $info['label'][$command->getLanguage()] = $label;
        }

        // -- make a period -----------------------------------------------------
        $period = [];
        $period['amountOfDaysToCalculateStartDate'] = (int) $changeTo->getAmountOfDaysToCalculateStartDate();
        $period['amountOfDaysToCalculateEndDate'] = (int) $changeTo->getAmountOfDaysToCalculateEndDate();

        // -- update the properties and save ---------------------------------
        $workflowStep->setNameAndTiming(
            $info,
            $period,
            $changeTo->isAllowsStartIfPreviousStepIsFulfilled()
        );
        $workflowStep->makeContentHash();
        $this->formWorkflowStepRepository->save($workflowStep);
    }
}
