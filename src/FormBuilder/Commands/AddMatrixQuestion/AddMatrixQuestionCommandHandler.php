<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddMatrixQuestion;

use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\OutOfSyncException;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\Common\RelativeNodeTargetDirectionEnum;
use KUL\FormBundle\FormBuilder\Manager\NodeCreationManager;

class AddMatrixQuestionCommandHandler extends CommandHandler
{
    /**
     * @throws SubmissionsExistException
     * @throws OutOfSyncException
     * @throws \JsonException
     * @throws EntityNotFoundException
     */
    public function __invoke(AddMatrixQuestionCommand $command): void
    {
        $matrix = $this->getFormNodeForCommand($command);
        $form = $matrix->getForm();

        $nodeType = $command->getAdd()->getNodeType();
        $label = $command->getAdd()->getLabel();
        $permissionTemplateId = $command->getAdd()->getPermissionTemplateId();
        $language = $command->getLanguage();

        if (RelativeNodeTargetDirectionEnum::ABOVE->value === $command->getAdd()->getTargetDirection()) {
            $insertSequence = $command->colIndex + 1;
        } else {
            $insertSequence = $command->colIndex;
        }

        $nodeCreationManager = new NodeCreationManager($this->entityManager);

        // insert the new node in every subcategory at index
        foreach ($matrix->getChildren() as $row) {
            $nodeCreationManager->createDefaultNodeWithPermissions(
                $nodeType,
                $row,
                $insertSequence,
                $form,
                $label,
                $language,
                $permissionTemplateId
            );
        }
    }
}
