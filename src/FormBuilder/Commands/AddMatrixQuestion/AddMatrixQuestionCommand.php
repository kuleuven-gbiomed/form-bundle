<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\AddMatrixQuestion;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractAddCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\AddNodePayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractAddCommand<AddNodePayload> */
class AddMatrixQuestionCommand extends AbstractAddCommand
{
    use LanguageTrait;

    /** @var AddNodePayload */
    protected PayloadInterface $add;

    public function __construct(
        int $id,
        string $type,
        string $contentHash,
        string $subject,
        bool $required,
        AddNodePayload $add,
        public readonly int $colIndex,
    ) {
        parent::__construct($id, $type, $contentHash, $subject, $required, $add);
    }
}
