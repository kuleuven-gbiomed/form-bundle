<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdatePermissions\Model;

class FlowPermission
{
    private array $steps = [];

    public function __construct()
    {
    }

    public static function convertFromArray(array $stepFlowPermissionArray): self
    {
        $result = new self();
        foreach ($stepFlowPermissionArray as $sourceStep) {
            $step = new StepFlowPermission($sourceStep['stepUid']);
            foreach ($sourceStep['rolePermissions'] as $rolePermission) {
                $rolePermission = new RolePermission(
                    $rolePermission['read'],
                    $rolePermission['write'],
                    $rolePermission['role']
                );
                $step->addRolePermission($rolePermission);
            }
            $result->addStep($step);
        }

        return $result;
    }

    public function addStep(StepFlowPermission $stepFlowPermission): void
    {
        $this->steps[] = $stepFlowPermission;
    }

    public function getSteps(): array
    {
        return $this->steps;
    }
}
