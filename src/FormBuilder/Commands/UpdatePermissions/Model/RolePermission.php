<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdatePermissions\Model;

class RolePermission implements \JsonSerializable
{
    public function __construct(
        private readonly bool $read,
        private readonly bool $write,
        private readonly string $role,
    ) {
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->read = $this->read;
        $json->role = strtoupper($this->role);
        $json->write = $this->write;

        return $json;
    }

    public function isRead(): bool
    {
        return $this->read;
    }

    public function isWrite(): bool
    {
        return $this->write;
    }

    public function getRole(): string
    {
        return $this->role;
    }
}
