<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdatePermissions\Model;

class StepFlowPermission implements \JsonSerializable
{
    private array $rolePermissions = [];

    public function __construct(private readonly string $stepUid)
    {
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->stepUid = $this->stepUid;
        $json->rolePermissions = $this->rolePermissions;

        return $json;
    }

    public function addRolePermission(RolePermission $rolePermission): void
    {
        $this->rolePermissions[] = $rolePermission;
    }

    public function getRolePermissions(): array
    {
        return $this->rolePermissions;
    }

    public function getStepUid(): string
    {
        return $this->stepUid;
    }
}
