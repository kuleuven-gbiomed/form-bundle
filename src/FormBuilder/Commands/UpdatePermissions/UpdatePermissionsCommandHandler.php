<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdatePermissions;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Util\FlowPermissionConverter;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermissionTemplate;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionTemplateRepository;

final class UpdatePermissionsCommandHandler extends CommandHandler
{
    public function __invoke(UpdatePermissionsCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        /** @psalm-var FormNodeFlowPermissionRepository $permissionRepo */
        $permissionRepo = $this->entityManager->getRepository(FormNodeFlowPermission::class);
        /** @psalm-var FormNodeFlowPermissionTemplateRepository $permissionTemplateRepo */
        $permissionTemplateRepo = $this->entityManager->getRepository(FormNodeFlowPermissionTemplate::class);
        $flowPermission = $permissionRepo->getByFormNode($formNode);

        // convert the frontend annotation to the backend annotation
        $flowPermissionData = FlowPermissionConverter::convertToFlowPermission($command->getChangeTo()->getFlowPermission());

        // if connected to a permission-template, save the connection to that template in the database
        if (0 === $command->getChangeTo()->getTemplateId()) {
            $flowPermission->removeTemplate();
        } else {
            $template = $permissionTemplateRepo->getById($command->getChangeTo()->getTemplateId());
            $flowPermission->setTemplate($template);
        }

        // save the flow permission information
        $flowPermission->setFlowPermission($flowPermissionData->getSteps());
        $flowPermission->makeContentHash();
        $permissionRepo->save($flowPermission);

        // touch the connected form node for an updated content hash
        $formNode->touch();
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
