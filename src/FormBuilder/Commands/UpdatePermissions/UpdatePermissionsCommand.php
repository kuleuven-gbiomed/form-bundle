<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdatePermissions;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PermissionPayload;

/** @extends AbstractUpdateCommand<PermissionPayload> */
class UpdatePermissionsCommand extends AbstractUpdateCommand
{
    /** @var PermissionPayload */
    protected PayloadInterface $changeFrom;
    /** @var PermissionPayload */
    protected PayloadInterface $changeTo;
}
