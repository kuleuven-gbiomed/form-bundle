<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteNode;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\CannotDeleteLastEditableNodeException;
use KUL\FormBundle\FormBuilder\Manager\NodeManager;

final class DeleteNodeCommandHandler extends CommandHandler
{
    public function __invoke(DeleteNodeCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $form = $formNode->getForm();
        $nodeCount = $form->getNodes()->count();
        if ($nodeCount < 3) { // this is the last editable node
            throw new CannotDeleteLastEditableNodeException();
        }

        if ($formNode->getForm()->getId() !== $command->getDelete()->getFormId()) {
            throw new \Exception('Form Id is not matching with the command information.');
        }

        $nodeManager = new NodeManager($this->entityManager);
        $nodeManager->deleteNodeRecursively($formNode);
    }
}
