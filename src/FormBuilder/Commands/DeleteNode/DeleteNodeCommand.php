<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteNode;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractDeleteCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\DeleteNodePayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractDeleteCommand<DeleteNodePayload> */
class DeleteNodeCommand extends AbstractDeleteCommand
{
    /** @var DeleteNodePayload */
    protected PayloadInterface $delete;
}
