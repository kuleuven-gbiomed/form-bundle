<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateScoreProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ScorePropertiesPayload;
use KUL\FormBundle\FormBuilder\Entity\FormNode;

final class UpdateScorePropertiesCommandHandler extends CommandHandler
{
    public function __invoke(UpdateScorePropertiesCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $changeTo = $command->getChangeTo();

        // -- validate formnode & command
        if (!$this->validateFormNode($formNode)) {
            throw new \Exception(AbstractCommand::COMMAND_NOT_VALID);
        }
        if (!$this->validateCommand($changeTo, $formNode)) {
            throw new \Exception(AbstractCommand::COMMAND_NOT_VALID);
        }

        // -- update the scoring parameters
        $scoringConfig = $formNode->getScoringConfig();
        $scoringConfig['scoringParameters']['scale'] = $changeTo->getScale();
        $scoringConfig['scoringParameters']['roundingMode'] = $changeTo->getRoundingMode();
        $scoringConfig['scoringParameters']['minScore'] = $changeTo->getMinScore();
        $scoringConfig['scoringParameters']['maxScore'] = $changeTo->getMaxScore();
        $formNode->setScoringConfig($scoringConfig);
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }

    private function validateFormNode(FormNode $formNode): bool
    {
        // -- check if the form node is valid for changing the score properties...
        // -- must be a choice question
        // -- not multiple

        if (FormNode::SUB_TYPE_CHOICE === $formNode->getSubType()) {
            if (true === $formNode->getPropsConfig()['multiple']) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    private function validateCommand(ScorePropertiesPayload $changeTo, FormNode $formNode): bool
    {
        if ($changeTo->getMinScore() >= $changeTo->getMaxScore()) {
            return false;
        }
        // rounding mode is 1 or 2
        if (!(1 === $changeTo->getRoundingMode() || 2 === $changeTo->getRoundingMode())) {
            return false;
        }
        // scale is 0,1,2 or 3
        if (!(0 === $changeTo->getScale()
            || 1 === $changeTo->getScale()
            || 2 === $changeTo->getScale()
            || 3 === $changeTo->getScale())) {
            return false;
        }
        if ($formNode->getScoringConfig()['scoringParameters']['passScore'] > $changeTo->getMaxScore()) {
            return false;
        }

        return true;
    }
}
