<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateScoreProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ScorePropertiesPayload;

/** @extends  AbstractUpdateCommand<ScorePropertiesPayload> */
class UpdateScorePropertiesCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var ScorePropertiesPayload */
    protected PayloadInterface $changeFrom;
    /** @var ScorePropertiesPayload */
    protected PayloadInterface $changeTo;
}
