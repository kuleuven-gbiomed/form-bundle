<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteChartRelation;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;
use KUL\FormBundle\FormBuilder\Commands\Util\ChartUtil;

class DeleteChartRelationCommandHandler extends CommandHandler
{
    public function __invoke(DeleteChartRelationCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        $delete = $command->getDelete();
        $participantNode = $this->formNodeRepository->getById($delete->getParticipantId());
        $chartNode = $this->formNodeRepository->getById($delete->getChartId());

        $propsConfig = $chartNode->getPropsConfig();
        $questionReferences = $propsConfig['question_references'];
        $participantNodeIndex = ChartUtil::findInQuestionReferences($participantNode->getUid(), $questionReferences);

        if (!(-1 === $participantNodeIndex)) {
            array_splice($questionReferences, $participantNodeIndex, 1);
            $propsConfig['question_references'] = $questionReferences;
        }

        $chartNode->setPropsConfig($propsConfig);
        $chartNode->makeContentHash();
        $this->formNodeRepository->save($chartNode);

        $participantNode->touch();
        $participantNode->makeContentHash();
        $this->formNodeRepository->save($participantNode);
    }
}
