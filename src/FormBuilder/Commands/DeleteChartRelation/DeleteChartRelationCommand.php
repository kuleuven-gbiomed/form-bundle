<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\DeleteChartRelation;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractDeleteCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\ChartRelationPayload;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;

/** @extends AbstractDeleteCommand<ChartRelationPayload> */
class DeleteChartRelationCommand extends AbstractDeleteCommand
{
    /** @var ChartRelationPayload */
    protected PayloadInterface $delete;
}
