<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateQuestionRequired;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class UpdateQuestionRequiredCommandHandler extends CommandHandler
{
    public function __invoke(UpdateQuestionRequiredCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        // ----------------------------------------------------------------------------------------- execute the command
        $changeToRequired = $command->getChangeTo()->isRequired();
        $formNode->setRequired($changeToRequired);

        // Special case for text and choice questions ----------------------------------------------------------------
        $propsConfig = $formNode->getPropsConfig();
        if ($formNode->getSubType() === $formNode::SUB_TYPE_TEXT) {
            if (0 === $propsConfig['minLength'] && $changeToRequired) {
                $propsConfig['minLength'] = 1;
                $formNode->setPropsConfig($propsConfig);
            }
            if (0 < $propsConfig['minLength'] && !$changeToRequired) {
                $propsConfig['minLength'] = 0;
                $formNode->setPropsConfig($propsConfig);
            }
        }

        if ($formNode->getSubType() === $formNode::SUB_TYPE_CHOICE) {
            if (0 === $propsConfig['minSelect'] && $changeToRequired) {
                $propsConfig['minSelect'] = 1;
                $formNode->setPropsConfig($propsConfig);
            } elseif (0 < $propsConfig['minSelect'] && !$changeToRequired) {
                $propsConfig['minSelect'] = 0;
                $formNode->setPropsConfig($propsConfig);
            }
        }
        // -------------------------------------------------------------------------------------------------------------

        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
