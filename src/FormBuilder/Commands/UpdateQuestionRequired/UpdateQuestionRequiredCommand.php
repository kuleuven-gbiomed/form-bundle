<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateQuestionRequired;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\QuestionRequiredPayload;

/** @extends  AbstractUpdateCommand<QuestionRequiredPayload> */
class UpdateQuestionRequiredCommand extends AbstractUpdateCommand
{
    /** @var QuestionRequiredPayload */
    protected PayloadInterface $changeFrom;
    /** @var QuestionRequiredPayload */
    protected PayloadInterface $changeTo;
}
