<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateTextProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class UpdateTextPropertiesCommandHandler extends CommandHandler
{
    public function __invoke(UpdateTextPropertiesCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);

        // -- execute the command
        $changeTo = $command->getChangeTo();

        // -- update props config
        $propsConfig = $formNode->getPropsConfig();
        $propsConfig['multiLine'] = $changeTo->isMultiLine();
        $propsConfig['minLength'] = $changeTo->getMinLength();
        $propsConfig['maxLength'] = $changeTo->getMaxLength();

        // -- update display mode
        if ($changeTo->isMultiLine()) {
            $formNode->setDisplayMode($changeTo->getDisplayMode());
        } else {
            $formNode->setDisplayMode('regular');
        }

        if ($changeTo->getMinLength() > 0) {
            $formNode->setRequired(true);
        }
        if ($command->getChangeFrom()->getMinLength() > 0 && 0 === $changeTo->getMinLength()) {
            $formNode->setRequired(false);
        }

        // -- update content hash and save
        $formNode->setPropsConfig($propsConfig);
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
