<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateTextProperties;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Common\LanguageTrait;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\TextPropertiesPayload;

/** @extends AbstractUpdateCommand<TextPropertiesPayload> */
class UpdateTextPropertiesCommand extends AbstractUpdateCommand
{
    use LanguageTrait;

    /** @var TextPropertiesPayload */
    protected PayloadInterface $changeFrom;
    /** @var TextPropertiesPayload */
    protected PayloadInterface $changeTo;
}
