<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateOptionSort;

use KUL\FormBundle\FormBuilder\Commands\Common\AbstractUpdateCommand;
use KUL\FormBundle\FormBuilder\Commands\Payloads\PayloadInterface;
use KUL\FormBundle\FormBuilder\Commands\Payloads\SortOptionPayload;

/** @extends AbstractUpdateCommand<SortOptionPayload> */
class UpdateOptionSortCommand extends AbstractUpdateCommand
{
    /** @var SortOptionPayload */
    protected PayloadInterface $changeFrom;
    /** @var SortOptionPayload */
    protected PayloadInterface $changeTo;
}
