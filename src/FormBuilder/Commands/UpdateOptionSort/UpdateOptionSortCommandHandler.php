<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Commands\UpdateOptionSort;

use KUL\FormBundle\FormBuilder\Commands\Common\CommandHandler;

final class UpdateOptionSortCommandHandler extends CommandHandler
{
    public function __invoke(UpdateOptionSortCommand $command): void
    {
        $formNode = $this->getFormNodeForCommand($command);
        $changeTo = $command->getChangeTo();

        // -- change the sequence
        $sequence = 0;
        foreach ($changeTo->getOptions() as $option) {
            $optionEntity = $this->formNodeOptionRepository->getById($option['id']);
            $optionEntity->setSequence($sequence);
            $optionEntity->makeContentHash();
            $this->formNodeOptionRepository->save($optionEntity);
            ++$sequence;
        }

        // -- touch the parent node
        $formNode->touch();
        $formNode->makeContentHash();
        $this->formNodeRepository->save($formNode);
    }
}
