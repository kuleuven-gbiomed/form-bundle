<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

/**
 * This defines the relationship of the source to the target node.
 */
enum FormNodeRelationType: string
{
    case UNLOCKS = 'unlocks';
    case UNLOCKABLE_BY = 'unlockable by';
    case SCORE_FOR = 'score for';
    case CALCULATION_OF = 'calculation of';
    case RADAR_CHART_SOURCE = 'radar chart source';
    case RADAR_CHART_THAT_USES = 'radar chart that uses';
    case PREFILLS = 'prefills';
    case PREFILLED_BY = 'prefilled by';
}
