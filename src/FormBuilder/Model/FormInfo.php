<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

use KUL\FormBundle\FormBuilder\Entity\FormInfo as FormInfoEntity;

class FormInfo implements \JsonSerializable
{
    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        private readonly int $id,
        private readonly array $label,
        private readonly array $description,
        private readonly array $roleDescriptions,
        private readonly string $contentHash,
    ) {
    }

    // -----------------------------------------------------------------------------------------------------------------
    // hydration
    // -----------------------------------------------------------------------------------------------------------------

    public static function hydrateFromEntity(FormInfoEntity $entity): self
    {
        return new self(
            $entity->getId(),
            $entity->getLabel(),
            $entity->getDescription(),
            $entity->getRoleDescriptions(),
            $entity->getContentHash()
        );
    }

    // -----------------------------------------------------------------------------------------------------------------
    // what to render as json
    // -----------------------------------------------------------------------------------------------------------------

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->label = $this->label;
        $json->description = $this->description;
        $json->roleDescriptions = $this->roleDescriptions;
        $json->contentHash = $this->contentHash;

        return $json;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // what to construct back to working version
    // -----------------------------------------------------------------------------------------------------------------

    public function constructJson(): \stdClass
    {
        $info = new \stdClass();
        $info->label = $this->label;
        $info->description = $this->description;
        $info->roleDescriptions = $this->roleDescriptions;

        return $info;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getId(): int
    {
        return $this->id;
    }

    public function getLabel(): array
    {
        return $this->label;
    }

    public function getDescription(): array
    {
        return $this->description;
    }

    public function getRoleDescriptions(): array
    {
        return $this->roleDescriptions;
    }

    public function getContentHash(): string
    {
        return $this->contentHash;
    }
}
