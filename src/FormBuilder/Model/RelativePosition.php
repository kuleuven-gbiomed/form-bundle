<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

enum RelativePosition: string
{
    case Above = 'above';
    case Below = 'below';
}
