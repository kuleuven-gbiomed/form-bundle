<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

enum FormWorkflowType: string
{
    case STEPSWITHFIXEDDATESTART = 'stepsWithFixedDateStart';
    case STEPSWITHCALCULATEDDATESTART = 'stepsWithCalculatedDateStart';
    case STEPSWITHCALCULATEDDATESTARTORFIXEDDATESTART = 'stepsWithCalculatedDateStartOrFixedDateStart';
}
