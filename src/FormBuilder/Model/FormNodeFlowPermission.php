<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission as FormNodeFlowPermissionEntity;

class FormNodeFlowPermission
{
    // ------------------------------------------------------------------------------------------------------- constants
    final public const DEFAULT_CATEGORY = 'default-category';
    final public const QUESTION = 'question';
    final public const OPTION = 'option';

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        private readonly int $id,
        private readonly string $type,
        private readonly array $nodeFlowPermission,
        private readonly string $contentHash,
    ) {
    }

    // -----------------------------------------------------------------------------------------------------------------
    // hydration
    // -----------------------------------------------------------------------------------------------------------------

    public static function hydrateFromEntity(FormNodeFlowPermissionEntity $entity): self
    {
        return new self(
            $entity->getId(),
            $entity->getType(),
            $entity->getFlowPermission(),
            $entity->getContentHash()
        );
    }

    // -----------------------------------------------------------------------------------------------------------------
    // what to render as json
    // -----------------------------------------------------------------------------------------------------------------

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->type = $this->type;
        $json->nodeFlowPermission = $this->nodeFlowPermission;
        $json->contentHash = $this->contentHash;

        return $json;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getNodeFlowPermission(): array
    {
        return $this->nodeFlowPermission;
    }

    public function getRolePermissionsForStepUid(string $stepUid): array
    {
        foreach ($this->nodeFlowPermission as $stepPermission) {
            if ($stepPermission['stepUid'] === $stepUid) {
                return $stepPermission['rolePermissions'];
            }
        }

        return [];
    }

    /** @return FormNode::PERMISSION_WRITABLE|FormNode::PERMISSION_READ_ONLY|null*/
    public function getPermissionForRoleAndStepUid(string $role, string $stepUid): ?string
    {
        $rolePermissions = $this->getRolePermissionsForStepUid($stepUid);
        foreach ($rolePermissions as $rolePermission) {
            if ($rolePermission['role'] === $role) {
                if ($rolePermission['write']) {
                    return FormNode::PERMISSION_WRITABLE;
                } elseif ($rolePermission['read']) {
                    return FormNode::PERMISSION_READ_ONLY;
                }
            }
        }

        return null;
    }

    public function getContentHash(): string
    {
        return $this->contentHash;
    }
}
