<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

class FormTemplate implements \JsonSerializable
{
    // -----------------------------------------------------------------------------------------------------------------
    // constructor & maker
    // -----------------------------------------------------------------------------------------------------------------

    private function __construct(
        private readonly int $id,
        private readonly string $uid,
        private readonly string $title,
        private readonly int $currentVersion,
        private readonly bool $active,
        private readonly string $status,
        private readonly ?\DateTimeImmutable $editingStartedAt,
        private readonly ?string $editingStartedBy,
    ) {
    }

    public static function make(
        int $id,
        string $uid,
        string $title,
        int $currentVersion,
        bool $active,
        string $status,
        ?\DateTimeImmutable $editingStartedAt,
        ?string $editingStartedBy,
    ): self {
        return new self($id, $uid, $title, $currentVersion, $active, $status, $editingStartedAt, $editingStartedBy);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // what to render as json
    // -----------------------------------------------------------------------------------------------------------------

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->id = $this->id;
        $json->uid = $this->uid;
        $json->title = $this->title;
        $json->currentVersion = $this->currentVersion;
        $json->active = $this->active;
        $json->status = $this->status;
        $json->editingStartedAt = $this->editingStartedAt;
        $json->editingStartedBy = $this->editingStartedBy;

        return $json;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getId(): int
    {
        return $this->id;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getCurrentVersion(): int
    {
        return $this->currentVersion;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getEditingStartedAt(): ?\DateTimeImmutable
    {
        return $this->editingStartedAt;
    }

    public function getEditingStartedBy(): ?string
    {
        return $this->editingStartedBy;
    }
}
