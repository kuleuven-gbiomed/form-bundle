<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

class FormTemplateStatus
{
    final public const ONLINE = 'online';
    final public const EDITING = 'editing';
}
