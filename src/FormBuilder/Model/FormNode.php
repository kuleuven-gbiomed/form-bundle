<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

use KUL\FormBundle\FormBuilder\Entity\FormNode as FormNodeEntity;
use KUL\FormBundle\FormBuilder\Queries\GetQuestionRadarChartParticipants\RadarChartParticipant;

class FormNode implements \JsonSerializable
{
    // ------------------------------------------------------------------------------------------------------- constants
    final public const TYPE_CATEGORY = 'category';
    final public const TYPE_QUESTION = 'question';

    final public const SUB_TYPE_CATEGORY = 'category';
    final public const SUB_TYPE_CHOICE = 'question_choice';
    final public const SUB_TYPE_NUMBER = 'question_number';
    final public const SUB_TYPE_TEXT = 'question_text';
    final public const SUB_TYPE_UPLOAD = 'question_upload';
    final public const SUB_TYPE_DATE = 'question_date';
    final public const SUB_TYPE_RADAR_CHART = 'read_only_view_radar_chart';

    final public const PERMISSION_READ_ONLY = 'readonly';
    final public const PERMISSION_WRITABLE = 'writeable';

    private ?string $permission = null;

    private array $children = [];
    private array $options = [];

    // ----------------------------------------------------------------------------------------------- calculated fields
    // The nullable fields below require extra queries so these can be set manually.
    private ?bool $isLockedQuestion = null;
    private ?bool $isUnlockerQuestion = null;
    private bool $isScoreQuestionForCalculation = false;
    private bool $isCalculatedQuestion = false;
    private ?bool $isChartParticipantQuestion = null;
    private ?bool $isPrefillingQuestion = null;
    private bool $isPrefilledQuestion = false;

    /** @var list<RadarChartParticipant> */
    private array $chartParticipantQuestions = [];

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        private readonly int $id,
        private readonly int $sequence,
        private readonly int $level,
        private readonly array $position,
        private readonly string $uid,
        private readonly string $type,
        private readonly string $subType,
        private readonly bool $required,
        private readonly array $info,
        private readonly bool $hidden,
        private readonly string $displayMode,
        private readonly array $firstHeaderCellTitle,
        private readonly string $contentHash,
        private readonly array $infoConfig = [],
        private readonly array $propsConfig = [],
        private readonly array $scoringConfig = [],
        private readonly array $prefillConfig = [],
        private readonly array $multiUnlockConfig = [],
    ) {
        if (self::SUB_TYPE_CHOICE === $subType) {
            $this->options = [];
        }

        if (self::TYPE_QUESTION === $type) {
            $this->isCalculatedQuestion = $this->isCalculatedQuestion();
            $this->isPrefilledQuestion = $this->isPrefilledQuestion();
            $this->permission = self::PERMISSION_WRITABLE;
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // hydration
    // -----------------------------------------------------------------------------------------------------------------

    public static function hydrateFromEntity(FormNodeEntity $entity): self
    {
        return new self(
            $entity->getId(),
            $entity->getSequence(),
            $entity->getLevel(),
            $entity->getPosition(),
            trim($entity->getUid()),
            trim($entity->getType()),
            trim($entity->getSubType()),
            $entity->getRequired(),
            $entity->getInfo(),
            $entity->getHidden(),
            $entity->getDisplayMode(),
            $entity->getFirstHeaderCellTitle(),
            $entity->getContentHash(),
            $entity->getInfoConfig(),
            $entity->getPropsConfig(),
            $entity->getScoringConfig(),
            $entity->getPrefillConfig(),
            $entity->getMultiUnlockConfig(),
        );
    }

    // -----------------------------------------------------------------------------------------------------------------
    // what to render as json
    // -----------------------------------------------------------------------------------------------------------------

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->id = $this->id;
        $json->sequence = $this->sequence;
        $json->level = $this->level;
        $json->position = $this->position;
        $json->uid = $this->uid;
        $json->type = $this->type;
        $json->subType = $this->subType;
        $json->required = $this->required;
        $json->info = $this->info;
        $json->hidden = $this->hidden;
        $json->displayMode = $this->displayMode;
        $json->firstHeaderCellTitle = $this->firstHeaderCellTitle;

        if (self::TYPE_QUESTION === $this->type) {
            $json->configInfo = $this->infoConfig;
            $json->configProps = $this->propsConfig;
            $json->configScoring = $this->scoringConfig;
            $json->configPrefill = $this->prefillConfig;
            $json->configMultiUnlock = $this->multiUnlockConfig;
            $json->isLockedQuestion = $this->isLockedQuestion;
            $json->isUnlockerQuestion = $this->isUnlockerQuestion;
            $json->isScoreQuestion = $this->isScoreQuestion();
            $json->isScoreQuestionForCalculation = $this->isScoreQuestionForCalculation;
            $json->isCalculatedQuestion = $this->isCalculatedQuestion;
            $json->isPrefilledQuestion = $this->isPrefilledQuestion;
            $json->isPrefillingQuestion = $this->isPrefillingQuestion;
            $json->isChartParticipantQuestion = $this->isChartParticipantQuestion;
            $json->permission = $this->permission;
        }

        if (self::SUB_TYPE_DATE === $this->getSubType()) {
            if (false === isset($this->propsConfig['prefillWithToday'])) {
                $json->configProps['prefillWithToday'] = false;
            }
        }

        if (self::SUB_TYPE_RADAR_CHART === $this->subType) {
            $json->chartParticipantQuestions = $this->chartParticipantQuestions;
        }

        $json->contentHash = $this->contentHash;
        $json->children = $this->children;

        if (self::SUB_TYPE_CHOICE === $this->subType) {
            $json->options = $this->options;
        }

        return $json;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // setters
    // -----------------------------------------------------------------------------------------------------------------

    public function addChild(self $formNode): void
    {
        $this->children[] = $formNode;
    }

    public function addOption(FormNodeOption $formNodeOption): void
    {
        if (self::SUB_TYPE_CHOICE === $this->subType) {
            $this->options[] = $formNodeOption;
        }
    }

    public function makeReadOnly(): void
    {
        $this->permission = self::PERMISSION_READ_ONLY;
    }

    /** @param self::PERMISSION_READ_ONLY|self::PERMISSION_WRITABLE|null $permission */
    public function setPermission(?string $permission): void
    {
        $this->permission = $permission;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // set (and overrule) some calculated fields
    // -----------------------------------------------------------------------------------------------------------------

    public function setLockedQuestion(bool $isLockedQuestion): void
    {
        $this->isLockedQuestion = $isLockedQuestion;
    }

    public function setUnlockerQuestion(bool $isUnLockerQuestion): void
    {
        $this->isUnlockerQuestion = $isUnLockerQuestion;
    }

    public function setIsScoreQuestionForCalculation(bool $isScoreQuestionForCalculation): void
    {
        $this->isScoreQuestionForCalculation = $isScoreQuestionForCalculation;
    }

    public function setIsChartParticipantQuestion(bool $isChartParticipantQuestion): void
    {
        $this->isChartParticipantQuestion = $isChartParticipantQuestion;
    }

    public function setIsPrefillingQuestion(bool $isPrefillingQuestion): void
    {
        $this->isPrefillingQuestion = $isPrefillingQuestion;
    }

    /** @param list<RadarChartParticipant> $chartParticipantQuestions */
    public function setChartParticipantQuestions(array $chartParticipantQuestions): void
    {
        $this->chartParticipantQuestions = $chartParticipantQuestions;
    }
    // -----------------------------------------------------------------------------------------------------------------
    // calculated fields
    // -----------------------------------------------------------------------------------------------------------------

    public function isScoreQuestion(): bool
    {
        $scoringConfig = $this->getScoringConfig();

        return array_key_exists('scoringParameters', $scoringConfig)
            && null !== $scoringConfig['scoringParameters']['minScore']
            && null !== $scoringConfig['scoringParameters']['maxScore'];
    }

    private function isCalculatedQuestion(): bool
    {
        $result = false;
        $scoringConfig = $this->getScoringConfig();
        if (!(0 === count($scoringConfig)) && isset($scoringConfig['globalScoreCalculationDependencyConfigs'])) {
            if (!(0 === (is_countable($scoringConfig['globalScoreCalculationDependencyConfigs']) ? count($scoringConfig['globalScoreCalculationDependencyConfigs']) : 0))) {
                $result = true;
            }
        }
        if (isset($scoringConfig['isCalculation'])) {
            $result = $scoringConfig['isCalculation'];
        }

        return $result;
    }

    private function isPrefilledQuestion(): bool
    {
        return isset($this->prefillConfig['prefillingQuestionUids'])
            && (is_countable($this->prefillConfig['prefillingQuestionUids'])
                ? count($this->prefillConfig['prefillingQuestionUids'])
                : 0) > 0;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getId(): int
    {
        return $this->id;
    }

    public function getSequence(): int
    {
        return $this->sequence;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isCategory(): bool
    {
        return self::TYPE_CATEGORY === $this->type;
    }

    public function getSubType(): string
    {
        return $this->subType;
    }

    public function getRequired(): bool
    {
        return $this->required;
    }

    public function getInfo(): array
    {
        return $this->info;
    }

    public function getHidden(): bool
    {
        return $this->hidden;
    }

    public function getDisplayMode(): string
    {
        return $this->displayMode;
    }

    public function getFirstHeaderCellTitle(): array
    {
        return $this->firstHeaderCellTitle;
    }

    public function getInfoConfig(): array
    {
        return $this->infoConfig;
    }

    public function getPropsConfig(): array
    {
        return $this->propsConfig;
    }

    public function getScoringConfig(): array
    {
        return $this->scoringConfig;
    }

    public function getPrefillConfig(): array
    {
        return $this->prefillConfig;
    }

    public function getContentHash(): string
    {
        return $this->contentHash;
    }

    public function getPermission(): ?string
    {
        return $this->permission;
    }

    public function getPosition(): array
    {
        return $this->position;
    }

    /** @return list<RadarChartParticipant> */
    public function getChartParticipantQuestions(): array
    {
        return $this->chartParticipantQuestions;
    }
}
