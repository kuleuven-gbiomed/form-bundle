<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflowStep as FormWorkflowStepEntity;
use KUL\FormBundle\FormBuilder\Queries\Common\InfoUtil;

class FormWorkFlowStep implements \JsonSerializable
{
    public function __construct(
        private readonly int $id,
        private readonly string $uid,
        private readonly string $type,
        private readonly int $sequence,
        private readonly array $info,
        private readonly array $period,
        private readonly bool $allowsStartIfPreviousStepIsFulfilled,
        private readonly array $rolesWithOneSubmit,
        private readonly array $submitSuccessUserInteraction,
        private readonly bool $hideReadOnlyNodesOnLanding,
        private readonly array $custom_options,
        private readonly string $contentHash,
        private readonly int $x,
        private readonly int $y,
    ) {
    }

    // -----------------------------------------------------------------------------------------------------------------
    // What to render as json
    // -----------------------------------------------------------------------------------------------------------------

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->id = $this->id;
        $json->uid = $this->uid;
        $json->type = $this->type;
        $json->sequence = $this->sequence;
        $json->info = InfoUtil::formatInfoForRendering($this->info);
        $json->allowsStartIfPreviousStepIsFulfilled = $this->allowsStartIfPreviousStepIsFulfilled;
        $json->period = $this->period;
        $json->contentHash = $this->contentHash;
        $json->x = $this->x;
        $json->y = $this->y;

        return $json;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // What to construct
    // -----------------------------------------------------------------------------------------------------------------

    public function constructJson(): \stdClass
    {
        $step = new \stdClass();
        $step->type = $this->type;
        $step->uid = $this->uid;
        $step->period = $this->period;
        $step->allowsStartIfPreviousStepIsFulfilled = $this->allowsStartIfPreviousStepIsFulfilled;
        $step->rolesWithOneSubmit = $this->rolesWithOneSubmit;
        $step->info = $this->info;
        $step->submitSuccessUserInteraction = $this->submitSuccessUserInteraction;
        $step->hideReadOnlyNodesOnLanding = $this->hideReadOnlyNodesOnLanding;
        $step->custom_options = $this->custom_options;

        return $step;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // What to render a formatted, normalised detail object
    // -----------------------------------------------------------------------------------------------------------------

    public function formatDetail(): \stdClass
    {
        $step = new \stdClass();
        $step->id = $this->id;
        $step->type = $this->type;
        $step->sequence = $this->sequence;
        $step->uid = $this->uid;
        $step->contentHash = $this->contentHash;

        if (StepWithFixedDateStart::TYPE === $this->type) {
            $period = [];
            $start = new \DateTime(trim((string) $this->period['fixedStartDate']));
            $end = new \DateTime(trim((string) $this->period['fixedEndDate']));
            $period['fixedStartDate'] = $start->format(\DATE_ATOM);
            $period['fixedEndDate'] = $end->format(\DATE_ATOM);
            $step->period = $period;
        } else {
            $step->period = $this->period;
        }

        $step->info = InfoUtil::formatInfoForRendering($this->info);

        $step->allowsStartIfPreviousStepIsFulfilled = $this->allowsStartIfPreviousStepIsFulfilled;
        $step->rolesWithOneSubmit = $this->rolesWithOneSubmit;

        $step->submitSuccessUserInteraction = $this->submitSuccessUserInteraction;

        $step->hideReadOnlyNodesOnLanding = $this->hideReadOnlyNodesOnLanding;
        $step->custom_options = $this->custom_options;

        return $step;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Hydration
    // -----------------------------------------------------------------------------------------------------------------

    public static function hydrateFromEntity(FormWorkflowStepEntity $entity): self
    {
        return new self(
            $entity->getId(),
            $entity->getUid(),
            $entity->getType(),
            $entity->getSequence(),
            $entity->getInfo(),
            $entity->getPeriod(),
            $entity->getAllowsStartIfPreviousStepIsFulfilled(),
            $entity->getRolesWithOneSubmit(),
            $entity->getSubmitSuccessUserInteraction(),
            $entity->getHideReadOnlyNodesOnLanding(),
            $entity->getCustomOptions(),
            $entity->getContentHash(),
            $entity->getX(),
            $entity->getY()
        );
    }
}
