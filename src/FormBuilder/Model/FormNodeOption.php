<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

use KUL\FormBundle\FormBuilder\Entity\FormNodeOption as FormNodeOptionEntity;

class FormNodeOption implements \JsonSerializable
{
    // ------------------------------------------------------------------------------------------------------- constants
    final public const TYPE_QUESTION_OPTION = 'question_option';

    final public const INPUT_TYPE_OPTION = 'option';
    final public const INPUT_TYPE_OPTION_WITH_ADDITIONAL_TEXT = 'optionWithAdditionalText';

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------
    public function __construct(
        private readonly int $id,
        private readonly int $sequence,
        private readonly string $uid,
        private readonly string $type,
        private readonly bool $required,
        private readonly string $displayMode,
        private readonly bool $defaultSelected,
        private readonly bool $hideLabelInRelevantChoiceDisplayMode,
        private readonly array $info,
        private readonly bool $hidden,
        private readonly string $descriptionDisplayMode,
        private readonly array $unlocksQuestionIds,
        private readonly array $multiUnlocksQuestionIds,
        private readonly string $inputType,
        private readonly array $propsConfig,
        private readonly string $contentHash,
        private readonly ?float $score,
    ) {
    }

    // -----------------------------------------------------------------------------------------------------------------
    // hydration
    // -----------------------------------------------------------------------------------------------------------------

    public static function hydrateFromEntity(FormNodeOptionEntity $entity): self
    {
        return new self(
            $entity->getId(),
            $entity->getSequence(),
            $entity->getUid(),
            $entity->getType(),
            $entity->isRequired(),
            $entity->getDisplayMode(),
            $entity->isDefaultSelected(),
            $entity->getHideLabelInRelevantChoiceDisplayMode(),
            $entity->getInfo(),
            $entity->getHidden(),
            $entity->getDescriptionDisplayMode(),
            $entity->getUnlocksQuestionIds(),
            $entity->getMultiUnlocksQuestionIds(),
            $entity->getInputType(),
            $entity->getPropsConfig(),
            $entity->getContentHash(),
            $entity->getScore(),
        );
    }

    // -----------------------------------------------------------------------------------------------------------------
    // what to render as json
    // -----------------------------------------------------------------------------------------------------------------

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->id = $this->id;
        $json->sequence = $this->sequence;
        $json->uid = $this->uid;
        $json->type = $this->type;
        $json->required = $this->required;
        $json->displayMode = $this->displayMode;
        $json->defaultSelected = $this->defaultSelected;
        $json->score = $this->score;
        $json->hideLabelInRelevantChoiceDisplayMode = $this->hideLabelInRelevantChoiceDisplayMode;
        $json->info = $this->info;
        $json->hidden = $this->hidden;
        $json->descriptionDisplayMode = $this->descriptionDisplayMode;

        // -- format unlocksQuestionIds as list (force) -------------------------------
        $listIds = [];
        foreach ($this->unlocksQuestionIds as $value) {
            $listIds[] = $value;
        }
        $json->unlocksQuestionIds = $listIds;

        $listIds = [];
        foreach ($this->multiUnlocksQuestionIds as $value) {
            $listIds[] = $value;
        }
        $json->multiUnlocksQuestionIds = $listIds;

        $json->inputType = $this->inputType;
        $json->configProps = $this->propsConfig;
        $json->contentHash = $this->contentHash;

        return $json;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getId(): int
    {
        return $this->id;
    }

    public function getSequence(): int
    {
        return $this->sequence;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getRequired(): bool
    {
        return $this->required;
    }

    public function getDisplayMode(): string
    {
        return $this->displayMode;
    }

    public function getDefaultSelected(): bool
    {
        return $this->defaultSelected;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function getHideLabelInRelevantChoiceDisplayMode(): bool
    {
        return $this->hideLabelInRelevantChoiceDisplayMode;
    }

    public function getInfo(): array
    {
        return $this->info;
    }

    public function getHidden(): bool
    {
        return $this->hidden;
    }

    public function getDescriptionDisplayMode(): string
    {
        return $this->descriptionDisplayMode;
    }

    public function getUnlocksQuestionIds(): array
    {
        return $this->unlocksQuestionIds;
    }

    public function getMultiUnlocksQuestionIds(): array
    {
        return $this->multiUnlocksQuestionIds;
    }

    public function getPropsConfig(): array
    {
        return $this->propsConfig;
    }

    public function getInputType(): string
    {
        return $this->inputType;
    }
}
