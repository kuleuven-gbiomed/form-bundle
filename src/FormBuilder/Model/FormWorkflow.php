<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

use KUL\FormBundle\FormBuilder\Entity\FormWorkflow as FormWorkflowEntity;

class FormWorkflow implements \JsonSerializable
{
    public function __construct(
        private readonly int $id,
        private readonly string $stepsType,
        private readonly array $steps,
        private readonly string $contentHash,
    ) {
    }

    // -----------------------------------------------------------------------------------------------------------------
    // what to render as json
    // -----------------------------------------------------------------------------------------------------------------

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->id = $this->id;
        $json->stepsType = $this->stepsType;
        $json->contentHash = $this->contentHash;
        $json->steps = $this->steps;

        return $json;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // construction
    // -----------------------------------------------------------------------------------------------------------------

    public function constructJson(): \stdClass
    {
        $workFlow = new \stdClass();
        $workFlow->stepsType = $this->stepsType;
        $workFlow->steps = [];
        foreach ($this->steps as $step) {
            $workFlow->steps[] = $step->constructJson();
        }

        return $workFlow;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // hydration
    // -----------------------------------------------------------------------------------------------------------------

    public static function hydrateFromEntity(FormWorkflowEntity $entity): self
    {
        $steps = [];
        foreach ($entity->getSteps()->getValues() as $stepEntity) {
            $steps[] = FormWorkFlowStep::hydrateFromEntity($stepEntity);
        }

        return new self($entity->getId(), $entity->getStepsType(), $steps, $entity->getContentHash());
    }
}
