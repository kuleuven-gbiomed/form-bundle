<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

class FormNodeRelation implements \JsonSerializable
{
    // ---------------------------------------------------------------------
    // constructor
    // ---------------------------------------------------------------------

    public function __construct(
        protected readonly FormNode $sourceNode,
        protected readonly FormNode $targetNode,
        protected readonly FormNodeRelationType $relationType,
        protected readonly RelativePosition $relativePosition,
    ) {
    }

    // ---------------------------------------------------------------------
    // what to render as json
    // ---------------------------------------------------------------------

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->sourceNode = $this->sourceNode;
        $json->targetNode = $this->targetNode;
        $json->relation = $this->relationType->value;
        $json->relativePosition = $this->relativePosition->value;

        return $json;
    }

    // ---------------------------------------------------------------------
    // getters
    // ---------------------------------------------------------------------

    public function getSourceNode(): FormNode
    {
        return $this->sourceNode;
    }

    public function getTargetNode(): FormNode
    {
        return $this->targetNode;
    }

    public function getRelationType(): FormNodeRelationType
    {
        return $this->relationType;
    }

    public function getRelativePosition(): RelativePosition
    {
        return $this->relativePosition;
    }
}
