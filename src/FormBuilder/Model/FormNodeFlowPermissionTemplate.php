<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermissionTemplate as FormNodeFlowPermissionTemplateEntity;

class FormNodeFlowPermissionTemplate
{
    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        private readonly int $id,
        private readonly string $name,
        private readonly array $nodeFlowPermission,
        private readonly string $contentHash,
    ) {
    }

    // -----------------------------------------------------------------------------------------------------------------
    // hydration
    // -----------------------------------------------------------------------------------------------------------------

    public static function hydrateFromEntity(FormNodeFlowPermissionTemplateEntity $entity): self
    {
        return new self(
            $entity->getId(),
            $entity->getName(),
            $entity->getFlowPermission(),
            $entity->getContentHash()
        );
    }

    // -----------------------------------------------------------------------------------------------------------------
    // what to render as json
    // -----------------------------------------------------------------------------------------------------------------

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->id = $this->getId();
        $json->name = $this->getName();
        $json->contentHash = $this->getContentHash();

        return $json;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNodeFlowPermission(): array
    {
        return $this->nodeFlowPermission;
    }

    public function getContentHash(): string
    {
        return $this->contentHash;
    }
}
