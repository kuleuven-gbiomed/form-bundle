<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Model;

use KUL\FormBundle\FormBuilder\Entity\Form as FormEntity;

class Form implements \JsonSerializable
{
    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        private readonly int $id,
        private readonly string $uid,
        private readonly string $targetingType,
        private readonly int $currentVersionNumber,
        private readonly array $languages,
        private readonly array $rolesThatCouldParticipate,
        private readonly string $contentHash,
        private readonly bool $showPositionNumbers,
        private readonly bool $enableReadOnlyChoiceAnswerOptionsToggling,
    ) {
    }

    // -----------------------------------------------------------------------------------------------------------------
    // hydration
    // -----------------------------------------------------------------------------------------------------------------

    public static function hydrateFromEntity(FormEntity $entity): self
    {
        return new self(
            $entity->getId(),
            trim($entity->getUid()),
            trim($entity->getTargetingType()),
            $entity->getCurrentVersionNumber(),
            $entity->getLanguages(),
            $entity->getRolesThatCouldParticipate(),
            trim($entity->getContentHash()),
            $entity->isShowPositionNumbers(),
            $entity->isEnableReadOnlyChoiceAnswerOptionsToggling()
        );
    }

    // -----------------------------------------------------------------------------------------------------------------
    // what to render as json
    // -----------------------------------------------------------------------------------------------------------------

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->id = $this->id;
        $json->uid = $this->uid;
        $json->targetingType = $this->targetingType;
        $json->currentVersionNumber = $this->currentVersionNumber;
        $json->lanugages = $this->languages;
        $json->rolesThatCouldParticipate = $this->rolesThatCouldParticipate;
        $json->contentHash = $this->contentHash;
        $json->showPositionNumbers = $this->showPositionNumbers;
        $json->enableReadOnlyChoiceAnswerOptionsToggling = $this->enableReadOnlyChoiceAnswerOptionsToggling;

        return $json;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getId(): int
    {
        return $this->id;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getTargetingType(): string
    {
        return $this->targetingType;
    }

    public function getLanguages(): array
    {
        return $this->languages;
    }

    public function getCurrentVersionNumber(): int
    {
        return $this->currentVersionNumber;
    }

    public function getRolesThatCouldParticipate(): array
    {
        return $this->rolesThatCouldParticipate;
    }

    public function getContentHash(): string
    {
        return $this->contentHash;
    }

    public function isShowPositionNumbers(): bool
    {
        return $this->showPositionNumbers;
    }

    public function isEnableReadOnlyChoiceAnswerOptionsToggling(): bool
    {
        return $this->enableReadOnlyChoiceAnswerOptionsToggling;
    }
}
