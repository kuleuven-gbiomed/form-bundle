<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetFormList;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormInfo;
use KUL\FormBundle\FormBuilder\Entity\FormNode as FormNodeEntity;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;
use KUL\FormBundle\FormBuilder\Model\FormNode;
use KUL\FormBundle\FormBuilder\Model\FormNodeOption;
use KUL\FormBundle\FormBuilder\Queries\GetQuestionRadarChartParticipants\GetQuestionRadarChartParticipants;
use KUL\FormBundle\FormBuilder\Queries\GetTranslatedCommandHistory\GetTranslatedCommandHistoryQuery;
use KUL\FormBundle\FormBuilder\Repository\FormInfoRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository;
use KUL\FormBundle\FormBuilder\Utility\FormListFilter;

class GetFormListQuery
{
    private readonly FormRepository $formRepo;
    private readonly FormInfoRepository $formInfoRepo;
    private readonly FormWorkflowRepository $formWorkflowRepo;
    private readonly FormNodeRepository $formNodeRepo;
    private readonly FormNodeFlowPermissionRepository $formNodeFlowPermissionRepo;
    private readonly GetTranslatedCommandHistoryQuery $getTranslatedCommandHistoryQuery;
    private array $listLockedQuestions = [];

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formInfoRepo = $entityManager->getRepository(FormInfo::class);
        $this->formWorkflowRepo = $entityManager->getRepository(FormWorkflow::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNodeEntity::class);
        $this->formNodeFlowPermissionRepo = $entityManager->getRepository(FormNodeFlowPermission::class);

        $this->getTranslatedCommandHistoryQuery = new GetTranslatedCommandHistoryQuery($this->entityManager);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // get summary form list
    // -----------------------------------------------------------------------------------------------------------------

    public function summary(int $formId, ?FormListFilter $filter): \stdClass
    {
        $result = new \stdClass();

        // ------------------------------------------------------------------------------------------------ map the form
        $formEntity = $this->formRepo->getById($formId);
        $form = \KUL\FormBundle\FormBuilder\Model\Form::hydrateFromEntity($formEntity);
        $result->id = $form->getId();
        $result->uid = $form->getUid();
        $result->targetingType = $form->getTargetingType();
        $result->currentVersionNumber = $form->getCurrentVersionNumber();
        $result->languages = $form->getLanguages();
        $result->rolesThatCouldParticipate = $form->getRolesThatCouldParticipate();
        $result->contentHash = $form->getContentHash();
        $result->unPublishedItemCount = $this->getTranslatedCommandHistoryQuery->countHistoryItemsSinceLastPublishByForm($formEntity);
        $result->showPositionNumbers = $form->isShowPositionNumbers();
        $result->enableReadOnlyChoiceAnswerOptionsToggling = $form->isEnableReadOnlyChoiceAnswerOptionsToggling();

        // ------------------------------------------------------------------------------------------- map the form info
        $formInfoEntity = $this->formInfoRepo->getByForm($formEntity);
        $formInfo = \KUL\FormBundle\FormBuilder\Model\FormInfo::hydrateFromEntity($formInfoEntity);
        $result->info = $formInfo;

        // --------------------------------------------------------------------------------------- map the form workflow
        $formWorkFlowEntity = $this->formWorkflowRepo->getByForm($formEntity);
        $formWorkFlow = \KUL\FormBundle\FormBuilder\Model\FormWorkflow::hydrateFromEntity($formWorkFlowEntity);
        $result->workFlow = $formWorkFlow;

        // --------------------------------------------------------------------------  map the basic form list structure
        $formRootEntity = $this->formNodeRepo->getRootNodeForForm($formEntity);
        $this->listLockedQuestions = self::getLockedQuestionIds($formRootEntity);
        $result->rootNode = $this->renderItemEntity($formRootEntity, $filter);

        return $result;
    }

    private function renderItemEntity(FormNodeEntity $formNodeEntity, ?FormListFilter $filter): FormNode
    {
        $formNode = FormNode::hydrateFromEntity($formNodeEntity);

        if ($formNode->isCategory()) {
            $childEntities = $formNodeEntity->getChildren();
            /** @var FormNodeEntity $childEntity */
            foreach ($childEntities as $childEntity) {
                $formNodeChild = $this->renderItemEntity($childEntity, $filter);
                $formNode->addChild($formNodeChild);
            }
        } else {
            // -- for number questions look for calculation participation in the database
            if (FormNode::SUB_TYPE_NUMBER === $formNode->getSubType()) {
                $calculationQuestions = $this->formNodeRepo->getNodesByCalculationParticipation($formNodeEntity->getForm(), $formNodeEntity->getUid());
                if (0 !== count($calculationQuestions)) {
                    $formNode->setIsScoreQuestionForCalculation(true);
                }

                // -- set the isChartParticipation flag with a database search on chart nodes...
                $chartQuestions = $this->formNodeRepo->getNodesByChartParticipation($formNodeEntity->getForm(), $formNodeEntity->getUid());
                if (0 !== count($chartQuestions)) {
                    $formNode->setIsChartParticipantQuestion(true);
                }
            }

            // ---------------------------------------------------------------- choice type question -> add some options
            if (FormNode::SUB_TYPE_CHOICE === $formNode->getSubType()) {
                $optionEntities = $formNodeEntity->getOptions();
                foreach ($optionEntities as $optionEntity) {
                    $formNodeOption = FormNodeOption::hydrateFromEntity($optionEntity);
                    if (count($formNodeOption->getUnlocksQuestionIds()) > 0) {
                        $formNode->setUnlockerQuestion(true);
                    }
                    $formNode->addOption($formNodeOption);
                }

                // for choice questions fix the false positive flag isScoringQuestion to query on the database
                if ($formNode->isScoreQuestion()) {
                    $calculationQuestions = $this->formNodeRepo->getNodesByCalculationParticipation($formNodeEntity->getForm(), $formNodeEntity->getUid());
                    $formNode->setIsScoreQuestionForCalculation(0 < count($calculationQuestions));
                }

                // -- set the isChartParticipation flag with a database search on chart nodes...
                $chartQuestions = $this->formNodeRepo->getNodesByChartParticipation($formNodeEntity->getForm(), $formNodeEntity->getUid());
                if (0 !== count($chartQuestions)) {
                    $formNode->setIsChartParticipantQuestion(true);
                }

                $prefillingQuestions = $this->formNodeRepo->getNodesByPrefilParticipation($formNodeEntity->getForm(), $formNodeEntity->getUid());
                if (0 !== count($prefillingQuestions)) {
                    $formNode->setIsPrefillingQuestion(true);
                }
            }

            // for chart questions get the relationships
            if (FormNode::SUB_TYPE_RADAR_CHART === $formNode->getSubType()) {
                $query = new GetQuestionRadarChartParticipants($this->entityManager);
                $formNode->setChartParticipantQuestions($query->participants($formNodeEntity->getForm()->getId(), $formNodeEntity->getId()));
            }

            // ----------------------------------------------------- look for the form list uuid in the locked questions
            foreach ($this->listLockedQuestions as $lockedQuestionId) {
                if ($formNode->getUid() === $lockedQuestionId) {
                    $formNode->setLockedQuestion(true);
                }
            }

            // ------------------------------------------------------------------------ set permission if filter applied
            if (isset($filter)) {
                $formNode->setPermission($this->getPermissionForFormNodeEntityAndFilter($formNodeEntity, $filter));
            }
        }

        return $formNode;
    }

    private static function getLockedQuestionIds(FormNodeEntity $formNode): array
    {
        $ids = [];
        if (FormNode::SUB_TYPE_CHOICE === $formNode->getSubType()) {
            foreach ($formNode->getOptions() as $optionEntity) {
                $formNodeOption = FormNodeOption::hydrateFromEntity($optionEntity);
                foreach ($formNodeOption->getUnlocksQuestionIds() as $lockQuestionId) {
                    $ids[] = $lockQuestionId;
                }
            }
        }
        foreach ($formNode->getChildren() as $childNode) {
            $ids = array_merge($ids, self::getLockedQuestionIds($childNode));
        }

        return $ids;
    }

    /** @return FormNode::PERMISSION_WRITABLE|FormNode::PERMISSION_READ_ONLY|null */
    private function getPermissionForFormNodeEntityAndFilter(FormNodeEntity $entity, ?FormListFilter $filter = null): ?string
    {
        if (isset($filter)) {
            $formPermissionEntity = $this->formNodeFlowPermissionRepo->getByFormNode($entity);
            $formPermission = \KUL\FormBundle\FormBuilder\Model\FormNodeFlowPermission::hydrateFromEntity($formPermissionEntity);

            return $formPermission->getPermissionForRoleAndStepUid($filter->getRole(), $filter->getStepUid());
        }

        return FormNode::PERMISSION_WRITABLE;
    }
}
