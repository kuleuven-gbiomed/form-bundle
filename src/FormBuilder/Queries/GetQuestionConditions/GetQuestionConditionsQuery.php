<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetQuestionConditions;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Queries\Common\InfoUtil;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class GetQuestionConditionsQuery
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function conditions(int $formId, int $itemId, string $language): array
    {
        $result = [];

        // ---------------------------------------------------------------- get the source information from the database
        $formEntity = $this->formRepo->getById($formId);
        $formNodeEntity = $this->formNodeRepo->getById($itemId);
        if (!($formEntity->getId() === $formNodeEntity->getForm()->getId())) {
            throw new \Exception('Form Id en FormNodeId are not of the same form!');
        }

        // -------------------------------------------------------------------------- find uid in options unlocking id's
        $conditions = $this->formNodeRepo->getNodesByOptionUnlockIds($formEntity, $formNodeEntity->getUid());
        foreach ($conditions as $condition) {
            $conditionResult = new \stdClass();
            $conditionResult->id = $condition->getId();
            $conditionResult->uid = $condition->getUid();
            $conditionResult->label = InfoUtil::extractLabel($condition->getInfo(), $language);
            $conditionResult->position = $condition->getPosition();
            $conditionResult->options = [];

            // ------------------------------------------------------------------- find the related options and add them
            foreach ($condition->getOptions() as $option) {
                $unlocksQuestionIds = $option->getUnlocksQuestionIds();
                if ([] !== $unlocksQuestionIds) {
                    if (in_array($formNodeEntity->getUid(), $unlocksQuestionIds, true)) {
                        $optionResult = new \stdClass();
                        $optionResult->id = $option->getId();
                        $optionResult->uid = $option->getUid();
                        $optionResult->label = InfoUtil::extractLabel($option->getInfo(), $language);
                        $optionResult->unlocksQuestionIds = $unlocksQuestionIds;
                        $conditionResult->options[] = $optionResult;
                    }
                }
            }
            $result[] = $conditionResult;
        }

        return $result;
    }
}
