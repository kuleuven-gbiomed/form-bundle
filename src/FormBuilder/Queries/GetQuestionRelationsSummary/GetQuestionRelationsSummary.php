<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetQuestionRelationsSummary;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Queries\GetNodeRelations\GetNodeRelations;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class GetQuestionRelationsSummary
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;
    private readonly GetNodeRelations $getQuestionRelations;

    // -------------------------------------------------------------------------------
    // constructor
    // -------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);

        $this->getQuestionRelations = new GetNodeRelations($entityManager);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function relations(int $formId, int $nodeId, string $language): array
    {
        $result = [];

        // -- get the source information from the database
        $formEntity = $this->formRepo->getById($formId);
        $formNodeEntity = $this->formNodeRepo->getById($nodeId);
        if (!($formEntity->getId() === $formNodeEntity->getForm()->getId())) {
            throw new \Exception('Form Id en FormNodeId are not of the same form!');
        }

        // -- make a summary version of the relations
        $relations = $this->getQuestionRelations->relations($nodeId);
        foreach ($relations as $relation) {
            $relationDto = new \stdClass();
            $relationDto->sourceNode = $this->makeQuestionSummary($relation->getSourceNode(), $language);
            $relationDto->relationType = $relation->getRelationType();
            $relationDto->targetNode = $this->makeQuestionSummary($relation->getTargetNode(), $language);
            $relationDto->relativePosition = $relation->getRelativePosition();
            $result[] = $relationDto;
        }

        return $result;
    }

    private function makeQuestionSummary(\KUL\FormBundle\FormBuilder\Model\FormNode $formNode, string $language): \stdClass
    {
        $node = new \stdClass();
        $node->id = $formNode->getId();
        $node->uid = $formNode->getUid();
        $node->type = $formNode->getType();
        $node->subType = $formNode->getSubType();
        $node->position = $formNode->getPosition();
        if (isset($formNode->getInfo()['label'][strtolower($language)])) {
            $node->label = $formNode->getInfo()['label'][strtolower($language)];
        } else {
            $node->label = '[label]';
        }
        $node->position = $formNode->getPosition();

        return $node;
    }
}
