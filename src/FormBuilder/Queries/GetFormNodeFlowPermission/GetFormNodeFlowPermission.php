<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetFormNodeFlowPermission;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;
use KUL\FormBundle\FormBuilder\Queries\Common\FlowPermissionUtil;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository;

class GetFormNodeFlowPermission
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;
    private readonly FormWorkflowRepository $formWorkflowRepo;
    private readonly FormNodeFlowPermissionRepository $formNodeFlowPermissionRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
        $this->formWorkflowRepo = $entityManager->getRepository(FormWorkflow::class);
        $this->formNodeFlowPermissionRepo = $entityManager->getRepository(FormNodeFlowPermission::class);
    }

    public function nodeFlowPermission(int $formId, int $itemId): \stdClass
    {
        // ---------------------------------------------------------------- get the source information from the database
        $formEntity = $this->formRepo->getById($formId);
        $formNodeEntity = $this->formNodeRepo->getById($itemId);
        if (!($formEntity->getId() === $formNodeEntity->getForm()->getId())) {
            throw new \Exception('Form Id en FormNodeId are not of the same form!');
        }
        // get the available roles
        $participatingRoles = $formEntity->getRolesThatCouldParticipate();
        // get the available steps
        $formWorklowEntity = $this->formWorkflowRepo->getByForm($formEntity);
        $workflowSteps = $formWorklowEntity->getSteps()->getValues();
        // get the flow permission entity
        $flowPermissionEntity = $this->formNodeFlowPermissionRepo->getByFormNode($formNodeEntity);

        $result = new \stdClass();

        // construct the permissions
        $result->flowPermission = [];
        foreach ($workflowSteps as $step) {
            $result->flowPermission[$step->getUid()] = [];
            foreach ($participatingRoles as $role) {
                $result->flowPermission[$step->getUid()][$role] = FlowPermissionUtil::findForStepAndRole(
                    $step->getUid(),
                    $role,
                    $flowPermissionEntity->getFlowPermission()
                );
            }
        }

        $template = $flowPermissionEntity->getTemplate();
        // link to template ID (if there is a connection)
        if (null === $template) {
            $result->templateId = 0;
        } else {
            $result->templateId = $template->getId();
        }

        return $result;
    }
}
