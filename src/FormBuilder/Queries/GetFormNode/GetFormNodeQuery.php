<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetFormNode;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Model\FormNode as FormNodeModel;
use KUL\FormBundle\FormBuilder\Model\FormNodeOption as FormNodeOptionModel;
use KUL\FormBundle\FormBuilder\Queries\GetQuestionRadarChartParticipants\GetQuestionRadarChartParticipants;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class GetFormNodeQuery
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // get details about this item
    // -----------------------------------------------------------------------------------------------------------------

    public function item(int $formId, int $itemId): FormNodeModel
    {
        // ---------------------------------------------------------------- get the source information from the database
        $formEntity = $this->formRepo->getById($formId);
        $formNodeEntity = $this->formNodeRepo->getById($itemId);
        if (!($formEntity->getId() === $formNodeEntity->getForm()->getId())) {
            throw new \Exception('Form Id en FormNodeId are not of the same form!');
        }

        // ---------------------------------------------------------- hydrate to the (read) model for json serialisation
        $result = FormNodeModel::hydrateFromEntity($formNodeEntity);

        // -- overrule the isScoring property with a database calculation participation.
        $calculationQuestions = $this->formNodeRepo->getNodesByCalculationParticipation($formEntity, $formNodeEntity->getUid());
        if (0 === count($calculationQuestions)) {
            $result->setIsScoreQuestionForCalculation(false);
        } else {
            $result->setIsScoreQuestionForCalculation(true);
        }

        // -- set the isChartParticipation flag with a database search on chart nodes...
        $chartQuestions = $this->formNodeRepo->getNodesByChartParticipation($formEntity, $formNodeEntity->getUid());
        if (0 === count($chartQuestions)) {
            $result->setIsChartParticipantQuestion(false);
        } else {
            $result->setIsChartParticipantQuestion(true);
        }

        // -- set the radar chart participants
        if ($result->getSubType() === $result::SUB_TYPE_RADAR_CHART) {
            $query = new GetQuestionRadarChartParticipants($this->entityManager);
            $result->setChartParticipantQuestions($query->participants($formId, $itemId));
        }

        // -- convert the options entities to read models
        $optionEntities = $formNodeEntity->getOptions();
        foreach ($optionEntities as $optionEntity) {
            $formNodeOption = FormNodeOptionModel::hydrateFromEntity($optionEntity);
            $result->addOption($formNodeOption);
        }

        return $result;
    }
}
