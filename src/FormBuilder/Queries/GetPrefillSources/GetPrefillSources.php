<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetPrefillSources;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Queries\Common\InfoUtil;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class GetPrefillSources
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;

    // ------------------------------------------------------------------------
    // constructor
    // ------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function sources(int $formId, int $itemId, string $language): array
    {
        $result = [];

        // ---------------------------- the source information from the database
        $formEntity = $this->formRepo->getById($formId);
        $formNodeEntity = $this->formNodeRepo->getById($itemId);
        if (!($formEntity->getId() === $formNodeEntity->getForm()->getId())) {
            throw new \Exception('Form Id en FormNodeId are not of the same form!');
        }

        $sourceQuestions = $formNodeEntity->getPrefillConfig()['prefillingQuestionUids'];
        foreach ($sourceQuestions as $sourceQuestion) {
            $sourceNodeEntity = $this->formNodeRepo->getByUidAndFormId($sourceQuestion, $formId);
            $source = new \stdClass();
            $source->id = $sourceNodeEntity->getId();
            $source->question = InfoUtil::extractLabel($sourceNodeEntity->getInfo(), $language);
            $source->uid = $sourceQuestion;
            $source->contentHash = $sourceNodeEntity->getContentHash();
            $source->position = $sourceNodeEntity->getPosition();
            $result[] = $source;
        }

        return $result;
    }
}
