<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetQuestionsByParent;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Queries\Common\InfoUtil;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class GetQuestionsByParentQuery
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function questions(int $formId, string $language, int $parentId): array
    {
        $result = [];
        $formEntity = $this->formRepo->getById($formId);

        $parentItem = $this->formNodeRepo->getById($parentId);
        $allChildren = $parentItem->getAllChildrenIds();
        $questions = $this->formNodeRepo->getQuestionsByFormAndListId($formEntity, $allChildren);
        foreach ($questions as $question) {
            $questionResult = new \stdClass();
            $questionResult->id = $question->getId();
            $questionResult->uid = $question->getUid();
            $questionResult->label = InfoUtil::extractLabel($question->getInfo(), $language);
            $questionResult->type = $question->getType();
            $questionResult->subtype = $question->getSubType();
            $questionResult->position = $question->getPosition();
            $result[] = $questionResult;
        }

        return $result;
    }
}
