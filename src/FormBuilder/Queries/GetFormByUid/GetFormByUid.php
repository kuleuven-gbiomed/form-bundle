<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetFormByUid;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Model\Form as FormModel;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class GetFormByUid
{
    private readonly FormRepository $formRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // get form by uid
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @throws EntityNotFoundException
     */
    public function get(string $uid): FormModel
    {
        $formEntity = $this->formRepo->getByUid($uid);

        return FormModel::hydrateFromEntity($formEntity);
    }
}
