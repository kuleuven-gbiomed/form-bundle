<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetQuestionRadarChartParticipants;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class GetQuestionRadarChartParticipants
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
    }

    /**
     * @return list<RadarChartParticipant>
     */
    public function participants(int $formId, int $itemId): array
    {
        $result = [];

        // ---------------------------------------------------------------- get the source information from the database
        $formEntity = $this->formRepo->getById($formId);
        $formNodeEntity = $this->formNodeRepo->getById($itemId);
        if (!($formEntity->getId() === $formNodeEntity->getForm()->getId())) {
            throw new \Exception('Form Id en FormNodeId are not of the same form!');
        }

        // --------------------------------------------------------- make a list of participants with their custom label
        $sourceQuestions = $formNodeEntity->getPropsConfig()['question_references'];
        foreach ($sourceQuestions as $sourceQuestion) {
            $sourceNodeEntity = $this->formNodeRepo->getByUidAndFormId($sourceQuestion['question_uid'], $formId);
            $result[] = new RadarChartParticipant(
                $sourceNodeEntity->getId(),
                $sourceQuestion['custom_labels'],
                $sourceNodeEntity->getInfo()['label'],
                $sourceQuestion['question_uid'],
                $sourceNodeEntity->getContentHash(),
                $sourceNodeEntity->getPosition(),
            );
        }

        return $result;
    }
}
