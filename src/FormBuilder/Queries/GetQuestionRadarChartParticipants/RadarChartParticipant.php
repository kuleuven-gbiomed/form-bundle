<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetQuestionRadarChartParticipants;

class RadarChartParticipant
{
    /**
     * @param array<string, string> $customLabel
     * @param array<string, string> $questionLabel
     * @param list<int>             $position
     */
    public function __construct(
        public readonly int $id,
        public readonly array $customLabel,
        public readonly array $questionLabel,
        public readonly string $uid,
        public readonly string $contentHash,
        public readonly array $position,
    ) {
    }
}
