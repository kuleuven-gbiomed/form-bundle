<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetCommandHistory;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\CommandHistoryEntry;
use KUL\FormBundle\FormBuilder\Repository\CommandHistoryEntryRepository;

class GetCommandHistoryQuery
{
    private readonly CommandHistoryEntryRepository $historyRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->historyRepo = $entityManager->getRepository(CommandHistoryEntry::class);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // get history
    // -----------------------------------------------------------------------------------------------------------------

    public function history(string $commandType, int $subjectId): array
    {
        return $this->historyRepo->getByCommandAndSubjectId($commandType, $subjectId);
    }
}
