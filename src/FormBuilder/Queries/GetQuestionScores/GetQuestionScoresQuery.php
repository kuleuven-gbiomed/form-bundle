<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetQuestionScores;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Queries\Common\InfoUtil;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class GetQuestionScoresQuery
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;

    // -----------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------

    public function __construct(protected EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function scores(int $formId, int $itemId, string $language): array
    {
        $result = [];

        // -------------------------- get the source information from the database
        $formEntity = $this->formRepo->getById($formId);
        $formNodeEntity = $this->formNodeRepo->getById($itemId);
        if (!($formEntity->getId() === $formNodeEntity->getForm()->getId())) {
            throw new \Exception('Form Id en FormNodeId are not of the same form!');
        }

        // ------------- make a list of score questions and their label and weight

        $sourceScores = $formNodeEntity->getScoringConfig()['globalScoreCalculationDependencyConfigs'];
        foreach ($sourceScores as $sourceScore) {
            $sourceNodeEntity = $this->formNodeRepo->getByUidAndFormId($sourceScore['scoreNodeUid'], $formId);
            $score = new \stdClass();
            $score->id = $sourceNodeEntity->getId();
            $score->weight = $sourceScore['weighing'];
            $score->question = InfoUtil::extractLabel($sourceNodeEntity->getInfo(), $language);
            $score->uid = $sourceScore['scoreNodeUid'];
            $score->contentHash = $sourceNodeEntity->getContentHash();
            $score->position = $sourceNodeEntity->getPosition();
            $result[] = $score;
        }

        return $result;
    }
}
