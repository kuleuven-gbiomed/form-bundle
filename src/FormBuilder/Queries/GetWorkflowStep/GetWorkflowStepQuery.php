<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetWorkflowStep;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflowStep;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowStepRepository;

class GetWorkflowStepQuery
{
    private readonly FormRepository $formRepo;
    private readonly FormWorkflowRepository $formWorkflowRepo;
    private readonly FormWorkflowStepRepository $formWorkflowStepRepo;

    // ------------------------------------------------------------------------------
    // constructor
    // ------------------------------------------------------------------------------
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formWorkflowRepo = $entityManager->getRepository(FormWorkflow::class);
        $this->formWorkflowStepRepo = $entityManager->getRepository(FormWorkflowStep::class);
    }

    // ------------------------------------------------------------------------------
    // query
    // ------------------------------------------------------------------------------

    public function get(int $formId, int $stepNumber): \stdClass|false
    {
        $formEntity = $this->formRepo->getById($formId);

        try {
            // $workflowStepEntity = $this->formWorkflowStepRepo->getById($stepId);
            $workflowStepEntity = $this->formWorkflowStepRepo->getBySequenceAndWorkflowId($stepNumber - 1, $this->formWorkflowRepo->getByForm($formEntity)->getId());
        } catch (EntityNotFoundException) {
            return false;
        }

        $workflowEntity = $workflowStepEntity->getFormWorkflow();

        if (!($formEntity->getId() === $workflowEntity->getForm()->getId())) {
            throw new \Exception('Form Id en Step Id are not of the same form!');
        }

        $workflowStep = \KUL\FormBundle\FormBuilder\Model\FormWorkFlowStep::hydrateFromEntity($workflowStepEntity);

        return $workflowStep->formatDetail();
    }
}
