<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetWorkflowSchema;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;
use KUL\FormBundle\FormBuilder\Queries\Common\InfoUtil;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository;

class GetWorkflowSchemaQuery
{
    private readonly FormRepository $formRepo;
    private readonly FormWorkflowRepository $formWorkflowRepo;

    // ------------------------------------------------------------------------------
    // constructor
    // ------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formWorkflowRepo = $entityManager->getRepository(FormWorkflow::class);
    }

    // ------------------------------------------------------------------------------
    // query
    // ------------------------------------------------------------------------------

    public function get(int $formId, string $language): array
    {
        $form = $this->formRepo->getById($formId);
        $workflow = $this->formWorkflowRepo->getByForm($form);
        $result = [];
        $steps = $workflow->getSteps()->getValues();
        $stepLabel = 'Stap';
        if ('en' === $language) {
            $stepLabel = 'Step';
        } elseif ('fr' === $language) {
            $stepLabel = 'Etape';
        }

        // -- list the nodes ------------------------------------------------------
        $counter = 0;
        foreach ($steps as $step) {
            ++$counter;
            $stepNode = new \stdClass();
            $stepNode->id = (string) $counter;
            $counterString = (string) $counter;
            $stepNode->label = $stepLabel.' '.$counterString;
            $stepNode->type = 'n-a';
            if (StepWithFixedDateStart::TYPE === $step->getType()) {
                $stepNode->type = 'fixed';
            } elseif (StepWithCalculatedDateStart::TYPE === $step->getType()) {
                $stepNode->type = 'relative';
            }
            // -- compile some data
            $stepNode->data = new \stdClass();
            $stepNode->data->id = $step->getId();
            $stepNode->data->sequence = $counter - 1;
            $stepNode->data->uuid = $step->getUid();
            if (1 === $counter) {
                $stepNode->data->type = 'output';
            } else {
                $stepNode->data->type = 'default';
            }
            $stepNode->data->description = InfoUtil::extractLabel($step->getInfo(), $language);
            if (StepWithFixedDateStart::TYPE === $step->getType()) {
                $stepNode->data->from = $step->getPeriod()['fixedStartDate'];
                $stepNode->data->to = $step->getPeriod()['fixedEndDate'];
            } elseif (StepWithCalculatedDateStart::TYPE === $step->getType()) {
                $stepNode->data->from = $step->getPeriod()['amountOfDaysToCalculateStartDate'];
                $stepNode->data->to = $step->getPeriod()['amountOfDaysToCalculateEndDate'];
            }
            // -- compile position
            $stepNode->position = new \stdClass();
            if (0 === $step->getX()) {
                $stepNode->position->x = 150;
            } else {
                $stepNode->position->x = $step->getX();
            }
            if (0 === $step->getY()) {
                $stepNode->position->y = ($counter - 1) * 250 + 50;
            } else {
                $stepNode->position->y = $step->getY();
            }
            $result[] = $stepNode;
        }

        // add the add button -----------------------------------------------------
        $stepAddNode = new \stdClass();
        $stepAddNode->id = (string) ($counter + 1);
        $stepAddNode->type = 'add';
        $stepAddNode->data = new \stdClass();
        $stepAddNode->data->type = 'input';
        $stepAddNode->data->step = $counter + 1;
        $stepAddNode->position = new \stdClass();
        if (0 === $workflow->getStartX()) {
            $stepAddNode->position->x = 350;
        } else {
            $stepAddNode->position->x = $workflow->getStartX();
        }
        if (0 === $workflow->getStartY()) {
            $stepAddNode->position->y = $counter * 250 + 50;
        } else {
            $stepAddNode->position->y = $workflow->getStartY();
        }
        $result[] = $stepAddNode;

        // list the relations // edges ---------------------------------------------
        $counter = 0;
        foreach ($steps as $step) {
            ++$counter;
            $defaultStrokeWidth = 4;
            $nextCounter = $counter + 1;
            $nextCounterString = (string) $nextCounter;
            $counterString = (string) $counter;
            $stepEdge = new \stdClass();
            $stepEdge->id = 'e'.$counterString.'-'.$nextCounterString;
            $stepEdge->source = (string) $counter;
            $stepEdge->target = (string) $nextCounter;
            $stepEdge->style = new \stdClass();
            $stepEdge->style->strokeWidth = $defaultStrokeWidth;
            $stepEdge->type = 'smoothstep';
            $stepEdge->markerEnd = 'arrow';
            if ($counter < count($steps)) {
                $nextStep = $steps[$counter];
                if ($nextStep->getAllowsStartIfPreviousStepIsFulfilled()) {
                    $stepEdge->animated = true;
                    $stepEdge->label = 'start na versturen van de vorige stap';
                } else {
                    $stepEdge->animated = false;
                }
            } else {
                $stepEdge->style->strokeWidth = 1;
                $stepEdge->animated = false;
            }
            $result[] = $stepEdge;
        }

        return $result;
    }
}
