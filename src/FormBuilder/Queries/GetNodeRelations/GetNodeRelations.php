<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetNodeRelations;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode as FormNodeEntity;
use KUL\FormBundle\FormBuilder\Model\FormNode;
use KUL\FormBundle\FormBuilder\Model\FormNodeOption;
use KUL\FormBundle\FormBuilder\Model\FormNodeRelation;
use KUL\FormBundle\FormBuilder\Model\FormNodeRelationType;
use KUL\FormBundle\FormBuilder\Model\RelativePosition;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;

class GetNodeRelations
{
    private readonly FormNodeRepository $formNodeRepo;

    // -------------------------------------------------------------------------------
    // constructor
    // -------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formNodeRepo = $entityManager->getRepository(FormNodeEntity::class);
    }

    /**
     * @return list<FormNodeRelation>
     */
    public function relations(int $nodeId): array
    {
        $formNode = $this->formNodeRepo->getById($nodeId);

        return $this->getRelationsRecursively($formNode);
    }

    /**
     * @return list<FormNodeRelation>
     *
     * @psalm-return list<FormNodeRelation>
     */
    public function getRelationsRecursively(FormNodeEntity $node): array
    {
        if ('question' === $node->getType()) {
            return $this->getQuestionRelations($node);
        }

        $result = [];
        foreach ($node->getChildren() as $childNode) {
            $childResult = $this->getRelationsRecursively($childNode);
            $result = [...$result, ...$childResult];
        }

        return $result;
    }

    /**
     * @return list<FormNodeRelation>
     */
    public function getQuestionRelations(FormNodeEntity $formNode): array
    {
        if ('question' !== $formNode->getType()) {
            throw new \BadMethodCallException('Can only be called if the $formNode has type "question"');
        }

        $formEntity = $formNode->getForm();

        return array_merge(
            // LOCKING & UN_LOCKING
            $this->getNodesUnlockedByNode($formEntity, $formNode),
            $this->getNodesUnlockingNode($formEntity, $formNode),

            // CALCULATION & SCORE
            isset($formNode->getScoringConfig()['globalScoreCalculationDependencyConfigs']) // calculation question?
                ? $this->getScoreNodesForNode($formEntity, $formNode)
                : [],
            $this->getCalculationNodesForNode($formEntity, $formNode),

            // PREFILL & PREFILL SOURCE
            isset($formNode->getPrefillConfig()['prefillingQuestionUids']) // prefilled question?
                ? $this->getNodesPrefillingNode($formEntity, $formNode)
                : [],
            $this->getNodesPrefilledByNode($formEntity, $formNode),

            // RADAR CHART & CHART PARTICIPANT
            isset($formNode->getPropsConfig()['question_references']) // radar chart node?
                ? $this->getSourceNodesForChartNode($formEntity, $formNode)
                : [],
            $this->getChartNodesUsingNode($formEntity, $formNode),
        );
    }

    /**
     * @return list<FormNodeRelation>
     *
     * @throws \Exception
     */
    public function getNodesUnlockedByNode(Form $form, FormNodeEntity $node): array
    {
        self::throwExceptionIfFormNodeIsNotOfForm($form, $node);

        $result = [];

        // -- convert the options entities to read models and find possible unlocks questions
        $lockedIds = [];
        $optionEntities = $node->getOptions();
        $sourceNode = FormNode::hydrateFromEntity($node);
        foreach ($optionEntities as $optionEntity) {
            $lockedIds = array_merge($lockedIds, $optionEntity->getUnlocksQuestionIds());
            $formNodeOption = FormNodeOption::hydrateFromEntity($optionEntity);
            $sourceNode->addOption($formNodeOption);
        }
        $lockedIds = array_unique($lockedIds);
        $lockedQuestions = $this->formNodeRepo->getQuestionsByFormAndListUid($form, $lockedIds);
        foreach ($lockedQuestions as $lockedQuestion) {
            $relation = new FormNodeRelation(
                $sourceNode,
                FormNode::hydrateFromEntity($lockedQuestion),
                FormNodeRelationType::UNLOCKS,
                $node->isBelowNode($lockedQuestion)
                    ? RelativePosition::Above
                    : RelativePosition::Below
            );
            $result[] = $relation;
        }

        return $result;
    }

    /**
     * @return list<FormNodeRelation>
     *
     * @throws \Exception
     */
    public function getNodesUnlockingNode(Form $form, FormNodeEntity $node): array
    {
        self::throwExceptionIfFormNodeIsNotOfForm($form, $node);

        $result = [];

        $lockingQuestions = $this->formNodeRepo->getNodesByOptionUnlockIds($form, $node->getUid());
        foreach ($lockingQuestions as $lockingQuestion) {
            $relation = new FormNodeRelation(
                FormNode::hydrateFromEntity($node),
                FormNode::hydrateFromEntity($lockingQuestion),
                FormNodeRelationType::UNLOCKABLE_BY,
                $node->isBelowNode($lockingQuestion)
                    ? RelativePosition::Above
                    : RelativePosition::Below
            );
            $result[] = $relation;
        }

        return $result;
    }

    /**
     * @return list<FormNodeRelation>
     *
     * @throws \Exception
     */
    public function getCalculationNodesForNode(Form $form, FormNodeEntity $scoreNode): array
    {
        self::throwExceptionIfFormNodeIsNotOfForm($form, $scoreNode);

        $result = [];

        $calculationParents = $this->formNodeRepo->getNodesByCalculationParticipation($form, $scoreNode->getUid());
        foreach ($calculationParents as $calculationParent) {
            $calculationNode = FormNode::hydrateFromEntity($calculationParent);
            $relation = new FormNodeRelation(
                FormNode::hydrateFromEntity($scoreNode),
                FormNode::hydrateFromEntity($calculationParent),
                FormNodeRelationType::SCORE_FOR,
                $scoreNode->isBelowNode($calculationParent)
                    ? RelativePosition::Above
                    : RelativePosition::Below
            );
            $result[] = $relation;
        }

        return $result;
    }

    /**
     * @return list<FormNodeRelation>
     *
     * @throws \Exception
     */
    public function getScoreNodesForNode(Form $form, FormNodeEntity $calculationNode): array
    {
        self::throwExceptionIfFormNodeIsNotOfForm($form, $calculationNode);

        $result = [];

        $scoreIds = [];
        $sourceNode = FormNode::hydrateFromEntity($calculationNode);
        foreach ($sourceNode->getScoringConfig()['globalScoreCalculationDependencyConfigs'] as $scoreQuestion) {
            $scoreIds[] = $scoreQuestion['scoreNodeUid'];
        }
        $scoreIds = array_unique($scoreIds);
        $scoreQuestions = $this->formNodeRepo->getQuestionsByFormAndListUid($form, $scoreIds);
        foreach ($scoreQuestions as $scoreQuestion) {
            $relation = new FormNodeRelation(
                $sourceNode,
                FormNode::hydrateFromEntity($scoreQuestion),
                FormNodeRelationType::CALCULATION_OF,
                $calculationNode->isBelowNode($scoreQuestion)
                    ? RelativePosition::Above
                    : RelativePosition::Below
            );
            $result[] = $relation;
        }

        return $result;
    }

    /**
     * @return list<FormNodeRelation>
     *
     * @throws \Exception
     */
    public function getNodesPrefillingNode(Form $form, FormNodeEntity $node): array
    {
        self::throwExceptionIfFormNodeIsNotOfForm($form, $node);

        $result = [];

        $sourceNode = FormNode::hydrateFromEntity($node);
        $prefillIds = $sourceNode->getPrefillConfig()['prefillingQuestionUids'];
        $prefillIds = array_unique($prefillIds);
        $prefillQuestions = $this->formNodeRepo->getQuestionsByFormAndListUid($form, $prefillIds);

        foreach ($prefillQuestions as $prefillQuestion) {
            $relation = new FormNodeRelation(
                $sourceNode,
                FormNode::hydrateFromEntity($prefillQuestion),
                FormNodeRelationType::PREFILLED_BY,
                $node->isBelowNode($prefillQuestion)
                    ? RelativePosition::Above
                    : RelativePosition::Below
            );
            $result[] = $relation;
        }

        return $result;
    }

    /**
     * @return list<FormNodeRelation>
     *
     * @throws \Exception
     */
    public function getNodesPrefilledByNode(Form $form, FormNodeEntity $node): array
    {
        self::throwExceptionIfFormNodeIsNotOfForm($form, $node);

        $result = [];

        $prefillParents = $this->formNodeRepo->getNodesByPrefilParticipation($form, $node->getUid());
        foreach ($prefillParents as $prefillParent) {
            $relation = new FormNodeRelation(
                FormNode::hydrateFromEntity($node),
                FormNode::hydrateFromEntity($prefillParent),
                FormNodeRelationType::PREFILLS,
                $node->isBelowNode($prefillParent)
                    ? RelativePosition::Above
                    : RelativePosition::Below
            );
            $result[] = $relation;
        }

        return $result;
    }

    /**
     * @return list<FormNodeRelation>
     *
     * @throws \Exception
     */
    public function getChartNodesUsingNode(Form $form, FormNodeEntity $chartSourceNode): array
    {
        self::throwExceptionIfFormNodeIsNotOfForm($form, $chartSourceNode);
        $result = [];

        $chartParents = $this->formNodeRepo->getNodesByChartParticipation($form, $chartSourceNode->getUid());
        foreach ($chartParents as $chartParent) {
            $relation = new FormNodeRelation(
                FormNode::hydrateFromEntity($chartSourceNode),
                FormNode::hydrateFromEntity($chartParent),
                FormNodeRelationType::RADAR_CHART_SOURCE,
                $chartSourceNode->isBelowNode($chartParent)
                    ? RelativePosition::Above
                    : RelativePosition::Below
            );
            $result[] = $relation;
        }

        return $result;
    }

    /**
     * @return list<FormNodeRelation>
     *
     * @throws \Exception
     */
    public function getSourceNodesForChartNode(Form $form, FormNodeEntity $chartNode): array
    {
        self::throwExceptionIfFormNodeIsNotOfForm($form, $chartNode);
        $result = [];

        $sourceNode = FormNode::hydrateFromEntity($chartNode);
        $sourceIds = [];
        foreach ($sourceNode->getPropsConfig()['question_references'] as $sourceQuestion) {
            $sourceIds[] = $sourceQuestion['question_uid'];
        }
        $sourceIds = array_unique($sourceIds);

        $sourceQuestions = $this->formNodeRepo->getQuestionsByFormAndListUid($form, $sourceIds);
        foreach ($sourceQuestions as $sourceQuestion) {
            $relation = new FormNodeRelation(
                $sourceNode,
                FormNode::hydrateFromEntity($sourceQuestion),
                FormNodeRelationType::RADAR_CHART_THAT_USES,
                $chartNode->isBelowNode($sourceQuestion)
                    ? RelativePosition::Above
                    : RelativePosition::Below
            );
            $result[] = $relation;
        }

        return $result;
    }

    private static function throwExceptionIfFormNodeIsNotOfForm(Form $form, FormNodeEntity $node): void
    {
        if (!($form->getId() === $node->getForm()->getId())) {
            throw new \Exception('Form Id en FormNodeId are not of the same form!');
        }
    }
}
