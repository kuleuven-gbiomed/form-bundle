<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\Common;

class InfoUtil
{
    public static function extractLabel(array $info, string $language): string
    {
        $label = '<label>';
        if ((is_countable($info['label']) ? count($info['label']) : 0) > 0 && isset($info['label'][$language])) {
            $label = $info['label'][$language];
        }

        return $label;
    }

    public static function formatInfoForRendering(array $info): \stdClass
    {
        $result = new \stdClass();

        // format the label
        if (0 === (is_countable($info['label']) ? count($info['label']) : 0)) {
            $result->label = new \stdClass();
        } else {
            $result->label = $info['label'];
        }

        // format the description
        if (0 === (is_countable($info['description']) ? count($info['description']) : 0)) {
            $result->description = new \stdClass();
        } else {
            $result->description = $info['description'];
        }

        // format the role descriptions
        if (0 === (is_countable($info['roleDescriptions']) ? count($info['roleDescriptions']) : 0)) {
            $result->roleDescriptions = new \stdClass();
        } else {
            $result->roleDescriptions = $info['roleDescriptions'];
        }

        return $result;
    }
}
