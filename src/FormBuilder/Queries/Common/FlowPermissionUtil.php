<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\Common;

class FlowPermissionUtil
{
    public static function findForStepAndRole(string $stepUid, string $role, array $flowPermission): \stdClass
    {
        $result = new \stdClass();
        $result->read = false;
        $result->write = false;

        foreach ($flowPermission as $stepFlow) {
            if ($stepFlow['stepUid'] === $stepUid) {
                foreach ($stepFlow['rolePermissions'] as $rolePermission) {
                    if ($rolePermission['role'] === $role) {
                        $result->read = $rolePermission['read'];
                        $result->write = $rolePermission['write'];
                    }
                }
            }
        }

        return $result;
    }
}
