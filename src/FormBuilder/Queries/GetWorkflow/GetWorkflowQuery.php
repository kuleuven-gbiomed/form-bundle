<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetWorkflow;

class GetWorkflowQuery
{
    // ------------------------------------------------------------------------------
    // query
    // ------------------------------------------------------------------------------

    public function get(int $formId): \stdClass
    {
        $result = new \stdClass();
        $result->form = 'get workflow detail for a form...';

        return $result;
    }
}
