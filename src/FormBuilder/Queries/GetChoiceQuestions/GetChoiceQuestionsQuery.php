<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetChoiceQuestions;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Queries\Common\InfoUtil;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class GetChoiceQuestionsQuery
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // get all the choice questions for a form
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function questions(int $formId, string $language): array
    {
        $result = [];
        $formEntity = $this->formRepo->getById($formId);

        $questions = $this->formNodeRepo->getNodesByFormAndSubType(
            $formEntity,
            \KUL\FormBundle\FormBuilder\Model\FormNode::SUB_TYPE_CHOICE
        );

        foreach ($questions as $question) {
            $questionResult = new \stdClass();
            $questionResult->id = $question->getId();
            $questionResult->uid = $question->getUid();
            $questionResult->label = InfoUtil::extractLabel($question->getInfo(), $language);
            $questionResult->options = [];
            $questionResult->position = $question->getPosition();
            foreach ($question->getOptions() as $option) {
                $optionResult = new \stdClass();
                $optionResult->id = $option->getId();
                $optionResult->uid = $option->getUid();
                $optionResult->label = InfoUtil::extractLabel($option->getInfo(), $language);
                $questionResult->options[] = $optionResult;
            }
            $result[] = $questionResult;
        }

        return $result;
    }
}
