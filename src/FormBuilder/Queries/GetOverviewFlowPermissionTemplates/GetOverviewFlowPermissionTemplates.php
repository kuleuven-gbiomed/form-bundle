<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetOverviewFlowPermissionTemplates;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermissionTemplate;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;
use KUL\FormBundle\FormBuilder\Queries\Common\FlowPermissionUtil;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionTemplateRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository;

class GetOverviewFlowPermissionTemplates
{
    private readonly FormRepository $formRepo;
    private readonly FormWorkflowRepository $formWorkflowRepo;
    private readonly FormNodeFlowPermissionTemplateRepository $formNodeFlowPermissionTemplateRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formWorkflowRepo = $entityManager->getRepository(FormWorkflow::class);
        $this->formNodeFlowPermissionTemplateRepo = $entityManager->getRepository(FormNodeFlowPermissionTemplate::class);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // query
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function templates(int $formId): array
    {
        $formEntity = $this->formRepo->getById($formId);
        $templates = $this->formNodeFlowPermissionTemplateRepo->getTemplatesByForm($formEntity);

        // get the available roles
        $participatingRoles = $formEntity->getRolesThatCouldParticipate();
        // get the available steps
        $formWorklowEntity = $this->formWorkflowRepo->getByForm($formEntity);
        $workflowSteps = $formWorklowEntity->getSteps()->getValues();

        $result = [];
        foreach ($templates as $template) {
            $resultTemplate = new \stdClass();
            $flowPermission = [];
            foreach ($workflowSteps as $step) {
                $flowPermission[$step->getUid()] = [];
                foreach ($participatingRoles as $role) {
                    $flowPermission[$step->getUid()][$role] = FlowPermissionUtil::findForStepAndRole(
                        $step->getUid(),
                        $role,
                        $template->getFlowPermission()
                    );
                }
            }
            $resultTemplate->id = $template->getId();
            $resultTemplate->contentHash = $template->getContentHash();
            $resultTemplate->flowPermission = $flowPermission;
            $resultTemplate->name = $template->getName();
            $result[] = $resultTemplate;
        }

        return $result;
    }
}
