<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetQuestionsByUidList;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Queries\Common\InfoUtil;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class GetQuestionsByUidListQuery
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function questions(int $formId, string $language, array $uidList): array
    {
        $result = [];
        $formEntity = $this->formRepo->getById($formId);
        $questions = $this->formNodeRepo->getQuestionsByFormAndListUid($formEntity, $uidList);

        foreach ($questions as $question) {
            $questionResult = new \stdClass();
            $questionResult->id = $question->getId();
            $questionResult->uid = $question->getUid();
            $questionResult->label = InfoUtil::extractLabel($question->getInfo(), $language);
            $questionResult->type = $question->getType();
            $questionResult->subtype = $question->getSubType();
            $questionResult->position = $question->getPosition();
            $result[] = $questionResult;
        }

        return $result;
    }
}
