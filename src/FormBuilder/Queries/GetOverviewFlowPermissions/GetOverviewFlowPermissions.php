<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetOverviewFlowPermissions;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode as FormNodeEntity;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;
use KUL\FormBundle\FormBuilder\Model\FormWorkFlowStep;
use KUL\FormBundle\FormBuilder\Queries\Common\FlowPermissionUtil;
use KUL\FormBundle\FormBuilder\Queries\Common\InfoUtil;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository;

class GetOverviewFlowPermissions
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;
    private readonly FormWorkflowRepository $formWorkflowRepo;
    private readonly FormNodeFlowPermissionRepository $formNodeFlowPermissionRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNodeEntity::class);
        $this->formWorkflowRepo = $entityManager->getRepository(FormWorkflow::class);
        $this->formNodeFlowPermissionRepo = $entityManager->getRepository(FormNodeFlowPermission::class);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // query
    // -----------------------------------------------------------------------------------------------------------------

    public function flowPermissions(int $formId): \stdClass
    {
        $formEntity = $this->formRepo->getById($formId);
        $formRootEntity = $this->formNodeRepo->getRootNodeForForm($formEntity);

        // ------------------------------------------------------------------------------------- get the available roles
        $participatingRoles = $formEntity->getRolesThatCouldParticipate();

        // ----------------------------------------------------------------------- get the available steps for this form
        $formWorklowEntity = $this->formWorkflowRepo->getByForm($formEntity);
        $workflowSteps = $formWorklowEntity->getSteps()->getValues();

        $result = new \stdClass();
        $result->roles = $participatingRoles;

        $resultSteps = [];
        foreach ($workflowSteps as $step) {
            $resultSteps[] = FormWorkFlowStep::hydrateFromEntity($step);
        }
        $result->steps = $resultSteps;

        $result->rootNode = $this->renderNodeFlowPermission($formRootEntity, $workflowSteps, $participatingRoles);

        return $result;
    }

    private function renderNodeFlowPermission(FormNodeEntity $formNodeEntity, array $steps, array $roles): \stdClass
    {
        $node = new \stdClass();
        $node->id = $formNodeEntity->getId();
        $node->position = $formNodeEntity->getPosition();
        $node->info = InfoUtil::formatInfoForRendering($formNodeEntity->getInfo());
        $node->type = $formNodeEntity->getType();
        $node->contentHash = $formNodeEntity->getContentHash();
        $node->children = [];

        if (FormNodeEntity::TYPE_QUESTION === $formNodeEntity->getType()) {
            // -------------------------------------------------------------------------- get the flow permission entity
            $flowPermissionEntity = $this->formNodeFlowPermissionRepo->getByFormNode($formNodeEntity);
            $flowPermission = [];
            foreach ($steps as $step) {
                $flowPermission[$step->getUid()] = [];
                foreach ($roles as $role) {
                    $flowPermission[$step->getUid()][$role] = FlowPermissionUtil::findForStepAndRole(
                        $step->getUid(),
                        $role,
                        $flowPermissionEntity->getFlowPermission()
                    );
                }
            }
            $node->flowPermission = $flowPermission;
        }

        foreach ($formNodeEntity->getChildren() as $childNode) {
            $node->children[] = $this->renderNodeFlowPermission($childNode, $steps, $roles);
        }

        return $node;
    }
}
