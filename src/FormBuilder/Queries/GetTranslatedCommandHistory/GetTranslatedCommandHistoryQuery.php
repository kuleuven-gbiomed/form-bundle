<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetTranslatedCommandHistory;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandTypes;
use KUL\FormBundle\FormBuilder\Entity\CommandHistoryEntry;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormInfo;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;
use KUL\FormBundle\FormBuilder\Repository\CommandHistoryEntryRepository;
use KUL\FormBundle\FormBuilder\Repository\FormInfoRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeOptionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class GetTranslatedCommandHistoryQuery
{
    private readonly CommandHistoryEntryRepository $historyRepo;
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;
    private readonly FormNodeOptionRepository $formNodeOptionRepo;
    private readonly FormInfoRepository $formInfoRepo;

    // ------------------------------------------------------------------------------
    // constructor
    // ------------------------------------------------------------------------------
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
        $this->historyRepo = $entityManager->getRepository(CommandHistoryEntry::class);
        $this->formRepo = $this->entityManager->getRepository(Form::class);
        $this->formNodeRepo = $this->entityManager->getRepository(FormNode::class);
        $this->formNodeOptionRepo = $this->entityManager->getRepository(FormNodeOption::class);
        $this->formInfoRepo = $this->entityManager->getRepository(FormInfo::class);
    }

    // ------------------------------------------------------------------------------
    // get history
    // ------------------------------------------------------------------------------

    /**
     * @return list<TranslateHistoryCommandDTO>
     *
     * @throws EntityNotFoundException
     */
    public function history(int $formId, string $language): array
    {
        $result = [];
        $form = $this->formRepo->getById($formId);
        $history = $this->historyRepo->getByFormUid($form->getUid());
        $formInfo = $this->formInfoRepo->getByForm($form);

        foreach ($history as $historyItem) {
            // -- default label from the form info
            $label = $formInfo->getLabel()[strtolower($language)] ?? '[label]';
            // -- overwrite label
            switch ($historyItem->getSubject()) {
                case 'node':
                    $node = $this->formNodeRepo->findById($historyItem->getSubjectId());
                    if (null === $node) {
                        $label = '[deleted item - '.$historyItem->getSubjectId().']';
                    } else {
                        $label = $this->extractLabelFromInfo($node->getInfo(), $language);
                    }
                    break;
                case 'option':
                    $option = $this->formNodeOptionRepo->findById($historyItem->getSubjectId());
                    if (null === $option) {
                        $label = '[deleted item - '.$historyItem->getSubjectId().']';
                    } else {
                        $label = $this->extractLabelFromInfo($option->getInfo(), $language);
                    }
                    break;
            }
            $commandLog = TranslateHistoryCommandDTO::make(
                $historyItem->getId(),
                $historyItem->getType(),
                $historyItem->getSubject(),
                $historyItem->getSubjectId(),
                $historyItem->getCurrentVersionNumber(),
                $label,
                $historyItem->getUser(),
                $historyItem->getTimestamp(),
            );
            $result[] = $commandLog;
        }

        return $result;
    }

    /**
     * @return list<TranslateHistoryCommandDTO>
     *
     * @throws EntityNotFoundException
     */
    public function historySinceLastPublish(int $formId, string $language): array
    {
        $result = [];
        $historyList = $this->history($formId, $language);
        foreach ($historyList as $historyListItem) {
            if (CommandTypes::PUBLISH_FORM === $historyListItem->getCommand()
                || CommandTypes::RESET_TO_WORKING_VERSION === $historyListItem->getCommand()) {
                break;
            }
            $result[] = $historyListItem;
        }

        return $result;
    }

    public function countHistoryItemsSinceLastPublish(int $formId, string $language): int
    {
        return count($this->historySinceLastPublish($formId, $language));
    }

    public function countHistoryItemsSinceLastPublishByForm(Form $form): int
    {
        $result = 0;
        $history = $this->historyRepo->getByFormUid($form->getUid());

        foreach ($history as $historyItem) {
            if (CommandTypes::PUBLISH_FORM === $historyItem->getType()
                || CommandTypes::RESET_TO_WORKING_VERSION === $historyItem->getType()) {
                break;
            }
            ++$result;
        }

        return $result;
    }

    private function extractLabelFromInfo(array $info, string $language): string
    {
        $label = '[label]';
        if (true == isset($info['label'])) {
            if (isset($info['label'][$language])) {
                $label = $info['label'][$language];
            }
        }

        return $label;
    }
}
