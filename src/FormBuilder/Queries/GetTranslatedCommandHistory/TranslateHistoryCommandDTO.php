<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Queries\GetTranslatedCommandHistory;

class TranslateHistoryCommandDTO implements \JsonSerializable
{
    private function __construct(
        private readonly int $id,
        private readonly string $command,
        private readonly string $subject,
        private readonly int $subjectId,
        private readonly int $versionNumber,
        private readonly string $label,
        private readonly string $by,
        private readonly \DateTimeImmutable $at,
    ) {
    }

    public static function make(
        int $id,
        string $command,
        string $subject,
        int $subjectId,
        int $versionNumber,
        string $label,
        string $by,
        \DateTimeImmutable $at,
    ): self {
        return new self($id, $command, $subject, $subjectId, $versionNumber, $label, $by, $at);
    }

    public function jsonSerialize(): \stdClass
    {
        $json = new \stdClass();
        $json->id = $this->getId();
        $json->command = $this->getCommand();
        $json->subject = $this->getSubject();
        $json->subjectId = $this->getSubjectId();
        $json->versionNumber = $this->getVersionNumber();
        $json->label = $this->getLabel();
        $json->by = $this->getBy();
        $json->at = $this->getAt()->format(\DateTimeInterface::ATOM);

        return $json;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCommand(): string
    {
        return $this->command;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getSubjectId(): int
    {
        return $this->subjectId;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getBy(): string
    {
        return $this->by;
    }

    public function getAt(): \DateTimeImmutable
    {
        return $this->at;
    }

    public function getVersionNumber(): int
    {
        return $this->versionNumber;
    }
}
