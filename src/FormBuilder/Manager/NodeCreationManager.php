<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Manager;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermissionTemplate;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;
use KUL\FormBundle\FormBuilder\Repository\FormNodeOptionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HtmlSanitizer\HtmlSanitizer;
use Symfony\Component\HtmlSanitizer\HtmlSanitizerConfig;

readonly class NodeCreationManager
{
    private FormNodeRepository $formNodeRepository;
    private FormNodeOptionRepository $formNodeOptionRepository;
    private HtmlSanitizer $htmlSanitizer;

    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
        $this->formNodeRepository = $this->entityManager->getRepository(FormNode::class);
        $this->formNodeOptionRepository = $this->entityManager->getRepository(FormNodeOption::class);
        $this->htmlSanitizer = new HtmlSanitizer(
            (new HtmlSanitizerConfig())->allowSafeElements()
        );
    }

    // -- category ------------------------------------------------------------

    private function createDefaultCategory(
        FormNode $newNode,
        string $label,
        string $language,
    ): FormNode {
        $newNode->importFields(
            false,
            $this->createInfo($language, $label),
            false,
            'default',
            [],
            [],
            [],
            [],
        );
        $newNode->makeContentHash();
        $this->formNodeRepository->save($newNode);

        return $newNode;
    }

    // -- text question -------------------------------------------------------

    private function createDefaultTextQuestion(
        FormNode $newNode,
        string $label,
        string $language,
    ): FormNode {
        // -- info config -- {"placeholder": [], "descriptionDisplayMode": "box"}
        $infoConfig = [];
        $infoConfig['placeholder'] = [];
        $infoConfig['descriptionDisplayMode'] = 'box';

        // -- props config -- {"maxLength": 255, "minLength": 0, "multiLine": false, "predefinedAnswers": []}
        $propsConfig = [];
        $propsConfig['minLength'] = 0;
        $propsConfig['maxLength'] = 255;
        $propsConfig['multiLine'] = false;
        $propsConfig['predefinedAnswers'] = [];

        // -- prefill config -- {"prefillingQuestionUids": []}
        $prefillConfig = [];
        $prefillConfig['prefillingQuestionUids'] = [];

        $newNode->importFields(
            false,
            $this->createInfo($language, $label),
            false,
            'regular',
            [],
            $infoConfig,
            $propsConfig,
            [],
            $prefillConfig,
        );
        $newNode->makeContentHash();
        $this->formNodeRepository->save($newNode);

        return $newNode;
    }

    // -- choice question ------------------------------------------------------

    private function createDefaultChoiceQuestion(
        FormNode $newNode,
        string $label,
        string $language,
    ): FormNode {
        // -- info config -- {"placeholder": [], "descriptionDisplayMode": "box"}
        $infoConfig = [];
        $infoConfig['placeholder'] = [];
        $infoConfig['descriptionDisplayMode'] = 'box';

        // -- props config
        // -- {"expanded": true, "multiple": false, "maxSelect": null, "minSelect": null, "custom_options": [], "sortOptionsAlphabetically": false}
        $propsConfig = [];
        $propsConfig['expanded'] = true;
        $propsConfig['multiple'] = false;
        $propsConfig['maxSelect'] = null;
        $propsConfig['minSelect'] = null;
        $propsConfig['custom_options'] = [];
        $propsConfig['sortOptionsAlphabetically'] = false;

        // -- scoring config
        // {"weighingType": "absolute",
        // "scoringParameters": {"scale": null, "maxScore": null, "minScore": null, "passScore": null, "roundingMode": null,
        // "showAutoCalculateBtn": true, "allowNonAutoCalculatedValue": true},
        // "rolesAllowedToSeeOptionScoreValues": [], "globalScoreCalculationDependencyConfigs": []}
        $scoringConfig = [];
        $scoringConfig['weighingType'] = 'absolute';
        $scoringConfig['scoringParameters'] = [];
        $scoringConfig['scoringParameters']['scale'] = null;
        $scoringConfig['scoringParameters']['maxScore'] = null;
        $scoringConfig['scoringParameters']['minScore'] = null;
        $scoringConfig['scoringParameters']['passScore'] = null;
        $scoringConfig['scoringParameters']['roundingMode'] = null;
        $scoringConfig['scoringParameters']['showAutoCalculateBtn'] = true;
        $scoringConfig['scoringParameters']['allowNonAutoCalculatedValue'] = true;
        $scoringConfig['rolesAllowedToSeeOptionScoreValues'] = [];
        $scoringConfig['globalScoreCalculationDependencyConfigs'] = [];

        // -- prefill config -- {"prefillingQuestionUids": []}
        $prefillConfig = [];
        $prefillConfig['prefillingQuestionUids'] = [];

        $newNode->importFields(
            false,
            $this->createInfo($language, $label),
            false,
            'regular',
            [],
            $infoConfig,
            $propsConfig,
            $scoringConfig,
            $prefillConfig,
        );
        $newNode->makeContentHash();
        $this->formNodeRepository->save($newNode);

        // -- create two default options
        for ($i = 1; $i <= 2; ++$i) {
            $option = new FormNodeOption(
                Uuid::uuid4()->toString(),
                $i - 1,
                FormNodeOption::TYPE_QUESTION_OPTION,
                $newNode
            );
            $propsConfig = [];
            $propsConfig['type'] = 'option';
            $option->importFields(
                false,
                'regular',
                false,
                $this->createInfo($language, 'Option '.(string) $i),
                false,
                false,
                'icon',
                [],
                [],
                FormNodeOption::INPUT_TYPE_OPTION,
                $propsConfig,
                null
            );
            $option->makeContentHash();
            $this->formNodeOptionRepository->save($option);
        }

        return $newNode;
    }

    // -- number question ----------------------------------------------------

    private function createDefaultNumberQuestion(
        FormNode $newNode,
        string $label,
        string $language,
    ): FormNode {
        // -- info config -- {"placeholder": [], "descriptionDisplayMode": "box"}
        $infoConfig = [];
        $infoConfig['placeholder'] = [];
        $infoConfig['descriptionDisplayMode'] = 'box';

        // -- props config
        // {"max": 100, "min": 0, "scale": 0, "roundingMode": 6, "stepInterval": 0, "custom_options": [], "showMaxAsInputAddon": false,
        // "showMinAsInputAddon": false}
        $propsConfig = [];
        $propsConfig['max'] = 100;
        $propsConfig['min'] = 0;
        $propsConfig['scale'] = 0;
        $propsConfig['roundingMode'] = 6;
        $propsConfig['stepInterval'] = 0;
        $propsConfig['custom_options'] = [];
        $propsConfig['showMaxAsInputAddon'] = false;
        $propsConfig['showMinAsInputAddon'] = false;

        // -- scoring config
        // {"passScore": null, "weighingType": "absolute", "showAutoCalculateBtn": true, "allowNonAutoCalculatedValue": true,
        // "globalScoreCalculationDependencyConfigs": []}
        $scoringConfig = [];
        $scoringConfig['passScore'] = null;
        $scoringConfig['weighingType'] = 'absolute';
        $scoringConfig['showAutoCalculateBtn'] = true;
        $scoringConfig['allowNonAutoCalculatedValue'] = true;
        $scoringConfig['globalScoreCalculationDependencyConfigs'] = [];

        // -- prefill config -- {"prefillingQuestionUids": []}
        $prefillConfig = [];
        $prefillConfig['prefillingQuestionUids'] = [];

        $newNode->importFields(
            false,
            $this->createInfo($language, $label),
            false,
            'regular',
            [],
            $infoConfig,
            $propsConfig,
            $scoringConfig,
            $prefillConfig,
        );
        $newNode->makeContentHash();
        $this->formNodeRepository->save($newNode);

        return $newNode;
    }

    // -- date question -------------------------------------------------------

    private function createDefaultDateQuestion(
        FormNode $newNode,
        string $label,
        string $language,
    ): FormNode {
        // -- info config -- {"placeholder": [], "descriptionDisplayMode": "box"}
        $infoConfig = [];
        $infoConfig['placeholder'] = [];
        $infoConfig['descriptionDisplayMode'] = 'box';

        // -- props config -- {"formatMask": "dd/mm/yy"}
        $propsConfig = [];
        $propsConfig['formatMask'] = 'dd/mm/yy';

        // -- prefill config -- {"prefillingQuestionUids": []}
        $prefillConfig = [];
        $prefillConfig['prefillingQuestionUids'] = [];

        $newNode->importFields(
            false,
            $this->createInfo($language, $label),
            false,
            'regular',
            [],
            $infoConfig,
            $propsConfig,
            [],
            $prefillConfig,
        );
        $newNode->makeContentHash();
        $this->formNodeRepository->save($newNode);

        return $newNode;
    }

    // -- upload question ------------------------------------------------------

    private function createDefaultUploadQuestion(
        FormNode $newNode,
        string $label,
        string $language,
    ): FormNode {
        // -- info config -- {"placeholder": [], "descriptionDisplayMode": "box"}
        $infoConfig = [];
        $infoConfig['placeholder'] = [];
        $infoConfig['descriptionDisplayMode'] = 'box';

        // -- props config
        // {"maxAmount": 4, "mimeTypes": ["text/plain"], "minAmount": 0, "extensions": ["txt"], "maxFileSize": 10000}
        $propsConfig = [];
        $propsConfig['maxAmount'] = 1;
        $propsConfig['mimeTypes'] = ['text/plain'];
        $propsConfig['extensions'] = ['txt'];
        $propsConfig['minAmount'] = 0;
        $propsConfig['maxFileSize'] = 10000;

        $newNode->importFields(
            false,
            $this->createInfo($language, $label),
            false,
            'regular',
            [],
            $infoConfig,
            $propsConfig,
            [],
            [],
        );

        $newNode->makeContentHash();
        $this->formNodeRepository->save($newNode);

        return $newNode;
    }

    // -- radar chart node  ---------------------------------------------------

    private function createDefaultRadarChartNode(
        FormNode $newNode,
        string $label,
        string $language,
    ): FormNode {
        // -- info config -- {"placeholder": [], "descriptionDisplayMode": "box"}
        $infoConfig = [];
        $infoConfig['placeholder'] = [];
        $infoConfig['descriptionDisplayMode'] = 'box';

        // -- props config
        // {"unit_sign": null, "hide_point_values": true, "hide_scale_values": true, "question_references": [],
        // "hide_point_unit_suffix": true, "hide_scale_unit_suffix": true, "ignore_individual_question_rights": true}
        $propsConfig = [];
        $propsConfig['unit_sign'] = null;
        $propsConfig['hide_point_values'] = false;
        $propsConfig['hide_scale_values'] = false;
        $propsConfig['question_references'] = [];
        $propsConfig['hide_point_unit_suffix'] = false;
        $propsConfig['hide_scale_unit_suffix'] = false;
        $propsConfig['ignore_individual_question_rights'] = true;

        $newNode->importFields(
            false,
            $this->createInfo($language, $label),
            false,
            'regular',
            [],
            $infoConfig,
            $propsConfig,
            [],
            [],
        );

        $newNode->makeContentHash();
        $this->formNodeRepository->save($newNode);

        return $newNode;
    }

    private function createDefaultReadOnlyViewBasicNode(
        FormNode $newNode,
        string $label,
        string $language,
    ): FormNode {
        // -- info config -- {"placeholder": [], "descriptionDisplayMode": "box"}
        $infoConfig = [];
        $infoConfig['placeholder'] = [];
        $infoConfig['descriptionDisplayMode'] = 'box';

        $newNode->importFields(
            false,
            $this->createInfo($language, $label),
            false,
            'default',
            [],
            $infoConfig
        );
        $newNode->makeContentHash();
        $this->formNodeRepository->save($newNode);

        return $newNode;
    }

    // ————————————————————————————————————————————————————————————————————————

    private function initNode(
        string $type,
        string $subType,
        ?FormNode $parent,
        int $sequence,
        Form $form,
    ): FormNode {
        $level = 0;
        if (null !== $parent) {
            $level = $parent->getLevel() + 1;
        }

        return new FormNode(
            Uuid::uuid4()->toString(),
            $parent,
            $level,
            $sequence,
            $type,
            $subType,
            $form
        );
    }

    // -- create info object  -------------------------------------------------

    private function createInfo(string $language, string $label): array
    {
        $info = [];
        $info['label'] = [];
        $info['label'][$language] = $this->htmlSanitizer->sanitize(trim($label));
        $info['description'] = [];
        $info['roleDescriptions'] = [];

        return $info;
    }

    // -- create the permissions for a question -------------------------------------

    private function createPermissions(FormNode $node, int $permissionTemplateId): FormNodeFlowPermission
    {
        $permissionRepo = $this->entityManager->getRepository(FormNodeFlowPermission::class);
        $permission = new FormNodeFlowPermission(
            FormNodeFlowPermission::QUESTION,
            $node,
            null
        );
        $permission->importFields([]);

        if (0 !== $permissionTemplateId) {
            $permissionTemplateRepo = $this->entityManager->getRepository(FormNodeFlowPermissionTemplate::class);
            $permissionTemplate = $permissionTemplateRepo->getById($permissionTemplateId);
            $permission->importFields($permissionTemplate->getFlowPermission());
            $permission->setTemplate($permissionTemplate);
        }

        $permission->makeContentHash();
        $permissionRepo->save($permission);

        return $permission;
    }

    // -- create the default permissions for a category ---------------------------------

    private function createCategoryPermissions(FormNode $node): FormNodeFlowPermission
    {
        $permissionRepo = $this->entityManager->getRepository(FormNodeFlowPermission::class);
        $permission = new FormNodeFlowPermission(
            FormNodeFlowPermission::DEFAULT_CATEGORY,
            $node,
            null
        );
        $permission->importFields([]);
        $permission->makeContentHash();
        $permissionRepo->save($permission);

        return $permission;
    }

    /**
     * @throws \JsonException
     */
    public function createDefaultNodeWithPermissions(string $nodeType, ?FormNode $parent, int $insertSequence, Form $form, string $label, string $language, int $permissionTemplateId): FormNode
    {
        // -- create default node in the database with permissions ----------
        switch ($nodeType) {
            case FormNode::SUB_TYPE_TEXT:
                $newNode = $this->initNode(
                    FormNode::TYPE_QUESTION,
                    FormNode::SUB_TYPE_TEXT,
                    $parent,
                    $insertSequence,
                    $form
                );
                $newNode = $this->createDefaultTextQuestion(
                    $newNode,
                    $label,
                    $language
                );
                $this->createPermissions($newNode, $permissionTemplateId);
                break;

            case FormNode::SUB_TYPE_CHOICE:
                $newNode = $this->initNode(
                    FormNode::TYPE_QUESTION,
                    FormNode::SUB_TYPE_CHOICE,
                    $parent,
                    $insertSequence,
                    $form
                );
                $newNode = $this->createDefaultChoiceQuestion(
                    $newNode,
                    $label,
                    $language
                );
                $this->createPermissions($newNode, $permissionTemplateId);
                break;

            case FormNode::SUB_TYPE_NUMBER:
                $newNode = $this->initNode(
                    FormNode::TYPE_QUESTION,
                    FormNode::SUB_TYPE_NUMBER,
                    $parent,
                    $insertSequence,
                    $form
                );
                $newNode = $this->createDefaultNumberQuestion(
                    $newNode,
                    $label,
                    $language
                );
                $this->createPermissions($newNode, $permissionTemplateId);
                break;

            case FormNode::SUB_TYPE_DATE:
                $newNode = $this->initNode(
                    FormNode::TYPE_QUESTION,
                    FormNode::SUB_TYPE_DATE,
                    $parent,
                    $insertSequence,
                    $form
                );
                $newNode = $this->createDefaultDateQuestion(
                    $newNode,
                    $label,
                    $language
                );
                $this->createPermissions($newNode, $permissionTemplateId);
                break;

            case FormNode::SUB_TYPE_UPLOAD:
                $newNode = $this->initNode(
                    FormNode::TYPE_QUESTION,
                    FormNode::SUB_TYPE_UPLOAD,
                    $parent,
                    $insertSequence,
                    $form
                );
                $newNode = $this->createDefaultUploadQuestion(
                    $newNode,
                    $label,
                    $language
                );
                $this->createPermissions($newNode, $permissionTemplateId);
                break;

            case FormNode::SUB_TYPE_RADAR_CHART:
                $newNode = $this->initNode(
                    FormNode::TYPE_QUESTION,
                    FormNode::SUB_TYPE_RADAR_CHART,
                    $parent,
                    $insertSequence,
                    $form
                );
                $newNode = $this->createDefaultRadarChartNode(
                    $newNode,
                    $label,
                    $language,
                );
                $this->createPermissions($newNode, $permissionTemplateId);
                break;

            case FormNode::SUB_TYPE_READ_ONLY_VIEW_BASIC:
                $newNode = $this->initNode(
                    FormNode::TYPE_QUESTION,
                    FormNode::SUB_TYPE_READ_ONLY_VIEW_BASIC,
                    $parent,
                    $insertSequence,
                    $form
                );
                $newNode = $this->createDefaultReadOnlyViewBasicNode(
                    $newNode,
                    $label,
                    $language,
                );
                $this->createPermissions($newNode, $permissionTemplateId);
                break;

            default: // category
                $newNode = $this->initNode(
                    FormNode::TYPE_CATEGORY,
                    FormNode::SUB_TYPE_CATEGORY,
                    $parent,
                    $insertSequence,
                    $form
                );
                $newNode = $this->createDefaultCategory(
                    $newNode,
                    $label,
                    $language
                );
                $this->createCategoryPermissions($newNode);
                break;
        }

        // -- fix the sequencing of all related nodes in the parent (after the inserted node)
        if (null !== $parent) {
            foreach ($parent->getChildren() as $childNode) {
                if ($childNode->getSequence() >= $insertSequence
                    && $childNode->getId() !== $newNode->getId()) {
                    $childNode->setSequence($childNode->getSequence() + 1);
                    $childNode->makeContentHash();
                    $this->formNodeRepository->save($childNode);
                }
            }
        }

        return $newNode;
    }
}
