<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;

class RemovalManager
{
    private readonly FormRepository $formRepo;
    private readonly FormNodeRepository $formNodeRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
    }

    /**
     * @throws EntityNotFoundException
     */
    public function removeFormBuilderData(string $templateId): bool
    {
        // Via the cascading configuration in the database, all related rows in other tables will be also deleted.
        $form = $this->formRepo->getByUid($templateId);
        $this->formRepo->deleteForm($form);

        // Delete history entries related to $form
        $query = $this->entityManager->createQuery(
            'DELETE FROM KUL\FormBundle\FormBuilder\Entity\CommandHistoryEntry c WHERE c.formUid = :uid'
        );
        $query->setParameter('uid', $templateId);
        $query->execute();

        return true;
    }

    public function removeFormBuilderDataOnlyNodes(string $templateId): bool
    {
        $form = $this->formRepo->getByUid($templateId);

        foreach ($form->getNodes()->getValues() as $node) {
            // Via the cascading configuration in the database, all related rows in other tables will be also deleted.
            $this->formNodeRepo->delete($node);
        }

        return true;
    }

    public function removeFormBuilderWorkflow(string $templateId): void
    {
        $query = $this->entityManager->createQuery(
            'DELETE FROM KUL\FormBundle\FormBuilder\Entity\FormWorkflow w WHERE w.uid = :uid'
        );
        $query->setParameter('uid', $templateId);
        $query->execute();
    }
}
