<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Manager;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\AbstractParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Entity\TemplateRepository;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormInfo;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;
use KUL\FormBundle\FormBuilder\Repository\FormInfoRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository;

class ConstructionManager
{
    private readonly FormRepository $formRepo;
    private readonly FormInfoRepository $formInfoRepo;
    private readonly FormWorkflowRepository $formWorkflowRepo;
    private readonly FormNodeRepository $formNodeRepo;
    private readonly FormNodeFlowPermissionRepository $formNodeFlowPermissionRepo;

    // -----------------------------------------------------------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(private readonly TemplateRepository $templateRepo, EntityManagerInterface $entityManager)
    {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formInfoRepo = $entityManager->getRepository(FormInfo::class);
        $this->formWorkflowRepo = $entityManager->getRepository(FormWorkflow::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
        $this->formNodeFlowPermissionRepo = $entityManager->getRepository(FormNodeFlowPermission::class);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Validation & publication functions
    // -----------------------------------------------------------------------------------------------------------------

    public function createWorkingVersion(int $formId): WorkingVersion
    {
        $workingVersionObjects = $this->constructWorkingVersion($formId);
        $resultJsonString = json_encode($workingVersionObjects, \JSON_THROW_ON_ERROR);

        return WorkingVersion::fromJsonDecodedArray(json_decode($resultJsonString, true, 512, \JSON_THROW_ON_ERROR));
    }

    public function validateForm(int $formId): array
    {
        try {
            $workingVersion = $this->createWorkingVersion($formId);

            $formEntity = $this->formRepo->getById($formId);
            $template = $this->templateRepo->getOneById($formEntity->getUid());
            $template->updateWorkingVersion($workingVersion);

            $resultMessages = [];
            $validationMessages = $template->getInvalidPublishMessages();
            foreach ($validationMessages as $message) {
                $resultMessage = new \stdClass();
                $resultMessage->message = $message->getMessage();
                $resultMessage->type = $message::class;
                $resultMessages[] = $resultMessage;
            }
        } catch (\Exception $e) {
            $resultMessage = new \stdClass();
            $resultMessage->message = $e->getMessage();
            $resultMessage->type = $e::class;
            $resultMessages = [$resultMessage];
        }

        return $resultMessages;
    }

    public function publishForm(int $formId, bool $useNewVersionNumber): bool
    {
        $workingVersion = $this->createWorkingVersion($formId);
        $formEntity = $this->formRepo->getById($formId);
        $template = $this->templateRepo->getOneById($formEntity->getUid());

        $template->updateWorkingVersion($workingVersion);
        $template->setShowPositionNumbers($formEntity->isShowPositionNumbers());
        if ($formEntity->isEnableReadOnlyChoiceAnswerOptionsToggling()) {
            $template->enableReadOnlyChoiceAnswerOptionsToggling();
        }

        if ($useNewVersionNumber) {
            $template->publishNewCurrentVersion();
            $this->templateRepo->save($template);

            return true;
        } else {
            if ($template->canRePublishAsExistingCurrentVersion()) {
                $template->rePublishAsExistingCurrentVersion();
                $this->templateRepo->save($template);

                return true;
            } else {
                throw new \Exception('Could not publish form with the same version number.');
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Construction unctions
    // -----------------------------------------------------------------------------------------------------------------

    private function constructWorkingVersion(int $formId): \stdClass
    {
        $result = new \stdClass();

        // ---------------------------------------------------------------------------------------------- construct info
        $formEntity = $this->formRepo->getById($formId);
        $formInfoEntity = $this->formInfoRepo->getByForm($formEntity);
        $formInfoModel = \KUL\FormBundle\FormBuilder\Model\FormInfo::hydrateFromEntity($formInfoEntity);
        $result->info = $formInfoModel->constructJson();

        // ------------------------------------------------------------------------------------------ construct workflow
        $workFlowEntity = $this->formWorkflowRepo->getByForm($formEntity);
        $workFlowModel = \KUL\FormBundle\FormBuilder\Model\FormWorkflow::hydrateFromEntity($workFlowEntity);
        $result->workFlow = $workFlowModel->constructJson();

        // ------------------------------------------------------------------------------------------ construct formlist
        $result->formList = new \stdClass();
        $rootNodeEntity = $this->formNodeRepo->getRootNodeForForm($formEntity);
        $result->formList->root = $this->constructFormItem($rootNodeEntity);

        return $result;
    }

    private function constructFormItem(FormNode $itemEntity): \stdClass
    {
        if (FormNode::TYPE_CATEGORY === $itemEntity->getType()) {
            $item = $this->constructCategory($itemEntity);
        } else {
            $item = $this->constructQuestion($itemEntity);
        }

        return $item;
    }

    private function constructCategory(FormNode $itemEntity): \stdClass
    {
        $category = new \stdClass();

        $category->uid = $itemEntity->getUid();
        $category->type = $itemEntity->getType();
        $category->info = $itemEntity->getInfo();
        $category->hidden = $itemEntity->getHidden();

        $category->children = [];
        foreach ($itemEntity->getChildren()->getValues() as $childEntity) {
            $category->children[] = $this->constructFormItem($childEntity);
        }

        if (!(0 === $itemEntity->getLevel())) {
            $workflowPermission = $this->formNodeFlowPermissionRepo->getByFormNode($itemEntity);
            $category->default_flow_permission = $workflowPermission->getFlowPermission();
        }

        $category->displayMode = $itemEntity->getDisplayMode();
        $category->firstHeaderCellTitle = $itemEntity->getFirstHeaderCellTitle();

        return $category;
    }

    // -- construct a question node ------------------------------------------------------------------------------------

    private function constructQuestion(FormNode $itemEntity): \stdClass
    {
        $question = new \stdClass();

        $question->uid = $itemEntity->getUid();
        $question->type = $itemEntity->getSubType();

        $workflowPermissionEntity = $this->formNodeFlowPermissionRepo->getByFormNode($itemEntity);
        $question->flowPermission = $workflowPermissionEntity->getFlowPermission();
        $question->info = $itemEntity->getInfo();
        $question->hidden = $itemEntity->getHidden();
        // -- info config
        $question->descriptionDisplayMode = $itemEntity->getInfoConfig()['descriptionDisplayMode'];

        switch ($itemEntity->getSubType()) {
            case FormNode::SUB_TYPE_CHOICE:
                $question->input = $this->constructChoiceInput($itemEntity);
                $question->children = $this->constructOptions($itemEntity);
                if (isset($itemEntity->getPrefillConfig()['prefillingQuestionUids'])) {
                    $question->prefillingQuestionUids = $itemEntity->getPrefillConfig()['prefillingQuestionUids'];
                }
                break;
            case FormNode::SUB_TYPE_NUMBER:
                $question->input = $this->constructNumberInput($itemEntity);
                break;
            case FormNode::SUB_TYPE_TEXT:
                $question->input = $this->constructTextInput($itemEntity);
                if (isset($itemEntity->getPrefillConfig()['prefillingQuestionUids'])) {
                    $question->prefillWithIds = $itemEntity->getPrefillConfig()['prefillingQuestionUids'];
                }
                break;
            case FormNode::SUB_TYPE_UPLOAD:
                $question->input = $this->constructUploadInput($itemEntity);
                break;
            case FormNode::SUB_TYPE_DATE:
                $question->input = $this->constructDateInput($itemEntity);
                break;
            case FormNode::SUB_TYPE_RADAR_CHART:
                // -- no input node but
                // -- extra properties for read_only_view_radar_chart
                $question->{RadarChartViewNode::KEY_QUESTION_REFERENCES} = $itemEntity->getPropsConfig()[RadarChartViewNode::KEY_QUESTION_REFERENCES];
                $question->{RadarChartViewNode::KEY_UNIT_SIGN} = $itemEntity->getPropsConfig()[RadarChartViewNode::KEY_UNIT_SIGN];
                $question->{RadarChartViewNode::KEY_HIDE_POINT_VALUES} = $itemEntity->getPropsConfig()[RadarChartViewNode::KEY_HIDE_POINT_VALUES];
                $question->{RadarChartViewNode::KEY_HIDE_POINT_UNIT_SUFFIX} = $itemEntity->getPropsConfig()[RadarChartViewNode::KEY_HIDE_POINT_UNIT_SUFFIX];
                $question->{RadarChartViewNode::KEY_HIDE_SCALE_VALUES} = $itemEntity->getPropsConfig()[RadarChartViewNode::KEY_HIDE_SCALE_VALUES];
                $question->{RadarChartViewNode::KEY_HIDE_SCALE_UNIT_SUFFIX} = $itemEntity->getPropsConfig()[RadarChartViewNode::KEY_HIDE_SCALE_UNIT_SUFFIX];
                $question->{RadarChartViewNode::KEY_IGNORE_INDIVIDUAL_QUESTION_RIGHTS} = $itemEntity->getPropsConfig()[RadarChartViewNode::KEY_IGNORE_INDIVIDUAL_QUESTION_RIGHTS];
        }

        // -- render custom options
        if (isset($itemEntity->getPropsConfig()['custom_options'])) {
            $question->custom_options = $itemEntity->getPropsConfig()['custom_options'];
        }

        // -- scoring config
        if (isset($itemEntity->getScoringConfig()['weighingType'])) {
            $question->weighingType = $itemEntity->getScoringConfig()['weighingType'];
        }
        if (isset($itemEntity->getScoringConfig()['rolesAllowedToSeeOptionScoreValues'])) {
            $question->rolesAllowedToSeeOptionScoreValues = $itemEntity->getScoringConfig()['rolesAllowedToSeeOptionScoreValues'];
        }
        if (isset($itemEntity->getScoringConfig()['globalScoreCalculationDependencyConfigs'])) {
            $question->globalScoreCalculationDependencyConfigs = $itemEntity->getScoringConfig()['globalScoreCalculationDependencyConfigs'];
        }
        if (isset($itemEntity->getMultiUnlockConfig()['multiUnlockingQuestionUids'])) {
            $question->{AbstractParentalInputNode::KEY_MULTI_UNLOCKING_QUESTION_UIDS} = $itemEntity->getMultiUnlockConfig()['multiUnlockingQuestionUids'];
        }

        return $question;
    }

    // -- construct a choice input  ------------------------------------------------------------------------------------

    private function constructChoiceInput(FormNode $itemEntity): \stdClass
    {
        $input = new \stdClass();
        $input->type = 'choice';
        $input->required = $itemEntity->getRequired();
        $input->displayMode = $itemEntity->getDisplayMode();
        $input->placeholder = $itemEntity->getInfoConfig()['placeholder'];
        $input->multiple = $itemEntity->getPropsConfig()['multiple'];
        $input->expanded = $itemEntity->getPropsConfig()['expanded'];
        $input->minSelect = $itemEntity->getPropsConfig()['minSelect'];
        $input->maxSelect = $itemEntity->getPropsConfig()['maxSelect'];
        $input->sortOptionsAlphabetically = $itemEntity->getPropsConfig()['sortOptionsAlphabetically'];
        $input->scoringParameters = $itemEntity->getScoringConfig()['scoringParameters'];

        return $input;
    }

    // -- construct the options ----------------------------------------------------------------------------------------

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    private function constructOptions(FormNode $itemEntity): array
    {
        $options = [];
        foreach ($itemEntity->getOptions()->getValues() as $optionEntity) {
            $option = new \stdClass();
            $option->uid = $optionEntity->getUid();
            $option->type = $optionEntity->getType();

            $option->input = new \stdClass();
            $option->input->type = $optionEntity->getInputType();
            $option->input->required = $optionEntity->isRequired();
            $option->input->displayMode = $optionEntity->getDisplayMode();
            $option->input->defaultSelected = $optionEntity->isDefaultSelected();
            $option->input->score = $optionEntity->getScore();
            $option->input->hideLabelInRelevantChoiceDisplayMode = $optionEntity->getHideLabelInRelevantChoiceDisplayMode();
            $option->input->descriptionDisplayMode = $optionEntity->getDescriptionDisplayMode();

            if ('optionWithAdditionalText' === $optionEntity->getInputType()) {
                $option->input->additionalTextRequired = $optionEntity->getPropsConfig()['additionalTextRequired'];
                $option->input->additionalTextMinLength = $optionEntity->getPropsConfig()['additionalTextMinLength'];
                $option->input->additionalTextMaxLength = $optionEntity->getPropsConfig()['additionalTextMaxLength'];
            }

            // $workflowPermissionEntity = $this->formNodeFlowPermissionRepo->getByFormNodeOption($optionEntity);
            // $option->flowPermission = $workflowPermissionEntity->getFlowPermission();

            $option->flowPermission = [];
            $option->info = $optionEntity->getInfo();
            $option->hidden = $optionEntity->getHidden();
            $option->unlocksQuestionIds = $optionEntity->getUnlocksQuestionIds();
            $option->{OptionInputNode::KEY_MULTI_UNLOCKS_QUESTION_IDS} = $optionEntity->getMultiUnlocksQuestionIds();

            $options[] = $option;
        }

        return $options;
    }

    // -- construct a text input ---------------------------------------------------------------------------------------

    private function constructTextInput(FormNode $itemEntity): \stdClass
    {
        $input = new \stdClass();
        $input->type = 'text';
        $input->required = $itemEntity->getRequired();
        $input->displayMode = $itemEntity->getDisplayMode();
        $input->placeholder = $itemEntity->getInfoConfig()['placeholder'];
        $input->multiLine = $itemEntity->getPropsConfig()['multiLine'];
        $input->minLength = $itemEntity->getPropsConfig()['minLength'];
        $input->maxLength = $itemEntity->getPropsConfig()['maxLength'];
        $input->predefinedAnswers = $itemEntity->getPropsConfig()['predefinedAnswers'];

        return $input;
    }

    // -- construct a number input -------------------------------------------------------------------------------------

    private function constructNumberInput(FormNode $itemEntity): \stdClass
    {
        $input = new \stdClass();
        $input->type = 'number';
        $input->required = $itemEntity->getRequired();
        $input->displayMode = $itemEntity->getDisplayMode();
        $input->placeholder = $itemEntity->getInfoConfig()['placeholder'];
        $input->min = $itemEntity->getPropsConfig()['min'];
        $input->max = $itemEntity->getPropsConfig()['max'];
        $input->roundingMode = $itemEntity->getPropsConfig()['roundingMode'];
        $input->scale = $itemEntity->getPropsConfig()['scale'];
        $input->stepInterval = $itemEntity->getPropsConfig()['stepInterval'];
        $input->showMaxAsInputAddon = $itemEntity->getPropsConfig()['showMaxAsInputAddon'];
        $input->showMinAsInputAddon = $itemEntity->getPropsConfig()['showMinAsInputAddon'];
        $input->passScore = $itemEntity->getScoringConfig()['passScore'];
        $input->showAutoCalculateBtn = $itemEntity->getScoringConfig()['showAutoCalculateBtn'];
        $input->allowNonAutoCalculatedValue = $itemEntity->getScoringConfig()['allowNonAutoCalculatedValue'];

        return $input;
    }

    // -- construct a upload input -------------------------------------------------------------------------------------

    private function constructUploadInput(FormNode $itemEntity): \stdClass
    {
        $input = new \stdClass();
        $input->type = 'upload';
        $input->required = $itemEntity->getRequired();
        $input->displayMode = $itemEntity->getDisplayMode();
        $input->{UploadInput::KEY_MIN_AMOUNT} = $itemEntity->getPropsConfig()[UploadInput::KEY_MIN_AMOUNT];
        $input->{UploadInput::KEY_MAX_AMOUNT} = $itemEntity->getPropsConfig()[UploadInput::KEY_MAX_AMOUNT];
        $input->{UploadInput::KEY_MAX_FILE_SIZE} = $itemEntity->getPropsConfig()[UploadInput::KEY_MAX_FILE_SIZE];
        $input->{UploadInput::KEY_MIME_TYPES} = $itemEntity->getPropsConfig()[UploadInput::KEY_MIME_TYPES];
        $input->{UploadInput::KEY_EXTENSIONS} = $itemEntity->getPropsConfig()[UploadInput::KEY_EXTENSIONS];

        return $input;
    }

    // -- construct a date input --------------------------------------------------------------------------------------

    private function constructDateInput(FormNode $itemEntity): \stdClass
    {
        $input = new \stdClass();
        $input->type = 'date';
        $input->required = $itemEntity->getRequired();
        $input->displayMode = $itemEntity->getDisplayMode();
        if ('' === $itemEntity->getInfoConfig()['placeholder']) {
            $input->placeholder = [];
        } else {
            $input->placeholder = $itemEntity->getInfoConfig()['placeholder'];
        }
        $input->formatMask = $itemEntity->getPropsConfig()['formatMask'];

        return $input;
    }
}
