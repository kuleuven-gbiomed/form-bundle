<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Manager;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermissionTemplate;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionTemplateRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeOptionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;

class NodeManager
{
    private readonly FormNodeFlowPermissionRepository $flowPermissionRepo;
    private readonly FormNodeFlowPermissionTemplateRepository $flowPermissionTemplateRepo;
    private readonly FormNodeRepository $formNodeRepo;
    private readonly FormNodeOptionRepository $formNodeOptionRepo;

    private array $copiedRootNodePosition = [];

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
        $this->flowPermissionRepo = $this->entityManager->getRepository(FormNodeFlowPermission::class);
        $this->flowPermissionTemplateRepo = $this->entityManager->getRepository(FormNodeFlowPermissionTemplate::class);
        $this->formNodeRepo = $this->entityManager->getRepository(FormNode::class);
        $this->formNodeOptionRepo = $this->entityManager->getRepository(FormNodeOption::class);
    }

    public function insertNode(
        FormNode $node,
        FormNode $newParent,
        int $sequence,
        string $label = '',
        string $language = '',
    ): void {
        /** @var FormNode $child */
        foreach ($newParent->getChildren() as $child) {
            if ($child->getSequence() >= $sequence) {
                $child->setSequence($child->getSequence() + 1);
            }
        }
        $node->setParent($newParent);
        $node->setLevel($newParent->getLevel() + 1);
        $node->setSequence($sequence);

        // Change the name of the node if label and language is provided
        if ('' !== trim($label) && '' !== $language) {
            $info = $node->getInfo();
            $info['label'][$language] = trim($label);
            $node->setInfo($info);
        }

        $this->entityManager->flush();
    }

    public function moveNodeTo(FormNode $nodeToMove, FormNode $newParent, int $sequence): void
    {
        $sourceParent = $nodeToMove->getParent();
        $sourceSequence = $nodeToMove->getSequence();
        if (null === $sourceParent) {
            throw new \Exception("Can't move the root node");
        }

        if ($nodeToMove->isCategory()) {
            // Check if node is not moving into itself or its children
            if (
                self::isChildPosition($nodeToMove->getPosition(), $newParent->getPosition())
                || $nodeToMove->getId() === $newParent->getId()
            ) {
                throw new MovedToSelfException("Can't move categories into itself or its children");
            }
        }

        $this->insertNode($nodeToMove, $newParent, $sequence);

        // restore the sequence at the old parent
        /** @var FormNode $child */
        foreach ($sourceParent->getChildren() as $child) {
            if ($child->getSequence() > $sourceSequence) {
                $child->setSequence($child->getSequence() - 1);
            }
        }
        $this->entityManager->flush();
    }

    public function copyNodeTo(
        FormNode $node,
        FormNode $parent,
        int $sequence,
        string $label,
        string $language,
        int $permissionTemplateId = 0,
    ): void {
        $nodeCopy = clone $node;

        $this->entityManager->persist($nodeCopy);

        $this->insertNode($nodeCopy, $parent, $sequence, $label, $language);

        $this->copiedRootNodePosition = $nodeCopy->getPosition();

        $permissionTemplate = [];
        if (0 !== $permissionTemplateId) {
            $permissionTemplate = $this->flowPermissionTemplateRepo->getById($permissionTemplateId)->getFlowPermission();
        }

        if ($node->isCategory()) {
            $this->entityManager->beginTransaction();
            try {
                $this->recursivelyCopyDependenciesInsideTree($node, $nodeCopy, $permissionTemplate);
                $this->removeDependenciesOutsideTree($nodeCopy);
            } catch (\Exception $e) {
                $this->entityManager->rollback();
                throw $e;
            }
            $this->entityManager->commit();
        } else {
            $this->setOrCopyFlowPermission($node, $nodeCopy, $permissionTemplate);

            // add score question copies as a dependency for existing calculation questions.
            /** @var FormNode[] $calculationNodes */
            $calculationNodes = $this->formNodeRepo->getNodesByCalculationParticipation($node->getForm(), $node->getUid());
            foreach ($calculationNodes as $calculationNode) {
                $this->ifNodeIsCalculationNodeDependencyAlsoAddNodeCopy($calculationNode, $node, $nodeCopy);
            }

            // add as a locked question for unlockerQuestion
            /** @var FormNodeOption[] $unlockerOptions */
            $unlockerOptions = $this->formNodeOptionRepo->getOptionsByUnlocksQuestionIds($node->getUid());
            foreach ($unlockerOptions as $unlockerOption) {
                $unlockerOption->addUnlockId($nodeCopy->getUid());
                $unlockerOption->makeContentHash();
                $unlockerOption->getFormNode()->makeContentHash();
            }

            // add as pre filler
            /** @var FormNode[] $prefilledNodes */
            $prefilledNodes = $this->formNodeRepo->getNodesByPrefilParticipation($node->getForm(), $node->getUid());
            foreach ($prefilledNodes as $prefilledNode) {
                $prefilledNode->addNodeAsPrefillingNode($nodeCopy);
                $prefilledNode->makeContentHash();
            }
        }
        $this->entityManager->flush();
    }

    public function deleteNodeRecursively(FormNode $node, bool $withRelations = false): void
    {
        $parentNode = $node->getParent();
        $sequenceDeletedAt = $node->getSequence();

        $this->removeAllDependenciesOnNode($node);

        // -- delete the connected form node flow permission
        $permissionRepo = $this->entityManager->getRepository(FormNodeFlowPermission::class);
        $permission = $permissionRepo->getByFormNode($node);
        $permissionRepo->delete($permission);

        // -- delete the actual node
        $this->formNodeRepo->delete($node);

        // -- fix the sequencing of all related nodes in the parent (after the deleted node)
        if (null !== $parentNode) {
            foreach ($parentNode->getChildren() as $childNode) {
                if ($childNode->getSequence() >= $sequenceDeletedAt) {
                    $childNode->setSequence($childNode->getSequence() - 1);
                    $childNode->makeContentHash();
                    $this->formNodeRepo->save($childNode);
                }
            }
        }
    }

    private function setOrCopyFlowPermission(FormNode $node, FormNode $nodeCopy, array $permissionTemplate): void
    {
        if ([] === $permissionTemplate) { // copy permissions
            $permissionTemplate = $this->flowPermissionRepo->getByFormNode($node)->getFlowPermission();
        }
        $type = 'question' === $node->getType() ? FormNodeFlowPermission::QUESTION : FormNodeFlowPermission::DEFAULT_CATEGORY;
        $newFormNodeFlowPermission = new FormNodeFlowPermission(type: $type, formNode: $nodeCopy);
        $newFormNodeFlowPermission->importFields($permissionTemplate);
        $newFormNodeFlowPermission->makeContentHash();
        $this->flowPermissionRepo->save($newFormNodeFlowPermission);
    }

    private function recursivelyCopyDependenciesInsideTree(
        FormNode $node,
        FormNode $nodeCopy,
        array $permissionTemplate,
    ): void {
        foreach ($node->getChildren() as $key => $child) {
            $childCopy = $nodeCopy->getChildren()->get($key);
            if (!$childCopy instanceof FormNode) {
                throw new \UnexpectedValueException('$childCopy should be FormNode. So $nodeCopy is not a copy of $node.');
            }
            $this->recursivelyCopyDependenciesInsideTree($child, $childCopy, $permissionTemplate);
        }

        $this->setOrCopyFlowPermission($node, $nodeCopy, $permissionTemplate);

        /**
         * For each copied score question with a calculation question inside the tree,
         * set it as a dependency of the copied calculation.
         * (we remove scores outside the tree later) @see removeDependenciesOutsideTree.
         *
         * @var FormNode[] $calculationNodes
         */
        $calculationNodes = $this->formNodeRepo->getNodesByCalculationParticipation($node->getForm(), $node->getUid());
        foreach ($calculationNodes as $calculationNode) {
            if (self::isChildPosition($this->copiedRootNodePosition, $calculationNode->getPosition())) {
                $this->ifNodeIsCalculationNodeDependencyAlsoAddNodeCopy($calculationNode, $node, $nodeCopy);
            }
        }

        // add as a locked question for unlockerQuestion
        /** @var FormNodeOption[] $unlockerOptions */
        $unlockerOptions = $this->formNodeOptionRepo->getOptionsByUnlocksQuestionIds($node->getUid());
        foreach ($unlockerOptions as $unlockerOption) {
            if (self::isChildPosition($this->copiedRootNodePosition, $unlockerOption->getFormNode()->getPosition())) {
                $unlockerOption->addUnlockId($nodeCopy->getUid());
                $unlockerOption->makeContentHash();
            }
        }

        // add as pre filler
        /** @var FormNode[] $prefilledNodes */
        $prefilledNodes = $this->formNodeRepo->getNodesByPrefilParticipation($node->getForm(), $node->getUid());
        foreach ($prefilledNodes as $prefilledNode) {
            if (self::isChildPosition($this->copiedRootNodePosition, $prefilledNode->getPosition())) {
                $prefilledNode->addNodeAsPrefillingNode($nodeCopy);
                $prefilledNode->makeContentHash();
            }
        }
        $nodeCopy->makeContentHash();
    }

    private function removeDependenciesOutsideTree(FormNode $node): void
    {
        foreach ($node->getChildren() as $child) {
            $this->removeDependenciesOutsideTree($child);
        }

        if ($node->isCalculationQuestion()) {
            $scoringConfig = $node->getScoringConfig();
            $scoringConfig['globalScoreCalculationDependencyConfigs'] = array_values(array_filter(
                $scoringConfig['globalScoreCalculationDependencyConfigs'],
                fn (array $dep) => self::isChildPosition(
                    $this->copiedRootNodePosition,
                    $this->formNodeRepo->getByUidAndFormId($dep['scoreNodeUid'], $node->getForm()->getId())->getPosition()
                )
            ));

            $node->setScoringConfig($scoringConfig);
        }

        /** @var FormNodeOption $option */
        foreach ($node->getOptions() as $option) {
            $option->setUnlockIds(array_values(array_filter(
                $option->getUnlocksQuestionIds(),
                fn (string $uid) => self::isChildPosition(
                    $this->copiedRootNodePosition,
                    $this->formNodeRepo->getByUidAndFormId($uid, $node->getForm()->getId())->getPosition()
                )
            )));
        }

        if ($node->isPrefilledQuestion()) {
            $node->setPrefillingQuestionUids(array_values(array_filter(
                $node->getPrefillingQuestionUids(),
                fn ($uid) => self::isChildPosition(
                    $this->copiedRootNodePosition,
                    $this->formNodeRepo->getByUidAndFormId($uid, $node->getForm()->getId())->getPosition()
                )
            )));
        }
    }

    public function removeAllDependenciesOnNode(FormNode $node): void
    {
        foreach ($node->getChildren() as $child) {
            $this->removeAllDependenciesOnNode($child);
        }

        // if score
        foreach ($this->getCalculationNodesForNode($node) as $calculationNode) {
            $scoringConfig = $calculationNode->getScoringConfig();

            $scoringConfig['globalScoreCalculationDependencyConfigs'] = array_values(array_filter(
                $calculationNode->getScoringConfig()['globalScoreCalculationDependencyConfigs'],
                fn (array $scoreConfig) => $scoreConfig['scoreNodeUid'] !== $node->getUid()
            ));

            $calculationNode->setScoringConfig($scoringConfig);
        }

        // if locked
        foreach ($this->getOptionsUnlockingNode($node) as $option) {
            $option->setUnlockIds(
                array_values(array_filter($option->getUnlocksQuestionIds(), fn ($uid) => $uid !== $node->getUid()))
            );
        }

        // if prefilledBy
        foreach ($this->getNodesPrefilledByNode($node) as $prefillerNode) {
            $prefillerNode->setPrefillingQuestionUids(
                array_values(array_filter($prefillerNode->getPrefillingQuestionUids(), fn ($uid) => $node->getUid() !== $uid))
            );
        }

        // is chartSource
        foreach ($this->getChartNodesUsingNode($node) as $chartNode) {
            $propsConfig = $chartNode->getPropsConfig();
            $propsConfig['question_references'] = array_values(array_filter(
                $chartNode->getPropsConfig()['question_references'],
                fn (array $chartRelation) => $node->getUid() !== $chartRelation['question_uid']
            ));
            $chartNode->setPropsConfig($propsConfig);
        }
    }

    private function ifNodeIsCalculationNodeDependencyAlsoAddNodeCopy(FormNode $calculationNode, FormNode $node, FormNode $nodeCopy): void
    {
        $scoringConfig = $calculationNode->getScoringConfig();
        $scoringList = $scoringConfig['globalScoreCalculationDependencyConfigs'];
        $scoringList[] = [
            'weighing' => array_values(array_filter(
                $scoringList,
                fn (array $item) => $item['scoreNodeUid'] === $node->getUid()
            ))[0]['weighing'],
            'scoreNodeUid' => $nodeCopy->getUid(),
        ];
        $scoringConfig['globalScoreCalculationDependencyConfigs'] = $scoringList;
        $calculationNode->setScoringConfig($scoringConfig);
        $calculationNode->makeContentHash();
    }

    /** @return list<FormNode> */
    public function getNodesUnlockedByNode(FormNode $node): array
    {
        $lockedIds = array_unique(
            array_reduce(
                $node->getOptions()->toArray(),
                fn ($carry, $option) => array_merge($carry, $option->getUnlocksQuestionIds()),
                []
            )
        );

        return $this->formNodeRepo->getQuestionsByFormAndListUid($node->getForm(), $lockedIds);
    }

    /** @return list<FormNode> */
    public function getNodesUnlockingNode(FormNode $node): array
    {
        return $this->formNodeRepo->getNodesByOptionUnlockIds($node->getForm(), $node->getUid());
    }

    /** @return list<FormNodeOption> */
    public function getOptionsUnlockingNode(FormNode $node): array
    {
        return $this->formNodeOptionRepo->getOptionsByUnlocksQuestionIds($node->getUid());
    }

    /** @return list<FormNode> */
    public function getCalculationNodesForNode(FormNode $node): array
    {
        return $this->formNodeRepo->getNodesByCalculationParticipation($node->getForm(), $node->getUid());
    }

    /** @return list<FormNode> */
    public function getScoreNodesForNode(FormNode $node): array
    {
        $scoreIds = array_unique(
            array_map(
                fn (array $scoreQuestion): string => $scoreQuestion['scoreNodeUid'],
                $node->getScoringConfig()['globalScoreCalculationDependencyConfigs'],
            )
        );

        return $this->formNodeRepo->getQuestionsByFormAndListUid($node->getForm(), $scoreIds);
    }

    /** @return list<FormNode> */
    public function getNodesPrefillingNode(FormNode $node): array
    {
        $prefillIds = array_unique($node->getPrefillConfig()['prefillingQuestionUids']);

        return $this->formNodeRepo->getQuestionsByFormAndListUid($node->getForm(), $prefillIds);
    }

    /** @return list<FormNode> */
    public function getNodesPrefilledByNode(FormNode $node): array
    {
        return $this->formNodeRepo->getNodesByPrefilParticipation($node->getForm(), $node->getUid());
    }

    /** @return list<FormNode> */
    public function getChartNodesUsingNode(FormNode $node): array
    {
        return $this->formNodeRepo->getNodesByChartParticipation($node->getForm(), $node->getUid());
    }

    /** @return list<FormNode> */
    public function getSourceNodesForChartNode(FormNode $node): array
    {
        $sourceIds = array_unique(
            array_map(
                fn (array $sourceQuestion): string => $sourceQuestion['question_uid'],
                $node->getPropsConfig()['question_references']
            )
        );

        return $this->formNodeRepo->getQuestionsByFormAndListUid($node->getForm(), $sourceIds);
    }

    /**
     * Check if a child position is contained within a parent position.
     *
     * @param int[] $parentPosition The potential parent position
     * @param int[] $childPosition  The potential child position
     *
     * @return bool True if the child position is contained within the parent position, false otherwise
     *
     * @psalm-pure
     */
    public static function isChildPosition(array $parentPosition, array $childPosition): bool
    {
        if (count($childPosition) <= count($parentPosition)) {
            return false;
        }

        foreach ($parentPosition as $i => $num) {
            if ($num !== $childPosition[$i]) {
                return false;
            }
        }

        return true;
    }
}
