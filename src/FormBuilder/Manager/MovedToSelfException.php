<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Manager;

use KUL\FormBundle\FormBuilder\Commands\Common\Exception\CommandFailedException;

class MovedToSelfException extends \Exception implements CommandFailedException
{
    public function getTitleForUser(): string
    {
        return 'movedToSelf.title';
    }

    public function getMessageForUser(): string
    {
        return 'movedToSelf.message';
    }
}
