<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Manager;

class TemplateExistsException extends \Exception
{
}
