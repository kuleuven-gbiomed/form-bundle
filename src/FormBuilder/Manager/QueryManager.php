<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\FormBuilder\Model\Form as FormModel;
use KUL\FormBundle\FormBuilder\Model\FormNode;
use KUL\FormBundle\FormBuilder\Model\FormNodeRelation;
use KUL\FormBundle\FormBuilder\Queries\GetCategories\GetCategoriesQuery;
use KUL\FormBundle\FormBuilder\Queries\GetChoiceQuestions\GetChoiceQuestionsQuery;
use KUL\FormBundle\FormBuilder\Queries\GetCommandHistory\GetCommandHistoryQuery;
use KUL\FormBundle\FormBuilder\Queries\GetFormByUid\GetFormByUid;
use KUL\FormBundle\FormBuilder\Queries\GetFormList\GetFormListQuery;
use KUL\FormBundle\FormBuilder\Queries\GetFormNode\GetFormNodeQuery;
use KUL\FormBundle\FormBuilder\Queries\GetFormNodeFlowPermission\GetFormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Queries\GetNodeRelations\GetNodeRelations;
use KUL\FormBundle\FormBuilder\Queries\GetOverviewFlowPermissions\GetOverviewFlowPermissions;
use KUL\FormBundle\FormBuilder\Queries\GetOverviewFlowPermissionTemplates\GetOverviewFlowPermissionTemplates;
use KUL\FormBundle\FormBuilder\Queries\GetPrefillSources\GetPrefillSources;
use KUL\FormBundle\FormBuilder\Queries\GetQuestionConditions\GetQuestionConditionsQuery;
use KUL\FormBundle\FormBuilder\Queries\GetQuestionRadarChartParticipants\GetQuestionRadarChartParticipants;
use KUL\FormBundle\FormBuilder\Queries\GetQuestionRadarChartParticipants\RadarChartParticipant;
use KUL\FormBundle\FormBuilder\Queries\GetQuestionRelationsSummary\GetQuestionRelationsSummary;
use KUL\FormBundle\FormBuilder\Queries\GetQuestionsByParent\GetQuestionsByParentQuery;
use KUL\FormBundle\FormBuilder\Queries\GetQuestionsByUidList\GetQuestionsByUidListQuery;
use KUL\FormBundle\FormBuilder\Queries\GetQuestionScores\GetQuestionScoresQuery;
use KUL\FormBundle\FormBuilder\Queries\GetTranslatedCommandHistory\GetTranslatedCommandHistoryQuery;
use KUL\FormBundle\FormBuilder\Queries\GetTranslatedCommandHistory\TranslateHistoryCommandDTO;
use KUL\FormBundle\FormBuilder\Queries\GetWorkflow\GetWorkflowQuery;
use KUL\FormBundle\FormBuilder\Queries\GetWorkflowSchema\GetWorkflowSchemaQuery;
use KUL\FormBundle\FormBuilder\Queries\GetWorkflowStep\GetWorkflowStepQuery;
use KUL\FormBundle\FormBuilder\Utility\FormListFilter;

class QueryManager
{
    // ----------------------------------------------------------------------------------
    // constructor
    // ----------------------------------------------------------------------------------

    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    // ----------------------------------------------------------------------------------
    // queries
    // ----------------------------------------------------------------------------------

    /**
     * @throws EntityNotFoundException
     */
    public function getFormByUid(string $uid): FormModel
    {
        $query = new GetFormByUid($this->entityManager);

        return $query->get($uid);
    }

    public function getFormListSummary(int $formId, ?FormListFilter $filter): \stdClass
    {
        $query = new GetFormListQuery($this->entityManager);

        return $query->summary($formId, $filter);
    }

    public function getFormNode(int $formId, int $itemId): FormNode
    {
        $query = new GetFormNodeQuery($this->entityManager);

        return $query->item($formId, $itemId);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function getChoiceQuestions(int $formId, string $language): array
    {
        $query = new GetChoiceQuestionsQuery($this->entityManager);

        return $query->questions($formId, $language);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function getCategories(int $formId, string $language): array
    {
        $query = new GetCategoriesQuery($this->entityManager);

        return $query->categories($formId, $language);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function getQuestionConditions(int $formId, int $itemId, string $language): array
    {
        $query = new GetQuestionConditionsQuery($this->entityManager);

        return $query->conditions($formId, $itemId, $language);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function getQuestionScores(int $formId, int $itemId, string $language): array
    {
        $query = new GetQuestionScoresQuery($this->entityManager);

        return $query->scores($formId, $itemId, $language);
    }

    /**
     * @return list<RadarChartParticipant>
     */
    public function getQuestionRadarChartParticipants(int $formId, int $itemId): array
    {
        $query = new GetQuestionRadarChartParticipants($this->entityManager);

        return $query->participants($formId, $itemId);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function getPrefillSources(int $formId, int $itemId, string $language): array
    {
        $query = new GetPrefillSources($this->entityManager);

        return $query->sources($formId, $itemId, $language);
    }

    /**
     * @return FormNodeRelation[]
     *
     * @psalm-return list<FormNodeRelation>
     */
    public function getNodeRelations(int $nodeId): array
    {
        $query = new GetNodeRelations($this->entityManager);

        return $query->relations($nodeId);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function getQuestionRelationsSummary(int $formId, int $nodeId, string $language): array
    {
        $query = new GetQuestionRelationsSummary($this->entityManager);

        return $query->relations($formId, $nodeId, $language);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function getQuestionsByUidList(int $formId, string $language, array $uidList): array
    {
        $query = new GetQuestionsByUidListQuery($this->entityManager);

        return $query->questions($formId, $language, $uidList);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function getQuestionsByParent(int $formId, string $language, int $parentId): array
    {
        $query = new GetQuestionsByParentQuery($this->entityManager);

        return $query->questions($formId, $language, $parentId);
    }

    public function getNodeFlowPermission(int $formId, int $itemId): \stdClass
    {
        $query = new GetFormNodeFlowPermission($this->entityManager);

        return $query->nodeFlowPermission($formId, $itemId);
    }

    public function getOverviewFlowPermissions(int $formId): \stdClass
    {
        $query = new GetOverviewFlowPermissions($this->entityManager);

        return $query->flowPermissions($formId);
    }

    /**
     * @return \stdClass[]
     *
     * @psalm-return list<\stdClass>
     */
    public function getFlowPermissionTemplates(int $formId): array
    {
        $query = new GetOverviewFlowPermissionTemplates($this->entityManager);

        return $query->templates($formId);
    }

    // --------------------------------------------------------------------------------
    // -- command history
    // --------------------------------------------------------------------------------
    /**
     * @return list<TranslateHistoryCommandDTO>
     */
    public function getTranslatedCommandHistory(int $formId, string $language): array
    {
        $query = new GetTranslatedCommandHistoryQuery($this->entityManager);

        return $query->history($formId, $language);
    }

    /**
     * @return list<TranslateHistoryCommandDTO>
     */
    public function getTranslatedCommandHistorySinceLastPublish(int $formId, string $language): array
    {
        $query = new GetTranslatedCommandHistoryQuery($this->entityManager);

        return $query->historySinceLastPublish($formId, $language);
    }

    public function countTranslatedCommandHistorySinceLastPublish(int $formId, string $language): int
    {
        $query = new GetTranslatedCommandHistoryQuery($this->entityManager);

        return $query->countHistoryItemsSinceLastPublish($formId, $language);
    }

    public function getCommandHistory(string $commandType, int $subjectId): array
    {
        $query = new GetCommandHistoryQuery($this->entityManager);

        return $query->history($commandType, $subjectId);
    }

    // --------------------------------------------------------------------------------
    // -- workflow queries
    // --------------------------------------------------------------------------------

    // TODO: Remove this as it is not used
    public function getWorkflowDetail(int $formId): \stdClass
    {
        $query = new GetWorkflowQuery();

        return $query->get($formId);
    }

    public function getWorkflowSchema(int $formId, string $language): array
    {
        $query = new GetWorkflowSchemaQuery($this->entityManager);

        return $query->get($formId, $language);
    }

    public function getWorkflowStepDetail(int $formId, int $stepNumber): \stdClass|false
    {
        $query = new GetWorkflowStepQuery($this->entityManager);

        return $query->get($formId, $stepNumber);
    }
}
