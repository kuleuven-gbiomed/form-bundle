<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Manager;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\Domain\Contract\UIdentifiableInterface;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ChildrenAwareInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\InputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\OperableForScoring;
use KUL\FormBundle\Domain\Template\Element\Node\Input\ChoiceInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\DateInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\NumberInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionWithAdditionalTextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\AbstractParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ScorableInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\RadarChartViewNode;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\ScoringParameters;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepCollectionInterface;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormInfo;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermission;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflowStep;
use KUL\FormBundle\FormBuilder\Repository\FormInfoRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeOptionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowStepRepository;

class DeconstructionManager
{
    private readonly FormRepository $formRepo;
    private readonly FormInfoRepository $formInfoRepo;
    private readonly FormWorkflowRepository $formWorkflowRepo;
    private readonly FormWorkflowStepRepository $formWorkflowStepRepo;
    private readonly FormNodeRepository $formNodeRepo;
    private readonly FormNodeOptionRepository $formNodeOptionRepo;
    private readonly FormNodeFlowPermissionRepository $formNodeFlowPermissionRepo;

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Constructor
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    public function __construct(
        private readonly TemplateRepository $templateRepo,
        private readonly EntityManagerInterface $entityManager,
    ) {
        $this->formRepo = $entityManager->getRepository(Form::class);
        $this->formInfoRepo = $entityManager->getRepository(FormInfo::class);
        $this->formWorkflowRepo = $entityManager->getRepository(FormWorkflow::class);
        $this->formWorkflowStepRepo = $entityManager->getRepository(FormWorkflowStep::class);
        $this->formNodeRepo = $entityManager->getRepository(FormNode::class);
        $this->formNodeOptionRepo = $entityManager->getRepository(FormNodeOption::class);
        $this->formNodeFlowPermissionRepo = $entityManager->getRepository(FormNodeFlowPermission::class);
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Deconstruction functions
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    public function updateFormAfterPublish(string $templateId): bool
    {
        $template = $this->templateRepo->getOneById($templateId);
        // ----------------------------------------------------------- update the form
        $form = $this->formRepo->findByUid($templateId);
        if (null === $form) {
            return false;
        }

        $currentVersion = 0;
        if ($template->hasCurrentVersion()) {
            $currentVersion = $template->getCurrentVersionNumber();
        }

        // todo check root formlist -> childeren  (FormList -> is empty...)

        $info = $template->getWorkingVersion()->getInfo();
        $form->importFields(
            $template->getTargetingType(),
            $currentVersion,
            $template->getHierarchicalRolesThatCouldParticipate()->toArray(),
            $template->isActive(),
            $template->isShowPositionNumbers(),
            $template->isReadOnlyChoiceAnswerOptionsTogglingEnabled(),
        );
        $form->makeContentHash();
        $form = $this->formRepo->save($form);

        return true;
    }

    public function importFormInfo(\KUL\FormBundle\Domain\Template\Element\FormInfo $info, FormInfo $formInfo): void
    {
        $formInfo->importFields(
            $info->getLocalizedLabel()->getLocaleStrings(),
            $info->getLocalizedDescription()->getLocaleStrings(),
            $info->getRoleLocalizedDescriptions()->toArray(),
        );
        $formInfo->makeContentHash();
        $this->formInfoRepo->save($formInfo);
    }

    /** @param StepCollectionInterface<array-key, StepInterface> $workFlowSteps */
    public function importFormWorkflow(FormWorkflow $formWorkflow, StepCollectionInterface $workFlowSteps): void
    {
        $formWorkflow->importFields($workFlowSteps::getType());
        $formWorkflow->makeContentHash();
        $formWorkflow = $this->formWorkflowRepo->save($formWorkflow);

        // ------------------------------------------------------------------------------ import the form workflow steps
        $stepIndex = 0;
        $workFlowStepsArray = JsonDecoder::toAssocArray(json_encode($workFlowSteps, \JSON_THROW_ON_ERROR, JsonDecoder::JSON_DEPTH));
        /** @var array $stepArray */
        foreach ($workFlowStepsArray as $stepArray) {
            $formWorkflowStep = $this->formWorkflowStepRepo->findByWorkflowAndSequence($formWorkflow, $stepIndex);
            if (null === $formWorkflowStep) {
                $formWorkflowStep = new FormWorkflowStep(
                    $this->extractStringData(UIdentifiableInterface::KEY_UID, $stepArray),
                    $stepIndex,
                    $formWorkflow,
                    $this->extractStringData(StepInterface::KEY_TYPE, $stepArray),
                    $this->extractArrayData(StepInterface::KEY_PERIOD, $stepArray),
                    $this->extractBooleanData(StepInterface::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED, $stepArray),
                    $this->extractArrayData(StepInterface::KEY_ROLES_WITH_ONE_SUBMIT, $stepArray),
                    $this->extractArrayData(StepInterface::KEY_INFO, $stepArray),
                    $this->extractArrayData(StepInterface::KEY_SUBMIT_SUCCESS_USER_INTERACTION, $stepArray),
                    $this->extractBooleanData(StepInterface::KEY_HIDE_READ_ONLY_NODES_ON_LANDING, $stepArray),
                    $this->extractArrayData(StepInterface::KEY_CUSTOM_OPTIONS, $stepArray),
                );
            }
            $formWorkflowStep->makeContentHash();
            $this->formWorkflowStepRepo->save($formWorkflowStep);
            ++$stepIndex;
        }
    }

    public function importFormList(Template $template, Form $form): void
    {
        $sourceRootNode = $template->getWorkingVersion()->getFormList()->getRootNode();
        $formNode = $this->initFormNode(
            $sourceRootNode->getUid(),
            null,
            0,
            0,
            FormNode::TYPE_CATEGORY,
            FormNode::SUB_TYPE_CATEGORY,
            $form,
        );

        // decode rootNode tree to nested array: json_encode will serialize category with all its nested nodes to JSON
        // based on \JsonSerializable::jsonSerialize and json_decode (with associative = TRUE) will decode to nested array
        // via JsonDecoder::toAssocArray
        $sourceRootNodeArray = JsonDecoder::toAssocArray(json_encode($sourceRootNode, \JSON_THROW_ON_ERROR, JsonDecoder::JSON_DEPTH));

        $formNode->importFields(
            false,
            $this->extractInfo($sourceRootNodeArray),
            $this->extractHidden($sourceRootNodeArray),
            $this->extractStringData(CategoryNode::KEY_DISPLAY_MODE, $sourceRootNodeArray),
            $this->extractArrayData(CategoryNode::KEY_FIRST_HEADER_CELL_TITLE, $sourceRootNodeArray),
        );
        $formNode->makeContentHash();
        $formNode = $this->formNodeRepo->save($formNode);

        // ----------------------------------------------------------------------------------- import the form list item
        $itemSequence = 0;
        /** @var array $srcNodeArray */
        foreach ($this->extractArrayData(CategoryNode::KEY_CHILDREN, $sourceRootNodeArray) as $srcNodeArray) {
            $this->deconstructFormNode($srcNodeArray, $form, $formNode, $itemSequence, 1);
            ++$itemSequence;
        }
    }

    public function deconstructForm(string $templateId, string $username): bool
    {
        $template = $this->templateRepo->getOneById($templateId);

        // ----------------------------------------------------------- import the form
        $form = $this->formRepo->findByUid($templateId);

        if (null === $form) {
            $form = new Form($templateId);
        }

        $form->deconstructedBy($username);
        $info = $template->getWorkingVersion()->getInfo();

        $currentVersion = 1;
        if ($template->hasCurrentVersion()) {
            $currentVersion = $template->getCurrentVersionNumber();
        }

        $form->importFields(
            $template->getTargetingType(),
            $currentVersion,
            $template->getHierarchicalRolesThatCouldParticipate()->toArray(),
            $template->isActive(),
            $template->isShowPositionNumbers(),
            $template->isReadOnlyChoiceAnswerOptionsTogglingEnabled(),
        );

        $form->makeContentHash();
        $form = $this->formRepo->save($form);

        // ---------------------------------------------------------- import the form info
        $formInfo = $this->formInfoRepo->findByForm($form);
        if (null === $formInfo) {
            $formInfo = new FormInfo($templateId, $form);
        }
        $this->importFormInfo($info, $formInfo);

        // ------------------------------------------------------- import the form workflow
        $workFlowSteps = $template->getWorkingVersion()->getWorkFlow()->getSteps();
        $formWorkflow = $this->formWorkflowRepo->findByForm($form);
        if (null === $formWorkflow) {
            $formWorkflow = new FormWorkflow($templateId, $form);
        }
        $this->importFormWorkflow($formWorkflow, $workFlowSteps);

        // ------------------------------------------------------------ import the form list
        $this->importFormList($template, $form);

        return true;
    }

    public function resetForm(Form $form): bool
    {
        $removalManager = new RemovalManager($this->entityManager);
        $template = $this->templateRepo->getOneById($form->getUid());
        $info = $template->getWorkingVersion()->getInfo();

        $form->setEnableReadOnlyChoiceAnswerOptionsToggling($template->isReadOnlyChoiceAnswerOptionsTogglingEnabled());
        $form->setShowPositionNumbers($template->isShowPositionNumbers());
        $form->makeContentHash();

        // ---------------------------------------------------------------------------------------- import the form info
        $formInfo = $this->formInfoRepo->findByForm($form);
        if (null === $formInfo) {
            $formInfo = new FormInfo($form->getUid(), $form);
        }
        $this->importFormInfo($info, $formInfo);

        // ------------------------------------------------------------------------------------ import the form workflow
        $removalManager->removeFormBuilderWorkflow($form->getUid());
        $workFlowSteps = $template->getWorkingVersion()->getWorkFlow()->getSteps();
        $formWorkflow = new FormWorkflow($form->getUid(), $form);
        $this->importFormWorkflow($formWorkflow, $workFlowSteps);

        // ---------------------------------------------------------------------------------------- import the form list

        $removalManager->removeFormBuilderDataOnlyNodes($form->getUid());
        $this->importFormList($template, $form);

        return true;
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // DECONSTRUCT form node
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function deconstructFormNode(
        array $srcNode,
        Form $form,
        ?FormNode $parent,
        int $sequence,
        int $level,
    ): void {
        switch (trim((string) $srcNode[NodeInterface::KEY_TYPE])) {
            case FormNode::TYPE_CATEGORY:
                $this->parseAndSaveCategory($srcNode, $form, $parent, $sequence, $level);
                break;
            case FormNode::SUB_TYPE_CHOICE:
                $this->parseAndSaveChoice($srcNode, $form, $parent, $sequence, $level);
                break;
            case FormNode::SUB_TYPE_NUMBER:
                $this->parseAndSaveNumber($srcNode, $form, $parent, $sequence, $level);
                break;
            case FormNode::SUB_TYPE_TEXT:
                $this->parseAndSaveText($srcNode, $form, $parent, $sequence, $level);
                break;
            case FormNode::SUB_TYPE_RADAR_CHART:
                $this->parseAndSaveRadarChart($srcNode, $form, $parent, $sequence, $level);
                break;
            case FormNode::SUB_TYPE_READ_ONLY_VIEW_BASIC:
                $this->parseAndSaveReadOnlyViewBasic($srcNode, $form, $parent, $sequence, $level);
                break;
            case FormNode::SUB_TYPE_UPLOAD:
                $this->parseAndSaveUpload($srcNode, $form, $parent, $sequence, $level);
                break;
            case FormNode::SUB_TYPE_DATE:
                $this->parseAndSaveDate($srcNode, $form, $parent, $sequence, $level);
                break;
            default:
                break;
        }
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // INIT form node
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function initFormNode(
        string $uid,
        ?FormNode $parent,
        int $sequence,
        int $level,
        string $type,
        string $subType,
        Form $form,
    ): FormNode {
        $formNode = $this->formNodeRepo->findByFormLevelSequenceTypeSubTypeUid($form, $level, $sequence, $type, $subType, $uid);
        if (null === $formNode) {
            $formNode = new FormNode($uid, $parent, $level, $sequence, $type, $subType, $form);
        }

        return $formNode;
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Parse and save CATEGORY
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function parseAndSaveCategory(
        array $srcNode,
        Form $form,
        ?FormNode $parent,
        int $sequence,
        int $level,
    ): void {
        $formNode = $this->initFormNode(
            $this->extractStringData(UIdentifiableInterface::KEY_UID, $srcNode),
            $parent,
            $sequence,
            $level,
            $this->extractStringData(NodeInterface::KEY_TYPE, $srcNode),
            $this->extractStringData(NodeInterface::KEY_TYPE, $srcNode),
            $form
        );
        $formNode->importFields(
            false,
            $this->extractInfo($srcNode),
            $this->extractHidden($srcNode),
            $this->extractStringData(CategoryNode::KEY_DISPLAY_MODE, $srcNode),
            $this->extractArrayData(CategoryNode::KEY_FIRST_HEADER_CELL_TITLE, $srcNode)
        );
        $formNode->makeContentHash();
        $formNode = $this->formNodeRepo->save($formNode);

        // -------------------------------------------------------------------------- save the connected flow permission
        $this->parseAndSaveFlowPermission(
            $this->extractArrayData(CategoryNode::KEY_DEFAULT_FLOW_PERMISSION, $srcNode),
            FormNodeFlowPermission::DEFAULT_CATEGORY,
            $formNode,
            null
        );

        // ----------------------------------------------------------------------------------------- some new parameters
        $newParent = $formNode;
        $newSequence = 0;
        $newLevel = $level + 1;
        // -------------------------------------------------------------------- loop over the children for this category
        foreach ($srcNode[ChildrenAwareInterface::KEY_CHILDREN] as $srcChildNodeArray) {
            $this->deconstructFormNode($srcChildNodeArray, $form, $newParent, $newSequence, $newLevel);
            ++$newSequence;
        }
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Parse and save CHOICE NODE
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function parseAndSaveChoice(
        array $srcNode,
        Form $form,
        ?FormNode $parent,
        int $sequence,
        int $level,
    ): void {
        $formNode = $this->initFormNode(
            $this->extractStringData(UIdentifiableInterface::KEY_UID, $srcNode),
            $parent,
            $sequence,
            $level,
            FormNode::TYPE_QUESTION,
            $this->extractStringData(NodeInterface::KEY_TYPE, $srcNode),
            $form
        );

        $inputArray = $this->extractArrayData(InputNode::KEY_INPUT, $srcNode);

        // ------------------------------------------------------------------------------ compile to four config classes
        $infoConfig = [
            InputNode::KEY_DESCRIPTION_DISPLAY_MODE => $this->extractStringData(InputNode::KEY_DESCRIPTION_DISPLAY_MODE, $srcNode),
            ChoiceInput::KEY_PLACEHOLDER => $this->extractArrayData(ChoiceInput::KEY_PLACEHOLDER, $inputArray),
        ];

        $propsConfig = [
            ChoiceInput::KEY_MULTIPLE => $this->extractBooleanData(ChoiceInput::KEY_MULTIPLE, $inputArray),
            ChoiceInput::KEY_EXPANDED => $this->extractBooleanData(ChoiceInput::KEY_EXPANDED, $inputArray),
            ChoiceInput::KEY_MIN_SELECT => $this->extractOptionalIntegerData(ChoiceInput::KEY_MIN_SELECT, $inputArray),
            ChoiceInput::KEY_MAX_SELECT => $this->extractOptionalIntegerData(ChoiceInput::KEY_MAX_SELECT, $inputArray),
            ChoiceInput::KEY_SORT_OPTIONS_ALPHABETICALLY => $this->extractBooleanData(ChoiceInput::KEY_SORT_OPTIONS_ALPHABETICALLY, $inputArray),
            ScorableInputNode::KEY_CUSTOM_OPTIONS => $this->extractArrayData(ScorableInputNode::KEY_CUSTOM_OPTIONS, $srcNode),
        ];

        $scoringConfig = [
            OperableForScoring::KEY_GLOBAL_SCORE_CALCULATION_DEPENDENCY_CONFIGS => $this->extractArrayData(OperableForScoring::KEY_GLOBAL_SCORE_CALCULATION_DEPENDENCY_CONFIGS, $srcNode),
            OperableForScoring::KEY_GLOBAL_SCORE_WEIGHING_TYPE => $this->extractStringData(OperableForScoring::KEY_GLOBAL_SCORE_WEIGHING_TYPE, $srcNode),
            ChoiceInputNode::KEY_ROLES_ALLOWED_TO_SEE_OPTION_SCORE_VALUES => $this->extractArrayData(ChoiceInputNode::KEY_ROLES_ALLOWED_TO_SEE_OPTION_SCORE_VALUES, $srcNode),
            ChoiceInput::KEY_SCORING_PARAMETERS => $this->extractArrayData(ChoiceInput::KEY_SCORING_PARAMETERS, $inputArray),
        ];

        $prefillConfig = [ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS => $this->extractArrayData(ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS, $srcNode)];

        $multiUnlockConfig = [AbstractParentalInputNode::KEY_MULTI_UNLOCKING_QUESTION_UIDS => $this->extractArrayData(AbstractParentalInputNode::KEY_MULTI_UNLOCKING_QUESTION_UIDS, $srcNode)];

        $formNode->importFields(
            $this->extractBooleanData(InputInterface::KEY_REQUIRED, $inputArray),
            $this->extractInfo($srcNode),
            $this->extractHidden($srcNode),
            $this->extractStringData(InputInterface::KEY_DISPLAY_MODE, $inputArray),
            [],
            $infoConfig,
            $propsConfig,
            $scoringConfig,
            $prefillConfig,
            $multiUnlockConfig,
        );

        $formNode->makeContentHash();
        $formNode = $this->formNodeRepo->save($formNode);

        // -------------------------------------------------------------------------- save the connected flow permission
        $this->parseAndSaveFlowPermission(
            $this->extractArrayData(InputNode::KEY_FLOW_PERMISSION, $srcNode),
            FormNodeFlowPermission::QUESTION,
            $formNode,
            null
        );

        // ------------------------------------------------------------------ parse the options for this question_choice
        $optionSequence = 0;
        /** @var array $srcOption */
        foreach ($this->extractArrayData(ChildrenAwareInterface::KEY_CHILDREN, $srcNode) as $srcOption) {
            $this->parseAndSaveOption($srcOption, $formNode, $optionSequence);
            ++$optionSequence;
        }
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Parse and save OPTION
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function parseAndSaveOption(array $srcOption, FormNode $formNode, int $sequence): void
    {
        $formNodeOption = $this->formNodeOptionRepo->findByFormNodeAndSequence($formNode, $sequence);
        if (null === $formNodeOption) {
            $formNodeOption = new FormNodeOption($srcOption[UIdentifiableInterface::KEY_UID], $sequence, $srcOption[NodeInterface::KEY_TYPE], $formNode);
        }

        // ------------------------------------------------------------- compile information for storing in the database
        $unlocksQuestionIds = [];
        if (isset($srcOption[OptionInputNode::KEY_UNLOCKS_QUESTION_IDS])) {
            $unlocksQuestionIds = $this->extractArrayData(OptionInputNode::KEY_UNLOCKS_QUESTION_IDS, $srcOption);
        }
        $multiUnlocksQuestionIds = [];
        if (isset($srcOption[OptionInputNode::KEY_MULTI_UNLOCKS_QUESTION_IDS])) {
            $multiUnlocksQuestionIds = $this->extractArrayData(OptionInputNode::KEY_MULTI_UNLOCKS_QUESTION_IDS, $srcOption);
        }
        $inputArray = $this->extractArrayData(InputNode::KEY_INPUT, $srcOption);
        $inputType = $this->extractStringData(InputInterface::KEY_TYPE, $inputArray);
        $propsConfig = ['type' => $inputType];
        if (FormNodeOption::INPUT_TYPE_OPTION_WITH_ADDITIONAL_TEXT === $inputArray[InputInterface::KEY_TYPE]) {
            $propsConfig[OptionWithAdditionalTextInput::KEY_ADDITIONAL_TEXT_REQUIRED] = $this->extractBooleanData(OptionWithAdditionalTextInput::KEY_ADDITIONAL_TEXT_REQUIRED, $inputArray);
            $propsConfig[OptionWithAdditionalTextInput::KEY_ADDITIONAL_TEXT_MIN_LENGTH] = $this->extractIntegerData(OptionWithAdditionalTextInput::KEY_ADDITIONAL_TEXT_MIN_LENGTH, $inputArray);
            $propsConfig[OptionWithAdditionalTextInput::KEY_ADDITIONAL_TEXT_MAX_LENGTH] = $this->extractIntegerData(OptionWithAdditionalTextInput::KEY_ADDITIONAL_TEXT_MAX_LENGTH, $inputArray);
        }

        $formNodeOption->importFields(
            $this->extractBooleanData(InputInterface::KEY_REQUIRED, $inputArray),
            $this->extractStringData(InputInterface::KEY_DISPLAY_MODE, $inputArray),
            $this->extractBooleanData(OptionInput::KEY_DEFAULT_SELECTED, $inputArray),
            $this->extractInfo($srcOption),
            $this->extractBooleanData(OptionInput::KEY_HIDE_LABEL_IN_RELEVANT_CHOICE_DISPLAY_MODE, $inputArray),
            $this->extractHidden($srcOption),
            $this->extractStringData(InputNode::KEY_DESCRIPTION_DISPLAY_MODE, $srcOption),
            $unlocksQuestionIds,
            $multiUnlocksQuestionIds,
            $inputType,
            $propsConfig,
            $this->extractOptionalNumericData(OptionInput::KEY_SCORE, $inputArray),
        );

        $formNodeOption->makeContentHash();
        $this->formNodeOptionRepo->save($formNodeOption);

        // -------------------------------------------------------------------------- save the connected flow permission
        //        $this->parseAndSaveFlowPermission(
        //            $this->extractArrayData(InputNode::KEY_FLOW_PERMISSION, $srcOption),
        //            FormNodeFlowPermission::OPTION,
        //            null,
        //            $formNodeOption
        //        );
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Parse and save NUMBER
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function parseAndSaveNumber(
        array $srcNode,
        Form $form,
        ?FormNode $parent,
        int $sequence,
        int $level,
    ): void {
        $formNode = $this->initFormNode(
            $this->extractStringData(UIdentifiableInterface::KEY_UID, $srcNode),
            $parent,
            $sequence,
            $level,
            FormNode::TYPE_QUESTION,
            $this->extractStringData(NodeInterface::KEY_TYPE, $srcNode),
            $form
        );
        $inputArray = $srcNode[InputNode::KEY_INPUT];

        // ------------------------------------------------------------------------------ compile to four config classes
        $infoConfig = [
            InputNode::KEY_DESCRIPTION_DISPLAY_MODE => $this->extractStringData(InputNode::KEY_DESCRIPTION_DISPLAY_MODE, $srcNode),
            NumberInput::KEY_PLACEHOLDER => $this->extractArrayData(NumberInput::KEY_PLACEHOLDER, $inputArray),
        ];

        $propsConfig = [
            NumberInput::KEY_MIN => $this->extractNumericData(NumberInput::KEY_MIN, $inputArray),
            NumberInput::KEY_MAX => $this->extractNumericData(NumberInput::KEY_MAX, $inputArray),
            NumberInput::KEY_ROUNDING_MODE => $this->extractIntegerData(NumberInput::KEY_ROUNDING_MODE, $inputArray),
            NumberInput::KEY_SCALE => $this->extractIntegerData(NumberInput::KEY_SCALE, $inputArray),
            NumberInput::KEY_STEP_INTERVAL => $this->extractNumericData(NumberInput::KEY_STEP_INTERVAL, $inputArray),
            NumberInput::KEY_SHOW_MAX_AS_INPUT_ADDON => $this->extractBooleanData(NumberInput::KEY_SHOW_MAX_AS_INPUT_ADDON, $inputArray),
            NumberInput::KEY_SHOW_MIN_AS_INPUT_ADDON => $this->extractBooleanData(NumberInput::KEY_SHOW_MIN_AS_INPUT_ADDON, $inputArray),
            ScorableInputNode::KEY_CUSTOM_OPTIONS => $this->extractArrayData(ScorableInputNode::KEY_CUSTOM_OPTIONS, $srcNode),
        ];

        $scoringConfig = [
            OperableForScoring::KEY_GLOBAL_SCORE_CALCULATION_DEPENDENCY_CONFIGS => $this->extractArrayData(OperableForScoring::KEY_GLOBAL_SCORE_CALCULATION_DEPENDENCY_CONFIGS, $srcNode),
            OperableForScoring::KEY_GLOBAL_SCORE_WEIGHING_TYPE => $this->extractStringData(OperableForScoring::KEY_GLOBAL_SCORE_WEIGHING_TYPE, $srcNode),
            ScoringParameters::KEY_PASS_SCORE => $this->extractOptionalNumericData(ScoringParameters::KEY_PASS_SCORE, $inputArray),
            ScoringParameters::KEY_SHOW_AUTO_CALCULATE_BTN => $this->extractBooleanData(ScoringParameters::KEY_SHOW_AUTO_CALCULATE_BTN, $inputArray),
            ScoringParameters::KEY_ALLOW_NON_AUTO_CALCULATED_VALUE => $this->extractBooleanData(ScoringParameters::KEY_ALLOW_NON_AUTO_CALCULATED_VALUE, $inputArray),
        ];

        $prefillConfig = [];
        $prefillConfig[ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS] = [];

        $multiUnlockConfig = [AbstractParentalInputNode::KEY_MULTI_UNLOCKING_QUESTION_UIDS => $this->extractArrayData(AbstractParentalInputNode::KEY_MULTI_UNLOCKING_QUESTION_UIDS, $srcNode)];

        $formNode->importFields(
            $this->extractBooleanData(InputInterface::KEY_REQUIRED, $inputArray),
            $this->extractInfo($srcNode),
            $this->extractHidden($srcNode),
            $this->extractStringData(InputInterface::KEY_DISPLAY_MODE, $inputArray),
            [],
            $infoConfig,
            $propsConfig,
            $scoringConfig,
            $prefillConfig,
            $multiUnlockConfig,
        );

        $formNode->makeContentHash();
        $formNode = $this->formNodeRepo->save($formNode);

        // -------------------------------------------------------------------------- save the connected flow permission
        $this->parseAndSaveFlowPermission(
            $this->extractArrayData(InputNode::KEY_FLOW_PERMISSION, $srcNode),
            FormNodeFlowPermission::QUESTION,
            $formNode,
            null
        );
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Parse and save TEXT NODE
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function parseAndSaveText(
        array $srcNode,
        Form $form,
        ?FormNode $parent,
        int $sequence,
        int $level,
    ): void {
        $formNode = $this->initFormNode(
            $this->extractStringData(UIdentifiableInterface::KEY_UID, $srcNode),
            $parent,
            $sequence,
            $level,
            FormNode::TYPE_QUESTION,
            $this->extractStringData(NodeInterface::KEY_TYPE, $srcNode),
            $form
        );
        $inputArray = $srcNode[InputNode::KEY_INPUT];

        // ------------------------------------------------------------------------------ compile to four config classes
        $infoConfig = [
            InputNode::KEY_DESCRIPTION_DISPLAY_MODE => $this->extractStringData(InputNode::KEY_DESCRIPTION_DISPLAY_MODE, $srcNode),
            TextInput::KEY_PLACEHOLDER => $this->extractArrayData(TextInput::KEY_PLACEHOLDER, $inputArray),
        ];

        $propsConfig = [
            TextInput::KEY_MULTI_LINE => $this->extractBooleanData(TextInput::KEY_MULTI_LINE, $inputArray),
            TextInput::KEY_MIN_LENGTH => $this->extractIntegerData(TextInput::KEY_MIN_LENGTH, $inputArray),
            TextInput::KEY_MAX_LENGTH => $this->extractIntegerData(TextInput::KEY_MAX_LENGTH, $inputArray),
            TextInput::KEY_PREDEFINED_ANSWERS => $this->extractArrayData(TextInput::KEY_PREDEFINED_ANSWERS, $inputArray),
        ];

        $scoringConfig = [];

        if (array_key_exists('prefillWithIds', $srcNode)) {
            $prefillConfig = [ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS => $this->extractArrayData('prefillWithIds', $srcNode)];
        } else {
            $prefillConfig = [];
            $prefillConfig[ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS] = [];
        }

        $multiUnlockConfig = [AbstractParentalInputNode::KEY_MULTI_UNLOCKING_QUESTION_UIDS => $this->extractArrayData(AbstractParentalInputNode::KEY_MULTI_UNLOCKING_QUESTION_UIDS, $srcNode)];

        $formNode->importFields(
            $this->extractBooleanData(InputInterface::KEY_REQUIRED, $inputArray),
            $this->extractInfo($srcNode),
            $this->extractHidden($srcNode),
            $this->extractStringData(InputInterface::KEY_DISPLAY_MODE, $inputArray),
            [],
            $infoConfig,
            $propsConfig,
            $scoringConfig,
            $prefillConfig,
            $multiUnlockConfig,
        );

        $formNode->makeContentHash();
        $formNode = $this->formNodeRepo->save($formNode);

        // -------------------------------------------------------------------------- save the connected flow permission
        $this->parseAndSaveFlowPermission(
            $this->extractArrayData(InputNode::KEY_FLOW_PERMISSION, $srcNode),
            FormNodeFlowPermission::QUESTION,
            $formNode,
            null
        );
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Parse and save RADAR CHART NODE
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function parseAndSaveRadarChart(
        array $srcNode,
        Form $form,
        ?FormNode $parent,
        int $sequence,
        int $level,
    ): void {
        $formNode = $this->initFormNode(
            $this->extractStringData(UIdentifiableInterface::KEY_UID, $srcNode),
            $parent,
            $sequence,
            $level,
            FormNode::TYPE_QUESTION,
            $this->extractStringData(NodeInterface::KEY_TYPE, $srcNode),
            $form
        );

        // ------------------------------------------------------------------------------ compile to four config classes
        $scoringConfig = $prefillConfig = [];

        $infoConfig = [
            InputNode::KEY_DESCRIPTION_DISPLAY_MODE => 'box',
            TextInput::KEY_PLACEHOLDER => '',
        ];

        $unitSign = null;
        if (!(null === $srcNode[RadarChartViewNode::KEY_UNIT_SIGN])) {
            $unitSign = $this->extractStringData(RadarChartViewNode::KEY_UNIT_SIGN, $srcNode);
        }

        $propsConfig = [
            RadarChartViewNode::KEY_QUESTION_REFERENCES => $this->extractArrayData(RadarChartViewNode::KEY_QUESTION_REFERENCES, $srcNode),
            RadarChartViewNode::KEY_IGNORE_INDIVIDUAL_QUESTION_RIGHTS => $this->extractBooleanData(RadarChartViewNode::KEY_IGNORE_INDIVIDUAL_QUESTION_RIGHTS, $srcNode),
            RadarChartViewNode::KEY_HIDE_POINT_VALUES => $this->extractBooleanData(RadarChartViewNode::KEY_HIDE_POINT_VALUES, $srcNode),
            RadarChartViewNode::KEY_HIDE_POINT_UNIT_SUFFIX => $this->extractBooleanData(RadarChartViewNode::KEY_HIDE_POINT_UNIT_SUFFIX, $srcNode),
            RadarChartViewNode::KEY_HIDE_SCALE_VALUES => $this->extractBooleanData(RadarChartViewNode::KEY_HIDE_SCALE_VALUES, $srcNode),
            RadarChartViewNode::KEY_HIDE_SCALE_UNIT_SUFFIX => $this->extractBooleanData(RadarChartViewNode::KEY_HIDE_SCALE_UNIT_SUFFIX, $srcNode),
            RadarChartViewNode::KEY_UNIT_SIGN => $unitSign,
        ];

        $multiUnlockConfig = [AbstractParentalInputNode::KEY_MULTI_UNLOCKING_QUESTION_UIDS => $this->extractArrayData(AbstractParentalInputNode::KEY_MULTI_UNLOCKING_QUESTION_UIDS, $srcNode)];

        $formNode->importFields(
            false,
            $this->extractInfo($srcNode),
            $this->extractHidden($srcNode),
            $this->extractStringData(InputInterface::KEY_DISPLAY_MODE, $srcNode),
            [],
            $infoConfig,
            $propsConfig,
            $scoringConfig,
            $prefillConfig,
            $multiUnlockConfig,
        );

        $formNode->makeContentHash();
        $formNode = $this->formNodeRepo->save($formNode);

        // -------------------------------------------------------------------------- save the connected flow permission
        $this->parseAndSaveFlowPermission(
            $this->extractArrayData(InputNode::KEY_FLOW_PERMISSION, $srcNode),
            FormNodeFlowPermission::QUESTION,
            $formNode,
            null
        );
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Parse and save READ ONLY VIEW BASIC
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function parseAndSaveReadOnlyViewBasic(
        array $srcNode,
        Form $form,
        ?FormNode $parent,
        int $sequence,
        int $level,
    ): void {
        $formNode = $this->initFormNode(
            $this->extractStringData(UIdentifiableInterface::KEY_UID, $srcNode),
            $parent,
            $sequence,
            $level,
            FormNode::TYPE_QUESTION,
            $this->extractStringData(NodeInterface::KEY_TYPE, $srcNode),
            $form
        );

        $formNode->importFields(
            required: false,
            info: $this->extractInfo($srcNode),
            hidden: $this->extractHidden($srcNode),
            displayMode: $this->extractStringData(InputInterface::KEY_DISPLAY_MODE, $srcNode),
            infoConfig: [
                InputNode::KEY_DESCRIPTION_DISPLAY_MODE => 'box',
                TextInput::KEY_PLACEHOLDER => '',
            ],
        );

        $formNode->makeContentHash();
        $formNode = $this->formNodeRepo->save($formNode);

        // -------------------------------------------------------------------------- save the connected flow permission
        $this->parseAndSaveFlowPermission(
            $this->extractArrayData(InputNode::KEY_FLOW_PERMISSION, $srcNode),
            FormNodeFlowPermission::QUESTION,
            $formNode,
            null
        );
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Parse and save DATE NODE
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function parseAndSaveDate(
        array $srcNode,
        Form $form,
        ?FormNode $parent,
        int $sequence,
        int $level,
    ): void {
        $formNode = $this->initFormNode(
            $this->extractStringData(UIdentifiableInterface::KEY_UID, $srcNode),
            $parent,
            $sequence,
            $level,
            FormNode::TYPE_QUESTION,
            $this->extractStringData(NodeInterface::KEY_TYPE, $srcNode),
            $form
        );

        $inputArray = $srcNode[InputNode::KEY_INPUT];
        $scoringConfig = [];
        $prefillConfig = [];
        $prefillConfig[ChoiceInputNode::KEY_PREFILLING_QUESTION_UIDS] = [];

        $propsConfig = [
            DateInput::KEY_FORMATMASK => $this->extractStringData(DateInput::KEY_FORMATMASK, $inputArray),
        ];

        $infoConfig = [
            InputNode::KEY_DESCRIPTION_DISPLAY_MODE => 'box',
            TextInput::KEY_PLACEHOLDER => '',
        ];

        $formNode->importFields(
            $this->extractBooleanData(InputInterface::KEY_REQUIRED, $inputArray),
            $this->extractInfo($srcNode),
            $this->extractHidden($srcNode),
            $this->extractStringData(InputInterface::KEY_DISPLAY_MODE, $inputArray),
            [],
            $infoConfig,
            $propsConfig,
            $scoringConfig,
            $prefillConfig
        );

        $formNode->makeContentHash();
        $formNode = $this->formNodeRepo->save($formNode);

        // -------------------------------------------------------------------------- save the connected flow permission
        $this->parseAndSaveFlowPermission(
            $this->extractArrayData(InputNode::KEY_FLOW_PERMISSION, $srcNode),
            FormNodeFlowPermission::QUESTION,
            $formNode,
            null
        );
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Parse and save UPLOAD NODE
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function parseAndSaveUpload(
        array $srcNode,
        Form $form,
        ?FormNode $parent,
        int $sequence,
        int $level,
    ): void {
        $formNode = $this->initFormNode(
            $this->extractStringData(UIdentifiableInterface::KEY_UID, $srcNode),
            $parent,
            $sequence,
            $level,
            FormNode::TYPE_QUESTION,
            $this->extractStringData(NodeInterface::KEY_TYPE, $srcNode),
            $form
        );
        $inputArray = $srcNode[InputNode::KEY_INPUT];

        // ------------------------------------------------------------------------------ compile to four config classes
        $scoringConfig = $prefillConfig = [];

        $infoConfig = [
            InputNode::KEY_DESCRIPTION_DISPLAY_MODE => $this->extractStringData(InputNode::KEY_DESCRIPTION_DISPLAY_MODE, $srcNode),
            TextInput::KEY_PLACEHOLDER => '',
        ];

        $propsConfig = [
            UploadInput::KEY_MIN_AMOUNT => $this->extractNumericData(UploadInput::KEY_MIN_AMOUNT, $inputArray),
            UploadInput::KEY_MAX_AMOUNT => $this->extractNumericData(UploadInput::KEY_MAX_AMOUNT, $inputArray),
            UploadInput::KEY_MAX_FILE_SIZE => $this->extractNumericData(UploadInput::KEY_MAX_FILE_SIZE, $inputArray),
            UploadInput::KEY_MIME_TYPES => $this->extractArrayData(UploadInput::KEY_MIME_TYPES, $inputArray),
            UploadInput::KEY_EXTENSIONS => $this->extractArrayData(UploadInput::KEY_EXTENSIONS, $inputArray),
        ];

        $formNode->importFields(
            false,
            $this->extractInfo($srcNode),
            $this->extractHidden($srcNode),
            $this->extractStringData(InputInterface::KEY_DISPLAY_MODE, $inputArray),
            [],
            $infoConfig,
            $propsConfig,
            $scoringConfig,
            $prefillConfig
        );

        $formNode->makeContentHash();
        $formNode = $this->formNodeRepo->save($formNode);

        // -------------------------------------------------------------------------- save the connected flow permission
        $this->parseAndSaveFlowPermission(
            $this->extractArrayData(InputNode::KEY_FLOW_PERMISSION, $srcNode),
            FormNodeFlowPermission::QUESTION,
            $formNode,
            null
        );
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // Parse and save FLOW PERMISSION
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function parseAndSaveFlowPermission(
        array $srcFlowPermission,
        string $type,
        ?FormNode $formNode,
        ?FormNodeOption $formNodeOption,
    ): void {
        $formNodeFlowPermission = match ($type) {
            FormNodeFlowPermission::OPTION => $formNodeOption instanceof FormNodeOption ? $this->formNodeFlowPermissionRepo->findByFormNodeOption($formNodeOption) : null,
            default => $formNode instanceof FormNode ? $this->formNodeFlowPermissionRepo->findByFormNode($formNode) : null,
        };
        if (null === $formNodeFlowPermission) {
            $formNodeFlowPermission = new FormNodeFlowPermission($type, $formNode, $formNodeOption);
        }
        $formNodeFlowPermission->importFields($srcFlowPermission);
        $formNodeFlowPermission->makeContentHash();
        $this->formNodeFlowPermissionRepo->save($formNodeFlowPermission);
    }

    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    // EXTRACTION FUNCTIONS
    // —————————————————————————————————————————————————————————————————————————————————————————————————————————————————

    private function extractInfo(array $sourceNode): array
    {
        return $this->extractArrayData(NodeInterface::KEY_INFO, $sourceNode);
    }

    private function extractHidden(array $sourceNode): bool
    {
        return $this->extractBooleanData(NodeInterface::KEY_HIDDEN, $sourceNode);
    }

    private function extractArrayData(string $key, array $source): array
    {
        $data = $source[$key];
        if (!is_array($data)) {
            throw new \Exception('data found for key <'.$key.'> is not of type array');
        }

        return $data;
    }

    private function extractStringData(string $key, array $source): string
    {
        $data = $source[$key];
        if (!is_string($data)) {
            throw new \Exception('data found for key <'.$key.'> is not of type string');
        }

        return $data;
    }

    private function extractBooleanData(string $key, array $source): bool
    {
        $data = $source[$key];
        if (!is_bool($data)) {
            throw new \Exception('data found for key <'.$key.'> is not of type boolean');
        }

        return $data;
    }

    private function extractIntegerData(string $key, array $source): int
    {
        $data = $source[$key];
        if (!is_int($data)) {
            throw new \Exception('data found for key <'.$key.'> is not of type integer');
        }

        return $data;
    }

    private function extractOptionalIntegerData(string $key, array $source): ?int
    {
        $data = $source[$key];
        if (!is_int($data) && !(null === $data)) {
            throw new \Exception('data found for key <'.$key.'> is not of type integer or NULL');
        }

        return $data;
    }

    private function extractNumericData(string $key, array $source): float|int
    {
        $data = $source[$key];
        // a number ending on .0 (e.g 5.0), could have been stored/deserialized as integer (e.g 5 instead of 5.0)
        // (presumably by (JSON) encoding / decoding-to-array, but I'm not sure). but mathematically speaking, an
        // integer is also a float and here, we don't mind if it's a float or int
        // on a side note, in PHP, when you pass an integer (e.g. 5) in to a method parameter typed as float,
        // it will be immediately cast by that method to a float (e.g. 5.0).

        if (!is_int($data) && !is_float($data)) {
            throw new \Exception('data found for key <'.$key.'> is not numeric (float or integer)');
        }

        return $data;
    }

    private function extractOptionalNumericData(string $key, array $source): float|int|null
    {
        $data = $source[$key];
        // a number ending on .0 (e.g 5.0), could have been stored/deserialized as integer (e.g 5 instead of 5.0)
        // (presumably by (JSON) encoding / decoding-to-array, but I'm not sure). but mathematically speaking, an
        // integer is also a float and here, we don't mind if it's a float or int
        // on a side note, in PHP, when you pass an integer (e.g. 5) in to a method parameter typed as float,
        // it will be immediately cast by that method to a float (e.g. 5.0).

        if (!is_int($data) && !is_float($data) && !(null === $data)) {
            throw new \Exception('data found for key <'.$key.'> is not numeric (float or integer) or NULL');
        }

        return $data;
    }
}
