<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Exception\InputInvalidArgumentException;
use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\MissingVersionException;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Exception\VersionException;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\ImportForm\ImportFormCommand;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Event\FormEditModeChanged;
use KUL\FormBundle\FormBuilder\Model\FormTemplate;
use KUL\FormBundle\FormBuilder\Model\FormTemplateStatus;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use KUL\FormBundle\FormBuilder\Utility\FormBuilderUser;
use KUL\FormBundle\Service\SubmittedTemplateService;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class TemplateManager
{
    // --------------------------------------------------------------------------------
    // Constructor
    // --------------------------------------------------------------------------------

    public function __construct(
        private TemplateRepository $templateRepository,
        private EntityManagerInterface $entityManager,
        private MessageBusInterface $formBundleEventBus,
        private ?SubmittedTemplateService $submittedTemplateService = null,
    ) {
    }

    // --------------------------------------------------------------------------------
    // Functions
    // --------------------------------------------------------------------------------

    public function listTemplates(): array
    {
        $templates = [];

        /** @var Template[] $templates */
        $sourceTemplates = $this->templateRepository->findAll();

        /** @var FormRepository formRepo */
        $formRepo = $this->entityManager->getRepository(Form::class);

        foreach ($sourceTemplates as $sourceTemplate) {
            if ($sourceTemplate->hasCurrentVersion()) {
                $form = $formRepo->findByUid($sourceTemplate->getId());
                if (null === $form) {
                    $formTemplate = FormTemplate::make(
                        0,
                        $sourceTemplate->getId(),
                        $sourceTemplate->getCurrentVersion()->getInfo()->getLocalizedLabel()->getFallback(),
                        $sourceTemplate->getCurrentVersionNumber(),
                        $sourceTemplate->isActive(),
                        FormTemplateStatus::ONLINE,
                        null,
                        null
                    );
                } else {
                    $formTemplate = FormTemplate::make(
                        $form->getId(),
                        $sourceTemplate->getId(),
                        $sourceTemplate->getCurrentVersion()->getInfo()->getLocalizedLabel()->getFallback(),
                        $sourceTemplate->getCurrentVersionNumber(),
                        $sourceTemplate->isActive(),
                        FormTemplateStatus::EDITING,
                        $form->getCreatedAt(),
                        $form->getUser()
                    );
                }
                $templates[] = $formTemplate;
            }
        }

        return $templates;
    }

    public function isEditing(string $templateId): bool
    {
        /** @psalm-var FormRepository $formRepo */
        $formRepo = $this->entityManager->getRepository(Form::class);
        $form = $formRepo->findByUid($templateId);

        return $form instanceof Form;
    }

    public function startEditing(string $templateId, FormBuilderUser $user): bool
    {
        $result = false;

        // -- deconstruct the working version
        /** @psalm-var FormRepository $formRepo */
        $formRepo = $this->entityManager->getRepository(Form::class);
        $form = $formRepo->findByUid($templateId);
        if (null === $form) {
            $this->entityManager->beginTransaction();
            try {
                $deconstructionManager = new DeconstructionManager($this->templateRepository, $this->entityManager);
                $result = $deconstructionManager->deconstructForm($templateId, $user->getIdentifier());
                $this->entityManager->commit();
            } catch (\Exception $e) {
                $this->entityManager->getConnection()->rollBack();
                throw $e;
            }
        }

        // -- dispatch event
        $this->formBundleEventBus->dispatch(new FormEditModeChanged($templateId, true));

        return $result;
    }

    /**
     * @throws EntityNotFoundException
     */
    public function stopEditing(string $templateId): bool
    {
        $removalManager = new RemovalManager($this->entityManager);
        $this->formBundleEventBus->dispatch(new FormEditModeChanged($templateId, false));

        return $removalManager->removeFormBuilderData($templateId);
    }

    // --------------------------------------------------------------------------------
    // Add form template
    // --------------------------------------------------------------------------------

    public function addNewTemplate(
        string $templateId,
        string $name,
        string $locale,
        string $targetingType,
        array $roles,
    ): void {
        $this->saveAndPublishTemplate(self::createStartingTemplate(
            $templateId,
            $name,
            $locale,
            $targetingType,
            $roles,
        ));
    }

    public function importTemplate(
        string $templateId,
        string $name,
        string $locale,
        string $targetingType,
        array $roles,
        FormList $formList,
        WorkFlow $workFlow,
    ): void {
        $this->saveAndPublishTemplate(self::createTemplate(
            $templateId,
            $name,
            $locale,
            $targetingType,
            $roles,
            $formList,
            $workFlow
        ));
    }

    private function saveAndPublishTemplate(Template $template): void
    {
        if ($this->templateRepository->hasOneById($template->getId())) {
            throw new TemplateExistsException('Template with same ID already exists.');
        }

        if (!$template->hasCurrentVersion()) {
            // -- publish the working version to a current version
            $template->publishNewCurrentVersion();
        }

        $this->templateRepository->save($template);
    }

    private static function createStartingTemplate(
        string $templateId,
        string $name,
        string $locale,
        string $targetingType,
        array $roles,
    ): Template {
        $now = new \DateTimeImmutable();
        $end = $now->add(new \DateInterval('P1M'));
        $step = StepWithFixedDateStart::fromUidAndMinimalDependencies(
            Uuid::uuid4()->toString(),
            StepFixedPeriod::fromDates($now, $end),
            LocalizedInfoWithRequiredLabel::fromLocalizedRequiredLabel(
                LocalizedRequiredString::fromLocaleTranslations([$locale => 'Stap 1'])
            ),
            false
        );
        $steps = StepWithFixedDateStartCollection::createEmpty();
        $steps->add($step);
        $workflow = WorkFlow::fromSteps($steps);

        $flowPermission = FlowPermission::createEmpty();
        $flowPermission = $flowPermission->withFullWritePermissionsForRoleInStepUid($roles[0], $step->getUid());

        $question = TextInputNode::fromUidAndDependencies(
            Uuid::uuid4()->toString(),
            TextInput::fromParameters(),
            $flowPermission,
            self::createInfoWithRequiredLabel(
                [$locale => 'Sleep een categorie of vraag op deze vraag om te starten...'],
            ),
            false,
            TextInputNode::DESCRIPTION_DISPLAY_MODE_BOX,
        );

        $formList = FormList::createWithEmptyRootNode();
        $formList = $formList->withNewNodeAddedToParentCategory($question, $formList->getRootNode());

        return self::createTemplate(
            $templateId,
            $name,
            $locale,
            $targetingType,
            $roles,
            $formList,
            $workflow,
        );
    }

    private static function createTemplate(
        string $templateId,
        string $name,
        string $locale,
        string $targetingType,
        array $roles,
        ?FormList $formList,
        ?WorkFlow $workFlow,
    ): Template {
        $formInfo = FormInfo::fromLocalizedStrings(
            LocalizedRequiredString::fromLocaleTranslations([
                $locale => $name,
            ]),
            LocalizedOptionalString::createEmpty(),
            RoleLocalizedOptionalStringCollection::createEmpty()
        );

        return new Template(
            id: $templateId,
            targetingType: $targetingType,
            workingVersion: WorkingVersion::fromElements($formInfo, $workFlow, $formList),
            hierarchicalRolesThatCouldParticipate: RoleCollection::fromRoleNames($roles),
            rolesAllowedToCopyAnswers: RoleCollection::createEmpty(),
            rolesWithDisabledAutoTemporarySaving: RoleCollection::createEmpty(),
            rolesAllowedToExport: RoleCollection::createEmpty(),
            rolesForAutoQuestionWriteAccessInAdminSide: RoleCollection::createEmpty(),
        );
    }

    /**
     * @param array<string, string> $labelTranslations
     * @param array<string, string> $descriptionTranslations
     *
     * @throws LocalizedStringException
     */
    private static function createInfoWithRequiredLabel(array $labelTranslations, array $descriptionTranslations = []): LocalizedInfoWithRequiredLabel
    {
        return LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            LocalizedRequiredString::fromLocaleTranslations($labelTranslations),
            LocalizedOptionalString::fromLocaleTranslations($descriptionTranslations),
            RoleLocalizedOptionalStringCollection::createEmpty()
        );
    }

    // --------------------------------------------------------------------------------
    // Import form template
    // --------------------------------------------------------------------------------
    /**
     * @throws SubmissionsExistException
     */
    public function replaceWorkFlowAndFormList(string $templateId, ImportFormCommand $command, FormBuilderUser $user): bool
    {
        if (null === $this->submittedTemplateService) {
            return false;
        } else {
            $template = $this->templateRepository->find($templateId);
            if ($template instanceof Template) {
                if ($this->submittedTemplateService->hasSubmissionsForTemplate($templateId)) {
                    throw new SubmissionsExistException();
                }
                $doPublishForm = false;
                /* @var WorkingVersion */
                $workingVersion = $template->getWorkingVersion();
                // -- parse the workflow and inject it in the working version
                if (0 !== count($command->getWorkflow())) {
                    $doPublishForm = true;
                    $workflow = WorkFlow::fromJsonDecodedArray($command->getWorkflow());
                    $workingVersion = $workingVersion->withWorkFlow($workflow);
                    $template->updateWorkingVersion($workingVersion);
                }
                // -- parse the provided formlist and add it to the working version
                if (0 !== count($command->getFormList())) {
                    $doPublishForm = true;
                    $formList = FormList::fromJsonDecodedArray($command->getFormList());
                    $workingVersion = $workingVersion->withFormList($formList);
                    $template->updateWorkingVersion($workingVersion);
                }
                // -- publish the working version to a new current version
                if ($doPublishForm) {
                    $template->publishNewCurrentVersion();
                    $this->templateRepository->save($template);
                }
                $result = true;
            } else {
                $result = false;
            }
        }

        return $result;
    }

    // --------------------------------------------------------------------------------
    // Duplicate a form template
    // --------------------------------------------------------------------------------
    /**
     * @param bool $overwrite if true, it will overwrite the template if it
     *                        does not have any submissions and is not in editing mode
     *
     * @throws FormInEditingModeException
     * @throws SubmissionsExistException
     *                                       If false and it fails it will @throws TemplateExistsException
     * @throws LocalizedStringException
     * @throws \JsonException
     * @throws InputInvalidArgumentException
     * @throws MissingVersionException
     * @throws StepException
     * @throws StepNotificationException
     * @throws VersionException
     */
    public function duplicateForm(string $sourceTemplateUid, string $targetTemplateUid, string $name = '', bool $overwrite = false): void
    {
        $sourceTemplate = $this->templateRepository->getOneById($sourceTemplateUid);
        $targetTemplate = $this->templateRepository->find($targetTemplateUid);

        if (!$targetTemplate instanceof Template) {
            // new template
            $locale = $sourceTemplate->getCurrentVersion()->getInfo()->getLocalizedLabel()->getAvailableLocales()[0];
            $label = '' === $name ? $sourceTemplate->getCurrentVersion()->getInfo()->getLocalizedLabel()->getFallback() : $name;
            $currentVersion = $sourceTemplate->getCurrentVersion();
            $newTemplate = self::createTemplate(
                $targetTemplateUid,
                $label,
                $locale,
                $sourceTemplate->getTargetingType(),
                $sourceTemplate->getHierarchicalRolesThatCouldParticipate()->getNamesArray(),
                $currentVersion->getFormList(),
                $currentVersion->getWorkFlow(),
            );

            $this->saveAndPublishTemplate($newTemplate);
        } elseif ($overwrite) {
            // overwrite the existing template
            if ($this->submittedTemplateService?->hasSubmissionsForTemplate($targetTemplateUid) ?? false) {
                throw new SubmissionsExistException("Submission already exists for template $targetTemplateUid");
            }

            /** @var FormRepository formRepo */
            $formRepo = $this->entityManager->getRepository(Form::class);
            $form = $formRepo->findByUid($targetTemplate->getId());

            // only overwrite the target form when it's not in editing mode
            if ($form instanceof Form) {
                throw new FormInEditingModeException('Form is in editing mode and can\'t be overwritten.');
            }

            $workingVersion = $targetTemplate->getWorkingVersion();
            $workingVersion = $workingVersion->withInfo($sourceTemplate->getWorkingVersion()->getInfo());
            $workingVersion = $workingVersion->withWorkFlow($sourceTemplate->getWorkingVersion()->getWorkFlow());
            $workingVersion = $workingVersion->withFormList($sourceTemplate->getWorkingVersion()->getFormList());
            $targetTemplate->updateWorkingVersion($workingVersion);
            $targetTemplate->publishNewCurrentVersion();
            $this->templateRepository->save($targetTemplate);
        } else {
            throw new TemplateExistsException("Template already exists for uid $targetTemplateUid");
        }
    }

    // --------------------------------------------------------------------------------
    // Delete form template
    // --------------------------------------------------------------------------------
    /**
     * @throws SubmissionsExistException
     */
    public function deleteForm(FormBuilderUser $user, string $templateId): bool
    {
        if (null === $this->submittedTemplateService) {
            return false;
        } else {
            $template = $this->templateRepository->find($templateId);
            if ($template instanceof Template) {
                if ($this->submittedTemplateService->hasSubmissionsForTemplate($templateId)) {
                    throw new SubmissionsExistException();
                }
                $this->templateRepository->remove($template);

                return true;
            } else {
                return false;
            }
        }
    }

    // ------------------------------------------------------------------------------------
    // Render some json that is stored in the database (working version // current version)
    // ------------------------------------------------------------------------------------

    public function getTemplateWorkingVersionJson(string $templateId): object
    {
        $template = $this->templateRepository->getOneById($templateId);
        $templateJson = json_decode(json_encode($template->getWorkingVersion(), \JSON_THROW_ON_ERROR), null, 512, \JSON_THROW_ON_ERROR);

        return $templateJson;
    }

    public function getTemplateCurrentVersionJson(string $templateId): object
    {
        $template = $this->templateRepository->getOneById($templateId);
        $templateJson = json_decode(json_encode($template->getCurrentVersion(), \JSON_THROW_ON_ERROR), null, 512, \JSON_THROW_ON_ERROR);

        return $templateJson;
    }
}
