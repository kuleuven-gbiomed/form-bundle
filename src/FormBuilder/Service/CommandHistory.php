<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Service;

use Doctrine\ORM\EntityManagerInterface;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandInterface;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandSubject;
use KUL\FormBundle\FormBuilder\Commands\DeleteNode\DeleteNodeCommand;
use KUL\FormBundle\FormBuilder\Commands\DeleteOption\DeleteOptionCommand;
use KUL\FormBundle\FormBuilder\Commands\DeleteWorkflowStep\DeleteWorkflowStepCommand;
use KUL\FormBundle\FormBuilder\Entity\CommandHistoryEntry;
use KUL\FormBundle\FormBuilder\Entity\Form;
use KUL\FormBundle\FormBuilder\Entity\FormNode;
use KUL\FormBundle\FormBuilder\Entity\FormNodeFlowPermissionTemplate;
use KUL\FormBundle\FormBuilder\Entity\FormNodeOption;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflow;
use KUL\FormBundle\FormBuilder\Entity\FormWorkflowStep;
use KUL\FormBundle\FormBuilder\Repository\CommandHistoryEntryRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeFlowPermissionTemplateRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeOptionRepository;
use KUL\FormBundle\FormBuilder\Repository\FormNodeRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowRepository;
use KUL\FormBundle\FormBuilder\Repository\FormWorkflowStepRepository;
use Symfony\Component\Serializer\SerializerInterface;

class CommandHistory
{
    private readonly CommandHistoryEntryRepository $historyRepo;

    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $entityManager,
    ) {
        $this->historyRepo = $entityManager->getRepository(CommandHistoryEntry::class);
    }

    public function append(CommandInterface $command): void
    {
        switch ($command->getSubject()) {
            case CommandSubject::OPTION:
                if ($command instanceof DeleteOptionCommand) {
                    $formId = $command->getDelete()->getFormId();
                } else {
                    /** @psalm-var FormNodeOptionRepository $formNodeOptionRepo */
                    $formNodeOptionRepo = $this->entityManager->getRepository(FormNodeOption::class);
                    $option = $formNodeOptionRepo->getById($command->getId());
                    $formId = $option->getFormNode()->getForm()->getId();
                }
                break;
            case CommandSubject::FORM:
            case CommandSubject::FORM_INFO:
                $formId = $command->getId();
                break;
            case CommandSubject::PERMISSION_TEMPLATE:
                /** @psalm-var FormNodeFlowPermissionTemplateRepository $formNodeFlowPermissionTemplateRepo */
                $formNodeFlowPermissionTemplateRepo = $this->entityManager->getRepository(FormNodeFlowPermissionTemplate::class);
                $template = $formNodeFlowPermissionTemplateRepo->getById($command->getId());
                $formId = $template->getForm()->getId();
                break;
            case CommandSubject::WORKFLOW:
                /** @psalm-var FormWorkflowRepository $formWorkflowRepo */
                $formWorkflowRepo = $this->entityManager->getRepository(FormWorkflow::class);
                $workflow = $formWorkflowRepo->getById($command->getId());
                $formId = $workflow->getForm()->getId();
                break;
            case CommandSubject::WORKFLOW_STEP:
                if ($command instanceof DeleteWorkflowStepCommand) {
                    $formId = $command->getDelete()->getFormId();
                } else {
                    /** @psalm-var FormWorkflowStepRepository $formWorkflowStepRepo */
                    $formWorkflowStepRepo = $this->entityManager->getRepository(FormWorkflowStep::class);
                    $step = $formWorkflowStepRepo->getById($command->getId());
                    $formId = $step->getFormWorkflow()->getForm()->getId();
                }
                break;
            default:
                /** @psalm-var FormNodeRepository $formNodeRepo */
                $formNodeRepo = $this->entityManager->getRepository(FormNode::class);
                // -- for the deletenode command fetch the form ID from the command
                if ($command instanceof DeleteNodeCommand) {
                    $formId = $command->getDelete()->getFormId();
                } else {
                    $formId = $formNodeRepo->getById($command->getId())->getForm()->getId();
                }
                break;
        }

        /** @psalm-var FormRepository $formRepo */
        $formRepo = $this->entityManager->getRepository(Form::class);
        $form = $formRepo->getById($formId);
        $historyEntry = new CommandHistoryEntry(
            $form->getCurrentVersionNumber(),
            $form->getUid(),
            new \DateTimeImmutable(),
            $command->getUser()->getIdentifier(),
            $command->getId(),
            $command->getSubject(),
            $command->getType(),
            $this->serializer->serialize($command, 'json')
        );

        if (str_contains(strtoupper($historyEntry->getType()), 'UPDATE')) {
            // don't save an update command history when nothing has changed between from and to state
            if ($historyEntry->getChangeFrom() !== $historyEntry->getChangeTo()) {
                $this->historyRepo->save($historyEntry);
            }
        } else {
            $this->historyRepo->save($historyEntry);
        }
    }
}
