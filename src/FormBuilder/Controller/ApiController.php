<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use KUL\FormBundle\Entity\TemplateRepository;
use KUL\FormBundle\FormBuilder\Commands\AddForm\AddFormCommand;
use KUL\FormBundle\FormBuilder\Commands\CommandResultServiceInterface;
use KUL\FormBundle\FormBuilder\Commands\Common\CommandInterface;
use KUL\FormBundle\FormBuilder\Commands\Common\Exception\SubmissionsExistException;
use KUL\FormBundle\FormBuilder\Commands\ImportForm\ImportFormCommand;
use KUL\FormBundle\FormBuilder\Manager\ConstructionManager;
use KUL\FormBundle\FormBuilder\Manager\FormInEditingModeException;
use KUL\FormBundle\FormBuilder\Manager\QueryManager;
use KUL\FormBundle\FormBuilder\Manager\TemplateManager;
use KUL\FormBundle\FormBuilder\Utility\FormBuilderUser;
use KUL\FormBundle\FormBuilder\Utility\FormListFilter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use ZipStream\ZipStream;

class ApiController extends AbstractController
{
    private array $apiAccess = [];
    private bool $isDevelopment = false;

    // -----------------------------------------------------------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------------------------------------------------------
    public function __construct(
        KernelInterface $appKernel,
        private readonly EntityManagerInterface $entityManager,
        private readonly TemplateRepository $templateRepo,
        private readonly CommandResultServiceInterface $commandResultService,
        private readonly ParameterBagInterface $params,
        private readonly TemplateManager $templateManager,
    ) {
        // -- some special development settings
        if ('dev' === $appKernel->getEnvironment()) {
            $this->apiAccess = ['Access-Control-Allow-Origin' => '*'];
            $this->isDevelopment = true;
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Controller functions
    // -----------------------------------------------------------------------------------------------------------------

    public function getEndpoint(FormBuilderUser $user): JsonResponse
    {
        $message = new \stdClass();
        $message->message = "Hello {$user->getIdentifier()}! Welcome to the Form Builder API";

        return new JsonResponse($message, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Template list, start editing, stop editing functions
    // -----------------------------------------------------------------------------------------------------------------

    public function listTemplates(): JsonResponse
    {
        $templates = $this->templateManager->listTemplates();

        return new JsonResponse($templates, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Start and stop editing of a form
    // -----------------------------------------------------------------------------------------------------------------

    public function startEditing(FormBuilderUser $user, string $templateId): JsonResponse
    {
        $result = $this->templateManager->startEditing($templateId, $user);

        return new JsonResponse($result, 200, $this->apiAccess);
    }

    public function stopEditing(string $templateId): JsonResponse
    {
        $result = $this->templateManager->stopEditing($templateId);

        return new JsonResponse($result, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Render JSON versions (working & current) in the browser
    // -----------------------------------------------------------------------------------------------------------------

    public function getTemplateWorkingVersionJson(string $templateId): JsonResponse
    {
        $templateWorkingVersion = $this->templateManager->getTemplateWorkingVersionJson($templateId);

        return new JsonResponse($templateWorkingVersion, 200, $this->apiAccess);
    }

    public function getTemplateCurrentVersionJson(string $templateId): JsonResponse
    {
        $templateCurrentVersion = $this->templateManager->getTemplateCurrentVersionJson($templateId);

        return new JsonResponse($templateCurrentVersion, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Get route for downloading a form as zip
    // -----------------------------------------------------------------------------------------------------------------
    public function downloadCurrenVersionAsZip(FormBuilderUser $user, string $templateId): ?StreamedResponse
    {
        $formBuilderPermissions = $user->getPermissions();
        if ('WRITE' === $formBuilderPermissions['allowDownloadFormZip']->value) {
            $fileName = date('Ymd').'__'.$templateId.'.zip';

            return new StreamedResponse(
                function () use ($templateId, $fileName): void {
                    $templateCurrentVersion = $this->templateManager->getTemplateCurrentVersionJson($templateId);
                    $zipStream = new ZipStream(outputName: $fileName);
                    $zipStream->addFile('form.json', json_encode($templateCurrentVersion, \JSON_THROW_ON_ERROR));
                    $zipStream->finish();
                },
            );
        } else {
            return null;
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Post route for uploading a form with a zip file
    // -----------------------------------------------------------------------------------------------------------------

    public function uploadFormAsZip(FormBuilderUser $user, string $templateId, Request $request): JsonResponse
    {
        $formBuilderPermissions = $user->getPermissions();
        if ('WRITE' === $formBuilderPermissions['allowUploadFormZip']->value) {
            // todo: not yet implemented
            $result = true;
        } else {
            $result = false;
        }

        return new JsonResponse($result, Response::HTTP_NOT_IMPLEMENTED, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Post route for adding a form
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * @throws \Exception
     */
    public function addForm(FormBuilderUser $user, Request $request, MessageBusInterface $formBundleEventBus): JsonResponse
    {
        $formBuilderPermissions = $user->getPermissions();
        if ('WRITE' !== $formBuilderPermissions['formAdd']->value) {
            throw $this->createAccessDeniedException('You do not have permissions to add a form.');
        }

        $rawCommand = $request->request->get('templateToAdd');
        if (null === $rawCommand) {
            return new JsonResponse(['error' => 'no command provided'], 400, $this->apiAccess);
        }

        try {
            $formBundleEventBus->dispatch(
                AddFormCommand::hydrateFromJson(
                    json_decode((string) $rawCommand, null, 512, \JSON_THROW_ON_ERROR)
                )
            );
        } catch (\Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()], 400, $this->apiAccess);
        }

        return new JsonResponse(true, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Post route for importing a form on an existing template
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * @throws SubmissionsExistException
     */
    public function importForm(FormBuilderUser $user, string $templateId, Request $request): JsonResponse
    {
        $formBuilderPermissions = $user->getPermissions();
        if ('WRITE' === $formBuilderPermissions['allowImportForm']->value) {
            $rawCommand = $request->request->get('templateToImport');
            if (null === $rawCommand) {
                $result = false;
            } else {
                $jsonCommand = json_decode((string) $rawCommand, null, 512, \JSON_THROW_ON_ERROR);

                // -- hydrate the raw command into a typed command
                $command = ImportFormCommand::hydrateFromJson($jsonCommand);
                $result = $this->templateManager->replaceWorkFlowAndFormList($templateId, $command, $user);
            }
        } else {
            $result = false;
        }

        return new JsonResponse($result, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Post route for duplicating a form (on an existing template)
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * @throws SubmissionsExistException
     */
    public function duplicateForm(FormBuilderUser $user, string $templateId, Request $request): JsonResponse
    {
        $formBuilderPermissions = $user->getPermissions();
        if ('WRITE' === $formBuilderPermissions['allowDuplicateForm']->value) {
            $rawCommand = $request->request->get('templateToDuplicate');
            if (null === $rawCommand) {
                return new JsonResponse([
                    'message' => 'Command no template found.',
                ], Response::HTTP_NOT_FOUND, $this->apiAccess);
            } else {
                try {
                    $jsonCommand = json_decode((string) $rawCommand, null, 512, \JSON_THROW_ON_ERROR);
                } catch (\JsonException $e) {
                    return new JsonResponse([
                        'message' => 'Json command not valid.',
                    ], Response::HTTP_BAD_REQUEST, $this->apiAccess);
                }

                $targetTemplateId = $jsonCommand->targetUid;

                try {
                    $this->templateManager->duplicateForm($templateId, $targetTemplateId, overwrite: true);
                } catch (SubmissionsExistException|FormInEditingModeException $e) {
                    return new JsonResponse([
                        'message' => $e->getMessage(),
                    ], Response::HTTP_FORBIDDEN, $this->apiAccess);
                }
            }
        } else {
            return new JsonResponse([
                'message' => 'Command no template found.',
            ], Response::HTTP_FORBIDDEN, $this->apiAccess);
        }

        return new JsonResponse(true, Response::HTTP_OK, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Delete route, for admin to clean-up forms that are obsolete.
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * @throws SubmissionsExistException
     */
    public function deleteForm(FormBuilderUser $user, string $templateId, Request $request): JsonResponse
    {
        $formBuilderPermissions = $user->getPermissions();
        $result = false;
        if ($request->isMethod('OPTIONS')) {
            if ('WRITE' === $formBuilderPermissions['allowDeleteForm']->value) {
                // Set headers to indicate which methods and headers are allowed for the actual request
                if ($this->isDevelopment) {
                    header('Access-Control-Allow-Origin: *');
                }
                header('Access-Control-Allow-Methods: DELETE');
                header('Access-Control-Allow-Headers: Content-Type');
                http_response_code(200); // OK, preflight request success
                echo json_encode(['message' => 'Preflight request success']);
                exit;
            }
        } elseif ($request->isMethod('DELETE')) {
            if ('WRITE' === $formBuilderPermissions['allowDeleteForm']->value) {
                $result = $this->templateManager->deleteForm($user, $templateId);
            }
        }

        return new JsonResponse($result, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // FormBuilder Queries
    // -----------------------------------------------------------------------------------------------------------------

    public function getFormBuilderFormList(int $formId, string $stepUid = '', string $role = ''): JsonResponse
    {
        $request = Request::createFromGlobals();

        $filter = null;
        if ('' !== $role && '' !== $stepUid) {
            $filter = new FormListFilter($role, $stepUid);
        }

        $manager = new QueryManager($this->entityManager);
        $formList = $manager->getFormListSummary($formId, $filter);

        return new JsonResponse($formList, 200, $this->apiAccess);
    }

    public function getFormBuilderSettings(FormBuilderUser $user): JsonResponse
    {
        $settings = new \stdClass();
        $settings->availableLanguages = $this->params->get('kul_form.translation.available_locales');
        $settings->defaultLanguage = $this->params->get('kul_form.translation.default_locale');
        $settings->availableRoles = $this->params->get('kul_form.roles.available_roles');
        $settings->availableAdminRoles = $this->params->get('kul_form.roles.available_admin_roles');
        $settings->logo = $this->params->get('kul_form.builder.logo');

        $settings->formBuilderUser = $user->getIdentifier();
        $settings->formBuilderUserRoles = $user->getRoles();
        $settings->formBuilderPermissions = $user->getPermissions();

        return new JsonResponse($settings, 200, $this->apiAccess);
    }

    public function getFormByUid(string $uid): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);

        try {
            $form = $manager->getFormByUid($uid);

            return new JsonResponse($form, Response::HTTP_OK, $this->apiAccess);
        } catch (EntityNotFoundException $e) {
            return new JsonResponse([
                'message' => 'No form found for UID.',
            ], Response::HTTP_NOT_FOUND, $this->apiAccess);
        }
    }

    public function getFormBuilderFormNode(int $formId, int $nodeId): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $formNode = $manager->getFormNode($formId, $nodeId);

        return new JsonResponse($formNode, 200, $this->apiAccess);
    }

    public function getFormBuilderFormNodeFlowPermission(int $formId, int $nodeId): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $nodeFlowPermission = $manager->getNodeFlowPermission($formId, $nodeId);

        return new JsonResponse($nodeFlowPermission, 200, $this->apiAccess);
    }

    public function getFormBuilderOverviewFlowPermissions(int $formId): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $nodeFlowPermission = $manager->getOverviewFlowPermissions($formId);

        return new JsonResponse($nodeFlowPermission, 200, $this->apiAccess);
    }

    public function getFormBuilderFlowPermissionTemplates(int $formId): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $templates = $manager->getFlowPermissionTemplates($formId);

        return new JsonResponse($templates, 200, $this->apiAccess);
    }

    public function getChoiceQuestions(int $formId, string $language): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $questions = $manager->getChoiceQuestions($formId, $language);

        return new JsonResponse($questions, 200, $this->apiAccess);
    }

    public function getCategories(int $formId, string $language): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $categories = $manager->getCategories($formId, $language);

        return new JsonResponse($categories, 200, $this->apiAccess);
    }

    public function getQuestions(int $formId, string $language, Request $request): JsonResponse
    {
        $questions = [];
        $manager = new QueryManager($this->entityManager);

        // depending on the request filter is done on the query of questions
        // * no request items: all questions
        // * parent Id: all questions for that categorie
        // * list uid: all questions in that list of uid's

        if ($request->query->has('uids')) {
            $uidList = explode(',', (string) $request->query->get('uids'));
            $questions = $manager->getQuestionsByUidList($formId, $language, $uidList);
        } elseif ($request->query->has('parent')) {
            $parentId = $request->query->getInt('parent');
            $questions = $manager->getQuestionsByParent($formId, $language, $parentId);
        }

        return new JsonResponse($questions, 200, $this->apiAccess);
    }

    public function getQuestionConditions(int $formId, int $nodeId, string $language): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $conditions = $manager->getQuestionConditions($formId, $nodeId, $language);

        return new JsonResponse($conditions, 200, $this->apiAccess);
    }

    public function getQuestionScores(int $formId, int $nodeId, string $language): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $scores = $manager->getQuestionScores($formId, $nodeId, $language);

        return new JsonResponse($scores, 200, $this->apiAccess);
    }

    public function getRadarChartParticipants(int $formId, int $nodeId): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $participants = $manager->getQuestionRadarChartParticipants($formId, $nodeId);

        return new JsonResponse($participants, 200, $this->apiAccess);
    }

    public function getPrefillSources(int $formId, int $nodeId, string $language): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $participants = $manager->getPrefillSources($formId, $nodeId, $language);

        return new JsonResponse($participants, 200, $this->apiAccess);
    }

    public function getNodeRelations(int $formId, int $nodeId): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $relations = $manager->getNodeRelations($nodeId);

        return new JsonResponse($relations, 200, $this->apiAccess);
    }

    public function getQuestionRelationsSummary(int $formId, int $nodeId, string $language): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $relations = $manager->getQuestionRelationsSummary($formId, $nodeId, $language);

        return new JsonResponse($relations, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // FormBuilder Workflow Endpoints
    // -----------------------------------------------------------------------------------------------------------------

    public function getWorkflow(int $formId): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $result = $manager->getWorkflowDetail($formId);

        return new JsonResponse($result, 200, $this->apiAccess);
    }

    public function getWorkflowSchema(int $formId, string $language): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $result = $manager->getWorkflowSchema($formId, $language);

        return new JsonResponse($result, 200, $this->apiAccess);
    }

    public function getWorkflowStepDetail(int $formId, int $stepNumber): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $conditions = $manager->getWorkflowStepDetail($formId, $stepNumber);

        return new JsonResponse($conditions, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // FormBuilder Command History Endpoints
    // -----------------------------------------------------------------------------------------------------------------

    public function getCommandHistory(string $commandType, int $subjectId): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $history = $manager->getCommandHistory($commandType, $subjectId);

        return new JsonResponse($history, 200, $this->apiAccess);
    }

    public function getTranslatedCommandHistory(int $formId, string $language): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $commandHistory = $manager->getTranslatedCommandHistory($formId, $language);

        return new JsonResponse($commandHistory, 200, $this->apiAccess);
    }

    public function getTranslatedCommandHistorySinceLastPublish(int $formId, string $language): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $commandHistory = $manager->getTranslatedCommandHistorySinceLastPublish($formId, $language);

        return new JsonResponse($commandHistory, 200, $this->apiAccess);
    }

    public function countTranslatedCommandHistorySinceLastPublish(int $formId, string $language): JsonResponse
    {
        $manager = new QueryManager($this->entityManager);
        $commandHistory = $manager->countTranslatedCommandHistorySinceLastPublish($formId, $language);

        return new JsonResponse($commandHistory, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // FormBuilder Command Endpoint
    // -----------------------------------------------------------------------------------------------------------------

    /** @see CommandParamConverter */
    public function executeFormBuilderCommand(?CommandInterface $command, FormBuilderUser $user): JsonResponse
    {
        $result = $this->commandResultService->getTranslatedCommandResult($command, $user);

        return new JsonResponse($result, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // FormBuilder: validation and publication endpoints
    // -----------------------------------------------------------------------------------------------------------------

    public function validateForm(int $formId): JsonResponse
    {
        $manager = new ConstructionManager($this->templateRepo, $this->entityManager);
        $validationMessages = $manager->validateForm($formId);

        return new JsonResponse($validationMessages, 200, $this->apiAccess);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Temp function for checking the construction function
    // -----------------------------------------------------------------------------------------------------------------

    public function constructForm(int $formId): JsonResponse
    {
        $manager = new ConstructionManager($this->templateRepo, $this->entityManager);
        $workingVersion = $manager->createWorkingVersion($formId);

        return new JsonResponse($workingVersion, 200, $this->apiAccess);
    }
}
