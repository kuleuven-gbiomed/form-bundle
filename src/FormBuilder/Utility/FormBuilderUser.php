<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Utility;

use KUL\FormBundle\FormBuilder\Permission;

final readonly class FormBuilderUser
{
    /**
     * @param string[]                 $roles
     * @param array<string,Permission> $permissions
     */
    public function __construct(
        private string $identifier,
        private array $roles,
        private array $permissions,
    ) {
    }

    /**
     * @param string[]   $roles
     * @param string[]   $availablePermissions
     * @param string[][] $permissionsForRoles
     */
    public static function newFromConfig(
        string $identifier,
        array $roles,
        array $availablePermissions,
        array $permissionsForRoles,
    ): self {
        return new self(
            $identifier,
            $roles,
            self::computePermissions(
                $roles,
                $availablePermissions,
                $permissionsForRoles
            ),
        );
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /** @return string[] */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /** @return array<string,Permission> */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @param string[]   $roles
     * @param string[]   $availablePermissions
     * @param string[][] $permissionsForRoles
     *
     * @return array<string,Permission>
     */
    private static function computePermissions(
        array $roles,
        array $availablePermissions,
        array $permissionsForRoles,
    ): array {
        $permissions = [];
        if (0 !== count($availablePermissions)) {
            $permissions = array_fill_keys($availablePermissions, Permission::NONE);
            foreach ($roles as $role) {
                if (array_key_exists($role, $permissionsForRoles)) {
                    foreach ($permissionsForRoles[$role] as $permissionName => $permissionSetting) {
                        $permissions[$permissionName] = $permissions[$permissionName]->add(Permission::from($permissionSetting));
                    }
                }
            }
        }

        return $permissions;
    }
}
