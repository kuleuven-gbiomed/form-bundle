<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Utility;

class FormListFilter
{
    private readonly string $role;
    private readonly string $stepUid;

    public function __construct(string $role, string $stepUid)
    {
        if ('' === $role) {
            throw new \InvalidArgumentException('parameter [role] must be a non-empty string');
        }

        if ('' === $stepUid) {
            throw new \InvalidArgumentException('parameter [stepUid] must be a non-empty string');
        }

        $this->role = $role;
        $this->stepUid = $stepUid;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getStepUid(): string
    {
        return $this->stepUid;
    }
}
