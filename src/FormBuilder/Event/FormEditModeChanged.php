<?php

declare(strict_types=1);

namespace KUL\FormBundle\FormBuilder\Event;

use KUL\FormBundle\Admin\Event\FormTemplateEvent;

readonly class FormEditModeChanged implements FormTemplateEvent
{
    public function __construct(
        private string $templateId,
        private bool $isInEditMode,
    ) {
    }

    public function getTemplateId(): string
    {
        return $this->templateId;
    }

    public function isInEditMode(): bool
    {
        return $this->isInEditMode;
    }
}
