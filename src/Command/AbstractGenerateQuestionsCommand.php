<?php

declare(strict_types=1);

namespace KUL\FormBundle\Command;

use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Console\Command\Command;

/**
 * An abstract command you can inherit from if you need a command to create/update your form templates.
 */
abstract class AbstractGenerateQuestionsCommand extends Command
{
    public function __construct(protected TemplateRepository $templateRepository, ?string $name = null)
    {
        parent::__construct($name);
    }

    protected function getTemplateWithQuestionsRemoved(string $templateId): Template
    {
        $template = $this->templateRepository->getOneById($templateId);

        $formList = $template->getWorkingVersion()->getFormList();
        if (!$formList->isEmpty()) {
            $template->updateWorkingVersion(
                $template->getWorkingVersion()->withFormList(FormList::createWithEmptyRootNode())
            );
        }

        return $template;
    }
}
