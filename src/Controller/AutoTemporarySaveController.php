<?php

declare(strict_types=1);

namespace KUL\FormBundle\Controller;

use KUL\FormBundle\Client\Response\DoAccessDeniedResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoAutoTemporarySaveFailedWithFormValidationErrorsResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoAutoTemporarySaveHasNotDetectedChangesToSaveResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoInvalidStepStoringAttemptWithAccessInNewStepResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoStoringAutoTemporarySaveSuccessResponse;
use KUL\FormBundle\Client\TemplateTargetStoringService;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Entity\Template;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class AutoTemporarySaveController
{
    public function __construct(
        private readonly TemplateTargetStoringService $templateTargetStoringService,
        private readonly LoggerInterface $logger,
        private readonly TranslatorInterface $translator,
    ) {
    }

    public function autoTemporarySaveAction(
        Request $request,
        Template $template,
        TemplateTargetableInterface $target,
        ClientWithTargetAccessInterface $access,
    ): JsonResponse {
        $data = ['auto_saved' => false];

        try {
            $serviceResponse = $this->templateTargetStoringService->getStoringAutoTemporarySaveResponse(
                $request,
                $template,
                $access,
                $target->getReferenceDate(),
                $target->getReferenceDate()
            );
        } catch (\Exception $e) {
            $this->logger->error('auto temporary failed: '.$e->getMessage(), $e->getTrace());
            $data['message'] = $this->translator->trans('storingAttemptInDifferentStep.autoTemporarySaveAttempt.tempAutoSaveFailed', [], 'kulFormStepStoringAttempt');
            $data['failed_submit'] = true;

            return new JsonResponse($data, JsonResponse::HTTP_BAD_REQUEST);
        }

        // user loaded form in a step that is now ended (by another user or by deadline) but user has still (but different) access in the new step
        if ($serviceResponse instanceof DoInvalidStepStoringAttemptWithAccessInNewStepResponse) {
            $steps = $template->getCurrentVersion()->getWorkFlow()->getSteps();
            $data['message'] = $this->translator->trans(
                'storingAttemptInDifferentStep.autoTemporarySaveAttempt.stillAccessMessage',
                [
                    '%usedStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($serviceResponse->getUsedStep(), $request->getLocale()),
                    '%currentStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($serviceResponse->getCurrentStep(), $request->getLocale()),
                ],
                'kulFormStepStoringAttempt'
            );
            $data['invalid_step_storing_attempt'] = true;
            $data['still_access_in_new_current_step'] = true;

            return new JsonResponse($data, JsonResponse::HTTP_OK);
        }

        // user loaded form in a step that is now ended (by another user or by deadline) and user has no more access at all in the new step
        if ($serviceResponse instanceof DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse) {
            $steps = $template->getCurrentVersion()->getWorkFlow()->getSteps();
            $data['message'] = $this->translator->trans(
                'storingAttemptInDifferentStep.autoTemporarySaveAttempt.noMoreAccessMessage',
                [
                    '%usedStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($serviceResponse->getUsedStep(), $request->getLocale()),
                    '%currentStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($serviceResponse->getCurrentStep(), $request->getLocale()),
                ],
                'kulFormStepStoringAttempt'
            );
            $data['invalid_step_storing_attempt'] = true;
            $data['still_access_in_new_current_step'] = false;

            return new JsonResponse($data, JsonResponse::HTTP_OK);
        }

        // user has no access in the step due to: user's fault (hacking...) or admin side changes to form while user was working in form:
        // admin disabled auto-save and/or admin removed/altered the access rights for the role that user has 1loaded form in.
        if ($serviceResponse instanceof DoAccessDeniedResponse) {
            $accessDeniedReason = $serviceResponse->getReason();

            $data['message'] = match ($accessDeniedReason) {
                DoAccessDeniedResponse::AUTO_SAVE_DISABLED_FOR_CLIENT_ACCESSING_ROLE => $this->translator->trans('storingAttemptInDifferentStep.autoTemporarySaveAttempt.tempSaveNotAllowed', [], 'kulFormStepStoringAttempt'),
                DoAccessDeniedResponse::CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP,
                DoAccessDeniedResponse::CLIENT_HAS_NO_ROLES_WITH_ACCESS_IN_CURRENT_STEP,
                DoAccessDeniedResponse::REQUEST_SLUG_ROLE_DOES_NOT_MATCH_TEMPLATE_CLIENT_TARGET_ACCESS => $this->translator->trans('storingAttemptInDifferentStep.autoTemporarySaveAttempt.noAccessAtThisPoint', [], 'kulFormStepStoringAttempt'),
                default => 'access denied',
            };
            $data['access_denied'] = true;

            return new JsonResponse($data, JsonResponse::HTTP_FORBIDDEN);
        }

        if ($serviceResponse instanceof DoAutoTemporarySaveHasNotDetectedChangesToSaveResponse) {
            $data['no_changes_detected'] = true;

            return new JsonResponse($data, JsonResponse::HTTP_OK);
        }

        if ($serviceResponse instanceof DoAutoTemporarySaveFailedWithFormValidationErrorsResponse) {
            $data['message'] = $this->translator->trans('storingAttemptInDifferentStep.autoTemporarySaveAttempt.tempSaveFailedWithInvalidSubmit', [], 'kulFormStepStoringAttempt');
            $data['invalid_submit'] = true;

            return new JsonResponse($data, JsonResponse::HTTP_BAD_REQUEST);
        }

        if ($serviceResponse instanceof DoStoringAutoTemporarySaveSuccessResponse) {
            $data['auto_saved'] = true;

            return new JsonResponse($data, JsonResponse::HTTP_OK);
        }

        $this->logger->error('unexpected auto save fail without exception.');
        $data['message'] = $this->translator->trans('storingAttemptInDifferentStep.autoTemporarySaveAttempt.tempAutoSaveFailed', [], 'kulFormStepStoringAttempt');
        $data['failed_submit'] = true;

        return new JsonResponse($data, JsonResponse::HTTP_BAD_REQUEST);
    }
}
