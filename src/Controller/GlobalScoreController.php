<?php

declare(strict_types=1);

namespace KUL\FormBundle\Controller;

use KUL\FormBundle\Client\Response\DoAccessDeniedResponse;
use KUL\FormBundle\Client\Response\DoGlobalScoreResponse\AbstractDoGlobalScoreResponse;
use KUL\FormBundle\Client\TemplateTargetGlobalScoreService;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Entity\Template;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GlobalScoreController
{
    public function __construct(
        private readonly TemplateTargetGlobalScoreService $globalScoreService,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function calculateGlobalScoreAction(
        Request $request,
        Template $template,
        TemplateTargetableInterface $target,
        ClientWithTargetAccessInterface $access,
        NodeInterface $node,
    ): JsonResponse {
        try {
            $serviceResponse = $this->globalScoreService->getGlobalScoreCalculatedValueResponse(
                // FIXME (FORM-126): Don't pass around the Request object
                $request,
                $template,
                $access,
                $node->getUid(),
                $target->getReferenceDate(),
                $target->getReferenceDate()
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return new JsonResponse(
                ['error' => $e->getMessage()],
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        // if no access due to user's fault (hacking/messing with url, e.g. trying to call calculation on a node(Uid) to
        // which user has no write access,...) or form already moved on in workflow (not user's fault)
        if ($serviceResponse instanceof DoAccessDeniedResponse) {
            return new JsonResponse(
                ['message' => 'Access denied. Reason: '.$serviceResponse->getReason(), JsonResponse::HTTP_FORBIDDEN]
            );
        }

        if ($serviceResponse instanceof AbstractDoGlobalScoreResponse) {
            // all handled doResponses (both the failed due InsufficientData or FormValidationErrors and the Success)
            // for calculation are JsonSerializable and contain JSON the (generic) front end is aware of.
            return new JsonResponse($serviceResponse, JsonResponse::HTTP_OK);
        }

        throw new AccessDeniedException('Something unexpected happened.');
    }
}
