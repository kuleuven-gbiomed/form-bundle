<?php

declare(strict_types=1);

namespace KUL\FormBundle\Controller;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\Revert\Exception\AccessForResetDeniedException;
use KUL\FormBundle\Client\Revert\Exception\AccessForRollbackDeniedException;
use KUL\FormBundle\Client\Revert\ResetService;
use KUL\FormBundle\Client\Revert\RollbackService;
use KUL\FormBundle\Client\TemplateTargetRevertService;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Entity\Template;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class RevertController extends AbstractController
{
    public function __construct(
        private readonly TemplateTargetRevertService $templateTargetRevertService,
        private readonly RollbackService $rollbackService,
        private readonly ResetService $resetService,
        private readonly LoggerInterface $logger,
        private readonly TranslatorInterface $translator,
    ) {
    }

    public function rollback(
        Request $request,
        Template $template,
        TemplateTargetableInterface $target,
        ClientWithTargetAccessInterface $access,
        StepInterface $step,
    ): Response {
        $clientAccess = $this->templateTargetRevertService->findClientAccessForRollbackStep(
            $request,
            $template,
            $target,
            $access,
            $step
        );

        if (!$clientAccess instanceof ClientCombinedAccessObject) {
            throw new AccessForRollbackDeniedException($this->translator->trans('revert.rollback.action.accessDenied', [], 'kulFormRevert'));
        }

        $stepLabel = $clientAccess->getCurrentVersionWorkflow()->getSteps()->getStepLabelPrefixedWithPositionNumber($step, $request->getLocale());

        try {
            // access found for client means there is a stored form in DB, which can be rolled back to given step,
            // and is allowed to be rolled back to given step by client
            $this->rollbackService->rollBackStoredTemplateTargetFormToStep(
                $clientAccess->getStoredTemplateTarget(),
                $step
            );

            $this->addFlash(
                'kul_form_revert_result',
                $this->translator->trans(
                    'revert.rollback.action.success',
                    ['%step%' => $stepLabel],
                    'kulFormRevert'
                )
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            $this->addFlash(
                'kul_form_revert_result',
                '<span class="text-danger">'.$this->translator->trans(
                    'revert.rollback.action.failed',
                    ['%step%' => $stepLabel],
                    'kulFormRevert'
                ).'</span>'
            );
        }

        return new RedirectResponse($this->templateTargetRevertService->getReloadUrl($request, $clientAccess));
    }

    public function apiRollback(
        Request $request,
        Template $template,
        TemplateTargetableInterface $target,
        ClientWithTargetAccessInterface $access,
        StepInterface $step,
    ): JsonResponse {
        $clientAccess = $this->templateTargetRevertService->findClientAccessForRollbackStep(
            $request,
            $template,
            $target,
            $access,
            $step
        );

        if (!$clientAccess instanceof ClientCombinedAccessObject) {
            return new JsonResponse(
                [
                    'success' => false,
                    'message' => $this->translator->trans('revert.rollback.action.accessDenied', [], 'kulFormRevert'),
                ],
                Response::HTTP_FORBIDDEN
            );
        }

        $stepLabel = $clientAccess->getCurrentVersionWorkflow()->getSteps()->getStepLabelPrefixedWithPositionNumber($step, $request->getLocale());

        try {
            // access found for client means there is a stored form in DB, which can be rolled back to given step,
            // and is allowed to be rolled back to given step by client
            $this->rollbackService->rollBackStoredTemplateTargetFormToStep(
                $clientAccess->getStoredTemplateTarget(),
                $step
            );

            return new JsonResponse(
                [
                    'success' => true,
                    'message' => $this->translator->trans('revert.rollback.action.success', ['%step%' => $stepLabel], 'kulFormRevert'),
                ]
            );
        } catch (\Exception) {
            return new JsonResponse(
                [
                    'success' => false,
                    'message' => $this->translator->trans('revert.rollback.action.failed', ['%step%' => $stepLabel], 'kulFormRevert'),
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function reset(
        Request $request,
        Template $template,
        TemplateTargetableInterface $target,
        ClientWithTargetAccessInterface $access,
    ): Response {
        $clientAccess = $this->templateTargetRevertService->findClientAccessForReset(
            $request,
            $template,
            $target,
            $access,
        );

        if (!$clientAccess instanceof ClientCombinedAccessObject) {
            throw new AccessForResetDeniedException($this->translator->trans('revert.reset.action.accessDenied', [], 'kulFormRevert'));
        }

        try {
            // access found for client means there is a stored form in DB, which can be reset, and is allowed to be reset by client
            $this->resetService->resetStoredTemplateTargetForm($clientAccess->getStoredTemplateTarget());

            $this->addFlash(
                'kul_form_revert_result',
                $this->translator->trans(
                    'revert.reset.action.success',
                    [],
                    'kulFormRevert'
                )
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            $this->addFlash(
                'kul_form_revert_result',
                $this->translator->trans(
                    'revert.reset.action.failed',
                    [],
                    'kulFormRevert'
                )
            );
        }

        return new RedirectResponse($this->templateTargetRevertService->getReloadUrl($request, $clientAccess));
    }

    public function apiReset(
        Request $request,
        Template $template,
        TemplateTargetableInterface $target,
        ClientWithTargetAccessInterface $access,
    ): JsonResponse {
        $clientAccess = $this->templateTargetRevertService->findClientAccessForReset(
            $request,
            $template,
            $target,
            $access,
        );

        if (!$clientAccess instanceof ClientCombinedAccessObject) {
            return new JsonResponse(
                [
                    'success' => false,
                    'message' => $this->translator->trans('revert.reset.action.accessDenied', [], 'kulFormRevert'),
                ],
                Response::HTTP_FORBIDDEN
            );
        }

        try {
            // access found for client means there is a stored form in DB, which can be reset, and is allowed to be reset by client
            $this->resetService->resetStoredTemplateTargetForm($clientAccess->getStoredTemplateTarget());

            return new JsonResponse(
                [
                    'success' => true,
                    'message' => $this->translator->trans('revert.reset.action.success', [], 'kulFormRevert'),
                ]
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return new JsonResponse(
                [
                    'success' => false,
                    'message' => $this->translator->trans('revert.reset.action.failed', [], 'kulFormRevert'),
                ]
            );
        }
    }
}
