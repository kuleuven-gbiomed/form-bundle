<?php

declare(strict_types=1);

namespace KUL\FormBundle\Controller;

use KUL\FormBundle\Client\Response\DoAccessDeniedResponse;
use KUL\FormBundle\Client\Response\DoFileResponse\DoFileAnswerDownloadSuccessResponse;
use KUL\FormBundle\Client\Response\DoFileResponse\DoFileAnswerUploadInvalidFilesResponse;
use KUL\FormBundle\Client\Response\DoFileResponse\DoFileAnswerUploadSuccessResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoInvalidStepStoringAttemptWithAccessInNewStepResponse;
use KUL\FormBundle\Client\Response\DoStoringResponse\DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse;
use KUL\FormBundle\Client\TemplateTargetFileAnswerService;
use KUL\FormBundle\Domain\Contract\ClientWithTargetAccessInterface;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Utility\DownloadFileResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class FileController
{
    private const UNKNOWN_FAILURE = 'something went wrong. Please contact an administrator if this problem persists.';

    public function __construct(
        private readonly TemplateTargetFileAnswerService $templateTargetFileAnswerService,
        private readonly LoggerInterface $logger,
        private readonly TranslatorInterface $translator,
    ) {
    }

    public function uploadFileQuestionAnswerAction(
        Request $request,
        Template $template,
        TemplateTargetableInterface $target,
        ClientWithTargetAccessInterface $access,
        NodeInterface $node,
    ): JsonResponse {
        try {
            $serviceResponse = $this->templateTargetFileAnswerService->getUploadFileQuestionAnswerResponse(
                // FIXME (FORM-126): Don't pass around the Request object
                $request,
                $template,
                $access,
                $node->getUid(),
                $target->getReferenceDate(),
                $target->getReferenceDate()
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return new JsonResponse(['error' => self::UNKNOWN_FAILURE], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        if ($serviceResponse instanceof DoFileAnswerUploadInvalidFilesResponse) {
            return new JsonResponse($serviceResponse->getUploadInvalidFilesResponseData(), Response::HTTP_OK);
        }

        $data = ['success' => false];

        // user loaded form in a step that is now ended (by another user or by deadline) but user has still (but different) access in the new step
        if ($serviceResponse instanceof DoInvalidStepStoringAttemptWithAccessInNewStepResponse) {
            $steps = $template->getCurrentVersion()->getWorkFlow()->getSteps();
            $data['message'] = $this->translator->trans(
                'storingAttemptInDifferentStep.autoTemporarySaveAttempt.stillAccessMessage',
                [
                    '%usedStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($serviceResponse->getUsedStep(), $request->getLocale()),
                    '%currentStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($serviceResponse->getCurrentStep(), $request->getLocale()),
                ],
                'kulFormStepStoringAttempt'
            );
            $data['invalid_step_storing_attempt'] = true;
            $data['still_access_in_new_current_step'] = true;

            return new JsonResponse($data, Response::HTTP_OK);
        }

        // user loaded form in a step that is now ended (by another user or by deadline) and user has no more access at all in the new step
        if ($serviceResponse instanceof DoInvalidStepStoringAttemptWithoutAccessInNewStepResponse) {
            $steps = $template->getCurrentVersion()->getWorkFlow()->getSteps();
            $data['message'] = $this->translator->trans(
                'storingAttemptInDifferentStep.autoTemporarySaveAttempt.noMoreAccessMessage',
                [
                    '%usedStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($serviceResponse->getUsedStep(), $request->getLocale()),
                    '%currentStepLabel%' => $steps->getStepLabelPrefixedWithPositionNumber($serviceResponse->getCurrentStep(), $request->getLocale()),
                ],
                'kulFormStepStoringAttempt'
            );
            $data['invalid_step_storing_attempt'] = true;
            $data['still_access_in_new_current_step'] = false;

            return new JsonResponse($data, Response::HTTP_OK);
        }

        // user has no access in the step due to: user's fault (hacking...) or admin side changes to form while user was working in form:
        // admin disabled auto-save and/or admin removed/altered the access rights for the role that user has 1loaded form in.
        if ($serviceResponse instanceof DoAccessDeniedResponse) {
            $accessDeniedReason = $serviceResponse->getReason();

            $data['message'] = match ($accessDeniedReason) {
                DoAccessDeniedResponse::CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP,
                DoAccessDeniedResponse::CLIENT_HAS_NO_ROLES_WITH_ACCESS_IN_CURRENT_STEP,
                DoAccessDeniedResponse::CLIENT_REQUESTS_INVALID_NODE,
                DoAccessDeniedResponse::CLIENT_ROLE_HAS_NO_ACCESS_IN_CURRENT_STEP_TO_SPECIFIED_NODE => $this->translator->trans('storingAttemptInDifferentStep.autoTemporarySaveAttempt.noAccessAtThisPoint', [], 'kulFormStepStoringAttempt'),
                default => 'access denied',
            };

            return new JsonResponse($data, Response::HTTP_FORBIDDEN);
        }

        if (!$serviceResponse instanceof DoFileAnswerUploadSuccessResponse) {
            throw new AccessDeniedException('Something unexpected happened.');
        }

        $data['success'] = true;
        $data['files_data'] = $serviceResponse->getSavedFiles()->jsonSerialize();

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function downloadFileQuestionAnswerAction(
        Request $request,
        Template $template,
        TemplateTargetableInterface $target,
        ClientWithTargetAccessInterface $access,
        NodeInterface $node,
        string $fileUid,
    ): Response {
        try {
            $serviceResponse = $this->templateTargetFileAnswerService->getDownloadFileQuestionAnswerResponse(
                // FIXME (FORM-126): Don't pass around the Request object
                $request,
                $template,
                $access,
                $node->getUid(),
                $fileUid,
                $target->getReferenceDate(),
                $target->getReferenceDate()
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['error' => self::UNKNOWN_FAILURE], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            }

            throw new \Exception(self::UNKNOWN_FAILURE);
        }

        if ($serviceResponse instanceof DoAccessDeniedResponse) {
            throw new AccessDeniedException('Cannot download file because the user does not have access.');
        }

        if (!$serviceResponse instanceof DoFileAnswerDownloadSuccessResponse) {
            throw new AccessDeniedException('Something unexpected happened.');
        }

        return new DownloadFileResponse(
            $serviceResponse->getFile(),
            $this->templateTargetFileAnswerService->getUploadDirectory()
        );
    }
}
