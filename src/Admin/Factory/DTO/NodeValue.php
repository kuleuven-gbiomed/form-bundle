<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Factory\DTO;

class NodeValue
{
    private string $submissionId;
    private string $submitter;
    private \DateTimeImmutable $submittedAt;
    private string $value;
    private string $valueScore;

    public function __construct(
        string $submissionId,
        string $submitter,
        \DateTimeImmutable $submittedAt,
        string $value,
        string $valueScore,
    ) {
        $this->submissionId = $submissionId;
        $this->submitter = $submitter;
        $this->submittedAt = $submittedAt;
        $this->value = $value;
        $this->valueScore = $valueScore;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getSubmissionId(): string
    {
        return $this->submissionId;
    }

    public function getSubmitter(): string
    {
        return $this->submitter;
    }

    public function getSubmittedAt(): \DateTimeImmutable
    {
        return $this->submittedAt;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getValueScore(): string
    {
        return $this->valueScore;
    }
}
