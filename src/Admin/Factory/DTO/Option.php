<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Factory\DTO;

class Option
{
    private string $id;
    private string $label;
    private string $score;
    private int $sequence;

    public function __construct(
        string $id,
        string $label,
        string $score,
        int $sequence,
    ) {
        $this->id = $id;
        $this->label = $label;
        $this->score = $score;
        $this->sequence = $sequence;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getId(): string
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getScore(): string
    {
        return $this->score;
    }

    public function getSequence(): int
    {
        return $this->sequence;
    }
}
