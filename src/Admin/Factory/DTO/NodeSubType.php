<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Factory\DTO;

enum NodeSubType: string
{
    case OTHER = 'other';
    case TEXT = 'text';
    case NUMBER = 'number';
    case CHOICE_RADIO = 'choice_radio';
    case CHOICE_CHECKBOX = 'choice_checkbox';
    case CHOICE_SELECT = 'choice_select';
    case DATE = 'date';
    case INFO = 'info';
    case FILE = 'file';
    case CHART = 'chart';
}
