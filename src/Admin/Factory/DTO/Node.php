<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Factory\DTO;

class Node
{
    private string $id;
    private string $type;
    private string $subtype;
    private string $label;
    private string $description;
    private int $sequence;
    private string $number;
    private array $children;
    private array $options;
    private array $submissions;
    private bool $required;

    public function __construct(
        string $id,
        string $type,
        string $subtype,
        string $label,
        string $description,
        int $sequence = 0,
        string $number = '',
        bool $required = false,
    ) {
        $this->id = $id;
        $this->type = $type;
        $this->subtype = $subtype;
        $this->label = $label;
        $this->description = $description;
        $this->sequence = $sequence;
        $this->number = $number;
        $this->required = $required;
        $this->children = [];
        $this->options = [];
        $this->submissions = [];
    }

    // -----------------------------------------------------------------------------------------------------------------
    // data setters
    // -----------------------------------------------------------------------------------------------------------------

    public function addChildNode(self $node): void
    {
        $this->children[] = $node;
    }

    public function addOption(Option $option): void
    {
        $this->options[] = $option;
    }

    public function addSubmission(NodeValue $submission): void
    {
        $this->submissions[] = $submission;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getType(): NodeType
    {
        return NodeType::from($this->type);
    }

    public function getSubType(): NodeSubType
    {
        return NodeSubType::from($this->subtype);
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getSequence(): int
    {
        return $this->sequence;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getSubmissions(): array
    {
        return $this->submissions;
    }
}
