<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Factory\DTO;

class FormTemplate
{
    private string $id;
    private string $name;
    private string $description;
    private string $language;
    private ?Node $rootNode;
    private array $submissions;

    public function __construct(
        string $id,
        string $name,
        string $description,
        string $language,
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->language = $language;
        $this->submissions = [];
        $this->rootNode = null;
    }

    public function setRootNode(Node $node): void
    {
        $this->rootNode = $node;
    }

    public function addSubmission(Submission $submission): void
    {
        $this->submissions[] = $submission;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getRootNode(): ?Node
    {
        return $this->rootNode;
    }

    public function getSubmissions(): array
    {
        return $this->submissions;
    }
}
