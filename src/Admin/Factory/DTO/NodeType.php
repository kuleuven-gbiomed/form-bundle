<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Factory\DTO;

enum NodeType: string
{
    case QUESTION = 'question';
    case CATEGORY = 'category';
    case INFO = 'info';
    case RADAR = 'radar_chart';
}
