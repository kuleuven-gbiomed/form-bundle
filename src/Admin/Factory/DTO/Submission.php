<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Factory\DTO;

class Submission
{
    private string $id;
    private \DateTimeImmutable $submittedAt;
    private string $targetCode;
    private string $targetId;
    private array $submitters;

    // -----------------------------------------------------------------------------------------------------------------
    // constructor
    // -----------------------------------------------------------------------------------------------------------------

    public function __construct(
        string $id,
        string $targetCode,
        string $targetId,
    ) {
        $this->id = $id;
        $this->submittedAt = new \DateTimeImmutable('1970-01-01');
        $this->targetCode = $targetCode;
        $this->targetId = $targetId;
        $this->submitters = [];
    }

    // -----------------------------------------------------------------------------------------------------------------
    // data setters
    // -----------------------------------------------------------------------------------------------------------------

    public function setLatestSubmissionDate(\DateTimeImmutable $submissionDate): void
    {
        if ($this->submittedAt >= $submissionDate) {
            return;
        }
        $this->submittedAt = $submissionDate;
    }

    public function addSubmitter(string $submitter): void
    {
        foreach ($this->submitters as $existingSubmitter) {
            if ($existingSubmitter === $submitter) {
                return;
            }
        }
        $this->submitters[] = $submitter;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // getters
    // -----------------------------------------------------------------------------------------------------------------

    public function getSubmittedAt(): \DateTimeImmutable
    {
        return $this->submittedAt;
    }

    public function getTargetCode(): string
    {
        return $this->targetCode;
    }

    public function getTargetId(): string
    {
        return $this->targetId;
    }

    public function getSubmitters(): array
    {
        return $this->submitters;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
