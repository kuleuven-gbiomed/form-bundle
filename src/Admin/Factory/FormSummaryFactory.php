<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Factory;

use KUL\FormBundle\Admin\Factory\DTO\FormTemplate;
use KUL\FormBundle\Admin\Factory\DTO\Node;
use KUL\FormBundle\Admin\Factory\DTO\NodeSubType;
use KUL\FormBundle\Admin\Factory\DTO\NodeType;
use KUL\FormBundle\Admin\Factory\DTO\NodeValue;
use KUL\FormBundle\Admin\Factory\DTO\Option;
use KUL\FormBundle\Admin\Factory\DTO\Submission;
use KUL\FormBundle\Client\History\InputNodeSubmitEntry;
use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ChildrenAwareInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Input\ChoiceInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\DateInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\NumberInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\UploadInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Entity\StoredTemplateTarget;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use KUL\FormBundle\Entity\TemplateRepository;

class FormSummaryFactory
{
    public function __construct(
        private TemplateRepository $templateRepository,
        private StoredTemplateTargetRepository $storedTemplateTargetRepository,
    ) {
    }

    public function getTemplateStructure(
        string $formId,
        string $language = 'nl',
    ): FormTemplate {
        $template = $this->templateRepository->getOneById($formId);
        try {
            $label = $template->getCurrentVersion()->getInfo()->getLabel($language);
        } catch (\Exception $e) {
            $label = '';
        }
        try {
            $description = $template->getCurrentVersion()->getInfo()->getDescription($language);
        } catch (\Exception $e) {
            $description = '';
        }
        $formTemplate = new FormTemplate(
            $template->getId(),
            $label,
            $description,
            $language,
        );

        // -- parse some nodes
        $rootNodeDTO = $this->parseRootNode($template->getCurrentVersion()->getFormList()->getRootNode());
        $level = 0;
        $sequence = 0;
        $currentNumber = '0';
        /** @var CategoryNode|InputNode $child */
        foreach ($template->getCurrentVersion()->getFormList()->getRootNode()->getChildren() as $child) {
            ++$sequence;
            $rootNodeDTO->addChildNode($this->parseNode($child, $sequence, $level, $currentNumber, $language));
        }
        $formTemplate->setRootNode($rootNodeDTO);

        return $formTemplate;
    }

    private function parseRootNode(CategoryNode $node, string $language = 'nl'): Node
    {
        $type = NodeType::CATEGORY;
        $subType = NodeSubType::OTHER;
        try {
            $label = $node->getLabel($language);
        } catch (\Exception $e) {
            $label = '';
        }
        try {
            $description = $node->getDescription($language);
        } catch (\Exception $e) {
            $description = '';
        }
        $nodeDTO = new Node(
            $node->getUid(),
            $type->value,
            $subType->value,
            $label,
            $description,
            0,
            '0',
        );

        return $nodeDTO;
    }

    private function parseNode(
        CategoryNode|InputNode $node,
        int $sequence,
        int $level,
        string $parentNumber,
        string $language = 'nl',
    ): Node {
        if ('0' === $parentNumber) {
            $number = (string) $sequence;
        } else {
            $number = $parentNumber.'.'.(string) $sequence;
        }
        $hasOptions = false;
        $options = [];
        if ($node instanceof CategoryNode) {
            $type = NodeType::CATEGORY;
            $subType = NodeSubType::OTHER;
            $required = false;
        } else {
            $type = NodeType::QUESTION;
            $nodeInput = $node->getInput();
            if ($nodeInput instanceof ChoiceInput) {
                if ($nodeInput->isExpanded()) {
                    $subType = NodeSubType::CHOICE_RADIO;
                    if ($node->isCheckboxNode()) {
                        $subType = NodeSubType::CHOICE_CHECKBOX;
                    }
                } else {
                    $subType = NodeSubType::CHOICE_SELECT;
                }
                $hasOptions = true;
                $sequence = 0;
                if ($node instanceof ChildrenAwareInterface) {
                    /** @var OptionInputNode $option */
                    foreach ($node->getChildren() as $option) {
                        try {
                            $optionLabel = $option->getLabel($language);
                        } catch (\Exception $e) {
                            $optionLabel = '';
                        }
                        ++$sequence;
                        if ($option->getInput()->hasScore()) {
                            $score = (string) $option->getInput()->getScore();
                        } else {
                            $score = '';
                        }
                        $options[] = new Option(
                            $option->getUid(),
                            $optionLabel,
                            $score,
                            $sequence,
                        );
                    }
                }
            } elseif ($nodeInput instanceof TextInput) {
                $subType = NodeSubType::TEXT;
            } elseif ($nodeInput instanceof NumberInput) {
                $subType = NodeSubType::NUMBER;
            } elseif ($nodeInput instanceof DateInput) {
                $subType = NodeSubType::DATE;
            } elseif ($nodeInput instanceof UploadInput) {
                $subType = NodeSubType::FILE;
            } else {
                $subType = NodeSubType::CHART;
            }
            $required = $node->isRequired();
        }
        try {
            $label = $node->getLabel($language);
        } catch (\Exception $e) {
            $label = '';
        }
        try {
            $description = $node->getDescription($language);
        } catch (\Exception $e) {
            $description = '';
        }
        $nodeDTO = new Node(
            $node->getUid(),
            $type->value,
            $subType->value,
            $label,
            $description,
            $sequence,
            $number,
            $required
        );
        if ($hasOptions) {
            foreach ($options as $option) {
                $nodeDTO->addOption($option);
            }
        }
        if (NodeType::CATEGORY === $type) {
            if ($node instanceof ChildrenAwareInterface) {
                $newSequence = 0;
                $newLevel = $level + 1;
                /** @var CategoryNode|InputNode $child */
                foreach ($node->getChildren() as $child) {
                    ++$newSequence;
                    $nodeDTO->addChildNode($this->parseNode(
                        node: $child,
                        sequence: $newSequence,
                        level: $newLevel,
                        parentNumber: $number,
                        language: $language
                    ));
                }
            }
        }

        return $nodeDTO;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Data-fetchers (for the form reporting bundle)
    // -----------------------------------------------------------------------------------------------------------------

    public function getTemplateStructureWithSubmissions(
        string $formId,
        string $language = 'nl',
        ?string $targetCode = null,
        ?string $targetId = null,
    ): FormTemplate {
        $formTemplate = $this->getTemplateStructure($formId, $language);
        $submissions = $this->storedTemplateTargetRepository->allForTemplateId($formTemplate->getId());

        /** @var StoredTemplateTarget $submission */
        foreach ($submissions as $submission) {
            $includeSubmission = false;
            if (null !== $targetCode && null !== $targetId) {
                if ($submission->getTargetCode() === $targetCode && $submission->getTargetId() === $targetId) {
                    $includeSubmission = true;
                }
            } else {
                $includeSubmission = true;
            }
            if (false === $includeSubmission) {
                continue;
            }

            // -- collect the answers
            $submitHistory = $submission->getLatestSavedQuestionEntriesForCurrentFormList();
            $answers = $submission->getSubmittedFormValues();

            /** @var Node $rootNode */
            $rootNode = $formTemplate->getRootNode();
            foreach ($rootNode->getChildren() as $node) {
                $node = $this->collectedAnswersForQuestion($node, $answers, $submitHistory, $submission->getId());
            }

            // -- build the general submission info
            $entriesOrderedBySubmitDate = $submitHistory->getSortedBySubmitDate();
            $latestDate = new \DateTimeImmutable();
            if (count($entriesOrderedBySubmitDate) >= 1) {
                /** @var InputNodeSubmitEntry $latestEntry */
                $latestEntry = $entriesOrderedBySubmitDate[0];
                $latestDate = $latestEntry->getSubmitDate();
            }

            $submissionDTO = new Submission(
                id: $submission->getId(),
                targetCode: $submission->getTargetCode(),
                targetId: $submission->getTargetId(),
            );
            $submissionDTO->setLatestSubmissionDate($latestDate);
            foreach ($submitHistory->getIterator() as $submissionElement) {
                $submissionDTO->addSubmitter($submissionElement->getUserId());
            }

            $formTemplate->addSubmission($submissionDTO);
        }

        return $formTemplate;
    }

    private function collectedAnswersForQuestion(
        Node $node,
        array $answers,
        SubmitHistory $submissions,
        string $submissionId,
    ): Node {
        $foundAnswer = false;
        $foundSubmission = false;

        $nodeValue = '';
        $nodeValueScore = '';
        $submitter = '';
        $submittedAt = new \DateTimeImmutable();

        if (NodeType::QUESTION === $node->getType()) {
            // -- answers
            foreach ($answers as $key => $answer) {
                if (trim(mb_strtoupper($node->getId())) === trim(mb_strtoupper($key))) {
                    if (false === (null === $answer)) {
                        $nodeValue = $answer;
                        // -- get the score related to the answer
                        if (NodeSubType::CHOICE_RADIO === $node->getSubType()
                            || NodeSubType::CHOICE_CHECKBOX === $node->getSubType()
                            || NodeSubType::CHOICE_SELECT === $node->getSubType()
                        ) {
                            /** @var Option $option */
                            foreach ($node->getOptions() as $option) {
                                if ($option->getId() === $answer) {
                                    $nodeValueScore = $option->getScore();
                                    break;
                                }
                            }
                        }
                        $foundAnswer = true;
                    }
                    break;
                }
            }
            // -- submissions
            foreach ($submissions->getIterator() as $submission) {
                if ($submission->getNodeUid() === $node->getId()) {
                    $foundSubmission = true;
                    $submitter = $submission->getUserId();
                    $submittedAt = $submission->getSubmitDate();
                    break;
                }
            }
        }

        if ($foundAnswer && $foundSubmission) {
            $nodeValueDTO = new NodeValue(
                submissionId: $submissionId,
                submitter: $submitter,
                submittedAt: $submittedAt,
                value: $nodeValue,
                valueScore: $nodeValueScore,
            );
            $node->addSubmission($nodeValueDTO);
        }

        if (NodeType::CATEGORY === $node->getType()) {
            foreach ($node->getChildren() as $child) {
                $child = $this->collectedAnswersForQuestion($child, $answers, $submissions, $submissionId);
            }
        }

        return $node;
    }

    public function getTemplateStructureWithSubmissionFor(
        string $formId,
        string $targetCode,
        string $targetId,
        string $language = 'nl',
    ): FormTemplate {
        return $this->getTemplateStructureWithSubmissions($formId, $language, $targetCode, $targetId);
    }
}
