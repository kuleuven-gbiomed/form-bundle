<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Event;

readonly class FormTemplateCopied implements FormTemplateEvent
{
    public function __construct(
        private string $sourceFormTemplateId,
        private string $targetFormTemplateId,
    ) {
    }

    public function getTemplateId(): string
    {
        return $this->targetFormTemplateId;
    }

    public function getTargetFormTemplateId(): string
    {
        return $this->targetFormTemplateId;
    }

    public function getSourceFormTemplateId(): string
    {
        return $this->sourceFormTemplateId;
    }
}
