<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Event;

readonly class FormTemplateImported implements FormTemplateEvent
{
    public function __construct(private string $templateId)
    {
    }

    public function getTemplateId(): string
    {
        return $this->templateId;
    }
}
