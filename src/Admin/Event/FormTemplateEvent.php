<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Event;

interface FormTemplateEvent
{
    public function getTemplateId(): string;
}
