<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Facade;

use KUL\FormBundle\Admin\Factory\DTO\FormTemplate;
use KUL\FormBundle\Admin\Factory\FormSummaryFactory;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Entity\StoredTemplateTargetRepository;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;

readonly class FormBundleFacade
{
    public function __construct(
        private TemplateRepository $templateRepository,
        private StoredTemplateTargetRepository $storedTemplateTargetRepository,
    ) {
    }

    public function getTemplateById(string $id): Template
    {
        return $this->templateRepository->getOneById($id);
    }

    public function getRolesWithWriteAccessForTemplate(string $formTemnplateId, string $stepId): array
    {
        $roles = [];
        $template = $this->templateRepository->getOneById($formTemnplateId);
        $formList = $template->getCurrentVersion()->getFormList();
        $workFlow = $template->getCurrentVersion()->getWorkFlow();
        $step = $workFlow->getOneStepByUid($stepId);
        $questions = new NodeCollection($formList->getFlattenedReachableParentalInputNodes()->toArray());
        $nodes = $questions;

        foreach ($template->getHierarchicalRolesThatCouldParticipate() as $role) {
            if ($nodes->hasWritableForRoleInStep($role, $step)) {
                $roles[] = $role;
            }
        }
        $roles = array_unique($roles);

        return $roles;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Datafetchers (for the form reporting bundle)
    // -----------------------------------------------------------------------------------------------------------------

    public function getTemplateStructure(
        string $formId,
        string $language = 'nl',
    ): FormTemplate {
        $factory = new FormSummaryFactory(
            $this->templateRepository,
            $this->storedTemplateTargetRepository,
        );

        return $factory->getTemplateStructure($formId, $language);
    }

    public function getTemplateStructureWithSubmissions(
        string $formId,
        string $language = 'nl',
    ): FormTemplate {
        $factory = new FormSummaryFactory(
            $this->templateRepository,
            $this->storedTemplateTargetRepository,
        );

        return $factory->getTemplateStructureWithSubmissions($formId, $language);
    }

    public function getTemplateStructureWithSubmissionFor(
        string $formId,
        string $targetCode,
        string $targetId,
        string $language = 'nl',
    ): FormTemplate {
        $factory = new FormSummaryFactory(
            $this->templateRepository,
            $this->storedTemplateTargetRepository,
        );

        return $factory->getTemplateStructureWithSubmissionFor($formId, $targetCode, $targetId, $language);
    }
}
