<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\RemoveFormTemplate;

use KUL\FormBundle\Admin\Event\PreFormTemplateRemoved;
use KUL\FormBundle\Client\Revert\ResetService;
use KUL\FormBundle\Entity\TemplateRepository;
use KUL\FormBundle\FormBuilder\Repository\CommandHistoryEntryRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class RemoveFormTemplateHandler
{
    public function __construct(
        private MessageBusInterface $formBundleEventBus,
        private TemplateRepository $templateRepository,
        private FormRepository $formRepository,
        private CommandHistoryEntryRepository $commandHistoryEntryRepository,
        private ResetService $resetService,
    ) {
    }

    public function __invoke(RemoveFormTemplate $removeFormTemplate): void
    {
        $template = $this->templateRepository->getOneById($removeFormTemplate->getTemplateId());

        // 0 dispatch a pre removed message, allows apps that have template entity references to safely remove them.
        $this->formBundleEventBus->dispatch(new PreFormTemplateRemoved($template->getId()));

        // --------------------------------------------------------------------------------
        // STEP 1: Reset (remove) all filled-in forms
        // --------------------------------------------------------------------------------
        $this->resetService->resetAllStoredTemplateTargetFormsForTemplate($template);

        // --------------------------------------------------------------------------------
        // STEP 2: Remove FormBuilder Data
        // --------------------------------------------------------------------------------
        // 2.1 History: delete history entries related to $form
        $this->commandHistoryEntryRepository->deleteEntriesByFormUid($template->getId());

        // 2.2 Form Builder Tables: Via the cascading configuration in the database,
        // all related rows in other tables will be also deleted...
        $form = $this->formRepository->findByUid($template->getId());
        if (null !== $form) {
            $this->formRepository->deleteForm($form);
        }

        // --------------------------------------------------------------------------------
        // STEP 3: Remove Template Entity
        // --------------------------------------------------------------------------------
        $this->templateRepository->remove($template);
    }
}
