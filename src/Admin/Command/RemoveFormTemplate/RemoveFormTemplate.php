<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\RemoveFormTemplate;

use KUL\FormBundle\Admin\Command\FormTemplateCommand;

/**
 * Remove a given template with all its submitted forms and form-builder data.
 */
readonly class RemoveFormTemplate implements FormTemplateCommand
{
    public function __construct(
        private string $templateId,
    ) {
    }

    public function getTemplateId(): string
    {
        return $this->templateId;
    }
}
