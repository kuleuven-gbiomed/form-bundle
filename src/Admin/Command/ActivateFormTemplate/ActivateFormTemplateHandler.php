<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\ActivateFormTemplate;

use KUL\FormBundle\Admin\Event\FormTemplateActivated;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class ActivateFormTemplateHandler
{
    public function __construct(
        private MessageBusInterface $formBundleEventBus,
        private TemplateRepository $templateRepository,
    ) {
    }

    public function __invoke(ActivateFormTemplate $command): void
    {
        $template = $this->templateRepository->getOneById($command->getTemplateId());

        if (!$template->isActive()) {
            $template->publishNewCurrentVersion();
            $this->templateRepository->save($template);
        }

        $this->formBundleEventBus->dispatch(new FormTemplateActivated($template->getId()));
    }
}
