<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\ImportFormTemplate;

use KUL\FormBundle\Admin\Event\FormTemplateImported;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Factory\CalculatedPeriodTemplateFactory;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class ImportFormTemplateHandler
{
    public function __construct(
        private MessageBusInterface $formBundleEventBus,
        private TemplateRepository $templateRepository,
        private CalculatedPeriodTemplateFactory $calculatedTemplateFactory,
    ) {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(ImportFormTemplate $command): void
    {
        $template = $this->templateRepository->find($command->getTemplateId());
        if ($template instanceof Template) {
            throw new \Exception('Template with id:'.$command->getTemplateId().' already exists.');
        }

        $this->importFormTemplate($command);
        $this->formBundleEventBus->dispatch(new FormTemplateImported($command->getTemplateId()));
    }

    private function importFormTemplate(ImportFormTemplate $command): void
    {
        /** @var array<string,string> $titlesKeyValue */
        $titlesKeyValue = [];
        foreach ($command->getTitles() as $titleObject) {
            $titlesKeyValue[(string) $titleObject['language']] = (string) $titleObject['title'];
        }
        $locale = $this->getFirstLocale($titlesKeyValue);

        $template = $this->calculatedTemplateFactory->createTemplate(
            templateUid: $command->getTemplateId(),
            formName: 'imported form',
            targetingType: $command->getTargetingType(),
            stepDays: [300],
            relevantRoles: $command->getRoles()
        );

        /* @var WorkingVersion $workingVersion */
        $workingVersion = $template->getWorkingVersion();

        $formInfoArray = json_decode($command->getInfo(), true);
        $formInfo = FormInfo::fromJsonDecodedArray($formInfoArray);

        // --------------------------------------------------------------------------------
        // -- set the form info
        // --------------------------------------------------------------------------------

        $formInfo = FormInfo::fromLocalizedStrings(
            LocalizedRequiredString::fromLocaleTranslations($titlesKeyValue),
            $formInfo->getLocalizedDescription(),
            $formInfo->getRoleLocalizedDescriptions()
        );

        $workingVersion = $workingVersion->withInfo($formInfo);

        // --------------------------------------------------------------------------------
        // -- set the workflow
        // --------------------------------------------------------------------------------

        $workflowArray = json_decode($command->getWorkflow(), true, 512, \JSON_THROW_ON_ERROR);
        $workflow = WorkFlow::fromJsonDecodedArray($workflowArray);
        $workingVersion = $workingVersion->withWorkFlow($workflow);

        // --------------------------------------------------------------------------------
        // -- create the FormList with full permissions for the admin roles
        // --------------------------------------------------------------------------------

        $formListArray = json_decode($command->getFormList(), true, 512, \JSON_THROW_ON_ERROR);
        $formList = FormList::fromJsonDecodedArray($formListArray);
        $workingVersion = $workingVersion->withFormList($formList);

        // --------------------------------------------------------------------------------
        // -- update the template
        // --------------------------------------------------------------------------------

        $template->updateWorkingVersion($workingVersion);

        // --------------------------------------------------------------------------------
        // -- publish the working version to a current version and deactivate the template
        // --------------------------------------------------------------------------------

        $template->publishNewCurrentVersion();
        $template->deactivate();
        $this->templateRepository->save($template);
    }

    // ———————————————————————————————————————————————————————————————————————————————————
    // Helper methods
    // ———————————————————————————————————————————————————————————————————————————————————

    private function getFirstLocale(array $titles): string
    {
        if (0 === count($titles)) {
            return 'nl';
        }

        return array_key_first($titles);
    }
}
