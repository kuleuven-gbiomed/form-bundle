<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\ImportFormTemplate;

use KUL\FormBundle\Admin\Command\FormTemplateCommand;

readonly class ImportFormTemplate implements FormTemplateCommand
{
    public function __construct(
        private string $templateId,
        private string $targetingType,
        private array $roles,
        private array $titles,
        private string $info,
        private string $workflow,
        private string $formList,
        private array $adminRoles = ['ROLE_DEVELOPER'],
    ) {
    }

    // -------------------------------------------------------------------
    // Getters
    // -------------------------------------------------------------------

    public function getTemplateId(): string
    {
        return $this->templateId;
    }

    public function getTargetingType(): string
    {
        return $this->targetingType;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getTitles(): array
    {
        return $this->titles;
    }

    public function getInfo(): string
    {
        return $this->info;
    }

    public function getWorkflow(): string
    {
        return $this->workflow;
    }

    public function getFormList(): string
    {
        return $this->formList;
    }

    public function getAdminRoles(): array
    {
        return $this->adminRoles;
    }
}
