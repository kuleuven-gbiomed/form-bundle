<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\RemoveFormTemplateSubmissions;

use KUL\FormBundle\Admin\Command\FormTemplateCommand;

/**
 * Remove all submitted forms for a given template but do not delete the form-builder data (if any),
 * and the template itself.
 */
readonly class RemoveFormTemplateSubmissions implements FormTemplateCommand
{
    public function __construct(
        private string $templateId,
    ) {
    }

    public function getTemplateId(): string
    {
        return $this->templateId;
    }
}
