<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\RemoveFormTemplateSubmissions;

use KUL\FormBundle\Admin\Event\FormTemplateSubmissionsRemoved;
use KUL\FormBundle\Client\Revert\ResetService;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class RemoveFormTemplateSubmissionsHandler
{
    public function __construct(
        private TemplateRepository $templateRepository,
        private MessageBusInterface $formBundleEventBus,
        private ResetService $resetService,
    ) {
    }

    public function __invoke(RemoveFormTemplateSubmissions $removeFormTemplateSubmissions): void
    {
        $template = $this->templateRepository->getOneById($removeFormTemplateSubmissions->getTemplateId());
        $this->resetService->resetAllStoredTemplateTargetFormsForTemplate($template);
        $this->formBundleEventBus->dispatch(new FormTemplateSubmissionsRemoved($template->getId()));
    }
}
