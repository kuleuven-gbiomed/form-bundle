<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\ChangeTitle;

use KUL\FormBundle\Admin\Event\TitleChanged;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class ChangeTitleHandler
{
    public function __construct(
        private MessageBusInterface $formBundleEventBus,
        private TemplateRepository $templateRepository,
    ) {
    }

    public function __invoke(ChangeTitle $command): void
    {
        $template = $this->templateRepository->getOneById($command->getTemplateId());
        $templateIsActive = $template->isActive();

        /* @var WorkingVersion $workingVersion */
        $workingVersion = $template->getWorkingVersion();

        /** @var FormInfo $originalInfo */
        $originalInfo = $workingVersion->getInfo();

        /** @var array<string,string> $titlesKeyValue */
        $titlesKeyValue = [];
        foreach ($command->getTitles() as $titleObject) {
            $titlesKeyValue[(string) $titleObject['language']] = (string) $titleObject['title'];
        }

        $formInfo = FormInfo::fromLocalizedStrings(
            LocalizedRequiredString::fromLocaleTranslations($titlesKeyValue),
            $originalInfo->getLocalizedDescription(),
            $originalInfo->getRoleLocalizedDescriptions(),
        );
        $workingVersion = $workingVersion->withInfo($formInfo);
        $template->updateWorkingVersion($workingVersion);

        $currentVersion = $template->getCurrentVersion();
        if ($workingVersion->getFormList()->isEmpty()) {
            $currentVersion = $currentVersion->withInfo($formInfo);
            $template->updateCurrentVersion($currentVersion);
        } else {
            $template->publishNewCurrentVersion();
        }
        if (false === $templateIsActive) {
            $template->deactivate();
        }
        $this->templateRepository->save($template);

        $this->formBundleEventBus->dispatch(new TitleChanged($template->getId()));
    }
}
