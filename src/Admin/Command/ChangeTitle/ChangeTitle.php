<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\ChangeTitle;

use KUL\FormBundle\Admin\Command\FormTemplateCommand;

readonly class ChangeTitle implements FormTemplateCommand
{
    /**
     * @param array<string, string> $titles
     */
    public function __construct(
        private string $templateId,
        private array $titles,
    ) {
    }

    public function getTemplateId(): string
    {
        return $this->templateId;
    }

    public function getTitles(): array
    {
        return $this->titles;
    }

    public function getLanguagesFromTitles(): array
    {
        return array_keys($this->titles);
    }
}
