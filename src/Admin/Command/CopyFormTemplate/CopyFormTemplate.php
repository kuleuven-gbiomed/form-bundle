<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\CopyFormTemplate;

use KUL\FormBundle\Admin\Command\FormTemplateCommand;

readonly class CopyFormTemplate implements FormTemplateCommand
{
    /**
     * @param array<string, string> $titles
     */
    public function __construct(
        private string $sourceFormTemplateId,
        private string $targetFormTemplateId,
        private array $titles,
    ) {
    }

    public function getTemplateId(): string
    {
        return $this->targetFormTemplateId;
    }

    public function getTitles(): array
    {
        return $this->titles;
    }

    public function getSourceFormTemplateId(): string
    {
        return $this->sourceFormTemplateId;
    }

    public function getTargetFormTemplateId(): string
    {
        return $this->targetFormTemplateId;
    }
}
