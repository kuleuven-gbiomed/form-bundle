<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\CopyFormTemplate;

use KUL\FormBundle\Admin\Event\FormTemplateCopied;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Exception\TemplateException;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class CopyFormTemplateHandler
{
    public function __construct(
        private MessageBusInterface $formBundleEventBus,
        private TemplateRepository $templateRepository,
    ) {
    }

    /**
     * @throws \JsonException
     * @throws LocalizedStringException
     * @throws TemplateException
     */
    public function __invoke(CopyFormTemplate $command): void
    {
        $originalTemplate = $this->templateRepository->getOneById($command->getSourceFormTemplateId());

        if (false === $originalTemplate->hasCurrentVersion()) {
            throw new \InvalidArgumentException('Original template has no current version and was never published');
        }

        // --------------------------------------------------------------------------------
        // -- duplicate the original template
        // --------------------------------------------------------------------------------

        $newTemplate = new Template(
            id: $command->getTargetFormTemplateId(),
            targetingType: $originalTemplate->getTargetingType(),
            workingVersion: $originalTemplate->getCurrentVersion()->toWorkingVersion(),
            hierarchicalRolesThatCouldParticipate: $originalTemplate->getHierarchicalRolesThatCouldParticipate(),
            rolesAllowedToCopyAnswers: new RoleCollection(),
            rolesWithDisabledAutoTemporarySaving: $originalTemplate->getRolesWithDisabledAutoTemporarySaving(),
            rolesAllowedToExport: $originalTemplate->getRolesAllowedToExport(),
            rolesForAutoQuestionWriteAccessInAdminSide: $originalTemplate->getRolesForAutomaticQuestionWriteAccessBuildingInAdminSide(),
            usePageBreaks: $originalTemplate->isPageBreaksEnabled(),
            enableReadOnlyChoiceAnswerOptionsToggling: $originalTemplate->isReadOnlyChoiceAnswerOptionsTogglingEnabled(),
            enableAutoTemporarySaving: $originalTemplate->isAutoTemporarySavingEnabled(),
            showPositionNumbers: $originalTemplate->isShowPositionNumbers(),
        );

        // --------------------------------------------------------------------------------
        // -- set the new titles
        // --------------------------------------------------------------------------------

        $workingVersion = $newTemplate->getWorkingVersion();

        /** @var FormInfo $originalInfo */
        $originalInfo = $workingVersion->getInfo();

        /** @var array<string,string> $titlesKeyValue */
        $titlesKeyValue = [];
        foreach ($command->getTitles() as $titleObject) {
            $titlesKeyValue[(string) $titleObject['language']] = (string) $titleObject['title'];
        }

        $formInfo = FormInfo::fromLocalizedStrings(
            LocalizedRequiredString::fromLocaleTranslations($titlesKeyValue),
            $originalInfo->getLocalizedDescription(),
            $originalInfo->getRoleLocalizedDescriptions(),
        );
        $workingVersion = $workingVersion->withInfo($formInfo);

        // --------------------------------------------------------------------------------
        // -- set working version and publish
        // --------------------------------------------------------------------------------

        $newTemplate->updateWorkingVersion($workingVersion);

        if (false === $newTemplate->canPublishNewCurrentVersion()) {
            throw new \Exception('Can not publish the copied template. The original current version of the source template was converted to a working version but it contains the following errors: '.implode(' || ', $newTemplate->getInvalidPublishMessages()->getMessages()));
        }

        $newTemplate->publishNewCurrentVersion();

        if ($originalTemplate->isDeactivated()) {
            $newTemplate->deactivate();
        }

        $this->templateRepository->save($newTemplate);

        $this->formBundleEventBus->dispatch(new FormTemplateCopied(
            $command->getSourceFormTemplateId(),
            $command->getTargetFormTemplateId(),
        ));
    }
}
