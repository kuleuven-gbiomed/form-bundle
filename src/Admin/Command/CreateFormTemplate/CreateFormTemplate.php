<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\CreateFormTemplate;

use KUL\FormBundle\Admin\Command\FormTemplateCommand;

readonly class CreateFormTemplate implements FormTemplateCommand
{
    public const FIXED_STEPS = 'FIXED_STEPS';
    public const RELATIVE_STEPS = 'RELATIVE_STEPS';

    /**
     * @param array<string, string> $titles
     * @param array<string>         $adminRoles
     * @param array<string>         $roles
     */
    public function __construct(
        private string $templateId,
        private string $targetingType,
        private array $titles,
        private array $roles,
        private int $numberOfWorkflowSteps = 2,
        private string $workflowType = self::RELATIVE_STEPS,
        private array $adminRoles = ['ROLE_DEVELOPER'],
    ) {
    }

    // -------------------------------------------------------------------
    // Getters
    // -------------------------------------------------------------------

    public function getTemplateId(): string
    {
        return $this->templateId;
    }

    public function getTargetingType(): string
    {
        return $this->targetingType;
    }

    public function getTitles(): array
    {
        return $this->titles;
    }

    public function getNumberOfWorkflowSteps(): int
    {
        return $this->numberOfWorkflowSteps;
    }

    public function getWorkflowType(): string
    {
        return $this->workflowType;
    }

    public function getAdminRoles(): array
    {
        return $this->adminRoles;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }
}
