<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\CreateFormTemplate;

use KUL\FormBundle\Admin\Event\FormTemplateCreated;
use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Factory\CalculatedPeriodTemplateFactory;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

readonly class CreateFormTemplateHandler
{
    public function __construct(
        private MessageBusInterface $formBundleEventBus,
        private TemplateRepository $templateRepository,
        private TranslatorInterface $translator,
        private CalculatedPeriodTemplateFactory $calculatedTemplateFactory,
    ) {
    }

    public function __invoke(CreateFormTemplate $command): void
    {
        $template = $this->templateRepository->find($command->getTemplateId());
        if ($template instanceof Template) {
            throw new \Exception('Template with id:'.$command->getTemplateId().' already exists.');
        }
        $this->createFormTemplate($command);
        $this->formBundleEventBus->dispatch(new FormTemplateCreated($command->getTemplateId()));
    }

    private function createFormTemplate(CreateFormTemplate $command): void
    {
        /** @var array<string,string> $titlesKeyValue */
        $titlesKeyValue = [];
        foreach ($command->getTitles() as $titleObject) {
            $titlesKeyValue[(string) $titleObject['language']] = (string) $titleObject['title'];
        }

        $locale = $this->getFirstLocale($titlesKeyValue);

        $template = $this->calculatedTemplateFactory->createTemplate(
            templateUid: $command->getTemplateId(),
            formName: 'new form',
            targetingType: $command->getTargetingType(),
            stepDays: [300],
            relevantRoles: $command->getRoles()
        );

        /* @var WorkingVersion $workingVersion */
        $workingVersion = $template->getWorkingVersion();

        // --------------------------------------------------------------------------------
        // -- set the form info
        // --------------------------------------------------------------------------------

        $formInfo = FormInfo::fromLocalizedStrings(
            LocalizedRequiredString::fromLocaleTranslations($titlesKeyValue),
            LocalizedOptionalString::createEmpty(),
            RoleLocalizedOptionalStringCollection::createEmpty()
        );
        $workingVersion = $workingVersion->withInfo($formInfo);

        // --------------------------------------------------------------------------------
        // -- set the workflow
        // --------------------------------------------------------------------------------

        $workflow = match ($command->getWorkflowType()) {
            CreateFormTemplate::FIXED_STEPS => $this->getDefaultWorkflowUsingFixedDates($command->getNumberOfWorkflowSteps(), $locale),
            default => $this->getDefaultWorkflowUsingCalculatedDates($command->getNumberOfWorkflowSteps(), $locale),
        };

        $workingVersion = $workingVersion->withWorkFlow($workflow);

        // --------------------------------------------------------------------------------
        // -- create the FormList with full permissions for the admin roles
        // --------------------------------------------------------------------------------

        $stepIds = $workingVersion->getWorkFlow()->getSteps()->getStepUids();
        $flowPermission = FlowPermission::createEmpty();
        foreach ($stepIds as $stepId) {
            foreach ($command->getAdminRoles() as $role) {
                $flowPermission = $flowPermission->withFullWritePermissionsForRoleInStepUid($role, $stepId);
            }
        }

        $question = TextInputNode::fromUidAndDependencies(
            Uuid::uuid4()->toString(),
            TextInput::fromParameters(
                false,
                TextInput::DISPLAY_MODE_REGULAR,
                LocalizedOptionalString::fromLocaleTranslations([$locale => 'placeholder']),
                false,
                TextInput::DEFAULT_LENGTH_MIN,
                TextInput::DEFAULT_LENGTH_MAX_TEXT,
                []
            ),
            $flowPermission,
            $this->createInfoWithRequiredLabel(
                [$locale => 'Sleep een categorie of vraag op deze vraag om te starten...'],
                [$locale => '']
            ),
            false,
            TextInputNode::DESCRIPTION_DISPLAY_MODE_BOX,
        );
        $formList = $workingVersion->getFormList();

        // --------------------------------------------------------------------------------
        // -- update the template
        // --------------------------------------------------------------------------------

        $template->updateWorkingVersion(
            $workingVersion->withFormList(
                $formList->withNewNodeAddedToParentCategory($question, $formList->getRootNode())
            )
        );

        // --------------------------------------------------------------------------------
        // -- publish the working version to a current version and deactivate the template
        // --------------------------------------------------------------------------------

        $template->publishNewCurrentVersion();
        $template->deactivate();
        $this->templateRepository->save($template);
    }

    // ———————————————————————————————————————————————————————————————————————————————————
    // Helper methods
    // ———————————————————————————————————————————————————————————————————————————————————

    private function getFirstLocale(array $titles): string
    {
        if (0 === count($titles)) {
            return 'nl';
        }

        return array_key_first($titles);
    }

    /**
     * @param array<string, string> $labelTranslations
     * @param array<string, string> $descriptionTranslations
     *
     * @throws LocalizedStringException
     */
    private function createInfoWithRequiredLabel(array $labelTranslations, array $descriptionTranslations = []): LocalizedInfoWithRequiredLabel
    {
        return LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            LocalizedRequiredString::fromLocaleTranslations($labelTranslations),
            LocalizedOptionalString::fromLocaleTranslations($descriptionTranslations),
            RoleLocalizedOptionalStringCollection::createEmpty()
        );
    }

    private function getDefaultWorkflowUsingCalculatedDates(int $numberOfSteps, string $locale): WorkFlow
    {
        $stepsArray = [];
        for ($step = 0; $step < $numberOfSteps; ++$step) {
            $start = ($step - 1) * 100;
            $end = $start + 100;
            $uuid = Uuid::uuid4()->toString();
            $stepsArray[] = StepWithCalculatedDateStart::fromUidAndMinimalDependencies(
                $uuid,
                StepCalculatedPeriod::fromCalculatingAmounts($start, $end),
                $this->buildStepInfo($step, $locale),
                false,
            );
        }

        return WorkFlow::fromSteps(new StepWithCalculatedDateStartCollection($stepsArray));
    }

    private function getDefaultWorkflowUsingFixedDates(int $numberOfSteps, string $locale): WorkFlow
    {
        $stepsArray = [];
        $now = new \DateTimeImmutable();
        for ($step = 0; $step < $numberOfSteps; ++$step) {
            $uuid = Uuid::uuid4()->toString();
            if (0 === $step) {
                $start = $now;
            } else {
                $stepString = (string) $step;
                $start = $now->add(new \DateInterval('P'.$stepString.'M'));
            }
            $endStepString = (string) ($step + 1);
            $end = $start->add(new \DateInterval('P'.$endStepString.'M'));
            $stepsArray[] = StepWithFixedDateStart::fromUidAndMinimalDependencies(
                $uuid,
                StepFixedPeriod::fromDates($start, $end),
                $this->buildStepInfo($step, $locale),
                false
            );
        }

        return WorkFlow::fromSteps(new StepWithFixedDateStartCollection($stepsArray));
    }

    private function buildStepInfo(int $step, string $locale): LocalizedInfoWithRequiredLabel
    {
        $stepNumber = (string) ($step + 1);

        return LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            LocalizedRequiredString::fromLocaleTranslations([
                $locale => $this->translator->trans('adminManage.general.step', [], 'kulFormAdminManage', $locale).' '.$stepNumber,
            ]),
            LocalizedOptionalString::createEmpty(),
            RoleLocalizedOptionalStringCollection::createEmpty()
        );
    }
}
