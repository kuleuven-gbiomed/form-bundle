<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\DeActivateFormTemplate;

use KUL\FormBundle\Admin\Event\FormTemplateDeActivated;
use KUL\FormBundle\Entity\TemplateRepository;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class DeActivateFormTemplateHandler
{
    public function __construct(
        private MessageBusInterface $formBundleEventBus,
        private TemplateRepository $templateRepository,
    ) {
    }

    public function __invoke(DeActivateFormTemplate $command): void
    {
        $template = $this->templateRepository->getOneById($command->getTemplateId());

        if ($template->isActive()) {
            $template->deactivate();
            $this->templateRepository->save($template);
        }

        $this->formBundleEventBus->dispatch(new FormTemplateDeActivated($template->getId()));
    }
}
