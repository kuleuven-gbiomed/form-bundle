<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command\DeActivateFormTemplate;

use KUL\FormBundle\Admin\Command\FormTemplateCommand;

readonly class DeActivateFormTemplate implements FormTemplateCommand
{
    public function __construct(
        private string $templateId,
    ) {
    }

    public function getTemplateId(): string
    {
        return $this->templateId;
    }
}
