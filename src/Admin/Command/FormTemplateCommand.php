<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Command;

interface FormTemplateCommand
{
    public function getTemplateId(): string;
}
