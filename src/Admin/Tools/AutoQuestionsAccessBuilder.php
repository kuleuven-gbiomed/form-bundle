<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Tools;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Class AutoQuestionsAccessBuilder.
 *
 * helper object to assist admins in building access rights on questions in a @see FormList
 * for a template's working version @see TemplateInterface::getWorkingVersion() in the admin side.
 */
final class AutoQuestionsAccessBuilder
{
    /**
     * helper for admin-side, returning an new @see FormList instance based on given formList, with updated question
     * write access rights for given role, based on & timed by given @see WorkFlow .
     * this allow allows admin-side template builders to auto-assign write access on all questions in
     * given @see FormList (if any) for the given role, as soon as (= from the first step in the given @see WorkFlow
     * for which) any other role than the given role has been assigned (read or write) access on those questions, and
     * with continued write access to those questions for from that moment on (= for all following, remaining steps).
     *
     * NOTE: although not limiting to admin-roles only, this helps to set write access for admins (admin-role) on every
     * question from the moment the same admins have set (read or write) access for other roles on questions during the
     * template building process in the admin-side. because it is custom and highly recommended that admins should get
     * write access to questions from the moment they set (read or write) access for non-admin roles, so they can always
     * update answers or give answers if a non-admin has failed to give an answer when they were supposed to to that.
     * this write access is set as soon as a question has ANY access - so read or write access - for other role(s)
     * and not just as soon as only write access is set. this is because it helps to make sure the admin will have
     * write access even if a question only grants read access for other user during the whole workflow, reasonably
     * assuming that setting read access to a question implies that some role either had write access in a previous step
     * or the admin is the one supposed to set write access for himself on a question that is for admins only.
     *
     * IMPORTANT: all this is based on a human logic for making sure relevant roles - usually admin roles - get the
     * ability to update/correct answers if needed when other lower level users have neglected giving answers or have
     * not been given the right to update once a question was answered. if a question has not been set with any access
     * for any role at all, that question will be skipped here! resulting in no access (no write no read) for
     * neither the given role and any other roles at all! that means that question will be useless. no-one will
     * be able to even see that question, let alone answer it. that will also result in the template to which this
     * working version belongs, will not publishable ( @see WorkingVersionFormListValidator ) because every question
     * must grant write access to at least one role in at least one step. even if a question is read-only for
     * 'normal' roles or not even read-only like an @see ScorableInputNode::operatesAsGlobalScoreNode(), then still
     * the domain decision is that then at least one role - i.e. an admin role - should get write access at some point
     * Global score questions (@see ScorableInputNode::operatesAsGlobalScoreNode() ) sometimes are deliberately set with
     * no (write) access for ANY role, so the calculated score is non-updatable by anyone, once auto-calculated on some
     * successful submit of its dependencies. yet it is still wise - AND required for publish - to set write access
     * for admin-roles on those global score questions. usually in the step when it is certain that that global score w
     * ill have been auto-calculated in the previous (or same) step.
     *
     * for more info, @see TemplateInterface::getRolesForAutomaticQuestionWriteAccessBuildingInAdminSide()
     * write access: @see FlowPermission::isStepRoleWriteAccessGranted()
     *
     * WARNING: this will reset all access for given role on all questions in given formList: as soon as a step is
     * found that grants read access to at least one role - including if that role turns out to be the given role -
     * this method will reset the access for given role to full write access in that step and in all following steps,
     * regardless of any previously access that was or was not granted for given role in all those steps.
     * WARNING: templates undergo changes on questions and steps when being build (or updated) in admin side. this
     * method is just a helper method and you may need to call this method again after changes to steps (e.g. new
     * step is added ) and/or questions (e.g. new question is added) or - even better - leave the admins making
     * the workingVersion the opportunity to make alterations to the access set by this method.
     *
     * ADVISE: in the admin-side, the admin could be/remain able to set access for their own roles in the same way
     * as for any role. so also before or after they set write access for any other role, or set access only for themselves and
     * no (write) access for any other role, or implicitly set no (write) access for their own roles, or only read
     * access for themselves, etc. hence using this method without giving admins the opportunity to customize access for
     * their own admin roles could be ill-wanted behaviour. In general, be careful to use this method or use it to
     * 'pre-set' the access for admin-roles but still give them opportunity to alter it in the access matrix in admin-side,
     * especially considering that admins may update working versions at any time (including adding/removing admin
     * and non-admin roles).
     *
     * note: readOnlyView nodes never grant write access, only read access, but the given (admin) role should get that
     * read access as soon as another non-admin role gets read access.
     *
     * @throws \Exception
     */
    public static function createUpdatedFormListWithAutoAssignedTimedWriteAccessByWorkFlowForRole(
        FormList $formList,
        WorkFlow $workFlow,
        Role $role,
        bool $assignAccessForReadOnlyViewNodes = true,
    ): FormList {
        // start from an exact copy from given formList (makes sure we always return a new formList instance for method
        // consistency if no update will be possible, because any question update on formList will return new instance)
        $newFormList = FormList::fromRootNode($formList->getRootNode());

        // get workflow steps. skip if workflow has no steps  (yet) for quick return to avoid needless question loops
        // immediately sort steps by logical flow (workflow sorts it own steps by default on construct, but you never know...)
        $sortedSteps = $workFlow->getSteps()->getSortedByAscendingFlow();
        if ($sortedSteps->isEmpty()) {
            return $newFormList;
        }

        // get questions usable for assigning write access: at least one role must have at least read access in at least
        // one step, making the question 'in-use' by at least one role, so that we can then set the higher ranked write
        // access for given role later..
        $questions = $newFormList->getFlattenedParentalInputNodes()->getReadableInAtLeastOneStep();
        // loop over the questions with read access somewhere in the workflow for at least one role and as soon as we
        // find the step that grants read access for the first time, start granting write access for given role to this
        // step ,and all steps that follows, based on the workflow sorted by logical order
        /** @var InputNode $question */
        foreach ($questions as $question) {
            $flowPermission = $question->getFlowPermission();
            $hitFirstStepWithReadAccess = false;

            /** @var StepInterface $step */
            foreach ($sortedSteps as $step) { // loop in a loop, damned ;)
                // if first step with read access was found in the workflow, grant full write access for any that follows
                if ($hitFirstStepWithReadAccess) {
                    $flowPermission = $flowPermission->withFullWritePermissionsForRoleInStepUid($role->getName(), $step->getUid());
                    $question->updateFlowPermission($flowPermission);
                    continue;
                }

                // if step is first step with some access for some role, add permission for given role and mark first step found
                if ($flowPermission->isStepReadAccessGrantedForAtLeastOneRole($step->getUid())) {
                    $flowPermission = $flowPermission->withFullWritePermissionsForRoleInStepUid($role->getName(), $step->getUid());
                    $question->updateFlowPermission($flowPermission);
                    $hitFirstStepWithReadAccess = true;
                }
            }
        }

        // return a new, updated version of the formList in this workingVersion with the updated questions
        foreach ($questions as $question) {
            // we know each question exists in formList, so no need to check existence in formList at this point
            $newFormList = $newFormList->withNodeUpdated($question);
        }

        if ($assignAccessForReadOnlyViewNodes) {
            // same for readOnlyView nodes: except that we grant only read access: at least one role must have read access
            // in at least one step, making the readOnlyView node 'in-use' by at least one role, so that we can then set
            // read access for given role later.
            $readOnlyNodes = $newFormList->getFlattenedReadOnlyViewNodes()->getReadableInAtLeastOneStep();
            /** @var InputNode $readOnlyNode */
            foreach ($readOnlyNodes as $readOnlyNode) {
                $flowPermission = $readOnlyNode->getFlowPermission();
                $hitFirstStepWithReadAccess = false;

                /** @var StepInterface $step */
                foreach ($sortedSteps as $step) {
                    if ($hitFirstStepWithReadAccess) {
                        // readOnlyView nodes don't grant write access to anyone -> admins get read access
                        $flowPermission = $flowPermission->withReadOnlyPermissionsForRoleInStepUid($role->getName(), $step->getUid());
                        $readOnlyNode->updateFlowPermission($flowPermission);
                        $newFormList = $newFormList->withNodeUpdated($readOnlyNode);
                        continue;
                    }

                    if ($flowPermission->isStepReadAccessGrantedForAtLeastOneRole($step->getUid())) {
                        $flowPermission = $flowPermission->withReadOnlyPermissionsForRoleInStepUid($role->getName(), $step->getUid());
                        $readOnlyNode->updateFlowPermission($flowPermission);
                        $newFormList = $newFormList->withNodeUpdated($readOnlyNode);
                        $hitFirstStepWithReadAccess = true;
                    }
                }
            }
        }

        return $newFormList;
    }

    /**
     * @see createUpdatedFormListWithAutoAssignedTimedWriteAccessByWorkFlowForRole
     *
     * @throws \Exception
     */
    public static function createUpdatedFormListWithAutoAssignedTimedWriteAccessByWorkFlowForRoles(
        FormList $formList,
        WorkFlow $workFlow,
        RoleCollection $roles,
        bool $assignAccessForReadOnlyViewNodes = true,
    ): FormList {
        // make sure we always return a new formList instance, starting from an exact copy from given formList
        $newFormList = FormList::fromRootNode($formList->getRootNode());

        // quick return to avoid needless formList checks (question loops, checks, etc. which is heavy on performance)
        if ($roles->isEmpty()) {
            return $newFormList;
        }

        foreach ($roles as $role) {
            $newFormList = static::createUpdatedFormListWithAutoAssignedTimedWriteAccessByWorkFlowForRole(
                $newFormList,
                $workFlow,
                $role,
                $assignAccessForReadOnlyViewNodes
            );
        }

        return $newFormList;
    }

    /**
     * helper method for admin-side, allowing to auto-assign write access on all questions in a @see WorkingVersion.
     *
     * @throws \Exception
     */
    public static function createUpdatedWorkingVersionWithAutoAssignedTimedWriteAccessByWorkFlowForRoles(
        WorkingVersion $workingVersion,
        RoleCollection $roles,
        bool $assignAccessForReadOnlyViewNodes = true,
    ): WorkingVersion {
        // skip if no workflow. but return new instance for method consistency
        if (!$workingVersion->hasWorkFlow()) {
            return $workingVersion->withInfo($workingVersion->hasInfo() ? $workingVersion->getInfo() : null);
        }

        $newFormList = static::createUpdatedFormListWithAutoAssignedTimedWriteAccessByWorkFlowForRoles(
            $workingVersion->getFormList(),
            $workingVersion->getWorkFlow(),
            $roles,
            $assignAccessForReadOnlyViewNodes
        );

        return $workingVersion->withFormList($newFormList);
    }
}
