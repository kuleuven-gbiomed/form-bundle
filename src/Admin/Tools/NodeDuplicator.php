<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Tools;

use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\NodeInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Question\CheckBoxInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\DateInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;
use Ramsey\Uuid\Uuid;

/**
 * Class NodeDuplicator.
 *
 * helper to duplicate (copy) nodes @see NodeInterface
 */
final class NodeDuplicator
{
    /**
     * returns a copy of given node with exact same parameters, values, scoring dependencies, etc... and including
     * the full tree with any nested child nodes (descendants) the given node might have (if it can have children).
     * uses auto generated uids for the new node and any of its descendants.
     *
     * NOTE: since we don't know the amount of descendants, we can not allow custom uids to be passed in for both the
     * new node and all its descendants. or we should have to provide a public method that calculates a number
     * corresponding to the amount of descendants, use that to assert on an array argument with uids in this method
     * and then that array argument will probably have to be an associative array so we can map each new uid to each
     * known descendant. way to overkill.
     *
     * NOTE: this duplication helps to create an exact copy, after which the update methods on nodes can be used to
     * change stuff on the duplicate (without creating a new instance on every update)
     *
     * NOTE: this could be used to duplicate a big tree of nodes, like the full @throws \Exception
     *
     * @see FormList::getRootNode()
     * @see FormList::getRootNode()
     */
    public static function duplicateNode(NodeInterface $node, ?string $uuid = null): NodeInterface
    {
        if (null === $uuid || '' === $uuid) {
            $uuid = self::createNewUid();
        }

        if ($node instanceof InputNode) {
            return self::duplicateInputNode($node, $uuid);
        }

        if ($node instanceof CategoryNode) {
            return self::duplicateCategoryNode($node, $uuid);
        }

        // if ever a new type of node is introduced, this will catch that it's unhandled
        throw new \InvalidArgumentException('cannot duplicate: unknown/unhandled node ['.$node::class.'] of type ['.$node::getType().']');
    }

    /**
     * @throws \Exception
     */
    public static function duplicateCategoryNode(CategoryNode $node, string $categoryUuid): CategoryNode
    {
        if (FormList::ROOT_UID === $node->getUid()) {
            throw new \InvalidArgumentException('it is not allowed to make a copy of the entire rootNode inside same FormList');
        }

        $children = [];
        foreach ($node->getChildren() as $child) {
            // recursive. make sure any descendants of child are duplicate and added as well
            // For the moment, the ID of the children is randomly generated.
            $children[] = self::duplicateNode($child, self::createNewUid());
        }
        /** @var NodeCollection<array-key, NodeInterface> $newNodeCollection */
        $newNodeCollection = new NodeCollection($children);

        return CategoryNode::fromUidAndDependencies(
            $categoryUuid,
            $node->getInfo(),
            $node->isHidden(),
            $newNodeCollection, // @phan-suppress-current-line PhanTypeMismatchArgumentProbablyReal
            $node->getDefaultFlowPermission(),
            $node->getDisplayMode(),
            $node->getLocalizedFirstHeaderCellTitle()
        );
    }

    /**
     * @throws \Exception
     */
    public static function duplicateInputNode(InputNode $node, string $uuid): InputNode
    {
        // FIXME: This is definitely wrong. We shouldn't need to know al implementations in advance.
        if ($node instanceof TextInputNode) {
            return self::duplicateTextInputNode($node, $uuid);
        }

        if ($node instanceof ChoiceInputNode) {
            return self::duplicateChoiceInputNode($node, $uuid);
        }

        if ($node instanceof NumberInputNode) {
            return self::duplicateNumberInputNode($node, $uuid);
        }

        if ($node instanceof OptionInputNode) {
            return self::duplicateOptionInputNode($node, $uuid);
        }

        if ($node instanceof CheckBoxInputNode) {
            return self::duplicateCheckBoxInputNode($node, $uuid);
        }

        if ($node instanceof UploadInputNode) {
            return self::duplicateUploadInputNode($node, $uuid);
        }

        if ($node instanceof DateInputNode) {
            return self::duplicateDateInputNode($node, $uuid);
        }

        // if a new type of node is introduced (WIP), this will catch that it's unhandled
        throw new \InvalidArgumentException('cannot duplicate: unknown/unhandled input node ['.$node::class.'] of type ['.$node::getType().']');
    }

    /**
     * @throws \Exception
     */
    public static function duplicateCheckBoxInputNode(CheckBoxInputNode $node, string $uuid): CheckBoxInputNode
    {
        return CheckBoxInputNode::fromUidAndDependencies(
            $uuid,
            $node->getInput(),
            $node->getFlowPermission(),
            $node->getInfo(),
            $node->isHidden(),
            $node->getDescriptionDisplayMode()
        );
    }

    /**
     * @throws \Exception
     */
    public static function duplicateTextInputNode(TextInputNode $node, string $uuid): TextInputNode
    {
        return TextInputNode::fromUidAndDependencies(
            $uuid,
            $node->getInput(),
            $node->getFlowPermission(),
            $node->getInfo(),
            $node->isHidden(),
            $node->getDescriptionDisplayMode()
        );
    }

    /**
     * @throws \Exception
     */
    public static function duplicateUploadInputNode(UploadInputNode $node, string $uuid): UploadInputNode
    {
        return UploadInputNode::fromUidAndDependencies(
            $uuid,
            $node->getInput(),
            $node->getFlowPermission(),
            $node->getInfo(),
            $node->isHidden(),
            $node->getDescriptionDisplayMode()
        );
    }

    /**
     * @throws \Exception
     */
    public static function duplicateNumberInputNode(NumberInputNode $node, string $uuid): NumberInputNode
    {
        return NumberInputNode::fromUidAndDependencies(
            $uuid,
            $node->getInput(),
            $node->getFlowPermission(),
            $node->getInfo(),
            $node->isHidden(),
            $node->getDescriptionDisplayMode(),
            $node->getGlobalScoreCalculationDependencyConfigs(),
            $node->getGlobalScoreWeighingType(),
            $node->getCustomOptions()
        );
    }

    /**
     * @throws \Exception
     */
    public static function duplicateOptionInputNode(OptionInputNode $node, string $uuid): OptionInputNode
    {
        return OptionInputNode::fromUidAndDependencies(
            $uuid,
            $node->getInput(),
            $node->getFlowPermission(),
            $node->getInfo(),
            $node->isHidden(),
            $node->getDescriptionDisplayMode()
        );
    }

    /**
     * @throws \Exception
     */
    public static function duplicateChoiceInputNode(ChoiceInputNode $node, string $uuid): ChoiceInputNode
    {
        $optionNodes = [];
        /** @var OptionInputNode $optionNode */
        foreach ($node->getChildren() as $optionNode) {
            // At the moment we don't have control over the options for the choice question.
            $optionNodes[] = self::duplicateOptionInputNode($optionNode, self::createNewUid());
        }

        return ChoiceInputNode::fromUidAndDependencies(
            $uuid,
            $node->getInput(),
            $node->getFlowPermission(),
            $node->getInfo(),
            $node->isHidden(),
            $node->getDescriptionDisplayMode(),
            new OptionInputNodeCollection($optionNodes),
            $node->getGlobalScoreCalculationDependencyConfigs(),
            $node->getGlobalScoreWeighingType(),
            $node->getRolesAllowedToSeeOptionScoreValues(),
            $node->getCustomOptions()
        );
    }

    /**
     * @throws \Exception
     */
    public static function duplicateDateInputNode(DateInputNode $node, string $uuid): DateInputNode
    {
        return DateInputNode::fromUidAndDependencies(
            $uuid,
            $node->getInput(),
            $node->getFlowPermission(),
            $node->getInfo(),
            $node->isHidden(),
            $node->getDescriptionDisplayMode()
        );
    }

    /**
     * @throws \Exception
     */
    private static function createNewUid(): string
    {
        return Uuid::uuid4()->toString();
    }
}
