<?php

declare(strict_types=1);

namespace KUL\FormBundle\Admin\Tools;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\Node\ReadOnly\AbstractReadOnlyViewNode;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepCollectionInterface;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Class QuestionsAccessRevoker.
 *
 * helper for admin-side to revoke access to questions, based on role(s) step(s) in any combination
 * when working on a @see WorkingVersion
 */
final class QuestionsAccessRevoker
{
    /**
     * returns a new instance of the given formList with permissions removed for given role in given step for all
     * questions in the given formList.
     *
     * NOTE: not asking for a workFlow here, and then checking if given step belongs to that workflow,... if step
     * does not exist (permissions for step do not exist on question(s)) then no permissions on those questions
     * will not be removed and no harm done.
     *
     * @throws \Exception
     */
    public static function createUpdatedFormListWithQuestionsAccessRevokedForRoleInStep(
        FormList $formList,
        Role $role,
        StepInterface $step,
        bool $includingReadOnlyNodesAccess = true,
    ): FormList {
        // start from an exact copy from given formList (makes sure we always return a new formList instance for method
        // consistency if no update will be possible, because any question update on formList will return new instance)
        $newFormList = FormList::fromRootNode($formList->getRootNode());

        // get questions usable for assigning access: skip if formList has no usable questions (yet)
        // update questions and update the new formList with each updated question
        /** @var InputNode $question */
        foreach ($newFormList->getFlattenedParentalInputNodes() as $question) {
            $question->revokeFlowPermissionForRoleInStep($role, $step);
            $newFormList = $newFormList->withNodeUpdated($question);
        }

        if ($includingReadOnlyNodesAccess) {
            /** @var AbstractReadOnlyViewNode $readOnlyViewNode */
            foreach ($newFormList->getFlattenedReadOnlyViewNodes() as $readOnlyViewNode) {
                $readOnlyViewNode->revokeFlowPermissionForRoleInStep($role, $step);
                $newFormList = $newFormList->withNodeUpdated($readOnlyViewNode);
            }
        }

        return $newFormList;
    }

    /**
     * returns a new instance of the given formList with permissions removed for given role in given steps for all
     * questions in the given formList.
     *
     * NOTE: not asking for a workFlow here, and then checking if given steps belongs to that workflow,... if steps
     * do not exist (permissions for step do not exist on question(s)) then no permissions on those questions
     * will not be removed and no harm done.
     *
     * @param StepCollectionInterface<array-key, StepInterface> $steps
     *
     * @throws \Exception
     */
    public static function createUpdatedFormListWithQuestionsAccessRevokedForRoleInSteps(
        FormList $formList,
        Role $role,
        StepCollectionInterface $steps,
        bool $includingReadOnlyNodesAccess = true,
    ): FormList {
        // start from an exact copy from given formList (makes sure we always return a new formList instance for method
        // consistency if no update will be possible, because any question update on formList will return new instance)
        $newFormList = FormList::fromRootNode($formList->getRootNode());

        foreach ($steps as $step) {
            $newFormList = self::createUpdatedFormListWithQuestionsAccessRevokedForRoleInStep($formList, $role, $step, $includingReadOnlyNodesAccess);
        }

        return $newFormList;
    }

    /**
     * returns a new instance of the given formList with permissions removed for given role in all steps in
     * given workFlow for all questions in the given formList.
     *
     * @throws \Exception
     */
    public static function createUpdatedFormListWithQuestionsAccessRevokedForRoleInWorkFlow(
        FormList $formList,
        Role $role,
        WorkFlow $workFlow,
        bool $includingReadOnlyNodesAccess = true,
    ): FormList {
        return self::createUpdatedFormListWithQuestionsAccessRevokedForRoleInSteps(
            $formList,
            $role,
            $workFlow->getSteps(),
            $includingReadOnlyNodesAccess
        );
    }

    /**
     * returns a new instance of the given formList with all permissions revoked: no role will have any access left
     * in any step.
     *
     * @throws \Exception
     */
    public static function createUpdatedFormListWithAllQuestionsAccessRevoked(
        FormList $formList,
        bool $includingReadOnlyNodesAccess = true,
    ): FormList {
        // start from an exact copy from given formList (makes sure we always return a new formList instance for method
        // consistency if no update will be possible, because any question update on formList will return new instance)
        $newFormList = FormList::fromRootNode($formList->getRootNode());

        // get questions usable for assigning access: skip if formList has no usable questions (yet)
        // update questions and update the new formList with each updated question
        /** @var InputNode $question */
        foreach ($newFormList->getFlattenedParentalInputNodes() as $question) {
            $question->revokeFlowPermissionForAllRolesInAllSteps();
            $newFormList = $newFormList->withNodeUpdated($question);
        }

        if ($includingReadOnlyNodesAccess) {
            /** @var AbstractReadOnlyViewNode $readOnlyViewNode */
            foreach ($newFormList->getFlattenedReadOnlyViewNodes() as $readOnlyViewNode) {
                $readOnlyViewNode->revokeFlowPermissionForAllRolesInAllSteps();
                $newFormList = $newFormList->withNodeUpdated($readOnlyViewNode);
            }
        }

        return $newFormList;
    }
}
