<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\Template\Utility\Collection\TemplateCollection;

/**
 * @extends EntityRepository<Template>
 */
class TemplateRepository extends EntityRepository
{
    private function buildQueryBuilderForId(string $id): QueryBuilder
    {
        $qb = $this->createQueryBuilder('tp');

        return $qb->select('tp')
            ->where($qb->expr()->eq('tp.id', ':templateId'))
            ->setParameter('templateId', $id);
    }

    public function hasOneById(string $id): bool
    {
        return $this->buildQueryBuilderForId($id)->getQuery()->getOneOrNullResult() instanceof Template;
    }

    public function getOneById(string $id): Template
    {
        $result = $this->buildQueryBuilderForId($id)->getQuery()->getOneOrNullResult();

        if ($result instanceof Template) {
            return $result;
        }

        throw new \BadMethodCallException('no template found for id ['.$id.']');
    }

    public function getAll(): TemplateCollection
    {
        /** @var TemplateInterface[] $all */
        $all = $this->findAll();

        return new TemplateCollection($all);
    }

    public function save(Template $template): void
    {
        $em = $this->_em;

        $em->persist($template);
        $em->flush();
    }

    public function remove(Template $template): void
    {
        $em = $this->_em;

        $em->remove($template);
        $em->flush();
    }

    public function removeById(string $templateId): void
    {
        $template = $this->getOneById($templateId);
        $this->remove($template);
    }
}
