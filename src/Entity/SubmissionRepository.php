<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * @extends EntityRepository<Submission>
 */
class SubmissionRepository extends EntityRepository
{
    public function delete(Submission $submission): void
    {
        $em = $this->_em;

        $em->remove($submission);
        $em->flush();
    }
}
