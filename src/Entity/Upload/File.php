<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity\Upload;

use KUL\FormBundle\Entity\Upload\Exception\FileNotFoundException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Mime\MimeTypeGuesserInterface;
use Symfony\Component\Mime\MimeTypes;

/**
 * Class File
 * TODO MUCH: copy pasted from SCONE which uses this in a SCONE embedded bundle: FormBundle for the actual upload of a file.
 */
class File
{
    /**
     * @see https://www.iana.org/assignments/media-types/application/octet-stream
     */
    final public const DEFAULT_MIME_TYPE = 'application/octet-stream';

    protected string $name;

    protected \DateTimeImmutable $savedAt;

    protected int $size;

    protected string $hash;

    protected string $mimeType;

    public function __construct(\SplFileInfo $file)
    {
        $this->name = Uuid::uuid4()->toString();

        $this->savedAt = new \DateTimeImmutable('now');

        $this->size = $file->getSize();

        $this->mimeType = self::guessMimeType($file);
        $this->hash = static::computeHash($file);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFilePath(string $storageDirectory): string
    {
        $realPath = realpath($storageDirectory.\DIRECTORY_SEPARATOR.$this->getName());

        // if not found, check if subdirectories were used to store this file.
        if (!is_string($realPath)) {
            $realPath = realpath($this->getFilePathWithDestinationSubDirectoriesBasedOnFileNameLetters($storageDirectory).$this->getName());
        }

        // if still not found, return an empty string for consistency.
        if (!is_string($realPath)) {
            return '';
        }

        return $realPath;
    }

    public function getSavedAt(): \DateTimeImmutable
    {
        return $this->savedAt;
    }

    /**
     * Returns the file size in bytes.
     *
     * @see http://php.net/manual/en/splfileinfo.getsize.php
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * Converts size in bytes to human readable size.
     */
    public function getReadableSize(int $precision = 2, string $prefixes = 'decimal'): string
    {
        return self::convertToReadableSize($this->getSize(), $precision, $prefixes);
    }

    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    public function hasMimeType(): bool
    {
        return '' !== $this->mimeType;
    }

    /**
     * Returns SHA256 hash of the file.
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * Verifies file existence in storage directory.
     */
    public function verifyExistence(string $storageDirectory): bool
    {
        return file_exists($this->getFilePath($storageDirectory));
    }

    /**
     * Verifies file hash of the file with a file in the storage directory.
     *
     * Warning! Computes file hash - may be too slow to do on every access.
     *
     * @throws FileNotFoundException
     */
    public function verifyHash(string $storageDirectory): bool
    {
        if (!$this->verifyExistence($storageDirectory)) {
            throw FileNotFoundException::createForFile($this, $storageDirectory);
        }

        return $this->getHash() === static::computeHash(
            new \SplFileInfo($this->getFilePath($storageDirectory))
        );
    }

    /**
     * Computes hash of the file using sha256 algorithm.
     *
     * @see https://en.wikipedia.org/wiki/SHA-2
     */
    public static function computeHash(\SplFileInfo $file): string
    {
        if (false === $file->getRealPath()) {
            throw new FileNotFoundException("File [{$file->getFilename()}] not found!");
        }

        $hash = hash_file('sha256', $file->getRealPath());

        if (false === $hash) {
            throw new FileNotFoundException("Failed to hash file [{$file->getFilename()}]!");
        }

        return $hash;
    }

    /**
     * Returns the mime type of the file.
     *
     * The mime type is guessed using a MimeTypeGuesser instance, which uses finfo(), mime_content_type() and the
     * system binary "file" (in this order), depending on which of those are available. In cases where mime type could
     * not be guessed the 'application/octet-stream' is used.
     *
     * @see MimeTypeGuesserInterface
     * @see https://en.wikipedia.org/wiki/Media_type
     */
    private static function guessMimeType(\SplFileInfo $file): string
    {
        // FIXME: Guessing the file type right here, makes the File class, and all methods of all inheriting classes
        // (and e.g. SavedFile::jsonSerialize()) untestable.
        // It's better to pass the mime type to the constructor, and let some file factory do the file type guessing.
        $mimeTypes = MimeTypes::getDefault();

        return $mimeTypes->guessMimeType($file->getRealPath()) ?? self::DEFAULT_MIME_TYPE;
    }

    /**
     * Converts bytes to human readable size.
     */
    public static function convertToReadableSize(int $bytes, int $precision = 2, string $prefixes = 'decimal'): string
    {
        $units = [
            'decimal' => ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            'binary' => ['B', 'KiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'],
        ];

        $unitBase = 'decimal' === $prefixes ? 1000 : 1024;

        $logarithmOfBytes = log($bytes, $unitBase);
        $unitIndex = (int) floor($logarithmOfBytes);
        $pointBetweenUnits = $logarithmOfBytes - (float) $unitIndex;

        // allows to switch eg.: from 100B to 0.1kB for precision 2 or from 1MB to 1.000001MB for precision 6
        $bumpPoint = log(10 ** $precision - 1, $unitBase);

        // bump unit index if point is after bump point
        if ($pointBetweenUnits > $bumpPoint) {
            ++$unitIndex;
        }

        $bytesInUnit = $unitBase ** $unitIndex;

        if (0 === $bytes) {
            return ((string) $bytes).$units[$prefixes][$unitIndex];
        } // avoid fatal error on division by zero below

        // combine rounded size with appropriate unit
        return ((string) round($bytes / $bytesInUnit, $precision)).$units[$prefixes][$unitIndex];
    }

    /**
     * returns a string based on the first (three) letters of the name.
     * This was introduced to reduce the amount of files per folder.
     * because if a single folder contained too much files (e.g. more than 20000 files) it might impact performance.
     */
    public function getFilePathWithDestinationSubDirectoriesBasedOnFileNameLetters(string $mainDestinationFolder): string
    {
        $letters = mb_str_split($this->getName());

        if (3 <= count($letters)) {
            $mainDestinationFolder .= '/'.$letters[0].'/'.$letters[1].'/'.$letters[2].'/';
        } elseif (2 === count($letters)) {
            $mainDestinationFolder .= '/'.$letters[0].'/'.$letters[1].'/';
        } elseif (1 === count($letters)) {
            $mainDestinationFolder .= '/'.$letters[0].'/';
        }

        return str_replace('//', '/', $mainDestinationFolder);
    }
}
