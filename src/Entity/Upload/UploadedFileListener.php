<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity\Upload;

use KUL\FormBundle\Entity\Upload\Exception\FailedToRemoveFileException;
use KUL\FormBundle\Entity\Upload\Exception\FileNotFoundException;
use KUL\FormBundle\Entity\Upload\Exception\WrongFileException;
use KUL\FormBundle\Entity\UploadedFile;

readonly class UploadedFileListener
{
    /** @var string[] */
    private array $storageDirectories;

    public function __construct(private bool $removeFileFromStorage, string ...$storageDirectories)
    {
        $this->storageDirectories = $storageDirectories;
    }

    /**
     * ensure UploadedFile entity is initialized & hydrated for usage in @see UploadedFileListener::postRemove() to
     * avoid Doctrine no longer finding the entity after it has been deleted (in transaction, or at least not
     * re-queryable anymore at that time) from DB before postRemove is triggered. (non-initialized status is usually a
     * consequence of lazy loading entity via owner side of a relation).
     *
     * this is a known Doctrine 'issue' with different solutions (eager fetching being one, but undesirable):
     *
     * @see https://stackoverflow.com/questions/21281659/symfony-doctrine-2-postremove-remove-file-strange-behavior
     * @see https://stackoverflow.com/questions/50137892/entity-created-in-postremove-listener-is-not-persisted
     *
     * initializing is ensured by calling a random member on the object (associated with a DB column) to trigger
     * Doctrine to initialize & hydrate the entity. we use properties 'hash' & 'name', which are the members that are
     * (indirectly) called and used in the postRemove via @see UploadedFile::verifyHash().
     *
     * @throws FileNotFoundException
     */
    public function preRemove(UploadedFile $file): void
    {
        // initialize by calling hash & name, ensuring both will be hydrated. to give this method legality, throw
        // exception if one is not found, indicating entity is not or could not be initialized and postRemove will fail
        if ('' === $file->getName()) {
            throw new \RuntimeException('UploadedFile is most likely not initialized: required name (uuid) for removal is missing!');
        }

        if ('' === $file->getHash()) {
            throw new \RuntimeException('UploadedFile ['.$file->getName().'] is most likely not initialized: required hash for removal is missing!');
        }
    }

    /**
     * remove file associated with entity that was deleted through @see UploadedFileRepository::delete() from storage.
     *
     * IMPORTANT:
     * if given UploadedFile is not initialized, any call on members on the UploadedFile instance (here properties
     * 'name' & 'hash' via methods filePath & verifyHash) will 'falsely' fail on 'entity not found'.
     *
     * from official Symfony/Doctrine docs: "a postRemove event is invoked after the DB delete operation is executed."
     * but if something fails in this postRemove, the record is not deleted from DB. So I think the deletion is then
     * rolled back in an isolated transaction!? either that or Doctrine 'locked' it in the persistence collection!?
     * anyhow, in this post remove, we can not initialize & hydrate the entity (by calling on a member, firing a DB call)!
     * if the entity was NOT initialized before that DB delete operation was executed, the entity will NOT have been
     * hydrated. the entity will still be in Doctrine's persistence collection, because Doctrine can delete the entity
     * in DB (because it 'knows' the ID, which ironically is the same as 'name' property here). but if we then call any
     * non-hydrated member/property in this postRemove, like 'hash' indirectly via @see UploadedFile::verifyHash() ,
     * Doctrine will try to execute a DB query in the background to hydrate the entity with its member value(s)! but
     * since it no longer exists in DB after the DB deletion operation or because it's locked in Doctrine persistence
     * collection, that will fail on (SQD/SQL) 'entity not found'.
     * to avoid this we make sure the entity is initialized in @see UploadedFileListener::preRemove()
     *
     * the DB deletion of UploadedFile via @see UploadedFileRepository::delete() should NOT fail just because file no
     * longer exist in storage! if it does not exist, it's either manually removed or moved or something went
     * wrong trying to place the file in storage when an @see UploadedFile was created. or DB is not in sync with the
     * storage. either way, it left the UploadedFile record in DB orphaned and useless and it gives more headaches to
     * keep it orphaned than to remove the orphan. so if the file is not found in storage (in any oif the known
     * (sub)directories), we skip the rest of the postRemove so that UploadedFile will still be deleted from DB,
     * as this is EXACTLY what is wanted when we call @see UploadedFileRepository::delete() !
     *
     * @throws FailedToRemoveFileException
     */
    public function postRemove(UploadedFile $file): void
    {
        if (!$this->removeFileFromStorage) {
            return;
        }

        foreach ($this->storageDirectories as $storageDirectory) {
            // DB deletion of UploadedFile via entityManager should NOT fail because file no longer exists in storage!
            if (!$file->verifyExistence($storageDirectory)) {
                continue;
            }

            // security check preventing removing wrong files
            if (!$file->verifyHash($storageDirectory)) {
                throw WrongFileException::createForUploadedFile($file, $storageDirectory);
            }

            // delete file
            unlink($file->getFilePath($storageDirectory));
            // double check delete
            /* @phpstan-ignore-next-line */
            if ($file->verifyExistence($storageDirectory)) {
                throw FailedToRemoveFileException::createForUploadedFile($file, $storageDirectory);
            }
        }
    }
}
