<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity\Upload;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Entity\UploadedFile;

/**
 * @method UploadedFile[] toArray()
 *
 * @extends ArrayCollection<array-key, UploadedFile>
 */
class UploadedFileCollection extends ArrayCollection implements \JsonSerializable
{
    public function jsonSerialize(): array
    {
        return array_map(fn (UploadedFile $file) => $file->jsonSerialize(), $this->toArray());
    }

    /**
     * @throws \BadMethodCallException
     */
    public function getOneByName(string $name): UploadedFile
    {
        $result = $this->filter(fn (UploadedFile $file) => $file->getName() === $name);

        $first = $result->first();
        if ($first instanceof UploadedFile && 1 === $result->count()) {
            return $first;
        }

        throw new \BadMethodCallException('Wrong amount of UploadedFile objects found. Expected to find exactly one,'." found: {$result->count()}.");
    }

    public function getNames(): array
    {
        return array_map(fn (UploadedFile $file) => $file->getName(), $this->toArray());
    }
}
