<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity\Upload\Exception;

use KUL\FormBundle\Entity\UploadedFile;

/**
 * Class WrongFileException.
 */
class WrongFileException extends \RuntimeException
{
    public static function createForUploadedFile(UploadedFile $file, string $storageDirectory): self
    {
        return new self("Uploaded file [{$file->getName()}] with hash [{$file->getHash()}] not found in storage directory [{$storageDirectory}]!");
    }
}
