<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity\Upload\Exception;

use KUL\FormBundle\Entity\Upload\File;

class FileNotFoundException extends \RuntimeException
{
    public static function createForFile(File $file, string $storageDirectory): self
    {
        return new self("File [{$file->getName()}] not found in storage directory [{$storageDirectory}]!");
    }
}
