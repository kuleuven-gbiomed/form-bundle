<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity\Upload\Exception;

use KUL\FormBundle\Entity\UploadedFile;

/**
 * Class FailedToRemoveFile.
 */
class FailedToRemoveFileException extends \Exception
{
    public static function createForUploadedFile(UploadedFile $file, string $storageDirectory): self
    {
        return new self("Failed to remove file [{$file->getName()}] from storage directory [{$storageDirectory}]!");
    }
}
