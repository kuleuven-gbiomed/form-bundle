<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity\Upload;

use Doctrine\Common\Collections\ArrayCollection;
use KUL\FormBundle\Domain\Contract\FormUserInterface;
use KUL\FormBundle\Entity\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile as TempFile;
use Symfony\Component\HttpFoundation\FileBag;

/**
 * @extends ArrayCollection<array-key, TempFile>
 */
class TempFileCollection extends ArrayCollection
{
    public static function createFromFileBag(FileBag $fileBag): self
    {
        $fileBagIterator = new \RecursiveIteratorIterator(
            new \RecursiveArrayIterator($fileBag->all()),
            \RecursiveIteratorIterator::SELF_FIRST
        );

        $files = [];
        foreach ($fileBagIterator as $file) {
            if (!$file instanceof TempFile) {
                continue;
            }

            $files[] = $file;
        }

        return new self($files);
    }

    /**
     * Moves temporary files to the final destination and map them to the uploaded files.
     *
     * $useDestinationSubDirectories:
     * creates the final destination with (up to) 3 subdirectories below the destinationDirectory, based on the filename.
     * for example destinationDirectory/1/a/4/1a446kfs4.jpg.
     *
     * @return UploadedFileCollection a collection representing the final files
     *
     * @throws FileException if the target file could not be created
     */
    public function finishUpload(
        FormUserInterface $uploader,
        string $destinationDirectory,
        bool $useDestinationSubDirectoriesBasedOnFileNameLetters = false,
    ): UploadedFileCollection {
        $mapTempToFinalFiles = function (TempFile $tempFile) use ($uploader, $destinationDirectory, $useDestinationSubDirectoriesBasedOnFileNameLetters): UploadedFile {
            $originalName = $tempFile->getClientOriginalName();
            if ('' === $originalName) {
                throw new \InvalidArgumentException("We don't expect temporary files without original name here.");
            }

            $finalFile = new UploadedFile($tempFile, $uploader, $originalName);

            if ($useDestinationSubDirectoriesBasedOnFileNameLetters) {
                $destinationDirectory = $finalFile->getFilePathWithDestinationSubDirectoriesBasedOnFileNameLetters($destinationDirectory);
            }

            $tempFile->move($destinationDirectory, $finalFile->getName());

            return $finalFile;
        };

        return new UploadedFileCollection(array_map($mapTempToFinalFiles, $this->toArray()));
    }
}
