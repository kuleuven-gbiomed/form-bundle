<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity;

use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Client\Submission\SubmissionCollection;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;
use KUL\FormBundle\Domain\TemplateTarget\AbstractJsonAwareStoredTemplateTarget;

/**
 * Class StoredTemplateTarget.
 *
 * generic, orm mapped way of storing answers, keeping workflow state & tracking end-users submit/tempsave history
 * for questions inside a workflow of a @see TemplateInterface
 * for a @see TemplateTargetableInterface (objetc/entity in implementing app)
 *
 * all based on role-workflow access inside that template (sole responsibility of this bundle)
 * and based on assumption that end-user has (role(s) with) access to that TemplateTargetable object
 * in the implementing app (sole responsibility of implementing app)
 * and where that TemplateTargetable object is uniquely identifiable by a 'targetCode' and 'targetIdentifier'
 * (in both implementing app as well as in this class's mapped DB table)
 *
 * this unique identifiability is the responsibility of implementing app to assert. target code in this identifiability
 * is in case implementing app uses primary keys, which are only unique per object)
 *
 * every record in the DB table associated with this entity class is a combo of this TemplateTargetable object/entity
 * and the @see TemplateInterface : i.e. a record (instance of this class) represents a set of answers (+workflow state,
 * etc.) for a TemplateTargetable object/entity given for a specific template. meaning that per template, each
 * TemplateTargetable has either one or no record. thus one record is the sole storage space for a target-template
 * combo and all actions & answers by all users on that combo are stored in thta one record
 *
 * this identifiability is also partially asserted on DB level in the table associated with this entity class by a SQL
 * unique index contraint on these three elements:
 * - @see TemplateInterface::getId()
 * - @see TemplateTargetableInterface::getTargetCode()
 * - @see TemplateTargetableInterface::getTargetIdentifier()
 */
class StoredTemplateTarget extends AbstractJsonAwareStoredTemplateTarget
{
    /**
     * DEPRECATED: please don't use this or at least be careful! the last used step should only be set & updated
     *             internally by an instance of this entity, as a consequence of a save or temporary save action.
     *             as the name suggests, this method only exist to fix some issues when things went/go badly wrong.
     */
    public function fixLastUsedStep(string $stepUid): void
    {
        $this->lastUsedStepUid = $stepUid;
    }

    /**
     * ****************************************************************************************************************
     * WARNING: please don't use this or at least be careful!
     *          this method overrides & bypasses the usual logic to create a submitted form & as such bypasses the usual processing of
     *          its answers and status. if used blindly, it could result in unworkable submitted forms or corrupted (answer/status) data!
     *          this method exists only for controlled migration purposes from old SCONE submitted form to ones in this bundle.
     *          this method is temporary and should be removed or at least marked 'internal' and/or 'deprecated'
     *          as soon as migration from old SCONE submitted forms is finished!
     * ****************************************************************************************************************
     */
    public static function createFromMigratedData(
        string $id,
        TemplateInterface $template,
        string $targetCode,
        string $targetId,
        int $lastUsedTemplateVersionNumber,
        string $lastUsedStepUid,
        SavedFileCollection $savedFiles,
        array $submittedFormValues,
        array $temporarySavedFormValues,
        SubmissionCollection $submissions,
        SubmissionCollection $temporarySubmissions,
    ): self {
        $object = new \ReflectionClass(self::class);
        $instance = $object->newInstanceWithoutConstructor();

        $instance->id = $id;
        $instance->template = $template;
        $instance->targetCode = $targetCode;
        $instance->targetId = $targetId;
        $instance->lastUsedTemplateVersionNumber = $lastUsedTemplateVersionNumber;
        $instance->lastUsedStepUid = $lastUsedStepUid;
        $instance->serializedSubmittedFormValues = json_encode($submittedFormValues, \JSON_THROW_ON_ERROR);
        $instance->serializedTemporarySavedFormValues = json_encode($temporarySavedFormValues, \JSON_THROW_ON_ERROR);

        $instance->savedFiles = $savedFiles;

        // add the submissions first, so we can build the Latest(Temp)SavedQuestionEntries from them next.
        $instance->submissions = $submissions;
        $instance->temporarySubmissions = $temporarySubmissions;

        // build the LatestSavedQuestionEntries for each question, based on current FormList
        // the instance is being build from scratch, so no existing latestSaveActionPerQuestionHistory entries will yet exist,
        // thus we can simply take the most recent given answer for each question and not have to compare with existing latest entries
        $latestSaveActionPerQuestionHistory = $instance->getSubmitHistoryForCurrentFormList()->getLatestPerQuestion();
        $instance->serializedLatestSavedQuestionEntries = json_encode($latestSaveActionPerQuestionHistory, \JSON_THROW_ON_ERROR);

        // build the LatestTempSavedQuestionEntries for each question, based on current FormList
        // the instance is being build from scratch, so no existing latestTempSaveActionPerQuestionHistory entries will yet exist,
        // thus we can simply take the most recent given answer for each question  and not have to compare with existing latest entries
        $latestTempSaveActionPerQuestionHistory = $instance->getTemporarySubmitHistoryForCurrentFormList()->getLatestPerQuestion();
        $instance->serializedLatestTemporarySavedQuestionEntries = json_encode($latestTempSaveActionPerQuestionHistory, \JSON_THROW_ON_ERROR);

        return $instance;
    }
}
