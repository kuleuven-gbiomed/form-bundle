<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity;

use KUL\FormBundle\Domain\Contract\FormUserInterface;
use KUL\FormBundle\Entity\Upload\File;

/**
 * Class UploadedFile
 * TODO MUCH: copy pasted from SCONE which uses this in a SCONE embedded bundle: FormBundle for the actual upload of a file.
 *
 * This file used to be in the 'Upload'-namespace, but doctrine was whining it couldn't find the entity.
 * So I just ended up moving it a level higher.
 */
class UploadedFile extends File implements \JsonSerializable
{
    protected string $uploaderId;

    protected string $uploaderFullName;

    /**
     * File constructor.
     *
     * @param string $originalName
     */
    public function __construct(\SplFileInfo $file, FormUserInterface $uploader, protected $originalName)
    {
        parent::__construct($file);

        $this->uploaderId = $uploader->getIdentifier();
        $this->uploaderFullName = $uploader->getPerson()->getFullName();
    }

    /**
     * ****************************************************************************************************************
     * WARNING: please don't use this or at least be careful! it creates this entity without an SplFileInfo!
     *          this method overrides & bypasses the usual logic to create an uploaded file.
     *          this method exists only for controlled migration purposes from old SCONE submitted form to ones in this bundle.
     *          this method is temporary and should be removed or at least marked 'internal' and/or 'deprecated'
     *          as soon as migration from old SCONE submitted forms is finished!
     * ****************************************************************************************************************
     */
    public static function createFromMigratedData(
        string $uploaderId,
        string $uploaderFullName,
        string $name,
        string $originalName,
        \DateTimeImmutable $savedAt,
        int $size,
        string $hash,
        string $mimeType,
    ): self {
        $object = new \ReflectionClass(self::class);
        $instance = $object->newInstanceWithoutConstructor();

        $instance->uploaderId = $uploaderId;
        $instance->uploaderFullName = $uploaderFullName;
        $instance->name = $name;
        $instance->originalName = $originalName;
        $instance->savedAt = $savedAt;
        $instance->size = $size;
        $instance->hash = $hash;
        $instance->mimeType = $mimeType;

        return $instance;
    }

    /**
     * @return string
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * @return string
     */
    public function getUploaderId()
    {
        return $this->uploaderId;
    }

    /**
     * @return string
     */
    public function getUploaderFullName()
    {
        return $this->uploaderFullName;
    }

    /**
     * Specify data which should be serialized to JSON.
     *
     * @see  http://php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return array{name: string, originalName: string, uploaderId: string, uploaderFullName: string, savedAt: string, timestamp: int, readableSize: string, size: int}
     */
    public function jsonSerialize(): array
    {
        return [
            'name' => $this->getName(),
            'originalName' => $this->getOriginalName(),
            'uploaderId' => $this->getUploaderId(),
            'uploaderFullName' => $this->getUploaderFullName(),
            'savedAt' => $this->getSavedAt()->format('G:i, j M Y'),
            'timestamp' => $this->getSavedAt()->getTimestamp(),
            'readableSize' => $this->getReadableSize(),
            'size' => $this->getSize(),
        ];
    }
}
