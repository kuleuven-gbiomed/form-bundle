<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity;

use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Domain\Template\Element\Node\Question\UploadInputNode;
use Ramsey\Uuid\Uuid;
use Webmozart\Assert\Assert;

/**
 * Class SavedFile.
 *
 * TODO MUCH: copy pasted from SCONE which uses this in a SCONE embedded bundle: FormBundle for the actual upload of a file.
 *
 * link between an uploaded file @see UploadedFile
 * and a question @see UploadInputNode::getUid()
 *
 * the upload of a file as an answer is done via an AJAX that stores the file as @see UploadedFile
 * and then couples that uploaded file via an identifier @see UploadedFile::getName()
 * to a question @see UploadInputNode as an 'saved answer' where the 'saved answer' is the file, hence savedFile
 * saved refers to either a file saved as temporary answer or a submitted answer
 *
 * @see SavedFileCollection
 *
 * ###### START COMMENT COPY-PASTED FROM OLD CODE
 *       if a user wants to edit the question and delete a file (or replace a file by another), the uid from the old
 *       file is only decoupled from the saved values for that question. because a saved file NEVER gets deleted.
 *       e.g. if a saved file for a question is deleted by a temporary save, that file is still not deleted in the
 *       actual final submitted values. and shouldn't be, because it isn't until someone actually final submits the values
 *       (in this case the saved file uids) for that question, that the file (decoupled as deleted from temporary
 *       values) is to be considered deleted in the final values (final submit = submitting the current temporary values)
 *       and for history/log/blame purposes, the file is then also not hard deleted, but kept decoupled from the final
 *       submitted values (in the DB however, that deleted file still exists with a soft coupling to the question)
 *
 *       HOW SUBMITTED (UPLOADED) FILES ARE LINKED TO THE QUESTION (= inputNode) :
 *       IN GENERAL: files are uploaded separately (via ajax) and attached to the submitted form. ajax returns the newly
 *       uploaded file uid, which get added (concatenated) on client side to the corresponding (hidden) upload
 *       question/field (@see UploadType ). to make those uploaded files to be considered as his/her submitted (or
 *       temporary saved) files by the user, that user then has to submit (or (auto) temporary save) the whole form
 *       as a way to indicate he/she wants to 'submit' this file(s) for that node (as is done with any other question).
 *       the concatenated (possibly updated) string that user submits in that way - and that gets added to the existing
 *       (temporary) form values (after of course form/file validation) - does then contain the coupling to all the
 *       uploaded files to be considered submitted (or temporary saved) for that question.
 *
 *       IN DETAIL: The uuids of SubmittedNodeFiles are be concatenated in a single string and as such found in the formValues,
 *       (keyed by the input's InputNode::uid = exactly same way as storing saved values for any other @see AbstractInput).
 *       that concatenation is convenient for performance, fast checks, fetches-by-file-uid, building links, etc.. but it
 *       can NOT be trusted a single truth that the saved files associated with those uuids. because on rendering
 *       & submitting the form, this upload input is actually a hidden text input, containing that concatenated string,
 *       and the user could alter that string if he/she wanted to.
 *       hence saved files are stored DB with link to the InputNode's uid (@see SavedFile::$nodeUid ).
 *       AND a link (via pivot many-to-many) to the whole submitted form it belongs to. so that the linkage can be
 *       checked in those places where it's required & needed (i.e. when working with direct user input data, e.g.
 *       when building the form or handling the submitted form with that concatenated string).
 *       in an ideal world, the concatenated string should never be made, stored or used and everything should be
 *       checked & retrieved solely via querying in the saved files table (using lots of listeners for some parts).
 *       but that is too difficult and elaborate in the (current) actual processing of a submission by an user and not
 *       always needed. So the concatenated string is used in conjunction, although never as a SINGLE TRUTH.
 *       it is used because it keeps the handling of formValues uniform: it aids a lot in the complex process of actually
 *       submitting / temporary saving a form's values with all the value-merging-logging-... going on: instead of
 *       only querying in the DB saved files for any action, we rely on that string to check and set some values,
 *       in combination with simple checks, where needed, on the DB existence of the files associated with the uids
 *       in that concatenated string.
 * ###### END COMMENT COPY-PASTED FROM OLD CODE
 */
class SavedFile implements \JsonSerializable
{
    private string $uid;

    /**
     * the uid of the inputNode (i.e. question) the file is uploaded and saved for.
     */
    private string $nodeUid;

    public function __construct(private UploadedFile $uploadedFile, string $nodeUid)
    {
        Assert::notEmpty($nodeUid);

        $this->uid = Uuid::uuid4()->toString();
        $this->nodeUid = $nodeUid;
    }

    /**
     * make a saved file for a node from a @see UploadedFile.
     */
    public static function fromUploadedFileAndInputNode(UploadedFile $uploadedFile, UploadInputNode $node): self
    {
        return new self($uploadedFile, $node->getUid());
    }

    /**
     * ****************************************************************************************************************
     * WARNING: please don't use this or at least be careful!
     *          this method overrides & bypasses the usual logic to create a saved file.
     *          this method exists only for controlled migration purposes from old SCONE submitted form to ones in this bundle.
     *          this method is temporary and should be removed or at least marked 'internal' and/or 'deprecated'
     *          as soon as migration from old SCONE submitted forms is finished!
     * ****************************************************************************************************************
     */
    public static function createFromMigratedData(
        UploadedFile $uploadedFile,
        string $id,
        string $nodeUid,
    ): self {
        $object = new \ReflectionClass(self::class);
        $instance = $object->newInstanceWithoutConstructor();

        $instance->uploadedFile = $uploadedFile;
        $instance->uid = $id;
        $instance->nodeUid = $nodeUid;

        return $instance;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getUploadedFile(): UploadedFile
    {
        return $this->uploadedFile;
    }

    public function getNodeUid(): string
    {
        return $this->nodeUid;
    }

    public function getOriginalName(): string
    {
        return $this->getUploadedFile()->getOriginalName();
    }

    /**
     * To make sure the filename can be used in a Zip file that is extractable in all OS's, we need to sanitize it
     * since users are allowed to upload the same file multiple times for the same question or upload different
     * files with exact same name for the same question, we need to make sure the filename is unique, to avoid files
     * being overwritten in Zip and/or making the zip file potentially corrupted/unzippable.
     * see https://github.com/maennchen/ZipStream-PHP/issues/154#issuecomment-669252691.
     *
     * note: it's highly unlikely an 'empty' filename or a filename with an invalid or no extension would get through the
     * upload process, but just in case we catch those situations without breaking and with some fallback sanitizing to
     * make sure files are unique and as such unzippable on all OS.
     */
    public function getCompressSafeFileName(): string
    {
        // get rid of special characters (except dot, underscore and hyphen) that might cause unzip problems on certain OS
        $sanitizedFileName = preg_replace('/[^a-z0-9A-Z._-]/', '-', $this->getOriginalName()) ?? '';

        // in case of special chars replaced by hyphens at start of string, drop those and any space that might result of that trimming.
        $sanitizedFileName = ltrim($sanitizedFileName, '- ');

        // get position of last dot (extension's dot)
        $lastDotPosition = mb_strrpos($sanitizedFileName, '.');

        $timestampString = (string) $this->getSavedAt()->getTimestamp();

        // in the unlikely event the filename has no dot - thus no extension - don't break, return as is with unique prefix
        if (false === $lastDotPosition) {
            return $timestampString.'-'.$sanitizedFileName;
        }

        // split filename up in its name part and extension part
        $name = mb_substr($sanitizedFileName, 0, $lastDotPosition);
        $extension = mb_substr($sanitizedFileName, $lastDotPosition + 1);

        // it's unlikely that one or both parts are empty strings. an empty name part is no problem, but since it's cleaner
        // to visually separate the name part from the unique suffix we'll add, using a hyphen as delimiter,  we'll only
        // add that hyphen if name is not empty. in the even more unlikely event the extension part is an empty string
        // (i.e. original filename ended with a dot), let's not add the dot (back) at the end. all this is unlike to
        // occur, but we need to make sure files are unzippable on all OS.
        $result = '' === $name ? $timestampString : $name.'-'.$timestampString;

        if ('' === $extension) {
            return $result;
        }

        return $result.'.'.$extension;
    }

    public function getReadableSize(int $precision = 2, string $prefixes = 'decimal'): string
    {
        return $this->getUploadedFile()->getReadableSize($precision, $prefixes);
    }

    public function getSavedAt(): \DateTimeImmutable
    {
        return $this->getUploadedFile()->getSavedAt();
    }

    /**
     * Specify data which should be serialized to JSON.
     *
     * @return array{uid: string, nodeUid: string, uploadedFile: mixed[]}
     */
    public function jsonSerialize(): array
    {
        return [
            'uid' => $this->getUid(),
            'nodeUid' => $this->getNodeUid(),
            'uploadedFile' => $this->getUploadedFile()->jsonSerialize(),
        ];
    }
}
