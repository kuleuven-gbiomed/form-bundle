<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity;

use Doctrine\ORM\EntityRepository;
use KUL\FormBundle\Entity\Upload\UploadedFileListener;

/**
 * @extends EntityRepository<UploadedFile>
 */
class UploadedFileRepository extends EntityRepository
{
    /**
     * Deletes uploaded file & removes file itself from storage.
     *
     * the associated file is removed from storage via two standard entity listener events:
     * - @see UploadedFileListener::preRemove() will ensure the entity is initialized (to solve Doctrine issue in postRemove no longer finding hydrated UploadedFile entity)
     * - @see UploadedFileListener::postRemove() will delete the file from storage, if it (still) exists.
     */
    public function delete(UploadedFile $file): void
    {
        $this->getEntityManager()->remove($file);
        $this->getEntityManager()->flush();
    }
}
