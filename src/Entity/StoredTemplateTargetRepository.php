<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Parameter;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\TemplateTarget\Contract\StoredTemplateTargetInterface;

/**
 * @extends EntityRepository<StoredTemplateTarget>
 */
class StoredTemplateTargetRepository extends EntityRepository
{
    public function getOneOrNullByTargetAndTemplate(
        TemplateTargetableInterface $target,
        Template $template,
    ): ?StoredTemplateTarget {
        $qb = $this->createQueryBuilder('stt');
        $qb->select('stt');

        /** @psalm-suppress TooManyArguments andX is a weird Doctrine variadic function */
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq('stt.template', ':template'),
            $qb->expr()->eq('stt.targetCode', ':targetCode'),
            $qb->expr()->eq('stt.targetId', ':targetId')
        ));

        /** @psalm-var ArrayCollection<array-key, mixed> $arrColl */
        $arrColl = new ArrayCollection([
            new Parameter('template', $template),
            new Parameter('targetCode', $target::getTargetCode()),
            new Parameter('targetId', $target->getTargetIdentifier()),
        ]);
        $qb->setParameters($arrColl);

        /** @var StoredTemplateTarget|null $result */
        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }

    public function getOneOrNullByStoredTemplateTargetId(
        string $storedTemplateTargetId,
    ): ?StoredTemplateTarget {
        $qb = $this->createQueryBuilder('stt');
        $qb->select('stt');

        $qb->where($qb->expr()->eq('stt.id', ':storedTemplateTargetId'));

        /** @psalm-var ArrayCollection<array-key, mixed> $arrColl */
        $arrColl = new ArrayCollection([new Parameter('storedTemplateTargetId', $storedTemplateTargetId)]);
        $qb->setParameters($arrColl);

        /** @var StoredTemplateTarget|null $result */
        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }

    public function hasForTemplate(string $templateId): bool
    {
        $qb = $this->createQueryBuilder('stt');
        $qb->select('count(stt)');
        $qb->where($qb->expr()->eq('stt.template', ':template'));

        $qb->setParameter(':template', $templateId);

        $count = $qb->getQuery()->getSingleScalarResult();

        return $count > 0;
    }

    /** @return list<StoredTemplateTarget> */
    public function allForTemplate(Template $template): array
    {
        $query = $this->createQueryBuilder('stt')
            ->select('stt')
            ->where('stt.template = :templateId')
            ->setParameter(':templateId', $template->getId());

        return $query->getQuery()->getResult();
    }

    /** @return list<StoredTemplateTarget> */
    public function allForTemplateId(string $templateId): array
    {
        $query = $this->createQueryBuilder('stt')
            ->select('stt')
            ->where('stt.template = :templateId')
            ->setParameter(':templateId', $templateId);

        return $query->getQuery()->getResult();
    }

    public function save(StoredTemplateTargetInterface $target): void
    {
        $em = $this->getEntityManager();

        $em->persist($target);
        $em->flush();
    }

    public function delete(StoredTemplateTargetInterface $target): void
    {
        $em = $this->getEntityManager();

        $em->remove($target);
        $em->flush();
    }
}
