<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity;

use KUL\FormBundle\Domain\Template\AbstractJsonAwareTemplate;
use KUL\FormBundle\Domain\Template\Contract\TemplateInterface;

/**
 * Class Template.
 *
 * generic way of configuring a @see TemplateInterface for building forms with questions, workflow, role based options, etc.
 */
class Template extends AbstractJsonAwareTemplate
{
}
