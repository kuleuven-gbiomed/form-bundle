<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity;

use KUL\FormBundle\Client\AnswerValue\Value;
use KUL\FormBundle\Client\AnswerValue\ValueCollection;
use KUL\FormBundle\Client\History\HistoryDateFormatter;
use KUL\FormBundle\Client\History\InputNodeSubmitEntry;
use KUL\FormBundle\Client\History\SubmitHistory;
use KUL\FormBundle\Client\Submission\SubmissionCollection;
use KUL\FormBundle\Domain\Contract\FormUserInterface;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\InputNode;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Utility\Role;
use Ramsey\Uuid\Uuid;

/**
 * Values submitted by a user, who filled out a symfony form based on a template.
 *
 * @see     SubmissionCollection
 */
class Submission
{
    private readonly string $id;
    /**
     * refers to step in @see WorkFlow in which user has saved/submitted.
     */
    private string $stepUid;
    private readonly string $role;
    private ?\DateTimeImmutable $originalSubmitDateBeforeRollback = null;

    private function __construct(
        private readonly string $submitterId,
        private readonly string $submitterFullName,
        private readonly int $versionNumber,
        private string $values,
        string $role,
        string $stepUid,
        private \DateTimeImmutable $submitDate,
        private readonly bool $autoSaved = false,
    ) {
        if ('' === $role) {
            throw new \InvalidArgumentException('submission role must be a non empty string.');
        }

        if ('' === $stepUid) {
            throw new \InvalidArgumentException('submission stepUid must be a non empty string.');
        }

        $this->id = Uuid::uuid4()->toString();
        $this->stepUid = $stepUid;
        $this->role = $role;
    }

    public static function createFromUserWithRoleInStepForValuesByVersionOnDate(
        FormUserInterface $user,
        Role $role,
        StepInterface $step,
        ValueCollection $values,
        int $versionNumber,
        \DateTimeImmutable $date,
        bool $autoSaved,
    ): self {
        return new self(
            $user->getIdentifier(),
            $user->getPerson()->getFullName(),
            $versionNumber,
            self::getValuesString($values),
            $role->getName(),
            $step->getUid(),
            $date,
            $autoSaved
        );
    }

    public static function createFromDataWithValues(
        string $submitterId,
        string $submitterFullName,
        int $versionNumber,
        ValueCollection $values,
        string $role,
        string $stepUid,
        \DateTimeImmutable $submitDate,
        bool $autoSaved = false,
    ): self {
        return new self(
            $submitterId,
            $submitterFullName,
            $versionNumber,
            self::getValuesString($values),
            $role,
            $stepUid,
            $submitDate,
            $autoSaved
        );
    }

    public function convertForRollbackByDate(\DateTimeImmutable $rollbackDate): void
    {
        // the original submitDate is stored redundantly to indicate submission was updated by rollback.
        $this->originalSubmitDateBeforeRollback = clone $this->submitDate;
        // the new submitDate is now the one from Rollback, so that the chronology of logs can be dealt with consequently (on submit dates).
        $this->submitDate = $rollbackDate;
    }

    public function isConvertedByRollback(): bool
    {
        return $this->originalSubmitDateBeforeRollback instanceof \DateTimeImmutable;
    }

    private static function getValuesString(ValueCollection $values): string
    {
        return json_encode($values, \JSON_THROW_ON_ERROR);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getSubmitterId(): string
    {
        return $this->submitterId;
    }

    public function getSubmitterFullName(): string
    {
        return $this->submitterFullName;
    }

    public function getVersionNumber(): int
    {
        return $this->versionNumber;
    }

    public function getSubmitDate(): \DateTimeImmutable
    {
        return $this->submitDate;
    }

    public function getSubmitDateString(?string $locale = null): string
    {
        return HistoryDateFormatter::format($this->getSubmitDate(), $locale);
    }

    public function getOriginalSubmitDateBeforeRollback(): \DateTimeImmutable
    {
        if (!$this->originalSubmitDateBeforeRollback instanceof \DateTimeImmutable) {
            throw new \Exception('no originalSubmitDateBeforeRollback found in submission!');
        }

        return $this->originalSubmitDateBeforeRollback;
    }

    public function getOriginalSubmitDateBeforeRollbackString(?string $locale = null): string
    {
        return HistoryDateFormatter::format($this->getOriginalSubmitDateBeforeRollback(), $locale);
    }

    public function getStepUid(): string
    {
        return $this->stepUid;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getValues(): ValueCollection
    {
        $values = [];
        foreach (json_decode($this->values, true, 512, \JSON_THROW_ON_ERROR) as $item) {
            $values[] = Value::createFromArray($item);
        }

        return new ValueCollection($values);
    }

    /**
     * is this submission the consequence of an auto save operation.
     */
    public function isAutoSaved(): bool
    {
        return $this->autoSaved;
    }

    /**
     * @throws \Exception
     */
    public function getSubmitHistoryForFormList(FormList $formList): SubmitHistory
    {
        $entries = [];
        /** @var Value $value */
        foreach ($this->getValues() as $value) {
            if (!$formList->hasNodeByUid($value->getUid())) {
                continue;
            }

            $node = $formList->getNodeByUid($value->getUid());
            if (!$node instanceof InputNode) {
                continue;
            }

            if (!$node instanceof ParentalInputNode) {
                continue;
            }

            $originalSubmitDateBeforeRollback = null;
            if ($this->isConvertedByRollback()) {
                $originalSubmitDateBeforeRollback = $this->getOriginalSubmitDateBeforeRollback();
            }

            $entries[] = InputNodeSubmitEntry::fromNodeAndDateAndValue(
                $node,
                $this->getSubmitDate(),
                $value->getValue(),
                $this->getSubmitterId(),
                $this->getSubmitterFullName(),
                $this->getStepUid(),
                $this->getRole(),
                $value->isAutoAdded(),
                $this->isAutoSaved(),
                $originalSubmitDateBeforeRollback
            );
        }

        return new SubmitHistory($entries);
    }

    /**
     * update an existing submission marked for auto save by another (i.e. more recent) submission marked for auto save,
     * for the same submitter (user identifier), role, step and (template) version.
     * this allows to avoid database overload with many submissions for auto save for the same UNIQUE combination of
     * user-role-step-version auto saving every x minutes for the same @see StoredTemplateTarget
     * note: submissions (history logs about form actions) should never be changed. but this is an exception required
     * and necessary for database capacity reasons, and it only allows for auto saved actions, no manually saved actions
     * note: the submitters id must be the same but the full name of the submitter can be different (in case of name
     * changes. the submitter full name is pure metadata, no identifying data).
     */
    public function updateBySubmissionForAutoSave(self $submission): void
    {
        if (!$this->isAutoSaved()) {
            throw new \DomainException('cannot update for autosave: source submission is not marked as autoSaved.');
        }

        if (!$submission->isAutoSaved()) {
            throw new \DomainException('cannot update for autosave: given submission is not marked as autoSaved.');
        }

        if ($this->getSubmitterId() !== $submission->getSubmitterId()
            || $this->getRole() !== $submission->getRole()
            || $this->getStepUid() !== $submission->getStepUid()
            || $this->getSubmitterId() !== $submission->getSubmitterId()
        ) {
            throw new \DomainException('cannot update for autosave: identifying auto save data of source and given submissions do not match.');
        }

        $this->submitDate = $submission->getSubmitDate();
        $this->values = self::getValuesString($submission->getValues());
    }

    public function fixStepUid(string $stepUid): void
    {
        $this->stepUid = $stepUid;
    }
}
