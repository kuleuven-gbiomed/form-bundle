<?php

declare(strict_types=1);

namespace KUL\FormBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * @extends EntityRepository<SavedFile>
 */
class SavedFileRepository extends EntityRepository
{
    public function delete(SavedFile $savedFile): void
    {
        $em = $this->getEntityManager();

        $em->remove($savedFile);
        $em->flush();
    }
}
