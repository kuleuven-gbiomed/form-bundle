<?php

declare(strict_types=1);

namespace KUL\FormBundle\Twig\Extensions;

use KUL\FormBundle\Client\Manage\QuestionLockChecker;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Contract\ParentalInputNode;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class QuestionLockExtension extends AbstractExtension
{
    public function __construct(private readonly QuestionLockChecker $questionLockChecker)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('kul_form_hasBeenAnswered', $this->hasBeenAnswered(...)),
            new TwigFunction('kul_form_hasAnsweredLockedChildren', $this->hasAnsweredLockedChildren(...)),
        ];
    }

    public function hasAnsweredLockedChildren(CategoryNode $category, array $submittedValues): bool
    {
        foreach ($category->getChildren() as $child) {
            if ($child instanceof CategoryNode) {
                if ($this->hasAnsweredLockedChildren($child, $submittedValues)) {
                    return true;
                }
            } elseif ($child instanceof ParentalInputNode) {
                if ($this->hasBeenAnswered($child, $submittedValues)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function hasBeenAnswered(ParentalInputNode $node, array $submittedValues): bool
    {
        return $this->questionLockChecker->isAnsweredQuestionUnlocked($node, $submittedValues);
    }
}
