<?php

declare(strict_types=1);

namespace KUL\FormBundle\Twig\Extensions;

use KUL\FormBundle\Client\Util\File\AbstractFileUrl;
use KUL\FormBundle\Client\Util\File\DownloadFileUrl;
use KUL\FormBundle\Entity\SavedFile;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class SubmittedFileExtension.
 *
 * Disclaimer: I cluelessly copied this from SCONE, thereby replacing
 * SubmittedFile by SavedFile, and replacing deprecated twig classes by new versions.
 */
final class SubmittedFileExtension extends AbstractExtension
{
    /** @var string */
    public const PLACEHOLDER_FILE_UID = AbstractFileUrl::SAVED_FILE_UID_PLACEHOLDER;
    /** @var string */
    public const PLACEHOLDER_DOWNLOAD_URL = '__DOWNLOAD_URL__';
    /** @var string */
    public const PLACEHOLDER_FILE_NAME = '__FILE_NAME__';
    /** @var string */
    public const PLACEHOLDER_FILE_SIZE = '__FILE_SIZE__';

    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'kul_form_render_read_only_submitted_file_table_row',
                $this->renderReadOnlySubmittedFileTableRow(...),
                ['needs_environment' => true]
            ),
            new TwigFunction(
                'kul_form_render_manageable_submitted_file_table_row',
                $this->renderManageableSubmittedFileTableRow(...),
                ['needs_environment' => true]
            ),
            new TwigFunction(
                'kul_form_render_manageable_prototype_table_row',
                $this->renderManageablePrototypeTableRow(...),
                ['needs_environment' => true]
            ),
        ];
    }

    /**
     * render a template/prototype HTML tr tag for a given @param Environment $environment.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     *
     * @see InputNode::$uid that will allow managing (deleting)
     * the submitted file for which the template will be used
     */
    public function renderManageablePrototypeTableRow(Environment $environment, string $nodeUid): string
    {
        return $this->renderFileTableRow($environment, $nodeUid, true);
    }

    /**
     * render a HTML tr tag for a given submitted file.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderReadOnlySubmittedFileTableRow(
        Environment $environment,
        SavedFile $submittedFile,
        DownloadFileUrl $downloadFileUrl,
    ): string {
        return $this->renderSubmittedFileTableRow($environment, $submittedFile, $downloadFileUrl, false);
    }

    /**
     * render a HTML tr tag for a given submitted file that does allow managing (deleting) that submitted file.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderManageableSubmittedFileTableRow(
        Environment $environment,
        SavedFile $submittedFile,
        DownloadFileUrl $downloadFileUrl,
    ): string {
        return $this->renderSubmittedFileTableRow($environment, $submittedFile, $downloadFileUrl, true);
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function renderSubmittedFileTableRow(
        Environment $environment,
        SavedFile $submittedFile,
        DownloadFileUrl $downloadFileUrl,
        bool $allowManage,
    ): string {
        return $this->renderFileTableRow(
            $environment,
            $submittedFile->getNodeUid(),
            $allowManage,
            $submittedFile->getUid(),
            $downloadFileUrl->getUrlBySavedFile($submittedFile),
            $submittedFile->getOriginalName(),
            $submittedFile->getReadableSize()
        );
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function renderFileTableRow(
        Environment $environment,
        string $nodeUid,
        bool $allowManage,
        string $fileUid = self::PLACEHOLDER_FILE_UID,
        string $downloadUrl = self::PLACEHOLDER_DOWNLOAD_URL,
        string $fileName = self::PLACEHOLDER_FILE_NAME,
        string $fileSize = self::PLACEHOLDER_FILE_SIZE,
    ): string {
        if ('' === $nodeUid) {
            throw new \InvalidArgumentException('nodeuid can not be an empty string');
        }

        if ('' === $fileUid) {
            throw new \InvalidArgumentException('fileUid can not be an empty string');
        }

        if ('' === $downloadUrl) {
            throw new \InvalidArgumentException('downloadUrl can not be an empty string');
        }

        if ('' === $fileName) {
            throw new \InvalidArgumentException('fileName can not be an empty string');
        }

        if ('' === $fileSize) {
            throw new \InvalidArgumentException('fileSize can not be an empty string');
        }

        return $environment->render('@KULForm/embed/upload/table-row.html.twig', [
            'nodeUid' => $nodeUid,
            'fileUid' => $fileUid,
            'allowManage' => $allowManage,
            'downloadUrl' => $downloadUrl,
            'name' => $fileName,
            'size' => $fileSize,
        ]);
    }
}
