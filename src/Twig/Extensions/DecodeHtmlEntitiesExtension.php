<?php

declare(strict_types=1);

namespace KUL\FormBundle\Twig\Extensions;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class DecodeHtmlEntitiesExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('kul_form_decode_html_entities', $this->decodeHtmlEntities(...)),
        ];
    }

    public function decodeHtmlEntities(?string $text): ?string
    {
        return !is_string($text) ? $text : html_entity_decode($text);
    }
}
