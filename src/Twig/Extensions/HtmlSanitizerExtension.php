<?php

declare(strict_types=1);

/*
 * This adapted from https://github.com/symfony/symfony/blob/33d464470c991b4b5a5453d3a32c4fe427706c61/src/Symfony/Bridge/Twig/Extension/HtmlSanitizerExtension.php
 * It won't be necessary after we move to symfony 6 or higher
 * We should adapt it to work with the default service
 */

namespace KUL\FormBundle\Twig\Extensions;

use Symfony\Component\HtmlSanitizer\HtmlSanitizer;
use Symfony\Component\HtmlSanitizer\HtmlSanitizerConfig;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class HtmlSanitizerExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('sanitize_html_custom', $this->sanitize(...), ['is_safe' => ['html']]),
        ];
    }

    public function sanitize(string $html): string
    {
        $htmlSanitizer = new HtmlSanitizer(
            (new HtmlSanitizerConfig())->allowSafeElements()->withMaxInputLength(-1)
        );

        return $htmlSanitizer->sanitize($html);
    }
}
