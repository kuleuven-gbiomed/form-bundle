<?php

declare(strict_types=1);

namespace KUL\FormBundle\Twig\Extensions;

use Symfony\Component\Form\FormView;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class FormViewFinderExtension.
 *
 * Disclaimer: I cluelessly copied this from SCONE.
 */
class FormViewFinderExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('kul_form_find_form_view_by_node_uid', $this->findFormViewByNodeUid(...)),
        ];
    }

    /**
     * @param string $nodeUid
     *
     * @return FormView|string|null
     */
    public function findFormViewByNodeUid(FormView $baseFormView, $nodeUid)
    {
        if (0 === count($baseFormView->children)) {
            return null;
        }

        return $this->getFormViewByNodeUid($baseFormView, $nodeUid);
    }

    /**
     * find formView by nodeUid
     * every single node build by FormList in a form has a unique uid
     * including the ChoiceType's option formViews. so a simple check on 'name'
     * (or key in formView children) is sufficient. no need to check parent FormViews here.
     *
     * WARNING: this method will only work with forms rendered by formList with flattened field names as node uids
     *
     * @param string $nodeUid
     *
     * @return FormView|null
     */
    protected function getFormViewByNodeUid(FormView $baseFormView, $nodeUid)
    {
        $result = null;
        foreach ($baseFormView->children as $formView) {
            if ($formView->vars['name'] === $nodeUid) {
                $result = $formView;
                break;
            }

            $childFormView = $this->getFormViewByNodeUid($formView, $nodeUid);
            if ($childFormView instanceof FormView) {
                $result = $childFormView;
                break;
            }
        }

        return $result;
    }
}
