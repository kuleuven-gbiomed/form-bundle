<?php

declare(strict_types=1);

namespace KUL\FormBundle\Twig\Extensions;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class StepTypeCheckerExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('kul_form_stepUsesFixedDates', $this->stepUsesFixedDates(...)),
            new TwigFunction('kul_form_stepUsesCalculatedDates', $this->stepUsesCalculatedDates(...)),
        ];
    }

    public function stepUsesFixedDates(StepInterface $step): bool
    {
        return $step instanceof StepWithFixedDateStart;
    }

    public function stepUsesCalculatedDates(StepInterface $step): bool
    {
        return $step instanceof StepWithCalculatedDateStart;
    }
}
