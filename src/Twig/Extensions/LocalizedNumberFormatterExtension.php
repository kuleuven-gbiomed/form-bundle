<?php

declare(strict_types=1);

namespace KUL\FormBundle\Twig\Extensions;

use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Rounding\RoundingModes;
use KUL\FormBundle\Utility\LocalizedNumberFormatter;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class LocalizedNumberFormatterExtension extends AbstractExtension
{
    public function __construct(private readonly string $defaultLocale)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('kul_form_localizedNumberFormat', $this->localizedNumberFormat(...)),
            new TwigFunction('kul_form_localizedChoiceOptionScoringValueNumberFormat', $this->localizedChoiceOptionScoringValueNumberFormat(...)),
        ];
    }

    /**
     * @param mixed $value could be null, empty string, int/float or numeric string representing int/float
     *
     * @phpstan-param RoundingModes::ROUND_* $roundingMode
     *
     * @throws \Exception
     */
    public function localizedNumberFormat(
        mixed $value,
        ?string $locale,
        ?int $minimumScale = null,
        ?int $exactScale = null,
        ?int $roundingMode = null,
    ): string {
        // if null or empty string, don't go formatting the value! because that would return 0 (zero) but we use this
        // to display number answers on questions and answer 0 (zero) is NOT equal to not answered (empty string)
        if (!is_numeric($value)) {
            return '';
        }

        if (null === $locale || '' === $locale) {
            $locale = $this->defaultLocale;
        }

        return LocalizedNumberFormatter::format($value, $locale, $minimumScale, $exactScale, $roundingMode);
    }

    public function localizedChoiceOptionScoringValueNumberFormat(OptionInputNode $optionNode, ?string $locale): string
    {
        if (!$optionNode->hasScoringValue()) {
            throw new \InvalidArgumentException('option node['.$optionNode->getUid().']['.$optionNode->getNestedLabel($this->defaultLocale).'] has no scoring value to format');
        }

        $value = $optionNode->getScoringValue();

        // if the choice question operates as a score and/or global score question, or it simply has some scoring
        // parameters set to make use of it for rendering purposes, use those parameters for scale & rounding (if set).
        /** @var ChoiceInputNode $choiceQuestion */
        $choiceQuestion = $optionNode->getParent();
        $scoringParameters = $choiceQuestion->getInputScoringParameters();
        $minimumScale = $scoringParameters->hasScale() ? $scoringParameters->getScale() : null;
        $roundingMode = $scoringParameters->hasRoundingMode() ? $scoringParameters->getRoundingMode() : null;

        // use the scale on the parent choice question as minimal scale (if set). not using exact scale here, in case
        // the scoringvalue intentionally has been set with a scoring value with more decimals.
        return $this->localizedNumberFormat($value, $locale, $minimumScale, null, $roundingMode);
    }
}
