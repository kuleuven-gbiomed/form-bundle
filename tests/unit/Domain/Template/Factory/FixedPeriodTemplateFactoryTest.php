<?php

declare(strict_types=1);
/** @noinspection PhpUnhandledExceptionInspection */

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Factory;

use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Factory\FixedPeriodTemplateFactory;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

final class FixedPeriodTemplateFactoryTest extends TestCase
{
    public function testCreateTemplate(): void
    {
        // FIXME: It seems I'm using an obsolete TranslatorInterface.
        // see https://jira.gbiomed.kuleuven.be/browse/FORM-134.
        /** @var MockObject|TranslatorInterface $translatorStub */
        $translatorStub = $this->createMock(TranslatorInterface::class);
        $translatorStub->method('trans')->willReturn('Step');

        $factory = new FixedPeriodTemplateFactory('en', $translatorStub);

        $uuid = 'ab388526-c8d2-4dd7-b2c5-d001dafd2d61';
        $roles = ['RESPONDENT_ROLE', 'ADMIN_ROLE'];

        $template = $factory->createTemplate(
            $uuid,
            'My form',
            'some_target_type',
            [
                new \DateTimeImmutable('2019-05-01'),
                new \DateTimeImmutable('2019-05-11'),
                new \DateTimeImmutable('2019-05-21'),
            ],
            $roles
        );

        $workingVersion = $template->getWorkingVersion();
        $formInfo = $workingVersion->getInfo();
        $workflow = $workingVersion->getWorkFlow();
        $steps = $workflow->getSteps();

        static::assertEquals($uuid, $template->getId());
        static::assertEquals('My form', $formInfo->getLabel('en'));
        static::assertEquals('some_target_type', $template->getTargetingType());
        static::assertEquals(RoleCollection::fromRoleNames($roles), $template->getHierarchicalRolesThatCouldParticipate());
        static::assertInstanceOf(StepWithFixedDateStartCollection::class, $steps);
        static::assertEquals(2, $steps->count());

        /** @var StepWithFixedDateStart[] $stepArray */
        $stepArray = array_values($steps->toArray());
        static::assertEquals(
            StepFixedPeriod::fromDates(
                new \DateTimeImmutable('2019-05-01'),
                new \DateTimeImmutable('2019-05-11')
            ),
            $stepArray[0]->getPeriod()
        );

        static::assertEquals(
            StepFixedPeriod::fromDates(
                new \DateTimeImmutable('2019-05-11'),
                new \DateTimeImmutable('2019-05-21')
            ),
            $stepArray[1]->getPeriod()
        );

        static::assertEquals('Step 1', $stepArray[0]->getLabel('nl'));
        static::assertEquals('Step 2', $stepArray[1]->getLabel('nl'));
    }
}
