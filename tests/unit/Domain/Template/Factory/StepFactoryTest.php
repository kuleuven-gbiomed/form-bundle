<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Factory;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Factory\StepFactory;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class StepFactoryTest.
 */
class StepFactoryTest extends TestCase
{
    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Factory\StepFactory::createFromSerializedArray()
     */
    public function testCreateFromSerializedArrayShouldReturnStepWithCalculatedDateStart(): void
    {
        $serializedData = SerializedDataFactory::createStepWithCalculatedDateStartArray('some_uid');

        static::assertInstanceOf(StepWithCalculatedDateStart::class, StepFactory::createFromSerializedArray($serializedData));
    }

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Factory\StepFactory::createFromSerializedArray()
     */
    public function testCreateFromSerializedArrayShouldReturnStepWithCalculatedDateOrPreviousFulFilledStart(): void
    {
        $serializedData = SerializedDataFactory::createStepWithCalculatedDateOrPreviousFulFilledStartArray(
            'some_uid'
        );

        static::assertInstanceOf(StepWithCalculatedDateStart::class, StepFactory::createFromSerializedArray($serializedData));
    }

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Factory\StepFactory::createFromSerializedArray()
     */
    public function testCreateFromSerializedArrayShouldReturnStepWithFixedDateStart(): void
    {
        $serializedData = SerializedDataFactory::createStepWithFixedDateStartArray('some_uid', new \DateTimeImmutable('2019-05-21 13:30'));

        static::assertInstanceOf(StepWithFixedDateStart::class, StepFactory::createFromSerializedArray($serializedData));
    }

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     * @throws \Exception
     *
     * @covers \KUL\FormBundle\Domain\Template\Factory\StepFactory::createFromSerializedArray()
     */
    public function testCreateFromSerializedArrayShouldReturnStepWithFixedDateOrPreviousFulFilledStart(): void
    {
        $serializedData = SerializedDataFactory::createStepWithFixedDateOrPreviousFulFilledStartArray(
            'some_uid',
            new \DateTimeImmutable('2017-05-21 14:00')
        );

        static::assertInstanceOf(StepWithFixedDateStart::class, StepFactory::createFromSerializedArray($serializedData));
    }
}
