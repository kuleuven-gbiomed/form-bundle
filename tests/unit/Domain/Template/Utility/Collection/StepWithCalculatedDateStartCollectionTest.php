<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Utility\Collection;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class StepWithCalculatedDateStartCollectionTest.
 *
 * @covers  \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection
 */
class StepWithCalculatedDateStartCollectionTest extends TestCase
{
    /** @var StepWithCalculatedDateStart */
    private $stepWithCalDateStartOne;
    /** @var StepWithCalculatedDateStart */
    private $stepWithCalDateStartTwo;
    /** @var StepWithCalculatedDateStart */
    private $stepWithCalDateStartThree;
    /** @var StepWithCalculatedDateStart */
    private $stepWithCalDateStartFourHavingOverlapWithTwo;

    /** @var \DateTimeImmutable */
    private $referenceDate;

    protected function setUp(): void
    {
        parent::setUp();

        $this->referenceDate = new \DateTimeImmutable('2019-05-20 10:00');

        $this->stepWithCalDateStartOne = ValueObjectFactory::createStepWithCalculatedDateStart(
            'uid_cal_1',
            -20,
            -10
        );
        $this->stepWithCalDateStartTwo = ValueObjectFactory::createStepWithCalculatedDateStart(
            'uid_cal_2',
            -10,
            0
        );
        $this->stepWithCalDateStartThree = ValueObjectFactory::createStepWithCalculatedDateOrPreviousFulFilledStart(
            'uid_cal_3',
            0,
            10
        );
        $this->stepWithCalDateStartFourHavingOverlapWithTwo = ValueObjectFactory::createStepWithCalculatedDateStart(
            'uid_cal_4',
            -5,
            0
        );
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::__construct
     */
    public function testConstructWithValidInstancesShouldReturnInstance(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        static::assertInstanceOf(StepWithCalculatedDateStartCollection::class, $result);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::__construct
     */
    public function testConstructWithMixedStepTypesShouldThrowException(): void
    {
        $this->expectException(\Exception::class);

        new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            ValueObjectFactory::createStepWithFixedDateOrPreviousFulFilledStartByStartDateAndOffset(
                'uid_fix_3',
                new \DateTimeImmutable('2019-05-22 9:00'),
                10
            ),
        ]);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::__construct
     */
    public function testConstructWithDuplicateUidsShouldThrowInvalidArgumentException(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            ValueObjectFactory::createStepWithCalculatedDateStart(
                $this->stepWithCalDateStartOne->getUid(),
                -20,
                -10
            ),
        ]);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::fromJsonDecodedArray()
     */
    public function testFromSerializedArray(): void
    {
        $data = [
            SerializedDataFactory::createStepWithCalculatedDateStartArray('uid_1'),
            SerializedDataFactory::createStepWithCalculatedDateStartArray('uid_2'),
            SerializedDataFactory::createStepWithCalculatedDateStartArray('uid_3'),
            SerializedDataFactory::createStepWithCalculatedDateStartArray('uid_4'),
        ];

        static::assertInstanceOf(StepWithCalculatedDateStartCollection::class, StepWithCalculatedDateStartCollection::fromJsonDecodedArray($data));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::hasSteps()
     */
    public function testHasSteps(): void
    {
        $result = new StepWithCalculatedDateStartCollection();
        static::assertFalse($result->hasSteps());

        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);
        static::assertTrue($result->hasSteps());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::hasOneStepThatDirectlyPrecedesStep()
     */
    public function testHasOneStepThatDirectlyPrecedesStep(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertTrue($result->hasOneStepThatDirectlyPrecedesStep($this->stepWithCalDateStartTwo));
        static::assertFalse($result->hasOneStepThatDirectlyPrecedesStep($this->stepWithCalDateStartOne));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::hasOneStepThatDirectlyPrecedesStep()
     */
    public function testHasOneStepThatDirectlyPrecedesStepWithUnknownStepThrowInvalidArgumentException(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartThree,
        ]);

        $this->expectException(\InvalidArgumentException::class);

        $result->hasOneStepThatDirectlyPrecedesStep($this->stepWithCalDateStartTwo);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::withNewStepAddedAtEnd()
     */
    public function testWithAddedStep(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
        ]);

        $result = $steps->withNewStepAddedAtEnd($this->stepWithCalDateStartThree);

        static::assertCount(3, $result);
        static::assertTrue($result->hasOneStepByUid($this->stepWithCalDateStartThree->getUid()));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::withRemovedStep()
     */
    public function testWithRemovedStep(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        $result = $steps->withRemovedStep($this->stepWithCalDateStartThree);

        static::assertCount(2, $result);
        static::assertFalse($result->hasOneStepByUid($this->stepWithCalDateStartThree->getUid()));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::withUpdatedStep()
     */
    public function testWithUpdatedStep(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertTrue($this->stepWithCalDateStartThree->allowsToStartPrematurelyIfPreviousStepIsFulFilled());

        $result = $steps->withUpdatedStep($this->stepWithCalDateStartThree->withoutPreviousStepFulFilledStartOption());

        static::assertCount(3, $result);
        static::assertTrue($result->hasOneStepByUid($this->stepWithCalDateStartThree->getUid()));
        static::assertFalse($result->getOneStepByUid($this->stepWithCalDateStartThree->getUid())->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
    }

    public function testFindStepsThatAreOpenByDate(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        $result = $steps->findOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('-25 days'),
            $this->referenceDate->modify('-25 days')
        );
        static::assertNull($result);

        $result = $steps->findOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('-6 days'),
            $this->referenceDate->modify('-6 days')
        );
        static::assertInstanceOf(StepWithCalculatedDateStart::class, $result);
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $result->getUid());

        $result = $steps->findOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('-4 days'),
            $this->referenceDate->modify('-4 days')
        );
        static::assertInstanceOf(StepWithCalculatedDateStart::class, $result);
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $result->getUid());

        $result = $steps->findOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('-1 days'),
            $this->referenceDate->modify('-1 days')
        );
        static::assertInstanceOf(StepWithCalculatedDateStart::class, $result);
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $result->getUid());

        $result = $steps->findOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+6 days'),
            $this->referenceDate->modify('+6 days')
        );
        static::assertInstanceOf(StepWithCalculatedDateStart::class, $result);
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $result->getUid());

        $result = $steps->findOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+25 days'),
            $this->referenceDate->modify('+25 days')
        );
        static::assertNull($result);
    }

    public function testFindOpenStepShouldThrowExceptionWhenThereAreMultipleOpenSteps(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        $this->expectException(\BadMethodCallException::class);
        $steps->findOpenStep($this->referenceDate->modify('-1 day'), $this->referenceDate, $this->referenceDate);
    }

    public function testFindOpenStepShouldThrowExceptionWhenThereAreMultipleOpenSteps2(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        // step two and four open: now > startdate and NOT now > enddate
        $this->expectException(\BadMethodCallException::class);
        $result = $steps->findOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+4 days'),
            $this->referenceDate->modify('+4 days')
        );
    }

    public function testFindOpenStepShouldThrowExceptionWhenThereAreMultipleOpenSteps3(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        // if now == startdatum of step, then the step is open.
        $this->expectException(\BadMethodCallException::class);
        $result = $steps->findOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+5 days'),
            $this->referenceDate->modify('+5 days')
        );
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection
     *
     * @throws \Exception
     */
    public function testHasOpenStep(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        static::assertFalse($steps->hasOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('-25 days'),
            $this->referenceDate->modify('-25 days')
        ));

        static::assertTrue($steps->hasOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('-6 days'),
            $this->referenceDate->modify('-6 days')
        ));
        static::assertTrue($steps->hasOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('-4 days'),
            $this->referenceDate->modify('-4 days')
        ));
        static::assertTrue($steps->hasOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('-1 days'),
            $this->referenceDate->modify('-1 days')
        ));
        static::assertTrue($steps->hasOpenStep($this->referenceDate, $this->referenceDate, $this->referenceDate));
        static::assertTrue($steps->hasOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+6 days'),
            $this->referenceDate->modify('+6 days')
        ));

        static::assertFalse($steps->hasOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+25 days'),
            $this->referenceDate->modify('+25 days')
        ));
    }

    public function testHasOpenStepShouldThrowWhenThereAreMultipleOpenSteps(): void
    {
        // I wonder if creating this kind of step collection should be allowed.
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        $this->expectException(\BadMethodCallException::class);
        static::assertTrue($steps->hasOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+1 days'),
            $this->referenceDate->modify('+1 days')
        ));
    }

    public function testHasOpenStepShouldThrowWhenThereAreMultipleOpenSteps2(): void
    {
        // I wonder if creating this kind of step collection should be allowed.
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        $this->expectException(\BadMethodCallException::class);
        static::assertTrue($steps->hasOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+4 days'),
            $this->referenceDate->modify('+4 days')
        ));
    }

    public function testHasOpenStepShouldThrowWhenThereAreMultipleOpenSteps3(): void
    {
        // I wonder if creating this kind of step collection should be allowed.
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        // step 4 has just started. step 2 is still open.
        $this->expectException(\BadMethodCallException::class);
        static::assertTrue($steps->hasOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+5 days'),
            $this->referenceDate->modify('+5 days')
        ));
    }

    public function testFindOneStepThatIsOpenByDateForStepsWithoutOverlapShouldFindOneStepForSpecificDate(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        $found = $steps->findOpenStep($this->referenceDate, $this->referenceDate, $this->referenceDate);
        static::assertInstanceOf(StepInterface::class, $found);
        // Step three has started right now.
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $found->getUid());
    }

    public function testFindOneStepThatIsOpenByDateForStepsWithoutOverlapShouldFindNoStepForSpecificDate(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        $found = $steps->findOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+100 days'),
            $this->referenceDate->modify('+100 days')
        );
        static::assertNull($found);
    }

    public function testFindOneStepThatIsOpenByDateForStepsWithOverlapShouldFindOneStepForSpecificDate(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        $found = $steps->findOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('-6 days'),
            $this->referenceDate->modify('-6 days')
        );
        static::assertInstanceOf(StepInterface::class, $found);
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $found->getUid());
    }

    public function testFindOneStepThatIsOpenByDateForStepsWithOverlapShouldFindNoStepForSpecificDate(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        $found = $steps->findOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+100 days'),
            $this->referenceDate->modify('+100 days')
        );
        static::assertNull($found);
    }

    public function testFindOpenStepWithMultipleOpenForDateShouldThrowBadMethodCallException(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        $this->expectException(\BadMethodCallException::class);

        $steps->findOpenStep($this->referenceDate->modify('-1 day'), $this->referenceDate, $this->referenceDate);
    }

    public function testHasOneStepThatIsOpenByDate(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertFalse($steps->hasOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+100 days'),
            $this->referenceDate->modify('+100 days')
        ));
        static::assertTrue($steps->hasOpenStep($this->referenceDate, $this->referenceDate, $this->referenceDate));
    }

    public function testGetOpenStep(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        // Step three starts right now.
        static::assertEquals(
            $this->stepWithCalDateStartThree->getUid(),
            $steps->getOpenStep($this->referenceDate, $this->referenceDate, $this->referenceDate)->getUid()
        );
    }

    public function testGetOneStepThatIsOpenByDateWithNoneFoundShouldThrowBadMethodCallException(): void
    {
        $steps = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        $this->expectException(\BadMethodCallException::class);

        $steps->getOpenStep(
            $this->referenceDate,
            $this->referenceDate->modify('+100 days'),
            $this->referenceDate->modify('+100 days')
        )->getUid();
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getOneStepThatDirectlyPrecedesStep()
     */
    public function testGetOneStepThatDirectlyPrecedesStep(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertEquals(
            $this->stepWithCalDateStartOne->getUid(),
            $result->getOneStepThatDirectlyPrecedesStep($this->stepWithCalDateStartTwo)->getUid()
        );
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getOneStepThatDirectlyPrecedesStep()
     */
    public function testGetOneStepThatDirectlyPrecedesStepWithNoneFoundShouldThrowBadMethodCallException(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        $this->expectException(\BadMethodCallException::class);

        $result->getOneStepThatDirectlyPrecedesStep($this->stepWithCalDateStartOne);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getOneStepThatDirectlyPrecedesStep()
     */
    public function testGetOneStepThatDirectlyPrecedesStepWithUnknownStepShouldThrowInvalidArgumentException(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
        ]);

        $this->expectException(\InvalidArgumentException::class);

        $result->getOneStepThatDirectlyPrecedesStep($this->stepWithCalDateStartThree);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::hasFinishingStep()
     */
    public function testHasFinishingStep(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertTrue($result->hasFinishingStep());

        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
        ]);

        static::assertTrue($result->hasFinishingStep());

        $result = new StepWithCalculatedDateStartCollection();

        static::assertFalse($result->hasFinishingStep());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getFinishingStep()
     */
    public function testGetFinishingStep(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $result->getFinishingStep()->getUid());

        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $result->getFinishingStep()->getUid());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getFinishingStep()
     */
    public function testGetFinishingStepWithoutAnyStepsShouldThrowBadMethodCallException(): void
    {
        $result = new StepWithCalculatedDateStartCollection();

        $this->expectException(\BadMethodCallException::class);

        $result->getFinishingStep();
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::isOverlappingStep
     */
    public function testIsOverlappingStep(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        // no overlap
        static::assertFalse($result->isOverlappingStep($this->stepWithCalDateStartOne));
        static::assertFalse($result->isOverlappingStep($this->stepWithCalDateStartOne->withoutPreviousStepFulFilledStartOption()));
        static::assertFalse($result->isOverlappingStep(ValueObjectFactory::createStepWithCalculatedDateStart(
            'some_completely_new_uid',
            $this->stepWithCalDateStartOne->getPeriod()->getAmountOfDaysToCalculateStartDate() + 100,
            $this->stepWithCalDateStartOne->getPeriod()->getAmountOfDaysToCalculateEndDate() + 100
        )));
        static::assertFalse($result->isOverlappingStep($this->stepWithCalDateStartThree));

        // no overlap
        static::assertTrue($result->isOverlappingStep($this->stepWithCalDateStartTwo));
        static::assertTrue($result->isOverlappingStep($this->stepWithCalDateStartFourHavingOverlapWithTwo));
        static::assertTrue($result->isOverlappingStep(ValueObjectFactory::createStepWithCalculatedDateStart(
            'some_other_completely_new_uid',
            $this->stepWithCalDateStartOne->getPeriod()->getAmountOfDaysToCalculateStartDate(),
            $this->stepWithCalDateStartOne->getPeriod()->getAmountOfDaysToCalculateEndDate()
        )));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::hasOverlappingSteps
     */
    public function testHasOverlappingSteps(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertFalse($result->hasOverlappingSteps());

        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
        ]);

        static::assertTrue($result->hasOverlappingSteps());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::hasOneStepThatDirectlyFollowsStep()
     */
    public function testHasOneStepThatDirectlyFollowsStep(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertTrue($result->hasOneStepThatDirectlyFollowsStep($this->stepWithCalDateStartOne));
        static::assertTrue($result->hasOneStepThatDirectlyFollowsStep($this->stepWithCalDateStartTwo));
        static::assertFalse($result->hasOneStepThatDirectlyFollowsStep($this->stepWithCalDateStartThree));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::hasOneStepThatDirectlyFollowsStep()
     */
    public function testHasOneStepThatDirectlyFollowsStepWithUnknownStepShouldThrowInvalidArgumentException(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        $this->expectException(\InvalidArgumentException::class);

        $result->hasOneStepThatDirectlyFollowsStep(
            ValueObjectFactory::createStepWithCalculatedDateStart(
                'some_uid',
                -20,
                -10
            )
        );
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getOneStepThatDirectlyFollowsStep()
     */
    public function testGetOneStepThatDirectlyFollowsStep(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertEquals(
            $this->stepWithCalDateStartTwo->getUid(),
            $result->getOneStepThatDirectlyFollowsStep($this->stepWithCalDateStartOne)->getUid()
        );
        static::assertEquals(
            $this->stepWithCalDateStartThree->getUid(),
            $result->getOneStepThatDirectlyFollowsStep($this->stepWithCalDateStartTwo)->getUid()
        );
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getOneStepThatDirectlyFollowsStep()
     */
    public function testGetOneStepThatDirectlyFollowsStepWithUnknownStepShouldThrowInvalidArgumentException(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        $this->expectException(\InvalidArgumentException::class);

        $result->getOneStepThatDirectlyFollowsStep(
            ValueObjectFactory::createStepWithCalculatedDateStart(
                'some_uid',
                -20,
                -10
            )
        );
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::doesFlowStartWithStep()
     */
    public function testDoesFlowStartWithStep(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertTrue($result->doesFlowStartWithStep($this->stepWithCalDateStartOne));
        static::assertFalse($result->doesFlowStartWithStep($this->stepWithCalDateStartTwo));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::doesFlowStartWithStep()
     */
    public function testDoesFlowStartWithStepWithUnknownStepShouldThrowInvalidArgumentException(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        $this->expectException(\InvalidArgumentException::class);

        $result->doesFlowStartWithStep(
            ValueObjectFactory::createStepWithCalculatedDateStart(
                'some_uid',
                -20,
                -10
            )
        );
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::hasOneStepByUid()
     */
    public function testHasOneStepByUid(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertTrue($result->hasOneStepByUid($this->stepWithCalDateStartOne->getUid()));
        static::assertFalse($result->hasOneStepByUid('azerterdfgsdfgsdf'));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getOneStepByUid()
     */
    public function testGetOneStepByUid(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertEquals(
            $this->stepWithCalDateStartOne->getUid(),
            $result->getOneStepByUid($this->stepWithCalDateStartOne->getUid())->getUid()
        );
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getSortedByAscendingFlow()
     */
    public function testGetSortedByAscendingFlow(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);
        $sorted = $result->getSortedByAscendingFlow();
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithCalDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartThree));
        static::assertFalse($sorted->doesFlowStartWithStep($this->stepWithCalDateStartTwo));
        static::assertFalse($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartTwo));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[2]->getUid());

        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
        ]);
        $sorted = $result->getSortedByAscendingFlow();
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithCalDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartThree));
        static::assertFalse($sorted->doesFlowStartWithStep($this->stepWithCalDateStartTwo));
        static::assertFalse($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartTwo));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[2]->getUid());

        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartOne,
        ]);
        $sorted = $result->getSortedByAscendingFlow();
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithCalDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartThree));
        static::assertFalse($sorted->doesFlowStartWithStep($this->stepWithCalDateStartTwo));
        static::assertFalse($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartTwo));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[2]->getUid());

        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartOne,
        ]);
        $sorted = $result->getSortedByAscendingFlow();
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithCalDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartThree));
        static::assertFalse($sorted->doesFlowStartWithStep($this->stepWithCalDateStartTwo));
        static::assertFalse($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartTwo));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[2]->getUid());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::hasStartingStep()
     */
    public function testHasStartingStep(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertTrue($result->hasStartingStep());

        $result = new StepWithCalculatedDateStartCollection();

        static::assertFalse($result->hasStartingStep());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getStartingStep()
     */
    public function testGetStartingStep(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartThree,
        ]);

        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $result->getStartingStep()->getUid());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getStartingStep()
     */
    public function testGetStartingStepIfEmptyCollectionShouldThrowBadMethodCallException(): void
    {
        $result = new StepWithCalculatedDateStartCollection();

        $this->expectException(\BadMethodCallException::class);
        $result->getStartingStep();
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::getPositionNumber()
     */
    public function testGetPositionNumber(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartOne,
        ]);

        static::assertEquals(1, $result->getPositionNumber($this->stepWithCalDateStartOne));
        static::assertEquals(2, $result->getPositionNumber($this->stepWithCalDateStartTwo));
        static::assertEquals(3, $result->getPositionNumber($this->stepWithCalDateStartThree));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection::doesFlowFinishWithStep()
     */
    public function testDoesFlowFinishesWithStep(): void
    {
        $result = new StepWithCalculatedDateStartCollection([
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartOne,
        ]);

        static::assertTrue($result->doesFlowFinishWithStep($this->stepWithCalDateStartThree));
        static::assertFalse($result->doesFlowFinishWithStep($this->stepWithCalDateStartOne));
        static::assertFalse($result->doesFlowFinishWithStep($this->stepWithCalDateStartTwo));
    }
}
