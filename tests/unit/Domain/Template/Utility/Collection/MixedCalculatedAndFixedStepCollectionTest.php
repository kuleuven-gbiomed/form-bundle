<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Utility\Collection;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class MixedCalculatedAndFixedStepCollectionTest.
 *
 * @covers  \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection
 */
class MixedCalculatedAndFixedStepCollectionTest extends TestCase
{
    /** @var StepWithCalculatedDateStart */
    private $stepWithCalDateStartOne;
    /** @var StepWithCalculatedDateStart */
    private $stepWithCalDateStartTwo;
    /** @var StepWithCalculatedDateStart */
    private $stepWithCalDateStartThree;
    /** @var StepWithCalculatedDateStart */
    private $stepWithCalDateStartFourHavingOverlapWithTwo;

    /** @var StepWithFixedDateStart */
    private $stepWithFixDateStartOne;
    /** @var StepWithFixedDateStart */
    private $stepWithFixDateStartTwo;
    /** @var StepWithFixedDateStart */
    private $stepWithFixDateStartThree;
    /** @var StepWithFixedDateStart */
    private $stepWithFixDateStartFourHavingOverlapWithTwo;

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->stepWithCalDateStartOne = ValueObjectFactory::createStepWithCalculatedDateStart(
            'uid_cal_1',
            -20,
            -10
        );
        $this->stepWithCalDateStartTwo = ValueObjectFactory::createStepWithCalculatedDateStart(
            'uid_cal_2',
            -10,
            0
        );
        $this->stepWithCalDateStartThree = ValueObjectFactory::createStepWithCalculatedDateOrPreviousFulFilledStart(
            'uid_cal_3',
            0,
            10
        );
        $this->stepWithCalDateStartFourHavingOverlapWithTwo = ValueObjectFactory::createStepWithCalculatedDateStart(
            'uid_cal_4',
            -5,
            0
        );

        $this->stepWithFixDateStartOne = ValueObjectFactory::createStepWithFixedDateStartByStartDateAndOffset(
            'uid_fix_1',
            new \DateTimeImmutable('2019-05-01 16:00'),
            10
        );
        $this->stepWithFixDateStartTwo = ValueObjectFactory::createStepWithFixedDateStartByStartDateAndOffset(
            'uid_fix_2',
            new \DateTimeImmutable('2019-05-11 16:00'),
            10
        );
        $this->stepWithFixDateStartThree = ValueObjectFactory::createStepWithFixedDateOrPreviousFulFilledStartByStartDateAndOffset(
            'uid_fix_3',
            new \DateTimeImmutable('2019-05-21 16:00'),
            10
        );
        $this->stepWithFixDateStartFourHavingOverlapWithTwo = ValueObjectFactory::createStepWithFixedDateStartByStartDateAndOffset(
            'uid_fix_4',
            new \DateTimeImmutable('2019-05-16 16:00'),
            10
        );
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection::__construct
     */
    public function testConstructWithValidInstancesShouldReturnInstance(): void
    {
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartThree,
            $this->stepWithFixDateStartFourHavingOverlapWithTwo,
        ]);

        static::assertInstanceOf(MixedCalculatedAndFixedStepCollection::class, $result);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection::hasOneStepThatDirectlyPrecedesStep()
     */
    public function testHasOneStepThatDirectlyPrecedesStep(): void
    {
        // check with mix of step types
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithFixDateStartOne,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartTwo,
        ]);

        static::assertTrue($result->hasOneStepThatDirectlyPrecedesStep($this->stepWithFixDateStartOne));
        static::assertTrue($result->hasOneStepThatDirectlyPrecedesStep($this->stepWithCalDateStartThree));
        static::assertTrue($result->hasOneStepThatDirectlyPrecedesStep($this->stepWithCalDateStartTwo));
        static::assertFalse($result->hasOneStepThatDirectlyPrecedesStep($this->stepWithCalDateStartOne));

        // check with single type of steps
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
        ]);

        static::assertFalse($result->hasOneStepThatDirectlyPrecedesStep($this->stepWithCalDateStartOne));
        static::assertTrue($result->hasOneStepThatDirectlyPrecedesStep($this->stepWithCalDateStartTwo));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection::withNewStepAddedAtEnd()
     */
    public function testwithNewStepAddedAtEnd(): void
    {
        $steps = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
        ]);

        // check with single type of steps
        $result = $steps->withNewStepAddedAtEnd($this->stepWithCalDateStartThree);
        static::assertCount(3, $result);
        /** @var StepInterface $lastStepInArray */
        $lastStepInArray = $result->toArray()[2];
        static::assertEquals($lastStepInArray->getUid(), $this->stepWithCalDateStartThree->getUid());
        static::assertEquals($result->getFinishingStep()->getUid(), $this->stepWithCalDateStartThree->getUid());

        // check with mix of step types
        $result = $result->withNewStepAddedAtEnd($this->stepWithFixDateStartOne);
        static::assertCount(4, $result);
        /** @var StepInterface $lastStepInArray */
        $lastStepInArray = $result->toArray()[3];
        static::assertEquals($lastStepInArray->getUid(), $this->stepWithFixDateStartOne->getUid());
        static::assertEquals($result->getFinishingStep()->getUid(), $this->stepWithFixDateStartOne->getUid());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection::withNewStepAddedAtBeginning()
     */
    public function testWithNewStepAddedAtBeginning(): void
    {
        $steps = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
        ]);

        // check with single type of steps
        $result = $steps->withNewStepAddedAtBeginning($this->stepWithCalDateStartThree);
        static::assertCount(3, $result);
        /** @var StepInterface $firstStepInArray */
        $firstStepInArray = $result->toArray()[0];
        static::assertEquals($firstStepInArray->getUid(), $this->stepWithCalDateStartThree->getUid());
        static::assertEquals($result->getStartingStep()->getUid(), $this->stepWithCalDateStartOne->getUid());

        // check with mix of step types
        $result = $result->withNewStepAddedAtBeginning($this->stepWithFixDateStartOne);
        static::assertCount(4, $result);
        /** @var StepInterface $firstStepInArray */
        $firstStepInArray = $result->toArray()[0];
        static::assertEquals($firstStepInArray->getUid(), $this->stepWithFixDateStartOne->getUid());
        static::assertEquals($result->getStartingStep()->getUid(), $this->stepWithFixDateStartOne->getUid());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection::withNewStepAddedAfterExistingStep()
     */
    public function testWithNewStepAddedAfterExistingStep(): void
    {
        $steps = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
        ]);

        $result = $steps->withNewStepAddedAfterExistingStep($this->stepWithFixDateStartOne, $this->stepWithCalDateStartOne);
        $result = $result->withNewStepAddedAfterExistingStep($this->stepWithCalDateStartThree, $this->stepWithCalDateStartTwo);
        static::assertCount(4, $result);
        /** @var StepInterface[] $stepsArray */
        $stepsArray = array_values($result->toArray());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $stepsArray[0]->getUid());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $stepsArray[1]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $stepsArray[2]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $stepsArray[3]->getUid());
    }

    public function testFindOpenStepsShouldThrowIfMultipleStepsAreOpen(): void
    {
        // FIXME: shouldn't we prevent such a collection from being created?
        $steps = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartThree,
            $this->stepWithFixDateStartFourHavingOverlapWithTwo,
        ]);

        // There are two fixed steps open
        $this->expectException(\BadMethodCallException::class);
        $steps->findOpenStep(
            new \DateTimeImmutable('2019-05-21 16:00'),
            new \DateTimeImmutable('2019-04-26 16:00'),
            new \DateTimeImmutable('2019-04-26 16:00')
        );
    }

    public function testFindOpenStepWithOnlyCalculatedDatesStepsAndWithoutOverlapShouldReturnOneStep(): void
    {
        $steps = new MixedCalculatedAndFixedStepCollection(
            [$this->stepWithCalDateStartOne, $this->stepWithCalDateStartTwo, $this->stepWithCalDateStartThree]
        );
        $found = $steps->findOpenStep(
            new \DateTimeImmutable('2019-05-21 16:00'),
            new \DateTimeImmutable('2019-05-21 16:00'),
            new \DateTimeImmutable('2019-05-21 16:00')
        );
        static::assertInstanceOf(StepInterface::class, $found);
        // Step three opens at this point, so step three should be the open one.
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $found->getUid());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection
     *
     * @throws \Exception
     */
    public function testFindOneStepThatIsOpenByDateWithOnlyFixedDatesStepsAndWithoutOverlapShouldReturnOneStep(): void
    {
        // The title of the test said 'without overlap', so let's take periods without overlap.
        $steps = new MixedCalculatedAndFixedStepCollection(
            [
                $this->stepWithFixDateStartOne,
                $this->stepWithFixDateStartTwo,
                $this->stepWithFixDateStartThree,
            ]
        );
        $found = $steps->findOpenStep(
            new \DateTimeImmutable('2019-05-20 16:00'),
            new \DateTimeImmutable('2019-05-21 16:00'),
            new \DateTimeImmutable('2019-05-21 16:00')
        );
        static::assertInstanceOf(StepInterface::class, $found);
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $found->getUid());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection
     *
     * @throws \Exception
     */
    public function testFindOneStepThatIsOpenByDateWithMixedStepsAndWithoutOverlapShouldReturnOneStep(): void
    {
        $steps = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartTwo,
            $this->stepWithFixDateStartFourHavingOverlapWithTwo,
        ]);

        // If you don't want overlap, you need to fiddle with the reference date.
        $found = $steps->findOpenStep(
            new \DateTimeImmutable('2019-05-21 16:00'),
            new \DateTimeImmutable('2019-06-05 16:00'),
            new \DateTimeImmutable('2019-06-05 16:00')
        );
        static::assertInstanceOf(StepInterface::class, $found);
        static::assertEquals($this->stepWithFixDateStartFourHavingOverlapWithTwo->getUid(), $found->getUid());

        // Steps that don't overlap given reference date +15.
        $steps = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartFourHavingOverlapWithTwo,
        ]);

        $found = $steps->findOpenStep(
            new \DateTimeImmutable('2019-05-21 16:00'),
            new \DateTimeImmutable('2019-06-05 16:00'),
            new \DateTimeImmutable('2019-06-05 16:00')
        );
        static::assertInstanceOf(StepInterface::class, $found);
        static::assertEquals($this->stepWithFixDateStartFourHavingOverlapWithTwo->getUid(), $found->getUid());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection
     *
     * @throws \Exception
     */
    public function testFindOneStepThatIsOpenByDateForStepsWithMixedStepsAndWithOverlapShouldFindNoStepForSpecificDate(): void
    {
        $steps = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartFourHavingOverlapWithTwo,
            $this->stepWithFixDateStartThree,
        ]);
        // The title says that no result should be found. So let's expect an exception.
        $this->expectException(\BadMethodCallException::class);
        $found = $steps->findOpenStep(
            new \DateTimeImmutable('2019-05-20 16:00'),
            new \DateTimeImmutable('2019-05-15 16:00'),
            new \DateTimeImmutable('2019-05-15 16:00')
        );
        static::assertInstanceOf(StepInterface::class, $found);
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $found->getUid());
    }

    public function testFindOpenStepForStepsWithMixedStepsAndWithOverlapOnThreeOpenStepsOfDifferentTypesShouldThrowBadMethodCallException(): void
    {
        $steps = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartFourHavingOverlapWithTwo,
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartFourHavingOverlapWithTwo,
            $this->stepWithFixDateStartThree,
        ]);

        $date = new \DateTimeImmutable('2019-05-25 16:00');

        $this->expectException(\BadMethodCallException::class);

        $steps->findOpenStep($date, $date, $date);
    }

    public function testFindOpenStepForStepsWithMixedStepsAndWithOverlapOnTwoOpenStepsOfSameTypeShouldThrowBadMethodCallException(): void
    {
        $steps = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartThree,
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartFourHavingOverlapWithTwo,
            $this->stepWithFixDateStartThree,
        ]);

        $date = new \DateTimeImmutable('2019-05-25 16:00');

        $this->expectException(\BadMethodCallException::class);

        $steps->findOpenStep($date, $date, $date);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection::getSortedByAscendingFlow()
     */
    public function testGetSortedByAscendingFlowWithNoMixedSteps(): void
    {
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartThree,
        ]);
        $sorted = $result->getSortedByAscendingFlow();
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithFixDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithFixDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[2]->getUid());

        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartThree,
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartTwo,
        ]);
        $sorted = $result->getSortedByAscendingFlow();
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithFixDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithFixDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[2]->getUid());

        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartThree,
        ]);
        $sorted = $result->getSortedByAscendingFlow();
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithCalDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[2]->getUid());

        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithCalDateStartThree,
            $this->stepWithCalDateStartOne,
            $this->stepWithCalDateStartTwo,
        ]);
        $sorted = $result->getSortedByAscendingFlow();
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithCalDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[2]->getUid());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection::getSortedByAscendingFlow()
     */
    public function testGetSortedByAscendingFlowWithMixedSteps(): void
    {
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartOne,
            $this->stepWithCalDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithCalDateStartTwo,
            $this->stepWithFixDateStartThree,
            $this->stepWithCalDateStartThree,
        ]);
        $sorted = $result->getSortedByAscendingFlow();
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithFixDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[2]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[3]->getUid());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[4]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[5]->getUid());

        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartThree,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartOne,
            $this->stepWithCalDateStartThree,
        ]);
        $sorted = $result->getSortedByAscendingFlow();
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithFixDateStartThree));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[2]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[3]->getUid());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[4]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[5]->getUid());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection::getSortedByAscendingFlowAndTargetDate()
     */
    public function testGetSortedByAscendingFlowAndTargetDateWithNoMixedSteps(): void
    {
        $date = new \DateTimeImmutable('2019-05-21');
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartThree,
        ]);
        $sorted = $result->getSortedByAscendingFlowAndTargetDate($date);
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithFixDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithFixDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[2]->getUid());

        $date = new \DateTimeImmutable('2019-02-21');
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartThree,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartOne,
        ]);
        $sorted = $result->getSortedByAscendingFlowAndTargetDate($date);
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithFixDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithFixDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[2]->getUid());

        $date = new \DateTimeImmutable('2019-08-21');
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartThree,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartOne,
        ]);
        $sorted = $result->getSortedByAscendingFlowAndTargetDate($date);
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithFixDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithFixDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[2]->getUid());

        $date = new \DateTimeImmutable('2019-05-16 16:00');
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartThree,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartOne,
        ]);
        $sorted = $result->getSortedByAscendingFlowAndTargetDate($date);
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithFixDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithFixDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[2]->getUid());

        static::assertLessThan(
            $this->stepWithFixDateStartTwo->getPeriod()->getFixedStartDate(),
            $this->stepWithFixDateStartOne->getPeriod()->getFixedStartDate()
        );
        static::assertLessThan(
            $this->stepWithFixDateStartThree->getPeriod()->getFixedStartDate(),
            $this->stepWithFixDateStartTwo->getPeriod()->getFixedStartDate()
        );
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\MixedCalculatedAndFixedStepCollection::getSortedByAscendingFlowAndTargetDate()
     */
    public function testGetSortedByAscendingFlowAndTargetDateWithMixedSteps(): void
    {
        $date = new \DateTimeImmutable('2019-05-21 16:00');
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartOne,
            $this->stepWithCalDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithCalDateStartTwo,
            $this->stepWithFixDateStartThree,
            $this->stepWithCalDateStartThree,
        ]);
        $sorted = $result->getSortedByAscendingFlowAndTargetDate($date);
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithFixDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[2]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[3]->getUid());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[4]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[5]->getUid());

        $date = new \DateTimeImmutable('2019-02-21 16:00');
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartThree,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartOne,
            $this->stepWithCalDateStartThree,
        ]);
        $sorted = $result->getSortedByAscendingFlowAndTargetDate($date);
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithCalDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithFixDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[2]->getUid());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[3]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[4]->getUid());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[5]->getUid());

        $date = new \DateTimeImmutable('2019-08-21 16:00');
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartThree,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartOne,
            $this->stepWithCalDateStartThree,
        ]);
        $sorted = $result->getSortedByAscendingFlowAndTargetDate($date);
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithFixDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithCalDateStartThree));
        /** @var StepInterface[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[2]->getUid());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[3]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[4]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[5]->getUid());

        $date = new \DateTimeImmutable('2019-05-16 16:00');
        $result = new MixedCalculatedAndFixedStepCollection([
            $this->stepWithFixDateStartThree,
            $this->stepWithCalDateStartTwo,
            $this->stepWithCalDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartOne,
            $this->stepWithCalDateStartThree,
        ]);
        $sorted = $result->getSortedByAscendingFlowAndTargetDate($date);
        static::assertTrue($sorted->doesFlowStartWithStep($this->stepWithCalDateStartOne));
        static::assertTrue($sorted->doesFlowFinishWithStep($this->stepWithFixDateStartThree));
        /** @var StepInterface[]|StepWithFixedDateStart[]|StepWithCalculatedDateStart[] $sortedArray */
        $sortedArray = array_values($sorted->toArray());
        static::assertEquals($this->stepWithCalDateStartOne->getUid(), $sortedArray[0]->getUid());
        static::assertEquals($this->stepWithFixDateStartOne->getUid(), $sortedArray[1]->getUid());
        static::assertEquals($this->stepWithCalDateStartTwo->getUid(), $sortedArray[2]->getUid());
        static::assertEquals($this->stepWithFixDateStartTwo->getUid(), $sortedArray[3]->getUid());
        static::assertEquals($this->stepWithCalDateStartThree->getUid(), $sortedArray[4]->getUid());
        static::assertEquals($this->stepWithFixDateStartThree->getUid(), $sortedArray[5]->getUid());

        static::assertLessThan(
            $this->stepWithFixDateStartOne->getPeriod()->getFixedStartDate(),
            $this->stepWithCalDateStartOne->getPeriod()->getCalculatedStartDate($date)
        );
        static::assertLessThan(
            $this->stepWithCalDateStartTwo->getPeriod()->getCalculatedStartDate($date),
            $this->stepWithFixDateStartOne->getPeriod()->getFixedStartDate()
        );
        static::assertLessThan(
            $this->stepWithFixDateStartTwo->getPeriod()->getFixedStartDate(),
            $this->stepWithCalDateStartTwo->getPeriod()->getCalculatedStartDate($date)
        );
        static::assertLessThan(
            $this->stepWithCalDateStartThree->getPeriod()->getCalculatedStartDate($date),
            $this->stepWithFixDateStartTwo->getPeriod()->getFixedStartDate()
        );
        static::assertLessThan(
            $this->stepWithFixDateStartThree->getPeriod()->getFixedStartDate(),
            $this->stepWithCalDateStartThree->getPeriod()->getCalculatedStartDate($date)
        );
    }
}
