<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Utility\Collection;

use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig;
use KUL\FormBundle\Domain\Template\Utility\Collection\RoleNotificationConfigCollection;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class RoleNotificationConfigCollectionTest.
 */
class RoleNotificationConfigCollectionTest extends TestCase
{
    /** @var Role */
    private $roleOne;
    /** @var Role */
    private $roleTwo;
    /** @var RoleNotificationConfig */
    private $roleOneNotificationConfig;
    /** @var RoleNotificationConfig */
    private $roleTwoNotificationConfig;

    protected function setUp(): void
    {
        parent::setUp();

        $this->roleOne = Role::fromName('ROLE_ONE');
        $this->roleTwo = Role::fromName('ROLE_TWO');
        $this->roleOneNotificationConfig = RoleNotificationConfig::fromJsonDecodedArray(SerializedDataFactory::createRoleNotificationConfigArray($this->roleOne->getName(), 5));
        $this->roleTwoNotificationConfig = RoleNotificationConfig::fromJsonDecodedArray(SerializedDataFactory::createRoleNotificationConfigArray($this->roleTwo->getName(), 5));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\RoleNotificationConfigCollection::__construct
     */
    public function testConstructorShouldReturnRoleNotificationConfigCollection(): void
    {
        $result = new RoleNotificationConfigCollection([$this->roleOneNotificationConfig, $this->roleTwoNotificationConfig]);

        static::assertInstanceOf(RoleNotificationConfigCollection::class, $result);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\RoleNotificationConfigCollection::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithValidDataShouldReturnRoleNotificationConfigCollection(): void
    {
        $data = [
            SerializedDataFactory::createRoleNotificationConfigArray($this->roleOne->getName(), 5),
            SerializedDataFactory::createRoleNotificationConfigArray($this->roleTwo->getName(), 3),
        ];

        $result = RoleNotificationConfigCollection::fromJsonDecodedArray($data);

        static::assertInstanceOf(RoleNotificationConfigCollection::class, $result);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\RoleNotificationConfigCollection::hasOneForRole()
     */
    public function testHasOneForRole(): void
    {
        $result = new RoleNotificationConfigCollection([$this->roleOneNotificationConfig, $this->roleTwoNotificationConfig]);

        static::assertTrue($result->hasOneForRole($this->roleOne));
        static::assertTrue($result->hasOneForRole($this->roleTwo));
        static::assertFalse($result->hasOneForRole(Role::fromName('FAKE_ROLE')));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\RoleNotificationConfigCollection::getOneForRole()
     */
    public function testGetOneForRoleShouldReturnRoleNotificationConfigInstance(): void
    {
        $result = new RoleNotificationConfigCollection([$this->roleOneNotificationConfig, $this->roleTwoNotificationConfig]);

        static::assertInstanceOf(RoleNotificationConfig::class, $result->getOneForRole($this->roleOne));
        static::assertSame($this->roleTwo->getName(), $result->getOneForRole($this->roleTwo)->getRole()->getName());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\RoleNotificationConfigCollection::getOneForRole()
     */
    public function testGetOneForRoleShouldThrowBadMethodCallException(): void
    {
        $result = new RoleNotificationConfigCollection([$this->roleOneNotificationConfig, $this->roleTwoNotificationConfig]);

        $this->expectException(\BadMethodCallException::class);
        $result->getOneForRole(Role::fromName('FAKE_ROLE'));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\RoleNotificationConfigCollection::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            SerializedDataFactory::createRoleNotificationConfigArray($this->roleOne->getName(), 5),
            SerializedDataFactory::createRoleNotificationConfigArray($this->roleTwo->getName(), 3),
        ]);

        $result = RoleNotificationConfigCollection::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
