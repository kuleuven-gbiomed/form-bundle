<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Utility\Collection;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class StepWithFixedDateStartCollectionTest.
 *
 * @covers  \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection
 */
class StepWithFixedDateStartCollectionTest extends TestCase
{
    /** @var StepWithFixedDateStart */
    private $stepWithFixDateStartOne;
    /** @var StepWithFixedDateStart */
    private $stepWithFixDateStartTwo;

    /** @var StepWithFixedDateStart */
    private $stepWithFixDateStartThree;
    /** @var StepWithFixedDateStart */
    private $stepWithFixDateStartFourHavingOverlapWithTwo;

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     * @throws \Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->stepWithFixDateStartOne = ValueObjectFactory::createStepWithFixedDateStartByStartDateAndOffset(
            'uid_fix_1',
            new \DateTimeImmutable('2019-04-30'),
            10
        );
        $this->stepWithFixDateStartTwo = ValueObjectFactory::createStepWithFixedDateStartByStartDateAndOffset(
            'uid_fix_2',
            new \DateTimeImmutable('2019-05-10'),
            10
        );
        $this->stepWithFixDateStartThree = ValueObjectFactory::createStepWithFixedDateOrPreviousFulFilledStartByStartDateAndOffset(
            'uid_fix_3',
            new \DateTimeImmutable('2019-05-20'),
            10
        );
        $this->stepWithFixDateStartFourHavingOverlapWithTwo = ValueObjectFactory::createStepWithFixedDateStartByStartDateAndOffset(
            'uid_fix_4',
            new \DateTimeImmutable('2019-05-15'),
            10
        );
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection::__construct
     */
    public function testConstructWithValidInstancesShouldReturnInstance(): void
    {
        $result = new StepWithFixedDateStartCollection([
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartThree,
            $this->stepWithFixDateStartFourHavingOverlapWithTwo,
        ]);

        static::assertInstanceOf(StepWithFixedDateStartCollection::class, $result);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection::__construct
     */
    public function testConstructWithMixedStepTypesShouldThrowException(): void
    {
        $this->expectException(\Exception::class);

        new StepWithFixedDateStartCollection([
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartThree,
            ValueObjectFactory::createStepWithCalculatedDateStart('uid_cal_3'),
        ]);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection::__construct
     */
    public function testConstructWithDuplicateUidsShouldThrowInvalidArgumentException(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new StepWithFixedDateStartCollection([
            $this->stepWithFixDateStartOne,
            $this->stepWithFixDateStartTwo,
            $this->stepWithFixDateStartThree,
            ValueObjectFactory::createStepWithFixedDateStart(
                $this->stepWithFixDateStartOne->getUid(),
                new \DateTimeImmutable('2019-06-21 16:00'),
                100,
                50
            ),
        ]);
    }

    public function testHasOneStepThatIsOpenByDateNowForCollectionWithOpenStep(): void
    {
        $collection = new StepWithFixedDateStartCollection([
            $this->stepWithFixDateStartFourHavingOverlapWithTwo,
        ]);

        $result = $collection->hasOpenStep(new \DateTimeImmutable('2019-05-20'));

        static::assertTrue($result);
    }

    public function testHasOpenStepForCollectionWithoutOpenStep(): void
    {
        $collection = new StepWithFixedDateStartCollection([]);

        $result = $collection->hasOpenStep(new \DateTimeImmutable('2019-05-20'));

        static::assertFalse($result);
    }
}
