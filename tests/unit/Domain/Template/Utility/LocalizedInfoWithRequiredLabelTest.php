<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Utility;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class LocalizedInfoWithRequiredLabelTest.
 *
 * @covers  \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel
 */
class LocalizedInfoWithRequiredLabelTest extends TestCase
{
    /**
     * @return array
     */
    private function getSerializedArrayFromValidMinimalAndOptionalData()
    {
        return [
            LocalizedInfoWithRequiredLabel::KEY_LABEL => ['nl' => 'dutch label string'],
            LocalizedInfoWithRequiredLabel::KEY_DESCRIPTION => ['nl' => 'dutch description string'],
            LocalizedInfoWithRequiredLabel::KEY_ROLE_DESCRIPTIONS => [
                [
                    RoleLocalizedOptionalString::KEY_ROLE => 'TEST_ROLE',
                    RoleLocalizedOptionalString::KEY_TRANSLATIONS => ['nl' => 'dutch role test description string'],
                ],
            ],
        ];
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::fromLocalizedStrings()
     */
    public function testFromLocalizedStringsWithOnlyMinimalRequiredLabelShouldReturnInstance(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        static::assertInstanceOf(LocalizedInfoWithRequiredLabel::class, $result);

        static::assertFalse($result->hasDescription('nl'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_TEST'), 'nl'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::fromLocalizedStrings()
     */
    public function testFromLocalizedStringsWithMinimalRequiredLabelAndOptionalStringsShouldReturnInstance(): void
    {
        $description = ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations();
        $dutchDescriptionString = $description->getForLocale('nl');

        $roleDescriptions = ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish();
        $firstRoleDescription = $roleDescriptions->getOneForRole(ValueObjectFactory::createFirstRole());
        $firstRoleDescriptionDutch = $firstRoleDescription->getLocalizedString()->getForLocale('nl');

        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            $description,
            $roleDescriptions
        );

        static::assertInstanceOf(LocalizedInfoWithRequiredLabel::class, $result);

        // test locale nl does exist
        static::assertTrue($result->hasDescription('nl'));
        static::assertSame($dutchDescriptionString, $result->getDescription('nl'));

        static::assertTrue($result->hasRoleDescription($firstRoleDescription->getRole(), 'nl'));
        static::assertSame($firstRoleDescriptionDutch, $result->getRoleDescription($firstRoleDescription->getRole(), 'nl'));

        // test fallback: if a description exist for one locale, then fallback should return a fallback for any locale
        static::assertTrue($result->hasDescription('fake'));
        static::assertSame($description->getFallback(), $result->getDescription('fake'));

        static::assertTrue($result->hasRoleDescription($firstRoleDescription->getRole(), 'fake'));
        static::assertSame($firstRoleDescription->getLocalizedString()->getFallback(), $result->getRoleDescription($firstRoleDescription->getRole(), 'fake'));

        // if no transaltion exist
        static::assertFalse($result->hasRoleDescription(Role::fromName('NON_EXISTING_ROLE'), 'nl'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('NON_EXISTING_ROLE'), 'fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithMinimalValidDataShouldReturnInstance(): void
    {
        $data = [LocalizedInfoWithRequiredLabel::KEY_LABEL => ['nl' => 'dutch label string']];

        $result = LocalizedInfoWithRequiredLabel::fromJsonDecodedArray($data);

        static::assertInstanceOf(LocalizedInfoWithRequiredLabel::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithMissingMinimalValidDataShouldThrowLocalizedStringException(): void
    {
        $data = [LocalizedInfoWithRequiredLabel::KEY_LABEL => []];

        $this->expectException(LocalizedStringException::class);

        LocalizedInfoWithRequiredLabel::fromJsonDecodedArray($data);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithMinimalAndOptionalDataShouldReturnInstance(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromJsonDecodedArray(
            $this->getSerializedArrayFromValidMinimalAndOptionalData()
        );

        static::assertInstanceOf(LocalizedInfoWithRequiredLabel::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::getLocalizedLabel()
     */
    public function testGetLocalizedLabelShouldReturnLocalizedRequiredString(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
        );

        static::assertInstanceOf(LocalizedRequiredString::class, $result->getLocalizedLabel());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::getLabel()
     */
    public function testGetLabelShouldAlwaysReturnTranslatedOrFallbackString(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
        );

        $localizedLabel = $result->getLocalizedLabel();

        // test locales
        static::assertSame($localizedLabel->getForLocale('nl'), $result->getLabel('nl'));
        static::assertSame($localizedLabel->getForLocale('en'), $result->getLabel('en'));
        // test fallback
        static::assertSame($localizedLabel->getFallback(), $result->getLabel('fake'));
        static::assertSame($localizedLabel->getForLocaleOrFallback('fake'), $result->getLabel('fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::getLocalizedDescription()
     */
    public function testGetLocalizedDescriptionIfNoTranslationExistShouldAlwaysReturnLocalizedOptionalString(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        static::assertInstanceOf(LocalizedOptionalString::class, $result->getLocalizedDescription());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::getLocalizedDescription()
     */
    public function testGetLocalizedDescriptionIfAtLeastTranslationExistShouldReturnLocalizedOptionalString(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchTranslation(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        static::assertInstanceOf(LocalizedOptionalString::class, $result->getLocalizedDescription());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::hasDescription()
     */
    public function testHasDescriptionIfNoTranslationsExistShouldAlwaysReturnFalse(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
        );

        // test locales
        static::assertFalse($result->hasDescription('nl'));
        static::assertFalse($result->hasDescription('en'));
        // test fallback
        static::assertFalse($result->hasDescription('fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::hasDescription()
     */
    public function testHasDescriptionIfAtLeastTranslationExistShouldAlwaysReturnTrue(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchTranslation(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
        );

        // test locales
        static::assertTrue($result->hasDescription('nl'));
        static::assertTrue($result->hasDescription('en'));
        // test fallback
        static::assertTrue($result->hasDescription('fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::getDescription()
     */
    public function testGetDescriptionIfNoTranslationsExistShouldThrowLocalizedStringException(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithDutchTranslation()
        );

        $this->expectException(LocalizedStringException::class);
        $result->getDescription('nl');
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::getDescription()
     */
    public function testGetDescriptionIfAtLeastOneTranslationsExistShouldAlwaysReturnTranslatedOrFallbackString(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
        );

        $localizedDescription = $result->getLocalizedDescription();

        // test known locales
        static::assertSame($localizedDescription->getForLocale('nl'), $result->getDescription('nl'));
        static::assertSame($localizedDescription->getForLocale('en'), $result->getDescription('en'));
        // test fallback
        static::assertSame($localizedDescription->getFallback(), $result->getDescription('fake'));
        static::assertSame($localizedDescription->getForLocaleOrFallback('fake'), $result->getDescription('fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::getRoleLocalizedDescriptions()
     */
    public function testGetRoleLocalizedDescriptionsIfNoRolesExistShouldReturnRoleLocalizedOptionalStringCollection(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        static::assertInstanceOf(RoleLocalizedOptionalStringCollection::class, $result->getRoleLocalizedDescriptions());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::getRoleLocalizedDescriptions()
     */
    public function testGetRoleLocalizedDescriptionsIfAtLeastOneRoleExistShouldReturnRoleLocalizedOptionalStringCollection(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithDutchTranslation()
        );

        static::assertInstanceOf(RoleLocalizedOptionalStringCollection::class, $result->getRoleLocalizedDescriptions());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::hasRoleDescription()
     */
    public function testHasRoleDescriptionIfNoRoleExistShouldAlwaysReturnFalse(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        // test locales
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'nl'));
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'en'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'en'));
        // test fallback
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'fake'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::hasRoleDescription()
     */
    public function testHasRoleDescriptionIfNoRoleHavingTranslationsExistShouldAlwaysReturnFalse(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithNoTranslations()
        );

        // test locales
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'nl'));
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'en'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'en'));
        // test fallback
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'fake'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::hasRoleDescription()
     */
    public function testHasRoleDescriptionIfAtLeastOneRoleHavingTranslationsShouldAlwaysReturnTrue(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithNoTranslations()
        );

        // test locales
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'nl'));
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'en'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'en'));
        // test fallback
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'fake'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::getRoleDescription()
     */
    public function testGetRoleDescriptionIfNoRoleExistShouldThrowBadMethodCallException(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        $this->expectException(\BadMethodCallException::class);
        $result->getRoleDescription(ValueObjectFactory::createFirstRole(), 'nl');
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::getRoleDescription()
     */
    public function testGetRoleDescriptionIfNoRoleHavingTranslationsExistShouldThrowLocalizedStringException(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithNoTranslations()
        );

        $this->expectException(LocalizedStringException::class);
        $result->getRoleDescription(ValueObjectFactory::createFirstRole(), 'nl');
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::getRoleDescription()
     */
    public function testGetRoleDescriptionIfAtLeastOneRoleHavingTranslationsExistShouldAlwaysReturnTranslatedOrFallbackString(): void
    {
        $result = LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithDutchTranslation()
        );

        $localizedRoleDescription = $result->getLocalizedDescription();

        // test known locales
        static::assertSame($localizedRoleDescription->getForLocale('nl'), $result->getRoleDescription(ValueObjectFactory::createFirstRole(), 'nl'));
        // test fallback
        static::assertSame($localizedRoleDescription->getFallback(), $result->getRoleDescription(ValueObjectFactory::createFirstRole(), 'fake'));
        static::assertSame($localizedRoleDescription->getForLocaleOrFallback('fake'), $result->getRoleDescription(ValueObjectFactory::createFirstRole(), 'fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode(
            [
                LocalizedInfoWithRequiredLabel::KEY_LABEL => SerializedDataFactory::createTranslationsArray(true, true),
                LocalizedInfoWithRequiredLabel::KEY_DESCRIPTION => SerializedDataFactory::createTranslationsArray(false, false),
                LocalizedInfoWithRequiredLabel::KEY_ROLE_DESCRIPTIONS => SerializedDataFactory::createRoleTranslationsCollectionArray(),
            ]
        );

        $result = LocalizedInfoWithRequiredLabel::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
