<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Utility;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class LocalizedInfoWithOptionalLabelTest.
 *
 * @covers  \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel
 */
class LocalizedInfoWithOptionalLabelTest extends TestCase
{
    /**
     * @return array
     */
    private function getSerializedArrayFromValidMinimalAndOptionalData()
    {
        return [
            LocalizedInfoWithOptionalLabel::KEY_LABEL => ['nl' => 'dutch label string'],
            LocalizedInfoWithOptionalLabel::KEY_DESCRIPTION => ['nl' => 'dutch description string'],
            LocalizedInfoWithOptionalLabel::KEY_ROLE_DESCRIPTIONS => [
                [
                    RoleLocalizedOptionalString::KEY_ROLE => 'TEST_ROLE',
                    RoleLocalizedOptionalString::KEY_TRANSLATIONS => ['nl' => 'dutch role test description string'],
                ],
            ],
        ];
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::fromLocalizedStrings()
     */
    public function testFromLocalizedStringsWithNoneOfTheOptionalStringsShouldReturnInstance(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        static::assertInstanceOf(LocalizedInfoWithOptionalLabel::class, $result);

        static::assertFalse($result->hasLabel('nl'));
        static::assertFalse($result->hasDescription('nl'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_TEST'), 'nl'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::fromLocalizedStrings()
     */
    public function testFromLocalizedStringsWithOnlyMinimalOptionalLabelShouldReturnInstance(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchTranslation(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        static::assertInstanceOf(LocalizedInfoWithOptionalLabel::class, $result);

        static::assertTrue($result->hasLabel('nl'));
        static::assertFalse($result->hasDescription('nl'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_TEST'), 'nl'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::fromLocalizedStrings()
     */
    public function testFromLocalizedStringsWithAllOptionalStringsShouldReturnInstance(): void
    {
        $label = ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations();
        $dutchLabelString = $label->getForLocale('nl');

        $description = ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations();
        $dutchDescriptionString = $description->getForLocale('nl');

        $roleDescriptions = ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish();
        $firstRoleDescription = $roleDescriptions->getOneForRole(ValueObjectFactory::createFirstRole());
        $firstRoleDescriptionDutch = $firstRoleDescription->getLocalizedString()->getForLocale('nl');

        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            $label,
            $description,
            $roleDescriptions
        );

        static::assertInstanceOf(LocalizedInfoWithOptionalLabel::class, $result);

        // test locale nl does exist
        static::assertTrue($result->hasLabel('nl'));
        static::assertSame($dutchLabelString, $result->getLabel('nl'));

        static::assertTrue($result->hasDescription('nl'));
        static::assertSame($dutchDescriptionString, $result->getDescription('nl'));

        static::assertTrue($result->hasRoleDescription($firstRoleDescription->getRole(), 'nl'));
        static::assertSame($firstRoleDescriptionDutch, $result->getRoleDescription($firstRoleDescription->getRole(), 'nl'));

        // test fallback: if a description exist for one locale, then fallback should return a fallback for any locale
        static::assertTrue($result->hasLabel('fake'));
        static::assertSame($description->getFallback(), $result->getLabel('fake'));

        static::assertTrue($result->hasDescription('fake'));
        static::assertSame($description->getFallback(), $result->getDescription('fake'));

        static::assertTrue($result->hasRoleDescription($firstRoleDescription->getRole(), 'fake'));
        static::assertSame($firstRoleDescription->getLocalizedString()->getFallback(), $result->getRoleDescription($firstRoleDescription->getRole(), 'fake'));

        // if no translation exist
        static::assertFalse($result->hasRoleDescription(Role::fromName('NON_EXISTING_ROLE'), 'nl'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('NON_EXISTING_ROLE'), 'fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithNoDataShouldReturnInstance(): void
    {
        $data = [];

        $result = LocalizedInfoWithOptionalLabel::fromJsonDecodedArray($data);

        static::assertInstanceOf(LocalizedInfoWithOptionalLabel::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWitAllOptionalDataShouldReturnInstance(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromJsonDecodedArray(
            $this->getSerializedArrayFromValidMinimalAndOptionalData()
        );

        static::assertInstanceOf(LocalizedInfoWithOptionalLabel::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::getLocalizedLabel()
     */
    public function testGetLocalizedLabelShouldReturnLocalizedOptionalString(): void
    {
        $resultWithOptionals = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
        );

        static::assertInstanceOf(LocalizedOptionalString::class, $resultWithOptionals->getLocalizedLabel());

        $resultWithNoOptionals = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        static::assertInstanceOf(LocalizedOptionalString::class, $resultWithNoOptionals->getLocalizedLabel());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::getLabel()
     */
    public function testGetLabelIfAtLeastOneTranslationExistShouldAlwaysReturnTranslatedOrFallbackString(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
        );

        $localizedLabel = $result->getLocalizedLabel();

        // test locales
        static::assertSame($localizedLabel->getForLocale('nl'), $result->getLabel('nl'));
        static::assertSame($localizedLabel->getForLocale('en'), $result->getLabel('en'));
        // test fallback
        static::assertSame($localizedLabel->getFallback(), $result->getLabel('fake'));
        static::assertSame($localizedLabel->getForLocaleOrFallback('fake'), $result->getLabel('fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::getLocalizedDescription()
     */
    public function testGetLocalizedDescriptionIfNoTranslationExistShouldAlwaysReturnLocalizedOptionalString(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        static::assertInstanceOf(LocalizedOptionalString::class, $result->getLocalizedDescription());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::getLocalizedDescription()
     */
    public function testGetLocalizedDescriptionIfAtLeastTranslationExistShouldReturnLocalizedOptionalString(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchTranslation(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        static::assertInstanceOf(LocalizedOptionalString::class, $result->getLocalizedDescription());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::hasDescription()
     */
    public function testHasDescriptionIfNoTranslationsExistShouldAlwaysReturnFalse(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
        );

        // test locales
        static::assertFalse($result->hasDescription('nl'));
        static::assertFalse($result->hasDescription('en'));
        // test fallback
        static::assertFalse($result->hasDescription('fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::hasDescription()
     */
    public function testHasDescriptionIfAtLeastTranslationExistShouldAlwaysReturnTrue(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchTranslation(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
        );

        // test locales
        static::assertTrue($result->hasDescription('nl'));
        static::assertTrue($result->hasDescription('en'));
        // test fallback
        static::assertTrue($result->hasDescription('fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::getDescription()
     */
    public function testGetDescriptionIfNoTranslationsExistShouldThrowLocalizedStringException(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithDutchTranslation()
        );

        $this->expectException(LocalizedStringException::class);
        $result->getDescription('nl');
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::getDescription()
     */
    public function testGetDescriptionIfAtLeastOneTranslationsExistShouldAlwaysReturnTranslatedOrFallbackString(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
        );

        $localizedDescription = $result->getLocalizedDescription();

        // test known locales
        static::assertSame($localizedDescription->getForLocale('nl'), $result->getDescription('nl'));
        static::assertSame($localizedDescription->getForLocale('en'), $result->getDescription('en'));
        // test fallback
        static::assertSame($localizedDescription->getFallback(), $result->getDescription('fake'));
        static::assertSame($localizedDescription->getForLocaleOrFallback('fake'), $result->getDescription('fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::getRoleLocalizedDescriptions()
     */
    public function testGetRoleLocalizedDescriptionsIfNoRolesExistShouldReturnRoleLocalizedOptionalStringCollection(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        static::assertInstanceOf(RoleLocalizedOptionalStringCollection::class, $result->getRoleLocalizedDescriptions());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::getRoleLocalizedDescriptions()
     */
    public function testGetRoleLocalizedDescriptionsIfAtLeastOneRoleExistShouldReturnRoleLocalizedOptionalStringCollection(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithDutchTranslation()
        );

        static::assertInstanceOf(RoleLocalizedOptionalStringCollection::class, $result->getRoleLocalizedDescriptions());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::hasRoleDescription()
     */
    public function testHasRoleDescriptionIfNoRoleExistShouldAlwaysReturnFalse(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        // test locales
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'nl'));
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'en'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'en'));
        // test fallback
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'fake'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::hasRoleDescription()
     */
    public function testHasRoleDescriptionIfNoRoleHavingTranslationsExistShouldAlwaysReturnFalse(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithNoTranslations()
        );

        // test locales
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'nl'));
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'en'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'en'));
        // test fallback
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'fake'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::hasRoleDescription()
     */
    public function testHasRoleDescriptionIfAtLeastOneRoleHavingTranslationsShouldAlwaysReturnTrue(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithNoTranslations()
        );

        // test locales
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'nl'));
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'en'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'en'));
        // test fallback
        static::assertFalse($result->hasRoleDescription(ValueObjectFactory::createFirstRole(), 'fake'));
        static::assertFalse($result->hasRoleDescription(Role::fromName('ROLE_FAKE'), 'fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::getRoleDescription()
     */
    public function testGetRoleDescriptionIfNoRoleExistShouldThrowBadMethodCallException(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingNoRoles()
        );

        $this->expectException(\BadMethodCallException::class);
        $result->getRoleDescription(ValueObjectFactory::createFirstRole(), 'nl');
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::getRoleDescription()
     */
    public function testGetRoleDescriptionIfNoRoleHavingTranslationsExistShouldThrowLocalizedStringException(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithNoTranslations()
        );

        $this->expectException(LocalizedStringException::class);
        $result->getRoleDescription(ValueObjectFactory::createFirstRole(), 'nl');
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::getRoleDescription()
     */
    public function testGetRoleDescriptionIfAtLeastOneRoleHavingTranslationsExistShouldAlwaysReturnTranslatedOrFallbackString(): void
    {
        $result = LocalizedInfoWithOptionalLabel::fromLocalizedStrings(
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithDutchTranslation()
        );

        $localizedRoleDescription = $result->getLocalizedDescription();

        // test known locales
        static::assertSame($localizedRoleDescription->getForLocale('nl'), $result->getRoleDescription(ValueObjectFactory::createFirstRole(), 'nl'));
        // test fallback
        static::assertSame($localizedRoleDescription->getFallback(), $result->getRoleDescription(ValueObjectFactory::createFirstRole(), 'fake'));
        static::assertSame($localizedRoleDescription->getForLocaleOrFallback('fake'), $result->getRoleDescription(ValueObjectFactory::createFirstRole(), 'fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode(
            [
                LocalizedInfoWithOptionalLabel::KEY_LABEL => SerializedDataFactory::createTranslationsArray(false, false),
                LocalizedInfoWithOptionalLabel::KEY_DESCRIPTION => SerializedDataFactory::createTranslationsArray(false, true),
                LocalizedInfoWithOptionalLabel::KEY_ROLE_DESCRIPTIONS => SerializedDataFactory::createRoleTranslationsCollectionArray(),
            ]
        );

        $result = LocalizedInfoWithOptionalLabel::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
