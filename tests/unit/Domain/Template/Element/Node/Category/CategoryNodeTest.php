<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\Node\Category;

use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\CategoryNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\NodeCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithOptionalLabel;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use PHPUnit\Framework\TestCase;

class CategoryNodeTest extends TestCase
{
    public function testItFlattensMultipleNestedCategories(): void
    {
        $daughterCategory = CategoryNode::fromUidAndDependencies(
            'ab1d4d82-d4f2-4698-b908-4503b68f4965',
            LocalizedInfoWithOptionalLabel::createEmpty(),
            false,
            new NodeCollection([
                $this->getQuestion(
                    'bc0274de-4cbc-4e73-a8e3-ba928c991c26',
                    'What is the name of your grandmother?'
                ),
            ]),
            FlowPermission::createEmpty()
        );

        $sonCategory = CategoryNode::fromUidAndDependencies(
            '9b91ba93-eeb6-496a-9008-c61a8186f95c',
            LocalizedInfoWithOptionalLabel::createEmpty(),
            false,
            new NodeCollection([
                $this->getQuestion(
                    '0aa2a070-7950-4cc3-b0a3-f315e5918595',
                    'What is the name of your grandfather?'
                ),
            ]),
            FlowPermission::createEmpty()
        );

        $parentCategory = CategoryNode::fromUidAndDependencies(
            'a446c25f-e80c-4c1f-afda-e9576703c24a',
            LocalizedInfoWithOptionalLabel::createEmpty(),
            false,
            new NodeCollection([
                $this->getQuestion(
                    'b2d09e29-3120-4008-a090-a18c37ae4c9b',
                    'What is the name of your mother?'
                ),
                $daughterCategory,
                $sonCategory,
            ]),
            FlowPermission::createEmpty()
        );

        $grandparentCategory = CategoryNode::fromUidAndDependencies(
            '6724ac6c-51b1-4e81-bf70-8039f30a4ca3',
            LocalizedInfoWithOptionalLabel::createEmpty(),
            false,
            new NodeCollection([
                $parentCategory,
                $this->getQuestion(
                    '6277d477-2d1f-49cc-a86f-5e8263395f6e',
                    'What is your name?'
                ),
            ]),
            FlowPermission::createEmpty()
        );

        $categories = $grandparentCategory->getFlattenedCategoryNodeDescendants();

        $expectedCategories = new CategoryNodeCollection([
            $parentCategory,
            $daughterCategory,
            $sonCategory,
        ]);

        static::assertEquals($expectedCategories, $categories);
    }

    private function getQuestion(string $uid, string $question): TextInputNode
    {
        return TextInputNode::fromUidAndMinimalDependencies(
            $uid,
            new TextInput(
                false,
                TextInput::DISPLAY_MODE_REGULAR,
                LocalizedOptionalString::createEmpty(),
                false,
                0,
                255,
                []
            ),
            LocalizedInfoWithRequiredLabel::fromLocalizedRequiredLabel(
                LocalizedRequiredString::fromLocaleTranslations(['en' => $question])
            )
        );
    }
}
