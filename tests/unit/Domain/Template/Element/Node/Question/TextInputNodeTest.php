<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\Node\Question;

use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use PHPUnit\Framework\TestCase;

class TextInputNodeTest extends TestCase
{
    public function testItKeepsTrackOfOtherTextInputIdsToPrefillWith(): void
    {
        $whatColorIsTheSea = $this->getQuestion('2a8a658a-d552-4997-ac8e-f8a277a04f30', 'What color is the sea?');
        $whatColorIsGrass = $this->getQuestion('030ec43b-1152-44a8-a8f6-20a6615da80d', 'What color is grass?');

        $whatColorIsTheWorld = $this->getQuestion('9b8cc01e-645c-4e24-b7f7-598f556d5be7', 'What color is the world?');
        $whatColorIsTheWorld->prefillWith($whatColorIsTheSea, $whatColorIsGrass);

        $expectedIds = [
            '2a8a658a-d552-4997-ac8e-f8a277a04f30',
            '030ec43b-1152-44a8-a8f6-20a6615da80d',
        ];

        static::assertEquals($expectedIds, $whatColorIsTheWorld->getPrefillingQuestionUids());
    }

    public function testItCanNotPrefillItself(): void
    {
        $howMuchIsTheFish = $this->getQuestion('599b45eb-e0c0-40b0-94af-89bc4ef011e6', 'How much is the fish?');

        $this->expectExceptionMessage('Question with ID 599b45eb-e0c0-40b0-94af-89bc4ef011e6 can not be used to prefill itself.');

        $howMuchIsTheFish->prefillWith($howMuchIsTheFish);
    }

    private function getQuestion(string $uid, string $question): TextInputNode
    {
        return TextInputNode::fromUidAndMinimalDependencies(
            $uid,
            new TextInput(
                false,
                TextInput::DISPLAY_MODE_REGULAR,
                LocalizedOptionalString::createEmpty(),
                false,
                0,
                255,
                []
            ),
            LocalizedInfoWithRequiredLabel::fromLocalizedRequiredLabel(
                LocalizedRequiredString::fromLocaleTranslations(['en' => $question])
            )
        );
    }
}
