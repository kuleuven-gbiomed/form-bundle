<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\WorkFlowStep;

use KUL\FormBundle\Domain\Template\Contract\StepInterface;
use KUL\FormBundle\Domain\Template\Contract\StepPeriodInterface;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod;

/**
 * Class DummyAbstractStep.
 *
 * this a dummy class intended to check the methods in @see AbstractStep
 * its intended to test the methods in that abstract class.
 * if a child class overwrites a method or is expected to return a specific type of implementation,
 * then that method should be tested for that child class itself
 */
final class DummyAbstractStep extends AbstractStep
{
    public static function getType(): string
    {
        return 'dummy_abstract_step';
    }

    protected static function guardStepPeriodClass(StepPeriodInterface $period): void
    {
    }

    /**
     * checks if there is overlap between @see StepCalculatedPeriod periods for this and given control step,
     * based on an arbitrary dynamically date to calculated each period.
     *
     * @param StepInterface|DummyAbstractStep $controlStep
     *
     * @throws \Exception
     */
    public function hasOverlapWithStep(StepInterface $controlStep): bool
    {
        throw new \DomainException('dummy class for testing; this method is tested in concrete implementations');
    }

    /**
     * checks if there is step starts before the given control step does,
     * based on an arbitrary dynamically date to calculated start date of each step.
     *
     * @throws \Exception
     */
    public function startsBeforeStep(StepInterface $controlStep): never
    {
        throw new \DomainException('dummy class for testing; this method is tested in concrete implementations');
    }

    public static function getPeriodFromJsonDecodedArray(array $data): DummyStepPeriod
    {
        return new DummyStepPeriod();
    }
}
