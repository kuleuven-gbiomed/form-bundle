<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\WorkFlowStep;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class StepWithCalculatedDateStartTest.
 *
 * @covers  \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart
 */
class StepWithCalculatedDateStartTest extends TestCase
{
    /** @var string */
    private $stepUid = 'test_uid';

    /** @var \DateTimeImmutable */
    private $referenceDate;

    protected function setUp(): void
    {
        parent::setUp();

        $this->referenceDate = new \DateTimeImmutable('2019-05-20 10:00');
    }

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart::fromUidAndDependencies()
     */
    public function testFromUidAndDependencies(): void
    {
        $result = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertInstanceOf(StepWithCalculatedDateStart::class, $result);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithFullKeyedDataShouldReturnInstance(): void
    {
        $data = [
            StepWithCalculatedDateStart::KEY_TYPE => StepWithCalculatedDateStart::getType(),
            StepWithCalculatedDateStart::KEY_UID => $this->stepUid,
            StepWithCalculatedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => false,
            StepWithCalculatedDateStart::KEY_PERIOD => SerializedDataFactory::createStepCalculatedPeriodArray(),
            StepWithCalculatedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => SerializedDataFactory::createRolesArray(),
            StepWithCalculatedDateStart::KEY_INFO => SerializedDataFactory::createInfoArray(),
            StepWithCalculatedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => SerializedDataFactory::createStepSubmitSuccessUserInteractionArray(),
            StepWithCalculatedDateStart::KEY_HIDE_READ_ONLY_NODES_ON_LANDING => false,
        ];

        $result = StepWithCalculatedDateStart::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepWithCalculatedDateStart::class, $result);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithMinimalRequiredKeyedDataShouldReturnInstance(): void
    {
        $data = [
            StepWithCalculatedDateStart::KEY_TYPE => StepWithCalculatedDateStart::getType(),
            StepWithCalculatedDateStart::KEY_UID => $this->stepUid,
            StepWithCalculatedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => false,
            StepWithCalculatedDateStart::KEY_PERIOD => SerializedDataFactory::createStepCalculatedPeriodArray(),
            StepWithCalculatedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => [],
            StepWithCalculatedDateStart::KEY_INFO => SerializedDataFactory::createInfoArray(),
            StepWithCalculatedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => [],
            StepWithCalculatedDateStart::KEY_HIDE_READ_ONLY_NODES_ON_LANDING => false,
        ];

        $result = StepWithCalculatedDateStart::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepWithCalculatedDateStart::class, $result);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithoutNonRequiredKeyedDataShouldReturnInstance(): void
    {
        $data = [
            StepWithCalculatedDateStart::KEY_TYPE => StepWithCalculatedDateStart::getType(),
            StepWithCalculatedDateStart::KEY_UID => $this->stepUid,
            StepWithCalculatedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => false,
            StepWithCalculatedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => [],
            StepWithCalculatedDateStart::KEY_PERIOD => SerializedDataFactory::createStepCalculatedPeriodArray(),
            StepWithCalculatedDateStart::KEY_INFO => SerializedDataFactory::createInfoArray(),
            StepWithCalculatedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => [],
        ];

        $result = StepWithCalculatedDateStart::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepWithCalculatedDateStart::class, $result);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart::getType()
     */
    public function testGetType(): void
    {
        $result = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertSame(StepWithCalculatedDateStart::getType(), $result::getType());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart::getPeriod()
     */
    public function testGetPeriod(): void
    {
        $result = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertInstanceOf(StepCalculatedPeriod::class, $result->getPeriod());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart::withPreviousStepFulFilledStartOption()
     */
    public function testWithPreviousStepFulFilledStartOptionShouldReturnNewInstanceOfOriginalStep(): void
    {
        $result = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertFalse($result->allowsToStartPrematurelyIfPreviousStepIsFulFilled());

        $resultWith = $result->withPreviousStepFulFilledStartOption();
        static::assertTrue($resultWith->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
        static::assertInstanceOf(StepWithCalculatedDateStart::class, $resultWith);
        static::assertSame($result->getUid(), $resultWith->getUid());
        static::assertNotSame($result, $resultWith);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart::withoutPreviousStepFulFilledStartOption()
     */
    public function testWithoutPreviousStepFulFilledStartOptionShouldReturnNewInstanceOfOriginalStep(): void
    {
        $result = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            true,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($result->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
        $resultWithout = $result->withoutPreviousStepFulFilledStartOption();
        static::assertFalse($resultWithout->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
        static::assertInstanceOf(StepWithCalculatedDateStart::class, $resultWithout);
        static::assertSame($result->getUid(), $resultWithout->getUid());
        static::assertNotSame($result, $resultWithout);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart::startsBeforeCalculatedStep
     */
    public function testStartsBeforeCalculatedStep(): void
    {
        $stepOne = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            true,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        $stepTwo = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(10, +11),
            true,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        $stepThree = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-5, +5),
            true,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($stepOne->startsBeforeCalculatedStep($stepTwo));
        static::assertFalse($stepTwo->startsBeforeCalculatedStep($stepOne));

        static::assertTrue($stepOne->startsBeforeCalculatedStep($stepThree));
        static::assertFalse($stepThree->startsBeforeCalculatedStep($stepOne));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart::overlapsWithCalculatedStep
     */
    public function testOverlapsWithCalculatedStep(): void
    {
        $stepOne = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            true,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        $stepTwo = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(10, +11),
            true,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        $stepThree = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-5, +5),
            true,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertFalse($stepOne->overlapsWithCalculatedStep($stepTwo));
        static::assertTrue($stepOne->overlapsWithCalculatedStep($stepThree));
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            StepWithCalculatedDateStart::KEY_TYPE => StepWithCalculatedDateStart::getType(),
            StepWithCalculatedDateStart::KEY_UID => $this->stepUid,
            StepWithCalculatedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => false,
            StepWithCalculatedDateStart::KEY_PERIOD => SerializedDataFactory::createStepCalculatedPeriodArray(),
            StepWithCalculatedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => SerializedDataFactory::createRolesArray(),
            StepWithCalculatedDateStart::KEY_INFO => SerializedDataFactory::createInfoArray(),
            StepWithCalculatedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => SerializedDataFactory::createStepSubmitSuccessUserInteractionArray(),
            StepWithCalculatedDateStart::KEY_HIDE_READ_ONLY_NODES_ON_LANDING => true,
            StepWithCalculatedDateStart::KEY_CUSTOM_OPTIONS => [],
        ]);

        $result = StepWithCalculatedDateStart::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public function testIsOpenByDateWithStepCalculatedPeriod(): void
    {
        $result = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($result->isOpenByDate($this->referenceDate, $this->referenceDate));
        static::assertFalse($result->isOpenByDate($this->referenceDate, $this->referenceDate->modify('-11 days'))); // end date is yesterday
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public function testIsStartedByDateWithCalculatedPeriod(): void
    {
        $result = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($result->isStartedByDate($this->referenceDate, $this->referenceDate));
        static::assertFalse($result->isStartedByDate($this->referenceDate, $this->referenceDate->modify('+200 days')));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public function testIsEndedByDateWithCalculatedPeriod(): void
    {
        $result = StepWithCalculatedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertFalse($result->isEndedByDate($this->referenceDate, $this->referenceDate));
        static::assertTrue($result->isEndedByDate($this->referenceDate, $this->referenceDate->modify('-11 days')));
    }
}
