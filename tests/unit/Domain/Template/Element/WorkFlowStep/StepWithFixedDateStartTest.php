<?php

/** @noinspection PhpUnhandledExceptionInspection */
/* @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\WorkFlowStep;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class StepWithFixedDateStartTest.
 *
 * @covers  \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart
 */
class StepWithFixedDateStartTest extends TestCase
{
    /** @var string */
    private $stepUid = 'test_uid';

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::fromUidAndDependencies()
     */
    public function testFromUidAndDependencies(): void
    {
        $result = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            ValueObjectFactory::createStepFixedPeriod(
                new \DateTimeImmutable('2019-05-22 8:30')
            ),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertInstanceOf(StepWithFixedDateStart::class, $result);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithFullKeyedDataShouldReturnInstance(): void
    {
        $data = [
            StepWithFixedDateStart::KEY_TYPE => StepWithFixedDateStart::getType(),
            StepWithFixedDateStart::KEY_UID => $this->stepUid,
            StepWithFixedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => false,
            StepWithFixedDateStart::KEY_PERIOD => SerializedDataFactory::createStepFixedPeriodArray(new \DateTimeImmutable('2019-05-21 13:00')),
            StepWithFixedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => SerializedDataFactory::createRolesArray(),
            StepWithFixedDateStart::KEY_INFO => SerializedDataFactory::createInfoArray(),
            StepWithFixedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => SerializedDataFactory::createStepSubmitSuccessUserInteractionArray(),
            StepWithFixedDateStart::KEY_HIDE_READ_ONLY_NODES_ON_LANDING => false,
        ];

        $result = StepWithFixedDateStart::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepWithFixedDateStart::class, $result);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithMinimalRequiredKeyedDataShouldReturnInstance(): void
    {
        $data = [
            StepWithFixedDateStart::KEY_TYPE => StepWithFixedDateStart::getType(),
            StepWithFixedDateStart::KEY_UID => $this->stepUid,
            StepWithFixedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => false,
            StepWithFixedDateStart::KEY_PERIOD => SerializedDataFactory::createStepFixedPeriodArray(
                new \DateTimeImmutable('2019-05-21 13:00')
            ),
            StepWithFixedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => [],
            StepWithFixedDateStart::KEY_INFO => SerializedDataFactory::createInfoArray(),
            StepWithFixedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => [],
            StepWithFixedDateStart::KEY_HIDE_READ_ONLY_NODES_ON_LANDING => false,
        ];

        $result = StepWithFixedDateStart::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepWithFixedDateStart::class, $result);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithoutNonRequiredKeyedDataShouldReturnInstance(): void
    {
        $data = [
            StepWithFixedDateStart::KEY_TYPE => StepWithFixedDateStart::getType(),
            StepWithFixedDateStart::KEY_UID => $this->stepUid,
            StepWithFixedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => false,
            StepWithFixedDateStart::KEY_PERIOD => SerializedDataFactory::createStepFixedPeriodArray(new \DateTimeImmutable('2019-05-21 13:00')),
            StepWithFixedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => [],
            StepWithFixedDateStart::KEY_INFO => SerializedDataFactory::createInfoArray(),
            StepWithFixedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => [],
        ];

        $result = StepWithFixedDateStart::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepWithFixedDateStart::class, $result);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::getType()
     */
    public function testGetType(): void
    {
        $result = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            ValueObjectFactory::createStepFixedPeriod(
                new \DateTimeImmutable('2019-05-22 8:30')
            ),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertSame(StepWithFixedDateStart::getType(), $result::getType());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::getPeriod()
     */
    public function testGetPeriod(): void
    {
        $result = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            ValueObjectFactory::createStepFixedPeriod(
                new \DateTimeImmutable('2019-05-22 8:30')
            ),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertInstanceOf(StepFixedPeriod::class, $result->getPeriod());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::allowsToStartPrematurelyIfPreviousStepIsFulFilled()
     */
    public function testAllowsToStartPrematurelyIfPreviousStepIsFulFilled(): void
    {
        $result = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            ValueObjectFactory::createStepFixedPeriod(
                new \DateTimeImmutable('2019-05-22 8:30')
            ),
            true,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($result->allowsToStartPrematurelyIfPreviousStepIsFulFilled());

        $result = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            ValueObjectFactory::createStepFixedPeriod(
                new \DateTimeImmutable('2019-05-22 8:30')
            ),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertFalse($result->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::withPreviousStepFulFilledStartOption()
     */
    public function testWithPreviousStepFulFilledStartOptionShouldReturnNewInstanceOfOriginalStep(): void
    {
        $result = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            ValueObjectFactory::createStepFixedPeriod(new \DateTimeImmutable('2019-05-21 15:30'), 10, 10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertFalse($result->allowsToStartPrematurelyIfPreviousStepIsFulFilled());

        $resultWith = $result->withPreviousStepFulFilledStartOption();
        static::assertTrue($resultWith->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
        static::assertInstanceOf(StepWithFixedDateStart::class, $resultWith);
        static::assertSame($result->getUid(), $resultWith->getUid());
        static::assertNotSame($result, $resultWith);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::withoutPreviousStepFulFilledStartOption()
     */
    public function testWithoutPreviousStepFulFilledStartOptionShouldReturnNewInstanceOfOriginalStep(): void
    {
        $result = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            ValueObjectFactory::createStepFixedPeriod(new \DateTimeImmutable('2019-05-21 15:30'), 10, 10),
            true,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($result->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
        $resultWithout = $result->withoutPreviousStepFulFilledStartOption();
        static::assertFalse($resultWithout->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
        static::assertInstanceOf(StepWithFixedDateStart::class, $resultWithout);
        static::assertSame($result->getUid(), $resultWithout->getUid());
        static::assertNotSame($result, $resultWithout);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::startsBeforeFixedStep
     */
    public function testStartsBeforeFixedStep(): void
    {
        $stepOne = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepFixedPeriod::fromDates(
                new \DateTimeImmutable('2019-05-12 8:30'),
                new \DateTimeImmutable('2019-06-01 8:30')
            ),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        $stepTwo = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepFixedPeriod::fromDates(
                new \DateTimeImmutable('2019-06-01 8:30'),
                new \DateTimeImmutable('2019-06-02 8:30')
            ),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        $stepThree = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepFixedPeriod::fromDates(
                new \DateTimeImmutable('2019-05-17 8:30'),
                new \DateTimeImmutable('2019-06-27 8:30')
            ),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($stepOne->startsBeforeFixedStep($stepTwo));
        static::assertFalse($stepTwo->startsBeforeFixedStep($stepOne));

        static::assertTrue($stepOne->startsBeforeFixedStep($stepThree));
        static::assertFalse($stepThree->startsBeforeFixedStep($stepOne));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::overlapsWithFixedStep
     */
    public function testOverlapsWithFixedStep(): void
    {
        $stepOne = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepFixedPeriod::fromDates(
                new \DateTimeImmutable('2019-05-12 8:30'),
                new \DateTimeImmutable('2019-06-01 8:30')
            ),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        $stepTwo = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepFixedPeriod::fromDates(
                new \DateTimeImmutable('2019-06-01 8:30'),
                new \DateTimeImmutable('2019-06-02 8:30')
            ),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        $stepThree = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepFixedPeriod::fromDates(
                new \DateTimeImmutable('2019-05-17 8:30'),
                new \DateTimeImmutable('2019-06-27 8:30')
            ),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertFalse($stepOne->overlapsWithFixedStep($stepTwo));
        static::assertTrue($stepOne->overlapsWithFixedStep($stepThree));
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            StepWithFixedDateStart::KEY_TYPE => StepWithFixedDateStart::getType(),
            StepWithFixedDateStart::KEY_UID => $this->stepUid,
            StepWithFixedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => true,
            StepWithFixedDateStart::KEY_PERIOD => SerializedDataFactory::createStepFixedPeriodArray(
                new \DateTimeImmutable('2019-05-21 13:00')
            ),
            StepWithFixedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => SerializedDataFactory::createRolesArray(),
            StepWithFixedDateStart::KEY_INFO => SerializedDataFactory::createInfoArray(),
            StepWithFixedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => SerializedDataFactory::createStepSubmitSuccessUserInteractionArray(),
            StepWithFixedDateStart::KEY_HIDE_READ_ONLY_NODES_ON_LANDING => true,
            StepWithCalculatedDateStart::KEY_CUSTOM_OPTIONS => [],
        ]);

        $result = StepWithFixedDateStart::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public function testIsOpenByDateWithStepFixedPeriod(): void
    {
        $result = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepFixedPeriod::fromDates(
                new \DateTimeImmutable('2019-05-12 8:30'),
                new \DateTimeImmutable('2019-06-01 8:30')
            ),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($result->isOpenByDate(
            new \DateTimeImmutable('2019-05-22 8:30')
        ));
        static::assertFalse($result->isOpenByDate(
            new \DateTimeImmutable('2019-05-11 8:30')
        ));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public function testIsStartedByDateWithFixedPeriod(): void
    {
        $result = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepFixedPeriod::fromDates(
                new \DateTimeImmutable('2019-05-12 8:30'),
                new \DateTimeImmutable('2019-06-01 8:30')
            ),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($result->isStartedByDate(
            new \DateTimeImmutable('2019-05-22 8:30')
        ));
        static::assertFalse($result->isStartedByDate(
            new \DateTimeImmutable('2019-05-11 8:30')
        ));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public function testIsEndedByDateWithFixedPeriod(): void
    {
        $result = StepWithFixedDateStart::fromUidAndDependencies(
            $this->stepUid,
            StepFixedPeriod::fromDates(
                new \DateTimeImmutable('2019-05-12 8:30'),
                new \DateTimeImmutable('2019-06-01 8:30')
            ),
            true,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertFalse($result->isEndedByDate(
            new \DateTimeImmutable('2019-05-22 8:30')
        ));
        static::assertTrue($result->isEndedByDate(
            new \DateTimeImmutable('2019-06-02 8:30')
        ));
    }
}
