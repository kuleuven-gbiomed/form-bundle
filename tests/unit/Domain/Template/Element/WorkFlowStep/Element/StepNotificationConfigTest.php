<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\WorkFlowStep\Element;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Utility\Collection\RoleNotificationConfigCollection;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class StepNotificationConfigTest.
 *
 * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig
 */
class StepNotificationConfigTest extends TestCase
{
    /** @var RoleNotificationConfig */
    private $firstRoleStartNotificationConfig;

    /** @var RoleNotificationConfig */
    private $secondRoleStartNotificationConfig;

    /** @var RoleNotificationConfig */
    private $firstRoleOngoingNotificationConfig;

    /** @var RoleNotificationConfig */
    private $secondRoleOngoingNotificationConfig;

    /**
     * @throws StepNotificationException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->firstRoleStartNotificationConfig = RoleNotificationConfig::fromRoleAndOffset(
            ValueObjectFactory::createFirstRole(),
            5
        );
        $this->firstRoleOngoingNotificationConfig = RoleNotificationConfig::fromRoleAndOffset(
            ValueObjectFactory::createFirstRole(),
            6
        );
        $this->secondRoleStartNotificationConfig = RoleNotificationConfig::fromRoleAndOffset(
            ValueObjectFactory::createSecondRole(),
            10
        );
        $this->secondRoleOngoingNotificationConfig = RoleNotificationConfig::fromRoleAndOffset(
            ValueObjectFactory::createSecondRole(),
            15
        );
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::fromRoleNotificationsAndMailBodies
     *
     * @throws StepNotificationException
     */
    public function testFromRoleNotificationsWithOnlyStartConfigsShouldReturnInstance(): void
    {
        $result = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([$this->firstRoleStartNotificationConfig, $this->secondRoleStartNotificationConfig]),
            new RoleNotificationConfigCollection([]),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );

        static::assertInstanceOf(StepNotificationConfig::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::fromRoleNotificationsAndMailBodies
     *
     * @throws StepNotificationException
     */
    public function testFromRoleNotificationsWithOnlyOngoingConfigsShouldReturnInstance(): void
    {
        $result = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([]),
            new RoleNotificationConfigCollection([$this->firstRoleOngoingNotificationConfig, $this->secondRoleOngoingNotificationConfig]),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );

        static::assertInstanceOf(StepNotificationConfig::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::fromRoleNotificationsAndMailBodies
     *
     * @throws StepNotificationException
     */
    public function testFromRoleNotificationsWithStartAndOngoingConfigsHavingValidOffsetsShouldReturnInstance(): void
    {
        $result = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([$this->firstRoleStartNotificationConfig, $this->secondRoleStartNotificationConfig]),
            new RoleNotificationConfigCollection([$this->firstRoleOngoingNotificationConfig, $this->secondRoleOngoingNotificationConfig]),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );

        static::assertInstanceOf(StepNotificationConfig::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::fromRoleNotificationsAndMailBodies
     *
     * @throws StepNotificationException
     */
    public function testFromRoleNotificationsWithStartAndOngoingConfigsHavingInvalidOffsetsShouldThrowStepNotificationException(): void
    {
        $offset = 1;
        $firstRoleStartNotificationConfig = RoleNotificationConfig::fromRoleAndOffset(
            ValueObjectFactory::createFirstRole(),
            $offset
        );
        $firstRoleOngoingNotificationConfig = RoleNotificationConfig::fromRoleAndOffset(
            ValueObjectFactory::createFirstRole(),
            $offset
        );

        $this->expectException(StepNotificationException::class);

        StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([$firstRoleStartNotificationConfig]),
            new RoleNotificationConfigCollection([$firstRoleOngoingNotificationConfig]),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::fromJsonDecodedArray
     */
    public function testFromSerializedArrayWithAllDataShouldReturnInstance(): void
    {
        $dataFirstRoleStartConfigArray = JsonDecoder::toAssocArray(json_encode($this->firstRoleStartNotificationConfig));
        $dataSecondRoleStartConfigArray = JsonDecoder::toAssocArray(json_encode($this->secondRoleStartNotificationConfig));
        $dataSecondRoleOngoingConfigArray = JsonDecoder::toAssocArray(json_encode($this->secondRoleOngoingNotificationConfig));
        $mailBodyStartArray = JsonDecoder::toAssocArray(json_encode(ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()));
        $mailBodyOngoingArray = JsonDecoder::toAssocArray(json_encode(ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()));

        $data = [
            StepNotificationConfig::KEY_ROLE_CONFIGS_TO_NOTIFY_OF_START => [$dataFirstRoleStartConfigArray, $dataSecondRoleStartConfigArray],
            StepNotificationConfig::KEY_ROLE_CONFIGS_TO_REMIND_OF_ONGOING => [$dataSecondRoleOngoingConfigArray],
            StepNotificationConfig::KEY_MAIL_BODY_FOR_START => $mailBodyStartArray,
            StepNotificationConfig::KEY_MAIL_BODY_FOR_ONGOING => $mailBodyOngoingArray,
        ];

        $result = StepNotificationConfig::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepNotificationConfig::class, $result);
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::fromJsonDecodedArray
     */
    public function testFromSerializedArrayWithMinimalDataShouldReturnInstance(): void
    {
        $data = [
            StepNotificationConfig::KEY_ROLE_CONFIGS_TO_NOTIFY_OF_START => [],
            StepNotificationConfig::KEY_ROLE_CONFIGS_TO_REMIND_OF_ONGOING => [],
            StepNotificationConfig::KEY_MAIL_BODY_FOR_START => [],
            StepNotificationConfig::KEY_MAIL_BODY_FOR_ONGOING => [],
        ];

        $result = StepNotificationConfig::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepNotificationConfig::class, $result);
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::fromJsonDecodedArray
     */
    public function testFromSerializedArrayWithNoDataShouldReturnInstance(): void
    {
        $data = [];

        $result = StepNotificationConfig::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepNotificationConfig::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::getRoleConfigsToNotifyOfStart
     *
     * @throws StepNotificationException
     */
    public function testGetRoleConfigsToNotifyOfStart(): void
    {
        $result = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([$this->firstRoleStartNotificationConfig]),
            new RoleNotificationConfigCollection([]),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );

        static::assertInstanceOf(RoleNotificationConfigCollection::class, $result->getRoleConfigsToNotifyOfStart());
        static::assertCount(1, $result->getRoleConfigsToNotifyOfStart());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::getRoleConfigsToRemindOfOngoing
     *
     * @throws StepNotificationException
     */
    public function testRoleConfigsToRemindOfOngoing(): void
    {
        $result = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([$this->firstRoleStartNotificationConfig]),
            new RoleNotificationConfigCollection([]),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );

        static::assertInstanceOf(RoleNotificationConfigCollection::class, $result->getRoleConfigsToRemindOfOngoing());
        static::assertCount(0, $result->getRoleConfigsToRemindOfOngoing());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::getLocalizedMailBodyForStartNotification
     *
     * @throws StepNotificationException
     */
    public function testGetLocalizedMailBodyForStartNotification(): void
    {
        $result = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([$this->firstRoleStartNotificationConfig]),
            new RoleNotificationConfigCollection([]),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );

        static::assertInstanceOf(LocalizedOptionalString::class, $result->getLocalizedMailBodyForStartNotification());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::getLocalizedMailBodyForOngoingNotification
     *
     * @throws StepNotificationException
     */
    public function testGetLocalizedMailBodyForOngoingNotification(): void
    {
        $result = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([$this->firstRoleStartNotificationConfig]),
            new RoleNotificationConfigCollection([]),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );

        static::assertInstanceOf(LocalizedOptionalString::class, $result->getLocalizedMailBodyForOngoingNotification());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::hasMailBodyForStartNotification
     *
     * @throws StepNotificationException
     */
    public function testHasMailBodyForStartNotification(): void
    {
        $result = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([$this->firstRoleStartNotificationConfig]),
            new RoleNotificationConfigCollection([]),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );

        static::assertFalse($result->hasMailBodyForStartNotification('nl'));
        static::assertFalse($result->hasMailBodyForStartNotification('fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::hasMailBodyForOngoingNotification
     *
     * @throws StepNotificationException
     */
    public function testHasMailBodyForOngoingNotification(): void
    {
        $result = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([$this->firstRoleStartNotificationConfig]),
            new RoleNotificationConfigCollection([]),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );

        static::assertTrue($result->hasMailBodyForOngoingNotification('nl'));
        static::assertTrue($result->hasMailBodyForOngoingNotification('en'));
        static::assertTrue($result->hasMailBodyForOngoingNotification('fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::getMailBodyForStartNotification
     *
     * @throws StepNotificationException
     */
    public function testGetMailBodyForStartNotification(): void
    {
        $mailbody = ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations();

        $result = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([$this->firstRoleStartNotificationConfig]),
            new RoleNotificationConfigCollection([]),
            $mailbody,
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );

        // check known locales
        static::assertSame(SerializedDataFactory::DUTCH_TRANSLATION, $result->getMailBodyForStartNotification('nl'));
        static::assertSame(SerializedDataFactory::ENGLISH_TRANSLATION, $result->getMailBodyForStartNotification('en'));
        // check fallback
        static::assertSame($mailbody->getFallback(), $result->getMailBodyForStartNotification('fake'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::getMailBodyForOngoingNotification
     *
     * @throws StepNotificationException
     */
    public function testGetMailBodyForOngoingNotification(): void
    {
        $mailbody = ValueObjectFactory::createLocalizedOptionalStringHavingDutchTranslation();

        $result = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection([$this->firstRoleStartNotificationConfig]),
            new RoleNotificationConfigCollection([]),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            $mailbody
        );

        // check known locales
        static::assertSame(SerializedDataFactory::DUTCH_TRANSLATION, $result->getMailBodyForOngoingNotification('nl'));
        // check fallback
        static::assertSame($mailbody->getFallback(), $result->getMailBodyForOngoingNotification('en'));
        static::assertSame($mailbody->getFallback(), $result->getMailBodyForOngoingNotification('fake'));
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig::jsonSerialize
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            StepNotificationConfig::KEY_ROLE_CONFIGS_TO_NOTIFY_OF_START => [
                [
                    RoleNotificationConfig::KEY_ROLE => 'ROLE_TEST_1',
                    RoleNotificationConfig::KEY_OFFSET => 1,
                ],
            ],
            StepNotificationConfig::KEY_ROLE_CONFIGS_TO_REMIND_OF_ONGOING => [
                [
                    RoleNotificationConfig::KEY_ROLE => 'ROLE_TEST_1',
                    RoleNotificationConfig::KEY_OFFSET => 2,
                ],
                [
                    RoleNotificationConfig::KEY_ROLE => 'ROLE_TEST_2',
                    RoleNotificationConfig::KEY_OFFSET => 1,
                ],
            ],
            StepNotificationConfig::KEY_MAIL_BODY_FOR_START => ['nl' => 'something dutch'],
            StepNotificationConfig::KEY_MAIL_BODY_FOR_ONGOING => [],
            StepNotificationConfig::KEY_ROLE_MAIL_BODIES_FOR_START => [],
            StepNotificationConfig::KEY_ROLE_MAIL_BODIES_FOR_ONGOING => [],
        ]);

        $result = StepNotificationConfig::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
