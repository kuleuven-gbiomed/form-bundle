<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\WorkFlowStep\Element;

use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use PHPUnit\Framework\TestCase;

/**
 * Class StepCalculatedPeriodTest.
 *
 * @covers  \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod
 */
class StepCalculatedPeriodTest extends TestCase
{
    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::fromCalculatingAmounts
     */
    public function testFromCalculatingAmountsWithValidAmountsShouldReturnInstance(): void
    {
        $result = StepCalculatedPeriod::fromCalculatingAmounts(-10, 30);

        static::assertInstanceOf(StepCalculatedPeriod::class, $result);
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::fromCalculatingAmounts
     */
    public function testFromCalculatingAmountsWithInValidAmountsDifferenceShouldThrowStepException(): void
    {
        $this->expectException(StepException::class);

        StepCalculatedPeriod::fromCalculatingAmounts(-5, -6);
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::fromJsonDecodedArray
     */
    public function testFromSerializedArrayWithValidDataShouldReturnInstance(): void
    {
        $data = [
            StepCalculatedPeriod::KEY_AMOUNT_OF_DAYS_TO_CALCULATE_START_DATE => 0,
            StepCalculatedPeriod::KEY_AMOUNT_OF_DAYS_TO_CALCULATE_END_DATE => 1,
        ];

        $result = StepCalculatedPeriod::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepCalculatedPeriod::class, $result);
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::getAmountOfDaysToCalculateStartDate
     */
    public function testGetAmountOfDaysToCalculateStartDate(): void
    {
        $result = StepCalculatedPeriod::fromCalculatingAmounts(8, 80);

        static::assertSame(8, $result->getAmountOfDaysToCalculateStartDate());
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::getAmountOfDaysToCalculateEndDate
     */
    public function testGetAmountOfDaysToCalculateEndDate(): void
    {
        $result = StepCalculatedPeriod::fromCalculatingAmounts(8, 80);

        static::assertSame(80, $result->getAmountOfDaysToCalculateEndDate());
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::getCalculatedStartDate
     */
    public function testGetCalculatedStartDate(): void
    {
        $calculatedPeriod = StepCalculatedPeriod::fromCalculatingAmounts(1, 10);

        $referenceDate = new \DateTimeImmutable('2019-05-21 14:30');
        $actualStartDate = $calculatedPeriod->getCalculatedStartDate($referenceDate);

        static::assertInstanceOf(\DateTimeImmutable::class, $actualStartDate);

        $expectedStartDate = new \DateTimeImmutable('2019-05-22 14:30');
        static::assertEquals($expectedStartDate, $actualStartDate);
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::getCalculatedEndDate
     */
    public function testGetCalculatedEndDate(): void
    {
        $calculatedPeriod = StepCalculatedPeriod::fromCalculatingAmounts(1, 10);

        $referenceDate = new \DateTimeImmutable('2019-05-21 14:30');
        $actualEndDate = $calculatedPeriod->getCalculatedEndDate($referenceDate);

        static::assertInstanceOf(\DateTimeImmutable::class, $actualEndDate);

        $expectedEndDate = new \DateTimeImmutable('2019-05-31 14:30');
        static::assertEquals($expectedEndDate, $actualEndDate);
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::isStartedByDate
     */
    public function testIsStartedByDate(): void
    {
        $result = StepCalculatedPeriod::fromCalculatingAmounts(1, 10);

        // TEST is started
        static::assertTrue($result->isStartedByDate(
            new \DateTimeImmutable('2019-05-21 15:00'),
            new \DateTimeImmutable('2019-05-20 14:59')
        ));

        static::assertTrue($result->isStartedByDate(
            new \DateTimeImmutable('2019-05-21 15:00'),
            new \DateTimeImmutable('2019-05-20 15:00')
        ));

        static::assertTrue($result->isStartedByDate(
            new \DateTimeImmutable('2019-05-30 15:00'),
            new \DateTimeImmutable('2019-05-20 15:00')
        ));

        static::assertTrue($result->isStartedByDate(
            new \DateTimeImmutable('2019-05-30 15:01'),
            new \DateTimeImmutable('2019-05-20 15:00')
        ));

        // TEST is NOT started

        static::assertFalse($result->isStartedByDate(
            new \DateTimeImmutable('2019-05-21 15:00'),
            new \DateTimeImmutable('2019-05-20 15:01')
        ));
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::isEndedByDate
     */
    public function testIsEndedByDate(): void
    {
        $result = StepCalculatedPeriod::fromCalculatingAmounts(1, 10);

        // TEST is ended
        static::assertTrue($result->isEndedByDate(
            new \DateTimeImmutable('2019-05-21 15:00'),
            new \DateTimeImmutable('2019-05-11 14:59')
        ));

        static::assertTrue($result->isEndedByDate(
            new \DateTimeImmutable('2019-05-21 15:00'),
            new \DateTimeImmutable('2019-05-11 15:00')
        ));

        // TEST is NOT ended

        static::assertFalse($result->isEndedByDate(
            new \DateTimeImmutable('2019-05-10 14:59'),
            new \DateTimeImmutable('2019-05-11 15:00')
        ));

        static::assertFalse($result->isEndedByDate(
            new \DateTimeImmutable('2019-05-10 15:00'),
            new \DateTimeImmutable('2019-05-11 15:00')
        ));

        static::assertFalse($result->isEndedByDate(
            new \DateTimeImmutable('2019-05-21 14:59'),
            new \DateTimeImmutable('2019-05-11 15:00')
        ));
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::isOpenByDate
     */
    public function testIsOpenByDate(): void
    {
        $result = StepCalculatedPeriod::fromCalculatingAmounts(1, 10);

        // TEST is open

        static::assertTrue($result->isOpenByDate(
            new \DateTimeImmutable('2019-05-21 15:00'),
            new \DateTimeImmutable('2019-05-20 15:00')
        ));

        static::assertTrue($result->isOpenByDate(
            new \DateTimeImmutable('2019-05-21 15:01'),
            new \DateTimeImmutable('2019-05-20 15:00')
        ));

        static::assertTrue($result->isOpenByDate(
            new \DateTimeImmutable('2019-05-30 14:59'),
            new \DateTimeImmutable('2019-05-20 15:00')
        ));

        // TEST is NOT open
        static::assertFalse($result->isOpenByDate(
            new \DateTimeImmutable('2019-05-21 14:59'),
            new \DateTimeImmutable('2019-05-20 15:00')
        ));

        static::assertFalse($result->isOpenByDate(
            new \DateTimeImmutable('2019-05-30 15:00'),
            new \DateTimeImmutable('2019-05-20 15:00')
        ));

        static::assertFalse($result->isOpenByDate(
            new \DateTimeImmutable('2019-05-30 15:01'),
            new \DateTimeImmutable('2019-05-20 15:00')
        ));
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::jsonSerialize
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            StepCalculatedPeriod::KEY_AMOUNT_OF_DAYS_TO_CALCULATE_START_DATE => 0,
            StepCalculatedPeriod::KEY_AMOUNT_OF_DAYS_TO_CALCULATE_END_DATE => 1,
        ]);

        $result = StepCalculatedPeriod::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod::overlapsWithCalculatedPeriod
     */
    public function testOverlapsWithCalculatedPeriod(): void
    {
        // without overlap
        $periodOne = StepCalculatedPeriod::fromCalculatingAmounts(0, 1);
        $periodTwo = StepCalculatedPeriod::fromCalculatingAmounts(1, 2);
        static::assertFalse($periodOne->overlapsWithCalculatedPeriod($periodTwo));

        $periodOne = StepCalculatedPeriod::fromCalculatingAmounts(1, 10);
        $periodTwo = StepCalculatedPeriod::fromCalculatingAmounts(10, 20);
        static::assertFalse($periodOne->overlapsWithCalculatedPeriod($periodTwo));

        $periodOne = StepCalculatedPeriod::fromCalculatingAmounts(10, 20);
        $periodTwo = StepCalculatedPeriod::fromCalculatingAmounts(0, 10);
        static::assertFalse($periodOne->overlapsWithCalculatedPeriod($periodTwo));

        $periodOne = StepCalculatedPeriod::fromCalculatingAmounts(-10, 2);
        $periodTwo = StepCalculatedPeriod::fromCalculatingAmounts(100, 500);
        static::assertFalse($periodOne->overlapsWithCalculatedPeriod($periodTwo));

        // with overlap
        $periodOne = StepCalculatedPeriod::fromCalculatingAmounts(1, 10);
        $periodTwo = StepCalculatedPeriod::fromCalculatingAmounts(1, 10);
        static::assertTrue($periodOne->overlapsWithCalculatedPeriod($periodTwo));

        $periodOne = StepCalculatedPeriod::fromCalculatingAmounts(1, 10);
        $periodTwo = StepCalculatedPeriod::fromCalculatingAmounts(1, 20);
        static::assertTrue($periodOne->overlapsWithCalculatedPeriod($periodTwo));

        $periodOne = StepCalculatedPeriod::fromCalculatingAmounts(1, 10);
        $periodTwo = StepCalculatedPeriod::fromCalculatingAmounts(5, 6);
        static::assertTrue($periodOne->overlapsWithCalculatedPeriod($periodTwo));

        $periodOne = StepCalculatedPeriod::fromCalculatingAmounts(1, 10);
        $periodTwo = StepCalculatedPeriod::fromCalculatingAmounts(9, 20);
        static::assertTrue($periodOne->overlapsWithCalculatedPeriod($periodTwo));

        $periodOne = StepCalculatedPeriod::fromCalculatingAmounts(1, 11);
        $periodTwo = StepCalculatedPeriod::fromCalculatingAmounts(10, 20);
        static::assertTrue($periodOne->overlapsWithCalculatedPeriod($periodTwo));

        $periodOne = StepCalculatedPeriod::fromCalculatingAmounts(10, 11);
        $periodTwo = StepCalculatedPeriod::fromCalculatingAmounts(1, 20);
        static::assertTrue($periodOne->overlapsWithCalculatedPeriod($periodTwo));
    }
}
