<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\WorkFlowStep\Element;

use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class RoleNotificationConfig.
 *
 * @covers  \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig
 */
class RoleNotificationConfigTest extends TestCase
{
    /**
     * @return array
     */
    private function getValidSerializedArrayData()
    {
        return [
            RoleNotificationConfig::KEY_ROLE => SerializedDataFactory::FIRST_ROLE,
            RoleNotificationConfig::KEY_OFFSET => 3,
        ];
    }

    /**
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig::fromRoleAndOffset
     */
    public function testFromRoleAndOffsetShouldReturnInstance(): void
    {
        $result = RoleNotificationConfig::fromRoleAndOffset(
            ValueObjectFactory::createFirstRole(),
            3
        );

        static::assertInstanceOf(RoleNotificationConfig::class, $result);
    }

    /**
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig::fromJsonDecodedArray
     */
    public function testFromSerializedArrayWithValidDataShouldReturnInstance(): void
    {
        $result = RoleNotificationConfig::fromJsonDecodedArray(
            $this->getValidSerializedArrayData()
        );

        static::assertInstanceOf(RoleNotificationConfig::class, $result);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig::getRole
     *
     * @throws StepNotificationException
     */
    public function testGetRoleShouldReturnRole(): void
    {
        $role = ValueObjectFactory::createFirstRole();
        $result = RoleNotificationConfig::fromRoleAndOffset($role, 3);

        static::assertInstanceOf(Role::class, $result->getRole());
        static::assertSame($role, $result->getRole());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig::getDaysOffset
     *
     * @throws StepNotificationException
     */
    public function testGetDaysOffset(): void
    {
        $offset = -5;
        $result = RoleNotificationConfig::fromRoleAndOffset(ValueObjectFactory::createFirstRole(), $offset);

        static::assertSame($offset, $result->getDaysOffset());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig::jsonSerialize
     *
     * @throws StepNotificationException
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            RoleNotificationConfig::KEY_ROLE => 'TEST_ROLE',
            RoleNotificationConfig::KEY_OFFSET => 3,
        ]);

        $result = RoleNotificationConfig::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
