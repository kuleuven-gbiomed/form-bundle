<?php

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\WorkFlowStep\Element;

use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use PHPUnit\Framework\TestCase;

/**
 * Class StepFixedPeriod.
 *
 * @covers  \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod
 */
class StepFixedPeriodTest extends TestCase
{
    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod::fromDates
     */
    public function testFromDates(): void
    {
        $result = StepFixedPeriod::fromDates(
            new \DateTimeImmutable('2019-05-11 15:00'),
            new \DateTimeImmutable('2019-05-31 15:00')
        );

        static::assertInstanceOf(StepFixedPeriod::class, $result);
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod::fromDates
     */
    public function testFromDatesWithEndDateBeforeStartDateShouldThrowStepException(): void
    {
        $this->expectException(StepException::class);

        StepFixedPeriod::fromDates(
            new \DateTimeImmutable('2019-05-11 15:00'),
            new \DateTimeImmutable('2019-05-11 14:00')
        );
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod::fromDates
     */
    public function testFromDatesWithEndDateSamesAsStartDateShouldThrowStepException(): void
    {
        $this->expectException(StepException::class);

        StepFixedPeriod::fromDates(
            new \DateTimeImmutable('2019-05-11 15:00'),
            new \DateTimeImmutable('2019-05-11 15:00')
        );
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod::fromJsonDecodedArray
     */
    public function testFromSerializedArrayWithValidDataShouldReturnInstance(): void
    {
        $data = [
            StepFixedPeriod::KEY_FIXED_START_DATE => '2019-05-11 15:00:00',
            StepFixedPeriod::KEY_FIXED_END_DATE => '2019-05-31 15:00:00',
        ];

        $result = StepFixedPeriod::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepFixedPeriod::class, $result);
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod::getFixedStartDate
     */
    public function testGetFixedStartDate(): void
    {
        $period = StepFixedPeriod::fromDates(
            new \DateTimeImmutable('2019-05-11 15:00'),
            new \DateTimeImmutable('2019-05-31 15:00')
        );

        static::assertEquals(
            new \DateTimeImmutable('2019-05-11 15:00'),
            $period->getFixedStartDate()
        );
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod::getFixedEndDate
     */
    public function testGetFixedEndDate(): void
    {
        $period = StepFixedPeriod::fromDates(
            new \DateTimeImmutable('2019-05-11 15:00'),
            new \DateTimeImmutable('2019-05-31 15:00')
        );

        static::assertEquals(
            new \DateTimeImmutable('2019-05-31 15:00'),
            $period->getFixedEndDate()
        );
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod::isStartedByDate
     */
    public function testIsStartedByDate(): void
    {
        $period = StepFixedPeriod::fromDates(
            new \DateTimeImmutable('2019-05-11 15:00'),
            new \DateTimeImmutable('2019-05-31 15:00')
        );

        // TEST is started

        static::assertTrue($period->isStartedByDate(new \DateTimeImmutable('2019-05-31 14:59')));
        static::assertTrue($period->isStartedByDate(new \DateTimeImmutable('2019-05-11 15:00')));
        static::assertTrue($period->isStartedByDate(new \DateTimeImmutable('2019-05-31 15:00')));
        static::assertTrue($period->isStartedByDate(new \DateTimeImmutable('2019-05-31 15:01')));

        // TEST is NOT stared

        static::assertFalse($period->isStartedByDate(new \DateTimeImmutable('2019-05-11 14:59')));
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod::isEndedByDate
     */
    public function testIsEndedByDate(): void
    {
        $period = StepFixedPeriod::fromDates(
            new \DateTimeImmutable('2019-05-11 15:00'),
            new \DateTimeImmutable('2019-05-31 15:00')
        );

        // TEST is ended

        static::assertTrue($period->isEndedByDate(new \DateTimeImmutable('2019-05-31 15:00')));
        static::assertTrue($period->isEndedByDate(new \DateTimeImmutable('2019-05-31 15:01')));

        // TEST is not ended

        static::assertFalse($period->isEndedByDate(new \DateTimeImmutable('2019-05-11 14:59')));
        static::assertFalse($period->isEndedByDate(new \DateTimeImmutable('2019-05-11 15:00')));
        static::assertFalse($period->isEndedByDate(new \DateTimeImmutable('2019-05-11 15:01')));
        static::assertFalse($period->isEndedByDate(new \DateTimeImmutable('2019-05-31 14:59')));
    }

    /**
     * startDate lies 10 days in the past from NOW
     * endDate lies 10 days in the future from NOW.
     *
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod::isOpenByDate
     */
    public function testIsOpenByDate(): void
    {
        $period = StepFixedPeriod::fromDates(
            new \DateTimeImmutable('2019-05-11 15:00'),
            new \DateTimeImmutable('2019-05-31 15:00')
        );

        static::assertFalse($period->isOpenByDate(new \DateTimeImmutable('2019-05-11 14:59')));
        static::assertTrue($period->isOpenByDate(new \DateTimeImmutable('2019-05-11 15:00')));
        static::assertTrue($period->isOpenByDate(new \DateTimeImmutable('2019-05-11 15:01')));
        static::assertTrue($period->isOpenByDate(new \DateTimeImmutable('2019-05-31 14:59')));
        static::assertFalse($period->isOpenByDate(new \DateTimeImmutable('2019-05-31 15:00')));
        static::assertFalse($period->isOpenByDate(new \DateTimeImmutable('2019-05-31 15:01')));
    }

    /**
     * @throws StepException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod::jsonSerialize
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode(
            $data = [
                StepFixedPeriod::KEY_FIXED_START_DATE => '2019-05-11 15:30:00',
                StepFixedPeriod::KEY_FIXED_END_DATE => '2019-05-31 15:30:00',
            ]
        );

        $result = StepFixedPeriod::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod::overlapsWithFixedPeriod
     */
    public function testHasOverlapWithPeriod(): void
    {
        // without overlap
        $periodOne = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-20'), new \DateTimeImmutable('2019-05-21'));
        $periodTwo = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-21'), new \DateTimeImmutable('2019-05-22'));
        static::assertFalse($periodOne->overlapsWithFixedPeriod($periodTwo));

        $periodOne = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-30'), new \DateTimeImmutable('2019-06-09'));
        $periodTwo = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-20'), new \DateTimeImmutable('2019-05-30'));
        static::assertFalse($periodOne->overlapsWithFixedPeriod($periodTwo));

        $periodOne = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-10'), new \DateTimeImmutable('2019-06-22'));
        $periodTwo = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-08-10'), new \DateTimeImmutable('2019-11-10'));
        static::assertFalse($periodOne->overlapsWithFixedPeriod($periodTwo));

        // with overlap
        $periodOne = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-21'), new \DateTimeImmutable('2019-05-30'));
        $periodTwo = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-21'), new \DateTimeImmutable('2019-05-30'));
        static::assertTrue($periodOne->overlapsWithFixedPeriod($periodTwo));

        $periodOne = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-21'), new \DateTimeImmutable('2019-05-30'));
        $periodTwo = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-21'), new \DateTimeImmutable('2019-06-09'));
        static::assertTrue($periodOne->overlapsWithFixedPeriod($periodTwo));

        $periodOne = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-21'), new \DateTimeImmutable('2019-05-30'));
        $periodTwo = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-25'), new \DateTimeImmutable('2019-05-26'));
        static::assertTrue($periodOne->overlapsWithFixedPeriod($periodTwo));

        $periodOne = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-21'), new \DateTimeImmutable('2019-05-30'));
        $periodTwo = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-29'), new \DateTimeImmutable('2019-06-09'));
        static::assertTrue($periodOne->overlapsWithFixedPeriod($periodTwo));

        $periodOne = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-30'), new \DateTimeImmutable('2019-05-31'));
        $periodTwo = StepFixedPeriod::fromDates(new \DateTimeImmutable('2019-05-21'), new \DateTimeImmutable('2019-06-09'));
        static::assertTrue($periodOne->overlapsWithFixedPeriod($periodTwo));
    }
}
