<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\WorkFlowStep\Element;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Utility\Collection\RoleNotificationConfigCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class StepSubmitSuccessUserInteractionTest.
 *
 * @covers  \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction
 */
class StepSubmitSuccessUserInteractionTest extends TestCase
{
    /** @var StepNotificationConfig */
    private $stepNotificationConfig;

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->stepNotificationConfig = StepNotificationConfig::fromRoleNotificationsAndMailBodies(
            new RoleNotificationConfigCollection(
                [
                    RoleNotificationConfig::fromRoleAndOffset(ValueObjectFactory::createFirstRole(), 5),
                ]
            ),
            new RoleNotificationConfigCollection([]),
            ValueObjectFactory::createLocalizedOptionalStringHavingNoTranslations(),
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
        );
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction::fromNotificationConfigAndMessages()
     */
    public function testFromNotificationConfigAndMessages(): void
    {
        $result = StepSubmitSuccessUserInteraction::fromNotificationConfigAndMessages(
            $this->stepNotificationConfig,
            ValueObjectFactory::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            ValueObjectFactory::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish(),
            false
        );

        static::assertInstanceOf(StepSubmitSuccessUserInteraction::class, $result);
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithFullKeyedDataShouldReturnInstance(): void
    {
        $data = [
            StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
            StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => SerializedDataFactory::createTranslationsArray(),
            StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => SerializedDataFactory::createRoleTranslationsCollectionArray(),
            StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => false,
        ];

        $result = StepSubmitSuccessUserInteraction::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepSubmitSuccessUserInteraction::class, $result);
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithMinimalRequiredKeyedDataShouldReturnInstance(): void
    {
        $data = [
            StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => [],
            StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => [],
            StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => [],
            StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => false,
        ];

        $result = StepSubmitSuccessUserInteraction::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepSubmitSuccessUserInteraction::class, $result);
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithNoKeyedDataShouldReturnInstance(): void
    {
        $data = [];

        $result = StepSubmitSuccessUserInteraction::fromJsonDecodedArray($data);

        static::assertInstanceOf(StepSubmitSuccessUserInteraction::class, $result);
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction::getNotificationConfig()
     */
    public function testGetNotificationConfig(): void
    {
        $fullData = [
            StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
            StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => SerializedDataFactory::createTranslationsArray(),
            StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => SerializedDataFactory::createRoleTranslationsCollectionArray(),
            StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => false,
        ];

        static::assertInstanceOf(
            StepNotificationConfig::class,
            StepSubmitSuccessUserInteraction::fromJsonDecodedArray($fullData)->getNotificationConfig()
        );

        static::assertInstanceOf(
            StepNotificationConfig::class,
            StepSubmitSuccessUserInteraction::fromJsonDecodedArray([])->getNotificationConfig()
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction::getSubmitSuccessMessage()
     */
    public function testGetSubmitSuccessMessage(): void
    {
        $fullData = [
            StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
            StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => SerializedDataFactory::createTranslationsArray(),
            StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => SerializedDataFactory::createRoleTranslationsCollectionArray(),
            StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => false,
        ];

        static::assertInstanceOf(
            LocalizedOptionalString::class,
            StepSubmitSuccessUserInteraction::fromJsonDecodedArray($fullData)->getSubmitSuccessMessage()
        );

        static::assertInstanceOf(
            LocalizedOptionalString::class,
            StepSubmitSuccessUserInteraction::fromJsonDecodedArray([])->getSubmitSuccessMessage()
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction::getRoleSubmitSuccessMessages()
     */
    public function testGetRoleSubmitSuccessMessages(): void
    {
        $fullData = [
            StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
            StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => SerializedDataFactory::createTranslationsArray(),
            StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => SerializedDataFactory::createRoleTranslationsCollectionArray(),
            StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => false,
        ];

        static::assertInstanceOf(
            RoleLocalizedOptionalStringCollection::class,
            StepSubmitSuccessUserInteraction::fromJsonDecodedArray($fullData)->getRoleSubmitSuccessMessages()
        );

        static::assertInstanceOf(
            RoleLocalizedOptionalStringCollection::class,
            StepSubmitSuccessUserInteraction::fromJsonDecodedArray([])->getRoleSubmitSuccessMessages()
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction::shouldRedirectAwayOnSubmitSuccess()
     */
    public function testShouldRedirectAwayOnSubmitSuccess(): void
    {
        $fullData = [
            StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
            StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => SerializedDataFactory::createTranslationsArray(),
            StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => SerializedDataFactory::createRoleTranslationsCollectionArray(),
            StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => true,
        ];

        static::assertTrue(
            StepSubmitSuccessUserInteraction::fromJsonDecodedArray($fullData)->shouldRedirectAwayOnSubmitSuccess()
        );

        static::assertFalse(
            StepSubmitSuccessUserInteraction::fromJsonDecodedArray([
                StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => false,
            ])->shouldRedirectAwayOnSubmitSuccess()
        );

        static::assertFalse(
            StepSubmitSuccessUserInteraction::fromJsonDecodedArray([])->shouldRedirectAwayOnSubmitSuccess()
        );
    }

    /**
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
            StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => SerializedDataFactory::createTranslationsArray(),
            StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => SerializedDataFactory::createRoleTranslationsCollectionArray(),
            StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => true,
        ]);

        $result = StepSubmitSuccessUserInteraction::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
