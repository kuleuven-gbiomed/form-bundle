<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\WorkFlowStep;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractStepTest.
 *
 * NOTE: this test uses a dummy class @see DummyAbstractStep
 * to test @see AbstractStep
 *
 * @covers  \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep
 */
class AbstractStepTest extends TestCase
{
    /** @var string */
    private $stepUid = 'test_uid';
    /** @var StepFixedPeriod */
    private $stepFixedPeriod;
    /** @var StepCalculatedPeriod */
    private $stepCalculatedPeriod;

    /**
     * @throws StepException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->stepCalculatedPeriod = ValueObjectFactory::createStepCalculatePeriod(-10, 10);
        $this->stepFixedPeriod = ValueObjectFactory::createStepFixedPeriod(new \DateTimeImmutable('2019-05-01 10:00'), 10, 10);
    }

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::__construct
     */
    public function testConstructorWithCalculatedPeriodShouldReturnAbstractStep(): void
    {
        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            $this->stepCalculatedPeriod,
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertInstanceOf(AbstractStep::class, $result);
    }

    /**
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::__construct
     */
    public function testConstructorWithFixedPeriodShouldReturnAbstractStep(): void
    {
        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            $this->stepFixedPeriod,
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertInstanceOf(AbstractStep::class, $result);
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::getUid
     */
    public function testGetUid(): void
    {
        $uid = 'some_uid';
        $result = DummyAbstractStep::fromUidAndDependencies(
            $uid,
            $this->stepCalculatedPeriod,
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertSame($uid, $result->getUid());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::allowsToStartPrematurelyIfPreviousStepIsFulFilled
     */
    public function testAllowsToStartPrematurelyIfPreviousStepIsFulFilled(): void
    {
        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            true,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($result->allowsToStartPrematurelyIfPreviousStepIsFulFilled());

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertFalse($result->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::getInfo
     */
    public function testGetInfo(): void
    {
        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertInstanceOf(LocalizedInfoWithRequiredLabel::class, $result->getInfo());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::getSubmitSuccessUserInteraction
     */
    public function testGetSubmitSuccessUserInteraction(): void
    {
        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            ValueObjectFactory::createFullyLoadedRoleCollection(),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertInstanceOf(StepSubmitSuccessUserInteraction::class, $result->getSubmitSuccessUserInteraction());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::getRolesLimitingUserToOneSubmitPerRole
     */
    public function testGetRolesLimitingUserToOneSubmitPerRole(): void
    {
        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertInstanceOf(RoleCollection::class, $result->getRolesLimitingUserToOneSubmitPerRole());
        static::assertCount(2, $result->getRolesLimitingUserToOneSubmitPerRole());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::limitsEachUniqueUserToOnlyOneSubmitUnderRole
     */
    public function testLimitsEachUniqueUserToOnlyOneSubmitUnderRole(): void
    {
        $firstRole = Role::fromName(SerializedDataFactory::FIRST_ROLE);
        $secondRole = Role::fromName(SerializedDataFactory::SECOND_ROLE);

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            new RoleCollection([$firstRole, $secondRole]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($result->limitsEachUniqueUserToOnlyOneSubmitUnderRole($firstRole));
        static::assertTrue($result->limitsEachUniqueUserToOnlyOneSubmitUnderRole($secondRole));
        static::assertFalse($result->limitsEachUniqueUserToOnlyOneSubmitUnderRole(Role::fromName('UNASSIGNED_ROLE')));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::shouldHideReadOnlyNodesOnLanding
     */
    public function testShouldHideReadOnlyNodesOnLanding(): void
    {
        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($result->shouldHideReadOnlyNodesOnLanding());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::allowsToStartPrematurelyIfPreviousStepIsFulFilled
     */
    public function testAllowsToStartPrematurelyIfPreviousStepIsFulFilledShouldReturnFalse(): void
    {
        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertFalse($result->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::shouldNotifyRoleOfStart
     */
    public function testShouldNotifyRoleOfStart(): void
    {
        $userInteraction = ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction();

        $existingRoleConfig = $userInteraction->getNotificationConfig()->getRoleConfigsToNotifyOfStart()->first();
        $existingRole = $existingRoleConfig->getRole();
        $unexistingRole = Role::fromName('COMPLETELY_FAKE_ROLE');

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction,
            true
        );

        static::assertTrue($result->shouldNotifyRoleOfStart($existingRole));
        static::assertFalse($result->shouldNotifyRoleOfStart($unexistingRole));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::shouldRemindRoleOfOngoing
     */
    public function testShouldRemindRoleOfOngoing(): void
    {
        $userInteraction = ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction();

        $existingRoleConfig = $userInteraction->getNotificationConfig()->getRoleConfigsToRemindOfOngoing()->first();
        $existingRole = $existingRoleConfig->getRole();
        $unexistingRole = Role::fromName('COMPLETELY_FAKE_ROLE');

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction,
            true
        );

        static::assertTrue($result->shouldRemindRoleOfOngoing($existingRole));
        static::assertFalse($result->shouldRemindRoleOfOngoing($unexistingRole));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::shouldRedirectAwayOnSubmitSuccess
     */
    public function testShouldRedirectAwayOnSubmitSuccess(): void
    {
        $userInteraction = StepSubmitSuccessUserInteraction::fromJsonDecodedArray(
            [
                StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
                StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => SerializedDataFactory::createTranslationsArray(),
                StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => SerializedDataFactory::createRoleTranslationsCollectionArray(),
                StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => true,
            ]
        );

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction,
            true
        );

        static::assertTrue($result->shouldRedirectAwayOnSubmitSuccess());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::getLabel
     */
    public function testGetLabel(): void
    {
        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertSame(SerializedDataFactory::DUTCH_TRANSLATION, $result->getLabel('nl'));
        static::assertNotEmpty(SerializedDataFactory::DUTCH_TRANSLATION, $result->getLabel('locale_to_trigger_fallback'));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::hasGeneralSubmitSuccessMessage
     */
    public function testHasGeneralSubmitSuccessMessage(): void
    {
        $userInteraction = StepSubmitSuccessUserInteraction::fromJsonDecodedArray(
            [
                StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
                StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => ['nl' => 'some dutch', 'en' => '', 'fr' => null],
                StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => SerializedDataFactory::createRoleTranslationsCollectionArray(),
                StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => true,
            ]
        );

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction,
            true
        );

        static::assertTrue($result->hasGeneralSubmitSuccessMessage('nl'));
        // test fallback on empty/null values
        static::assertTrue($result->hasGeneralSubmitSuccessMessage('en'));
        static::assertTrue($result->hasGeneralSubmitSuccessMessage('fr'));
        static::assertTrue($result->hasGeneralSubmitSuccessMessage('fake_locale'));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::getGeneralSubmitSuccessMessage
     */
    public function testGetGeneralSubmitSuccessMessageShouldReturnDutchString(): void
    {
        $dutchString = 'some dutch';

        $userInteraction = StepSubmitSuccessUserInteraction::fromJsonDecodedArray(
            [
                StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
                StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => ['nl' => 'some dutch', 'en' => '', 'fr' => null],
                StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => SerializedDataFactory::createRoleTranslationsCollectionArray(),
                StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => true,
            ]
        );

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction,
            true
        );

        static::assertSame($dutchString, $result->getGeneralSubmitSuccessMessage('nl'));
        // test fallback
        static::assertSame($dutchString, $result->getGeneralSubmitSuccessMessage('en'));
        static::assertSame($dutchString, $result->getGeneralSubmitSuccessMessage('fr'));
        static::assertSame($dutchString, $result->getGeneralSubmitSuccessMessage('fake_locale'));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::getGeneralSubmitSuccessMessage
     */
    public function testGetGeneralSubmitSuccessMessageWhenNoTranslationsExistShouldThrowLocalizedStringExeption(): void
    {
        $userInteraction = StepSubmitSuccessUserInteraction::fromJsonDecodedArray(
            [
                StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
                StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => [],
                StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => SerializedDataFactory::createRoleTranslationsCollectionArray(),
                StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => true,
            ]
        );

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction,
            true
        );

        $this->expectException(LocalizedStringException::class);

        $result->getGeneralSubmitSuccessMessage('nl');
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::hasRoleSubmitSuccessMessage
     */
    public function testHasRoleSubmitSuccessMessage(): void
    {
        $roleMessages = [
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_TRANSLATIONS_FOR_NL_AND_EN', true, true),
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_TRANSLATIONS_FOR_NL', true, false),
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_NO_TRANSLATIONS', false, false),
        ];

        $userInteraction = StepSubmitSuccessUserInteraction::fromJsonDecodedArray(
            [
                StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
                StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => SerializedDataFactory::createTranslationsArray(),
                StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => $roleMessages,
                StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => true,
            ]
        );

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction,
            true
        );

        // test role which has non-empty translations for nl & en locales
        static::assertTrue($result->hasRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL_AND_EN'), 'nl'));
        static::assertTrue($result->hasRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL_AND_EN'), 'en'));
        // test fallback for role which has non-empty translations for nl & en locales
        static::assertTrue($result->hasRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL_AND_EN'), 'fake_locale'));

        // test role which has non-empty translations for nl locale only
        static::assertTrue($result->hasRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL'), 'nl'));
        // test fallback for role which has non-empty translations for nl locale only
        static::assertTrue($result->hasRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL'), 'en'));
        static::assertTrue($result->hasRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL'), 'fake_locale'));

        // test role which has no non-empty translations for any locale
        static::assertFalse($result->hasRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_NO_TRANSLATIONS'), 'nl'));
        static::assertFalse($result->hasRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_NO_TRANSLATIONS'), 'en'));
        static::assertFalse($result->hasRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_NO_TRANSLATIONS'), 'fake_locale'));

        // test unexisting role
        static::assertFalse($result->hasRoleSubmitSuccessMessage(Role::fromName('SOME_FAKE_ROLE'), 'nl'));
        static::assertFalse($result->hasRoleSubmitSuccessMessage(Role::fromName('SOME_FAKE_ROLE'), 'en'));
        static::assertFalse($result->hasRoleSubmitSuccessMessage(Role::fromName('SOME_FAKE_ROLE'), 'fake_locale'));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::getRoleSubmitSuccessMessage
     */
    public function testGetRoleSubmitSuccessMessageShouldReturnCorrectString(): void
    {
        $roleMessages = [
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_TRANSLATIONS_FOR_NL_AND_EN', true, true),
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_TRANSLATIONS_FOR_NL', true, false),
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_NO_TRANSLATIONS', false, false),
        ];

        $userInteraction = StepSubmitSuccessUserInteraction::fromJsonDecodedArray(
            [
                StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
                StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => SerializedDataFactory::createTranslationsArray(),
                StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => $roleMessages,
                StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => true,
            ]
        );

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction,
            true
        );

        // test role which has non-empty translations for nl & en locales
        static::assertSame(SerializedDataFactory::DUTCH_TRANSLATION, $result->getRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL_AND_EN'), 'nl'));
        static::assertSame(SerializedDataFactory::ENGLISH_TRANSLATION, $result->getRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL_AND_EN'), 'en'));
        // test fallback for role which has non-empty translations for nl & en locales
        static::assertNotEmpty($result->getRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL_AND_EN'), 'fake_locale'));

        // test role which has non-empty translations for nl locale only
        static::assertSame(SerializedDataFactory::DUTCH_TRANSLATION, $result->getRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL'), 'nl'));
        // test fallback for role which has non-empty translations for nl locale only
        static::assertSame(SerializedDataFactory::DUTCH_TRANSLATION, $result->getRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL'), 'en'));
        static::assertSame(SerializedDataFactory::DUTCH_TRANSLATION, $result->getRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_TRANSLATIONS_FOR_NL'), 'fake_locale'));
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::getRoleSubmitSuccessMessage
     */
    public function testGetRoleSubmitSuccessMessageIfNoTranslationsForRoleShouldThrowLocalizedStringException(): void
    {
        $roleMessages = [
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_TRANSLATIONS_FOR_NL_AND_EN', true, true),
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_TRANSLATIONS_FOR_NL', true, false),
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_NO_TRANSLATIONS', false, false),
        ];

        $userInteraction = StepSubmitSuccessUserInteraction::fromJsonDecodedArray(
            [
                StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
                StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => SerializedDataFactory::createTranslationsArray(),
                StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => $roleMessages,
                StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => true,
            ]
        );

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction,
            true
        );

        $this->expectException(LocalizedStringException::class);

        $result->getRoleSubmitSuccessMessage(Role::fromName('ROLE_WITH_NO_TRANSLATIONS'), 'nl');
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::getRoleSubmitSuccessMessage
     */
    public function testGetRoleSubmitSuccessMessageIfNoRoleShouldThrowBadMethodCallExceptionException(): void
    {
        $roleMessages = [
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_TRANSLATIONS_FOR_NL_AND_EN', true, true),
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_TRANSLATIONS_FOR_NL', true, false),
            SerializedDataFactory::createRoleTranslationsArray('ROLE_WITH_NO_TRANSLATIONS', false, false),
        ];

        $userInteraction = StepSubmitSuccessUserInteraction::fromJsonDecodedArray(
            [
                StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => SerializedDataFactory::createStepNotificationConfigArray(),
                StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => SerializedDataFactory::createTranslationsArray(),
                StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => $roleMessages,
                StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => true,
            ]
        );

        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction,
            true
        );

        $this->expectException(\BadMethodCallException::class);

        $result->getRoleSubmitSuccessMessage(Role::fromName('SOME_FAKE_ROLE'), 'nl');
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::withPreviousStepFulFilledStartOption
     */
    public function testWithPreviousStepFulFilledStartOption(): void
    {
        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            false,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertFalse($result->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
        static::assertTrue($result->withPreviousStepFulFilledStartOption()->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
    }

    /**
     * @throws StepException
     * @throws LocalizedStringException
     * @throws StepNotificationException
     *
     * @covers \KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::withoutPreviousStepFulFilledStartOption
     */
    public function testWithoutPreviousStepFulFilledStartOption(): void
    {
        $result = DummyAbstractStep::fromUidAndDependencies(
            $this->stepUid,
            StepCalculatedPeriod::fromCalculatingAmounts(-10, +10),
            true,
            RoleCollection::fromJsonDecodedArray([SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE]),
            ValueObjectFactory::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            ValueObjectFactory::createFullLoadedStepSubmitSuccessUserInteraction(),
            true
        );

        static::assertTrue($result->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
        static::assertFalse($result->withoutPreviousStepFulFilledStartOption()->allowsToStartPrematurelyIfPreviousStepIsFulFilled());
    }
}
