<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Template\Element\WorkFlowStep;

use KUL\FormBundle\Domain\Template\Contract\StepPeriodInterface;

class DummyStepPeriod implements StepPeriodInterface
{
}
