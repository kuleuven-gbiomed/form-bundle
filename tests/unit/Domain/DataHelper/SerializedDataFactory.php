<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\DataHelper;

use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\RoleNotificationConfig;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepNotificationConfig;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString;

/**
 * Class SerializedDataFactory.
 *
 * helps to construct valid test data for (de)serializing purposes
 */
class SerializedDataFactory
{
    /** @var string */
    public const DUTCH_TRANSLATION = 'dutch translation';
    /** @var string */
    public const ENGLISH_TRANSLATION = 'english translation';
    /** @var string */
    public const FIRST_ROLE = 'ROLE_FIRST';
    /** @var string */
    public const SECOND_ROLE = 'ROLE_SECOND';

    public static function createRolesArray(bool $addFirstRole = true, bool $addSecondRole = true): array
    {
        $data = [];

        if ($addFirstRole) {
            $data[] = self::FIRST_ROLE;
        }

        if ($addSecondRole) {
            $data[] = self::SECOND_ROLE;
        }

        return $data;
    }

    public static function createTranslationsArray(bool $addNl = true, bool $addEn = true): array
    {
        $data = [];

        if ($addNl) {
            $data['nl'] = self::DUTCH_TRANSLATION;
        }

        if ($addEn) {
            $data['en'] = self::ENGLISH_TRANSLATION;
        }

        return $data;
    }

    public static function createRoleTranslationsArray(string $role = self::FIRST_ROLE, bool $addNl = true, bool $addEn = true): array
    {
        return [
            RoleLocalizedOptionalString::KEY_ROLE => $role,
            RoleLocalizedOptionalString::KEY_TRANSLATIONS => self::createTranslationsArray($addNl, $addEn),
        ];
    }

    public static function createRoleTranslationsCollectionArray(
        array $roles = [self::FIRST_ROLE => ['nl', 'en'], self::SECOND_ROLE => ['nl', 'en']],
    ): array {
        $data = [];

        foreach ($roles as $role => $locales) {
            $data[] = self::createRoleTranslationsArray($role, in_array('nl', $locales, true), in_array('en', $locales, true));
        }

        return $data;
    }

    public static function createInfoArray(
        array $label = ['nl', 'en'],
        array $description = ['nl', 'en'],
        array $roleDescriptions = [self::FIRST_ROLE => ['nl', 'en'], self::SECOND_ROLE => ['nl', 'en']],
    ): array {
        return [
            LocalizedInfoWithRequiredLabel::KEY_LABEL => self::createTranslationsArray(in_array('nl', $label, true), in_array('en', $label, true)),
            LocalizedInfoWithRequiredLabel::KEY_DESCRIPTION => self::createTranslationsArray(in_array('nl', $description, true), in_array('en', $description, true)),
            LocalizedInfoWithRequiredLabel::KEY_ROLE_DESCRIPTIONS => self::createRoleTranslationsCollectionArray($roleDescriptions),
        ];
    }

    /**
     * @param int $offset
     */
    public static function createRoleNotificationConfigArray(string $role = self::FIRST_ROLE, $offset = 0): array
    {
        return [
            RoleNotificationConfig::KEY_ROLE => $role,
            RoleNotificationConfig::KEY_OFFSET => $offset,
        ];
    }

    public static function createRoleNotificationConfigCollectionArray(
        array $roles = [self::FIRST_ROLE => 0, self::SECOND_ROLE => 0],
    ): array {
        $data = [];

        foreach ($roles as $role => $offset) {
            $data[] = self::createRoleNotificationConfigArray($role, $offset);
        }

        return $data;
    }

    public static function createRoleTranslationArray(
        array $roles = [self::FIRST_ROLE => ['nl' => self::DUTCH_TRANSLATION, 'en' => self::ENGLISH_TRANSLATION], self::SECOND_ROLE => ['nl' => self::DUTCH_TRANSLATION, 'en' => self::ENGLISH_TRANSLATION]],
    ): array {
        $data = [];

        foreach ($roles as $role => $localeTranslations) {
            $data[] = [
                RoleLocalizedOptionalString::KEY_ROLE => $role,
                RoleLocalizedOptionalString::KEY_TRANSLATIONS => $localeTranslations,
            ];
        }

        return $data;
    }

    public static function createStepNotificationConfigArray(
        array $startRoles = [self::FIRST_ROLE => 0, self::SECOND_ROLE => 0],
        array $ongoingRoles = [self::FIRST_ROLE => 1, self::SECOND_ROLE => 1],
        array $startMailLocales = ['nl', 'en'],
        array $ongoingMailLocales = ['nl', 'en'],
    ): array {
        return [
            StepNotificationConfig::KEY_ROLE_CONFIGS_TO_NOTIFY_OF_START => self::createRoleNotificationConfigCollectionArray($startRoles),
            StepNotificationConfig::KEY_ROLE_CONFIGS_TO_REMIND_OF_ONGOING => self::createRoleNotificationConfigCollectionArray($ongoingRoles),
            StepNotificationConfig::KEY_MAIL_BODY_FOR_START => self::createTranslationsArray(
                in_array('nl', $startMailLocales, true),
                in_array('en', $startMailLocales, true)
            ),
            StepNotificationConfig::KEY_MAIL_BODY_FOR_ONGOING => self::createTranslationsArray(
                in_array('nl', $ongoingMailLocales, true),
                in_array('en', $ongoingMailLocales, true)
            ),
            StepNotificationConfig::KEY_ROLE_MAIL_BODIES_FOR_START => self::createRoleTranslationArray(),
            StepNotificationConfig::KEY_ROLE_MAIL_BODIES_FOR_ONGOING => self::createRoleTranslationArray(),
        ];
    }

    /**
     * @param int $startAmount
     * @param int $endAmount
     *
     * @return array
     */
    public static function createStepCalculatedPeriodArray($startAmount = 0, $endAmount = 1)
    {
        return [
            StepCalculatedPeriod::KEY_AMOUNT_OF_DAYS_TO_CALCULATE_START_DATE => $startAmount,
            StepCalculatedPeriod::KEY_AMOUNT_OF_DAYS_TO_CALCULATE_END_DATE => $endAmount,
        ];
    }

    public static function createStepFixedPeriodArray(
        \DateTimeImmutable $referenceDate,
        int $daysBefore = 10,
        int $daysAfter = 10,
    ): array {
        $startDate = $referenceDate->modify('-'.abs($daysBefore).' days');
        $endDate = $referenceDate->modify('+'.abs($daysAfter).' days');

        return [
            StepFixedPeriod::KEY_FIXED_START_DATE => $startDate->format(StepFixedPeriod::DATE_FORMAT),
            StepFixedPeriod::KEY_FIXED_END_DATE => $endDate->format(StepFixedPeriod::DATE_FORMAT),
        ];
    }

    public static function createStepSubmitSuccessUserInteractionArray(
        array $startRoles = [self::FIRST_ROLE => 0, self::SECOND_ROLE => 0],
        array $ongoingRoles = [self::FIRST_ROLE => 1, self::SECOND_ROLE => 1],
        array $startMailLocales = ['nl', 'en'],
        array $ongoingMailLocales = ['nl', 'en'],
        array $generalMessageLocales = ['nl', 'en'],
        array $roleMessages = [self::FIRST_ROLE => ['nl', 'en'], self::SECOND_ROLE => ['nl', 'en']],
        bool $redirectAway = false,
    ): array {
        return [
            StepSubmitSuccessUserInteraction::KEY_NOTIFICATION_CONFIG => self::createStepNotificationConfigArray($startRoles, $ongoingRoles, $startMailLocales, $ongoingMailLocales),
            StepSubmitSuccessUserInteraction::KEY_GENERAL_SUBMIT_SUCCESS_MESSAGE => self::createTranslationsArray(in_array('nl', $generalMessageLocales, true), in_array('en', $generalMessageLocales, true)),
            StepSubmitSuccessUserInteraction::KEY_ROLE_SUBMIT_SUCCESS_MESSAGES => self::createRoleTranslationsCollectionArray($roleMessages),
            StepSubmitSuccessUserInteraction::KEY_REDIRECT_AWAY_ON_SUBMIT_SUCCESS => $redirectAway,
        ];
    }

    public static function createStepWithCalculatedDateStartArray(
        string $uid,
        int $periodStartAmount = -10,
        int $periodEndAmount = 10,
        array $rolesWithOneSubmit = [self::FIRST_ROLE, self::SECOND_ROLE],
        ?array $infoArray = null,
        ?array $userInteractionArray = null,
        bool $hideReadOnly = false,
    ): array {
        $infoData = $infoArray ?? self::createInfoArray();
        $userInteractionData = $userInteractionArray ?? self::createStepSubmitSuccessUserInteractionArray();

        return [
            StepWithCalculatedDateStart::KEY_TYPE => StepWithCalculatedDateStart::getType(),
            StepWithCalculatedDateStart::KEY_UID => $uid,
            StepWithCalculatedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => false,
            StepWithCalculatedDateStart::KEY_PERIOD => self::createStepCalculatedPeriodArray($periodStartAmount, $periodEndAmount),
            StepWithCalculatedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => $rolesWithOneSubmit,
            StepWithCalculatedDateStart::KEY_INFO => $infoData,
            StepWithCalculatedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => $userInteractionData,
            StepWithCalculatedDateStart::KEY_HIDE_READ_ONLY_NODES_ON_LANDING => $hideReadOnly,
        ];
    }

    public static function createStepWithCalculatedDateOrPreviousFulFilledStartArray(
        string $uid,
        int $periodStartAmount = -10,
        int $periodEndAmount = 10,
        array $rolesWithOneSubmit = [self::FIRST_ROLE, self::SECOND_ROLE],
        ?array $infoArray = null,
        ?array $userInteractionArray = null,
        bool $hideReadOnly = false,
    ): array {
        $infoData = $infoArray ?? self::createInfoArray();
        $userInteractionData = $userInteractionArray ?? self::createStepSubmitSuccessUserInteractionArray();

        return [
            StepWithCalculatedDateStart::KEY_TYPE => StepWithCalculatedDateStart::getType(),
            StepWithCalculatedDateStart::KEY_UID => $uid,
            StepWithCalculatedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => true,
            StepWithCalculatedDateStart::KEY_PERIOD => self::createStepCalculatedPeriodArray($periodStartAmount, $periodEndAmount),
            StepWithCalculatedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => $rolesWithOneSubmit,
            StepWithCalculatedDateStart::KEY_INFO => $infoData,
            StepWithCalculatedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => $userInteractionData,
            StepWithCalculatedDateStart::KEY_HIDE_READ_ONLY_NODES_ON_LANDING => $hideReadOnly,
        ];
    }

    /**
     * @param string[] $rolesWithOneSubmit
     *
     * @throws \Exception
     */
    public static function createStepWithFixedDateStartArray(
        string $uid,
        \DateTimeImmutable $referenceDate,
        int $periodDaysBefore = 10,
        int $periodDaysAfter = 10,
        array $rolesWithOneSubmit = [self::FIRST_ROLE, self::SECOND_ROLE],
        ?array $infoArray = null,
        ?array $userInteractionArray = null,
        bool $hideReadOnly = false,
    ): array {
        $infoData = $infoArray ?? self::createInfoArray();
        $userInteractionData = $userInteractionArray ?? self::createStepSubmitSuccessUserInteractionArray();

        return [
            StepWithFixedDateStart::KEY_TYPE => StepWithFixedDateStart::getType(),
            StepWithFixedDateStart::KEY_UID => $uid,
            StepWithFixedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => false,
            StepWithFixedDateStart::KEY_PERIOD => self::createStepFixedPeriodArray($referenceDate, $periodDaysBefore, $periodDaysAfter),
            StepWithFixedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => $rolesWithOneSubmit,
            StepWithFixedDateStart::KEY_INFO => $infoData,
            StepWithFixedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => $userInteractionData,
            StepWithFixedDateStart::KEY_HIDE_READ_ONLY_NODES_ON_LANDING => $hideReadOnly,
        ];
    }

    /**
     * @param string[] $rolesWithOneSubmit
     *
     * @throws \Exception
     */
    public static function createStepWithFixedDateOrPreviousFulFilledStartArray(
        string $uid,
        \DateTimeImmutable $referenceDate,
        int $periodDaysBefore = 10,
        int $periodDaysAfter = 10,
        array $rolesWithOneSubmit = [self::FIRST_ROLE, self::SECOND_ROLE],
        ?array $infoArray = null,
        ?array $userInteractionArray = null,
        bool $hideReadOnly = false,
    ): array {
        $infoData = $infoArray ?? self::createInfoArray();
        $userInteractionData = $userInteractionArray ?? self::createStepSubmitSuccessUserInteractionArray();

        return [
            StepWithFixedDateStart::KEY_TYPE => StepWithFixedDateStart::getType(),
            StepWithFixedDateStart::KEY_UID => $uid,
            StepWithFixedDateStart::KEY_ALLOWS_START_IF_PREVIOUS_STEP_IS_FULFILLED => true,
            StepWithFixedDateStart::KEY_PERIOD => self::createStepFixedPeriodArray($referenceDate, $periodDaysBefore, $periodDaysAfter),
            StepWithFixedDateStart::KEY_ROLES_WITH_ONE_SUBMIT => $rolesWithOneSubmit,
            StepWithFixedDateStart::KEY_INFO => $infoData,
            StepWithFixedDateStart::KEY_SUBMIT_SUCCESS_USER_INTERACTION => $userInteractionData,
            StepWithFixedDateStart::KEY_HIDE_READ_ONLY_NODES_ON_LANDING => $hideReadOnly,
        ];
    }
}
