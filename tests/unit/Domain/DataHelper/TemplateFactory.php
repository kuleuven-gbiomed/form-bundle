<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\DataHelper;

use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Category\CategoryNode;
use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Entity\Template;

class TemplateFactory
{
    /**
     * Create a published template for a given id.
     */
    public static function create(string $templateId): Template
    {
        $steps = ValueObjectFactory::createValidStepWithCalculatedDateStartCollection();
        $flowPermission = FlowPermission::createEmpty();
        foreach ($steps->getStepUids() as $stepId) {
            $flowPermission = $flowPermission->withFullWritePermissionsForRoleInStepUid('ROLE_ADMIN', $stepId);
        }
        $question = TextInputNode::fromUidAndDependencies(
            'e767ef14-0fae-4cff-84b2-79c7f0812f20',
            TextInput::fromParameters(
                false,
                TextInput::DISPLAY_MODE_REGULAR,
                LocalizedOptionalString::fromLocaleTranslations(['nl' => 'placeholder']),
                false,
                TextInput::DEFAULT_LENGTH_MIN,
                TextInput::DEFAULT_LENGTH_MAX_TEXT,
                []
            ),
            $flowPermission,
            LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                LocalizedRequiredString::fromLocaleTranslations(['nl' => 'Sleep een categorie of vraag op deze vraag om te starten...']),
                LocalizedOptionalString::fromLocaleTranslations(['nl' => '']),
                RoleLocalizedOptionalStringCollection::createEmpty()
            ),
            false,
            TextInputNode::DESCRIPTION_DISPLAY_MODE_BOX,
        );

        $category = CategoryNode::fromUidAndMinimalDependencies('form');
        $category->attachChild($question);
        $formList = FormList::fromRootNode($category);

        $template = new Template(
            $templateId,
            'test-target',
            WorkingVersion::fromElements(
                FormInfo::fromLocalizedStrings(
                    LocalizedRequiredString::fromLocaleTranslations(['en' => 'dan']),
                    LocalizedOptionalString::createEmpty(),
                    RoleLocalizedOptionalStringCollection::createEmpty()
                ),
                WorkFlow::fromSteps($steps),
                $formList,
            ),
            RoleCollection::fromRoleNames(['ROLE_ADMIN', 'ROLE_SCHEME_GROUP_READER', 'ROLE_USER']),
            RoleCollection::fromRoleNames(['ROLE_ADMIN']),
            RoleCollection::createEmpty(),
            RoleCollection::fromRoleNames(['ROLE_ADMIN']),
            RoleCollection::createEmpty()
        );

        $template->publishNewCurrentVersion();

        return $template;
    }
}
