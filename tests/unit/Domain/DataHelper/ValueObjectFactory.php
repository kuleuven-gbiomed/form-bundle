<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\DataHelper;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepFixedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithFixedDateStart;
use KUL\FormBundle\Domain\Template\Exception\StepException;
use KUL\FormBundle\Domain\Template\Exception\StepNotificationException;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithFixedDateStartCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Role;

/**
 * Class ValueObjectFactory.
 */
class ValueObjectFactory
{
    // ##
    // required string
    // ##

    /**
     * @return LocalizedRequiredString
     *
     * @throws LocalizedStringException
     */
    public static function createLocalizedRequiredStringHavingDutchTranslation()
    {
        return LocalizedRequiredString::fromJsonDecodedArray(
            SerializedDataFactory::createTranslationsArray(true, false)
        );
    }

    /**
     * @return LocalizedRequiredString
     *
     * @throws LocalizedStringException
     */
    public static function createLocalizedRequiredStringHavingDutchAndEnglishTranslation()
    {
        return LocalizedRequiredString::fromJsonDecodedArray(
            SerializedDataFactory::createTranslationsArray(true, true)
        );
    }

    /**
     * @return Role
     */
    public static function createFirstRole()
    {
        return Role::fromName(SerializedDataFactory::FIRST_ROLE);
    }

    /**
     * @return Role
     */
    public static function createSecondRole()
    {
        return Role::fromName(SerializedDataFactory::SECOND_ROLE);
    }

    // ##
    // optional string
    // ##

    /**
     * @return LocalizedOptionalString
     *
     * @throws LocalizedStringException
     */
    public static function createLocalizedOptionalStringHavingNoTranslations()
    {
        return LocalizedOptionalString::fromJsonDecodedArray([]);
    }

    /**
     * @return LocalizedOptionalString
     *
     * @throws LocalizedStringException
     */
    public static function createLocalizedOptionalStringHavingDutchTranslation()
    {
        return LocalizedOptionalString::fromJsonDecodedArray(SerializedDataFactory::createTranslationsArray(true, false));
    }

    /**
     * @return LocalizedOptionalString
     *
     * @throws LocalizedStringException
     */
    public static function createLocalizedOptionalStringHavingDutchAndEnglishTranslations()
    {
        return LocalizedOptionalString::fromJsonDecodedArray(SerializedDataFactory::createTranslationsArray(true, true));
    }

    // ##
    // role related optional string
    // ##

    /**
     * @throws LocalizedStringException
     */
    public static function createRoleLocalizedOptionalStringHavingNoTranslations(?string $roleName = null): RoleLocalizedOptionalString
    {
        return RoleLocalizedOptionalString::fromJsonDecodedArray([
            RoleLocalizedOptionalString::KEY_ROLE => $roleName ?? SerializedDataFactory::FIRST_ROLE,
            RoleLocalizedOptionalString::KEY_TRANSLATIONS => [],
        ]);
    }

    /**
     * @throws LocalizedStringException
     */
    public static function createRoleLocalizedOptionalStringHavingDutchTranslation(?string $roleName = null): RoleLocalizedOptionalString
    {
        return RoleLocalizedOptionalString::fromJsonDecodedArray([
            RoleLocalizedOptionalString::KEY_ROLE => $roleName ?? SerializedDataFactory::FIRST_ROLE,
            RoleLocalizedOptionalString::KEY_TRANSLATIONS => SerializedDataFactory::createTranslationsArray(true, false),
        ]);
    }

    /**
     * @throws LocalizedStringException
     */
    public static function createRoleLocalizedOptionalStringHavingDutchAndEnglishTranslations(?string $roleName = null): RoleLocalizedOptionalString
    {
        return RoleLocalizedOptionalString::fromJsonDecodedArray([
            RoleLocalizedOptionalString::KEY_ROLE => $roleName ?? SerializedDataFactory::FIRST_ROLE,
            RoleLocalizedOptionalString::KEY_TRANSLATIONS => SerializedDataFactory::createTranslationsArray(true, true),
        ]);
    }

    // ##
    // multi roles related optional string
    // ##

    /**
     * @return RoleLocalizedOptionalStringCollection
     */
    public static function createRoleLocalizedOptionalStringCollectionHavingNoRoles()
    {
        return new RoleLocalizedOptionalStringCollection([]);
    }

    /**
     * @return RoleLocalizedOptionalStringCollection
     *
     * @throws LocalizedStringException
     */
    public static function createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithDutchTranslation()
    {
        return new RoleLocalizedOptionalStringCollection([
            self::createRoleLocalizedOptionalStringHavingDutchTranslation(),
        ]);
    }

    /**
     * @return RoleLocalizedOptionalStringCollection
     *
     * @throws LocalizedStringException
     */
    public static function createRoleLocalizedOptionalStringCollectionHavingOnlyFirstRoleWithNoTranslations()
    {
        return new RoleLocalizedOptionalStringCollection([
            self::createRoleLocalizedOptionalStringHavingNoTranslations(),
        ]);
    }

    /**
     * @return RoleLocalizedOptionalStringCollection
     *
     * @throws LocalizedStringException
     */
    public static function createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
    {
        return new RoleLocalizedOptionalStringCollection([
            self::createRoleLocalizedOptionalStringHavingDutchTranslation(),
            self::createRoleLocalizedOptionalStringHavingDutchAndEnglishTranslations(SerializedDataFactory::SECOND_ROLE),
        ]);
    }

    // ##
    // other
    // ##

    /**
     * @return RoleCollection
     */
    public static function createFullyLoadedRoleCollection()
    {
        return new RoleCollection([self::createFirstRole(), self::createSecondRole()]);
    }

    /**
     * @return LocalizedInfoWithRequiredLabel
     *
     * @throws LocalizedStringException
     */
    public static function createFullyLoadedLocalizedInfoWithRequiredLabel()
    {
        return LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            self::createLocalizedRequiredStringHavingDutchAndEnglishTranslation(),
            self::createLocalizedOptionalStringHavingDutchAndEnglishTranslations(),
            self::createRoleLocalizedOptionalStringCollectionHavingFirstRoleWithDutchAndSecondRoleWithDutchAndEnglish()
        );
    }

    /**
     * @return StepSubmitSuccessUserInteraction
     *
     * @throws LocalizedStringException
     * @throws StepNotificationException
     */
    public static function createFullLoadedStepSubmitSuccessUserInteraction()
    {
        return StepSubmitSuccessUserInteraction::fromJsonDecodedArray(
            SerializedDataFactory::createStepSubmitSuccessUserInteractionArray()
        );
    }

    /**
     * @param int $periodStartAmount
     * @param int $periodEndAmount
     *
     * @return StepCalculatedPeriod
     *
     * @throws StepException
     */
    public static function createStepCalculatePeriod($periodStartAmount = -10, $periodEndAmount = 10)
    {
        return StepCalculatedPeriod::fromJsonDecodedArray(SerializedDataFactory::createStepCalculatedPeriodArray($periodStartAmount, $periodEndAmount));
    }

    public static function createStepFixedPeriod(
        \DateTimeImmutable $referenceDate,
        int $periodDaysBefore = 10,
        int $periodDaysAfter = 10,
    ): StepFixedPeriod {
        return StepFixedPeriod::fromJsonDecodedArray(SerializedDataFactory::createStepFixedPeriodArray($referenceDate, $periodDaysBefore, $periodDaysAfter));
    }

    /**
     * @param int $daysToAddToStartDate
     *
     * @return StepFixedPeriod
     *
     * @throws StepException
     */
    public static function createStepFixedPeriodByStartDateAndOffset(
        \DateTimeImmutable $fixedStartDate,
        $daysToAddToStartDate,
    ) {
        return StepFixedPeriod::fromDates($fixedStartDate, $fixedStartDate->modify('+'.abs($daysToAddToStartDate).' days'));
    }

    /**
     * @param string $uid
     * @param int    $periodStartAmount
     * @param int    $periodEndAmount
     * @param array  $rolesWithOneSubmit
     * @param bool   $hideReadOnly
     *
     * @return StepWithCalculatedDateStart
     *
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public static function createStepWithCalculatedDateStart(
        $uid,
        $periodStartAmount = -10,
        $periodEndAmount = 10,
        $rolesWithOneSubmit = [SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE],
        $info = null,
        $userInteraction = null,
        $hideReadOnly = false,
    ) {
        if (!$info instanceof LocalizedInfoWithRequiredLabel) {
            $info = self::createFullyLoadedLocalizedInfoWithRequiredLabel();
        }

        if (!$userInteraction instanceof StepSubmitSuccessUserInteraction) {
            $userInteraction = self::createFullLoadedStepSubmitSuccessUserInteraction();
        }

        return StepWithCalculatedDateStart::fromUidAndDependencies(
            $uid,
            self::createStepCalculatePeriod($periodStartAmount, $periodEndAmount),
            false,
            RoleCollection::fromJsonDecodedArray($rolesWithOneSubmit),
            $info instanceof LocalizedInfoWithRequiredLabel ? $info : self::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction instanceof StepSubmitSuccessUserInteraction ? $userInteraction : self::createFullLoadedStepSubmitSuccessUserInteraction(),
            $hideReadOnly
        );
    }

    /**
     * @return StepWithCalculatedDateStartCollection
     *
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public static function createValidStepWithCalculatedDateStartCollection()
    {
        return new StepWithCalculatedDateStartCollection([
            self::createStepWithCalculatedDateStart(
                'uid_cal_1',
                -20,
                -10
            ),
            self::createStepWithCalculatedDateStart(
                'uid_cal_2',
                -10,
                0
            ),
            self::createStepWithCalculatedDateOrPreviousFulFilledStart(
                'uid_cal_3',
                0,
                10
            ),
        ]);
    }

    /**
     * @return StepWithFixedDateStartCollection
     *
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public static function createValidStepWithFixedDateStartCollection()
    {
        return new StepWithFixedDateStartCollection([
            self::createStepWithFixedDateStartByStartDateAndOffset(
                'uid_fix_1',
                new \DateTimeImmutable('2019-05-02 9:00'),
                10
            ),
            self::createStepWithFixedDateStartByStartDateAndOffset(
                'uid_fix_2',
                new \DateTimeImmutable('2019-05-12 9:00'),
                10
            ),
            self::createStepWithFixedDateOrPreviousFulFilledStartByStartDateAndOffset(
                'uid_fix_3',
                new \DateTimeImmutable('2019-05-22 9:00'),
                10
            ),
        ]);
    }

    /**
     * @param string $uid
     * @param int    $periodStartAmount
     * @param int    $periodEndAmount
     * @param array  $rolesWithOneSubmit
     * @param bool   $hideReadOnly
     *
     * @return StepWithCalculatedDateStart
     *
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public static function createStepWithCalculatedDateOrPreviousFulFilledStart(
        $uid,
        $periodStartAmount = -10,
        $periodEndAmount = 10,
        $rolesWithOneSubmit = [SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE],
        $info = null,
        $userInteraction = null,
        $hideReadOnly = false,
    ) {
        if (!$info instanceof LocalizedInfoWithRequiredLabel) {
            $info = self::createFullyLoadedLocalizedInfoWithRequiredLabel();
        }

        if (!$userInteraction instanceof StepSubmitSuccessUserInteraction) {
            $userInteraction = self::createFullLoadedStepSubmitSuccessUserInteraction();
        }

        return StepWithCalculatedDateStart::fromUidAndDependencies(
            $uid,
            self::createStepCalculatePeriod($periodStartAmount, $periodEndAmount),
            true,
            RoleCollection::fromJsonDecodedArray($rolesWithOneSubmit),
            $info instanceof LocalizedInfoWithRequiredLabel ? $info : self::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction instanceof StepSubmitSuccessUserInteraction ? $userInteraction : self::createFullLoadedStepSubmitSuccessUserInteraction(),
            $hideReadOnly
        );
    }

    /**
     * @param string[] $rolesWithOneSubmit
     *
     * @return StepWithFixedDateStart
     *
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public static function createStepWithFixedDateStart(
        string $uid,
        \DateTimeImmutable $referenceDate,
        int $periodDaysBeforeNow = 10,
        int $periodDaysAfterNow = 10,
        array $rolesWithOneSubmit = [SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE],
        ?array $info = null,
        ?array $userInteraction = null,
        bool $hideReadOnly = false,
    ) {
        if (!$info instanceof LocalizedInfoWithRequiredLabel) {
            $info = self::createFullyLoadedLocalizedInfoWithRequiredLabel();
        }

        if (!$userInteraction instanceof StepSubmitSuccessUserInteraction) {
            $userInteraction = self::createFullLoadedStepSubmitSuccessUserInteraction();
        }

        return StepWithFixedDateStart::fromUidAndDependencies(
            $uid,
            self::createStepFixedPeriod($referenceDate, $periodDaysBeforeNow, $periodDaysAfterNow),
            false,
            RoleCollection::fromJsonDecodedArray($rolesWithOneSubmit),
            $info instanceof LocalizedInfoWithRequiredLabel ? $info : self::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction instanceof StepSubmitSuccessUserInteraction ? $userInteraction : self::createFullLoadedStepSubmitSuccessUserInteraction(),
            $hideReadOnly
        );
    }

    /**
     * @param string $uid
     * @param int    $daysToAddToStartDate
     * @param array  $rolesWithOneSubmit
     * @param bool   $hideReadOnly
     *
     * @return StepWithFixedDateStart
     *
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public static function createStepWithFixedDateStartByStartDateAndOffset(
        $uid,
        \DateTimeImmutable $fixedStartDate,
        $daysToAddToStartDate = 10,
        $rolesWithOneSubmit = [SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE],
        $info = null,
        $userInteraction = null,
        $hideReadOnly = false,
    ) {
        if (!$info instanceof LocalizedInfoWithRequiredLabel) {
            $info = self::createFullyLoadedLocalizedInfoWithRequiredLabel();
        }

        if (!$userInteraction instanceof StepSubmitSuccessUserInteraction) {
            $userInteraction = self::createFullLoadedStepSubmitSuccessUserInteraction();
        }

        return StepWithFixedDateStart::fromUidAndDependencies(
            $uid,
            self::createStepFixedPeriodByStartDateAndOffset($fixedStartDate, $daysToAddToStartDate),
            false,
            RoleCollection::fromJsonDecodedArray($rolesWithOneSubmit),
            $info instanceof LocalizedInfoWithRequiredLabel ? $info : self::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction instanceof StepSubmitSuccessUserInteraction ? $userInteraction : self::createFullLoadedStepSubmitSuccessUserInteraction(),
            $hideReadOnly
        );
    }

    /**
     * @param string[] $rolesWithOneSubmit
     *
     * @return StepWithFixedDateStart
     *
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public static function createStepWithFixedDateOrPreviousFulFilledStart(
        string $uid,
        \DateTimeImmutable $referenceDate,
        int $periodDaysBeforeNow = 10,
        int $periodDaysAfterNow = 10,
        array $rolesWithOneSubmit = [SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE],
        ?array $info = null,
        ?array $userInteraction = null,
        bool $hideReadOnly = false,
    ) {
        if (!$info instanceof LocalizedInfoWithRequiredLabel) {
            $info = self::createFullyLoadedLocalizedInfoWithRequiredLabel();
        }

        if (!$userInteraction instanceof StepSubmitSuccessUserInteraction) {
            $userInteraction = self::createFullLoadedStepSubmitSuccessUserInteraction();
        }

        return StepWithFixedDateStart::fromUidAndDependencies(
            $uid,
            self::createStepFixedPeriod($referenceDate, $periodDaysBeforeNow, $periodDaysAfterNow),
            true,
            RoleCollection::fromJsonDecodedArray($rolesWithOneSubmit),
            $info instanceof LocalizedInfoWithRequiredLabel ? $info : self::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction instanceof StepSubmitSuccessUserInteraction ? $userInteraction : self::createFullLoadedStepSubmitSuccessUserInteraction(),
            $hideReadOnly
        );
    }

    /**
     * @param string $uid
     * @param int    $daysToAddToStartDate
     * @param array  $rolesWithOneSubmit
     * @param bool   $hideReadOnly
     *
     * @return StepWithFixedDateStart
     *
     * @throws LocalizedStringException
     * @throws StepException
     * @throws StepNotificationException
     */
    public static function createStepWithFixedDateOrPreviousFulFilledStartByStartDateAndOffset(
        $uid,
        \DateTimeImmutable $fixedStartDate,
        $daysToAddToStartDate = 10,
        $rolesWithOneSubmit = [SerializedDataFactory::FIRST_ROLE, SerializedDataFactory::SECOND_ROLE],
        $info = null,
        $userInteraction = null,
        $hideReadOnly = false,
    ) {
        if (!$info instanceof LocalizedInfoWithRequiredLabel) {
            $info = self::createFullyLoadedLocalizedInfoWithRequiredLabel();
        }

        if (!$userInteraction instanceof StepSubmitSuccessUserInteraction) {
            $userInteraction = self::createFullLoadedStepSubmitSuccessUserInteraction();
        }

        return StepWithFixedDateStart::fromUidAndDependencies(
            $uid,
            self::createStepFixedPeriodByStartDateAndOffset($fixedStartDate, $daysToAddToStartDate),
            true,
            RoleCollection::fromJsonDecodedArray($rolesWithOneSubmit),
            $info instanceof LocalizedInfoWithRequiredLabel ? $info : self::createFullyLoadedLocalizedInfoWithRequiredLabel(),
            $userInteraction instanceof StepSubmitSuccessUserInteraction ? $userInteraction : self::createFullLoadedStepSubmitSuccessUserInteraction(),
            $hideReadOnly
        );
    }
}
