<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\TemplateTarget;

use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Client\Util\File\DownloadFileUrl;
use KUL\FormBundle\Domain\Target\Utility\TargetAnswerExtractor;
use KUL\FormBundle\Domain\Template\Element\Node\Input\CheckboxInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\ChoiceInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\NumberInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionWithAdditionalTextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\CheckBoxInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\NumberInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Entity\StoredTemplateTarget;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class TargetAnswerExtractorTest extends TestCase
{
    public const TEXT_INPUT_NODE_UID = '1f263d95-a70d-4a66-aac9-cf41cd8c1f5d';
    public const NUMBER_INPUT_NODE_UID = 'dc51bacb-1b71-49fb-9447-610faf0ae9dd';
    public const CHOICE_INPUT_NODE_UID_1 = '9fa797d9-8b06-4129-9942-d8bd469731ea';
    public const CHOICE_INPUT_NODE_UID_2 = '3ff01854-803b-444c-83c4-7bfc96f76f2b';
    public const CHOICE_INPUT_NODE_UID_3 = '3ec1c712-c1a2-4eab-81d8-62d1c2620578';
    public const OPTION_INPUT_NODE_UID_1 = 'f03bea7f-7ca0-4a67-9b7a-91e97836236a';
    public const OPTION_INPUT_NODE_UID_2 = 'f1ca32dd-c13d-4fe0-bc8b-8a9923859241';
    public const OTHER_OPTION_INPUT_NODE_UID = '4cae3267-465b-41fe-b1d5-cc7ce1564f08';
    public const CHECKBOX_INPUT_NODE_UID_1 = '67eea5f6-0fea-460b-b1a7-621759039b1c';
    public const CHECKBOX_INPUT_NODE_UID_2 = '79bddc02-267a-437c-8ed6-e7527d3bad29';
    public const UPLOAD_INPUT_NODE_UID = 'd1b5328d-ae7c-4104-9241-4f689e3e7447';

    /**
     * @var TargetAnswerExtractor
     */
    private $targetAnswerExtractor;

    /**
     * @var StoredTemplateTarget|MockObject
     */
    private $storedTemplateTarget;

    protected function setUp(): void
    {
        $downloadFileUrl = new DownloadFileUrl(
            '/somewhere/__NODE_UID__/over_the_rainbox/__FILE_UID__'
        );
        $this->storedTemplateTarget = $this->createMock(StoredTemplateTarget::class);

        $this->storedTemplateTarget->expects(static::once())
            ->method('getSubmittedFormValues')
            ->willReturn($this->getSubmittedValues());

        $this->storedTemplateTarget->expects(static::once())
            ->method('getSavedFiles')
            ->willReturn(new SavedFileCollection());

        $this->targetAnswerExtractor = TargetAnswerExtractor::createForTarget(
            $this->storedTemplateTarget,
            $downloadFileUrl,
            'en'
        );
    }

    public function testItExtractsTextAnswers(): void
    {
        $node = TextInputNode::fromUidAndMinimalDependencies(
            self::TEXT_INPUT_NODE_UID,
            TextInput::fromParameters(),
            LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                LocalizedRequiredString::fromLocaleTranslations(['en' => 'label']),
                LocalizedOptionalString::createEmpty(),
                RoleLocalizedOptionalStringCollection::createEmpty()
            )
        );

        $extractedAnswer = $this->targetAnswerExtractor->getSubmittedValueForNode($node);

        static::assertEquals('text antwoord', $extractedAnswer);
    }

    public function testItExtractsNumberAnswers(): void
    {
        $node = NumberInputNode::fromUidAndMinimalDependencies(
            self::NUMBER_INPUT_NODE_UID,
            NumberInput::fromParameters(),
            LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                LocalizedRequiredString::fromLocaleTranslations(['en' => 'label']),
                LocalizedOptionalString::createEmpty(),
                RoleLocalizedOptionalStringCollection::createEmpty()
            )
        );

        $extractedAnswer = $this->targetAnswerExtractor->getSubmittedValueForNode($node);

        static::assertEquals('666', $extractedAnswer);
    }

    public function testItExtractsSingleChoiceAnswers(): void
    {
        $node = $this->getChoiceInputNodeWithId(self::CHOICE_INPUT_NODE_UID_1);

        $extractedAnswer = $this->targetAnswerExtractor->getSubmittedValueForNode($node);

        static::assertEquals(
            'label-'.Uuid::uuid5(self::CHOICE_INPUT_NODE_UID_1, self::OPTION_INPUT_NODE_UID_1)->toString(),
            $extractedAnswer
        );
    }

    public function testItExtractsMultipleChoiceAnswers(): void
    {
        $node = $this->getChoiceInputNodeWithId(self::CHOICE_INPUT_NODE_UID_2, true);

        $extractedAnswer = $this->targetAnswerExtractor->getSubmittedValueForNode($node);

        static::assertEquals(
            'label-'.Uuid::uuid5(self::CHOICE_INPUT_NODE_UID_2, self::OPTION_INPUT_NODE_UID_1)->toString().
            TargetAnswerExtractor::CHOICES_VALUE_DELIMITER.
            'label-'.Uuid::uuid5(self::CHOICE_INPUT_NODE_UID_2, self::OPTION_INPUT_NODE_UID_2)->toString(),
            $extractedAnswer
        );
    }

    public function testItExtractsOtherOptionChoiceAnswers(): void
    {
        $node = $this->getChoiceInputNodeWithId(self::CHOICE_INPUT_NODE_UID_3);

        $extractedAnswer = $this->targetAnswerExtractor->getSubmittedValueForNode($node);

        static::assertEquals(
            'label-'.Uuid::uuid5(self::CHOICE_INPUT_NODE_UID_3, self::OTHER_OPTION_INPUT_NODE_UID)->toString().
            ': this is something else',
            $extractedAnswer
        );
    }

    /**
     * @dataProvider checkboxDataProvider
     */
    public function testItExtractsCheckboxInputAnswers(CheckBoxInputNode $node, string $answer): void
    {
        $extractedAnswer = $this->targetAnswerExtractor->getSubmittedValueForNode($node);

        static::assertEquals($answer, $extractedAnswer);
    }

    private function getChoiceInputNodeWithId(string $uid, bool $multiple = false): ChoiceInputNode
    {
        $option1uid = Uuid::uuid5($uid, self::OPTION_INPUT_NODE_UID_1)->toString();
        $option2uid = Uuid::uuid5($uid, self::OPTION_INPUT_NODE_UID_2)->toString();
        $option3uid = Uuid::uuid5($uid, self::OTHER_OPTION_INPUT_NODE_UID)->toString();

        $options = new OptionInputNodeCollection(
            [
                OptionInputNode::fromUidAndMinimalDependencies(
                    $option1uid,
                    OptionInput::fromParameters(),
                    LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                        LocalizedRequiredString::fromLocaleTranslations(['en' => 'label-'.$option1uid]),
                        LocalizedOptionalString::createEmpty(),
                        RoleLocalizedOptionalStringCollection::createEmpty()
                    )
                ),
                OptionInputNode::fromUidAndMinimalDependencies(
                    $option2uid,
                    OptionInput::fromParameters(),
                    LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                        LocalizedRequiredString::fromLocaleTranslations(['en' => 'label-'.$option2uid]),
                        LocalizedOptionalString::createEmpty(),
                        RoleLocalizedOptionalStringCollection::createEmpty()
                    )
                ),
                OptionInputNode::fromUidAndMinimalDependencies(
                    $option3uid,
                    OptionWithAdditionalTextInput::fromParameters(),
                    LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                        LocalizedRequiredString::fromLocaleTranslations(['en' => 'label-'.$option3uid]),
                        LocalizedOptionalString::createEmpty(),
                        RoleLocalizedOptionalStringCollection::createEmpty()
                    )
                ),
            ]
        );

        return ChoiceInputNode::fromUidAndMinimalDependencies(
            $uid,
            ChoiceInput::fromParameters(
                false,
                ChoiceInput::DISPLAY_MODE_REGULAR,
                null,
                $multiple,
                false,
                $multiple ? 0 : null,
                $multiple ? 2 : null
            ),
            LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                LocalizedRequiredString::fromLocaleTranslations(['en' => 'label']),
                LocalizedOptionalString::createEmpty(),
                RoleLocalizedOptionalStringCollection::createEmpty()
            ),
            $options
        );
    }

    public function checkboxDataProvider(): array
    {
        return [
            [
                'node' => $this->getCheckboxInputNode(self::CHECKBOX_INPUT_NODE_UID_1),
                'answer' => '✓',
            ],
            [
                'node' => $this->getCheckboxInputNode(self::CHECKBOX_INPUT_NODE_UID_2),
                'answer' => '✗',
            ],
        ];
    }

    private function getCheckboxInputNode(string $uid): CheckBoxInputNode
    {
        return CheckBoxInputNode::fromUidAndMinimalDependencies(
            $uid,
            CheckboxInput::fromParameters(),
            LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                LocalizedRequiredString::fromLocaleTranslations(['en' => 'label-'.$uid]),
                LocalizedOptionalString::createEmpty(),
                RoleLocalizedOptionalStringCollection::createEmpty()
            )
        );
    }

    /**
     * @return string[]
     */
    private function getSubmittedValues(): array
    {
        return [
            self::TEXT_INPUT_NODE_UID => 'text antwoord',
            self::NUMBER_INPUT_NODE_UID => '666',
            // choice question that is not multiple with only first option selected
            self::CHOICE_INPUT_NODE_UID_1 => true,
            Uuid::uuid5(self::CHOICE_INPUT_NODE_UID_1, self::OPTION_INPUT_NODE_UID_1)->toString() => true,
            // choice question that is multiple with two options selected
            self::CHOICE_INPUT_NODE_UID_2 => true,
            Uuid::uuid5(self::CHOICE_INPUT_NODE_UID_2, self::OPTION_INPUT_NODE_UID_1)->toString() => true,
            Uuid::uuid5(self::CHOICE_INPUT_NODE_UID_2, self::OPTION_INPUT_NODE_UID_2)->toString() => true,
            // choice question with other option selected and filled in
            self::CHOICE_INPUT_NODE_UID_3 => true,
            Uuid::uuid5(self::CHOICE_INPUT_NODE_UID_3, self::OTHER_OPTION_INPUT_NODE_UID)->toString() => 'this is something else',
            self::CHECKBOX_INPUT_NODE_UID_1 => true,
            self::CHECKBOX_INPUT_NODE_UID_2 => false,
        ];
    }
}
