<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\TemplateTarget;

use KUL\FormBundle\Client\Access\ClientCombinedAccessObject;
use KUL\FormBundle\Client\Access\ClientWithTargetAccess;
use KUL\FormBundle\Domain\Contract\FormUserInterface;
use KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\Node\Input\TextInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Question\TextInputNode;
use KUL\FormBundle\Domain\Template\Element\WorkFlow;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepCalculatedPeriod;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\Element\StepSubmitSuccessUserInteraction;
use KUL\FormBundle\Domain\Template\Element\WorkFlowStep\StepWithCalculatedDateStart;
use KUL\FormBundle\Domain\Template\Utility\Collection\StepWithCalculatedDateStartCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Template\Version\PublishedVersion;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\TemplateTarget\AbstractJsonAwareStoredTemplateTarget;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleSet;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Domain\Utility\Person;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Entity\Template;
use PHPUnit\Framework\TestCase;

final class AbstractJsonAwareStoredTemplateTargetTest extends TestCase
{
    public const STEP_IDENTIFIER = '2ffd6794-20c4-42d6-8cbb-e5412a82bafd';

    private function getTarget($accessObject): AbstractJsonAwareStoredTemplateTarget
    {
        return new class($accessObject, [], false, true) extends AbstractJsonAwareStoredTemplateTarget {};
    }

    public function testGetSavedFiles(): void
    {
        $accessObjectStub = $this->getClientWithTargetAccessStub(
            new \DateTimeImmutable('2019-05-20')
        );

        $target = $this->getTarget($accessObjectStub);

        $savedFiles = $target->getSavedFiles();

        static::assertEmpty($savedFiles);
    }

    public function testUpdateTemporaryDoesNotReturnNextStep(): void
    {
        $accessObjectStub = $this->getClientWithTargetAccessStub(
            new \DateTimeImmutable('2019-09-24')
        );
        $target = $this->getTarget($accessObjectStub);

        $target->updateTemporary($accessObjectStub, [], false);

        // Step should not change after temporary saving.
        static::assertEquals(self::STEP_IDENTIFIER, $target->getLastUsedStep()->getUid());
    }

    private function getClientWithTargetAccessStub(\DateTimeImmutable $currentDate): ClientCombinedAccessObject
    {
        // Mocks, mocks, mocks 😱
        // FIXME: Too many mocks needed for just creating a form submission for a target.

        $clientWithTargetAccessStub = $this->createMock(ClientWithTargetAccess::class);
        $clientWithTargetAccessStub->method('getTarget')->willReturn(
            new class implements TemplateTargetableInterface {
                public static function getTargetCode(): string
                {
                    return 'whatever';
                }

                public function getTargetIdentifier(): string
                {
                    return 'theId';
                }

                public function getReferenceDate(): \DateTime
                {
                    return new \DateTime();
                }
            }
        );
        $clientWithTargetAccessStub->method('getCurrentDate')->willReturn($currentDate);
        $clientWithTargetAccessStub->method('getAccessingRoles')->willReturn(RoleSet::fromRoleNames(['ROLE_USER']));

        $step = new StepWithCalculatedDateStart(
            self::STEP_IDENTIFIER,
            StepCalculatedPeriod::fromCalculatingAmounts(1, 2),
            false,
            RoleCollection::createEmpty(),
            LocalizedInfoWithRequiredLabel::fromLocalizedRequiredLabel(LocalizedRequiredString::fromLocaleTranslations(
                ['en' => 'dan']
            )),
            StepSubmitSuccessUserInteraction::createEmptyWithDefaults(),
            false
        );

        $stepCollection = new StepWithCalculatedDateStartCollection([$step]);
        $workflow = WorkFlow::fromSteps($stepCollection);

        $question = TextInputNode::fromUidAndMinimalDependencies(
            'd1345d6e-727f-4682-bd9e-b917d4fd4e4c',
            new TextInput(
                false,
                TextInput::DISPLAY_MODE_REGULAR,
                LocalizedOptionalString::createEmpty(),
                false,
                0,
                255,
                []
            ),
            LocalizedInfoWithRequiredLabel::fromLocalizedRequiredLabel(
                LocalizedRequiredString::fromLocaleTranslations(['en' => 'dan'])
            )
        );
        $question->updateFlowPermission(
            FlowPermission::createFromStepUids([self::STEP_IDENTIFIER])->withFullWritePermissionsForRoleInStepUid(
                'ROLE_USER',
                self::STEP_IDENTIFIER
            )
        );

        $formList = FormList::createWithEmptyRootNode();
        $formList = $formList->withNewNodeAddedToParent($question, $formList->getRootNode());

        $template = new Template(
            '50d1fef4-35f9-54bf-b5cf-0d3485152947',
            'test-target',
            WorkingVersion::fromElements(
                FormInfo::fromLocalizedStrings(
                    LocalizedRequiredString::fromLocaleTranslations(['en' => 'dan']),
                    LocalizedOptionalString::createEmpty(),
                    RoleLocalizedOptionalStringCollection::createEmpty()
                ),
                $workflow,
                $formList
            ),
            RoleCollection::fromRoleNames(['ROLE_ADMIN', 'ROLE_SCHEME_GROUP_READER', 'ROLE_USER']),
            RoleCollection::fromRoleNames(['ROLE_ADMIN']),
            RoleCollection::createEmpty(),
            RoleCollection::fromRoleNames(['ROLE_ADMIN']),
            RoleCollection::createEmpty()
        );

        $publishedVersion = PublishedVersion::createForTemplateWithIncrementedVersionNumber($template);

        $userStub = $this->createMock(FormUserInterface::class);
        $userStub->method('getPerson')->willReturn(Person::fromNames('Joe', 'Schmoe'));
        $userStub->method('getIdentifier')->willReturn('f20c2722-b5be-48dc-8b2f-8bd535fd9991');

        $template->publishNewCurrentVersion();

        return new ClientCombinedAccessObject(
            $template,
            $clientWithTargetAccessStub,
            Role::fromName('ROLE_USER'),
            RoleCollection::fromRoleNames(['ROLE_ADMIN']),
            new \DateTimeImmutable('2021-04-01'),
            $step,
            new \DateTimeImmutable('2021-03-01'),
            null,
            null,
        );
    }
}
