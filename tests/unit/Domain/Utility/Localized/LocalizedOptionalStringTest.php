<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Utility\Localized;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use PHPUnit\Framework\TestCase;

/**
 * Class LocalizedOptionalStringTest.
 *
 * @covers  \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString
 */
class LocalizedOptionalStringTest extends TestCase
{
    /** @var array */
    private $defaultData;

    protected function setUp(): void
    {
        parent::setUp();

        $this->defaultData = [
            'nl' => 'dutch string',
            'fr' => '',
            'en' => null,
        ];
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneValidLocaleWithTranslationShouldReturnInstance(): void
    {
        $data = ['nl' => 'test nl  string'];

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertInstanceOf(LocalizedOptionalString::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingNoLocaleWithTranslationShouldReturnInstance(): void
    {
        $data = [];

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertInstanceOf(LocalizedOptionalString::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneValidLocaleWithTranslationAndTwoLocalesWithInvalidTranslationsShouldReturnInstance(): void
    {
        $data = $this->defaultData;

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertInstanceOf(LocalizedOptionalString::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneValidLocaleWithTranslationAndTwoLocalesWithInvalidTranslationsShouldReturnInstanceWithOnlyOneAvailableLocale(): void
    {
        $data = [
            'nl' => 'only valid translation available',
            'fr' => '',
            'en' => null,
        ];

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertInstanceOf(LocalizedOptionalString::class, $result);
        static::assertTrue($result->hasTranslations());
        static::assertCount(1, $result->getAvailableLocales());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneLocaleWithEmptyStringTranslationShouldReturnInstanceWithNoAvailableTranslations(): void
    {
        $data = ['nl' => ''];

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertInstanceOf(LocalizedOptionalString::class, $result);
        static::assertFalse($result->hasTranslations());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneLocaleWithNullAsTranslationShouldReturnInstanceWithNoAvailableTranslations(): void
    {
        $data = ['nl' => null];

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertInstanceOf(LocalizedOptionalString::class, $result);
        static::assertFalse($result->hasTranslations());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneLocaleWithObjectAsTranslationShouldThrowException(): void
    {
        $data = ['nl' => new \stdClass()];

        $this->expectException(LocalizedStringException::class);

        LocalizedOptionalString::fromJsonDecodedArray($data);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneEmptyStringAsLocaleWithValidTranslationShouldThrowException(): void
    {
        $data = [
            '' => 'nl translations without locale',
        ];

        $this->expectException(LocalizedStringException::class);

        LocalizedOptionalString::fromJsonDecodedArray($data);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneInvalidLocaleWithTranslationAndOneValidLocaleWithTranslationShouldThrowException(): void
    {
        $data = [
            '' => 'nl translations without locale',
            'en' => 'en translations with locale',
        ];

        $this->expectException(LocalizedStringException::class);

        LocalizedOptionalString::fromJsonDecodedArray($data);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingLocalesWithMixedTranslationsShouldReturnInstanceWithOnlyFrenchTranslation(): void
    {
        $data = $this->defaultData;

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertTrue($result->hasForLocale('nl'));
        static::assertFalse($result->hasForLocale('fr'));
        static::assertFalse($result->hasForLocale('en'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::hasTranslations()
     */
    public function testHasTranslationsWhenNoTranslationsShouldReturnFalse(): void
    {
        $data = [];

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertFalse($result->hasTranslations());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::hasForLocaleOrFallback()
     */
    public function testHasForLocaleOrFallbackShouldAlwaysReturnTrueForNoMatterWhatLocale(): void
    {
        $data = $this->defaultData;

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertTrue($result->hasForLocaleOrFallback('nl'));
        static::assertTrue($result->hasForLocaleOrFallback('fr'));
        static::assertTrue($result->hasForLocaleOrFallback('en'));
        static::assertTrue($result->hasForLocaleOrFallback('de'));
        static::assertTrue($result->hasForLocaleOrFallback('es'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::getForLocaleOrFallback()
     */
    public function testGetForLocaleOrFallbackWithOnlyOneTranslationAvailableShouldReturnSameStringForAllLocales(): void
    {
        $data = $this->defaultData;

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        $trans = $result->getForLocaleOrFallback('blabla');

        static::assertSame($trans, $result->getForLocaleOrFallback('nl'));
        static::assertSame($trans, $result->getForLocaleOrFallback('en'));
        static::assertSame($trans, $result->getForLocaleOrFallback('fr'));
        static::assertSame($trans, $result->getForLocaleOrFallback('es'));
        static::assertSame($trans, $result->getForLocaleOrFallback('de'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::getForLocale()
     */
    public function testGetForLocaleShouldThrowExceptionForNonExistingLocale(): void
    {
        $data = $this->defaultData;

        $this->expectException(LocalizedStringException::class);

        LocalizedOptionalString::fromJsonDecodedArray($data)->getForLocale('blabla');
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::getForLocale()
     */
    public function testGetForLocaleShouldThrowExceptionForNonAvailableLocale(): void
    {
        $data = $this->defaultData;

        $this->expectException(LocalizedStringException::class);

        LocalizedOptionalString::fromJsonDecodedArray($data)->getForLocale('fr');
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::getForLocale()
     */
    public function testGetForLocaleWithOnlyOneValidTranslationAvailableShouldReturnSameStringForThatOneLocale(): void
    {
        $data = $this->defaultData;
        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertSame($data['nl'], $result->getForLocale('nl'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::hasForLocale()
     */
    public function testHasForLocaleWithOnlyOneValidTranslationAvailableShouldReturnTrueForThatOneLocale(): void
    {
        $data = $this->defaultData;

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertTrue($result->hasForLocale('nl'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::hasForLocale()
     */
    public function testHasForLocaleWithOnlyOneValidTranslationAvailableShouldReturnFalseForAnyAnotherLocale(): void
    {
        $data = $this->defaultData;

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertFalse($result->hasForLocale('en'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::hasFallback()
     */
    public function testHasFallBackWithOnlyOneValidTranslationAvailableShouldReturnTrue(): void
    {
        $data = $this->defaultData;

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertTrue($result->hasFallback());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::hasFallback()
     */
    public function testHasFallBackWithTwoValidTranslationsAvailableShouldReturnTrue(): void
    {
        $data = $this->defaultData;

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertTrue($result->hasFallback());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::getFallback()
     */
    public function testGetFallBackWithTwoValidTranslationsAvailableShouldReturnString(): void
    {
        $data = $this->defaultData;

        $result = LocalizedOptionalString::fromJsonDecodedArray($data)->getFallback();

        static::assertTrue(is_string($result) && '' !== $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::getFallback()
     */
    public function testGetFallBackWithOneValidTranslationsAvailableShouldReturnSameStringForFallbackAsForThatLocale(): void
    {
        $data = $this->defaultData;

        $result = LocalizedOptionalString::fromJsonDecodedArray($data)->getFallback();

        static::assertSame($data['nl'], $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::withUpdatedLocaleTranslation()
     */
    public function testWithUpdatedLocaleTranslationForExistingLocaleShouldReturnWithCorrectUpdatedLocaleTranslation(): void
    {
        $data = ['nl' => 'original dutch'];

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        $updated = $result->withUpdatedLocaleTranslation('nl', 'updated dutch');

        static::assertSame('updated dutch', $updated->getForLocale('nl'));
        static::assertNotEquals($data['nl'], $updated->getForLocale('nl'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::withUpdatedLocaleTranslation()
     */
    public function testWithUpdatedLocaleTranslationWithNewLocaleShouldReturnWithAdditionalLocaleTranslation(): void
    {
        $data = ['nl' => 'original dutch'];

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        $updated = $result->withUpdatedLocaleTranslation('en', 'new english');

        static::assertCount(2, $updated->getAvailableLocales());
        static::assertSame('original dutch', $updated->getForLocale('nl'));
        static::assertSame('new english', $updated->getForLocale('en'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::withRemovedLocale()
     */
    public function testWithRemovedLocaleTranslationWhenTwoTranslationsExistShouldRemoveOneTranslation(): void
    {
        $data = [
            'nl' => 'original dutch',
            'en' => 'original english',
        ];

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        $updated = $result->withRemovedLocale('nl');

        static::assertCount(1, $updated->getAvailableLocales());
        static::assertFalse($updated->hasForLocale('nl'));
        static::assertTrue($updated->hasForLocale('en'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::withRemovedLocale()
     */
    public function testWithRemovedLocaleTranslationWhenSingleExistingLocaleTranslationsIsRemovedShouldReturnWithoutAnyAvailableTranslations(): void
    {
        $data = ['nl' => 'original dutch'];

        $result = LocalizedOptionalString::fromJsonDecodedArray($data);

        $withRemoved = $result->withRemovedLocale('nl');

        static::assertFalse($withRemoved->hasTranslations());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::equalsLocaleTranslation()
     */
    public function testEqualsLocaleTranslationForSameLocaleAndSameCaseTranslationShouldReturnTrue(): void
    {
        $locString = LocalizedOptionalString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);

        static::assertTrue($locString->equalsLocaleTranslation('nl', 'dutch'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::equalsLocaleTranslation()
     */
    public function testEqualsLocaleTranslationForSameLocaleAndDifferentTranslationShouldReturnTrue(): void
    {
        $locString = LocalizedOptionalString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);

        static::assertTrue($locString->equalsLocaleTranslation('nl', 'DUTCH'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::equals()
     */
    public function testEqualsWithSameTranslationsInSameOrderShouldReturnTrue(): void
    {
        $first = LocalizedOptionalString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);
        $second = LocalizedOptionalString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);

        static::assertTrue($second->equals($first));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::equals()
     */
    public function testEqualsWithSameTranslationsInDifferentOrderShouldReturnTrue(): void
    {
        $first = LocalizedOptionalString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);
        $second = LocalizedOptionalString::fromJsonDecodedArray([
            'en' => 'english',
            'nl' => 'dutch',
        ]);

        static::assertTrue($second->equals($first));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::equals()
     */
    public function testEqualsWithSameTranslationsWithDifferentCaseShouldReturnTrue(): void
    {
        $first = LocalizedOptionalString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);
        $second = LocalizedOptionalString::fromJsonDecodedArray([
            'en' => 'English',
            'nl' => 'DUTCH',
        ]);

        static::assertTrue($second->equals($first));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::equals()
     */
    public function testEqualsWithDifferentAmountShouldReturnFalse(): void
    {
        $first = LocalizedOptionalString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);
        // order of locale translations is not important, only amount, locales and each locale string
        $second = LocalizedOptionalString::fromJsonDecodedArray([
            'en' => 'english',
            'nl' => 'dutch',
            'fr' => 'french',
        ]);

        static::assertFalse($second->equals($first));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::equals()
     */
    public function testEqualsWithDifferentTranslationShouldReturnFalse(): void
    {
        $first = LocalizedOptionalString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);
        // order of locale translations is not important, only amount, locales and each locale string
        $second = LocalizedOptionalString::fromJsonDecodedArray([
            'en' => 'english',
            'nl' => 'different dutch',
        ]);

        static::assertFalse($second->equals($first));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::jsonSerialize()
     */
    public function testJsonSerializeAndBackShouldReturnEqualObjects(): void
    {
        $original = LocalizedOptionalString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
            'fr' => null,
        ]);

        $json = json_encode($original->jsonSerialize());

        $deSerialized = LocalizedOptionalString::fromJsonDecodedArray(JsonDecoder::toAssocArray($json));

        static::assertTrue($deSerialized->equals($original));
        static::assertSame(count($original->getAvailableLocales()), count($deSerialized->getAvailableLocales()));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            'nl' => 'dutch',
            'en' => 'english',
        ]);

        $result = LocalizedOptionalString::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
