<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Utility\Localized;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Role;
use PHPUnit\Framework\TestCase;

/**
 * Class RoleLocalizedOptionalString.
 *
 * @covers  \KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString
 */
class RoleLocalizedOptionalStringTest extends TestCase
{
    /** @var array */
    private $defaultData;

    protected function setUp(): void
    {
        parent::setUp();

        $this->defaultData = [
            'nl' => 'dutch string',
            'fr' => '',
            'en' => null,
        ];
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithInvalidValidRoleNameShouldThrowException(): void
    {
        $data = [
            RoleLocalizedOptionalString::KEY_ROLE => '',
            RoleLocalizedOptionalString::KEY_TRANSLATIONS => [
                'nl' => 'dutch string',
            ],
        ];

        $this->expectException(\InvalidArgumentException::class);

        RoleLocalizedOptionalString::fromJsonDecodedArray($data);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithValidRoleAndTranslationsShouldReturnInstanceWithRoleHavingTranslations(): void
    {
        $data = [
            RoleLocalizedOptionalString::KEY_ROLE => 'ROLE_TEST',
            RoleLocalizedOptionalString::KEY_TRANSLATIONS => [
                'nl' => 'dutch string',
            ],
        ];

        $result = RoleLocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertInstanceOf(RoleLocalizedOptionalString::class, $result);
        static::assertInstanceOf(Role::class, $result->getRole());
        static::assertSame('ROLE_TEST', $result->getRole()->getName());
        static::assertTrue($result->hasTranslations());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithValidRoleAndNoTranslationsShouldReturnInstanceWithRoleHavingNoTranslations(): void
    {
        $data = [
            RoleLocalizedOptionalString::KEY_ROLE => 'ROLE_TEST',
            RoleLocalizedOptionalString::KEY_TRANSLATIONS => [],
        ];

        $result = RoleLocalizedOptionalString::fromJsonDecodedArray($data);

        static::assertInstanceOf(RoleLocalizedOptionalString::class, $result);
        static::assertInstanceOf(Role::class, $result->getRole());
        static::assertSame('ROLE_TEST', $result->getRole()->getName());
        static::assertFalse($result->hasTranslations());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            RoleLocalizedOptionalString::KEY_ROLE => 'ROLE_TEST',
            RoleLocalizedOptionalString::KEY_TRANSLATIONS => [
                'nl' => 'dutch string',
            ],
        ]);

        $result = RoleLocalizedOptionalString::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
