<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Utility\Localized;

use KUL\FormBundle\Domain\Exception\LocalizedStringException;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use PHPUnit\Framework\TestCase;

/**
 * Class LocalizedRequiredStringTest.
 *
 * @covers  \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString
 */
class LocalizedRequiredStringTest extends TestCase
{
    /** @var array */
    private $defaultData;

    protected function setUp(): void
    {
        parent::setUp();

        $this->defaultData = [
            'nl' => 'dutch string',
            'fr' => '',
            'en' => null,
        ];
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneValidLocaleWithTranslationShouldReturnInstance(): void
    {
        $data = ['nl' => 'test nl  string'];

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        static::assertInstanceOf(LocalizedRequiredString::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneValidLocaleWithTranslationAndTwoLocalesWithInvalidTranslationsShouldReturnInstance(): void
    {
        $data = $this->defaultData;

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        static::assertInstanceOf(LocalizedRequiredString::class, $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneValidLocaleWithTranslationAndTwoLocalesWithInvalidTranslationsShouldReturnInstanceWithOnlyOneAvailableLocale(): void
    {
        $data = $this->defaultData;

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        static::assertCount(1, $result->getAvailableLocales());
        static::assertNotCount(count($data), $result->getAvailableLocales());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingNoTranslationsShouldThrowException(): void
    {
        $data = [];

        $this->expectException(LocalizedStringException::class);

        LocalizedRequiredString::fromJsonDecodedArray($data);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneLocaleWithEmptyStringTranslationShouldThrowException(): void
    {
        $data = ['nl' => ''];

        $this->expectException(LocalizedStringException::class);

        LocalizedRequiredString::fromJsonDecodedArray($data);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneLocaleWithNullAsTranslationShouldThrowException(): void
    {
        $data = ['nl' => null];

        $this->expectException(LocalizedStringException::class);

        LocalizedRequiredString::fromJsonDecodedArray($data);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneLocaleWithObjectAsTranslationShouldThrowException(): void
    {
        $data = ['nl' => new \stdClass()];

        $this->expectException(LocalizedStringException::class);

        LocalizedRequiredString::fromJsonDecodedArray($data);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneEmptyStringAsLocaleWithValidTranslationShouldThrowException(): void
    {
        $data = [
            '' => 'nl translations without locale',
        ];

        $this->expectException(LocalizedStringException::class);

        LocalizedRequiredString::fromJsonDecodedArray($data);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingOneInvalidLocaleWithTranslationAndOneValidLocaleWithTranslationShouldThrowException(): void
    {
        $data = [
            '' => 'nl translations without locale',
            'en' => 'en translations with locale',
        ];

        $this->expectException(LocalizedStringException::class);

        LocalizedRequiredString::fromJsonDecodedArray($data);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithDataHavingLocalesWithMixedTranslationsShouldReturnInstanceWithOnlyFrenchTranslation(): void
    {
        $data = $this->defaultData;

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        static::assertTrue($result->hasForLocale('nl'));
        static::assertFalse($result->hasForLocale('fr'));
        static::assertFalse($result->hasForLocale('en'));
        static::assertCount(1, $result->getAvailableLocales());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::hasForLocaleOrFallback()
     */
    public function testHasForLocaleOrFallbackShouldAlwaysReturnTrueForNoMatterWhatLocale(): void
    {
        $data = $this->defaultData;

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        static::assertTrue($result->hasForLocaleOrFallback('nl'));
        static::assertTrue($result->hasForLocaleOrFallback('fr'));
        static::assertTrue($result->hasForLocaleOrFallback('en'));
        static::assertTrue($result->hasForLocaleOrFallback('de'));
        static::assertTrue($result->hasForLocaleOrFallback('es'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::getForLocaleOrFallback()
     */
    public function testGetForLocaleOrFallbackWithOnlyOneTranslationAvailableShouldReturnSameStringForAllLocales(): void
    {
        $data = $this->defaultData;

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        $trans = $result->getForLocaleOrFallback('blabla');

        static::assertSame($trans, $result->getForLocaleOrFallback('nl'));
        static::assertSame($trans, $result->getForLocaleOrFallback('en'));
        static::assertSame($trans, $result->getForLocaleOrFallback('fr'));
        static::assertSame($trans, $result->getForLocaleOrFallback('es'));
        static::assertSame($trans, $result->getForLocaleOrFallback('de'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::getForLocale()
     */
    public function testGetForLocaleShouldThrowExceptionForNonExistingLocale(): void
    {
        $data = $this->defaultData;

        $this->expectException(LocalizedStringException::class);

        LocalizedRequiredString::fromJsonDecodedArray($data)->getForLocale('blabla');
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::getForLocale()
     */
    public function testGetForLocaleShouldThrowExceptionForNonAvailableLocale(): void
    {
        $data = $this->defaultData;

        $this->expectException(LocalizedStringException::class);

        LocalizedRequiredString::fromJsonDecodedArray($data)->getForLocale('fr');
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::getForLocale()
     */
    public function testGetForLocaleWithOnlyOneValidTranslationAvailableShouldReturnSameStringForThatOneLocale(): void
    {
        $data = $this->defaultData;
        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        static::assertSame($data['nl'], $result->getForLocale('nl'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::hasForLocale()
     */
    public function testHasForLocaleWithOnlyOneValidTranslationAvailableShouldReturnTrueForThatOneLocale(): void
    {
        $data = $this->defaultData;

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        static::assertTrue($result->hasForLocale('nl'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::hasForLocale()
     */
    public function testHasForLocaleWithOnlyOneValidTranslationAvailableShouldReturnFalseForAnyAnotherLocale(): void
    {
        $data = $this->defaultData;

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        static::assertFalse($result->hasForLocale('en'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::hasFallback()
     */
    public function testHasFallBackWithOnlyOneValidTranslationAvailableShouldReturnTrue(): void
    {
        $data = $this->defaultData;

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        static::assertTrue($result->hasFallback());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::hasFallback()
     */
    public function testHasFallBackWithTwoValidTranslationsAvailableShouldReturnTrue(): void
    {
        $data = $this->defaultData;

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        static::assertTrue($result->hasFallback());
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::getFallback()
     */
    public function testGetFallBackWithTwoValidTranslationsAvailableShouldReturnString(): void
    {
        $data = $this->defaultData;

        $result = LocalizedRequiredString::fromJsonDecodedArray($data)->getFallback();

        static::assertTrue(is_string($result) && '' !== $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::getFallback()
     */
    public function testGetFallBackWithOneValidTranslationsAvailableShouldReturnSameStringForFallbackAsForThatLocale(): void
    {
        $data = $this->defaultData;

        $result = LocalizedRequiredString::fromJsonDecodedArray($data)->getFallback();

        static::assertSame($data['nl'], $result);
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::withUpdatedLocaleTranslation()
     */
    public function testWithUpdatedLocaleTranslationForExistingLocaleShouldReturnWithCorrectUpdatedLocaleTranslation(): void
    {
        $data = ['nl' => 'original dutch'];

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        $updated = $result->withUpdatedLocaleTranslation('nl', 'updated dutch');

        static::assertSame('updated dutch', $updated->getForLocale('nl'));
        static::assertNotEquals($data['nl'], $updated->getForLocale('nl'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::withUpdatedLocaleTranslation()
     */
    public function testWithUpdatedLocaleTranslationWithNewLocaleShouldReturnWithAdditionalLocaleTranslation(): void
    {
        $data = ['nl' => 'original dutch'];

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        $updated = $result->withUpdatedLocaleTranslation('en', 'new english');

        static::assertCount(2, $updated->getAvailableLocales());
        static::assertSame('original dutch', $updated->getForLocale('nl'));
        static::assertSame('new english', $updated->getForLocale('en'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::withRemovedLocale()
     */
    public function testWithRemovedLocaleTranslationWhenTwoTranslationsExistShouldRemoveOneTranslation(): void
    {
        $data = [
            'nl' => 'original dutch',
            'en' => 'original english',
        ];

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        $updated = $result->withRemovedLocale('nl');

        static::assertCount(1, $updated->getAvailableLocales());
        static::assertFalse($updated->hasForLocale('nl'));
        static::assertTrue($updated->hasForLocale('en'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::withRemovedLocale()
     */
    public function testWithRemovedLocaleTranslationWhenSingleExistingLocaleTranslationsIsRemovedShouldThrowException(): void
    {
        $data = ['nl' => 'original dutch'];

        $result = LocalizedRequiredString::fromJsonDecodedArray($data);

        $this->expectException(LocalizedStringException::class);

        $result->withRemovedLocale('nl');
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::equalsLocaleTranslation()
     */
    public function testEqualsLocaleTranslationForSameLocaleAndSamecaseTranslationShouldReturnTrue(): void
    {
        $locString = LocalizedRequiredString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);

        static::assertTrue($locString->equalsLocaleTranslation('nl', 'dutch'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::equalsLocaleTranslation()
     */
    public function testEqualsLocaleTranslationForSameLocaleAndDifferentTranslationShouldReturnTrue(): void
    {
        $locString = LocalizedRequiredString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);

        static::assertTrue($locString->equalsLocaleTranslation('nl', 'DUTCH'));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::equals()
     */
    public function testEqualsWithSameTranslationsInSameOrderShouldReturnTrue(): void
    {
        $first = LocalizedRequiredString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);
        $second = LocalizedRequiredString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);

        static::assertTrue($second->equals($first));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::equals()
     */
    public function testEqualsWithSameTranslationsInDifferentOrderShouldReturnTrue(): void
    {
        $first = LocalizedRequiredString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);
        $second = LocalizedRequiredString::fromJsonDecodedArray([
            'en' => 'english',
            'nl' => 'dutch',
        ]);

        static::assertTrue($second->equals($first));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::equals()
     */
    public function testEqualsWithSameTranslationsWithDifferentCaseShouldReturnTrue(): void
    {
        $first = LocalizedRequiredString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);
        $second = LocalizedRequiredString::fromJsonDecodedArray([
            'en' => 'English',
            'nl' => 'DUTCH',
        ]);

        static::assertTrue($second->equals($first));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::equals()
     */
    public function testEqualsWithDifferentAmountShouldReturnFalse(): void
    {
        $first = LocalizedRequiredString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);
        // order of locale translations is not important, only amount, locales and each locale string
        $second = LocalizedRequiredString::fromJsonDecodedArray([
            'en' => 'english',
            'nl' => 'dutch',
            'fr' => 'french',
        ]);

        static::assertFalse($second->equals($first));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::equals()
     */
    public function testEqualsWithDifferentTranslationShouldReturnFalse(): void
    {
        $first = LocalizedRequiredString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
        ]);
        // order of locale translations is not important, only amount, locales and each locale string
        $second = LocalizedRequiredString::fromJsonDecodedArray([
            'en' => 'english',
            'nl' => 'different dutch',
        ]);

        static::assertFalse($second->equals($first));
    }

    /**
     * @throws LocalizedStringException
     *
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::jsonSerialize()
     */
    public function testJsonSerializeAndBackShouldReturnEqualObjects(): void
    {
        $original = LocalizedRequiredString::fromJsonDecodedArray([
            'nl' => 'dutch',
            'en' => 'english',
            'fr' => null,
        ]);

        $json = json_encode($original);

        $deSerialized = LocalizedRequiredString::fromJsonDecodedArray(JsonDecoder::toAssocArray($json));

        static::assertTrue($deSerialized->equals($original));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            'nl' => 'dutch',
            'en' => 'english',
        ]);

        $result = LocalizedRequiredString::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
