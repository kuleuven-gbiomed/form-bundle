<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Utility;

use KUL\FormBundle\Client\SavedFile\SavedFileCollection;
use KUL\FormBundle\Client\Util\File\AbstractFileUrl;
use KUL\FormBundle\Client\Util\File\DownloadFileUrl;
use KUL\FormBundle\Domain\Target\Utility\TargetAnswerExtractor;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Tests\Unit\NodeFactory;
use PHPUnit\Framework\TestCase;

class TargetAnswerExtractorTest extends TestCase
{
    public function testItExportsChoiceScoresAndLabels(): void
    {
        $uid = '667d443d-9295-4510-8aba-ead6a36ec967';
        $option1UId = '1b1af276-b889-4fb3-a7f9-deee6dc4aec7';
        $option2UId = 'bec93300-3c7a-433d-95f8-80e9129efec8';

        $flowPermission = FlowPermission::createEmpty()
            ->withFullWritePermissionsForRoleInStepUid('ROLE_PARTICIPANT', '0eefd304-0be1-4c8a-96d6-89b3783cbfca')
            ->withReadOnlyPermissionsForRoleInStepUid('ROLE_PARTICIPANT', '802c456d-4017-4b4b-9a35-4457b85da22e');

        $options = [
            [$option1UId, 'answerOne', 1],
            [$option2UId, 'answerTwo', 2],
        ];

        $nodeFactory = new NodeFactory($flowPermission);

        $choiceNode = $nodeFactory->createChoiceNodeWithCustomOptions(
            $uid,
            'bla',
            'nothing',
            $options,
            false,
            true,
        );

        $submittedValues = [
            $uid => true,
            $option1UId => true,
        ];

        $extractor = TargetAnswerExtractor::fromValues(
            $submittedValues,
            new SavedFileCollection([]),
            new DownloadFileUrl(AbstractFileUrl::NODE_UID_PLACEHOLDER.AbstractFileUrl::SAVED_FILE_UID_PLACEHOLDER),
            'en',
        );

        $export = $extractor->getSubmittedValueForNode($choiceNode, true);

        self::assertEquals(1, $export);

        $export = $extractor->getSubmittedValueForNode($choiceNode);

        self::assertEquals('answerOne', $export);

        $choiceNode = $nodeFactory->createChoiceNodeWithCustomOptions(
            $uid,
            'bla',
            'nothing',
            $options,
            true, // should not export scores when multiple
            true,
        );

        $submittedValues = [
            $uid => true,
            $option1UId => true,
            $option2UId => true,
        ];

        $extractor = TargetAnswerExtractor::fromValues(
            $submittedValues,
            new SavedFileCollection([]),
            new DownloadFileUrl(AbstractFileUrl::NODE_UID_PLACEHOLDER.AbstractFileUrl::SAVED_FILE_UID_PLACEHOLDER),
            'en',
        );

        $export = $extractor->getSubmittedValueForNode($choiceNode, true);

        self::assertEquals('answerOne # answerTwo', $export);
    }
}
