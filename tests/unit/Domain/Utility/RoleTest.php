<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Utility;

use KUL\FormBundle\Domain\Utility\Role;
use PHPUnit\Framework\TestCase;

/**
 * Class RoleTest.
 *
 * @covers  \KUL\FormBundle\Domain\Utility\Role
 */
class RoleTest extends TestCase
{
    public const TEST = 'rrrr';

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Role::fromName()
     */
    public function testFromNameShouldReturnInstance(): void
    {
        $name = 'ROLE_TEST';

        $result = Role::fromName($name);

        static::assertInstanceOf(Role::class, $result);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Role::fromName()
     */
    public function testFromNameWithEmptyStringShouldThrowException(): void
    {
        $name = '';

        $this->expectException(\InvalidArgumentException::class);

        Role::fromName($name);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Role::getName()
     */
    public function testGetNameShouldReturnRoleName(): void
    {
        $name = 'ROLE_TEST';

        $result = Role::fromName($name);

        static::assertSame($name, $result->getName());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Role::jsonSerialize()
     */
    public function testJsonSerializeShouldReturnRoleName(): void
    {
        $name = 'ROLE_TEST';

        $result = Role::fromName($name);

        static::assertSame($name, $result->jsonSerialize());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Role::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode('ROLE_TEST');

        $result = Role::fromName(json_decode($originalJson, true));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
