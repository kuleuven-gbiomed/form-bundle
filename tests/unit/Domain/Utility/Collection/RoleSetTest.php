<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Utility\Collection;

use KUL\FormBundle\Domain\Utility\Collection\RoleSet;
use KUL\FormBundle\Domain\Utility\Role;
use PHPUnit\Framework\TestCase;

class RoleSetTest extends TestCase
{
    public function testItShouldNotAddDuplicatedUsingSet(): void
    {
        $roleSet = $this->getRoleSet();
        $roleSet->set('D', Role::fromName('ROLE_ADMIN'));

        $expectedRoleSet = new RoleSet([
            'B' => Role::fromName('ROLE_GOD_MODE'),
            'C' => Role::fromName('ROLE_PEASANT'),
            'D' => Role::fromName('ROLE_ADMIN'),
        ]);

        static::assertEquals($expectedRoleSet, $roleSet);
    }

    public function testItAddsNewRoles(): void
    {
        $roleSet = $this->getRoleSet();
        $roleSet->add(Role::fromName('ROLE_KING'));

        $expectedRoleSet = new RoleSet([
            'A' => Role::fromName('ROLE_ADMIN'),
            'B' => Role::fromName('ROLE_GOD_MODE'),
            'C' => Role::fromName('ROLE_PEASANT'),
            Role::fromName('ROLE_KING'),
        ]);

        static::assertEquals($expectedRoleSet, $roleSet);
    }

    public function testItShouldNotAddDuplicateRoles(): void
    {
        $roleSet = $this->getRoleSet();
        $roleSet->add(Role::fromName('ROLE_KING'));
        $roleSet->add(Role::fromName('ROLE_KING'));

        $expectedRoleSet = new RoleSet([
            'A' => Role::fromName('ROLE_ADMIN'),
            'B' => Role::fromName('ROLE_GOD_MODE'),
            'C' => Role::fromName('ROLE_PEASANT'),
            Role::fromName('ROLE_KING'),
        ]);

        static::assertEquals($expectedRoleSet, $roleSet);
    }

    private function getRoleSet(): RoleSet
    {
        return new RoleSet([
            'A' => Role::fromName('ROLE_ADMIN'),
            'B' => Role::fromName('ROLE_GOD_MODE'),
            'C' => Role::fromName('ROLE_PEASANT'),
        ]);
    }
}
