<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Utility\Collection;

use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Localized\RoleLocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Role;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\SerializedDataFactory;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\ValueObjectFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class RoleLocalizedOptionalStringCollectionTest.
 */
class RoleLocalizedOptionalStringCollectionTest extends TestCase
{
    /** @var Role */
    private $roleOne;
    /** @var Role */
    private $roleTwo;
    /** @var RoleLocalizedOptionalString */
    private $roleOneLocalizedOptionalString;
    /** @var RoleLocalizedOptionalString */
    private $roleTwoLocalizedOptionalString;

    protected function setUp(): void
    {
        parent::setUp();

        $this->roleOne = Role::fromName('ROLE_ONE');
        $this->roleTwo = Role::fromName('ROLE_TWO');
        $this->roleOneLocalizedOptionalString = ValueObjectFactory::createRoleLocalizedOptionalStringHavingDutchAndEnglishTranslations($this->roleOne->getName());
        $this->roleTwoLocalizedOptionalString = ValueObjectFactory::createRoleLocalizedOptionalStringHavingNoTranslations($this->roleTwo->getName());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection::__construct
     */
    public function testConstructorShouldReturnRoleLocalizedOptionalStringCollection(): void
    {
        $result = new RoleLocalizedOptionalStringCollection([$this->roleOneLocalizedOptionalString, $this->roleTwoLocalizedOptionalString]);

        static::assertInstanceOf(RoleLocalizedOptionalStringCollection::class, $result);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithValidDataShouldReturnRoleLocalizedOptionalStringCollection(): void
    {
        $data = [
            SerializedDataFactory::createRoleTranslationsArray($this->roleOne->getName(), true, true),
            SerializedDataFactory::createRoleTranslationsArray($this->roleTwo->getName(), false, false),
        ];

        $result = RoleLocalizedOptionalStringCollection::fromJsonDecodedArray($data);

        static::assertInstanceOf(RoleLocalizedOptionalStringCollection::class, $result);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection::hasOneForRole()
     */
    public function testHasOneForRole(): void
    {
        $result = new RoleLocalizedOptionalStringCollection([$this->roleOneLocalizedOptionalString, $this->roleTwoLocalizedOptionalString]);

        static::assertTrue($result->hasOneForRole($this->roleOne));
        static::assertTrue($result->hasOneForRole($this->roleTwo));
        static::assertFalse($result->hasOneForRole(Role::fromName('FAKE_ROLE')));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection::getOneForRole()
     */
    public function testGetOneForRole(): void
    {
        $result = new RoleLocalizedOptionalStringCollection([$this->roleOneLocalizedOptionalString, $this->roleTwoLocalizedOptionalString]);

        static::assertInstanceOf(RoleLocalizedOptionalString::class, $result->getOneForRole($this->roleOne));
        static::assertSame($this->roleTwo->getName(), $result->getOneForRole($this->roleTwo)->getRole()->getName());
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            SerializedDataFactory::createRoleTranslationsArray($this->roleOne->getName(), true, true),
            SerializedDataFactory::createRoleTranslationsArray($this->roleTwo->getName(), false, false),
        ]);

        $result = RoleLocalizedOptionalStringCollection::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
