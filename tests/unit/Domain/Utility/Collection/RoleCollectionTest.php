<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Domain\Utility\Collection;

use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\JsonDecoder;
use KUL\FormBundle\Domain\Utility\Role;
use PHPUnit\Framework\TestCase;

/**
 * Class RoleCollectionTest.
 *
 * @covers  \KUL\FormBundle\Domain\Utility\Collection\RoleCollection
 */
class RoleCollectionTest extends TestCase
{
    /** @var Role */
    private $roleOne;
    /** @var Role */
    private $roleTwo;

    protected function setUp(): void
    {
        parent::setUp();

        $this->roleOne = Role::fromName('ROLE_ONE');
        $this->roleTwo = Role::fromName('ROLE_TWO');
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Collection\RoleCollection::__construct
     */
    public function testConstructorShouldReturnRoleCollection(): void
    {
        $result = new RoleCollection([$this->roleOne, $this->roleTwo]);

        static::assertInstanceOf(RoleCollection::class, $result);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Collection\RoleCollection::fromJsonDecodedArray()
     */
    public function testFromSerializedArrayWithValidDataShouldReturnRoleCollection(): void
    {
        $data = [
            $this->roleOne->getName(),
            $this->roleTwo->getName(),
        ];

        $result = RoleCollection::fromJsonDecodedArray($data);

        static::assertInstanceOf(RoleCollection::class, $result);
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Collection\RoleCollection::hasRole()
     */
    public function testHasRoleShouldReturnTrue(): void
    {
        $result = new RoleCollection([$this->roleOne, $this->roleTwo]);

        static::assertTrue($result->hasRole($this->roleOne));
    }

    /**
     * @covers \KUL\FormBundle\Domain\Utility\Collection\RoleCollection::jsonSerialize()
     */
    public function testJsonEncodedInstanceBuildFromStrictlyValidJsonDataShouldEqualOriginalJsonData(): void
    {
        $originalJson = json_encode([
            $this->roleOne->getName(),
            $this->roleTwo->getName(),
        ]);

        $result = RoleCollection::fromJsonDecodedArray(JsonDecoder::toAssocArray($originalJson));

        static::assertJsonStringEqualsJsonString($originalJson, json_encode($result));
    }
}
