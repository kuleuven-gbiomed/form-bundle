<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Client\Slugifier;

use KUL\FormBundle\Client\Slugifier\RoleSlugifier;
use PHPUnit\Framework\TestCase;

class RoleSlugifierTest extends TestCase
{
    public function testConstructor(): void
    {
        $slugifier = new RoleSlugifier(['ROLE_ONE', 'ROLE_TWO']);

        static::assertCount(2, $slugifier->getSlugRoles());
        static::assertTrue($slugifier->hasSlugForRole('ROLE_ONE'));
        static::assertTrue($slugifier->hasSlugForRole('ROLE_TWO'));
        static::assertEquals('one', $slugifier->getSlugForRole('ROLE_ONE'));
        static::assertEquals('two', $slugifier->getSlugForRole('ROLE_TWO'));
        static::assertTrue($slugifier->hasRoleForSlug('one'));
        static::assertTrue($slugifier->hasRoleForSlug('two'));
        static::assertEquals('ROLE_ONE', $slugifier->getRoleForSlug('one'));
        static::assertEquals('ROLE_TWO', $slugifier->getRoleForSlug('two'));
    }

    public function testGetSlugForRoleWithInvalidRoleShouldThrowBadMethodCallException(): void
    {
        $slugifier = new RoleSlugifier(['ROLE_ONE', 'ROLE_TWO']);

        $this->expectException(\BadMethodCallException::class);

        $slugifier->getSlugForRole('Role_THREE');
    }

    public function testGetRolForSlugWithInvalidSlugShouldThrowBadMethodCallException(): void
    {
        $slugifier = new RoleSlugifier(['ROLE_ONE', 'ROLE_TWO']);

        $this->expectException(\BadMethodCallException::class);

        $slugifier->getRoleForSlug('three');
    }
}
