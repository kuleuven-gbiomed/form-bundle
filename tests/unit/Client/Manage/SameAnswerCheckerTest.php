<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Client\Manage;

use KUL\FormBundle\Client\Manage\SameAnswerChecker;
use KUL\FormBundle\Domain\Template\Element\FormList;
use KUL\FormBundle\Domain\Template\Element\FormListInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Input\ChoiceInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionInput;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

final class SameAnswerCheckerTest extends TestCase
{
    public function testItGetsLabelOfCommonChoiceQuestionAnswer(): void
    {
        $question1Id = 'cd2550f0-15e8-46ed-8175-696cfe5c920a';
        $question2Id = 'fb0c3367-78b6-40b8-96bd-da737f17e60e';

        /** @var MockObject|FormList $formListStub */
        $formListStub = $this->createMock(FormListInterface::class);
        $formListStub->method('getNodeByUid')->willReturnCallback(
            fn ($id): ChoiceInputNode => $this->getChoiceInputNode($id)
        );

        $checker = new SameAnswerChecker();

        $actual = $checker->getLabelOfCommonChoiceQuestionsAnswer(
            [$question1Id, $question2Id],
            'en',
            $formListStub,
            // for choice questions, the submitted answer array is expected to contain
            // optionId => true for selected options, and nothing for options that are
            // not selected. (So no optionId => false). I think.
            [
                $this->getOptionId($question1Id, '2') => true,
                $this->getOptionId($question2Id, '2') => true,
            ]
        );

        static::assertEquals('Two', $actual);
    }

    private function getChoiceInputNode(string $questionId): ChoiceInputNode
    {
        $option1Id = $this->getOptionId($questionId, '1');
        $option2Id = $this->getOptionId($questionId, '2');

        return ChoiceInputNode::fromUidAndMinimalDependencies(
            $questionId,
            ChoiceInput::fromParameters(),
            LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                LocalizedRequiredString::fromLocaleTranslations([
                    'en' => 'Hello',
                ]),
                LocalizedOptionalString::createEmpty(),
                RoleLocalizedOptionalStringCollection::createEmpty()
            ),
            new OptionInputNodeCollection([
                $this->getOptionInputNode($option1Id, 'One'),
                $this->getOptionInputNode($option2Id, 'Two'),
            ])
        );
    }

    private function getOptionInputNode(string $option1Id, string $label): OptionInputNode
    {
        return OptionInputNode::fromUidAndMinimalDependencies(
            $option1Id,
            OptionInput::fromParameters(),
            LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
                LocalizedRequiredString::fromLocaleTranslations([
                    'en' => $label,
                ]),
                LocalizedOptionalString::createEmpty(),
                RoleLocalizedOptionalStringCollection::createEmpty()
            )
        );
    }

    private function getOptionId(string $questionId, string $number): string
    {
        return Uuid::uuid5($questionId, $number)->toString();
    }
}
