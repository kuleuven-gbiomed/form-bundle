<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Admin\Command;

use KUL\FormBundle\Admin\Command\CopyFormTemplate\CopyFormTemplate;
use KUL\FormBundle\Admin\Command\CopyFormTemplate\CopyFormTemplateHandler;
use KUL\FormBundle\Admin\Event\FormTemplateCopied;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;
use KUL\FormBundle\Tests\Unit\Domain\DataHelper\TemplateFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class CopyFormTemplateHandlerTest extends TestCase
{
    /**
     * @var MessageBusInterface&MockObject
     */
    private $formBundleEventBus;

    /**
     * @var TemplateRepository&MockObject
     */
    private $templateRepository;

    /**
     * @var CopyFormTemplateHandler
     */
    private $copyFormTemplateHandler;

    protected function setUp(): void
    {
        parent::setUp();

        $this->formBundleEventBus = $this->createMock(MessageBusInterface::class);
        $this->templateRepository = $this->createMock(TemplateRepository::class);

        $this->copyFormTemplateHandler = new CopyFormTemplateHandler(
            $this->formBundleEventBus,
            $this->templateRepository,
        );
    }

    public static function createDeactivatedTemplate(string $templateId): Template
    {
        $template = TemplateFactory::create($templateId);
        $template->deactivate();

        return $template;
    }

    public static function templatesDataProvider(): array
    {
        return [
            'an active template' => [
                'template' => TemplateFactory::create('3a4a17ee-cf93-4de1-86d4-9624f24059f6'),
                'isActive' => true,
            ],
            'a deactivated template' => [
                'template' => self::createDeactivatedTemplate('859f717b-bedc-487f-b5d5-bb00e8dcfd53'),
                'isActive' => false,
            ],
        ];
    }

    /**
     * @dataProvider templatesDataProvider
     */
    public function testItShouldKeepTheOriginalActivionStatusAfterMakingACopy(Template $template, bool $isActive): void
    {
        $copyFormTemplate = new CopyFormTemplate(
            $template->getId(),
            '572049a7-7436-42b4-8b31-d2e4177268a8',
            [
                [
                    'language' => 'en',
                    'title' => 'dan',
                ],
            ]
        );

        $this->templateRepository
            ->expects(self::once())
            ->method('getOneById')
            ->with($template->getId())
            ->willReturn($template);

        $this->formBundleEventBus
            ->expects(self::once())
            ->method('dispatch')
            ->with($event = new FormTemplateCopied(
                $template->getId(),
                '572049a7-7436-42b4-8b31-d2e4177268a8',
            ))
            ->willReturn(new Envelope($event));

        $this->templateRepository
            ->expects(self::once())
            ->method('save')
            ->with(self::callback(fn (Template $template): bool => $template->isActive() === $isActive));

        ($this->copyFormTemplateHandler)($copyFormTemplate);
    }
}
