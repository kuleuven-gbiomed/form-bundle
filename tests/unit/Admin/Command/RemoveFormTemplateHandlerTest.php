<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit\Admin\Command;

use KUL\FormBundle\Admin\Command\RemoveFormTemplate\RemoveFormTemplate;
use KUL\FormBundle\Admin\Command\RemoveFormTemplate\RemoveFormTemplateHandler;
use KUL\FormBundle\Admin\Event\PreFormTemplateRemoved;
use KUL\FormBundle\Client\Revert\ResetService;
use KUL\FormBundle\Domain\Template\Element\FormInfo;
use KUL\FormBundle\Domain\Template\Version\WorkingVersion;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Entity\Template;
use KUL\FormBundle\Entity\TemplateRepository;
use KUL\FormBundle\FormBuilder\Repository\CommandHistoryEntryRepository;
use KUL\FormBundle\FormBuilder\Repository\FormRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @covers \KUL\FormBundle\Admin\Command\RemoveFormTemplateHandler
 */
class RemoveFormTemplateHandlerTest extends TestCase
{
    /**
     * @var MessageBusInterface&MockObject
     */
    private $messageBus;

    /**
     * @var TemplateRepository&MockObject
     */
    private $templateRepository;

    /**
     * @var RemoveFormTemplateHandler
     */
    private $removeFormTemplateHandler;

    protected function setUp(): void
    {
        $this->templateRepository = $this->createMock(TemplateRepository::class);
        $this->messageBus = $this->createMock(MessageBusInterface::class);

        $this->removeFormTemplateHandler = new RemoveFormTemplateHandler(
            $this->messageBus,
            $this->templateRepository,
            $this->createMock(FormRepository::class),
            $this->createMock(CommandHistoryEntryRepository::class),
            $this->createMock(ResetService::class),
        );
    }

    public function testItSendsAPreRemovedMessageBeforeDeletingATemplate(): void
    {
        $template = $this->getTemplate();
        $expectedMessage = new PreFormTemplateRemoved($template->getId());
        $messageDispatched = false;
        $returnEnvelop = function () use (&$messageDispatched, $expectedMessage) {
            $messageDispatched = true;

            return new Envelope($expectedMessage);
        };

        $this->templateRepository
            ->expects(self::once())
            ->method('getOneById')
            ->with($template->getId())
            ->willReturn($template);

        $this->messageBus
            ->expects(self::once())
            ->method('dispatch')
            ->with($expectedMessage)
            ->willReturnCallback($returnEnvelop);

        $this->templateRepository
            ->expects(self::once())
            ->method('remove')
            ->with($template)
            ->willReturnCallback(function () use (&$messageDispatched): void {
                self::assertTrue(
                    $messageDispatched,
                    sprintf(
                        'Failed asserting that a %s message was dispatched before deleting %s',
                        PreFormTemplateRemoved::class,
                        Template::class
                    )
                );
            });

        ($this->removeFormTemplateHandler)(new RemoveFormTemplate($template->getId()));
    }

    private function getTemplate(): Template
    {
        return new Template(
            'ef4f7acc-a372-4d77-988a-b39baf2b8410',
            'test-target',
            WorkingVersion::fromElements(
                FormInfo::fromLocalizedStrings(
                    LocalizedRequiredString::fromLocaleTranslations(['en' => 'dan']),
                    LocalizedOptionalString::createEmpty(),
                    RoleLocalizedOptionalStringCollection::createEmpty()
                ),
                null,
                null
            ),
            RoleCollection::fromRoleNames(['ROLE_ADMIN', 'ROLE_SCHEME_GROUP_READER', 'ROLE_USER']),
            RoleCollection::fromRoleNames(['ROLE_ADMIN']),
            RoleCollection::createEmpty(),
            RoleCollection::fromRoleNames(['ROLE_ADMIN']),
            RoleCollection::createEmpty()
        );
    }
}
