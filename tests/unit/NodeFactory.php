<?php

declare(strict_types=1);

namespace KUL\FormBundle\Tests\Unit;

use KUL\FormBundle\Domain\Template\Element\Node\Contract\ScorableInputInterface;
use KUL\FormBundle\Domain\Template\Element\Node\Input\ChoiceInput;
use KUL\FormBundle\Domain\Template\Element\Node\Input\OptionInput;
use KUL\FormBundle\Domain\Template\Element\Node\Permission\FlowPermission;
use KUL\FormBundle\Domain\Template\Element\Node\Question\ChoiceInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Question\OptionInputNode;
use KUL\FormBundle\Domain\Template\Element\Node\Rounding\RoundingModes;
use KUL\FormBundle\Domain\Template\Element\Node\Scoring\ScoringParameters;
use KUL\FormBundle\Domain\Template\Element\Node\Utility\Collection\CalculationDependencyConfigCollection;
use KUL\FormBundle\Domain\Template\Utility\Collection\OptionInputNodeCollection;
use KUL\FormBundle\Domain\Template\Utility\LocalizedInfoWithRequiredLabel;
use KUL\FormBundle\Domain\Utility\Collection\RoleCollection;
use KUL\FormBundle\Domain\Utility\Collection\RoleLocalizedOptionalStringCollection;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedOptionalString;
use KUL\FormBundle\Domain\Utility\Localized\LocalizedRequiredString;
use KUL\FormBundle\Utility\NumberScaleChecker;

class NodeFactory
{
    final public const PLACEHOLDER = 'Please choose...';

    public function __construct(private readonly FlowPermission $flowPermission)
    {
    }

    public function createChoiceNodeWithCustomOptions(
        string $uid,
        string $label,
        string $description,
        array $optionsWithUidLabelAndScore, // [uid, label, score(optional)]
        bool $multiple,
        bool $required = false,
        array $roleNames = [],
    ): ChoiceInputNode {
        $optionInputNodes = OptionInputNodeCollection::createEmpty();
        /** @var float[] $optionScores */
        $optionScores = [];
        foreach ($optionsWithUidLabelAndScore as $option) {
            $optionScores[] = $optionScore = $option[2] ?? null;

            $optionInputNodes->add(
                self::createOptionInputNode($option[0], ['en' => $option[1]], $optionScore)
            );
        }

        $choiceNode = ChoiceInputNode::fromUidAndDependencies(
            $uid,
            ChoiceInput::fromParameters(
                $required,
                ChoiceInput::DISPLAY_MODE_REGULAR,
                LocalizedOptionalString::fromLocaleTranslations(['en' => self::PLACEHOLDER]),
                $multiple, //  one of scoring conditions for choice questions is that it does not allow multiple!
                minSelect: 0,
                maxSelect: count($optionScores),
            ),
            $this->flowPermission,
            self::createInfoWithRequiredLabel(['en' => $label], ['en' => $description]),
            false,
            ChoiceInputNode::DESCRIPTION_DISPLAY_MODE_BOX,
            $optionInputNodes,
            CalculationDependencyConfigCollection::createEmpty(),
            ChoiceInputNode::GLOBAL_SCORE_DEFAULT_WEIGHING_TYPE,
            RoleCollection::fromRoleNames($roleNames),
        );

        $scores = array_column($optionsWithUidLabelAndScore, 2);

        $minScore = min($scores);
        $maxScore = max($scores);

        $minScoreScale = is_float($minScore) ? NumberScaleChecker::getAmountOfDecimalsForNumber($minScore) : ScorableInputInterface::DEFAULT_SCALE;
        $maxScoreScale = is_float($maxScore) ? NumberScaleChecker::getAmountOfDecimalsForNumber($maxScore) : ScorableInputInterface::DEFAULT_SCALE;
        $optionScoreScales = [];
        foreach ($optionScores as $score) {
            $optionScoreScales[] = NumberScaleChecker::getAmountOfDecimalsForNumber($score);
        }

        $maxOptionScoreScale = max($optionScoreScales);
        $scale = max($minScoreScale, $maxScoreScale, $maxOptionScoreScale);

        $choiceNode->updateInput(
            $choiceNode->getInput()->withScoringParameters(
                ScoringParameters::fromParametersRequiredForValidScoring(
                    $minScore,
                    $maxScore,
                    RoundingModes::DEFAULT,
                    $scale,
                    true,
                    true,
                    1
                )
            )
        );

        return $choiceNode;
    }

    /** @param array<string,string> $labelTranslations */
    public static function createOptionInputNode(string $uid, array $labelTranslations, ?float $optionScore = null): OptionInputNode
    {
        return OptionInputNode::fromUidAndDependencies(
            $uid,
            OptionInput::fromParameters(
                false,
                OptionInput::DISPLAY_MODE_REGULAR,
                false,
                $optionScore,
                false
            ),
            FlowPermission::createEmpty(),
            self::createInfoWithRequiredLabel($labelTranslations),
            false,
            OptionInputNode::DESCRIPTION_DISPLAY_MODE_ICON
        );
    }

    /**
     * @param array<string, string> $labelTranslations
     * @param array<string, string> $descriptionTranslations
     */
    public static function createInfoWithRequiredLabel(array $labelTranslations, array $descriptionTranslations = []): LocalizedInfoWithRequiredLabel
    {
        return LocalizedInfoWithRequiredLabel::fromLocalizedStrings(
            LocalizedRequiredString::fromLocaleTranslations($labelTranslations),
            LocalizedOptionalString::fromLocaleTranslations($descriptionTranslations),
            RoleLocalizedOptionalStringCollection::createEmpty()
        );
    }
}
