# Installation

Install this bundle as a Composer dependency. This project is not published on Packagist so you have to point your
 composer.json file straight to the source:

```json
{
  "repositories": {
    form-bundle: {
      "type": "git",
      "url": "git@gitlab.com:kuleuven-gbiomed/form-bundle.git"
    }
  }
}
```

After Composer knows where to find the git repository, you can run:

```bash
composer require kuleuven/form-bundle
```

As part of the installation, you will have to configure two form bundle specific Symfony message buses:
 `form.bundle.command.bus` and `form.bundle.event.bus`, eg:

```yaml
framework:
  messenger:
    default_bus: message.bus
    buses:
      message.bus: ~
      form.bundle.command.bus: ~
      form.bundle.event.bus:
        default_middleware: allow_no_handlers
```

Any commands sent over the command bus, should have a handler configured by the bundle itself.

The events coming from the bundle will probably have no handlers inside your application to start with.
Even if they would, future updates of the bundle might add events so it's always a good idea to add the `allow_no_handlers` option.

Once you start adding multiple message buses, Symfony will be confused which is the default one, so this has to be made explicit.

## JavaScript dependencies & styles
Add these to devDependencies in your package.json

```sh
npm install -D \
form-bundle@./vendor/kuleuven/form-bundle \
blueimp-file-upload \
bootbox@5 \
bootstrap3@npm:bootstrap@3 \
chart.js@4.4.3 \
jquery@3 \
jquery-ui \
selectize
```

```sh
# for vite install this
npm install -D sass-embedded
```

For the bundler you need entries for the following files:
* [jQuery.js](src/Resources/public/js/jQuery.js) ./public/bundles/kulform/js/jQuery.js
* [formBundleDeps.js](src/Resources/public/js/formBundleDeps.js) ./public/bundles/kulform/js/formBundleDeps.js
* Styles (pick one)
  * vite: [formBundleStyles.scss](src/Resources/public/stylesheet/formBundleStyles.scss)  ./public/bundles/kulform/stylesheet/formBundleStyles.scss
  * webpack: [formBundleStyles.js](src/Resources/public/js/formBundleStyles.js) ./public/bundles/kulform/stylesheet/formBundleStyles.js

These are copied by assets:install
jQuery should be made global in a file that is loaded first in a separate twig tag.

### Base twig for Vite
```twig
{% extends 'base.html.twig' %}

{% block stylesheets %}
  {{ vite_entry_link_tags('formBundleStyles') }}
  {{ vite_entry_link_tags('formBundleDeps') }}
{% endblock %}

{% block javascripts %}
  {{ vite_entry_script_tags('jQuery') }}
  {{ vite_entry_script_tags('formBundleDeps') }}
  {{ vite_entry_script_tags('formBundleStyles') }}
  <link rel="stylesheet" href="{{ asset('bundles/kulform/stylesheet/uielement.min.css') }}">
{% endblock %}
```

### Base twig for Webpack Encore
```twig
{% extends 'base.html.twig' %}

{% block stylesheets %}
    {{ encore_entry_link_tags('formBundleStyles') }}
    {{ encore_entry_link_tags('formBundleDeps') }}
    <link rel="stylesheet" href="{{ asset('bundles/kulform/stylesheet/uielement.min.css') }}">
{% endblock %}

{% block javascripts %}
    {{ encore_entry_script_tags('jQuery') }}
    {{ encore_entry_script_tags('formBundleDeps') }}
{% endblock %}
```

-----------------------------------------------------------
## Doctrine Migrations

Add this bundles migration path to your apps' config/packages/doctrine_migrations.yaml
```yaml
doctrine_migrations:
  migrations_paths:
  'FormBundleMigrations': '%kernel.project_dir%/vendor/kuleuven/form-bundle/migrations'
```

-----------------------------------------------------------
# Automated builds

Your build environment probably doesn't have SSH access to this repository.
As a workaround you can overide the repository config mentioned in the installation section with a single composer
 command:

```shell
composer config repositories.form-bundle vcs https://gitlab.com/kuleuven-gbiomed/form-bundle.git
composer update kuleuven/form-bundle
```

This will tell Composer to download the bundle with HTTP, which does not require credentials by default, instead of git.

The solution above only works for public repositories. A more robust solution that will keep working if we ever decide
 to take this project private again, is using a deploy token.

To set this up you first create a deploy token under _settings > repository > Deploy tokens_. Make sure the token scope
 contains _read_repository_. Now configure composer in your build environment to use this token:

 ```shell
composer config gitlab-token.gitlab.com <username> <token>
 ```
-----------------------------------------------------------

# Local development

The Form Bundle won't do much on its own.
That's why we created this demo app: https://gitlab.com/kuleuven-gbiomed/form-bundle-demo.

The demo app is a dependant of the bundle by installing it with Composer. This way you can manually test the bundle
 in an actual application and run the end to end tests before unleashing your changes to other dependant projects.

While it is recommended to always check your changes against the tests included with the demo app, you can also work
 against the Form bundle dependency in other projects. Before you start editing the bundle in the context of a dependant
 project, you will have to set it up as a git repository with the following command:

```shell
rm -rf vendor/kuleuven/form-bundle && composer update --prefer-source kuleuven/form-bundle
```

At this point, if everything went as expected, PHPStorm will probably get confused because you are nesting
 projects/repositories. Go to Preferences > Version control > Directory mappings and add the bundle directory:
 vendor/kuleuven/form-bundle

-----------------------------------------------------------
# kul_form.yaml setup
you must provide a kul_form.yaml file under your app's root->config->packages

the minimally required keys and syntax:
```
kul_form:
    translation:
        default_locale:        # default locale e.g. en
        available_locales:     # list of locales in your app
    roles:
        available_roles:       # list of available roles in your app
        available_admin_roles: # list of available admin roles in your app
    upload:
        submitted_files_dir:   #storage directory where uploaded files (as answers) will be stored
  
    template_types:            
        - targeting_type: # unique string 1
          client:
              routes:
                  storing_view: 
                  storing_save: 
                  storing_auto_temporary_save: 
                  upload_file_question_answer: 
                  download_file_question_answer: 
                  global_score_calculated_value: 
                  overview_target_access_view:
        - targeting_type: # unique string 2
          ...

```
**_template_types_**: 

contains a detailed list of Template targeting types (\KUL\FormBundle\Domain\Template\AbstractJsonAwareTemplate::$targetingType). each targeting_type must be a unique string. the form-bundle will use this targeting_type (by retrieving it from Template::$targetingType on each request - template is a required parameter in all form related requests) to determine which set of your controllers it must use. the purpose of this targetingType is to allow to use different controllers (for rendering and saving a form) and to determine access to the subject of a form (via parameter converters). because the form-bundle does not know about those subjects (e.g. SCONE Placement, EQA Assesment, Datasheet, etc. ) and cannot knwo if the user has access to them. by using the targetingType you can determine in your parameterConverters and your controllers if your accessing user has access to the subject associated to the Template with that targeting type, with what roles and at what time. the subject is also always a required parameter in each request and determined by interface \KUL\FormBundle\Domain\Target\Contract\TemplateTargetableInterface on your app's entities/objects etc. once you clear the user via this targetting type on the subject, then the form-bundle will assume you granted access to subject for that Template (targeting type) and do the rest.

**_routes_**:

it is advised to use parameter converters to check the initial user access to the functionality behind these routes. initial user access being your app's side of the access to the subject of the form, to the Template (type) or its possibly associated entity or object, etc. you can opt to not use paramConverters, but then you will have to do yhis checks in the controllers. 

for routes storing_view, storing_save and overview_target_access_view, this is rather easy, since the controller actions for those routes are always provided by your app (and eventually use the form-bundle services). but all other routes are provided by the form-bundle, so you will have to create your own then, based on those provided. since all these routes use the same syntax for the request parameters, it is easy to provide only one or a few paramConverter to do those checks. the form-bundle will always assert that the syntax for each of the routes is what it expects upon each request, this goes for the amount, the name as well as the optionality for each request parameter. this assertion is important for form-bundle, since it uses what routes you define to build the urls for each functionality when rendering a form.

Please consult Demo project to learn how to build & del with these routes and how to call and use form-bundle services in them. 

remember to use the correct syntax for each url (request) parameter. if you do not, the form-bundle will throw exceptions about it as soon as someone tries to access a form.


_storing_view_:

point to a route and controller action in your app to render a form for the end-user.

this route in your app always points to a custom controller action in your app, where eventually you call the form-bundle to get all info about access and parameters to render the form.

----------------

_storing_save_:

point to a route and controller action in your app to save a form submitted by the end-user or re-render the form in case of user-added validation errors in the form.

this route in your app always points to a custom controller action in your app, where eventually you call the form-bundle to get all info about access, to tell the form-bundle to save the answers and also parameters to re-render the form in case of user-added validation errors in the form .

----------------

_overview_target_access_view_:

point to a route and controller action in your app to get info (workflow status, access type for requesting user) for a form associated with a subject. this route is meant for listing purposes in overview pages where e.g. for a certain template-form, all subjects are listed for which a form should be filled in and this routes then gets the info about access so you can render links or a message showing no access or such...

this route in your app always points to a custom controller action in your app, where eventually you call the form-bundle to get all info about access and parameters to render the form.

---------------

_storing_auto_temporary_save_:

point to a route and controller action that is used to auto-save a form in the background while an end-user is filling in the form.

this route in your app usually points to a controller action provided by the form-bundle: \KUL\FormBundle\Controller\AutoTemporarySaveController::autoTemporarySaveAction.

but you can opt to overwrite this with your own controller, in which case you must do all processing yourself (you can still use the services the provided controller uses as well to do the bulk of the work)

----------------

_upload_file_question_answer_:

point to a route and controller action that is used to upload a file that a user wants to consider as an answer for an upload question.

this route in your app usually points to a controller action provided by the form-bundle: \KUL\FormBundle\Controller\FileController::uploadFileQuestionAnswerAction.

but you can opt to overwrite this with your own controller, in which case all checks and storing is your responsibility (but you can still use & call the services the provided controller uses as well)

_download_file_question_answer_:

point to a route and controller action that is used to download a file that was given or considered as an answer for an upload question.

this route in your app usually points to a controller action provided by the form-bundle: \KUL\FormBundle\Controller\FileController::downloadFileQuestionAnswerAction.

but you can opt to overwrite this with your own controller, in which case all checks and storing is your responsibility (but you can still use & call the services the provided controller uses as well)

_global_score_calculated_value_:

point to a route and controller action that is used by an end-user who presses a button '(re)calculate' which triggers a call to this action to calculate the score and returns with an answer to display in the question's input field or with a message about insufficient data or such...

this route in your app usually points to a controller action provided by the form-bundle: \KUL\FormBundle\Controller\GlobalScoreController::calculateGlobalScoreAction

but you can opt to overwrite this with your own controller, in which case all checks and storing is your responsibility (but you can still use & call the services the provided controller uses as well)



# Info, help and what (dark) magic this bundle can do or is doing to your app

## Revert

a partially or completely filled-in form (StoredTemplateTarget DB record with submitted and/or temporary saved answers to some or all questions that is currently is a certain step of its workflow) can be reset for a fresh restart or rolled back to a previous step. 
these revert actions de facto overrule the normal workflow in filled-in forms. Therefore, revert should be a privilege granted to admin(role)s, not to non-admin user(role)s, who are supposed to follow a form's workflow in one way only: forward. Be aware that if you grant non-admin roles revert options, then you give those associated users the possibility to delete (reset) and/or roll back (edit) already given answers in steps that are already supposed to be finished. revert is very useful in case of impactful changes made to the form's Template in form-builder / admin side which require some already filled-in forms to be reverted to a certain step to make the workflow 'work' again. Revert can also be used to allow users to correct some (non-)deliberate mistakes made earlier in the workflow, but it should be an admin who decides when such correction is to be allowed and subsequently executes the revert, never the non-admin users. Please, never provide your non-admin roles any revert options, unless you have a very, very good reason to do so.

### reset
Reset is nothing more that a complete deletion of the record in the DB, including the history (submission logs), uploaded files, etc. Nothing is kept and the form is to be restarted from scratch. usually it restarts from the first step, but if that first step is already passed its deadline, it will 'start' from a next step when the form is accessed. the latter is why it is better to not allow normal Admin (roles) to reset if first step is passed its deadline. restarting from a next step could ruin the workflow and get the form in some sort of dead workflow limbo. The same for rollback to a step passed its deadline. But it is possible nonetheless and should be done together - before or after but preferably after - the deadline of first step is prolonged via admin side (form builder).

### rollback
A rollback to a previous step means the current workflow status of filled-in form (i.e. its current open step, StoredTemplateTarget::lastUsedStep) is set back to that previous step. With it, all the submitted (non-temp-saved) answers for the relevant questions are converted in to temporary saved answers, so that those relevant questions can be re-answered in that previous step, while the converted answers serve as temporary saved answers so that users can choose to re-use those answers, replace them or edit them before re-submitting. what defines a 'relevant' question, depends on to which step the rollback is to be done, and the write access permissions of that question. if the rollback detects that one or more of the dependency questions for that global score question will be only answerable in the step-to-roll-back-to or one of the steps following the step-to-roll-back-to, then the question is relevant and its submitted answer (if given) will be converted to a temp saved answer. in all other cases, the submitted answer (if given) will NOT be converted and is kept as submitted answer. 

```
for more detailed info on which questions are relevant & subsequently converted during rollback,, please see docblock on
@see \KUL\FormBundle\Client\Revert\FormAnswersRollbackHelper::shouldConvertQuestionAnswer 
```

the rollback will also convert any history log (Submission) about a 'submit action' (not temp saved action) done in the current step to an a 'temporary saved action' and do the same for any 'submit actions' done in the step to roll back to and any step between the step to roll back to and the current step. 


It is also possible (if relevant) to roll back to the current step itself. in that case, the submitted answers for relevant questions and 'submit action' history logs are converted for that current step in the same way as for a rollback to a previous step. this is useful to 'restart' the current step. For example; if (some) users have only 'write access' to submit (not temp save) answers only once in the current step and have done so, they no longer can edit those answers (assuming none of the submit actions triggered a next step to open up (prematurely). rollback to the same current step will give them one more chance to edit the converted answers and re-submit them as submitted answers. 

#### rollback of (auto)calculated global score questions
answers for relevant (auto)calculated global score questions are not converted to a temporary saved answer but instead deleted!

A calculation for a global score question is done automatically only once, when during a submit action of a user, the system detects all the answers of the dependency questions for that global score question are present (all submitted, not just temp saved). once calculated during such a submit action, the answer is stored as submitted (not temp saved) answer, and then no auto calculation is done again! (note that it is possible to configure a global score question to keep auto-recalculating on every submit action in a certain step, but that is alternative behaviour and will not affect the rollback of it). The once calculated answer, however, can always be edited manually for those user-roles that have 'write access' (usually only admin roles, in steps coming after the one were it gets calculated). So, if an answer for the global score question is already available, because of an earlier auto-calculation or due to a manual submit/edit, no auto calculation is thus performed. it is important to realize - especially in light of rollback - that when a user manually fills in an answer for a global score question because he/she has 'write access' to it, no auto-calculation is triggered! if we were to convert (auto) calculated answers of those relevant globals score questions during rollback to temporary saved answers, like with other questions, it would mean that the auto calculation would be done again, since no submitted answer would be present. That might seam fine, but the problem is a temporary saved answer would be present for it after the rollback. that would mean that a user-role with write access could unintentionally manually re-submit that now temp saved answer as submitted answer and then no auto calculation would be done, but the rollback will always have made it possible for at least one of its dependency questions to be edited, thus potentially altering the (auto) calculated answer if it were to be auto calculated server side. for this reason, we do not convert the answer, but delete it. that way, no temp saved answer will be present at all and any user-role that can manually fill it in, will have to use the re-calculate button to get the calculated answer or manually provide an answer. if the user-role does not fill it in - and the global score question is required, which reality has proven it always is - he/she will get at least a validation error upon submit and realize it needs to address the global score question. in most cases - if not all, only admin roles will have 'write access' to global score questions and thus can manually fill it in, but experience learns that admins are less careful when accessing a form with their more-than-other-roles write access.

```
for more detailed info on which global score questions are relevant & subsequently deleted during rollback, please see docblock on
`@see \KUL\FormBundle\Client\Revert\FormAnswersRollbackHelper::shouldEraseGlobalScoreAnswer`
```

note that revert (via end-user side) is not an admin side operation. access to (and rendering of) revert actions is based on both a (implementing) app wide configuration of roles, but those roles als must have access (at least read access to at least one question) to the specific filled-in form itself to get revert options and execute them. only when the user has access to the filled-in form in its current workflow step, will revert actions be considered for rendering & execution.

### configure the revert
once configured, the bundle will provide client-side logic (Twig, JS, etc.) as well as handling the execution of revert action, using a controller and associated routes configured inside this bundle. apart from the minimal configuration, you can always opt to handle it in your own way, especially the client side.

minimal configuration is `roles config` and the `routes config`. unless you do the client side rendering yourself, add `the revert actions dropdown embeddable twig` in the form detail page twig in which you render your form.

#### don't care about revert?
if you don't want to use revert, simply leave out the config parameter `kul_form.roles.roles_allowed_to_revert`. a twig parameter 'revertActions' will still be passed from the form bundle \KUL\FormBundle\Client\TemplateTargetStoringService but it will be an empty collection, which you can simply ignore. direct access to the revertController actions will then always return an 'access denied'.

#### roles config
you must define which roles are allowed to revert via config. remember to use the same roles you use to build question permissions in your templates, as the role must also have access to a filled-in form.

if you leave out this config, revert will not be available for anyone. if you do provide it, you need to supply at least one role. the example below is representative for a real live app (SCONE). you can choose to allow a role to reset, rollback or both. it's better to only grant higher admin roles the possibility to revert to steps that are passed deadline or/and don't grant them access. in reality, template forms are build to give admin roles access in each step.

```
kul_form:
    ...
    roles:
        roles_allowed_to_revert:
            ROLE_COORDINATION:
                reset: false
                rollback: true
                includeRevertToAccessDeniedSteps: false
                includeRevertToDeadlinedSteps: false
            ROLE_COORDINATOR:
                reset: true
                rollback: true
                includeRevertToAccessDeniedSteps: false
                includeRevertToDeadlinedSteps: false
            ROLE_DEVELOPER:
                reset: true
                rollback: true
                includeRevertToAccessDeniedSteps: true
                includeRevertToDeadlinedSteps: true
```

#### routes config
add the following to your routes.yml to give your app access to the revert routes in the form-bundle. the given prefix is a default/example.

```
kul_forms_client_revert_routing:
    resource: '@KULFormBundle/Resources/config/revert_routing.yml'
    prefix:   /kulform_client/revert
```

#### revert actions dropdown embeddable twig
here, we assume you render the detail form page from your implementing app's controller which uses (calls) `\KUL\FormBundle\Client\TemplateTargetStoringService::getStoringViewResponse` to get all end-user-role access-based twig parameters. It is safe to say that this is most likely what you are doing, or you would not be using the bulk of the form-bundle code.

you can embed the provided twig `@KULForm/embed/revert/revert-actions.html.twig`in your twig file that renders the detail page for forms. It will render a dropdown with the available revert options for the accessing user-role. the accessObject that is passed in is the same one already used for all other rendering of the form. the embeddable twig renders a dropdown, but also provide all client side code to handle the revert option selected by the user from the dropdown, including a popover with general info on reverting, extensive 'are you sure' pop-ups, 'processing...' pop-up, and the rendering of revert related flash messages after a revert was executed (which will reload the page via a back-end redirect).
If nothing is shown to the end-user, it means the accessing user (with the role he/she is currently viewing the detail form page) is not allowed to revert or there is simply nothing to revert (yet). if the accessing user has multiple roles, he can always switch between roles to check if one or more of his roles allows him to revert.

if you don't want to use this embeddable twig, you can always render it yourself. the form-bundle will always pass the twig parameter `revertActions` (even if you have not configured `kul_form.roles.roles_allowed_to_revert`, in which case in will always be an empty Collection). only if the end-user-role accessing the detail form page has no access to the form at that time, will this parameter, like all other parameters provided by form-bundle, not be present. the mentioned embeddable twig will not render or do anything nor break or throw error if this parameter is not present. you can use this parameter to render the revert actions yourself. this parameter contains everything you need to build the urls for each revert action. If it is empty, the accessing user is not allowed to revert or there is nothing to revert.


```
 {% include '@KULForm/embed/revert/revert-actions.html.twig' %}
 
 if like to explicitly specify what parameters you're passing between twig files, you can write the above as shown below
 (but then you might have to wrap this around a check to see if `revertActions`, depending where you place this embeddable)
 {% include '@KULForm/embed/revert/revert-actions.html.twig' with {'revertActions': revertActions} %}
 
```

## Auto-save

auto save is a periodic temporary save action that is executed in the background via API.

just like a manual temporary save action, the auto-save will store answers as temporary answers. auto-save has to be enabled for a Template form and can additionally be disabled for that Template form for certain roles . It can also be disabled per request, for example when users are impersonating. When enabled on the Template and not disabled based on role, the user who renders the form with a role, still has to have write access to at least one question for the auto-save to be possible.

when fully enabled and a user renders a form, the auto-save will then be scheduled (in front-end) to be executed every x time - based on a configurable interval in the kul_form.yaml. when the interval finishes, the auto-save will check if the actual auto-save api action needs to be triggered. as long as the user did not make any changes to answers on the questions that were loaded when he/she rendered the form (i.e. loaded the form page), the auto-save will not be triggered and a re-scheduling will be set. As soon as the user makes a change (changes an existing answer or fills in a previously not yet answered question), the auto-save will detect the change. it will then fire off the auto-save api action. only the questions to which user made changes will be sent along for auto-temporary-saving in the back-end. the back-end will then store those answers as the new last known temporary saved answers. contrary to a manual temporary save action, no new submission-log will be stored in history about this auto-save action, but only one submission log for auto-save by this user-role-step-formVersion combo. will be kept and updated upon every auto-save action for that combo.

once the back-end has finished the auto-save action, it will return with a response and the front-end will re-schedule for another auto-save. if the user then makes other changes, then another auto-save api action will be fired off after the scheduled interval. But also if the user did not make any other changes, will the auto-save api action be triggered! so as soon as the user made at least one change in comparison to the answers that were already there when he loaded the form, auto-save api calls will always be triggered. this has two reasons. for one, the check for changes is deliberately always against the initially loaded in answers, and thus not against the changes from the last auto-save api call. and this leads to reason two: we trigger the auto-save api call also, so we can check in the back-end if the user is still working in the step in which he loaded the form. if not, we can then trigger a warning for the user that the form has already passed to a next step, and he can no longer work in that step.

if the user has made no other changes between two scheduled auto-saves intervals, but he made changes against the initially loaded answers, then - as mentioned - an auto-save api call will be triggered, but the back-end will then detect no changes, and it will then skip storing answers and return with a response that no-changes were detected and re-schedule the next auto save. it however possible that another user has already made changes to the answers that are being auto-saved by the first user. in that case changes will be detected and overwritten in storage.

during an auto-save api call and during other api actions (e.g. a calculation triggered via the (re)calculate button and a file upload/download), the auto-save is 'suspended'. this means the scheduling is still running, but the auto-save will immediately re-schedule if it is 'suspended' instead of checking if an auto-save is to be triggered; as soon as such api action is finished, the suspension is lifted. in practice, with a default auto-save interval of 3 minutes, it won't happen that much that another api action will happen at the same moment that an auto-save api action is triggered, but it will happen nonetheless, hence the suspension.

if the auto-save api call in back-end notices that the form is already in another (next) step than the one the user initially loaded the form in, it will respond accordingly to front-end and the user will get a non-dismissable pop-up with info about this and a message & button to reload the form.

if the 'idle checker' is used by the implementing app, and it detects no changes at all in x minutes (default 60 minutes), this idle checker will also suspend auto-save after those x minutes of idleness. just like the auto-save, the idle checker is scheduled to check for changes every x time (minutes) via a kul_form.yaml config. but unlike the auto-save, after the interval finishes, it always checks against changes from the last idle check interval, thus not against the answer from the initial form page load (except, off course, for the first idle check).

#### overriding default auto-save interval

the default interval in which auto-save will be triggered is 3 minutes.
you can choose to overwrite this in the kul_form.yaml config. the  number represents milliseconds. it must be at least 10000 or higher. the default is 180000 (3 minutes)
```
kul_form
    ...
    auto_save:
        interval_in_milliseconds: 180000

```

#### enabling auto-save and overruling it:
this assumes you have embedded the embeddable twig and api controller for auto-save (see 'auto-save route config' and 'auto-save embeddable twig')

the order in which auto-save is enable is the following:

1 the template must have enabled auto-save: `\KUL\FormBundle\Domain\Template\Contract\TemplateInterface::isAutoTemporarySavingEnabled`

2 the user requesting the form must access it with a role that is not configured to diable auto-save on the template: `\KUL\FormBundle\Domain\Template\Contract\TemplateInterface::shouldDisableAutoTemporarySavingForRole`

3 the auto-save must not be forcefully overruled via paramConverter:
the service that you use to render a form and save a form (the latter will also render the form if form-validation errors are found upon submit), has the option to disable auto-save on-the-fly. this is handy if you detect (i.e. in your param converter) that you're dealing with a user impersonation, and the impersonated user has a role that is allowed for auto-save, but you don't want the impersonator to trigger auto-saves in the name of the impersonated user.:
`\KUL\FormBundle\Client\TemplateTargetStoringService::getStoringViewResponse(forceDisableAutoTemporarySave)`
`\KUL\FormBundle\Client\TemplateTargetStoringService::getStoringSaveResponse(forceDisableAutoTemporarySave)`


note: the order 1 - 2 -3 is not entirely correct off course: the `forceDisableAutoTemporarySave` from 3 has ultimate priority off course.
so correct order at request runtime is actually 3-1-2.

#### auto-save route config
a default api controller for the auto-save handling is provided. similar how one is provided for uploading & downloading files and global score calculating. This api controller has all the responses that the also provided embeddable twig knows and can deal with. like for the controllers for uploading & downloading files and global score calculating, it'll be your job to provide paramConverters in your implementing app to build the arguments, including dealing with any access checks for the accessing user to the target of the form (e.g. Datasheet, SetRound in EQA or Placement in SCONE) and making sure the right roles of the user are passed along to the controller; if you don't want to do that, you can crate your own controller and use that one. in that case you can have a look at the provide api controller for auto-save to get you started (probably just copy paste everuthing in there to start)

if you use the provided api controller for the auto-save handling:
```
your_amazing_route_name_that_the kul_form.yaml_knows about:
  path: /template/{templateId}/target/{targetCode}/{targetId}/storing/autosave/{roleSlug}
  defaults: { _controller: KUL\FormBundle\Controller\AutoTemporarySaveController::autoTemporarySaveAction}
  methods: [POST]
```

then add your route to kul_fom.yaml file under client.routes for each targeting_type under main key template_types

```
    template_types: [
        {
            targeting_type: 'datasheet_submissions',
            client: {
                routes: {
                    ...
                    storing_auto_temporary_save: 'your_amazing_route_name_that_the kul_form.yaml_knows about',
                    ...
                }
            },
```

example taken from Demo bundle for 'target': 'datasheet_submission':
```
kul_form.datasheet_submissions.client.storing_auto_temporary_save:
  path: /template/{templateId}/target/{targetCode}/{targetId}/storing/autosave/{roleSlug}
  defaults: { _controller: KUL\FormBundle\Controller\AutoTemporarySaveController::autoTemporarySaveAction}
  methods: [POST]
```


#### auto-save embeddable twig
you can embed the provided twig @KULForm/embed/revert/revert-actions.html.twig in your twig file that renders the detail page for forms. It will take care of all auto-save front-end handling, including triggering the api calls
if you don't want to use this embeddable twig, you can always render it yourself. 

embed this twig in your (body) javascript block:

```
{% block javascripts %}
...
{% include "@KULForm/embed/autosave/js.html.twig" %}
....
{% endblock %}

```
or use the twig that embeds everything the bundle offers, which also includes auto-save
```
{% block javascripts %}
...
{% include '@KULForm/embed/collected-embeds-javascripts.html.twig' %}
....
{% endblock %}

```

## idle checker

the 'idle checker' checks for inactivity by end-user on forms in front-end. it is an always present functionality. it's interval to check for inactivity can be set in the bundle config (kul_form.yaml).

if the 'idle checker' is used by the implementing app, and it detects no changes at all in x minutes (default 60 minutes), then this idle checker will suspend auto-save after those x minutes of idleness and show the user a pop-up saying that no changes were detected in the last x (default 60) minutes. just like the auto-save, the idle checker is scheduled to check for changes every x time (minutes) via a kul_form.yaml config (default 60 minutes). but unlike the auto-save, after the interval finishes, it always checks against changes from the last idle check interval, thus not against the answers (state)) from the initial form page load (except, off course, for the first idle check).

#### overriding default idle-checking interval

the default interval in which an idle-check will be triggered is 60 minutes.
you can choose to overwrite this in the kul_form.yaml config. the  number represents minutes. it must be at least 5 minutes or higher. the default is 60 minutes
```
kul_form
    ...
    form_activity:
        idle_check_interval_in_minutes: 60

```
#### idle checker embeddable twig

embed this twig in your (body) javascript block:

```
{% block javascripts %}
...
{% include "@KULForm/embed/form-activity/idle-checker.html.twig" %}
....
{% endblock %}

```
or use the twig that embeds everything the bundle offers, which also includes the idle-checker
```
{% block javascripts %}
...
{% include '@KULForm/embed/collected-embeds-javascripts.html.twig' %}
....
{% endblock %}

```

## toggle read-only questions

if this option is enabled on a Template, alls user that render a form in which they have a combination of read-only and write access to questions, will get a button rendered that allows them to hide/show the read-only questions. this helps those users to quickly filter out the form to the questions they can fill in (write access). if a user has only read-only questions or only questions with write access when rendering a form, the button is not shown, as it has no purpose in those cases. In reality, that means that admins - who usually are granted write access to all questions that are to be rendered in a step - never get to see that button. 

this option is set per individual Template. default it is set to false (not enabled)


```
\KUL\FormBundle\Domain\Template\AbstractJsonAwareTemplate::enableReadOnlyQuestionsToggling
```

note: in the configuration of a step on a Template, you can opt to render the read-only questions as hidden by default on page landing. this then goes for all users, but still requires that the Template itself is configured to enableReadOnlyQuestions Toggling and that the user has a combo of read-only and write access when rendering the form in that particular step. if enabled, and the user has a combo of read-only and write access, and the step is configured to hide all read-only questions on page landing, then the user will not see the read-only questions upon page landing but can use the toggle button to show (and hide) them again at will.

```
\KUL\FormBundle\Domain\Template\Element\WorkFlowStep\AbstractStep::shouldHideReadOnlyNodesOnLanding
```


#### embeddable twig for read-only questions toggling

embed this twig somewhere in (the header of) your body block, where you want the button to be rendered:

note: this twig also provides the logic and button for the read-only-choice-answer-options Toggling (see next chapter). for both the  the read-only questions and the read-only-choice-answer-options Toggling, the buttons and javascript is provided in this embeddable. but each button will only be rendered if that toggling ability is enabled on the Template and the rendered form contains the relevant questions/access to toggle.

```
{% block body %}
...
{% include '@KULForm/embed/togglers/read-only-togglers.html.twig' %}
....
{% endblock %}

```
or use the twig that embeds everything the bundle offers, which also includes the read-only & read-only-choice-answer-options togglers. (and the main info panel, history panel, role switcher, revert actions and form itself)
```
{% block javascripts %}
...
{% include '@KULForm/embed/collected-embeds-javascripts.html.twig' %}
....
{% endblock %}

```

#### embeddable twig for read-only-choice-answer-options toggling

embed this twig somewhere in (the header of) your body block, where you want the button to be rendered:

note: this twig also provides the logic and button for the read-only questions Toggling (see previous chapter). for both the read-only-choice-answer-options and the read-only questions Toggling, the buttons and javascript is provided in this embeddable. but each button will only be rendered if that toggling ability is enabled on the Template and the rendered form contains the relevant questions/access to toggle.

```
{% block body %}
...
{% include '@KULForm/embed/togglers/read-only-togglers.html.twig' %}
....
{% endblock %}

```
or use the twig that embeds everything the bundle offers, which also includes the read-only-choice-answer-options & the read-only togglers. (and the main info panel, history panel, role switcher, revert actions and form itself)
```
{% block javascripts %}
...
{% include '@KULForm/embed/collected-embeds-javascripts.html.twig' %}
....
{% endblock %}

```



