<?php

declare(strict_types=1);

namespace FormBundleMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250311131758 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE kuleuven_form_fb_form_node ADD multi_unlock_config JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE kuleuven_form_fb_form_node_option ADD multi_unlocks_question_ids JSON DEFAULT NULL');

        $this->addSql("UPDATE kuleuven_form_fb_form_node SET multi_unlock_config = '{\"multiUnlockingQuestionUids\": []}' WHERE multi_unlock_config IS NULL;");
        $this->addSql("UPDATE kuleuven_form_fb_form_node_option SET multi_unlocks_question_ids = '[]' WHERE multi_unlocks_question_ids IS NULL;");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE kuleuven_form_fb_form_node DROP multi_unlock_config');
        $this->addSql('ALTER TABLE kuleuven_form_fb_form_node_option DROP multi_unlocks_question_ids');
    }
}
